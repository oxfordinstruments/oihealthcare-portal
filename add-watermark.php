<?php

@mkdir(".wm", 0777);

$in = preg_replace(['-^/-', '-\.\./-'], '', $_SERVER['PATH_INFO']);

$out = ".wm/$in";

$log = fopen(".wm/log-" . date('Y-m'), 'a');
if (flock($log, LOCK_EX)) {
    fwrite($log, date(DATE_W3C) . '|' . $_SERVER['PATH_INFO']
   	  . '|' . $_SERVER['HTTP_REFERER'] . "\n");
    fflush($log);
    flock($log, LOCK_UN);
    fclose($log);
}

if (file_exists($in)
    && (!file_exists($out) || filemtime($out) < filemtime($in))) {
    $image = new Imagick();
    $image->readImage($in) or die("Couldn't load $in");

    $wm = new Imagick();
    $wm->readImage("watermark.png") or die("Couldn't load $wm");
	
	// how big are the images?
	$iWidth = $image->getImageWidth();
	$iHeight = $image->getImageHeight();
	$wWidth = $wm->getImageWidth();
	$wHeight = $wm->getImageHeight();
	
	if ($iHeight < $wHeight || $iWidth < $wWidth) {
		// resize the watermark
		$wm->scaleImage($iWidth, $iHeight);
	
		// get new size
		$wWidth = $wm->getImageWidth();
		$wHeight = $wm->getImageHeight();
	}
	
	// calculate the position
	$x = ($iWidth - $wWidth) / 2;
	$y = ($iHeight - $wHeight) / 2;
	
	
	
    $image->compositeImage($wm, imagick::COMPOSITE_OVER, $x, $y);

    @mkdir(dirname($out), 0777, true);
    $image->writeImage($out);
}

header('Location: /' . $out);
?>