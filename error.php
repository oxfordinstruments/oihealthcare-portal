<?php
session_name("OIREPORT");
session_start();
require_once($_SERVER['DOCUMENT_ROOT'].'/errors_inc.php');

if(isset($_GET['n'])){
	$error_pri=$errors_array[$_GET['n']];
	$number = $_GET['n'];
	if(isset($_GET['t'])){
		$error_sec=$_GET['t'];
	}else{
		$error_sec="unknown";
	}
}else{
	$error_pri = $errors_array[0];
	$number = '0';
	if(isset($_GET['t'])){
		$error_sec=$_GET['t'];
	}else{
		$error_sec="unknown";
	}
}
if(isset($_GET['p'])) {$page=$_GET['p'];}else{$page="NA";}
$settings = new SimpleXMLElement($_SERVER['DOCUMENT_ROOT'].'/settings.xml', null, true);

?>
<!DOCTYPE html>
<html>
<head>
<title>Service Portal - Oxford Instruments</title>
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link href="/resources/css/screen.min.css" rel="stylesheet" type="text/css" media="screen" />
<link href="/resources/css/320.min.css" rel="stylesheet" type="text/css" />
<link href="/resources/css/720.min.css" rel="stylesheet" type="text/css" />
<link href="/resources/css/960.min.css" rel="stylesheet" type="text/css" />
<link href="/resources/css/print.min.css" rel="stylesheet" type="text/css" media="print" />
<!--[if lt IE 9 ]>
        <link href="/resources/css/screen-ie.min.css" rel="stylesheet" type="text/css" />
    <![endif]-->
<!--[if IE 9 ]>
        <link href="/resources/css/screen-ie9.min.css" rel="stylesheet" media="screen" />
    <![endif]-->
<link href="/resources/css/main_engineer_960.css" rel="stylesheet" type="text/css" media="screen" />
<link href="/resources/css/main_engineer_720.css" rel="stylesheet" type="text/css" media="screen" />
<link href="/resources/css/main_engineer_320.css" rel="stylesheet" type="text/css" media="screen" />
<meta charset="UTF-8" />
<link href="/resources/images/favicon.ico" type="image/x-icon" rel="shortcut icon"/>
<link href="/resources/images/favicon.ico" type="image/x-icon" rel="icon"/>
<script type="text/javascript" src="/resources/js/jquery/jquery.min.js"></script>

<script type="text/javascript" src="/resources/js/skinnytip.js"></script>
<script type="text/javascript">
$(document).ready(function() {
 		
});
</script>

</head>
<body>
<?php require($_SERVER['DOCUMENT_ROOT'].'/report/head_foot/default_header.php'); ?>
<div id="OIReportContent"> 
    <h1>Error has occurred.</h1><br>
	
	<h2>Error:&nbsp;
	<?php echo $error_pri; ?><br>
	Error Detail:&nbsp;
	<?php echo $error_sec; ?><br>
	Error Number:&nbsp;
	<?php echo $number; ?><br>
	Page in error:&nbsp;
	<?php echo $page; ?></h2>
	<br>
	<h2>Please contact support at <a href="mailto:<?php echo $settings->email_support; ?>"><?php echo $settings->email_support; ?></a> if this error persists.</h2>
	<h2>If you were logged into the portal then you have now been logged out for security.</h2>
</div>
<?php require($_SERVER['DOCUMENT_ROOT'].'/report/head_foot/default_footer.php'); ?>