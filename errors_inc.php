<?php
//Errors Definitions
$errors_array = array(
	0 => 'Unknown Error',
	1 => 'The portal is closed for maintenance',
	2 => 'Debugging',
	3 => 'Logout',
	4 => 'Invalid URL GET',
	5 => 'Invalid URL PUT',
	100 => 'Refresh Prevented',
	101 => 'System Archived',
	102 => 'System Edited',
	103 => 'System Created',
	104 => 'Trailer Deleted',
	105 => 'Trailer Created',
	106 => 'Trailer Edited',
	107 => 'Customer Deleted',
	108 => 'Customer Created',
	109 => 'Customer Edited',
	110 => 'Facility Deleted',
	111 => 'Facility Created',
	112 => 'Facility Edited',
	113 => 'System Future New',
	114 => 'System Future Convert',
	200 => 'Emails Sent',
	1000 => 'SQL Error',
	1001 => 'Invalid Ban',
	1002 => 'Login Error',
	1003 => 'Default Role Error',
	1004 => 'Secret Update Error',
	1005 => 'Reset email could not be sent',
	1006 => 'Registration error please contact support',
	1007 => 'Registration email could not be sent',
	1008 => 'User banned',
	1009 => 'User locked out',
	1010 => 'Inacive user',
	1011 => 'Login Incorrect',
	1012 => 'Token missing from password reset request',
	1013 => 'Please request another password reset',
	1014 => 'Reset complete email could not be sent',
	1015 => 'Bad ban type',
	1016 => 'Invalid System Unique ID',
	1017 => 'Invalid UID',
	1018 => 'Invalid ID Number',
	1019 => 'Invalid Unique ID or System ID',
	1020 => 'No permission to edit face sign-off',
	1021 => 'Invalid Sectors',
	1022 => 'Cannot get reports for a system without a System ID',
	1023 => 'You do not have permission to create or edit reports',
	1024 => 'Invalid Request ID',
	1025 => 'Invalid Report ID',
	1026 => 'No email receipents',
	1027 => 'No Delete Info',
	1028 => 'No Edit Info',
	1029 => 'Cannot send evening update empty',
	1030 => 'Cannot send evening update without engineer name',
	1031 => 'Cannot send evening update without System ID',
	1032 => 'New role not set',
	1033 => 'Invalid User',
	1034 => 'Role Error',
	1035 => 'Refresh prevented',
	1036 => 'Incomplete Address',
	1037 => 'Password reset request for user',
	1038 => 'Secret reset request for user',
	1039 => 'Customer with no systems',
	1040 => 'Engineer/Contractor does not have permission to have no assigne systems',
	1041 => 'Running LDAP operations',
	1042 => 'Invalid Group',
	1043 => 'No Systems Assigned',
	1044 => 'Invalid System ID',
	1045 => 'Template Missing',
	1046 => 'Year not supplied',
	1047 => 'Could not change help settings',
	1048 => 'Invalid Unique ID',
	1049 => 'Invalid Table',
	1050 => 'Reading Deleted',
	1051 => 'Reading Insert',
	1052 => 'Reading Update',
	1053 => 'Email Send Error',
	1054 => 'Invalid Facility Unique ID',
	1055 => 'Invalid Customer Unique ID',
	1056 => 'You do not have permission to submit a PAR/CAR/FBC',
	1057 => 'You do not have permission to edit a PAR/CAR/FBC',
	1058 => 'You do not have permission to close a PAR/CAR/FBC',
	1059 => 'Invalid CAR Unique ID',
	1060 => 'Invalid PAR Unique ID',
	1061 => 'Invalid FBC Unique ID',
	1062 => 'PAR/CAR/FBC Info',
	1063 => 'Closed CAR/PAR/FBC edit not allowed',
	1064 => 'Invalid CAR Unique ID in GET/POST',
	1065 => 'Invalid FBC Unique ID in GET/POST',
	1066 => 'Invalid PAR Unique ID in GET/POST',
	1067 => 'Invalid System Type',
	1068 => 'System Type not in GET',
	1069 => 'Invalid Sales Quote MYSQLI Insert ID',
	1070 => 'Edit converted quote not allowed',
	1071 => 'No tools permission',
	1072 => 'Registration Error',
	1073 => 'Contact Support to reset your password and secret questions',
	1074 => 'Invalid Version Number',
	1075 => 'Version is 0',
	1076 => 'You do not have permission to print face sheets',
	1077 => 'Unsubscribe error occurred. Please contact support!',
	1078 => 'Email address not subscribed. Please contact support if this is incorrect.'
);

?>