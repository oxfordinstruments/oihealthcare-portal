<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 6/19/2017
 * Time: 12:30 PM
 */
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);
ini_set("display_errors", true);

require_once 'vendor/autoload.php';

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/php_utils.php');
$php_utils = new phpUtils();

$whichserver = $php_utils->whichServer();

if($whichserver != false){
	if($whichserver == 'evotodi' or $whichserver == 'oiheal5'){$db = 'oiheal5_fagt';}
	if($whichserver == 'oihportal'){$db = 'oihporta_fagt';}
}

$mysqli = new mysqli("$host", "$username", "$password", $db);

$csv_cols = array('attach_id', 'attach_uuid', 'attach_aid', 'attach');

Kint::$enabled_mode = true;

$sql = "SELECT a.id, a.aid, a.name, a.uuid
		FROM attach AS a 
		ORDER BY a.aid;";
$arr = array();
$result = $mysqli->query($sql);
while ($row = $result->fetch_assoc()){
	$arr[$row['id']] = array('id'=>$row['id'], 'uuid'=>$row['uuid'], 'aid'=>$row['aid'], 'name'=>$row['name']);
}


$fh = fopen('php://output', 'w');
ob_start();
fputcsv($fh, $csv_cols);
foreach ($arr as $attach){
	$id = $attach['id'];
	$uuid = $attach['uuid'];
	$aid = $attach['aid'];
	$name = $aid . ' ' . $attach['name'];
	fputcsv($fh, array($id, $uuid, $aid, $name));
}
$string = ob_get_clean();

$filename = 'csv_' . date('Ymd') .'_' . date('His');

// Output CSV-specific headers
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);
header("Content-Type: application/octet-stream");
header("Content-Disposition: attachment; filename=\"$filename.csv\";" );
header("Content-Transfer-Encoding: binary");
exit($string);