<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 6/19/2017
 * Time: 12:30 PM
 */
//error_reporting(E_ALL);
//ini_set("display_errors", true);

require_once 'vendor/autoload.php';

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/php_utils.php');
$php_utils = new phpUtils();

$whichserver = $php_utils->whichServer();

if($whichserver != false){
	if($whichserver == 'evotodi' or $whichserver == 'oiheal5'){$db = 'oiheal5_fagt';}
	if($whichserver == 'oihportal'){$db = 'oihporta_fagt';}
}

$mysqli = new mysqli("$host", "$username", "$password", $db);

$csv_cols = array('id', 'uuid', 'group_id', 'group');

Kint::$enabled_mode = true;

$sql = "SELECT g.id, g.gid, g.name, g.uuid FROM groups AS g	ORDER BY g.gid;";
$arr = array();
$result = $mysqli->query($sql);
while ($row = $result->fetch_assoc()){
	$arr[$row['id']] = array('id'=>$row['id'], 'gid'=>$row['gid'], 'name'=>$row['name'], 'uuid'=>$row['uuid']);
}


$fh = fopen('php://output', 'w');
ob_start();
fputcsv($fh, $csv_cols);
foreach ($arr as $group){
	$id = $group['id'];
	$uuid = $group['uuid'];
	$gid = $group['gid'];
	$name = $gid . ' ' . $group['name'];
	fputcsv($fh, array($id, $uuid, $gid, $name));
}
$string = ob_get_clean();

$filename = 'csv_' . date('Ymd') .'_' . date('His');

// Output CSV-specific headers
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);
header("Content-Type: application/octet-stream");
header("Content-Disposition: attachment; filename=\"$filename.csv\";" );
header("Content-Transfer-Encoding: binary");
exit($string);