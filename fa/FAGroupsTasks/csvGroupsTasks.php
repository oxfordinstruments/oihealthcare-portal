<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 6/19/2017
 * Time: 12:30 PM
 */
//error_reporting(E_ALL);
//ini_set("display_errors", true);

require_once 'vendor/autoload.php';

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/php_utils.php');
$php_utils = new phpUtils();

$whichserver = $php_utils->whichServer();

if($whichserver != false){
	if($whichserver == 'evotodi' or $whichserver == 'oiheal5'){$db = 'oiheal5_fagt';}
	if($whichserver == 'oihportal'){$db = 'oihporta_fagt';}
}

$mysqli = new mysqli("$host", "$username", "$password", $db);

$csv_cols = array('group_id', 'group_uuid', 'task_id', 'task_uuid', 'seq');

Kint::$enabled_mode = true;

$fh = fopen('php://output', 'w');
ob_start();
fputcsv($fh, $csv_cols);

$sql = "SELECT gt.group_id, gt.task_id, gt.group_uuid, gt.task_uuid, gt.seq
		FROM groups_tasks AS gt 
		ORDER BY gt.group_id, gt.seq;";
$groups = array();
$result = $mysqli->query($sql);
while ($row = $result->fetch_assoc()){
	fputcsv($fh, array($row['group_id'], $row['group_uuid'], $row['task_id'], $row['task_uuid'], $row['seq']));
}
$string = ob_get_clean();

$filename = 'csv_' . date('Ymd') .'_' . date('His');

// Output CSV-specific headers
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);
header("Content-Type: application/octet-stream");
header("Content-Disposition: attachment; filename=\"$filename.csv\";" );
header("Content-Transfer-Encoding: binary");
exit($string);