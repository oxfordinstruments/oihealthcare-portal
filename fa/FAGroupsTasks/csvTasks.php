<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 6/19/2017
 * Time: 12:30 PM
 */
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);
ini_set("display_errors", true);

require_once 'vendor/autoload.php';

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/php_utils.php');
$php_utils = new phpUtils();

$whichserver = $php_utils->whichServer();

if($whichserver != false){
	if($whichserver == 'evotodi' or $whichserver == 'oiheal5'){$db = 'oiheal5_fagt';}
	if($whichserver == 'oihportal'){$db = 'oihporta_fagt';}
}

$mysqli = new mysqli("$host", "$username", "$password", $db);

$csv_cols = array(
	'task_id',
	'task_uuid',
	'task_tid',
	'Name',
	'Description',
	'Unit Cost',
	'Markup Type',
	'Markup Value',
	'Unit Price',
	'Mandatory',
	'Estimated Hours',
	'Estimated Minutes',
	'GL Account',
	'Taxable',
	'Tax Name1',
	'Tax Group1',
	'Tax Name2',
	'Tax Group2',
	'PM Completed Date',
	'PM Task',


	);

Kint::$enabled_mode = true;

$sql = "SELECT t.id, t.tid, t.name, t.uuid, t.hours, t.mins
		FROM tasks AS t 
		ORDER BY t.tid;";
$arr = array();
$result = $mysqli->query($sql);
while ($row = $result->fetch_assoc()){
	$desc = preg_replace('/T\d*\.\d*/', '', $row['name']);
	$pm = 'N';
	if(preg_match('/\bPM\b/', $row['name'])){
		$pm = 'Y';
	}

	$arr[$row['id']] = array(
		'id'=>$row['id'],
		'uuid'=>$row['uuid'],
		'tid'=>$row['tid'],
		'name'=>$row['name'],
		'desc'=>$desc,
		'pm'=>$pm,
		'hours'=>$row['hours'],
		'mins'=>$row['mins']
	);
}


$fh = fopen('php://output', 'w');
ob_start();
fputcsv($fh, $csv_cols);
$b = null;
$z = 0;
foreach ($arr as $task){
	$id = $task['id'];
	$uuid = $task['uuid'];
	$tid = $task['tid'];
	$name = $tid . ' ' . $task['name'];
	$desc = $task['desc'];
	$pm = $task['pm'];
	$hours = $task['hours'];
	if($hours == 0){
		$hours = null;
	}
	$mins = $task['mins'];
	if($mins == 0){
		$mins = null;
	}
	fputcsv($fh, array($id, $uuid, $tid, $name, $desc, $b, $b, $b, $b, $z, $hours, $mins, $b, $z, $b, $b, $b, $b, $b, $pm));
}
$string = ob_get_clean();

$filename = 'csv_' . date('Ymd') .'_' . date('His');

// Output CSV-specific headers
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);
header("Content-Type: application/octet-stream");
header("Content-Disposition: attachment; filename=\"$filename.csv\";" );
header("Content-Transfer-Encoding: binary");
exit($string);