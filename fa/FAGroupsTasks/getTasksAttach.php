<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 6/1/2017
 * Time: 6:47 PM
 */
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);
ini_set('display_errors', 'On');

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/php_utils.php');
$php_utils = new phpUtils();

$whichserver = $php_utils->whichServer();

if($whichserver != false){
	if($whichserver == 'evotodi' or $whichserver == 'oiheal5'){$db = 'oiheal5_fagt';}
	if($whichserver == 'oihportal'){$db = 'oihporta_fagt';}
}
$mysqli = new mysqli("$host", "$username", "$password", $db);

if(!isset($_GET['id'])){
	die('bad');
}

$result = $mysqli->query("SELECT a.id, a.aid, a.name FROM attach AS a
								LEFT JOIN tasks_attach AS ta ON ta.attach_id = a.id
								WHERE ta.task_id = '".$_GET['id']."'
								ORDER BY ta.seq;");
$attach = array();
while ($row = $result->fetch_assoc()){
	array_push($attach, $row['aid']);
}
echo json_encode($attach);