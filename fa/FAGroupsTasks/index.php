<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 6/1/2017
 * Time: 9:58 AM
 */

/**
 * Setting production to true will disable the ability to delete groups and tasks
 */
$production = true;

error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);
ini_set("display_errors", true);

//phpinfo();
//die(__DIR__);

require_once 'vendor/autoload.php';
$loader = new Twig_Loader_Filesystem('views');
$twig = new Twig_Environment($loader, array(
	'cache' => false,
));

Kint::$enabled_mode = true;

//require_once 'settings.php';
//$mysqli = new mysqli($server, $username, $password, $db);

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/php_utils.php');
$php_utils = new phpUtils();

$whichserver = $php_utils->whichServer();

if($whichserver != false){
	if($whichserver == 'evotodi' or $whichserver == 'oiheal5'){$db = 'oiheal5_fagt';}
	if($whichserver == 'oihportal'){$db = 'oihporta_fagt';}
}
$mysqli = new mysqli("$host", "$username", "$password", $db);

$tasks = getTasks();
$groups = getGroups();
$attachs = getAttachs();
$rightData = getRightData();
$tasksData = getTasksData();
$tasksAttachData = getTasksAttachData();
$groupsData = getGroupsData();
$groupsTasksData = getGroupsTasksData();
$attachData = getAttachsData();

//Kint::dump($attachs);
//die();


if(count($_POST) == 0) {
	$template = $twig->loadTemplate('index.html.twig');
	echo $template->render(array('tasks' => $tasks, 'groups' => $groups, 'attachs'=>$attachs, 'rightData' => $rightData, 'prod'=>$production));
}else{

	if(isset($_POST['full'])){
		$template = $twig->loadTemplate('full.html.twig');
		echo $template->render(array('rightData' => $rightData));
		return false;
	}

	if(isset($_POST['attachs'])){
		$template = $twig->loadTemplate('attachs.html.twig');
		echo $template->render(array('attachData' => $attachData));
		return false;
	}

	if(isset($_POST['tasks'])){
		$template = $twig->loadTemplate('tasks.html.twig');
		echo $template->render(array('tasksData' => $tasksData));
		return false;
	}

	if(isset($_POST['groups'])){
		$template = $twig->loadTemplate('groups.html.twig');
		echo $template->render(array('groupsData' => $groupsData));
		return false;
	}

	if(isset($_POST['groupstasks'])){
		$template = $twig->loadTemplate('groupstasks.html.twig');
		echo $template->render(array('groupsTasksData' => $groupsTasksData));
		return false;
	}

	if(isset($_POST['tasksattach'])){
		$template = $twig->loadTemplate('tasksattach.html.twig');
		echo $template->render(array('tasksAttachData' => $tasksAttachData));
		return false;
	}

//	Kint::dump($_POST);
//	die();

	if(isset($_POST['groupId'])) {
		if(intval($_POST['edit_group']) == 0) {
			if ($stmt = $mysqli->prepare("INSERT INTO groups (uuid, gid, name) VALUES (?, ?, ?);")) {
				$stmt->bind_param('sss', $uuid, $gid, $name);
				$uuid = $php_utils->generate_uuid();
				$gid = $_POST['groupId'];
				$name = $_POST['groupName'];
				$stmt->execute();
				$stmt->close();
			} else {
				die("Error: " . $mysqli->error);
			}
		}else{
			if ($stmt = $mysqli->prepare("UPDATE groups SET gid = ?, name = ? WHERE id = ?;")) {
				$stmt->bind_param('ssi', $gid, $name, $id);
				$gid = $_POST['groupId'];
				$name = $_POST['groupName'];
				$id = $_POST['edit_group'];
				$stmt->execute();
				$stmt->close();
			} else {
				die("Error: " . $mysqli->error);
			}

			if(!$mysqli->query("DELETE FROM groups_tasks WHERE group_id = $id;")){
				die("Error: " . $mysqli->error);
			}

		}

		if ($stmt = $mysqli->prepare("INSERT INTO groups_tasks (group_id, group_uuid, task_id, task_uuid, seq) 
									(SELECT 
										(SELECT id FROM groups WHERE gid = ?) AS g_id,
										(SELECT uuid FROM groups WHERE gid = ?) AS g_uuid,
										(SELECT id FROM tasks WHERE tid = ?) AS t_id, 
										(SELECT uuid FROM tasks WHERE tid = ?) AS t_uuid, 
										? )
									;")) {
			$stmt->bind_param('ssssi', $gid, $gid, $tid, $tid, $seq);

			$x = 1;
			foreach ($_POST['groupTasks'] as $task) {
				$gid = $_POST['groupId'];
				$tid = $task;
				$seq = $x;
				$stmt->execute();
				$x++;
			}
			$stmt->close();
		} else {
			die("Error: " . $mysqli->error);
		}

	}

	if(isset($_POST['taskId'])){
		if(intval($_POST['edit_task']) == 0){
			if ($stmt = $mysqli->prepare("INSERT INTO tasks (uuid, tid, name) VALUES (?, ?, ?);")) {
				$stmt->bind_param('sss', $uuid, $tid, $name);
				$uuid = $php_utils->generate_uuid();
				$tid = $_POST['taskId'];
				$name = $_POST['taskName'];
				$stmt->execute();
				$stmt->close();
			} else {
				die("Error: " . $mysqli->error);
			}
		}else {
			if ($stmt = $mysqli->prepare("UPDATE tasks SET tid = ?, name = ? WHERE id = ?;")) {
				$stmt->bind_param('ssi', $tid, $name, $id);
				$tid = $_POST['taskId'];
				$name = $_POST['taskName'];
				$id = $_POST['edit_task'];
				$stmt->execute();
				$stmt->close();
			} else {
				die("Error: " . $mysqli->error);
			}

			if(!$mysqli->query("DELETE FROM tasks_attach WHERE task_id = $id;")){
				die("Error: " . $mysqli->error);
			}
		}

		if ($stmt = $mysqli->prepare("INSERT INTO tasks_attach (task_id, task_uuid, attach_id, attach_uuid, seq) 
									(SELECT 
										(SELECT id FROM tasks WHERE tid = ?) AS t_id,
										(SELECT uuid FROM tasks WHERE tid = ?) AS t_uuid,
										(SELECT id FROM attach WHERE aid = ?) AS a_id, 
										(SELECT uuid FROM attach WHERE aid = ?) AS a_uuid, 
										? )
									;")) {
			$stmt->bind_param('ssssi', $tid, $tid, $aid, $aid, $seq);

			$x = 1;
			foreach ($_POST['tasksAttach'] as $attach) {
				$tid = $_POST['taskId'];
				$aid = $attach;
				$seq = $x;
				$stmt->execute();
				$x++;
			}
			$stmt->close();
		} else {
			die("Error: " . $mysqli->error);
		}
	}

	if(isset($_POST['attachId'])){
		if(intval($_POST['edit_attach']) == 0){
			if($stmt = $mysqli->prepare("INSERT INTO attach (uuid, aid, name) VALUES (?, ?, ?);")){
				$stmt->bind_param('sss', $uuid, $aid, $name);
				$uuid = $php_utils->generate_uuid();
				$aid = $_POST['attachId'];
				$name = $_POST['attachName'];
				$stmt->execute();
				$stmt->close();
			}else{
				die("Error: " . $mysqli->error);
			}
		}else{
			if ($stmt = $mysqli->prepare("UPDATE attach SET aid = ?, name = ? WHERE id = ?;")) {
				$stmt->bind_param('ssi', $aid, $name, $edit_id);
				$aid = $_POST['attachId'];
				$name = $_POST['attachName'];
				$edit_id = $_POST['edit_attach'];
				$stmt->execute();
				$stmt->close();
			} else {
				die("Error: " . $mysqli->error);
			}
		}
	}

	if(isset($_POST['delGroup']) and $_POST['delGroup'] != ''){
		if ($stmt = $mysqli->prepare("DELETE FROM groups_tasks WHERE group_id = ?;")) {
			$stmt->bind_param('i', $id);
			$id = $_POST['delGroup'];
			$stmt->execute();
			$stmt->close();
		} else {
			die("Error: " . $mysqli->error);
		}

		if ($stmt = $mysqli->prepare("DELETE FROM groups WHERE id = ?;")) {
			$stmt->bind_param('i', $id);
			$id = $_POST['delGroup'];
			$stmt->execute();
			$stmt->close();
		} else {
			die("Error: " . $mysqli->error);
		}
	}

	if(isset($_POST['delTask']) and $_POST['delTask'] != ''){
		if ($stmt = $mysqli->prepare("DELETE FROM groups_tasks WHERE task_id = ?;")) {
			$stmt->bind_param('i', $id);
			$id = $_POST['delTask'];
			$stmt->execute();
			$stmt->close();
		} else {
			die("Error: " . $mysqli->error);
		}

		if ($stmt = $mysqli->prepare("DELETE FROM tasks_attach WHERE task_id = ?;")) {
			$stmt->bind_param('i', $id);
			$id = $_POST['delTask'];
			$stmt->execute();
			$stmt->close();
		} else {
			die("Error: " . $mysqli->error);
		}

		if ($stmt = $mysqli->prepare("DELETE FROM tasks WHERE id = ?;")) {
			$stmt->bind_param('i', $id);
			$id = $_POST['delTask'];
			$stmt->execute();
			$stmt->close();
		} else {
			die("Error: " . $mysqli->error);
		}
	}

	if(isset($_POST['delAttach']) and $_POST['delAttach'] != ''){
		if ($stmt = $mysqli->prepare("DELETE FROM tasks_attach WHERE attach_id = ?;")) {
			$stmt->bind_param('i', $id);
			$id = $_POST['delAttach'];
			$stmt->execute();
			$stmt->close();
		} else {
			die("Error: " . $mysqli->error);
		}

		if ($stmt = $mysqli->prepare("DELETE FROM attach WHERE id = ?;")) {
			$stmt->bind_param('i', $id);
			$id = $_POST['delAttach'];
			$stmt->execute();
			$stmt->close();
		} else {
			die("Error: " . $mysqli->error);
		}
	}


	$mysqli->ping();
	$tasks = getTasks();
	$groups = getGroups();
	$attachs = getAttachs();
	$rightData = getRightData();
	$_POST = null;
	$template = $twig->loadTemplate('index.html.twig');
	echo $template->render(array('tasks' => $tasks, 'groups' => $groups, 'attachs'=>$attachs, 'rightData' => $rightData));

}



function getRightData(){
	global $mysqli;
	$rightData = array();
	$sql = "SELECT g.id, g.gid, g.name FROM groups as g ORDER BY g.gid;";
	$resultG = $mysqli->query($sql);
	while ($row = $resultG->fetch_assoc()){
		$sql = "SELECT t.id, t.tid, t.name
			FROM tasks AS t 
			LEFT JOIN groups_tasks AS gt ON gt.task_id = t.id
			WHERE gt.group_id = '".$row['id']."'
			ORDER BY gt.seq;";
		$gtasks = array();
		$resultT = $mysqli->query($sql);
		while ($rowT = $resultT->fetch_assoc()){
			$sql = "SELECT a.id, a.aid, a.name
				FROM attach AS a
				LEFT JOIN tasks_attach AS ta ON ta.attach_id = a.id
				WHERE ta.task_id = '".$rowT['id']."';";
			$gtattach = array();
			$resultA = $mysqli->query($sql);
			while ($rowA = $resultA->fetch_assoc()){
				$gtattach[$rowA['id']] = array('id'=>$rowA['id'], 'aid'=>$rowA['aid'], 'fname'=>$rowA['name']);
			}

			$gtasks[$rowT['id']] = array('id'=>$rowT['id'], 'tid'=>$rowT['tid'], 'tname'=>$rowT['name'], 'attach'=>$gtattach);
		}
		$rightData[$row['id']] = array('id'=>$row['id'], 'gid'=>$row['gid'], 'gname'=>$row['name'],'tasks'=>$gtasks);
	}
	return $rightData;
}

function getGroups(){
	global $mysqli;
	$sql = "SELECT g.id, g.uuid, g.gid, g.name FROM groups AS g ORDER BY g.gid;";
	$result = $mysqli->query($sql);
	$groups = array();
	while ($row = $result->fetch_assoc()){
		array_push($groups, $row);
	}
	return $groups;
}

function getTasks(){
	global $mysqli;
	$sql = "SELECT t.id, t.uuid, t.tid, t.name FROM tasks AS t ORDER BY t.tid;";
	$result = $mysqli->query($sql);
	$tasks = array();
	while ($row = $result->fetch_assoc()){
		array_push($tasks, $row);
	}
	return $tasks;
}

function getAttachs(){
	global $mysqli;
	$sql = "SELECT a.id, a.uuid, a.aid, a.name FROM attach AS a ORDER BY a.aid;";
	$result = $mysqli->query($sql);
	$attachs = array();
	while ($row = $result->fetch_assoc()){
		array_push($attachs, $row);
	}
	return $attachs;
}

function getTasksData(){
	global $mysqli;

	$sql = "SELECT t.id, t.tid, t.name, t.uuid
		FROM tasks AS t 
		LEFT JOIN groups_tasks AS gt ON gt.task_id = t.id
		ORDER BY t.tid;";
	$gtasks = array();
	$resultT = $mysqli->query($sql);
	while ($rowT = $resultT->fetch_assoc()){
		$sql = "SELECT a.id, a.aid, a.name, a.uuid
			FROM attach AS a
			LEFT JOIN tasks_attach AS ta ON ta.attach_id = a.id
			WHERE ta.task_id = '".$rowT['id']."';";
		$gtattach = array();
		$resultA = $mysqli->query($sql);
		while ($rowA = $resultA->fetch_assoc()){
			$gtattach[$rowA['id']] = array('id'=>$rowA['id'], 'uuid'=>$rowA['uuid'], 'aid'=>$rowA['aid'], 'fname'=>$rowA['name']);
		}

		$gtasks[$rowT['id']] = array('id'=>$rowT['id'], 'uuid'=>$rowT['uuid'], 'tid'=>$rowT['tid'], 'tname'=>$rowT['name'], 'attach'=>$gtattach);
	}

	return $gtasks;

}

function getTasksAttachData(){
	global $mysqli;

	$sql = "SELECT ta.task_id, ta.attach_id, ta.task_uuid, ta.attach_uuid, seq
		FROM tasks_attach AS ta
		ORDER BY ta.task_id;";
	$tasksattach = array();
	$result = $mysqli->query($sql);
	while ($row = $result->fetch_assoc()){

//		$tasksattach[$row['task_uuid']] = $row['attach_uuid'];
		array_push($tasksattach, array('task_id'=>$row['task_id'], 'task_uuid'=>$row['task_uuid'], 'attach_id'=>$row['attach_id'], 'attach_uuid'=>$row['attach_uuid'], 'seq'=>$row['seq'] ));
	}

	return $tasksattach;
}

function getGroupsData(){
	global $mysqli;

	$sql = "SELECT g.id, g.gid, g.name, g.uuid
		FROM groups AS g 
		ORDER BY g.gid;";
	$groups = array();
	$result = $mysqli->query($sql);
	while ($row = $result->fetch_assoc()){
		$groups[$row['id']] = array('id'=>$row['id'], 'uuid'=>$row['uuid'], 'gid'=>$row['gid'], 'gname'=>$row['name']);
	}

	return $groups;

}

function getAttachsData(){
	global $mysqli;

	$sql = "SELECT a.id, a.aid, a.name, a.uuid
		FROM attach AS a 
		ORDER BY a.aid;";
	$attach = array();
	$result = $mysqli->query($sql);
	while ($row = $result->fetch_assoc()){
		$attach[$row['id']] = array('id'=>$row['id'], 'uuid'=>$row['uuid'], 'aid'=>$row['aid'], 'aname'=>$row['name']);
	}

	return $attach;

}

function getGroupsTasksData(){
	global $mysqli;

	$sql = "SELECT gt.group_id, gt.group_uuid, gt.task_id, gt.task_uuid, gt.seq
		FROM groups_tasks AS gt 
		ORDER BY gt.group_id, gt.seq;";
	$groupstasks = array();
	$result = $mysqli->query($sql);
	while ($row = $result->fetch_assoc()){
		array_push($groupstasks,array('group_id'=>$row['group_id'], 'group_uuid'=>$row['group_uuid'], 'task_id'=>$row['task_id'], 'task_uuid'=>$row['task_uuid'], 'seq'=>$row['seq']));
	}

	return $groupstasks;

}

?>


