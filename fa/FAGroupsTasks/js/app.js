/**
 * Created by Justin on 6/1/2017.
 */

$(document).ready(function() {

	var max_fields      = 21; //maximum input boxes allowed
	var group_wrapper         = $(".group_input_fields_wrap"); //Fields wrapper
	var group_add_button      = $(".group_add_field_button"); //Add button ID
	var task_wrapper         = $(".task_input_fields_wrap"); //Fields wrapper
	var task_add_button      = $(".task_add_field_button"); //Add button ID

	var gx = 1; //initlal text box count
	$(group_add_button).click(function(e){ //on add input button click
		e.preventDefault();
		if(gx < max_fields){ //max input box allowed
			gx++; //text box increment
			$(group_wrapper).append('<div class="groupTasksDiv"><label class="lblt" for="groupTasks">Task <span class="taskNums">1</span> </label><input class="inputShortTask groupTasksClass" readonly type="text" name="groupTasks[]"/><input class="inputShortTask" readonly type="text" name="groupTasksName[]"/><a href="#" class="group_remove_field">Remove</a></div>'); //add input box
		}
	});

	$(group_wrapper).on("click",".group_remove_field", function(e){ //user click on remove text
		e.preventDefault(); $(this).parent('div').remove();
		gx--;
		setTaskNums();
	});

	var tx = 1; //initlal text box count
	$(task_add_button).click(function(e){ //on add input button click
		e.preventDefault();
		if(tx < max_fields){ //max input box allowed
			tx++; //text box increment
			$(task_wrapper).append('<div class="taskAttachDiv"><label class="lblt" for="tasksAttach">Attach <span class="attachNums">1</span> </label><input class="inputShortAttach tasksAttachClass" readonly type="text" name="tasksAttach[]"/><input class="inputShortAttach" readonly type="text" name="tasksAttachName[]"/><a href="#" class="task_remove_field">Remove</a></div>'); //add input box
		}
	});

	$(task_wrapper).on("click",".task_remove_field", function(e){ //user click on remove text
		e.preventDefault(); $(this).parent('div').remove();
		tx--;
		setTaskNums();
	});

	$("#groupId").change(function () {
		if($("#groupBtn").text() !== 'Edit Group') {
			$(".group_tid").each(function () {
				if ($("#groupId").val().toLowerCase() === $(this).text().toLowerCase()) {
					showError("Group ID already exists!");
				}
			});
		}
	});

	$("#groupName").change(function () {
		if($("#groupBtn").text() !== 'Edit Group') {
			$(".group_name").each(function () {
				if ($("#groupName").val().toLowerCase() === $(this).text().toLowerCase()) {
					showError("Group Name already exists!");
				}
			});
		}
	});

	$("#taskId").change(function () {
		if($("#taskBtn").text() !== 'Edit Task') {
			$(".task_tid").each(function () {
				if ($("#taskId").val().toLowerCase() === $(this).text().toLowerCase()) {
					showError("Task ID already exists!");
				}
			});
		}
	});

	$("#taskName").change(function () {
		if($("#taskBtn").text() !== 'Edit Task') {
			$(".task_name").each(function () {
				if ($("#taskName").val().toLowerCase() === $(this).text().toLowerCase()) {
					showError("Task Name already exists!");
				}
			});
		}
	});

});

function setTaskNums() {
	var x = 1;
	$(".taskNums").each(function () {
		$(this).text(x);
		x++;
	})
}

function setAttachNums() {
	var x = 1;
	$(".attachNums").each(function () {
		$(this).text(x);
		x++;
	})
}

function showError(txt) {
	var ec = $("#errorContainer").clone().appendTo("#errorOuter");
	$(ec).show();
	$(ec).find("#errorTxt").text(txt);
	$(ec).fadeOut(10000)
}

function groupSubmit() {
	console.log("Group Submit");

	if($("#groupId").val() === "" || $("#groupName").val() === ""){
		showError("Fill in Group ID and Name");
		return false;
	}
	if($("#groupBtn").text() !== 'Edit Group') {
		$(".group_name").each(function () {
			if ($("#groupName").val().toLowerCase() === $(this).text().toLowerCase()) {
				showError("Group Name already exists!");
				return false;
			}
		});

		$(".group_tid").each(function () {
			if ($("#groupId").val().toLowerCase() === $(this).text().toLowerCase()) {
				showError("Group ID already exists!");
				return false;
			}
		});
	}
	if($(".groupTasksClass").length === 0){
		showError("Group must have tasks assigned!");
		return false;
	}

	$(".groupTasksClass").each(function () {
		if($(this).val() === ""){
			showError("Blank group task field found!");
			return false;
		}
	});
	$("#newGroup").submit();
}

function taskSubmit() {
	console.log("Task Sub");

	if($("#taskId").val() === "" || $("#taskName").val() === ""){
		showError("Fill in Task ID and Name");
		return false;
	}
	if($("#taskBtn").text() !== 'Edit Task') {
		$(".task_tid").each(function () {
			if ($("#taskId").val().toLowerCase() === $(this).text().toLowerCase()) {
				showError("Task ID already exists!");
				return false;
			}
		});

		$(".task_name").each(function () {
			if ($("#taskName").val().toLowerCase() === $(this).text().toLowerCase()) {
				showError("Task Name already exists!");
				return false;
			}
		});
	}
	$("#newTask").submit();
}

function addTask(val) {
	$(".group_add_field_button").click();
	gtc = $(".groupTasksClass").last();
	gtc.val(val);
	$(".task_tid").each(function () {
		if($(this).text() === val){
			gtc.next().val($(this).next().text());
		}
	});
	setTaskNums();

}

function attachSubmit() {
	console.log("Attach Sub");

	if($("#atTaskId").val() === "" || $("#formId").val() === "" || $("#formName").val() === ""){
		showError("Fill in Task ID and Form Name and Form ID");
		return false;
	}

	$("#newAttach").submit();
}

function editTask(id) {
	console.log(id);
	var attach = null;
	$.ajax({
		method: "GET",
		url: 'getTasksAttach.php',
		data: {id: id},
		async: false,
		cache: false
	}).done(function(rtn) {
		console.log(rtn);
		attach = $.parseJSON(rtn);
	});

	if(typeof attach === 'object'){
		console.log(attach);

		$(".taskAttachDiv").remove();

		Object.keys(attach).forEach(function(key) {

			console.log(key, attach[key]);
			$(".task_add_field_button").click();
			tac = $(".tasksAttachClass").last();
			tac.val(attach[key]);

			$(".attach_aid").each(function () {
				if($(this).text() === attach[key]){
					tac.next().val($(this).next().text());
				}
			});
		});
		setAttachNums();
	}

	$(".task_id").each(function () {
		if($(this).text() === id){
			$("#taskId").val($(this).next().text());
			$("#taskName").val($(this).next().next().text());
			$("#taskBtn").text('Edit Task');
			$("#edit_task").val(id);
		}
	});




}

function editGroup(id) {
	console.log(id);
	var tasks = null;
	$.ajax({
		method: "GET",
		url: 'getGroupTasks.php',
		data: {id: id},
		async: false,
		cache: false
	}).done(function(rtn) {
		console.log(rtn);
		tasks = $.parseJSON(rtn);
	});

	if(typeof tasks === 'object'){
		console.log(tasks);

		$(".groupTasksDiv").remove();

		Object.keys(tasks).forEach(function(key) {

			console.log(key, tasks[key]);
			$(".group_add_field_button").click();
			gtc = $(".groupTasksClass").last();
			gtc.val(tasks[key]);

			$(".task_tid").each(function () {
				if($(this).text() === tasks[key]){
					gtc.next().val($(this).next().text());
				}
			});
		});
		setTaskNums();
	}
	
	$(".group_id").each(function () {
		if($(this).text() === id){
			$("#groupId").val($(this).next().text());
			$("#groupName").val($(this).next().next().text());
			$("#groupBtn").text('Edit Group');
			$("#edit_group").val(id);
		}
	});
}

function addAttach(val) {
	$(".task_add_field_button").click();
	gtc = $(".tasksAttachClass").last();
	gtc.val(val);
	$(".attach_aid").each(function () {
		if($(this).text() === val){
			gtc.next().val($(this).next().text());
		}
	});
	setAttachNums();
}

function editAttach(id) {
	$(".attach_id").each(function () {
		if($(this).text() === id){
			$("#attachId").val($(this).next().text());
			$("#attachName").val($(this).next().next().text());
			$("#attachBtn").text('Edit Attach');
			$("#edit_attach").val(id);
		}
	});
}

function groupClear() {
	$("#groupName").val('');
	$("#groupId").val('');
	$(".groupTasksDiv").remove();
}

function taskClear() {
	$("#taskName").val('');
	$("#taskId").val('');
	$(".taskAttachDiv").remove();
}

function attachClear() {
	$("#attachName").val('');
	$("#attachId").val('');
}

function delGroup(id) {
	console.log(id);
	$("#delGroup").val(id);
	$("#delForm").submit();
}

function delTask(id) {
	console.log(id);
	$("#delTask").val(id);
	$("#delForm").submit();
}

function delAttach(id) {
	console.log(id);
	$("#delAttach").val(id);
	$("#delForm").submit();
}

function showFullSubmit() {
	$("#full").attr('checked', true);
	$("#showForm").submit();
}

function showAttachSubmit() {
	$("#attachs").attr('checked', true);
	$("#showForm").submit();
}

function showTasksSubmit() {
	$("#tasks").attr('checked', true);
	$("#showForm").submit();
}

function showGroupsSubmit() {
	$("#groups").attr('checked', true);
	$("#showForm").submit();
}

function showGroupsTasksSubmit() {
	$("#groupstasks").attr('checked', true);
	$("#showForm").submit();
}

function showTasksAttachSubmit() {
	$("#tasksattach").attr('checked', true);
	$("#showForm").submit();
}

function csvTasks() {
	window.open('csvTasks.php');
}

function csvAttach() {
	window.open('csvAttach.php');
}

function csvGroups() {
	window.open('csvGroups.php');
}

function csvGroupsTasks() {
	window.open('csvGroupsTasks.php');
}

function csvTasksAttach() {
	window.open('csvTasksAttach.php');
}