<?php

die("Use faApi project");

$debug = true;
$getExisting = false;


$apiKey = 'd89e20a05079484d91c4168b5c800bfc';
$uid = '450767f1330f4f1b9349f69741d8fc2d';
$base = "https://api.fieldaware.net/";

$callcnt = 0;


$ch = curl_init();
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	'Authorization: Token '.$apiKey,
	'Accept-Type: application/json'
));

if($getExisting){
	$users = getAllData($ch, 'user/');
	$locations = getAllData($ch, 'location/');
	$contacts = getAllData($ch, 'contact/');
	$tasks = getAllData($ch, 'task/');
	$items = getAllData($ch, 'item/');
	$assets = getAllData($ch, 'asset/');
	$jobs = getAllData($ch, 'job/', '?start=2017-09-04T16%3A00&end=2017-09-10T16%3A00');

	file_put_contents(__DIR__ . '\users.txt', print_r($users, true));
	file_put_contents(__DIR__ . '\locations.txt', print_r($locations, true));
	file_put_contents(__DIR__ . '\contacts.txt', print_r($contacts, true));
	file_put_contents(__DIR__ . '\tasks.txt', print_r($tasks, true));
	file_put_contents(__DIR__ . '\items.txt', print_r($items, true));
	file_put_contents(__DIR__ . '\assets.txt', print_r($assets, true));
	file_put_contents(__DIR__ . '\jobs.txt', print_r($jobs, true));
}


echo PHP_EOL,"Call Count = ",$callcnt,PHP_EOL;

curl_close($ch);


testPost();



exit(0);

function getAllData($ch, $url, $args = "?page=", $slice = -1, $showData = false){
	global $callcnt, $base, $apiKey;

	echo "Getting ".$url."...",PHP_EOL;
	$page = 0;
	$pages = 0;
	$arr = array();
	if($args != "?page="){
		$args.="&page=";
	}
	while(true){
		$u = $base.$url.$args.$page;
		curl_setopt($ch, CURLOPT_URL,$u);
		$callcnt++;
		if($ret = curl_exec($ch)){
			$ret = json_decode($ret, true);
		}
//		echo print_r($ret),PHP_EOL;
		if(curl_getinfo($ch, CURLINFO_HTTP_CODE) != 200){
			echo PHP_EOL."ERROR:".PHP_EOL;
			echo print_r(curl_getinfo($ch));
			echo PHP_EOL;
			die();
		}
		foreach($ret['items'] as $item){
			array_push($arr, $item);
		}
		echo "...retrieved page ".$ret['page'],PHP_EOL;
		$pages = ceil($ret['count']/$ret['pageSize']);
		if($page == $pages-1){
			break;
		}
		$page++;
	}
	if($showData){
		d($arr, $slice);
	}

	return $arr;
}

function getDetailData($ch, $url, $slice = -1, $showData = false){
	global $callcnt, $base, $apiKey;

	echo "Getting ".$url."...",PHP_EOL;
	$u = $base.$url;
	curl_setopt($ch, CURLOPT_URL,$u);
	$callcnt++;
	if($ret = curl_exec($ch)){
		$ret = json_decode($ret, true);
	}
	if(curl_getinfo($ch, CURLINFO_HTTP_CODE) != 200){
		echo PHP_EOL."ERROR:".PHP_EOL;
		echo print_r(curl_getinfo($ch));
		echo PHP_EOL;
		die();
	}
	if($showData){
		d($ret, $slice);
	}

	return $ret;
}


function d($obj, $slice = -1){
	global $debug;
	if($debug){
		echo print_r(array_slice($obj,0,$slice));
	}
}

function testPost(){
	global $callcnt, $base, $apiKey;

	$params = [
		'scheduledOn' => '2017-09-05T12:00:00',
		'startedOn' => '2017-09-05T13:00:00',
		'completedOn' => '2017-09-05T14:00:00',
		'description' => 'THIS IS POST FROM API',
		'location' => [
			'uuid' => 'a3d46a163263489f910c83a735560ccd'
		],
		'jobLead' => [
			'uuid' => 'ebce6104f7da42c2863c1517b6a1323c'
		],
		'asset' => [
			'uuid' => '99103786d4b941098b735eac125060da'
		],
		'contact' => [
			'uuid' => '75b177f5ff1a4ada95ae661546ee2ea7'
		],
		"state" => [
			"active"
		],
		"estDuration" => 120,
		"tasks" => [
			[
				"task" => [
					"uuid" => "af09dedbacb34af2a7a4f1ca82f8efc7"
				],
				"note" => "Task 1 from api",
				"done" => true
			],
			[
				"task" => [
					"uuid" => "af09dedbacb34af2a7a4f1ca82f8efc7"
				],
				"note" => "Task 2 from api",
				"done" => true
			],
		],
		"labor" => [
			[
				"user" => [
					"uuid" => "ebce6104f7da42c2863c1517b6a1323c"
				],
				"quantity" => 60,
				"rate" => [
					"name" => "Labor Regular",
					"rate" => 0
				]
			],
			[
				"user" => [
					"uuid" => "ebce6104f7da42c2863c1517b6a1323c"
				],
				"quantity" => 72,
				"rate" => [
					"name" => "Labor Overtime",
					"rate" => 0
				]
			]
		]
	];


//	$params = [
//		"tasks" => [
//			[
//				"task" => [
//					"uuid" => "af09dedbacb34af2a7a4f1ca82f8efc7"
//				],
//				"note" => "Task 1 from api",
//				"done" => true
//			],
//			[
//				"task" => [
//					"uuid" => "af09dedbacb34af2a7a4f1ca82f8efc7"
//				],
//				"note" => "Task 2 from api",
//				"done" => true
//			],
//		]
//	];

	/*
	 * Labor Regular = 9aca8b21a4b34da09ca485b71806f505
	 * Labor Overtime = f06cd73b17da4c6485e359d0b461d95f
	 * Travel Regular = be8d9532e7034b96bbebfb88812ed433
	 * Travel Overtime = 922c96c174f64b869fa892f7b67ee381
	 */


//	$params = [
//		"labor" => [
//			[
//				"user" => [
//					"uuid" => "ebce6104f7da42c2863c1517b6a1323c"
//				],
//				"quantity" => 60,
//				"rate" => [
//					"uuid" => "9aca8b21a4b34da09ca485b71806f505"
//
//				]
//			],
//			[
//				"user" => [
//					"uuid" => "ebce6104f7da42c2863c1517b6a1323c"
//				],
//				"quantity" => 60,
//				"rate" => [
//					"uuid" => "be8d9532e7034b96bbebfb88812ed433"
//
//				]
//			]
//		]
//	];

	$params = [
		"completedOn" => "2017-09-06T20:00:00"
	];

	echo print_r(json_encode($params)),PHP_EOL,PHP_EOL;


	$ch = curl_init();
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'Authorization: Token '.$apiKey,
		'Content-Type: application/json',
		'Content-Length: ' . strlen(json_encode($params))
	));
	$u = $base.'job/4cd1ab15ce3a427d8b575cf41239cf65';
	curl_setopt($ch, CURLOPT_URL,$u);
//curl_setopt($ch, CURLOPT_POST, json_encode($params));
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
	echo print_r(json_decode(curl_exec($ch), true)),PHP_EOL,PHP_EOL;
	echo print_r(curl_getinfo($ch));


}

