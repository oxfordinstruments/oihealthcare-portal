<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 6/19/2017
 * Time: 3:25 PM
 */
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
ini_set('display_errors', 'On');

$debug = false;
$makeFile = true;

$docroot = $_SERVER['DOCUMENT_ROOT'];
define('EOL','<br />');
define('FEOL',"\n");
define('FMT', 'Y-m-d H:i:s');

if(isset($_GET['debug'])){
	$debug = true;
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($docroot.'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

require_once($docroot.'/resources/moment_php/Moment.php');
require_once($docroot.'/resources/moment_php/MomentException.php');
require_once($docroot.'/resources/moment_php/MomentHelper.php');
require_once($docroot.'/resources/moment_php/MomentLocale.php');
require_once($docroot.'/resources/moment_php/MomentFromVo.php');

require_once($docroot.'/report/common/scripts/php_utils.php');
$php_utils =  new phpUtils();

$php_utils->message('BEGIN');

$php_utils->message('Getting reports and calculating data...');

$sql = "SELECT
sh.report_id, sh.date, sh.reg_labor, sh.reg_travel, sh.ot_labor, sh.ot_travel, sh.unique_id, u.firstname, u.lastname,
sr.invoice_required, sr.valuation_do_not_invoice, sr.invoice_expense, sr.invoice_shipping, 
sr.invoice_labor_ot, sr.invoice_labor_ot_rate, sr.invoice_labor_reg, sr.invoice_labor_reg_rate,  
sr.invoice_travel_ot, sr.invoice_travel_ot_rate, sr.invoice_travel_reg, sr.invoice_travel_reg_rate,
u.name AS CreatedBy,
DATE_FORMAT(convert_tz(str_to_date(sr.finalize_date, '%Y-%m-%dT%H:%i:%sZ'),'utc', 'America/New_York'),'%Y-%m-%d %H:%i:%s')  AS `CreatedDate`,
sr.system_id, sr.system_unique_id, sr.system_ver_unique_id
FROM systems_hours AS sh
LEFT JOIN systems_reports AS sr ON sr.unique_id = sh.unique_id
LEFT JOIN users AS u ON u.uid = sr.engineer
WHERE sr.status = 'closed' and sr.invoice_required = 'y' and sr.valuation_do_not_invoice = 'n'";
if($debug){
	$sql.=" LIMIT 10";
}
$sql.=";";

$result = $mysqli->query($sql);
$data = array();
while($row = $result->fetch_assoc()){
//	dd($row);
	$temp = array();
	if(floatval($row['invoice_labor_reg_rate']) != 0.0){
		$temp['chrghours'] = number_format(floatval($row['reg_labor']), 2, '.', '');
		$temp['fname'] = $row['firstname'];
		$temp['cost'] = number_format(floatval($row['invoice_labor_reg']), 2, '.', '');;
		$temp['lname'] = $row['lastname'];
		$temp['rate'] = number_format(floatval($row['invoice_labor_reg_rate']), 2, '.', '');
		$temp['CreatedBy'] = $row['CreatedBy'];
		$temp['CreatedDate'] = $row['CreatedDate'];
		$temp['OIHP_report_unique_id'] = $row['unique_id'];
		$temp['OIHP_report_id'] = $row['report_id'];
		$temp['OIHP_data_type'] = 'reg_labor';
		$temp['OIHP_system_id'] = $row['system_id'];
		$temp['OIHP_system_unique_id'] = $row['system_unique_id'];
		$temp['OIHP_system_ver_unique_id'] = $row['system_ver_unique_id'];
		array_push($data, $temp);
	}

	if(floatval($row['invoice_labor_ot_rate']) != 0.0){
		$temp['chrghours'] = number_format(floatval($row['ot_labor']), 2, '.', '');
		$temp['fname'] = $row['firstname'];
		$temp['cost'] = number_format(floatval($row['invoice_labor_ot']), 2, '.', '');;
		$temp['lname'] = $row['lastname'];
		$temp['rate'] = number_format(floatval($row['invoice_labor_ot_rate']), 2, '.', '');
		$temp['CreatedBy'] = $row['CreatedBy'];
		$temp['CreatedDate'] = $row['CreatedDate'];
		$temp['OIHP_report_unique_id'] = $row['unique_id'];
		$temp['OIHP_report_id'] = $row['report_id'];
		$temp['OIHP_data_type'] = 'ot_labor';
		$temp['OIHP_system_id'] = $row['system_id'];
		$temp['OIHP_system_unique_id'] = $row['system_unique_id'];
		$temp['OIHP_system_ver_unique_id'] = $row['system_ver_unique_id'];
		array_push($data, $temp);
	}

	if(floatval($row['invoice_travel_reg_rate']) != 0.0){
		$temp['chrghours'] = number_format(floatval($row['reg_travel']), 2, '.', '');
		$temp['fname'] = $row['firstname'];
		$temp['cost'] = number_format(floatval($row['invoice_travel_reg']), 2, '.', '');;
		$temp['lname'] = $row['lastname'];
		$temp['rate'] = number_format(floatval($row['invoice_travel_reg_rate']), 2, '.', '');
		$temp['CreatedBy'] = $row['CreatedBy'];
		$temp['CreatedDate'] = $row['CreatedDate'];
		$temp['OIHP_report_unique_id'] = $row['unique_id'];
		$temp['OIHP_report_id'] = $row['report_id'];
		$temp['OIHP_data_type'] = 'reg_travel';
		$temp['OIHP_system_id'] = $row['system_id'];
		$temp['OIHP_system_unique_id'] = $row['system_unique_id'];
		$temp['OIHP_system_ver_unique_id'] = $row['system_ver_unique_id'];
		array_push($data, $temp);
	}

	if(floatval($row['invoice_travel_ot_rate']) != 0.0){
		$temp['chrghours'] = number_format(floatval($row['ot_travel']), 2, '.', '');
		$temp['fname'] = $row['firstname'];
		$temp['cost'] = number_format(floatval($row['invoice_travel_ot']), 2, '.', '');;
		$temp['lname'] = $row['lastname'];
		$temp['rate'] = number_format(floatval($row['invoice_travel_ot_rate']), 2, '.', '');
		$temp['CreatedBy'] = $row['CreatedBy'];
		$temp['CreatedDate'] = $row['CreatedDate'];
		$temp['OIHP_report_unique_id'] = $row['unique_id'];
		$temp['OIHP_report_id'] = $row['report_id'];
		$temp['OIHP_data_type'] = 'ot_travel';
		$temp['OIHP_system_id'] = $row['system_id'];
		$temp['OIHP_system_unique_id'] = $row['system_unique_id'];
		$temp['OIHP_system_ver_unique_id'] = $row['system_ver_unique_id'];
		array_push($data, $temp);
	}
}

d($data);

$fname = 'fa_labor-' .(string)time(). '.csv';
$php_utils->message('Creating csv file '.$fname);

$f = null;
if($makeFile){
	$f = fopen($fname, 'w');
}

echo "<h1>FA Labor CSV</h1><hr>",EOL;
$columns = array(
	'Chargeable Hours',
	'First Name',
	'Labor Cost',
	'Last Name',
	'Rate',
	'CreatedBy',
	'CreatedDate',
	'OIHP_report_unique_id',
	'OIHP_report_id',
	'OIHP_data_type',
	'OIHP_system_id',
	'OIHP_system_unique_id',
	'OIHP_system_ver_unique_id'
);
echo implode(',', $columns),EOL;
if($makeFile){
	fwrite($f, implode(',', $columns) . FEOL);
}
$x = 0;
foreach($data as $row){
	if($x < 20){
		echo implode(',', $row),EOL;
	}
	if($makeFile){
		fwrite($f, implode(',', $row) . FEOL);
	}
	$x++;
}
if($makeFile){
	fclose($f);
}
echo "................... Download to see full output.",EOL;
echo "<hr>";
if($makeFile){
	echo "<a href='$fname'>Download CSV $fname</a>", EOL;
}else{
	echo "makeFile is false";
}
