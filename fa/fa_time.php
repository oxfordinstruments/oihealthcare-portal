<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 6/19/2017
 * Time: 3:25 PM
 */
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
ini_set('display_errors', 'On');
$debug = false;
$docroot = $_SERVER['DOCUMENT_ROOT'];
define('EOL','<br />');
define('FEOL',"\n");
define('FMT', 'Y-m-d H:i:s');

if(isset($_GET['debug'])){
	$debug = true;
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($docroot.'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

require_once($docroot.'/resources/moment_php/Moment.php');
require_once($docroot.'/resources/moment_php/MomentException.php');
require_once($docroot.'/resources/moment_php/MomentHelper.php');
require_once($docroot.'/resources/moment_php/MomentLocale.php');
require_once($docroot.'/resources/moment_php/MomentFromVo.php');

require_once($docroot.'/report/common/scripts/php_utils.php');
$php_utils =  new phpUtils();

$php_utils->message('BEGIN');

$php_utils->message('Getting reports and calculating data...');

$sql = "SELECT sh.report_id, sh.date, sh.reg_labor, sh.reg_travel, sh.ot_labor, sh.ot_travel, sh.unique_id, u.firstname, u.lastname,
		u.name AS CreatedBy,
		DATE_FORMAT(convert_tz(str_to_date(sr.finalize_date, '%Y-%m-%dT%H:%i:%sZ'),'utc', 'America/New_York'),'%Y-%m-%d %H:%i:%s')  AS `CreatedDate`,
		sr.system_id, sr.system_unique_id, sr.system_ver_unique_id
		FROM systems_hours AS sh
		LEFT JOIN systems_reports AS sr ON sr.unique_id = sh.unique_id
		LEFT JOIN users AS u ON u.uid = sr.engineer";
if($debug){
	$sql.=" LIMIT 10";
}
$sql.=";";

$result = $mysqli->query($sql);
$data = array();
while($row = $result->fetch_assoc()){
	$startTime = new Moment\Moment($row['date']);
	$startTime->addHours(8);
	$startTime->setImmutableMode(true);

	$startTime1 = $startTime->cloning();
	$startTime2 = $startTime->cloning();

	$temp['duration'] = intval($row['reg_labor']) + intval($row['ot_labor']);
	$temp['duration'] *= 60;
	$temp['endTime'] = $startTime1->addHours($temp['duration'])->format(FMT);
	$temp['firstname'] = $row['firstname'];
	$temp['joblead'] = 1;
	$temp['lastname'] = $row['lastname'];
	$temp['startTime'] = $startTime->format(FMT);
	$temp['Travel'] = 'Non-Transit';
	$temp['CreatedBy'] = $row['CreatedBy'];
	$temp['CreatedDate'] = $row['CreatedDate'];
	$temp['OIHP_report_unique_id'] = $row['unique_id'];
	$temp['OIHP_report_id'] = $row['report_id'];
	$temp['OIHP_system_id'] = $row['system_id'];
	$temp['OIHP_system_unique_id'] = $row['system_unique_id'];
	$temp['OIHP_system_ver_unique_id'] = $row['system_ver_unique_id'];

	array_push($data, $temp);

	$temp['duration'] = intval($row['reg_travel']) + intval($row['ot_travel']);
	$temp['duration'] *= 60;
	$temp['endTime'] = $startTime2->addHours($temp['duration'])->format(FMT);
	$temp['firstname'] = $row['firstname'];
	$temp['joblead'] = 1;
	$temp['lastname'] = $row['lastname'];
	$temp['startTime'] = $startTime->format(FMT);
	$temp['Travel'] = 'Transit';
	$temp['CreatedBy'] = $row['CreatedBy'];
	$temp['CreatedDate'] = $row['CreatedDate'];
	$temp['OIHP_report_unique_id'] = $row['unique_id'];
	$temp['OIHP_report_id'] = $row['report_id'];
	$temp['OIHP_system_id'] = $row['system_id'];
	$temp['OIHP_system_unique_id'] = $row['system_unique_id'];
	$temp['OIHP_system_ver_unique_id'] = $row['system_ver_unique_id'];

	array_push($data, $temp);
}

d($data);

$fname = 'fa_time-' .(string)time(). '.csv';
$php_utils->message('Creating csv file '.$fname);

$f = fopen($fname, 'w');

echo "<h1>FA Time CSV</h1><hr>",EOL;
$columns = array(
	'Duration',
	'EndTime',
	'First Name',
	'Is Job Lead',
	'Last Name',
	'StartTime',
	'Travel',
	'CreatedBy',
	'CreatedDate',
	'OIHP_report_unique_id',
	'OIHP_report_id',
	'OIHP_system_id',
	'OIHP_system_unique_id',
	'OIHP_system_ver_unique_id'
);
echo implode(',', $columns),EOL;
fwrite($f, implode(',', $columns).FEOL);
$x = 0;
foreach($data as $row){
	if($x < 20){
		echo implode(',', $row),EOL;
	}
	fwrite($f, implode(',', $row).FEOL);
	$x++;
}
fclose($f);
echo "................... Download to see full output.",EOL;
echo "<hr>";
echo "<a href='$fname'>Download CSV $fname</a>",EOL;
