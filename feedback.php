<?php
//Update Completed 12/12/14
session_name("OIREPORT");
session_start();

set_include_path($settings->php_include_path);
$settings = new SimpleXMLElement($_SERVER['DOCUMENT_ROOT'].'/settings.xml', null, true);
require_once($_SERVER['DOCUMENT_ROOT'].'/define_inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

process_si_contact_form();
?>

<!DOCTYPE html>
<html>
<head>
<style type="text/css">
<!--
.error {
	color: #f00;
	font-weight: bold;
	font-size: 1.2em;
}
.success {
	color: #00f;
	font-weight: bold;
	font-size: 1.2em;
}
fieldset {
	width: 90%;
}
legend {
	font-size: 24px;
}
.note {
	font-size: 18px;
}
-->
</style>
<title>Service Portal - Oxford Instruments</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link href="/resources/css/screen.min.css" rel="stylesheet" type="text/css" media="screen" />
<link href="/resources/css/320.min.css" rel="stylesheet" type="text/css" />
<link href="/resources/css/720.min.css" rel="stylesheet" type="text/css" />
<link href="/resources/css/960.min.css" rel="stylesheet" type="text/css" />
<link href="/resources/css/print.min.css" rel="stylesheet" type="text/css" media="print" />
<!--[if lt IE 9 ]>
        <link href="/resources/css/screen-ie.min.css" rel="stylesheet" type="text/css" />
    <![endif]-->
<!--[if IE 9 ]>
        <link href="/resources/css/screen-ie9.min.css" rel="stylesheet" media="screen" />
    <![endif]-->

<meta charset="UTF-8" />
<link href="/resources/images/favicon.ico" type="image/x-icon" rel="shortcut icon"/>
<link href="/resources/images/favicon.ico" type="image/x-icon" rel="icon"/>
<script src="/resources/js/jquery-1.7.2.min.js"></script>
<script src="/resources/js/jquery.badBrowser.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#contact_yes").click(function(){
		$("#contact_info_div").show('slow');
	});
	$("#contact_no").click(function(){
		document.getElementById("contact_info").value = "";
		$("#contact_info_div").hide('slow');
	});
	if(document.getElementById("contact_yes").checked){
		$("#contact_info_div").show();
	}
	
});
function highlight(){
	document.getElementById("code").focus();
	<?php if(isset($_SESSION['surveyform']['success']) && $_SESSION['surveyform']['success'] == true){ ?>
	$("#requestdiv").hide();
	<?php }	?>
	
	<?php if(isset($_GET['e'])){?>
	document.getElementById("code").value = '<?php echo $_GET['e']; ?>';
	<?php } ?>
}
function submitcheck(){
	document.forms["form"].submit();
}
</script>
</head>
<body onLoad="highlight()">
<?php require($_SERVER['DOCUMENT_ROOT'].'/report/head_foot/default_header.php'); ?>
<div class="outerContainer" id="content">
	<div style="width:100%; margin-top:20px; text-align:center;  margin-bottom:20px;">
		<h1 style="color:#F79447">Experience Survey</h1>
	</div>
	<div style="width:500px; margin-left:auto; margin-right:auto; color:#1B2673; padding-top:15px; padding-bottom:10px">
		<div style="text-align:center">
			<?php
			//process_si_contact_form(); // Process the form, if it was submitted		
			if (isset($_SESSION['surveyform']['error']) &&  $_SESSION['surveyform']['error'] == true): /* The last form submission had 1 or more errors */ ?>
			<span class="error">There was a problem with your submission.<br>Errors are displayed below in red.</span><br />
			<br />
			<?php elseif (isset($_SESSION['surveyform']['success']) && $_SESSION['surveyform']['success'] == true): /* form was processed successfully */ ?>
			<span class="success">Thank you, your answers have been submitted.<br>We appreciate the time you have taken to fill out our survey!<br></span><br />
			<br />
			<?php endif; ?>
		</div>
		<div id="requestdiv">
			<form id="form" name="form" method="post" action="<?php echo htmlspecialchars($_SERVER['REQUEST_URI']) ?>">
				<input type="hidden" name="do" value="contact" />
				<div style="text-align:center">
					<div style="text-align:left; width:75%; margin-left:auto; margin-right:auto"><strong>Survey Code (6-digit code)*:</strong>&nbsp; &nbsp;<?php echo @$_SESSION['surveyform']['code_error'] ?></div>
					<div style=" width:75%; margin-left:auto; margin-right:auto; margin-bottom:20px;">
						<input style="width:100%" type="text" id="code" name="code" value="<?php echo htmlspecialchars(@$_SESSION['surveyform']['code']) ?>" />
					</div>
					
					
					<div style="text-align:left; width:75%; margin-left:auto; margin-right:auto"><strong>Based on this service experience, how likely is it that you would recommend OiHeathcare to a friend or colleague given the opportunity?*</strong>&nbsp; &nbsp;<?php echo @$_SESSION['surveyform']['score_error'] ?></div>
					<div style=" width:75%; margin-left:auto; margin-right:auto; margin-bottom:20px;">
						<select style="width:100%" id="score" name="score">
							<option <?php if($_SESSION['surveyform']['score'] == '10'){echo "selected";} ?> value="10" style="color:#090">10 - Extremely Likely</option>
							<option <?php if($_SESSION['surveyform']['score'] == '9'){echo "selected";} ?> value="9" style="color:#090">9</option>
							<option <?php if($_SESSION['surveyform']['score'] == '8'){echo "selected";} ?> value="8" style="color:#03F">8</option>
							<option <?php if($_SESSION['surveyform']['score'] == '7'){echo "selected";} ?> value="7" style="color:#03F">7</option>
							<option <?php if($_SESSION['surveyform']['score'] == '6'){echo "selected";} ?> value="6" style="color:#C00">6</option>
							<option <?php if($_SESSION['surveyform']['score'] == '5'){echo "selected";} ?> value="5" style="color:#C00">5</option>
							<option <?php if($_SESSION['surveyform']['score'] == '4'){echo "selected";} ?> value="4" style="color:#C00">4</option>
							<option <?php if($_SESSION['surveyform']['score'] == '3'){echo "selected";} ?> value="3" style="color:#C00">3</option>
							<option <?php if($_SESSION['surveyform']['score'] == '2'){echo "selected";} ?> value="2" style="color:#C00">2</option>
							<option <?php if($_SESSION['surveyform']['score'] == '1'){echo "selected";} ?> value="1" style="color:#C00">1</option>
							<option <?php if($_SESSION['surveyform']['score'] == '0'){echo "selected";} ?> value="0" style="color:#C00">0 - Not Likely At All</option>
						</select>
					</div>

					
					<div style="text-align:left; width:75%; margin-left:auto; margin-right:auto"><strong>Comments</strong></div>
					<div style=" width:75%; margin-left:auto; margin-right:auto; margin-bottom:20px;">
						<textarea style="width:100%" type="text" id="comments" name="comments"><?php echo htmlspecialchars(@$_SESSION['surveyform']['comments'])?></textarea>
					</div>					
					
					
					<div style="text-align:left; width:75%; margin-left:auto; margin-right:auto"><strong>May we contact you to provide follow-up or to update on any corrective actions (if applicable)?</strong></div>
					<div style=" width:75%; margin-left:auto; margin-right:auto; margin-bottom:20px; margin-top:5px">
						<input name="contact" type="radio" value="Yes" id="contact_yes" <?php if($_SESSION['surveyform']['contact'] == 'Yes'){echo "checked";} ?>>&nbsp;Yes&nbsp;<input name="contact" type="radio" value="No" id="contact_no" <?php if($_SESSION['surveyform']['contact'] == 'No' || !isset($_SESSION['surveyform']['contact'])){echo "checked";} ?>>&nbsp;No
					</div>
					
					<div id="contact_info_div" style="display:none">
						<div style="text-align:left; width:75%; margin-left:auto; margin-right:auto"><strong>Your Contact Information (optional, if preferred over info already provided) </strong></div>
						<div style=" width:75%; margin-left:auto; margin-right:auto; margin-bottom:20px;">
							<textarea style="width:100%" type="text" id="contact_info" name="contact_info"><?php echo htmlspecialchars(@$_SESSION['surveyform']['contact_info'])?></textarea>
						</div>
					</div>
					
					
					<p> <br />
						<input name="Submit" id="submitbtn" type="button" value="Submit" class="button" onclick="submitcheck();" />
					</p>
				</div>
			</form>
			</fieldset>
		</div>
	</div>
	<div style="width:100%; margin-top:20px; margin-bottom:60px">
		<div style="margin-left:auto; margin-right:auto; width:50%; text-align:left;"> This system should be accessed by authorized <?php echo $settings->company_name; ?> personnel and customers only. Unauthorized access or use of this computer system may subject violators to criminal, civil, and/or administrative action. </div>
	</div>
</div>
<?php require($_SERVER['DOCUMENT_ROOT'].'/report/head_foot/default_footer.php'); ?>
<?php
// The form processor PHP code
function process_si_contact_form()
{
	global $log;
	global $settings;
	
	$_SESSION['surveyform'] = array(); // re-initialize the form session data
	if ($_SERVER['REQUEST_METHOD'] == 'POST' && @$_POST['do'] == 'contact') {
		// if the form has been submitted
		
		

		foreach($_POST as $key => $value) {
			if (!is_array($key)) {
				// sanitize the input data
				$_POST[$key] = htmlspecialchars(stripslashes(trim($value)));
			}
		}
	
		$code    = @$_POST['code'];    // code from the form
		$contact    = @$_POST['contact'];    // contact from the form
		$contact_info    = @$_POST['contact_info'];    // contact from the form
		$score    = @$_POST['score'];   // score from the form
		$comments = @$_POST['comments'];   // comments from the form
		//print_r($_POST);
		
		$_SESSION['surveyform']['code'] = $code;       // save code
		$_SESSION['surveyform']['contact'] = $contact;       // save contact
		$_SESSION['surveyform']['contact_info'] = $contact_info;       // save contact
		$_SESSION['surveyform']['score'] = $score;     // save role
		$_SESSION['surveyform']['comments'] = $comments;     // save comment
	
		$errors = array();  // initialize empty error array
		
		//$host="localhost"; // Host name
		//$username="oiserv5_web"; // Mysql username
		//$password="Plat1num"; // Mysql password
		//$db_name="oiserv5_report"; // Database name
		require($_SERVER['DOCUMENT_ROOT'].'/mysqlInfo.php');
		$mysqli = new mysqli($host, $username, $password, $db_name);
		if ($mysqli->connect_errno) {die($mysqli->connect_error);}
			
		$sql="SELECT * FROM nps_codes WHERE code = '".trim($code)."';";
		if(!$resultCode = $mysqli->query($sql)){
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,true,basename(__FILE__));
		}
		$rowCode = $resultCode->fetch_assoc();
		
		if(strtolower($rowCode['used']) == 'y'){
			$errors['code_error'] = '<br>Survey code has already been used';
		}elseif($resultCode->num_rows == 0){
			$errors['code_error'] = '<br>Survey code is invalid';	
		}elseif(time() > strtotime($rowCode['expire'])){
			$errors['code_error'] = '<br>Survey code has expired';
		}
		
		if (strlen($code) < 6) {
			// name too short, add error
			$errors['code_error'] = '<br>Survey Code must be 6 characters';
		}

		if (sizeof($errors) == 0) {
			// no errors, send the form
			$_SESSION['surveyform']['error'] = false;  // no error with form
			$_SESSION['surveyform']['success'] = true; // message sent
			
			$ip = $_SERVER['REMOTE_ADDR'];
			$domain = gethostbyaddr($ip);
			$date = date(storef,time()); 
			$browser = $_SERVER['HTTP_USER_AGENT'];
	
			$sql="UPDATE nps_codes SET `used`='Y', `used_date`='".date(storef,time())."' WHERE code = '".trim($code)."';";
			if(!$resultCodeUpdate = $mysqli->query($sql)){
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,true,basename(__FILE__));	
			}
			
//			$sql="INSERT INTO `nps_responses` (`code`, `date`, `score`, `comment`, `can_contact`, `contact_info`, `unique_id`, `quarter_unique_id`, `ip` , `domain`, `browser`)
//			VALUES ('$code', '$date', '$score', '$comments', '$contact', '$contact_info', '".$rowCode['unique_id']."', '".$rowCode['quarter_unique_id']."', '$ip', '$domain', '$browser');";
//			if(!$resultCodeUpdate = $mysqli->query($sql)){
//				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,true,basename(__FILE__));
//			}

			$sql="INSERT INTO `nps_responses` (`code`, `date`, `score`, `comment`, `can_contact`, `contact_info`, `unique_id`, `quarter_unique_id`, `ip` , `domain`, `browser`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
			$stmt = $mysqli->prepare($sql);

			if (!$stmt->bind_param("sssssssssss",
				$code,
				$date,
				$score,
				$comments,
				$contact,
				$contact_info,
				$rowCode['unique_id'],
				$rowCode['quarter_unique_id'],
				$ip,
				$domain,
				$browser
			)
			) {
				echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
				die();
			}

			if (!$stmt->execute()) {
				echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
				die();
			}

		} else {
			// save the entries, this is to re-populate the form
			$_SESSION['surveyform']['code'] = $code;       // save code
			$_SESSION['surveyform']['contact'] = $contact;       // save contact
			$_SESSION['surveyform']['contact_info'] = $contact_info;       // save contact
			$_SESSION['surveyform']['score'] = $score;     // save score
			$_SESSION['surveyform']['comments'] = $comments;     // save comment
			foreach($errors as $key => $error) {
				// set up error messages to display with each field
				$_SESSION['surveyform'][$key] = "<span style=\"font-weight: bold; color: #f00\">$error</span>";
			}		
			$_SESSION['surveyform']['error'] = true; // set error floag
			//return false;
		}
	} // POST
}

$_SESSION['surveyform']['success'] = false; // clear success value after running





