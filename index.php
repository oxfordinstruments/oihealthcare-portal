<?php
$at_login_page = true;
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_auto_login.php');
$sal = new sessionAutoLogin($mysqli);

if($sal->auto_login_check() != false){
	header("location:".$_SESSION['loc']);	
}


?>
<!DOCTYPE html>
<html>
<head>
<title><?php readfile($_SERVER['DOCUMENT_ROOT'].'/company_name.txt'); ?> Service Portal</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link href="/resources/css/screen.min.css" rel="stylesheet" type="text/css" media="screen" />
<link href="/resources/css/320.min.css" rel="stylesheet" type="text/css" />
<link href="/resources/css/720.min.css" rel="stylesheet" type="text/css" />
<link href="/resources/css/960.min.css" rel="stylesheet" type="text/css" />
<link href="/resources/css/print.min.css" rel="stylesheet" type="text/css" media="print" />
<!--[if lt IE 9 ]>
        <link href="/resources/css/screen-ie.min.css" rel="stylesheet" type="text/css" />
    <![endif]-->
<!--[if IE 9 ]>
        <link href="/resources/css/screen-ie9.min.css" rel="stylesheet" media="screen" />
    <![endif]-->

<meta charset="UTF-8" />
<link href="/resources/images/favicon.ico" type="image/x-icon" rel="shortcut icon"/>
<link href="/resources/images/favicon.ico" type="image/x-icon" rel="icon"/>
<link href="resources/css/main_engineer_960.css" rel="stylesheet" type="text/css">
<link href="resources/css/main_engineer_720.css" rel="stylesheet" type="text/css">
<link href="resources/css/main_engineer_320.css" rel="stylesheet" type="text/css">
<script src="/resources/js/jquery-1.7.2.min.js"></script>
<script src="/resources/js/jquery.badBrowser.js"></script>
<xsl:variable name="companyName" select="'Test Company'"/>
</head>
<body>
<?php require($_SERVER['DOCUMENT_ROOT'].'/report/head_foot/default_header.php'); ?>
<div id="OIReportContent"> 
<div style="padding-top:20px; height:400px; text-align:justify; font-size:16px">
	<p><strong style="font-size:19px; color:#1B2673;">Welcome to the <?php readfile($_SERVER['DOCUMENT_ROOT'].'/company_name.txt'); ?> Service Portal!</strong></p>
	<img src="/resources/images/CT_MR_Side_by_site_Shadow.png" width="341" height="206" style="padding-left:20px;" align="right">
    <p><?php readfile($_SERVER['DOCUMENT_ROOT'].'/company_name.txt'); ?> is committed to supporting our  customers&rsquo; success by offering world class services and support tools through  our online service portal. The service portal is a secure site that provides  you with instant access to critical information about your GE CT and/or MR  systems – allowing you to view the status of service requests, and service  records at any time. </p>
<p>If you are a customer, <a href="register.php">click here to register</a>. Once  registered, you will automatically receive an email notification detailing your  login credentials. Login at any time to view your site&rsquo;s service records,  uptime index and more.</p>
	<p>Already Registered? <a href="<?php echo $settings->full_url; ?>/report">Click here to login</a>.</p>
</div>
</div>
<?php require($_SERVER['DOCUMENT_ROOT'].'/report/head_foot/default_footer.php'); ?>