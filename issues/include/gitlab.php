<?php
class GitLab {
	private $api_search = "projects/search/";

	public function get_proj_id(){
		$rtn = $this->curl($this->api_search.GL_PROJ);
		if(!is_array($rtn)){
			$json = json_decode($rtn, true);
			$id = $json[0]['id'];
			return $id;
		}else{
			return "FAILED:".$rtn[1].":".$rtn[2].":".$rtn[3];
		}
	}

	public function get_issue_id($proj_id){
		$rtn = $this->curl("projects/".$proj_id."/issues?state=opened");
		if(!is_array($rtn)){
			$json = json_decode($rtn, true);
			$id = $json[0]['id'];
			return $id;
		}else{
			return "FAILED:".$rtn[1].":".$rtn[2].":".$rtn[3];
		}
	}

	private function curl($api){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, GL_CURLURL.$api);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("PRIVATE-TOKEN: ".GL_TOKEN));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)');
		$output = curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		if(intval($httpcode) == 200 or intval($httpcode) == 201){
			return $output;
		}else{
			return array(false,$httpcode, $output, $ch);
		}
	}
}
?>