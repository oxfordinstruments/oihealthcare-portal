<?php
error_reporting(E_ALL);
$debug = false;
if($debug){echo 'start<br>';}

$log = $_GET['file'];
switch(strtolower($log)){
	case 'messagelog':
		$f = $_SERVER['DOCUMENT_ROOT'].'/log/messagelog.txt';
		if($debug){echo 'case messagelog<br>';}
		break;
	case 'infolog':
		$f = $_SERVER['DOCUMENT_ROOT'].'/log/infolog.txt';
		if($debug){echo 'case infolog<br>';}
		break;
	case 'errorlog':
		$f = $_SERVER['DOCUMENT_ROOT'].'/log/errorlog.txt';
		if($debug){echo 'case errorlog<br>';}
		break;
	case 'ldaplog':
		$f = $_SERVER['DOCUMENT_ROOT'].'/log/ldaplog.txt';
		if($debug){echo 'case ldaplog<br>';}
		break;
	default:
		if($debug){echo 'case default<br>';}
		echo '0';
		return false;	
}
if($debug){echo $f.'<br>';}
if(!$myTextFileHandler = fopen($f,"r+")){
	if($debug){echo 'fopen failed<br>';}
	echo '0';
	return false;	
}
if(!ftruncate($myTextFileHandler, 0)){
	if($debug){echo 'ftruncate failed<br>';}
	echo '0';
	return false;	
}

if($debug){echo 'complete<br>';}
echo '1';
return true;
?>

