<?php
//Logging functions
//error_reporting(E_ALL);

if(isset($_POST['type']) and isset($_POST['text'])){
	$logger = new logger();
	$number = 0;
	$location = 'na';
	$line = 0;
	$type = htmlspecialchars($_POST['type']);
	$text = htmlspecialchars($_POST['text']);
	$number = htmlspecialchars($_POST['number']);
	$location = htmlspecialchars($_POST['location']);
	$line = htmlspecialchars($_POST['line']);
	
	switch($type){
		case 'error':
			$logger->logerr($text,$number,false,$location,$line);
			break;
		case 'info':
			$logger->loginfo($text,$number,false,$location,$line);
			break;
		case 'message':
			$logger->logmsg($text,$number,false,$location,$line);
			break;
		default:
			die("input error");
	}
}

class logger{
	protected $errors_arr;
	private $settings;
	private $docroot;
	private $cli = false;
	
	public function __construct($docroot = NULL){
		if($docroot == NULL){
			$docroot = $_SERVER['DOCUMENT_ROOT'];
			if(substr(php_sapi_name(), 0, 3) == 'cli' || empty($_SERVER['REMOTE_ADDR'])){
				//shell
				$docroot = preg_replace("/(public_html).*/",'public_html',dirname(__FILE__),2);
				$this->cli = true;
			}
		}
		$this->docroot = $docroot;
		$errors_array = array();
		require_once($docroot.'/errors_inc.php');
		$this->errors_arr = $errors_array;
		$this->settings = new SimpleXMLElement($docroot.'/settings.xml', null, true);
	}
		
	public function logmsg($txt,$num = 0,$rd = false,$file=false,$line=false){
		$this->logdata($txt,1,$num,$rd,$file,$line);	
	}
	
	public function loginfo($txt,$num = 0,$rd = false,$file=false,$line=false){
		$this->logdata($txt,2,$num,$rd,$file,$line);	
	}
	
	public function logerr($txt,$num = 0,$rd = false,$file=false,$line=false){
		$this->logdata($txt,3,$num,$rd,$file,$line);	
	}
	

	
	//
	//log function
	//
	private function logdata($txt,$type,$num,$rd,$file,$line){
		//$type = 1 message
		//$type = 2 information
		//$type = 3 error
		if(!$this->cli){
			$protocol = strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https')	=== FALSE ? 'http' : 'https';
			$host = $_SERVER['HTTP_HOST'];
			$script = $_SERVER['SCRIPT_NAME'];
			$params = $_SERVER['QUERY_STRING'];
			$currentUrl = $protocol . '://' . $host . $script;
			if($params != ''){	
				$currentUrl .= '?'.$params;
			}
		}else{
			try{
				$currentUrl = $_SERVER['SCRIPT_NAME'];
			}catch (Exception $e) {
    			//echo 'Caught exception: ',  $e->getMessage(), "\n";
				$currentUrl = '';
			}
		}
		
		if(!$file){
			$file = 'NA';	
		}
		
		if(!$line){
			$line = 'NA';	
		}
		
		$error = "[ ".date('r',time())." ] ".$currentUrl."|FILE: ".$file."|LINE: ".$line." |NUM: ".$num." | MSG: ".$txt;

		if(!$this->cli){
			if($_SESSION['login'] != ""){
				$error .= " | UID: ".$_SESSION['login'];
			}
		}else{
			$error .= " | UID: localhost";
		}
		$error .= PHP_EOL;
		switch($type){
			case 1:
				$path = $this->docroot."/log/messagelog.txt";
				break;
			case 2:
				$path = $this->docroot."/log/infolog.txt";
				break;
			case 3:
				$path = $this->docroot."/log/errorlog.txt";
				break;	
		}
		if(!error_log($error,3,$path)){
			die('Could not write to error log: '.$path);	
		}
		if($rd){
			if(!headers_sent()){
				header("location:/error.php?n=$num&t=".htmlspecialchars($txt)."&p=$file");
				exit;
			}else{
				echo "<script>";
				echo "window.location = '".$this->settings->full_url."/error.php?n=$num&t=$txt&p=$file';";
				echo "</script>";
				exit;
			}
		}
	}


	//
	//Log Errors
	//
	public function logldap($txt,$num = ""){
		$protocol = strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https')	=== FALSE ? 'http' : 'https';
		$host = $_SERVER['HTTP_HOST'];
		$script = $_SERVER['SCRIPT_NAME'];
		$params = $_SERVER['QUERY_STRING'];
		$currentUrl = $protocol . '://' . $host . $script;
		if($params != ''){	
			$currentUrl .= '?'.$params;
		}
		
		$error = "[ ".date('r',time())." ] ".$currentUrl." | MSG: ".$txt;
		if($num != ""){
			$error .= " | ERR: ".$num;
		}
		if($_SESSION['login'] != ""){
			$error .= " | UID: ".$_SESSION['login'];
		}
		$error .= PHP_EOL;
		$path = $this->docroot."/log/ldaplog.txt";
		error_log($error,3,$path);
	}
	
	public function logldapblankline($count = 1){
		$path = $this->docroot."/log/ldaplog.txt";
		for ($i = 1; $i <= $count; $i++) {
			error_log(' '.PHP_EOL,3,$path);
		}
	}

}//end class
?>