<?php
//Update Completed 11/25/14
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');
?>

<!doctype html>
<html>
<head>
<?php require_once($head_include);?>
<?php require_once($css_include);?>
<?php require_once($js_include);?>

<script type="text/javascript" language="javascript" src="/resources/js/jquery/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() { 
		var mydiv = $('#err');
		mydiv.scrollTop(mydiv.prop('scrollHeight'));
		
		var mydiv = $('#msg');
		mydiv.scrollTop(mydiv.prop('scrollHeight'));
		
		var mydiv = $('#inf');
		mydiv.scrollTop(mydiv.prop('scrollHeight'));
		
		var mydiv = $('#ldap');
		mydiv.scrollTop(mydiv.prop('scrollHeight'));
	});
</script>
</head>
<body>		
	<div style="margin-left:20px; margin-right:20px;">
		<div style="margin-bottom:20px;">
			<h2>Current Time: <?php echo date(storef,time()); ?></h2>
		</div>
		<h2>Error Log</h2>
		<div id="err" class="scr" style="overflow:auto; height:500px">
			<pre>
			<?php
			echo file_get_contents($_SERVER['DOCUMENT_ROOT']."/log/errorlog.txt");
			?>
			</pre>
		</div>
		<hr>
		<h2>Message Log</h2>
		<div id="msg" class="scr" style="overflow:auto; height:500px">
			<pre>
			<?php
			echo file_get_contents($_SERVER['DOCUMENT_ROOT']."/log/messagelog.txt");
			?>
			</pre>
		</div>
		<hr>
		<h2>Info Log</h2>
		<div id="inf" class="scr" style="overflow:auto; height:500px">
			<pre>
			<?php
			echo file_get_contents($_SERVER['DOCUMENT_ROOT']."/log/infolog.txt");
			?>
			</pre>
		</div>
		<hr>
		<h2>LDAP Log</h2>
		<div id="ldap" class="scr" style="overflow:auto; height:500px">
			<pre>
			<?php
			echo file_get_contents($_SERVER['DOCUMENT_ROOT']."/log/ldaplog.txt");
			?>
			</pre>
		</div>
	</div>
</body>
</html>