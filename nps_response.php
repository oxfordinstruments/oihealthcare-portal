<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

$used_code=<<<_HTML
<br />
<br />
<br />
<br />
<table width="100%" align="center">
<tr>
<td align="center">
Thank you but the feedback for your code has already been received.
</td>
</tr>
</table>
_HTML;

$invalid=<<<_HTML
<br />
<br />
<br />
<br />
<table width="100%" align="center">
<tr>
<td align="center">
Thank you but the code you entered is invalid.
</td>
</tr>
</table>
_HTML;

$expired=<<<_HTML
<br />
<br />
<br />
<br />
<table width="100%" align="center">
<tr>
<td align="center">
Thank you but the code you have entered has expired.
</td>
</tr>
</table>
_HTML;

$good=<<<_HTML
<br />
<br />
<br />
<br />
<table width="100%" align="center">
<tr>
<td align="center">
Thank you for your feedback
</td>
</tr>
</table>
_HTML;
ob_start();

$settings = new SimpleXMLElement($_SERVER['DOCUMENT_ROOT'].'/settings.xml', null, true);
require_once($_SERVER['DOCUMENT_ROOT'].'/define_inc.php');

require($_SERVER['DOCUMENT_ROOT'].'/mysqlInfo.php');

$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno) {die($mysqli->connect_error);}

$sql="SELECT * FROM nps_codes WHERE code = '".trim($_POST['q21_feedbackLink'])."';";
if(!$resultCode = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__));
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__));
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__));
}
$rowCode = $resultCode->fetch_assoc();

if(strtolower($rowCode['used']) == 'y'){die($used_code);}

if($resultCode->num_rows == 0){die($invalid);}

if(time() > strtotime($rowCode['expire'])){die($expired);}

$sql="UPDATE nps_codes SET `used`='Y', `used_date`='".date(storef,time())."' WHERE code = '".trim($_POST['q21_feedbackLink'])."';";
if(!$resultCodeUpdate = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__));
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__));
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__));
}

$ip = $_SERVER['REMOTE_ADDR'];
$domain = gethostbyaddr($ip);

//$sql="INSERT INTO `nps_responses` (`code`, `date`, `score`, `comment`, `can_contact`, `contact_info`, `unique_id`, `ip` , `domain`)
//VALUES ('".$rowCode['code']."', '".date(storef,time())."', '".$_POST['q20_basedOn']."', '".$_POST['q6_clickTo6']."',
// '".$_POST['q7_mayWe']."', '".$_POST['q3_yourContact3']."', '".$rowCode['unique_id']."',
// '$ip', '$domain');";

$sql="INSERT INTO `nps_responses` (`code`, `date`, `score`, `comment`, `can_contact`, `contact_info`, `unique_id`, `ip` , `domain`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";
$stmt = $mysqli->prepare($sql);

if (!$stmt->bind_param("sssssssss",
						$rowCode['code'],
						date(storef,time()),
						$_POST['q20_basedOn'],
						$_POST['q6_clickTo6'],
						$_POST['q7_mayWe'],
						$_POST['q3_yourContact3'],
						$rowCode['unique_id'],
						$ip,
						$domain
						)
	) {
	echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
}

if (!$stmt->execute()) {
	echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
}

//if(!$resultCodeUpdate = $mysqli->query($sql)){
//	$log->logerr($sql,1000,false,basename(__FILE__));
//	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__));
//	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__));
//}

ob_end_flush();

exit($good);
?>

