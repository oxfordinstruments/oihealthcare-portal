<?php
//
// Error reporting
//
//error_reporting(E_ALL);
//error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
//ini_set('display_errors', 'On');

session_name("OIREPORT");
session_start();

$settings = new SimpleXMLElement($_SERVER['DOCUMENT_ROOT'].'/settings.xml', null, true);
require_once($_SERVER['DOCUMENT_ROOT'].'/define_inc.php');

set_include_path($settings->php_include_path);

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require($_SERVER['DOCUMENT_ROOT'].'/mysqlInfo.php');
$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if($mysqli->connect_errno){
	$log->logerr($sql,1000,false,basename(__FILE__));
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__));
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__));
}

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/smarty/libs/Smarty.class.php');
$smarty = new Smarty();
$smarty->setTemplateDir($_SERVER['DOCUMENT_ROOT'].$settings->email_templates);
$smarty->setCompileDir($_SERVER['DOCUMENT_ROOT'].$settings->email_template_c);
$smarty->setCacheDir($_SERVER['DOCUMENT_ROOT'].$settings->email_cache);
$smarty->setConfigDir($_SERVER['DOCUMENT_ROOT'].$settings->email_configs);
$smarty->force_compile = true;
$smarty->debugging = true;
$smarty->caching = false;
$smarty->assign('company_name',$settings->company_name);
$smarty->assign('copyright_date',$settings->copyright_date);
$smarty->assign('email_pics',$settings->email_pics);

$send = true;

if($settings->disable_email == '1'){
	$send = false;
}

$roles = array(1=>'System CT/MR Operator', 2=>'Facility Manager', 3=>'Oxford Instruments Employee', 4=>'Oxford Instruments Contractor');

process_si_contact_form();
?>

	<!DOCTYPE html>
	<html>
	<head>
		<style type="text/css">
			<!--
			.error {
				color: #f00;
				font-weight: bold;
				font-size: 1.2em;
			}
			.success {
				color: #00f;
				font-weight: bold;
				font-size: 1.2em;
			}
			fieldset {
				width: 90%;
			}
			legend {
				font-size: 24px;
			}
			.note {
				font-size: 18px;
			}
			-->
			.text-xs-center {
				text-align: center;
			}

			.g-recaptcha {
				display: inline-block;
			}
		</style>
		<title>Service Portal - Oxford Instruments</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link href="/resources/css/screen.min.css" rel="stylesheet" type="text/css" media="screen" />
		<link href="/resources/css/320.min.css" rel="stylesheet" type="text/css" />
		<link href="/resources/css/720.min.css" rel="stylesheet" type="text/css" />
		<link href="/resources/css/960.min.css" rel="stylesheet" type="text/css" />
		<link href="/resources/css/print.min.css" rel="stylesheet" type="text/css" media="print" />
		<!--[if lt IE 9 ]>
		<link href="/resources/css/screen-ie.min.css" rel="stylesheet" type="text/css" />
		<![endif]-->
		<!--[if IE 9 ]>
		<link href="/resources/css/screen-ie9.min.css" rel="stylesheet" media="screen" />
		<![endif]-->

		<meta charset="UTF-8" />
		<link href="/resources/images/favicon.ico" type="image/x-icon" rel="shortcut icon"/>
		<link href="/resources/images/favicon.ico" type="image/x-icon" rel="icon"/>
		<script type='text/javascript' src='/resources/js/jsmd5.js'></script>
		<script type='text/javascript' src='/resources/js/php.default.min.js'></script>
		<script src="/resources/js/jquery-1.7.2.min.js"></script>
		<script src="/resources/js/jquery.badBrowser.js"></script>
		<script type="text/javascript">
			function highlight(){
				document.getElementById("name").focus();
				<?php
				if(isset($_SESSION['registerform']['success']) && $_SESSION['registerform']['success'] == true){
				?>
				$("#requestdiv").hide();
				<?php
				}
				?>
			}
			function submitcheck(){
				document.forms["form"].submit();
			}
		</script>
	</head>
<body>
<?php require($_SERVER['DOCUMENT_ROOT'].'/report/head_foot/default_header.php'); ?>
	<div class="outerContainer" id="content">
		<div style="width:100%; margin-top:20px; text-align:center;  margin-bottom:20px;">
			<h1 style="color:#F79447">Email Subscription</h1>
		</div>
		<div style="width:500px; margin-left:auto; margin-right:auto;">
			<div style="text-align:center">
				<?php
				//process_si_contact_form(); // Process the form, if it was submitted
				if (isset($_SESSION['registerform']['error']) &&  $_SESSION['registerform']['error'] == true): /* The last form submission had 1 or more errors */ ?>
					<span class="error">There was a problem with your submission.  Errors are displayed below in red.</span><br />
					<br />
				<?php elseif (isset($_SESSION['registerform']['success']) && $_SESSION['registerform']['success'] == true): /* form was processed successfully */ ?>
					<span class="success">Thank you. Your subscription has been submitted!<br>
			You will receive an email when your subscription has been approved.<br>
			If you any questions please contact <a href="mailto:<?php echo $settings->email_support ?>"><?php echo $settings->email_support ?></a><br>
			<br>
			<a href="/" target="_self">Click here to return to the main page.</a></span><br />
					<br />
				<?php endif; ?>
			</div>
			<div id="requestdiv">
				<form id="form" name="form" method="post">
					<input type="hidden" name="do" value="contact" />
					<div style="text-align:center">
						<div style="text-align:left; width:75%; margin-left:auto; margin-right:auto"><strong>Full Name*:</strong>&nbsp; &nbsp;<?php echo @$_SESSION['registerform']['name_error'] ?></div>
						<div style=" width:75%; margin-left:auto; margin-right:auto; margin-bottom:20px;">
							<input style="width:100%" type="text" id="name" name="name" value="<?php echo htmlspecialchars(@$_SESSION['registerform']['name']) ?>" />
						</div>
						<div style="text-align:left; width:75%; margin-left:auto; margin-right:auto"><strong>Email*:</strong>&nbsp; &nbsp;<?php echo @$_SESSION['registerform']['email_error'] ?></div>
						<div style=" width:75%; margin-left:auto; margin-right:auto; margin-bottom:20px;">
							<input style="width:100%" type="text" id="email" name="email" value="<?php echo htmlspecialchars(@$_SESSION['registerform']['email']) ?>" />
						</div>
						<div id="system_id_div">
							<div style="text-align:left; width:75%; margin-left:auto; margin-right:auto"><strong>System/Site ID(s)*:</strong>&nbsp; &nbsp;<?php echo @$_SESSION['registerform']['system_id_error'] ?></div>
							<div style=" width:75%; margin-left:auto; margin-right:auto; margin-bottom:20px;">
								<input style="width:100%" type="text" id="system_id" name="system_id" maxlength="200" value="<?php echo htmlspecialchars(@$_SESSION['registerform']['system_id']) ?>" />
							</div>
						</div>

						<div style="text-align: center; width:75%; margin-left:auto; margin-right:auto"><?php echo @$_SESSION['registerform']['captcha_error'] ?></div>
						<div id="g-recaptcha" class="g-recaptcha" data-sitekey="<?php echo $settings->google->site_key;?>"></div>
						<script type="text/javascript" src="https://www.google.com/recaptcha/api.js?hl=<?php echo $settings->google->language;?>"></script>
						<p> <br />
							<input name="Submit" id="submitbtn" type="button" value="Submit" class="button" onclick="submitcheck();" />
						</p>
					</div>
				</form>
				</fieldset>
			</div>
		</div>
		<div style="width:100%; margin-top:20px; margin-bottom:60px">
			<div style="margin-left:auto; margin-right:auto; width:50%; text-align:left;"> This system should be accessed by authorized <?php echo $settings->company_name; ?> personnel and customers only. Unauthorized access or use of this computer system may subject violators to criminal, civil, and/or administrative action. </div>
		</div>
	</div>
<?php require($_SERVER['DOCUMENT_ROOT'].'/report/head_foot/default_footer.php'); ?>
<?php
// The form processor PHP code
function process_si_contact_form()
{
	global $log, $smarty, $mysqli, $roles, $settings;

	$_SESSION['registerform'] = array(); // re-initialize the form session data
	if ($_SERVER['REQUEST_METHOD'] == 'POST' && @$_POST['do'] == 'contact') {
		// if the form has been submitted
		require($_SERVER['DOCUMENT_ROOT'].'/resources/google/recaptcha/autoload.php');
		$recaptcha = new \ReCaptcha\ReCaptcha((string)$settings->google->secret_key);
		$recaptcha_resp = $recaptcha->verify(@$_POST['g-recaptcha-response']);
		$recaptcha_verify = false;
		if($recaptcha_resp->isSuccess()){
			$recaptcha_verify = true;
		}

		//echo print_r($_POST),EOL;
		//die();

		foreach($_POST as $key => $value) {
			if (!is_array($key)) {
				// sanitize the input data
				$_POST[$key] = htmlspecialchars(stripslashes(trim($value)));
			}
		}

		$name    = @$_POST['name'];    // name from the form
		$email   = @$_POST['email'];   // email from the form
		$system_id = @$_POST['system_id'];   // system id from the form
		$captcha = @$_POST['ct_captcha']; // the user's entry for the captcha code
		$name    = substr($name, 0, 64);  // limit name to 64 characters

		$_SESSION['registerform']['name'] = $name;       // save name from the form submission
		$_SESSION['registerform']['email'] = $email;     // save email
		$_SESSION['registerform']['system_id'] = $system_id;     // save system id

		$errors = array();  // initialize empty error array


		if (strlen($name) < 3) {
			// name too short, add error
			$errors['name_error'] = 'Your name is required more than 3 letters';
		}

		if (strlen($system_id) == 0) {
			// no system id given
			$errors['system_id_error'] = 'System ID(s) required';
		}

		if (strlen($email) == 0) {
			// no email address given
			$errors['email_error'] = 'Email address required';
		}else if ( !preg_match('/^(?:[\w\d]+\.?)+@(?:(?:[\w\d]\-?)+\.)+\w{2,4}$/i', $email)) {
			// invalid email format
			$errors['email_error'] = 'Email address entered is invalid';
		}

		// Only try to validate the captcha if the form has no errors
		// This is especially important for ajax calls
		if (sizeof($errors) == 0) {
			if(!$recaptcha_verify){
				$errors['captcha_error'] = "Re-Captcha failed. Are you a robot?";
			}
		}

		if (sizeof($errors) == 0) {
			// no errors, send the form
			$_SESSION['registerform']['error'] = false;  // no error with form
			$_SESSION['registerform']['success'] = true; // message sent

			$ip = $_SERVER['REMOTE_ADDR'];
			$domain = gethostbyaddr($ip);
			$date = date(storef,time());
			$browser = $_SERVER['HTTP_USER_AGENT'];
			$unique_id = md5(uniqid());

			$sql="INSERT INTO registration_requests (`name`, `email_list`, `email`, `system_id`, `ip`, `domain`, `browser`, `date`, `unique_id`) 
			VALUES (\"".htmlspecialchars($name)."\", 'Y', \"".htmlspecialchars($email)."\", \"".htmlspecialchars($system_id)."\", '$ip', '$domain', '$browser', '$date', '$unique_id');";
			if(!$result = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__));
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__));
				$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__));
				header('location:/error.php?n=1006&p=register.php');
			}

			senduseremail($name, $email);
			sendadminemail($unique_id);
			$mysqli->close();
		} else {
			// save the entries, this is to re-populate the form
			$_SESSION['registerform']['name'] = $name;       // save name from the form submission
			$_SESSION['registerform']['email'] = $email;     // save email
			$_SESSION['registerform']['system_id'] = $system_id;     // save system id
			foreach($errors as $key => $error) {
				// set up error messages to display with each field
				$_SESSION['registerform'][$key] = "<span style=\"font-weight: bold; color: #f00\">$error</span>";
			}
			$_SESSION['registerform']['error'] = true; // set error floag
			//return false;
		}
	} // POST
}

function sendadminemail($unique_id){
	global $log, $smarty, $send, $mysqli, $roles, $settings;

	$sql="SELECT * FROM registration_requests WHERE unique_id = '$unique_id';";
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__));
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__));
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__));
	}
	$row = $result->fetch_assoc();
	$smarty->assign('name',$row['name']);
	$smarty->assign('email',$row['email']);
	$smarty->assign('system_id',$row['system_id']);
	$smarty->assign('ip',$row['ip']);
	$smarty->assign('id',$row['id']);
	$smarty->assign('domain',$row['domain']);
	$smarty->assign('browser',$row['browser']);
	$smarty->assign('date',$row['date']);
	$smarty->assign('unique_id',$row['unique_id']);


	$subject = $settings->short_name.' Portal Email Subscription Request';

	require_once($_SERVER['DOCUMENT_ROOT'].'/resources/PHPMailer/PHPMailerAutoload.php');

	$mailAdmin = new PHPMailer;
	$mailAdmin->IsSMTP();
	$mailAdmin->Host = $settings->email_host;
	$mailAdmin->SMTPAuth = true;
	$mailAdmin->Username = $settings->email_registration;
	$mailAdmin->Password = $settings->email_password;
	$mailAdmin->SMTPSecure = 'tls';
	$mailAdmin->From = $settings->email_registration;
	$mailAdmin->FromName = 'Registration '.$settings->short_name.' Portal';
	$mailAdmin->AddAddress($settings->email_support, 'Support '.$settings->short_name.' Portal');
	$mailAdmin->AddReplyTo($settings->email_support, 'Support '.$settings->short_name.' Portal');
	$mailAdmin->IsHTML(true);
	$mailAdmin->Subject = $subject;
	$mailAdmin->Body = $smarty->fetch('account_register_req_admin.tpl');
	if($send){
		if(!$mailAdmin->Send()) {
			$log->logerr($mailAdmin->ErrorInfo,1007,true,basename(__FILE__));
			echo 'Email could not be sent.';
			echo 'Mailer Error: ' . $mailAdmin->ErrorInfo;
			exit;
		}else{
			$log->loginfo(implode('; ',array_keys($mailAdmin->getAllRecipientAddresses())),200,false,basename(__FILE__));
		}
	}
}

function senduseremail($_name,$_email){
	global $log, $smarty, $send, $settings;

	$smarty->assign('name',$_name);
	$smarty->assign('base_url_pretty', $settings->base_url_pretty);
	$smarty->assign('support_email', $settings->email_support);

	$subject = $settings->short_name.' Portal Email Subscription Request';

	require_once($_SERVER['DOCUMENT_ROOT'].'/resources/PHPMailer/PHPMailerAutoload.php');

	$mail = new PHPMailer;
	$mail->IsSMTP();
	$mail->Host = $settings->email_host;
	$mail->SMTPAuth = true;
	$mail->Username = $settings->email_registration;
	$mail->Password = $settings->email_password;
	$mail->SMTPSecure = 'tls';
	$mail->From = $settings->email_registration;
	$mail->FromName = 'Registration '.$settings->short_name.' Portal';
	$mail->AddAddress($_email, $_name);
	$mail->AddReplyTo($settings->email_support, 'Support '.$settings->short_name.' Portal');
	$mail->AddReplyTo($settings->email_support, 'Support '.$settings->short_name.' Portal');
	$mail->IsHTML(true);
	$mail->Subject = $subject;
	$mail->Body = $smarty->fetch('account_register_req_user.tpl');
	if($send){
		if(!$mail->Send()) {
			$log->logerr($mail->ErrorInfo,1007,true,basename(__FILE__));
			echo 'Email could not be sent.';
			echo 'Mailer Error: ' . $mail->ErrorInfo;
			exit;
		}else{
			$log->loginfo(implode('; ',array_keys($mail->getAllRecipientAddresses())),200,false,basename(__FILE__));
		}
	}
}

function randomString($length) {
	$key = null;
	$keys = array_merge(range(0,9), range('a', 'z'), range('A', 'Z'));
	for($i=0; $i < $length; $i++) {
		$key .= $keys[array_rand($keys)];
	}
	return $key;
}

$_SESSION['registerform']['success'] = false; // clear success value after running