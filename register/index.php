<?php
session_name("OIREPORT");
session_start();

$settings = new SimpleXMLElement($_SERVER['DOCUMENT_ROOT'].'/settings.xml', null, true);
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>

<title>Service Portal - Oxford Instruments</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link href="/resources/css/screen.min.css" rel="stylesheet" type="text/css" media="screen" />
<link href="/resources/css/320.min.css" rel="stylesheet" type="text/css" />
<link href="/resources/css/720.min.css" rel="stylesheet" type="text/css" />
<link href="/resources/css/960.min.css" rel="stylesheet" type="text/css" />
<link href="/resources/css/print.min.css" rel="stylesheet" type="text/css" media="print" />
<!--[if lt IE 9 ]>
        <link href="/resources/css/screen-ie.min.css" rel="stylesheet" type="text/css" />
    <![endif]-->
<!--[if IE 9 ]>
        <link href="/resources/css/screen-ie9.min.css" rel="stylesheet" media="screen" />
    <![endif]-->

<meta charset="UTF-8" />
<link href="/resources/images/favicon.ico" type="image/x-icon" rel="shortcut icon"/>
<link href="/resources/images/favicon.ico" type="image/x-icon" rel="icon"/>
<script type='text/javascript' src='/resources/js/jsmd5.js'></script>
<script type='text/javascript' src='/resources/js/php.default.min.js'></script>
<script type="text/javascript" src="/resources/js/jquery/jquery.min.js"></script>
<script type="text/javascript" src="/resources/js/jquery-ui/jquery-ui.min.js"></script>
	<style>
		#header_div{
			width:100%;
			margin-top:20px;
			text-align:center;
			margin-bottom:20px;
		}
		.section_div{
			width:600px;
			margin-left:auto;
			margin-right:auto;
			font-size: larger;
		}
		#account_div p br{
			margin-bottom: 5px;
		}
		.fix_ul{
			list-style: disc;
			margin: 0;
			padding-left: 40px;
		}
		#disclaimer{
			width:100%;
			margin-top:50px;
			margin-bottom:60px
		}
		#disclaimer_text{
			margin-left:auto;
			margin-right:auto;
			width:50%;
			text-align:left;
		}
	</style>
</head>
<body>
<?php require($_SERVER['DOCUMENT_ROOT'].'/report/head_foot/default_header.php'); ?>
<div class="outerContainer" id="content">
	<div id="header_div">
		<h1><?php echo (string)$settings->base_url_pretty; ?> Account</h1>
	</div>
	<div id="account_div" class="section_div">
		<p>Signing up for an account is easy!<br>
			Quickly provide your information, click submit and you’re off. <?php echo (string)$settings->company_name; ?> will review your application for an account and you will receive an e-mail notification.</p>
		<p>Here are just some of the many benefits you’ll have access to with an account:
		<ul class="fix_ul">
			<li>Current system health</li>
			<li>Quick access to service records</li>
			<li>Service Index to track downtime</li>
			<li>Stats about your service</li>
			<li>Contract and Warranty Information</li>
			<li>Email Updates about your systems</li>
			<li>Many more ...</li>
		</ul>
		</p>
		<form id="account" method="GET" action="account.php">
			<input name="Submit" type="button" value="Request an account" class="button" onclick="document.getElementById('account').submit();" />
		</form>
	</div>
	<div id="header_div">
		<h1>Email Subscription</h1>
	</div>
	<div class="section_div" ">
		<p>An email subscription is great for those people who just want know when there is a problem, and when that problem is fixed.<br>
			Emails sent concerning your systems are direct and to the point. No wading through advertisements and spam, just the information you need.</p>
		<form id="email" method="get" action="email.php">
			<input name="Submit" type="button" value="Subscribe to email" class="button" onclick="document.getElementById('email').submit();" />
		</form>
	</div>
	<div id="disclaimer">
		<div id="disclaimer_text"> This system should be accessed by authorized <?php echo (string)$settings->company_name; ?> personnel and customers only. Unauthorized access or use of this computer system may subject violators to criminal, civil, and/or administrative action. </div>
	</div>
</div>
<?php require($_SERVER['DOCUMENT_ROOT'].'/report/head_foot/default_footer.php'); ?>
<?php
$_SESSION['registerform']['success'] = false; // clear success value after running

?>



