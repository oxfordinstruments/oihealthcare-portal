<?php
/**
 * @package OiHealthcarePortal
 * @file unsubscribe.php
 * @author Evotodi <evotodi@gmail.com>
 * @date 11/1/2016
 * @time 10:19 AM
 */
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
ini_set('display_errors', 'On');

session_name("OIREPORT");
session_start();

$debug = false;
if(isset($_GET['debug'])){
	$debug = true;
}

$settings = new SimpleXMLElement($_SERVER['DOCUMENT_ROOT'].'/settings.xml', null, true);
require_once($_SERVER['DOCUMENT_ROOT'].'/define_inc.php');

set_include_path($settings->php_include_path);

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require($_SERVER['DOCUMENT_ROOT'].'/mysqlInfo.php');
$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if($mysqli->connect_errno){
	$log->logerr($sql,1000,false,basename(__FILE__));
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__));
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__));
}

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

if(!isset($_GET['eid']) or !isset($_GET['svuid'])){
	$log->logerr('EID or SVUID not in GET',4,false,basename(__FILE__));
	$log->logerr('Unsubscribe link was invalid.',1077,true,basename(__FILE__));
}

if(strlen($_GET['eid']) != 32 or strlen($_GET['svuid']) != 32){
	$log->logerr('EID or SVUID length wrong',4,false,basename(__FILE__));
	$log->logerr('Unsubscribe link was invalid.',1077,true,basename(__FILE__));
}

$email_unique_id = $_GET['eid'];
$system_ver_unique_id = $_GET['svuid'];


d($_GET);

$data = array();
$data_first = array();
get_data();

d($data_first);
d($_SESSION);
process_si_contact_form();
?>
<!DOCTYPE html>
	<html>
	<head>
		<style type="text/css">
			<!--
			.error {
				color: #f00;
				font-weight: bold;
				font-size: 1.2em;
			}
			.success {
				color: #00f;
				font-weight: bold;
				font-size: 1.5em;
			}
			fieldset {
				width: 90%;
			}
			legend {
				font-size: 24px;
			}
			.note {
				font-size: 18px;
			}
			-->
			.text-xs-center {
				text-align: center;
			}

			.g-recaptcha {
				display: inline-block;
			}
		</style>
		<title>Service Portal - Oxford Instruments</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link href="/resources/css/screen.min.css" rel="stylesheet" type="text/css" media="screen" />
		<link href="/resources/css/320.min.css" rel="stylesheet" type="text/css" />
		<link href="/resources/css/720.min.css" rel="stylesheet" type="text/css" />
		<link href="/resources/css/960.min.css" rel="stylesheet" type="text/css" />
		<link href="/resources/css/print.min.css" rel="stylesheet" type="text/css" media="print" />
		<!--[if lt IE 9 ]>
		<link href="/resources/css/screen-ie.min.css" rel="stylesheet" type="text/css" />
		<![endif]-->
		<!--[if IE 9 ]>
		<link href="/resources/css/screen-ie9.min.css" rel="stylesheet" media="screen" />
		<![endif]-->
		<link rel="stylesheet" type="text/css" href="/resources/js/iButton/css/jquery.ibutton.min.css">
		<link href="/resources/js/impromptu/jquery-impromptu.min.css" rel="stylesheet" type="text/css">
		<link href="/resources/js/impromptu/themes/base.css" rel="stylesheet" type="text/css" >

		<meta charset="UTF-8" />
		<link href="/resources/images/favicon.ico" type="image/x-icon" rel="shortcut icon"/>
		<link href="/resources/images/favicon.ico" type="image/x-icon" rel="icon"/>
		<script type='text/javascript' src='/resources/js/jsmd5.js'></script>
		<script type='text/javascript' src='/resources/js/php.default.min.js'></script>
		<script src="/resources/js/jquery-1.7.2.min.js"></script>
		<script src="/resources/js/jquery.badBrowser.js"></script>
		<script src="/resources/js/iButton/lib/jquery.ibutton.min.js"></script>
		<script src="/resources/js/iButton/lib/jquery.easing.1.3.js"></script>
		<script type="text/javascript" language="javascript" src="/resources/js/impromptu/jquery-impromptu.min.js"></script>

		<script type="text/javascript">
			$(document).ready(function() {
				$(".switch").iButton({
					resizeHandle: true,
					resizeContainer: true,
					duration: 250,
					easing: "swing"
				});
			});
			function highlight(){
				document.getElementById("name").focus();
				<?php
				if(isset($_SESSION['registerform']['success']) && $_SESSION['registerform']['success'] == true){
				?>
				$("#requestdiv").hide();
				<?php
				}
				?>
			}
			function submitcheck(){
				console.log("SUBMITCHECK");
				document.forms["form"].submit();
			}
			function unsubscribeall(){
				console.log("UNSUBSCRIBE");
				$.prompt("<h3>You are about to completely unsubscribe and terminate your email subscription with <?php echo $settings->base_url_pretty;?>. You will have to re-subscribe if you change your mind in the future.<br>Do you wish to proceed?</h3>",{
					title: "Unsubscribe and Remove Email",
					buttons: { Yes: 1, No: -1 },
					focus: 1,
					submit:function(e,v,m,f){
						e.preventDefault();
						if(v == 1){
							$.prompt.close();
							$("#unsubscribe").attr("checked",true);
							document.forms["form"].submit();
						}else{
							$.prompt.close();
						}
					}
				});
			}
		</script>
	</head>
<body>
<?php require($_SERVER['DOCUMENT_ROOT'].'/report/head_foot/default_header.php'); ?>
	<div class="outerContainer" id="content">
		<div style="width:100%; margin-top:20px; text-align:center;  margin-bottom:20px;">
			<h1 style="color:#F79447">Manage Email Subscription</h1>
		</div>
		<div style="width:50%; margin-left:auto; margin-right:auto;">
			<div style="text-align:center">
				<?php
				if (isset($_SESSION['registerform']['error']) &&  $_SESSION['registerform']['error'] == true): /* The last form submission had 1 or more errors */ ?>
					<span class="error">There was a problem with your submission.  Errors are displayed below in red.</span><br />
					<br />
				<?php elseif (isset($_SESSION['registerform']['success']) && $_SESSION['registerform']['success'] == true): /* form was processed successfully */ ?>
					<span class="success">Your email subscription settings have been changed.</span><br />
					<br />
				<?php endif; ?>
			</div>
			<div>
				<form id="form" name="form" method="post">
					<input type="hidden" name="do" value="contact" />
					<div style="text-align:center">
						<div style="float: left; width: 50%;">
							<div style="text-align:left; width:75%; margin-left:auto; margin-right:auto"><strong>Name*:</strong>&nbsp; &nbsp;<?php echo @$_SESSION['registerform']['name_error'] ?></div>
							<div style=" width:75%; margin-left:auto; margin-right:auto; margin-bottom:20px;">
								<input style="width:100%" type="text" id="name" name="name" maxlength="50" value="<?php if(!isset($_SESSION['registerform']['error'])){echo $data_first['name'];}else{echo htmlspecialchars(@$_SESSION['registerform']['name']);} ?>" />
							</div>
						</div>
						<div style="float: left; width: 50%;">
							<div style="text-align:left; width:75%; margin-left:auto; margin-right:auto"><strong>Email*:</strong>&nbsp; &nbsp;<?php echo @$_SESSION['registerform']['email_error'] ?></div>
							<div style=" width:75%; margin-left:auto; margin-right:auto; margin-bottom:20px;">
								<input style="width:100%" type="text" id="email" name="email" maxlength="50" value="<?php if(!isset($_SESSION['registerform']['error'])){echo $data_first['email'];}else{echo htmlspecialchars(@$_SESSION['registerform']['email']);} ?>" />
							</div>
						</div>
						<div style="margin-bottom:20px; margin-top: 20px; display: inline-block;">
							<table cellpadding="15">
								<thead>
									<tr align="left">
										<th>System ID</th>
										<th>System Nickname</th>
										<th>Receive Service Reports</th>
										<th>Receive Service Requests</th>
									</tr>
								</thead>
								<tbody>
									<?php
										foreach($data as $key=>$value){
											echo "<tr>";
											echo "<td align=\"left\">".$value['system_id']."</td>";
											echo "<td align=\"left\">".$value['nickname']."</td>";
											echo "<td align=\"center\"><input name=\"rcv_report[]\" data-role=\"flipswitch\" class=\"switch\" type=\"checkbox\" value=\"".$key."\" title=\"Report\"";
											if(strtolower($value['rcv_service_report']) == 'y'){echo " checked=\"checked\" ";}
											echo "/></td>";
											echo "<td align=\"center\"><input name=\"rcv_request[]\" data-role=\"flipswitch\" class=\"switch\" type=\"checkbox\" value=\"".$key."\" title=\"Request\"";
											if(strtolower($value['rcv_service_request']) == 'y'){echo " checked=\"checked\" ";}
											echo "/></td>";
											echo "</tr>";
										}
									?>
								</tbody>
							</table>
						</div>

						<p> <br />
							<input name="savebtn" id="submitbtn" type="button" value="Save Changes" class="button" onclick="submitcheck();" /> &emsp;
							<input name="unsubscribebtn" id="unsubscribebtn" type="button" value="Unsubscribe & Remove Email" class="button" onclick="unsubscribeall();" />
						</p>
					</div>
					<div style="display: none;">
						<input name="unsubscribe" id="unsubscribe" type="checkbox" value="unsubscribe" />
					</div>
				</form>
				</fieldset>
			</div>
		</div>
		<div style="width:100%; margin-top:50px; margin-bottom:60px">
			<div style="margin-left:auto; margin-right:auto; width:50%; text-align:left;"> This system should be accessed by authorized <?php echo $settings->company_name; ?> personnel and customers only. Unauthorized access or use of this computer system may subject violators to criminal, civil, and/or administrative action. </div>
		</div>
	</div>
<?php require($_SERVER['DOCUMENT_ROOT'].'/report/head_foot/default_footer.php'); ?>
<?php
// The form processor PHP code
function process_si_contact_form()
{
	global $log, $mysqli, $settings, $data, $data_first;

	d($_POST);

	if(isset($_POST['unsubscribe'])){
		$date = date(storef,time());
		$sql="UPDATE customers_email_list SET active = 'N', opt_out_date = '".$date."' WHERE email_unique_id = '".$data_first['email_unique_id']."';";
		s($sql);
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__));
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__));
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__));
		}
		$mysqli->close();
		header("location: unsubscribed.php");
		exit();
	}

	$_SESSION['registerform'] = array(); // re-initialize the form session data
	if ($_SERVER['REQUEST_METHOD'] == 'POST' && @$_POST['do'] == 'contact') {
		// if the form has been submitted

		//dd($_POST);

		foreach($_POST as $key => $value) {
			if (!is_array($_POST[$key])) {
				// sanitize the input data
				$_POST[$key] = htmlspecialchars(stripslashes(trim($value)));
			}
		}

		$name = $_POST['name'];    // name from the form
		$email = $_POST['email'];   // email from the form
		$rcv_report = $_POST['rcv_report'];  //report checkboxes
		$rcv_request = $_POST['rcv_request'];  //request checkboxes

		$name = substr($name, 0, 64);  // limit name to 64 characters

		$_SESSION['registerform']['name'] = $name;       // save name from the form submission
		$_SESSION['registerform']['email'] = $email;     // save email
		$_SESSION['registerform']['rcv_report'] = $rcv_report;     // save rcv_report
		$_SESSION['registerform']['rcv_request'] = $rcv_request;     // save rcv_request

		d($_POST);
		d($_SESSION);

		$errors = array();  // initialize empty error array


		if (strlen($name) < 4) {
			// name too short, add error
			$errors['name_error'] = 'Your name is required more than 3 letters';
		}

		if (strlen($email) == 0) {
			// no email address given
			$errors['email_error'] = 'Email address required';
		}else if ( !preg_match('/^(?:[\w\d]+\.?)+@(?:(?:[\w\d]\-?)+\.)+\w{2,4}$/i', $email)) {
			// invalid email format
			$errors['email_error'] = 'Email address entered is invalid';
		}

		if(preg_match('/[^a-zA-Z\. \-_0-9]/',$name)){
			// name invalid chars, add error
			$errors['name_error'] = 'Invalid characters. Use a-z 0-9 . - _ only';
		}

		if (sizeof($errors) == 0) {
			// no errors, send the form
			$_SESSION['registerform']['error'] = false;  // no error with form
			$_SESSION['registerform']['success'] = true; // message sent

			$date = date(storef,time());

			$sql="UPDATE customers_email_list SET
			name = '".$name."',
			email = '".$email."',
			edited_date = '".$date."'
			WHERE email_unique_id = '".$data_first['email_unique_id']."';";
			s($sql);
			if(!$result = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__));
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__));
				$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__));
			}

			foreach($data as $key=>$value){
				if(in_array($key, $rcv_report)){$report = 'Y';}else{$report = 'N';}
				if(in_array($key, $rcv_request)){$request = 'Y';}else{$request = 'N';}
				$sql="UPDATE customers_email_list SET rcv_service_report = '".$report."', rcv_service_request = '".$request."' WHERE id = ".$key.";";
				s($sql);
				if(!$result = $mysqli->query($sql)){
					$log->logerr($sql,1000,false,basename(__FILE__));
					$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__));
					$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__));
				}
			}
			get_data();
			$mysqli->close();
		} else {
			// save the entries, this is to re-populate the form
			$_SESSION['registerform']['name'] = $name;       // save name from the form submission
			$_SESSION['registerform']['email'] = $email;     // save email
			$_SESSION['registerform']['rcv_report'] = $rcv_report;     // save rcv_report
			$_SESSION['registerform']['rcv_request'] = $rcv_request;     // save rcv_request

			foreach($errors as $key => $error) {
				// set up error messages to display with each field
				$_SESSION['registerform'][$key] = "<span style=\"font-weight: bold; color: #f00\">$error</span>";
			}
			$_SESSION['registerform']['error'] = true; // set error floag
			//return false;
		}
	} // POST
}
function get_data(){
	global $log, $mysqli, $settings, $data, $data_first, $email_unique_id;

	$sql="SELECT cel.*, sbc.nickname
FROM customers_email_list AS cel
JOIN systems_base_cont AS sbc ON sbc.ver_unique_id = cel.system_ver_unique_id
WHERE cel.email_unique_id = '".$mysqli->real_escape_string($email_unique_id)."' 
AND sbc.property = 'c'
AND cel.active = 'Y';";
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__));
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__));
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__));
	}

	$data = array();
	while($row = $result->fetch_assoc()){
		$data[$row['id']] = $row;
	}
	d($data);

	if(count($data) == 0){
		$log->logerr('email_unique_id not found in db',4,false,basename(__FILE__));
		$log->logerr('Email address does not have a subscription registered.',1078,true,basename(__FILE__));
	}
	$data_first = reset($data);
}

function randomString($length) {
	$key = null;
	$keys = array_merge(range(0,9), range('a', 'z'), range('A', 'Z'));
	for($i=0; $i < $length; $i++) {
		$key .= $keys[array_rand($keys)];
	}
	return $key;
}

$_SESSION['registerform']['success'] = false; // clear success value after running