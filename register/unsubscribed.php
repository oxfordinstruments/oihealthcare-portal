<!DOCTYPE html>
	<html>
	<head>
		<style type="text/css">
			<!--
			.error {
				color: #f00;
				font-weight: bold;
				font-size: 1.2em;
			}
			.success {
				color: #00f;
				font-weight: bold;
				font-size: 1.5em;
			}
			fieldset {
				width: 90%;
			}
			legend {
				font-size: 24px;
			}
			.note {
				font-size: 18px;
			}
			-->
			.text-xs-center {
				text-align: center;
			}

			.g-recaptcha {
				display: inline-block;
			}
		</style>
		<title>Service Portal - Oxford Instruments</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link href="/resources/css/screen.min.css" rel="stylesheet" type="text/css" media="screen" />
		<link href="/resources/css/320.min.css" rel="stylesheet" type="text/css" />
		<link href="/resources/css/720.min.css" rel="stylesheet" type="text/css" />
		<link href="/resources/css/960.min.css" rel="stylesheet" type="text/css" />
		<link href="/resources/css/print.min.css" rel="stylesheet" type="text/css" media="print" />
		<!--[if lt IE 9 ]>
		<link href="/resources/css/screen-ie.min.css" rel="stylesheet" type="text/css" />
		<![endif]-->
		<!--[if IE 9 ]>
		<link href="/resources/css/screen-ie9.min.css" rel="stylesheet" media="screen" />
		<![endif]-->

		<meta charset="UTF-8" />
		<link href="/resources/images/favicon.ico" type="image/x-icon" rel="shortcut icon"/>
		<link href="/resources/images/favicon.ico" type="image/x-icon" rel="icon"/>
		<script type='text/javascript' src='/resources/js/jsmd5.js'></script>
		<script type='text/javascript' src='/resources/js/php.default.min.js'></script>
		<script src="/resources/js/jquery-1.7.2.min.js"></script>
		<script src="/resources/js/jquery.badBrowser.js"></script>

	</head>
<body>
<?php require($_SERVER['DOCUMENT_ROOT'].'/report/head_foot/default_header.php'); ?>
	<div class="outerContainer" id="content">
		<div style="width:100%; margin-top:20px; text-align:center;  margin-bottom:20px;">
			<h1 style="color:#F79447">Email Subscription Terminated</h1>
		</div>
		<div style="width:100%; margin-top:50px; margin-bottom:60px">
			<div style="margin-left:auto; margin-right:auto; width:50%; text-align:left;"> This system should be accessed by authorized <?php echo $settings->company_name; ?> personnel and customers only. Unauthorized access or use of this computer system may subject violators to criminal, civil, and/or administrative action. </div>
		</div>
	</div>
<?php require($_SERVER['DOCUMENT_ROOT'].'/report/head_foot/default_footer.php'); ?>
