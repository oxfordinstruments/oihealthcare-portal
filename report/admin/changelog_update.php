<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

$debug=false;
if(isset($_GET['debug'])){
	$debug=true;
}

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

$users = array();
$sql="SELECT u.uid, u.name
FROM users AS u
INNER JOIN users_groups AS ug ON ug.uid = u.uid
INNER JOIN users_group_id AS grid ON grid.id = ug.gid AND grid.`group` = 'grp_employee'
WHERE u.id >= 10
ORDER BY u.uid ASC;";
s($sql);
if(!$resultUsers = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
while($rowUsers = $resultUsers->fetch_assoc()){
	array_push($users,$rowUsers['name']);
}
d($users);

?>
<!DOCTYPE html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<?php require_once($js_include);?>
<script src="/resources/js/jquery.ui.datepicker.js"></script>
<script src="/resources/js/jquery-dynamic-form_MODIFIED.js"></script>
<script type="text/javascript">
	var users = [
		<?php 
			echo "'",implode("','",$users),"'";
		?>
	];
	$(document).ready(function(e) {
		$(function() {
			$( ".date" ).datepicker({
				numberOfMonths: 1,
				showButtonPanel: true,
				changeYear: true,
				constrainInput: true,
				duration: "fast",
				dateFormat: "<?php echo dpdispfd; ?>"
			});	
		});
		$( ".autocomp" ).autocomplete({
			source: users
		});
	});
	
	function send_check(){
		var ids = [];
		var errors = [];
		
		var error = false;

		if($("#date").val() == ""){ids.push('#date'); errors.push('Date');}
		if($("#branch").val() == ""){ids.push('#ver'); errors.push('Branch');}
		if($("#hash").val() == ""){ids.push('#hash'); errors.push('Commit Hash');}
		if($("#uid").val() == ""){ids.push('#uid'); errors.push('Name');}
		if($("#change").val() == ""){ids.push('#change'); errors.push('Changes');}
		if($("#tests").val() == ""){ids.push('#tests'); errors.push('Tests');}
		
		console.log("ids: " + ids);
		console.log("errors: " + errors);
		showErrors(ids,errors);
		
		if(ids.length <= 0){
			<?php if(!$_SESSION['mobile_device']){ ?>
				$.prompt("<h3>Update Changelog?</h3>",{
					title: "Update Changelog",
					buttons: { Yes: 1, No: -1 },
					focus: 1,
					submit:function(e,v,m,f){ 
						e.preventDefault();
						showErrors(ids,errors);
						$("#errors").hide();
						if(v == 1){
							$.prompt.close();
							document.forms["form"].submit();
						}else{
							$.prompt.close();
						}
					}
				});
			<?php }else{ ?>
				if(confirm("Update Changelog?")){
					document.forms["form"].submit();
				}
			<?php } ?>

		}
	}
	
	function showErrors(ids,errors){
		//thin solid #2C3594
//		$("label, input, select, .chosen-container").each(function(index, element) {
//			$(this).animate({
//				borderColor: "#2C3594",
//				boxShadow: 'none'
//			});
//		});
//		
//		$.each(ids,function(index,value){
//			$(value).animate({
//				borderColor: "#cc0000",
//				boxShadow: '0 0 5px 3px rgba(255,0,0,0.4)'
//			});
//		});
		$("#errors > span").html("");
		$.each(errors,function(index, value){
			$("#errors > span").append(value + "<br>");
		});
		$("#errors").show('slow');
		$(document).scrollTop(0);
	}
</script>

<style type="text/css">
textarea {
	width: 100%;
	height: 250px;
}
</style>
</head>
<body>
<?php require_once($header_include); ?>

<div id="OIReportContent">
	<div id="errors" style="text-align:center;display:none; margin-bottom:25px; font-size:18px;">
		<h2 style="margin:0px; padding:0px; font-size:24px;">Errors to fix</h2>
		<span style="color:#F00">
		</span>
	</div>
	<div style="text-align:center">
		<h1>Changelog Update</h1>
	</div>
	<div id="clFormDiv" style="width:75%; margin-left:auto; margin-right:auto; margin-top:15px;">
		<form id="form" method="post" action="changelog_update_do.php">
			<table width="100%" cellpadding="0" cellspacing="10">
				<tr>
					<td><label>Date<input type="text"  class="date" name="date" id="date" value="" /></label></td>
					<td><label>Branch<input type="text" name="branch" id="branch" value="" /></label></td>
					<td><label>Commit Hash<input type="text" name="hash" id="hash" value="" /></label></td>
					<td><label>Name<input type="text" id="uid" name="uid"  class="autocomp" /></label></td>
				</tr>
				<tr>
					<td colspan="4"><label>Changes Made<textarea name="change" id="change"></textarea></label></td>
				</tr>
				<tr>
					<td colspan="4"><label>Tests Performed<textarea name="tests" id="tests"></textarea></label></td>
				</tr>
				<tr>
					<td colspan="4"><input type="button" id="send_btn" name="send_btn" onClick="send_check();" value="Update" /></td>
				</tr>
			</table>
			<?php if($debug){ ?>
				<input type="hidden" id="debug" name="debug" value="Y" />
			<?php } ?>

		</form>
	</div>
</div>

<?php require_once($footer_include); ?>