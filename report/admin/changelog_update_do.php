<?php
$debug = false;
if(strtolower($_POST['debug']) == 'y'){
	$debug = true;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();


require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

d($_POST);



$sql="INSERT INTO changelog (`date`, `branch`, commit_hash, `change`, `tests`, uid) VALUES(
'".date(storef,strtotime($_POST['date']))."',
'".$mysqli->real_escape_string($_POST['branch'])."',
'".$mysqli->real_escape_string($_POST['hash'])."',
'".$mysqli->real_escape_string($_POST['change'])."',
'".$mysqli->real_escape_string($_POST['tests'])."',
'".$mysqli->real_escape_string($_POST['uid'])."'
);";
s($sql);
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

?>


<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
<script type="text/javascript">
function delayer(){
    <?php if(!$debug){ ?>
	window.location = "changelog_update.php";
	<?php } ?>
}
</script>
</head>
<body onLoad="setTimeout('delayer()', 100)">
<h1><span class="red">Page will return now</span></h1>
</body>
</html>