<?php
$debug = false;

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();


require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

d($_POST);



$sql="SELECT * FROM changelog ORDER BY id DESC;";
s($sql);
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

?>


<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>OiHealthcare Portal Change Log</title>
<style type="text/css">
.header {
	font-size: large;
	font-weight: bold;
	margin-left: 100px;
	margin-top: 10px;
}
.info {
	padding-left: 10px;
	font-size: small;
	font-style: italic;
	margin-left: 100px;
}
.sect {
	margin-left: 100px;
	padding-left: 30px;
	font-weight: bold;
}
pre {
	display: inline;
}
.data {
	padding-left: 60px;
	margin-bottom: 2px;
	margin-left: 100px;
	font-weight: bold;
}
.title {
	font-size: xx-large;
	font-weight: bold;
	margin-left: 75px;
	margin-bottom: 2px;
}
</style>
</head>
<body>
<div class="title">Changelog</div>

<?php
while($row = $result->fetch_assoc()){
	echo "<div class='header'>".date(phpdispfd,strtotime($row['date']))." - ".$row['version']."</div>";
	echo "<div class='info'>Commit Hash: ".$row['commit_hash']."&emsp;&emsp;User: ".$row['uid']."</div>";
	echo "<div class='sect'>Changes:<br></div><div class='data'><pre>".$row['change']."</pre></div>";
	echo "<div class='sect'>Tests:<br></div><div class='data'><pre>".$row['tests']."</pre></div>";
	
	
}
?>

<!--<div class='header'>".date(phpdispfd,strtotime($row['date']))." - ".$row['version']."</div>
<div class='info'>Commit Hash: ".$row['commit_hash']."&emsp;&emsp;User: ".$row['uid']."</div>
<div class="sect">Changes:<br></div>
<div class='data'><pre>".$row['change']."</pre></div>
<div class="sect">Tests:<br></div>
<div class='data'><pre>".$row['tests']."</pre></div>-->

</body>
</html>