<?php
//Update Completed 11/25/14
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

$debug=false;
if(isset($_GET['debug'])){
	$debug=true;
}

?>
<!DOCTYPE html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<?php require_once($js_include);?>
<script type="text/javascript">
	$(document).ready(function(e) {
		$("#role_employee").change(function(e) {
			if($("#role_employee").is(":checked")){
				$(".cls_employee").attr("checked", false);	
			}
		});
		$(".cls_employee").change(function(e) {
			if($(".cls_employee").is(":checked")){
				$("#role_employee").attr("checked", false);	
			}
		});
	});
	
	function send_check(){
		var ids = [];
		var errors = [];
		
		var error = false;
		if($("#email_form input:checkbox:checked").length == 0){
			$('#email_form input:checkbox').each(function(index, element) {
				ids.push(this.id); 
			});
			errors.push('Select a user group');
		}
		if($("#subject").val() == ""){
			ids.push('#subject');
			errors.push('Subject is blank');	
		}
		
		if($("#text").val() == ""){
			ids.push('#text');
			errors.push('Body is blank');	
		}
		
		console.log("ids: " + ids);
		console.log("errors: " + errors);
		showErrors(ids,errors);
		
		if(ids.length <= 0){
			<?php if(!$_SESSION['mobile_device']){ ?>
				$.prompt("<h3>Send Emails?</h3>",{
					title: "Send Emails",
					buttons: { Yes: 1, No: -1 },
					focus: 1,
					submit:function(e,v,m,f){ 
						e.preventDefault();
						showErrors(ids,errors);
						$("#errors").hide();
						if(v == 1){
							$.prompt.close();
							document.forms["email_form"].submit();
							$("#email_form")[0].reset();
						}else{
							$.prompt.close();
						}
					}
				});
			<?php }else{ ?>
				if(confirm("Send Emails?")){
					document.forms["email_form"].submit();
				}
			<?php } ?>

		}
	}
	
	function preview_email(){
		$("#preview").val('Y');
		document.forms['email_form'].target='_blank';
		document.forms['email_form'].submit();
		$("#preview").val('N');
	}
	
	function showErrors(ids,errors){
		//thin solid #2C3594
//		$("label, input, select, .chosen-container").each(function(index, element) {
//			$(this).animate({
//				borderColor: "#2C3594",
//				boxShadow: 'none'
//			});
//		});
//		
//		$.each(ids,function(index,value){
//			$(value).animate({
//				borderColor: "#cc0000",
//				boxShadow: '0 0 5px 3px rgba(255,0,0,0.4)'
//			});
//		});
		$("#errors > span").html("");
		$.each(errors,function(index, value){
			$("#errors > span").append(value + "<br>");
		});
		$("#errors").show('slow');
		$(document).scrollTop(0);
	}
</script>

<style type="text/css">
#subject {
	width: 100%;
}
#text {
	width: 100%;
	height: 250px;
}
</style>
</head>
<body>
<?php require_once($header_include); ?>

<div id="OIReportContent">
	<div id="errors" style="text-align:center;display:none; margin-bottom:25px; font-size:18px;">
		<h2 style="margin:0px; padding:0px; font-size:24px;">Errors to fix</h2>
		<span style="color:#F00">
		</span>
	</div>
	<div style="text-align:center">
		<h1>Email Portal Users</h1>
	</div>
	<div id="emailFormDiv" style="width:75%; margin-left:auto; margin-right:auto; margin-top:15px;">
		<form id="email_form" method="post" action="email_users_do.php">
			<strong>Select the group(s) to receive the email:</strong>
			<br>
			<br>
			<label><input type="checkbox" name="role[role_employee]" id="role_employee">Employees</label>
			<br>
			&emsp;&emsp;
			<label><input type="checkbox" name="role[role_dispatcher]" id="role_dispatcher" class="cls_employee">Dispatchers</label>
			<label><input type="checkbox" name="role[role_engineer]" id="role_engineer" class="cls_employee">Engineers</label>
			<label><input type="checkbox" name="role[role_management]" id="role_management" class="cls_employee">Management</label>
			<label><input type="checkbox" name="role[role_finance]" id="role_finance" class="cls_employee">Finance</label>
			<label><input type="checkbox" name="role[role_sales]" id="role_sales" class="cls_employee">Sales</label>
			<label><input type="checkbox" name="role[role_sales_manager]" id="role_sales_manager" class="cls_employee">Sales Manager</label>
			<label><input type="checkbox" name="role[role_quality]" id="role_quality" class="cls_employee">Quality</label>
			<label><input type="checkbox" name="role[role_basic]" id="role_basic" class="cls_employee">Basic</label>
			<br>
			<label><input type="checkbox" name="role[role_contractor]" id="role_contractor">Contractors</label>
			<br>
			<label><input type="checkbox" name="role[role_customer]" id="role_customer">Customers</label>
			<br>
			<br>
			<label>Subject<input type="text" name="subject" id="subject" value="" /></label>
			<br>
			<br>
			<label>Body<textarea name="text" id="text"></textarea></label>
			<br>
			<br>
			<input type="button" id="send_btn" name="send_btn" onClick="send_check();" value="Send Emails" />
			<input type="button" id="preview_btn" name="preview_btn" onClick="preview_email();" value="Preview Email" />
			
			
			<input type="hidden" id="preview" name="preview" value="N" />
			<?php if($debug){ ?>
				<input type="hidden" id="debug" name="debug" value="Y" />
			<?php } ?>
		</form>
	</div>
</div>

<?php require_once($footer_include); ?>