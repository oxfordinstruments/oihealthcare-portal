<?php
$debug = false;
if(strtolower($_POST['debug']) == 'y'){
	$debug = true;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();


require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

d($_POST);

$send = true;
if(intval($settings->disable_email) == 1){
	$send = false;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/smarty/libs/Smarty.class.php');
$smarty = new Smarty();
$smarty->setTemplateDir($_SERVER['DOCUMENT_ROOT'].$settings->email_templates);
$smarty->setCompileDir($_SERVER['DOCUMENT_ROOT'].$settings->email_template_c);
$smarty->setCacheDir($_SERVER['DOCUMENT_ROOT'].$settings->email_cache);
$smarty->setConfigDir($_SERVER['DOCUMENT_ROOT'].$settings->email_configs);
$smarty->use_sub_dirs = true;
$smarty->force_compile = true;
//$smarty->debugging = true;
$smarty->caching = false;

$preview = false;
if(strtolower($_POST['preview']) == 'y'){
	$preview = true;	
}

$smarty->assign('subject',$_POST['subject']);
$smarty->assign('text',$_POST['text']);
$smarty->assign('company_name',$settings->company_name);
$smarty->assign('copyright_date',$settings->copyright_date);
$smarty->assign('email_pics',$settings->email_pics);
$smarty->assign('oih_url',$settings->full_url);

$roles = "'". implode("','", array_keys($_POST['role'])). "'";
s($roles);

$sql="SELECT u.name, u.email, u.uid
FROM users AS u
LEFT JOIN users_roles AS roles ON roles.uid = u.uid
LEFT JOIN users_role_id AS rid ON rid.id = roles.rid
WHERE rid.role IN($roles)
AND u.id >= 10 AND u.email != '' AND u.active = 'y'
GROUP BY u.email
ORDER BY u.name ASC;";
s($sql);
$emails = array();
if(!$resultEmail = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
while($rowEmail = $resultEmail->fetch_assoc()){
	array_push($emails,array("email"=>$rowEmail['email'],"name"=>$rowEmail['name']));
}

if(isset($_POST['role']['role_employee'])){
	$sql="SELECT u.name, u.email, u.uid
	FROM users AS u
	LEFT JOIN users_groups AS ug ON ug.uid = u.uid
	WHERE ug.gid = 1 AND u.id >= 10 AND u.email != '' AND u.active = 'y'
	GROUP BY u.email
	ORDER BY u.name ASC;";	
	if(!$resultEmail = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	while($rowEmail = $resultEmail->fetch_assoc()){
		array_push($emails,array("email"=>$rowEmail['email'],"name"=>$rowEmail['name']));
	}
}
d($emails);

//
// Die here is just previewing email and not debugging
//
if($preview){
	echo $smarty->fetch('admin_email.tpl');
	echo '<div style="overflow:scroll;">';
	echo "<h1>Email Receipents Outlook Format: </h1><br>";
	$emails_address = array();
	foreach($emails as $data){
		array_push($emails_address, $data['email']);
	}
	echo wordwrap(implode('; ',$emails_address),200,"<br>",false),"<br><br>";
	echo "</div>";
	echo "<div>";
	echo "<h1>Email Receipents By Name: </h1><br>";
	foreach($emails as $data){
		echo "Name: ",$data['name'],"&emsp;Email: ",$data['email'],"<br>";	
	}
	echo "</div>";
	die();
}

if($debug){
	$emails = array(array('email'=>(string)$settings->email_support, 'name'=>'Support'));	
}

//load phpmailer
require_once($_SERVER['DOCUMENT_ROOT'].'/resources/PHPMailer/PHPMailerAutoload.php');

//Engineer Request Email
$mail = new PHPMailer;
$mail->IsSMTP();
$mail->Host = (string)$settings->email_host;
$mail->SMTPAuth = true;
$mail->Username = (string)$settings->email_support;
$mail->Password = (string)$settings->email_password;
$mail->SMTPSecure = 'tls';
$mail->From = (string)$settings->email_support;
$mail->FromName = 'Announcements '.$settings->short_name.' Portal';
foreach($emails as $email){
	$mail->AddAddress(trim($email['email']),$email['name']);  // Add a recipient
	$email_result .= "Email sent to: ".$email['email']."\n";
}
$mail->AddReplyTo((string)$settings->email_support, 'Support '.$settings->short_name.' Portal');
$mail->AddCC((string)$settings->email_support);
$mail->IsHTML(true);
$mail->Subject = (string)$_POST['subject'];
$mail->Body = $smarty->fetch('admin_email.tpl');

d($mail);

if($debug){
	echo "Subject: ",$mail->Subject,EOL;
	echo $mail->Body,EOL,"<hr>",EOL;	
}

if($send){
	if(!$mail->Send()) {
	   echo 'Email could not be sent.';
	   $email_result = "Email could not be sent!";
	   echo 'Mailer Error: ' . $mail->ErrorInfo;
	   $log->logerr($mail->ErrorInfo,1053,false,basename(__FILE__),__LINE__);
	   exit;
	}else{
		$log->loginfo( $row['id'].' '. implode('; ',array_keys($mail->getAllRecipientAddresses())),200,false,basename(__FILE__),__LINE__);
	}
}else{
	echo "Debug: Not sending email.<br />";	
}
echo "<hr>";
echo "<br /><pre>Email Result\n";
echo $email_result;
echo "<br /></pre>";

?>

