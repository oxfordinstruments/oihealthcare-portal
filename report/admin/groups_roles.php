<?php
$debug = false;

require_once($_SERVER['DOCUMENT_ROOT'] . '/report/common/session_control.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'] . '/resources/kint/Kint.class.php');
if (!$debug) {
	Kint::enabled(false);
}

if (!isset($_GET['group'])) {
	$log->logerr('Group not in get', 1067, false, basename(__FILE__), __LINE__);
	die('Group not valid');
}

//Get Group Info
$sql = "SELECT * FROM users_group_id WHERE id = " . $_GET['group'] . ";";
if (!$resultGroup = $mysqli->query($sql)) {
	$log->logerr($sql, 1000, false, basename(__FILE__), __LINE__);
	$log->logerr('There was error running the query [' . $mysqli->error . ']', 1000, false, basename(__FILE__), __LINE__);
	$log->logerr('Error occurred, contact support.', 1000, true, basename(__FILE__), __LINE__);
}
$rowGroup = $resultGroup->fetch_assoc();
d($rowGroup);

//Get all roles
$sql = "SELECT * FROM users_role_id ORDER BY name ASC;";
if (!$resultAllRoles = $mysqli->query($sql)) {
	$log->logerr($sql, 1000, false, basename(__FILE__), __LINE__);
	$log->logerr('There was error running the query [' . $mysqli->error . ']', 1000, false, basename(__FILE__), __LINE__);
	$log->logerr('Error occurred, contact support.', 1000, true, basename(__FILE__), __LINE__);
}

$all_roles = array();
while ($rowAllRoles = $resultAllRoles->fetch_assoc()) {
	$all_roles[$rowAllRoles['id']] = array('name' => $rowAllRoles['name'], 'role' => $rowAllRoles['role'], 'description' => $rowAllRoles['description']);
}
d($all_roles);

//Get current selected group's roles
$sql = "SELECT ugr.rid, uri.name
FROM users_group_roles AS ugr
LEFT JOIN users_role_id AS uri ON uri.id = ugr.rid 
WHERE ugr.gid = '" . $_GET['group'] . "' ORDER BY ugr.rid;";
if (!$resultCurRoles = $mysqli->query($sql)) {
	$log->logerr($sql, 1000, false, basename(__FILE__), __LINE__);
	$log->logerr('There was error running the query [' . $mysqli->error . ']', 1000, false, basename(__FILE__), __LINE__);
	$log->logerr('Error occurred, contact support.', 1000, true, basename(__FILE__), __LINE__);
}
$cur_roles = array();
while ($rowCurRoles = $resultCurRoles->fetch_assoc()) {
	$cur_roles[$rowCurRoles['rid']] = $rowCurRoles['name'];
}
d($cur_roles);


?>

<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>Untitled Document</title>
	<script src="/resources/js/underscore.js"></script>
	<script src="/resources/js/jquery/jquery.min.js"></script>

	<script type="text/javascript">
		var cur_roles = _.values(<?php echo json_encode($cur_roles) ?>);
		console.log(cur_roles);

		function submit_check(){

			var data = $('input[type="checkbox"]:checked').serializeArray(),
				obj = [];
			console.log(data);

			for(var i = 0; i < data.length; i++){
				obj.push(data[i].value);
			}

			console.log(obj);
			var roles_removed = _.difference(cur_roles, obj);
			console.log(roles_removed);

			if(roles_removed.length > 0){
//				if(confirm("You have removed a role that was assigned. This possibly will cause problems with users. Proceed?")){
//					document.forms['form'].submit();
//				}
				alert("Cannot submit with a previously assigned role un-checked.");
			}else{
				document.forms['form'].submit();
			}

		}
	</script>
</head>
<body>
<h1>Roles for <?php echo $rowGroup['name']; ?></h1>
<form id="form" name="form" method="post" action="groups_roles_save.php">
	<input type="button" name="Submit" onclick="submit_check();" value="Save Roles Selection"/>
	<br>
	<table border="1" cellpadding="1" cellspacing="1">
		<th>Selected</th>
		<th>Roles Name</th>
		<th>Description</th>
		<?php
		foreach ($all_roles as $key => $value) {
			echo '<tr>';
			echo "<td><input type=\"checkbox\" name=\"roles[" . $key . "]\" value=\"" . $value['name'] . "\"";
			if (!empty($cur_roles[$key])) {
				echo " checked ";
			}
			echo " />";
			echo "<td>" . $value['name'] . "</td>";
			echo "<td>" . $value['description'] . "</td>";
			echo "</tr>\n";
		}
		?>
	</table>
	<br>
	<input type="hidden" name="group" value="<?php echo $_GET['group']; ?>"/>
	<input type="hidden" name="orig_roles" value='<?php echo serialize($cur_roles); ?>'/>
	<input type="button" name="Submit" onclick="submit_check();" value="Save Roles Selection"/>
</form>
</body>
</html>