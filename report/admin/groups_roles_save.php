<?php
$debug = false;

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();
require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

$orig_roles = unserialize($_POST['orig_roles']);
d($orig_roles);

d(array_diff($orig_roles, $_POST['roles']));

dd($_POST);

if(!isset($_POST['group'])){
	$log->logerr('Group not in post',1067,false,basename(__FILE__),__LINE__);
	die('Group not valid');
}

$sql="DELETE FROM users_group_roles WHERE gid = ".$_POST['group'].";";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

echo "Setting Roles:<br>";

if(count($_POST['roles']) >= 1){

	$sql="INSERT INTO users_group_roles (gid,rid) VALUES";
	
	foreach($_POST['roles'] as $key=> $role){
		echo "&emsp;",$role,"<br>";
		//$sql .= "($key,".$_POST['group']."),";
		$sql .= "(".$_POST['group'].", $key),";
	}
	$sql = rtrim($sql, ',');
	$sql .= ";";
	s($sql);
	
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}

}else{
	echo "&emsp;none<br>";	
}
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
<script type="text/javascript">
function delayer(){
    <?php if(!$debug){ ?>
	window.location = "manage_groups.php"
	<?php } ?>
}
</script>
</head>
<body onLoad="setTimeout('delayer()', 3000)">
<h1><span class="red">Page will return in 3 seconds</span></h1>
</body>
</html>