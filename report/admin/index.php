<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

$sql="SELECT
(SELECT COUNT(id) AS `count` FROM systems_base_cont WHERE property = 'C') AS systems_no_archive,
(SELECT COUNT(id) AS `count` FROM systems_base_cont WHERE property = 'A') AS systems_archive,
(SELECT COUNT(id) AS `count` FROM facilities WHERE property = 'C') AS facilities_no_archive,
(SELECT COUNT(id) AS `count` FROM facilities WHERE property = 'A') AS facilities_archive,
(SELECT COUNT(id) AS `count` FROM customers WHERE property = 'C') AS customers_no_archive,
(SELECT COUNT(id) AS `count` FROM customers WHERE property = 'A') AS customers_archive,
(SELECT COUNT(id) AS `count` FROM users WHERE `active`='y') AS users_active,
(SELECT COUNT(id) AS `count` FROM users WHERE `active`='n') AS users_inactive,
(SELECT COUNT(id) AS `count` FROM systems_reports WHERE `status`='open') AS systems_reports_open,
(SELECT COUNT(id) AS `count` FROM systems_requests WHERE `status`='open' AND `deleted` = 'n') AS systems_requests_open,
(SELECT COUNT(u.id) AS `count` FROM users as u 
LEFT JOIN users_groups as ug on ug.uid = u.uid
LEFT JOIN users_group_id as gid on gid.id = ug.gid
WHERE gid.`group` = 'grp_employee') AS users_grp_employee,
(SELECT COUNT(u.id) AS `count` FROM users as u 
LEFT JOIN users_groups as ug on ug.uid = u.uid
LEFT JOIN users_group_id as gid on gid.id = ug.gid
WHERE gid.`group` = 'grp_contractor') AS users_grp_contractor,
(SELECT COUNT(u.id) AS `count` FROM users as u 
LEFT JOIN users_groups as ug on ug.uid = u.uid
LEFT JOIN users_group_id as gid on gid.id = ug.gid
WHERE gid.`group` = 'grp_customer') AS users_grp_customer;";
if(!$result=$mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowStats = $result->fetch_assoc();

 
$myusername=$_SESSION["login"];
$sql="SELECT * FROM users WHERE `uid`='$myusername';";
if(!$resultUser=$mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowUser = $resultUser->fetch_assoc();
 
?>
<!DOCTYPE html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<link rel="stylesheet" type="text/css" href="/resources/js/syntaxhighlighter/styles/shCore.css">
<link rel="stylesheet" type="text/css" href="/resources/js/syntaxhighlighter/styles/shCoreDefault.css">
<link rel="stylesheet" type="text/css" href="/resources/js/syntaxhighlighter/styles/shThemeDefault.css">
<!--<link rel="stylesheet" type="text/css" href="/resources/js/jquery-mobile-flip-switch/jquery.mobile.custom.structure.min.css">
<link rel="stylesheet" type="text/css" href="/resources/js/jquery-mobile-flip-switch/jquery.mobile.custom.theme.min.css">
-->
<?php require_once($js_include);?>
<script src="/resources/js/syntaxhighlighter/scripts/shCore.js"></script>
<script src="/resources/js/syntaxhighlighter/scripts/shBrushPlain.js"></script>
<!--<script src="/resources/js/jquery-mobile-flip-switch/jquery.mobile.custom.min.js"></script>
-->
<script type="text/javascript">
$(document).ready(function() {
 	
/////////////////////////////////////////////////////////////////////////////////////		
		<?php
		if(strtolower($_SESSION['prefs']['pref_show_help']) != 'y' or $_SESSION['mobile_device'] == true){
		?>
		$('#tiplayer').hide()
		<?php
			}
		?> 		
});
</script>
<script type="text/javascript">
	$(document).ready(function(){
		if(<?php if($settings->close_site == 1){echo 'false';}else{echo 'true';} ?>){
			$("#closeSiteStatus").val('Site Open');
			$("#closeSiteBtn").val('Close Site');
		}else{
			$("#closeSiteStatus").val('Site Closed');
			$("#closeSiteBtn").val('Open Site');	
		}
		
		last50logins = $('#last50loginsTable').dataTable({
			"bJQueryUI": true,
			//"bStateSave": true,
			"iCookieDuration": 60*60*24*365, // 1 year
			"sPaginationType": "full_numbers",
			"aaSorting": [],
			"fnInitComplete": function () {
           		 this.$('tr').click( function () {
               		 var href = $(this).find("a").attr("href");
					 if(href) {
						window.location = href;
					 }
           		 });
       		 }
		});
		
		failedLogins = $('#failedLoginsTable').dataTable({
			"bJQueryUI": true,
			//"bStateSave": true,
			"iCookieDuration": 60*60*24*365, // 1 year
			"sPaginationType": "full_numbers",
			"aaSorting": [],
			"fnInitComplete": function () {
           		 this.$('tr').click( function () {
               		 var href = $(this).find("a").attr("href");
					 if(href) {
						window.location = href;
					 }
           		 });
       		 }
		});
		
		banList = $('#banListTable').dataTable({
			"bJQueryUI": true,
			//"bStateSave": true,
			"iCookieDuration": 60*60*24*365, // 1 year
			"sPaginationType": "full_numbers",
			"aaSorting": [],
			"fnInitComplete": function () {
           		 this.$('tr').click( function () {
               		 var href = $(this).find("a").attr("href");
					 if(href) {
						window.location = href;
					 }
           		 });
       		 }
		});
		
		neverLoggedIn = $('#neverLoggedInTable').dataTable({
			"bJQueryUI": true,
			//"bStateSave": true,
			"iCookieDuration": 60*60*24*365, // 1 year
			"sPaginationType": "full_numbers",
			"aaSorting": []
		});
		
		neverLoggedInDMS = $('#neverLoggedInDMSTable').dataTable({
			"bJQueryUI": true,
			//"bStateSave": true,
			"iCookieDuration": 60*60*24*365, // 1 year
			"sPaginationType": "full_numbers",
			"aaSorting": []
		});
		
		
		errorLog = $('#errorLogTable').dataTable({
			"bJQueryUI": true,
			//"bStateSave": true,
			"iCookieDuration": 60*60*24*365, // 1 year
			"sPaginationType": "full_numbers",
			"aaSorting": []
		});
		
		$('.clearlog').click(function(event) {
			event.preventDefault();
			if(confirm('Really clear the log file? This cannot be undone!!!!!')){			 
				$.ajax(this.href, {
					success: function(data) {
						if(trim(data) == '1'){
							alert("Log Cleared");
						}else{
							alert("Failed to clear log");
							alert("Return Data:" + data);
						}
					},
					error: function() {
						alert("Clear log ajax failed");
					}
				});
			}
		});
		
	});
</script>

<script type="text/javascript">
	function bancheck(t, v){
		if(v.toLowerCase() === "root" || v == "69.27.61.60"){
			alert("Root cannot be banned");
			return;	
		}
		if(confirm("Are you sure you want to ban the " + t + ": " + v + " ?")){
			switch(t)
			{
			case "IP":
			  window.location.assign("/report/ban.php?ip=" + v);
			  break;
			case "User":
			  window.location.assign("/report/ban.php?user=" + v);
			  break;
            case "Domain":
			  window.location.assign("/report/ban.php?domain=" + v);
			  break;
			default:
			  window.location.assign("/error.php?n=1015&t="+t+"&p=admin.php");
			}	
		}
	}
	/////////////////////////////////////////////////////////////////////////////////////	
	function unbancheck(t, v){
		if(confirm("Are you sure you want to remove the ban for " + t + ": " + v + " ?")){			
			  window.location.assign("/report/ban.php?unban=" + v);	
		}
	}
	/////////////////////////////////////////////////////////////////////////////////////
	function closesitecheck(){
		if(document.getElementById("closeSiteStatus").value == "Site Open"){
			if(!confirm("Only approved users will be allowed to login.\nClose Site?")){
				return true;					
			}
			if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			}else{// code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange=function(){
				if (xmlhttp.readyState==4 && xmlhttp.status==200){
					alert(xmlhttp.responseText);
				}			
			}
			xmlhttp.open("GET","close_site.php?close=true",false);
			xmlhttp.send();
		}else{
			if(!confirm("All active users be allowed to login.\nOpen Site?")){
				return true;					
			}
			if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			}else{// code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange=function(){
				if (xmlhttp.readyState==4 && xmlhttp.status==200){
					alert(xmlhttp.responseText);
				}			
			}
			xmlhttp.open("GET","close_site.php?close=false",false);
			xmlhttp.send();
		}
	}
	/////////////////////////////////////////////////////////////////////////////////////
	function removeSystemDo(){
		if(document.getElementById("fromSystemId").value == ""){
			alert("From System ID cannot be blank");
			return true;
		}
		if(document.getElementById("toSystemId").value == ""){
			alert("To System ID cannot be blank");
			return true;
		}
		if(!confirm("Are you sure you want to remove this system?")){
			return true;					
		}
		var fromid = document.getElementById("fromSystemId").value;
		var toid = document.getElementById("toSystemId").value;
		window.open("/report/scripts/remove_system.php?from_system_id="+fromid+"&to_system_id="+toid);
	}

	function changeSystemIdDo() {
		if(document.getElementById("changeFromSystemId").value == ""){
			alert("From System ID cannot be blank");
			return true;
		}
		if(document.getElementById("changeToSystemId").value == ""){
			alert("To System ID cannot be blank");
			return true;
		}
		if(!confirm("Are you sure you want to change this system ID?")){
			return true;
		}
		var fromid = document.getElementById("changeFromSystemId").value;
		var toid = document.getElementById("changeToSystemId").value;
		window.open("/report/scripts/change_system_id.php?from_system_id="+fromid+"&to_system_id="+toid);
	}
	/////////////////////////////////////////////////////////////////////////////////////
	function renameUserDo() {
		if(document.getElementById("fromUID").value == ""){
			alert("From UID cannot be blank");
			return true;
		}
		if(document.getElementById("toUID").value == ""){
			alert("To UID cannot be blank");
			return true;
		}
		if(!confirm("Are you sure you want to rename this user?")){
			return true;
		}
		var fromid = document.getElementById("fromUID").value;
		var toid = document.getElementById("toUID").value;
		window.open("/report/scripts/rename_user.php?fromuid="+fromid+"&touid="+toid);
	}
</script>


</head>
<body>
<?php require_once($header_include); ?>
<div id="OIReportContent"> 
	<div id="ReportOuterFrame">
		<div id="ReportFrameTitle"> Administration </div>
			<div id="stat" style="width:50%; text-align:center; float:left">
				<div style="width:100%;"><h3>Stats</h3></div>
				<table width="100%" border="1" class="bodytext">
					<tr>
						<td width="60%">Users Active</td>
						<td align="center"><?php echo $rowStats['users_active']; ?></td>
					</tr>
					<tr>
						<td>Users Inactive</td>
						<td align="center"><?php echo $rowStats['users_inactive']; ?></td>
					</tr>
					<tr>
						<td>Users Employees</td>
						<td align="center"><?php echo $rowStats['users_grp_employee']; ?></td>
					</tr>
					<tr>
						<td>Users Customers</td>
						<td align="center"><?php echo $rowStats['users_grp_customer']; ?></td>
					</tr>
					<tr>
						<td>Users Contractors</td>
						<td align="center"><?php echo $rowStats['users_grp_contractor']; ?></td>
					</tr>
					<tr>
						<td>Customers Un-Archived</td>
						<td align="center"><?php echo $rowStats['customers_no_archive']; ?></td>
					</tr>
					
					<tr>
						<td>Cstomers Archived</td>
						<td align="center"><?php echo $rowStats['customers_archive']; ?></td>
					</tr>
					<tr>
						<td>Facilities Un-Archived</td>
						<td align="center"><?php echo $rowStats['facilities_no_archive']; ?></td>
					</tr>
					
					<tr>
						<td>Facilities Archived</td>
						<td align="center"><?php echo $rowStats['facilities_archive']; ?></td>
					</tr>
					<tr>
						<td>Systems Un-Archived</td>
						<td align="center"><?php echo $rowStats['systems_no_archive']; ?></td>
					</tr>
					
					<tr>
						<td>Systems Archived</td>
						<td align="center"><?php echo $rowStats['systems_archive']; ?></td>
					</tr>
					<tr>
						<td>Open Requests</td>
						<td align="center"><?php echo $rowStats['systems_requests_open']; ?></td>
					</tr>
					<tr>
						<td>Open Reports</td>
						<td align="center"><?php echo $rowStats['systems_reports_open']; ?></td>
					</tr>   
					<tr>
						<td>Portal Database Inuse</td>
						<td align="center"><?php echo $db_name; ?></td>
					</tr> 
					<tr>
						<td>DMS Database Inuse</td>
						<td align="center"><?php echo $db_name_dms; ?></td>
					</tr>    
				</table>
		</div>
		<div id="More" style="float:left; width:50%; text-align:center;">
			<div style="width:100%;"><h3>Links</h3></div>
			<table width="100%" border="1" cellpadding="1" cellspacing="1" class="bodytext">
				<tr>
					<td><a href="changelog_update.php">Changelog Update</a></td>
				</tr>
				<tr>
					<td><a href="changelog_view.php">View Changelog</a></td>
				</tr>
				<tr>
					<td><a href="/report/common/reports/user_permissions_list.php" target="new">User Permisions List</a></td>
				</tr>
				<tr>
					<td><a href="/log/show_all_logs.php" target="new">Show All Logs</a></td>
				</tr>
				<tr>
					<td><a href="/log/showlog.php?file=messagelog.txt" target="new">Message Log</a></td>
				</tr>
				<tr>
					<td><a href="/log/showlog.php?file=infolog.txt" target="new">Info Log</a></td>
				</tr>
				<tr>
					<td><a href="/log/showlog.php?file=errorlog.txt" target="new">Error Log</a></td>
				</tr>
				<tr>
					<td><a href="/log/showlog.php?file=ldaplog.txt" target="new">LDAP Log</a></td>
				</tr>
				<tr>
					<td>Clear Logs:&emsp;
						<a href="/log/clearlog.php?file=messagelog" class="clearlog">Message Log</a>&emsp;
						<a href="/log/clearlog.php?file=infolog" class="clearlog">Info Log</a>&emsp;
						<a href="/log/clearlog.php?file=errorlog" class="clearlog">Error Log</a>&emsp;
						<a href="/log/clearlog.php?file=ldaplog" class="clearlog">LDAP Log</a>
					</td>
				</tr>
				<tr>
					<td><a href="manage_systems_types.php" target="new">Manage Systems Types</a></td>
				</tr>
				<tr>
					<td><a href="manage_groups.php" target="new">Manage Groups's Roles</a></td>
				</tr>
				<tr>
					<td><a href="manage_roles.php" target="new">Manage Role's Permissions</a></td>
				</tr>
				<tr>
					<td><a href="/report/scripts/update_ldap.php" target="new">Update LDAP server manualy</a></td>
				</tr>
				<tr>
					<td><a href="email_users.php">Email Users</a></td>
				</tr>
			</table>
		</div>
		<div id="clear" style="clear:both"></div>
    </div>

<div class="line"></div>

    <div id="ReportOuterFrame">
		<div id="ReportFrameTitle"> Admin Actions </div>
		<form method="post" name="closeSite" id="closeSite" action="">
			<input type="submit" name="closeSiteBtn" id="closeSiteBtn" value="Close Site" onClick="closesitecheck();">
			<input type="text" id="closeSiteStatus" name="closeSiteStatus" />
		</form>
		<br>
		<form method="post" name="removeSystem" id="removeSystem" action="">
			<input type="submit" name="removeSystemBtn" id="removeSystemBtn" value="Remove System" onClick="removeSystemDo();">
			<label>From System ID<input type="text" id="fromSystemId" name="fromSystemId" /></label>
			<label>To System ID<input type="text" id="toSystemId" name="toSystemId" /></label>
		</form>
	    <br>
	    <form method="post" name="changeSystemId" id="changeSystemId" action="">
		    <input type="submit" name="changeSystemIdBtn" id="changeSystemIdBtn" value="Change System ID" onClick="changeSystemIdDo();">
		    <label>From System ID<input type="text" id="changeFromSystemId" name="changeFromSystemId" /></label>
		    <label>To System ID<input type="text" id="changeToSystemId" name="changeToSystemId" /></label>
	    </form>
	    <br>
	    <form method="post" name="renameUser" id="renameUser" action="">
		    <input type="submit" name="renameUserBtn" id="renameUserBtn" value="Rename User" onClick="renameUserDo();">
		    <label>From UID<input type="text" id="fromUID" name="fromUID" /></label>
		    <label>To UID<input type="text" id="toUID" name="toUID" /></label>
	    </form>
    </div>
	
<div class="line"></div>	
	<div id="ReportOuterFrame">
		<div id="ReportFrameTitle"> Email Addys </div>
		
		<?php 
			$sql="SELECT u.name, u.email
			FROM users AS u
			LEFT JOIN users_groups AS ug ON ug.uid = u.uid
			LEFT JOIN users_group_id AS gid ON gid.id = ug.gid
			WHERE active = 'Y' AND gid.`group` = 'grp_employee' AND u.id > 9;";
			if(!$resultUsersEmplyoee = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
				$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
			}
			
			$sql="SELECT u.name, u.email
			FROM users AS u
			LEFT JOIN users_roles AS ur ON ur.uid = u.uid
			LEFT JOIN users_role_id AS rid ON rid.id = ur.rid
			WHERE active = 'Y' AND rid.role = 'role_engineer' AND u.id > 9;";
			if(!$resultUsersEngineer = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
				$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
			}
			
			$sql="SELECT u.name, u.email
			FROM users AS u
			LEFT JOIN users_roles AS ur ON ur.uid = u.uid
			LEFT JOIN users_role_id AS rid ON rid.id = ur.rid
			WHERE active = 'Y' AND rid.role = 'role_dispatch' AND u.id > 9;";
			if(!$resultUsersDispatch = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
				$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
			}
			
			$sql="SELECT u.name, u.email
			FROM users AS u
			LEFT JOIN users_groups AS ug ON ug.uid = u.uid
			LEFT JOIN users_group_id AS gid ON gid.id = ug.gid
			WHERE active = 'Y' AND gid.`group` = 'grp_contractor' AND u.id > 9;";
			if(!$resultUsersContractor = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
				$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
			}
			
			$sql="SELECT u.name, u.email
			FROM users AS u
			LEFT JOIN users_groups AS ug ON ug.uid = u.uid
			LEFT JOIN users_group_id AS gid ON gid.id = ug.gid
			WHERE active = 'Y' AND gid.`group` = 'grp_customer' AND u.id > 9;";
			if(!$resultUsersCustomer = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
				$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
			}
		?>
		
		<div id="employee_email">
			<div style="width:100%;"><h3>Employees</h3></div>
			<pre class="brush: plain; collapse: true"><?php 
				while($rowUsersEmployee = $resultUsersEmplyoee->fetch_assoc()){
					echo $rowUsersEmployee['name']." (".$rowUsersEmployee['email'].");\n";	
				}
			?></pre>
		</div>
		
		<div id="engineer_email">
			<div style="width:100%;"><h3>Engineers</h3></div>
			<pre class="brush: plain; collapse: true"><?php 
				while($rowUsersEngineer = $resultUsersEngineer->fetch_assoc()){
					echo $rowUsersEngineer['name']." (".$rowUsersEngineer['email'].");\n";	
				}
			?></pre>
		</div>
		
		<div id="dispatch_email">
			<div style="width:100%;"><h3>Dispatchers</h3></div>
			<pre class="brush: plain; collapse: true"><?php 
				while($rowUsersDispatch = $resultUsersDispatch->fetch_assoc()){
					echo $rowUsersDispatch['name']." (".$rowUsersDispatch['email'].");\n";	
				}
			?></pre>
		</div>
		
		<div id="contractor_email">
			<div style="width:100%;"><h3>Contractors</h3></div>
			<pre class="brush: plain; collapse: true"><?php
				while($rowUsersContractor = $resultUsersContractor->fetch_assoc()){
					echo $rowUsersContractor['name']." (".$rowUsersContractor['email'].");\n";	
				}
			?></pre>
		</div>
		
		<div id="customer_email">
			<div style="width:100%;">
				<h3>Customers</h3></div>
			<pre class="brush: plain; collapse: true"><?php
				while($rowUsersCustomer = $resultUsersCustomer->fetch_assoc()){
					echo $rowUsersCustomer['name']." (".$rowUsersCustomer['email'].");\n";	
				}
			?></pre>
		</div>
    </div>
	
<div class="line"></div>
    
	<div id="tables" style="width:100%; margin-bottom:15px">
	        
			<div id="ReportOuterFrame">
				<?php  
					$sql = "SELECT * FROM `users_logins` ORDER BY `id` DESC LIMIT 50;";
					if(!$resultLast50 = $mysqli->query($sql)){
						$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
						$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
						$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
					}
				?>  
				<div id="ReportFrame">     
					<div id="ReportFrameTitle"> Last 50 user logins </div>
					<div id="last50loginsDiv" <?php if($resultLast50->num_rows == 0){echo "style=\"display:none\"";} ?>>
						<table width="100%" id="last50loginsTable">
							<thead>
								<tr>
									<th>UID</th> 
									<th>Failed</th>                          
									<th>IP</th>
									<th>Domain</th>
									<th>Type</th>               
								</tr>
							</thead>
							<tbody>
								<?php
									if($resultLast50->num_rows != 0){
										while($rowLast50 = $resultLast50->fetch_assoc())
										{
											echo "<tr>\n";
											echo "<td><a onClick=\"bancheck('User', '".$rowLast50['uid']."')\">".$rowLast50['uid']."</a></td>\n";
											echo "<td>".$rowLast50['failed']."</td>\n";
											echo "<td><a onClick=\"bancheck('IP', '".$rowLast50['ip']."')\">".$rowLast50['ip']."</a></td>\n";
											echo "<td><a onClick=\"bancheck('Domain', '".$rowLast50['domain']."')\">".$rowLast50['domain']."</a></td>\n";
											echo "<td>".$rowLast50['type']."</td>\n";
											echo "</tr>\n";
										}  
									}
								?>                        
							</tbody>
						</table>
					</div>
					<div id="last50loginsDiv" <?php if($resultLast50->num_rows != 0){echo "style=\"display:none\"";}else{echo "style=\"text-align:center\"";} ?>>
						<br />
						<h2>No user logins</h2>
					</div>
			 	 </div>
       		 </div>
			 
<div class="line"></div>
			 
			 <div id="ReportOuterFrame">
				<?php  
					$sql = "SELECT * FROM `users_logins` WHERE lower(`failed`) = 'yes' ORDER BY `id` DESC LIMIT 200;";
					if(!$resultFailed = $mysqli->query($sql)){
						$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
						$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
						$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
					}
				?>  
				<div id="ReportFrame">     
					<div id="ReportFrameTitle"> Failed login attempts </div>
					<div id="failedLoginsDiv" <?php if($resultFailed->num_rows == 0){echo "style=\"display:none\"";} ?>>
						<table width="100%" id="failedLoginsTable">
							<thead>
								<tr>
									<th>UID</th> 
									<th>Failed</th>                          
									<th>IP</th>
									<th>Domain</th>
									<th>Type</th>               
								</tr>
							</thead>
							<tbody>
								<?php
									if($resultFailed->num_rows != 0){
										while($rowFailed = $resultFailed->fetch_assoc())
										{
											echo "<tr>\n";
											echo "<td><a onClick=\"bancheck('User', '".$rowFailed['uid']."')\">".$rowFailed['uid']."</a></td>\n";
											echo "<td>".$rowLast50['failed']."</td>\n";
											echo "<td><a onClick=\"bancheck('IP', '".$rowFailed['ip']."')\">".$rowFailed['ip']."</a></td>\n";
											echo "<td><a onClick=\"bancheck('Domain', '".$rowFailed['domain']."')\">".$rowFailed['domain']."</a></td>\n";
											echo "<td>".$rowFailed['type']."</td>\n";
											echo "</tr>\n";
										}  
									}
								?>                        
							</tbody>
						</table>
					</div>
					<div id="failedLoginsDiv" <?php if($resultFailed->num_rows != 0){echo "style=\"display:none\"";}else{echo "style=\"text-align:center\"";} ?>>
						<br />
						<h2>No failed logins</h2>
					</div>
			 	 </div>
       		 </div>
			 
<div class="line"></div>
			 
			 <div id="ReportOuterFrame">
				<?php  
					$sql = "SELECT * FROM `admin_banlist` ORDER BY id DESC;";
					if(!$resultBanned = $mysqli->query($sql)){
						$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
						$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
						$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
					}
				?>  
				<div id="ReportFrame">     
					<div id="ReportFrameTitle"> List of banned User/IPs/Domains </div>
					<div id="banListDiv" <?php if($resultBanned->num_rows == 0){echo "style=\"display:none\"";} ?>>
						<table width="100%" id="banListTable">
							<thead>
								<tr>
									<th>Ban Type</th> 
									<th>Value</th>                          
									<th>Banned By</th>               
								</tr>
							</thead>
							<tbody>
								<?php
									if($resultBanned->num_rows != 0){
										while($rowBanned = $resultBanned->fetch_assoc())
										{
											if($rowBanned['ip'] != ""){
												$type = "&nbsp;&nbsp;IP";
											}
											if($rowBanned['user'] != ""){
												$type = "&nbsp;&nbsp;User";
											}
											if($rowBanned['domain'] != ""){
												$type = "&nbsp;&nbsp;Domain";
											}
											
											echo "<tr>\n";
											echo "<td>".$type."</td>\n";
											echo "<td><a onClick=\"unbancheck('".$type."', '".$rowBanned['ban']."')\">".$rowBanned['ban']."</a></td>\n";
											echo "<td>".$rowBanned['banned_by']."</td>\n";
											echo "</tr>\n";
										}  
									}
								?>                        
							</tbody>
						</table>
					</div>
					<div id="banListDiv" <?php if($resultBanned->num_rows != 0){echo "style=\"display:none\"";}else{echo "style=\"text-align:center\"";} ?>>
						<br />
						<h2>No bans</h2>
					</div>
			 	 </div>
       		 </div>			 
			 
<div class="line"></div>
			 
			 <div id="ReportOuterFrame">
				<?php  
					$sql = "SELECT uid,name,email FROM users WHERE last_login IS NULL AND active = 'Y';";
					if(!$resultNoLogin = $mysqli->query($sql)){
						$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
						$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
						$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
					}
				?>  
				<div id="ReportFrame">     
					<div id="ReportFrameTitle"> Users that have never logged in to the Portal</div>
					<div id="neverLoggedInDiv" <?php if($resultNoLogin->num_rows == 0){echo "style=\"display:none\"";} ?>>
						<table width="100%" id="neverLoggedInTable">
							<thead>
								<tr>
									<th>UID</th> 
									<th>Name</th>                          
									<th>Email</th>          
								</tr>
							</thead>
							<tbody>
								<?php
									if($resultNoLogin->num_rows != 0){
										while($rowNoLogin = $resultNoLogin->fetch_assoc())
										{											
											echo "<tr>\n";
											echo "<td>".$rowNoLogin['uid']."</td>\n";
											echo "<td>".$rowNoLogin['name']."</td>\n";
											echo "<td>".$rowNoLogin['email']."</td>\n";
											echo "</tr>\n";
										}  
									}
								?>                        
							</tbody>
						</table>
					</div>
					<div id="neverLoggedInDiv" <?php if($resultNoLogin->num_rows != 0){echo "style=\"display:none\"";}else{echo "style=\"text-align:center\"";} ?>>
						<br />
						<h2>All users have logged in once</h2>
					</div>
			 	 </div>
       		 </div>	

<div class="line"></div>
			 
			 <div id="ReportOuterFrame">
				<?php  
					$sql = "SELECT pu.uid, pu.name, pu.email
					FROM ".$db_name.".users AS pu
					LEFT JOIN ".$db_name.".users_groups AS pug ON pug.uid = pu.uid
					LEFT JOIN ".$db_name.".users_group_id AS pgid ON pgid.id = pug.gid
					LEFT JOIN ".$db_name_dms.".tblUsers AS dmsu ON dmsu.login COLLATE utf8_unicode_ci = pu.uid
					WHERE dmsu.login IS NULL AND pu.active = 'Y' AND pgid.`group` = 'grp_employee' AND pu.id > 9;";
					
					if(!$resultNoLoginDMS = $mysqli->query($sql)){
						$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
						$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
						$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
					}
				?>  
				<div id="ReportFrame">     
					<div id="ReportFrameTitle"> Users that have never logged in to the DMS</div>
					<div id="neverLoggedInDMSDiv" <?php if($resultNoLogin->num_rows == 0){echo "style=\"display:none\"";} ?>>
						<table width="100%" id="neverLoggedInDMSTable">
							<thead>
								<tr>
									<th>UID</th> 
									<th>Name</th>                          
									<th>Email</th>          
								</tr>
							</thead>
							<tbody>
								<?php
									if($resultNoLoginDMS->num_rows != 0){
										while($rowNoLoginDMS = $resultNoLoginDMS->fetch_assoc())
										{											
											echo "<tr>\n";
											echo "<td>".$rowNoLoginDMS['uid']."</td>\n";
											echo "<td>".$rowNoLoginDMS['name']."</td>\n";
											echo "<td>".$rowNoLoginDMS['email']."</td>\n";
											echo "</tr>\n";
										}  
									}
								?>                        
							</tbody>
						</table>
					</div>
					<div id="neverLoggedInDMSDiv" <?php if($resultNoLoginDMS->num_rows != 0){echo "style=\"display:none\"";}else{echo "style=\"text-align:center\"";} ?>>
						<br />
						<h2>All users have logged in once</h2>
					</div>
			 	 </div>
       		 </div>			 
			 
			 			 
			 
			 
		</div>
		
	
	
	

</div>
<script type="text/javascript">
		SyntaxHighlighter.all();
</script>
<?php require_once($footer_include); ?>
