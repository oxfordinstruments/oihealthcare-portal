<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

$sql = "SELECT id, name FROM users_role_id;";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

?>

<!DOCTYPE html>
<html>
<head>
<?php require_once($head_include);?>
<?php require_once($css_include);?>
<?php require_once($js_include);?>


<script type="text/javascript">
$(document).ready(function() {
	$('#table').dataTable({
		"bJQueryUI": true,
		"bStateSave": false,
		"sPaginationType": "full_numbers",
		"iDisplayLength": 50
	});	
});
</script>
</head>
<body>
<?php require_once($header_include); ?>
<div id="OIReportContent"> 
	<div id="ReportOuterFrame">
		<div id="ReportFrameTitle"> Manage Role's Permissions </div>
		
		<table width="100%" id="table">
			<thead>
				<tr>
					<th>ID</th>
					<th>Role</th>
				</tr>
			</thead>
			<tbody>
				<?php
					while($row = $result->fetch_assoc())
					{
						echo "<tr>\n";
						echo "<td align='center'><a target=\"_self\" href=\"roles_perms.php?role=".$row['id']."\">". $row['id'] ."</a></td>\n";
						echo "<td>". $row['name']."</td>\n";
						echo "</tr>\n";
					}
					?>     
			</tbody>
		</table>		
		
		
	</div>
</div>
<?php require_once($footer_include); ?>