<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

$sql = "SELECT * FROM systems_types;";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

?>

<!DOCTYPE html>
<html>
<head>
<?php require_once($head_include);?>
<?php require_once($css_include);?>
<?php require_once($js_include);?>


<script type="text/javascript">
$(document).ready(function() {
	$('#systemsTable').dataTable({
		"bJQueryUI": true,
		"bStateSave": false,
		"sPaginationType": "full_numbers"
	});	
});
</script>
</head>
<body>
<?php require_once($header_include); ?>
<div id="OIReportContent"> 
	<div id="ReportOuterFrame">
		<div id="ReportFrameTitle"> Manage Systems Types </div>
		
		<table width="100%" id="systemsTable">
			<thead>
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th>Type</th>
					<th>Mfg</th>
					<th>Edit Options</th>
					<th>Edit Coils</th>
					<th>Edit Accessories</th>
					<th>Edit Consoles</th>
					<th>Edit Tables</th>
				</tr>
			</thead>
			<tbody>
				<?php
					while($row = $result->fetch_assoc())
					{
						echo "<tr>\n";	
						echo "<td>". $row['id']."</td>\n";
						echo "<td>". $row['name']."</td>\n";
						echo "<td>". $row['type']."</td>\n";
						echo "<td>". $row['mfg']."</td>\n";
						echo "<td><a target=\"_self\" href=\"systems_options.php?system=".$row['id']."\">Options</a></td>\n";
						if(strtolower($row['type']) == 'mr'){
							echo "<td><a target=\"_self\" href=\"systems_coils.php?system=".$row['id']."\">Coils</a></td>\n";
						}else{
							echo "<td>&nbsp;</td>";	
						}
						echo "<td><a target=\"_self\" href=\"systems_accessories.php?system=".$row['id']."\">Accessories</a></td>\n";
						echo "<td><a target=\"_self\" href=\"systems_consoles.php?system=".$row['id']."\">Consoles</a></td>\n";
						echo "<td><a target=\"_self\" href=\"systems_tables.php?system=".$row['id']."\">Tables</a></td>\n";
						echo "</tr>\n";
					}
					?>     
			</tbody>
		</table>		
		
		
	</div>
</div>
<?php require_once($footer_include); ?>