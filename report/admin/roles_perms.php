<?php
$debug = false;

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

if(!isset($_GET['role'])){
	$log->logerr('Role not in get',1067,false,basename(__FILE__),__LINE__);
	die('Role not valid');
}

//Get Role Info
$sql="SELECT * FROM users_role_id WHERE id = ".$_GET['role'].";";
if(!$resultRole = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowRole = $resultRole->fetch_assoc();
d($rowRole);

//Get all perms
$sql="SELECT * FROM users_perm_id ORDER BY name ASC;";
if(!$resultAllPerms = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

$all_perms = array();
while($rowAllPerms = $resultAllPerms->fetch_assoc()){
	$all_perms[$rowAllPerms['id']] = array('name'=>$rowAllPerms['name'], 'perm'=>$rowAllPerms['perm'], 'notes'=>$rowAllPerms['notes']);
}
d($all_perms);

//Get current selected role's perms
$sql="SELECT urp.pid, upi.name
FROM users_role_perms AS urp
LEFT JOIN users_perm_id AS upi ON upi.id = urp.pid
WHERE urp.rid = '".$_GET['role']."' ORDER BY urp.pid;";
if(!$resultCurPerms = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$cur_perms = array();
while($rowCurPerms = $resultCurPerms->fetch_assoc()){
	$cur_perms[$rowCurPerms['pid']] = $rowCurPerms['name'];

}
d($cur_perms);


?>

<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>Untitled Document</title>
	<script src="/resources/js/underscore.js"></script>
	<script src="/resources/js/jquery/jquery.min.js"></script>

	<script type="text/javascript">

		var cur_perms = _.values(<?php echo json_encode($cur_perms) ?>);
		console.log(cur_perms);

		function submit_check(){

			var data = $('input[type="checkbox"]:checked').serializeArray(),
				obj = [];
			console.log(data);

			for(var i = 0; i < data.length; i++){
				obj.push(data[i].value);
			}

			console.log(obj);
			var perms_removed = _.difference(cur_perms, obj);
			console.log(perms_removed);

			if(perms_removed.length > 0){
				if(confirm("You have removed a permission that was assigned. This possibly will cause problems with users. Proceed?")){
					document.forms['form'].submit();
				}
			}else{
				document.forms['form'].submit();
			}

		}

	</script>

</head>
<body>
<h1>Permissions for <?php echo $rowRole['name']; ?></h1>
<form id="form" name="form" method="post"  action="roles_perms_save.php">
<input type="button" name="Submit" onclick="submit_check();" value="Save Permissions Selection" />
<br>
<table border="1" cellpadding="1" cellspacing="1">
	<th>Selected</th>
	<th>Permission Name</th>
	<th>Notes</th>
	<?php 
	foreach($all_perms as $key=> $value){
		echo '<tr>';
		echo "<td><input type=\"checkbox\" name=\"perms[".$key."]\" value=\"".$value['name']."\"";
		if(!empty($cur_perms[$key])){
			echo " checked ";	
		}
		echo " />";
		echo "<td>".$value['name']."</td>";
		echo "<td>".$value['notes']."</td>";
		echo "</tr>\n";
	}
	?>
</table>
<br>
<input type="hidden" name="role" value="<?php echo $_GET['role']; ?>" />
<input type="hidden" name="orig_perms" value='<?php echo json_encode($cur_perms); ?>' />
	<input type="button" name="Submit" onclick="submit_check();" value="Save Permissions Selection" />
</form>
</body>
</html>