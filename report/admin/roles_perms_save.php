<?php
$debug = false;

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();
require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

$orig_perms = json_decode($_POST['orig_perms'],true);
d($orig_perms);

$remove_perm = false;
if(!empty(array_diff($orig_perms, $_POST['perms']))) {
	$remove_perm = true;
}
$remove_perms_arr = array_keys(array_diff($orig_perms, $_POST['perms']));
d($remove_perms_arr);

dd($_POST);

if(!isset($_POST[role])){
	$log->logerr('Role not in post',1067,false,basename(__FILE__),__LINE__);
	die('Role not valid');
}

$sql="DELETE FROM users_role_perms WHERE rid = ".$_POST['role'].";";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

if($remove_perm){
	$sql = "DELETE FROM users_perms WHERE pid in(".implode(',',$remove_perms_arr).");";
	s($sql);
	if (!$result = $mysqli->query($sql)) {
		$log->logerr($sql, 1000, false, basename(__FILE__), __LINE__);
		$log->logerr('There was error running the query [' . $mysqli->error . ']', 1000, false, basename(__FILE__), __LINE__);
		$log->logerr('Error occurred, contact support.', 1000, true, basename(__FILE__), __LINE__);
	}
}

echo "Setting Permissions:<br>";

if(count($_POST['perms']) >= 1){

	$sql="INSERT INTO users_role_perms (rid,pid) VALUES";
	
	foreach($_POST['perms'] as $key=> $perm){
		echo "&emsp;",$perm,"<br>";
		$sql .= "(".$_POST['role'].", $key),";
	}
	$sql = rtrim($sql, ',');
	$sql .= ";";
	s($sql);
	
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}

}else{
	echo "&emsp;none<br>";	
}
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
<script type="text/javascript">
function delayer(){
    <?php if(!$debug){ ?>
	window.location = "manage_roles.php"
	<?php } ?>
}
</script>
</head>
<body onLoad="setTimeout('delayer()', 3000)">
<h1><span class="red">Page will return in 3 seconds</span></h1>
</body>
</html>