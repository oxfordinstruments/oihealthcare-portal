<?php
//Update Completed 04/16/15
$debug = false;

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

if(!isset($_GET['system'])){
	$log->logerr('System not in get',1067,false,basename(__FILE__),__LINE__);	
	die('System type not valid');
}

//Get System Type Info
$sql="SELECT * FROM systems_types WHERE id = ".$_GET['system'].";";
if(!$resultSystem = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowSystem = $resultSystem->fetch_assoc();
d($rowSystem);

//Get all option for modality
$sql="SELECT * FROM misc_accessories WHERE modality = '".$rowSystem['modality']."' ORDER BY name ASC;";
if(!$resultAllAccessories = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

$all_accessories = array();
while($rowAllAccessories = $resultAllAccessories->fetch_assoc()){
	$all_accessories[$rowAllAccessories['id']] = array('name'=>$rowAllAccessories['name'], 'notes'=>$rowAllAccessories['notes']);
}
d($all_accessories);

//Get current select options for system type
$sql="SELECT msa.id, msa.name, msa.notes
FROM systems_accessories AS sa
LEFT JOIN misc_accessories AS msa ON msa.id = sa.accessories_id
WHERE sa.systems_types_id = '".$_GET['system']."';";
if(!$resultCurAccessories = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$cur_accessories = array();
while($rowCurAccessories = $resultCurAccessories->fetch_assoc()){
	$cur_accessories[$rowCurAccessories['id']] = array('name'=>$rowCurAccessories['name'], 'notes'=>$rowCurAccessories['notes']);
}
d($cur_accessories);


?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
</head>
<body>
<h1>Options for <?php echo $rowSystem['name']; ?></h1>
<form id="form" name="form" method="post"  action="systems_accessories_save.php">
<input type="submit" name="Submit" value="Save Accessories Selection" />
<br>
<table border="1" cellpadding="1" cellspacing="1">
	<th>Selected</th>
	<th>Accessory Name</th>
	<th>Notes</th>
	<?php 
	foreach($all_accessories as $key=>$value){
		echo '<tr>';
		echo "<td><input type=\"checkbox\" name=\"accessories[".$key."]\" value=\"".$value['name']."\"";
		if(!empty($cur_accessories[$key])){
			echo " checked ";	
		}
		echo " />";
		echo "<td>".$value['name']."</td>";
		echo "<td>".$value['notes']."</td>";
		echo "</tr>\n";
	}
	?>
</table>
<br>
<input type="hidden" name="system_type" value="<?php echo $_GET['system']; ?>" />
<input type="submit" name="Submit" value="Save Accessories Selection" />
</form>
</body>
</html>