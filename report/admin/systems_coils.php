<?php
//Update Completed 04/16/15
$debug = false;

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

if(!isset($_GET['system'])){
	$log->logerr('System not in get',1067,false,basename(__FILE__),__LINE__);	
	die('System type not valid');
}

//Get System Type Info
$sql="SELECT * FROM systems_types WHERE id = ".$_GET['system'].";";
if(!$resultSystem = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowSystem = $resultSystem->fetch_assoc();
d($rowSystem);

//Get all option for modality
$sql="SELECT * FROM misc_coils ORDER BY name ASC;";
if(!$resultAllCoils = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

$all_coils = array();
while($rowAllCoils = $resultAllCoils->fetch_assoc()){
	$all_coils[$rowAllCoils['id']] = array('name'=>$rowAllCoils['name'], 'notes'=>$rowAllCoils['notes']);
}
d($all_coils);

//Get current select options for system type
$sql="SELECT msc.id, msc.name, msc.notes
FROM systems_coils AS sc
LEFT JOIN misc_coils AS msc ON msc.id = sc.coil_id
WHERE sc.systems_types_id = '".$_GET['system']."';";
if(!$resultCurCoils = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$cur_coils = array();
while($rowCurCoils = $resultCurCoils->fetch_assoc()){
	$cur_coils[$rowCurCoils['id']] = array('name'=>$rowCurCoils['name'], 'notes'=>$rowCurCoils['notes']);
}
d($cur_coils);


?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
</head>
<body>
<h1>Options for <?php echo $rowSystem['name']; ?></h1>
<form id="form" name="form" method="post"  action="systems_coils_save.php">
<input type="submit" name="Submit" value="Save Coils Selection" />
<br>
<table border="1" cellpadding="1" cellspacing="1">
	<th>Selected</th>
	<th>Coil Name</th>
	<th>Notes</th>
	<?php 
	foreach($all_coils as $key=>$value){
		echo '<tr>';
		echo "<td><input type=\"checkbox\" name=\"coils[".$key."]\" value=\"".$value['name']."\"";
		if(!empty($cur_coils[$key])){
			echo " checked ";	
		}
		echo " />";
		echo "<td>".$value['name']."</td>";
		echo "<td>".$value['notes']."</td>";
		echo "</tr>\n";
	}
	?>
</table>
<br>
<input type="hidden" name="system_type" value="<?php echo $_GET['system']; ?>" />
<input type="submit" name="Submit" value="Save Coils Selection" />
</form>
</body>
</html>