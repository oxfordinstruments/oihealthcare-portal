<?php
//Update Completed 04/16/15
$debug = false;

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

if(!isset($_GET['system'])){
	$log->logerr('System not in get',1067,false,basename(__FILE__),__LINE__);	
	die('System type not valid');
}

//Get System Type Info
$sql="SELECT * FROM systems_types WHERE id = ".$_GET['system'].";";
if(!$resultSystem = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowSystem = $resultSystem->fetch_assoc();
d($rowSystem);

//Get all option for modality
$sql="SELECT * FROM misc_sw_options WHERE modality = '".$rowSystem['modality']."' ORDER BY name ASC;";
if(!$resultAllOptions = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

$all_options = array();
while($rowAllOptions = $resultAllOptions->fetch_assoc()){
	$all_options[$rowAllOptions['id']] = array('name'=>$rowAllOptions['name'], 'notes'=>$rowAllOptions['notes']);
}
d($all_options);

//Get current select options for system type
$sql="SELECT mso.id, mso.name, mso.description, mso.notes
FROM systems_sw_options AS so
LEFT JOIN misc_sw_options AS mso ON mso.id = so.option_id
WHERE so.systems_types_id = '".$_GET['system']."';";
if(!$resultCurOptions = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$cur_options = array();
while($rowCurOptions = $resultCurOptions->fetch_assoc()){
	$cur_options[$rowCurOptions['id']] = array('name'=>$rowCurOptions['name'], 'notes'=>$rowCurOptions['notes']);
}
d($cur_options);


?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
</head>
<body>
<h1>Options for <?php echo $rowSystem['name']; ?></h1>
<form id="form" name="form" method="post"  action="systems_options_save.php">
<input type="submit" name="Submit" value="Save Options Selection" />
<br>
<table border="1" cellpadding="1" cellspacing="1">
	<th>Selected</th>
	<th>Option Name</th>
	<th>Option Description</th>
	<th>Notes</th>
	<?php 
	foreach($all_options as $key=>$value){
		echo '<tr>';
		echo "<td><input type=\"checkbox\" name=\"options[".$key."]\" value=\"".$value['name']."\"";
		if(!empty($cur_options[$key])){
			echo " checked ";	
		}
		echo " />";
		echo "<td>".$value['name']."</td>";
		echo "<td>".$value['description']."</td>";
		echo "<td>".$value['notes']."</td>";
		echo "</tr>\n";
	}
	?>
</table>
<br>
<input type="hidden" name="system_type" value="<?php echo $_GET['system']; ?>" />
<input type="submit" name="Submit" value="Save Options Selection" />
</form>
</body>
</html>