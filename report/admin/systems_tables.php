<?php
//Update Completed 04/16/15
$debug = true;

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

if(!isset($_GET['system'])){
	$log->logerr('System not in get',1067,false,basename(__FILE__),__LINE__);	
	die('System type not valid');
}

//Get System Type Info
$sql="SELECT * FROM systems_types WHERE id = ".$_GET['system'].";";
if(!$resultSystem = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowSystem = $resultSystem->fetch_assoc();
d($rowSystem);

//Get all option for modality
$sql="SELECT * FROM misc_tables WHERE modality = '".$rowSystem['modality']."' ORDER BY name ASC;";
if(!$resultAllTables = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

$all_tables = array();
while($rowAllTables = $resultAllTables->fetch_assoc()){
	$all_tables[$rowAllTables['id']] = array('name'=>$rowAllTables['name'], 'notes'=>$rowAllTables['notes']);
}
d($all_tables);

//Get current select tables for system type
$sql="SELECT mst.id, mst.name, mst.notes
FROM systems_tables AS st
LEFT JOIN misc_tables AS mst ON mst.id = st.table_id
WHERE st.systems_types_id = '".$_GET['system']."';";
if(!$resultCurTables = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$cur_tables = array();
while($rowCurTables = $resultCurTables->fetch_assoc()){
	$cur_tables[$rowCurTables['id']] = array('name'=>$rowCurTables['name'], 'notes'=>$rowCurTables['notes']);
}
d($cur_tables);


?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
</head>
<body>
<h1>Options for <?php echo $rowSystem['name']; ?></h1>
<form id="form" name="form" method="post"  action="systems_tables_save.php">
<input type="submit" name="Submit" value="Save Tables Selection" />
<br>
<table border="1" cellpadding="1" cellspacing="1">
	<th>Selected</th>
	<th>Table Name</th>
	<th>Notes</th>
	<?php 
	foreach($all_tables as $key=>$value){
		echo '<tr>';
		echo "<td><input type=\"checkbox\" name=\"tables[".$key."]\" value=\"".$value['name']."\"";
		if(!empty($cur_tables[$key])){
			echo " checked ";	
		}
		echo " />";
		echo "<td>".$value['name']."</td>";
		echo "<td>".$value['notes']."</td>";
		echo "</tr>\n";
	}
	?>
</table>
<br>
<input type="hidden" name="system_type" value="<?php echo $_GET['system']; ?>" />
<input type="submit" name="Submit" value="Save Tables Selection" />
</form>
</body>
</html>