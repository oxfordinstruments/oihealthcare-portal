<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/session_handler.php');
session_name("OIREPORT");
$session = new Session();

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

if(isset($_GET['ip'])){
	$ip = $_GET['ip'];
}
if(isset($_GET['user'])){
	$user = $_GET['user'];
}
if(isset($_GET['domain'])){
	$domain = $_GET['domain'];
}
if(isset($_GET['unban'])){
	$unban = $_GET['unban'];
}
if($ip == "" && $user == "" and $domain == "" && $unban == ""){
	$log->logerr('Blanks',1001,true,basename(__FILE__),__LINE__);
}

$loc=$_SESSION['loc'];

$refering_uri = "https://".$_SERVER['HTTP_HOST']."/".$loc."/";


ob_start();//Start output buffering
$settings = new SimpleXMLElement($_SERVER['DOCUMENT_ROOT'].'/settings.xml', null, true);//Open the settings.xml file

require($_SERVER['DOCUMENT_ROOT'].'/mysqlInfo.php');//require the mysql connection info
$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno) {
	$log->logerr($mysqli->connect_error,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support',1000,true,basename(__FILE__),__LINE__);
}

if($ip != ""){
	$sql="INSERT INTO `admin_banlist` (`ban`, `ip`, `banned_by`) VALUES ('$ip', 'x', '".$_SESSION['login']."');";
    $result=$mysqli->query($sql);
	header("location:$refering_uri");
}

if($user != ""){
	$sql="INSERT INTO `admin_banlist` (`ban`, `user`, `banned_by`) VALUES ('$user', 'x', '".$_SESSION['login']."');";
    $result=$mysqli->query($sql);
	header("location:$refering_uri");
}

if($domain != ""){
	$sql="INSERT INTO `admin_banlist` (`ban`, `domain`, `banned_by`) VALUES ('$domain', 'x', '".$_SESSION['login']."');";
    $result=$mysqli->query($sql);
	header("location:$refering_uri");
}

if($unban != ""){
	$sql="DELETE FROM `admin_banlist` WHERE  `ban` = '$unban'";
    $result=$mysqli->query($sql);
	header("location:$refering_uri");
}
?>