<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

$comp_word_limt 	 = intval($settings->complaint_word_limit);
$serv_word_limt		 = intval($settings->service_word_limit);
$custact_word_limt	 = intval($settings->custact_word_limit);
$probrpt_word_limt	 = intval($settings->probrpt_word_limit);


$myusername = $_SESSION["login"];
$sql = "SELECT * FROM users WHERE uid='$myusername';";
if(!$resultUser = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowUser = $resultUser->fetch_assoc();

$sql="SELECT * FROM `systems_status`";
if(!$resultStatus = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$arr_status = array();
while($rowStatus = $resultStatus->fetch_assoc())
{
	array_push_assoc($arr_status, $rowStatus['id'], $rowStatus['status']);
}

$sql="SELECT u.uid, u.name
FROM users AS u
LEFT JOIN users_roles AS ur ON ur.uid = u.uid
LEFT JOIN users_role_id AS rid ON rid.id = ur.rid
WHERE rid.role = 'role_engineer' OR rid.role = 'role_contractor';";
if(!$resultEngineers = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$arr_engineers = array();
while($rowEngineers = $resultEngineers->fetch_assoc())
{
	array_push_assoc($arr_engineers, $rowEngineers['uid'], $rowEngineers['name']);
}

function array_push_assoc(&$array, $key, $value){
	$array[$key] = $value;
	return $array;
}

function limit_words($string, $word_limit)
{
	$words = explode(" ",$string);
	if(count($words) > $word_limit){
		return implode(" ",array_splice($words,0,$word_limit))."...(more)";
	}else{
		return implode(" ",array_splice($words,0,$word_limit));
	}
}

?>
<!DOCTYPE html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<?php require_once($js_include);?>

<script type="text/javascript">
	function start(){
		//getCookies();	
	}
/////////////////////////////////////////////////////////////////////////////////////	
	function getCookie(c_name)
	{
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++)
		{
			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
			x=x.replace(/^\s+|\s+$/g,"");
			if (x==c_name)
			{
			return unescape(y);
			}
		}
	}
/////////////////////////////////////////////////////////////////////////////////////	
$(document).ready(function() {	
		reqestsTable = $('#requestsTable').dataTable({
			"bJQueryUI": true,
			//"bStateSave": true,
			"iCookieDuration": 60*60*24*365, // 1 year
			"sPaginationType": "full_numbers",
			"aaSorting": [],
			"fnInitComplete": function () {
           		 this.$('tr').click( function () {
               		 var href = $(this).find("a").attr("href");
					 if(href) {
						window.location = href;
					 }
           		 });
       		 }
		});
		
/////////////////////////////////////////////////////////////////////////////////////		
		pastTable = $('#pastReportsTable').dataTable({
			"bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "../common/connectors/completed_reports_connector.php",
			"bJQueryUI": true,
			"aLengthMenu": [[5, 10, 25, 50], [5, 10, 25, 50]],
			"iCookieDuration": 60*60*24*365, // 1 year
			"iDisplayLength": 5,
			"sPaginationType": "full_numbers",
			"aaSorting": [ [2,'desc'] ], 			
			"aoColumnDefs": [
                        { "bSearchable": false, "bVisible": false, "aTargets": [ 0 ] }
            ],
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
					//$('tr:eq(0)', nRow).addClass("pastReportsHelp");
					$('td:eq(0)', nRow).html( function() {	
						var moment_dt = moment($(this).text()).tz(aData[9]);
						return "<a href='/report/common/report_view.php?id=" + aData[8] + "&uid=<?php echo $myusername ?>'>" + moment_dt.format('<?php echo jsdispfd; ?>') + "</a>";
					});
					this.$('tr').click( function () {
               		 var href = $(this).find("a").attr("href");
					 if(href) {
						window.location = href;
					 }
           		 });
			},
			"fnInitComplete": function () {
           		 this.$('tr').addClass("pastReportsHelp");
       		},
			"fnServerParams": function ( aoData ) {
			  aoData.push( { "name": "oiuid", "value": "<?php echo $myusername ?>" },
			  			   { "name": "oiall", "value": "1"} ); //change to 0 for just engineer
			}
		});
/////////////////////////////////////////////////////////////////////////////////////	
		<?php
		if(strtolower($rowUser['pref_show_help']) != 'y' or $_SESSION['mobile_device'] == true){
		?>
		$('#tiplayer').hide()
		<?php
		}
		?>
/////////////////////////////////////////////////////////////////////////////////////
		//$("div#sitesTable_filter").children("label").children("input").focus();
		$("div#requestsTable_filter > label > input").focus();
		
		$(document).keyup(function(e) {			
			$("#requestsTable").each(function(index, element) {
				if($(this).children("tbody").children("tr").length == 1 && e.which == 13){
					$(this).find("td > a").click();
				}
			});
			
			$("#pastReportsTable").each(function(index, element) {
				if($(this).children("tbody").children("tr").length == 1 && e.which == 13){
					$(this).find("td > a").click();
				}
			});
			
		});
});
/////////////////////////////////////////////////////////////////////////////////////
</script>
</head>
<body onLoad="start()">
<?php require_once($header_include); ?>
    
<div id="OIReportContent"> 
        <div id="ReportOuterFrame">
			<div id="ReportFrame" style="text-align:center">
				<div id="ReportFrameTitle"> Quick Links </div>
				<br>
				<h2><a href="<?php echo $settings->kb_url; ?>" target="_blank"><?php echo $settings->short_name; ?> Knowledge Base <sup>1</sup></a></h2>
				<h2><a href="<?php echo $settings->dms_url; ?>" target="_blank"><?php echo $settings->short_name; ?> DMS <sup>1</sup></a></h2>
				<h2><a href="https://remote.oxinst.com" target="_blank"><?php echo $settings->short_name; ?> Intranet <sup>2</sup></a></h2>
				<h2><a href="https://mail.oxinst.com" target="_blank"><?php echo $settings->short_name; ?> Webmail <sup>2</sup></a></h2>
				<span style="font-size:10px;">1 same login as Portal</span>
				<br>
				<span style="font-size:10px;">2 same login as Oxford email</span>
			</div>
			<div class="line"></div>
	        <?php  
				$sql = "SELECT r.*, sbc.credit_hold, sbc.pre_paid, sbc.property, sb.status AS cur_system_status
						FROM systems_requests AS r
						LEFT JOIN systems_base AS sb ON sb.unique_id = r.system_unique_id
						LEFT JOIN systems_base_cont AS sbc ON sb.unique_id = sbc.unique_id
						WHERE r.`status`='open' AND r.`deleted` = 'n'
						ORDER BY r.id ASC;";
				if(!$resultRequests = $mysqli->query($sql)){
					$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
					$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
					$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
				}
			?>  
            <div id="ReportFrame">     
            <div id="ReportFrameTitle"> Open Service Requests </div>
            <div id="requestsDiv" <?php if($resultRequests->num_rows == 0){echo "style=\"display:none\"";} ?>>
	            <div class="legend">Color Legend:&nbsp;<span class="credit_hold">Credit Hold</span>&emsp;<span class="pre_paid">Pre-Paid</span>&emsp;<span class="future">Future</span></div>
                <table width="100%" id="requestsTable" >
                    <thead>
                        <tr>
                        	<th width="75px">Date</th> 
                        	<th width="50px">Request Number</th>                          
                            <th width="50px">System ID</th>
                            <th>System Nickname</th>
                            <th width="300">Problem</th>
                            <th>Engineer</th>
                            <th width="125px">System Status</th>
                            <th width="50px">Report Started</th>                   
                        </tr>
                    </thead>
                    <tbody>
                    	<?php
						if($resultRequests->num_rows != 0){
							while($rowRequest = $resultRequests->fetch_assoc())
							{
								if(strtolower($rowRequest['property']) == 'f'){
									echo "<tr class=\"future\">\n";
								}elseif(strtolower($rowRequest['credit_hold']) == "y"){
									echo "<tr class=\"credit_hold\">\n";
								}elseif(strtolower($rowRequest['pre_paid']) == "y"){
									echo "<tr class=\"pre_paid\">\n";
								}else{
									echo "<tr>\n";
								}
								if(strtolower($rowRequest['report_started']) != 'y'){
									echo "<td>". date(phpdispfd,strtotime($rowRequest['request_date']))."</td>\n";
								}else{
									echo "<td><a style=\"color:#960\" onClick=\"alert('Service report has been started for this request.')\">". date(phpdispfd,strtotime($rowRequest['request_date']))."</a></td>\n";
								}
								echo "<td>". $rowRequest['request_num']."</td>\n";
								echo "<td>". $rowRequest['system_id']."</td>\n";
								echo "<td>". $rowRequest['system_nickname']."</td>\n";
								echo "<td>". limit_words($rowRequest['problem_reported'],$probrpt_word_limt)."</td>\n";
								echo "<td>". $arr_engineers[$rowRequest['engineer']]."</td>\n";
								echo "<td>". $arr_status[$rowRequest['cur_system_status']]."</td>\n";
								echo "<td>"; if(strtolower($rowRequest['report_started'])=="y"){echo "Yes";} echo "</td>\n";
								echo "</tr>\n";
							}
						}
						?>                        
                    </tbody>
                </table>
            </div>
            <div id="requestDiv" <?php if($resultRequests->num_rows != 0){echo "style=\"display:none\"";}else{echo "style=\"text-align:center\"";} ?>>
            	<br />
            	<h2>No Service Requests</h2>
            </div>
          </div>
        </div>
		<div class="line"></div>
		<div id="ReportFrame">
        <div id="ReportFrameTitle">Completed Reports</div>
			<div id="pastReprotsDiv">
				<table width="100%" id="pastReportsTable" >
					<thead>
						<tr>
							<th width="1px">idx</th>
							<th width="75px">Date</th>
							<th width="50px">Report ID</th>
							<th width="50px">System ID</th>
							<th width="100px">System Nickname</th>
							<th width="75px">Engineer</th>
							<th width="150px">Complaint</th>
							<th>Service</th>
						</tr>
					</thead>
					<tbody>    
					</tbody>
				</table>
			</div>
		</div>         
	</div>
  </div>
    <br> 
</div>
<?php require_once($footer_include); ?>