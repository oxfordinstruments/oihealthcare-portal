<?php
//Update Completed 12/12/14
require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

$debug = false;

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/session_handler.php');
session_name("OIREPORT");
ini_set('session.cookie_lifetime', 300);
ini_set('session.gc_maxlifetime', 300);
$session = new Session();

ob_start();//Start output buffering
$settings = new SimpleXMLElement($_SERVER['DOCUMENT_ROOT'].'/settings.xml', null, true);
require_once($_SERVER['DOCUMENT_ROOT'].'/define_inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

require($_SERVER['DOCUMENT_ROOT']."/mysqlInfo.php");//require the mysql connection info

// Connect to server and select databse.
$mysqli = new mysqli("$host", "$username", "$password", "$db_name");

/* check connection */
if ($mysqli->connect_errno) {
	$log->logerr('There was error connecting to server ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

d($_POST);

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_auto_login.php');
$sal = new sessionAutoLogin($mysqli);

$browser = $_SERVER['HTTP_USER_AGENT'];

$login_username=base64_decode($_POST['uname']);
$login_password=base64_decode($_POST['pword']);
$salt = "STyf1anSTydajgnmd";
$encrypted_mypassword = "{MD5}" . base64_encode( pack( "H*", md5( $login_password . $salt ) ) );
$encoded_password = openssl_encrypt($login_password, SSL_METHOD, SSL_KEY, false, SSL_IV);

$ip = $_SERVER['REMOTE_ADDR'];
$domain = gethostbyaddr($ip);

$sql="SELECT * FROM users WHERE uid='$login_username' and pwd='$encrypted_mypassword';";
s($sql);
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

$t = time();
$ts = date(storef,$t);     
$count = $result->num_rows;
s("Count: ".$count);
// If result matched $login_username and $login_password, table row must be 1 row

$sql="SELECT id FROM admin_banlist WHERE ban = '$login_username' OR ban = '$ip' OR ban = '$domain';";
s($sql);
if(!$resultBan = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$banCount = $resultBan->num_rows;

s("Ban Count: ".$banCount);

if($banCount > 0)
{
	//setcookie("qazwsxedc", 10, time()+900);
	$sql="INSERT INTO `users_logins` (`uid`, `ip`, `domain`, `failed`, `pwd`, `type`, `browser`) VALUES ('$login_username', '$ip', '$domain', 'banned', '$login_password', 'web', '$browser');";
	s($sql);
	if(!$result = mysql_query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	
	if($debug){
		echo "BANNED",EOL;
	}else{
		$log->loginfo($username,1008);
		header("location:login_ban.php");	
	}
	die("BANNED");
}

if(isset($_COOKIE["oihp_tries"])){
	$tries=$_COOKIE["oihp_tries"];
	$tries++;
	setcookie("oihp_tries", $tries, time()+900, '/');
}else{
	setcookie("oihp_tries", 1, time()+900, '/');
	$tries="";
}

if($debug){$tries=0;}

if($tries>=5){
	$log->loginfo("To many login attempts: ".$login_username,1009,false,basename(__FILE__),__LINE__);
	header("location:locked.php");
}else{
	if($count==1){	
		$sql="CALL users_data_get('$login_username');";
		s($sql);
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
		$row = $result->fetch_assoc();
		
		$userdata = array();
		foreach($row as $key => $value){
			$userdata[$key] = $value;
		}
		
		//Clear the remaining results from the above mysql call statement
		if($mysqli->more_results()){
			while ($mysqli->next_result());
		}
		
		
		if(strtolower($userdata['active']) == 'y'){
			$employee = false;
			$contractor = false;
			$customer = false;
			
			if(strtolower($userdata['perm_extend_login']) == 'y'){
				setcookie('OIREPORT', session_id(), time() + (60*60*10), '/');
			}else{
				setcookie('OIREPORT', session_id(), time() + (60*60*3), '/');
			}
			
			if(strtolower($row['grp_employee']) == 'y'){ $employee = true; $_SESSION['group'] = 1; }
			if(strtolower($row['grp_contractor']) == 'y'){ $contractor = true; $_SESSION['group'] = 2;}
			if(strtolower($row['grp_customer']) == 'y'){ $customer = true; $_SESSION['group'] = 3;}
			
			if(($employee + $customer + $contractor) != 1){
				if(!$debug){
					$log->logerr('Multi-Group: '.$login_username,1002,false,basename(__FILE__),__LINE__);
					$log->logerr('Employee: '.$employee,1002,false,basename(__FILE__),__LINE__);
					$log->logerr('Contractor: '.$contractor,1002,false,basename(__FILE__),__LINE__);
					$log->logerr('Customer: '.$customer,1002,false,basename(__FILE__),__LINE__);
					header("Location:/error.php?n=1002&t=multi-group&p=check_login.php");	
				}else{
					echo "MULTI-GROUP ERROR",EOL;
				}
			}
			
			//Add user data to session
			unset($userdata['pwd']);
			unset($userdata['secret_1']);
			unset($userdata['secret_2']);
			$_SESSION['userdata'] = $userdata;	
			
			//Set default location and role
			$_SESSION['loc'] = $userdata['location'];
			$_SESSION['role'] = $userdata['default_role'];
			$_SESSION['current_role'] = $userdata['role'];
			
			//add roles to session
			$current_roles = array();
			foreach($userdata as $key=>$value){
				if(preg_match("/^role_/",$key) and strtolower($value) == "y"){
					$current_roles[$key] = 'y';	
				}
			}
			$_SESSION['roles'] = $current_roles;
			
			//add permissions to session
			$current_perms = array();
			foreach($userdata as $key=>$value){
				if(preg_match("/^perm_/",$key) and strtolower($value) == "y"){
					$current_perms[$key] = 'y';	
				}
			}
			$_SESSION['perms'] = $current_perms;
			
			//add preferences to session
			$current_prefs = array();
			foreach($userdata as $key=>$value){
				if(preg_match("/^pref_/",$key) and strtolower($value) == "y"){
					$current_prefs[$key] = 'y';	
				}
			}
			$_SESSION['prefs'] = $current_prefs;
			
			//add qualifications to session
			$current_quals = array();
			foreach($userdata as $key=>$value){
				if(preg_match("/^qual_/",$key) and strtolower($value) == "y"){
					$current_quals[$key] = 'y';	
				}
			}
			$_SESSION['quals'] = $current_quals;
				
			
			
			//Check for address GET statements		//Add changes to session_control.php also
			if(isset($_POST['email_request'])){ //check if login is from email request
				$_SESSION['email_request'] = $_POST['email_request'];
				$_SESSION['urlGET'] = true;	
			}			
			if(isset($_POST['email_face'])){ //check if login is from email face sheet
				$_SESSION['email_face'] = $_POST['email_face'];
				$_SESSION['urlGET'] = true;
			}
			
			
			
			//Set login vars
			$_SESSION['login'] = $login_username;
			$_SESSION['user_name'] = $userdata['name'];
			$_SESSION['initials'] = $userdata['initials'];
			
			//set special timeout for dispatch
			if(strtolower($userdata['perm_extend_login']) == 'y'){
				$_SESSION['login_extend'] = $login_username;
			}
						
			d($_SESSION);
								
			
			$sql="UPDATE `users` SET `last_login`='$ts', `ip`='$ip', `domain`='$domain', `logged_in`='1', `linux_pwd`='$encoded_password' WHERE `uid`='$login_username' and `pwd`='$encrypted_mypassword'";
			s($sql);
			if(!$result = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
				$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
			}
			$sql="INSERT INTO `users_logins` (`uid`, `ip`, `domain`, `failed`, `pwd`, `type`, `browser`) VALUES ('$login_username', '$ip', '$domain', 'no', 'hidden', 'web', '$browser');";
			s($sql);
			if(!$result = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
				$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
			}
			
			$sal->auto_login_create();
			
			if(!$debug){
				header("location:login_success.php");
			}else{
				echo "Header Location login_success.php",EOL;
			}
		} else {
			$sql="UPDATE `users` SET `last_login`='$ts', `ip`='$ip', `domain`='$domain' WHERE `uid`='$login_username' and `pwd`='$encrypted_mypassword'";
			s($sql);
			if(!$result = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
				$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
			}
			$sql="INSERT INTO users_logins (`uid`, `ip`, `domain`, `failed`, `pwd`, `type`, `browser`) VALUES ('$login_username', '$ip', '$domain', 'no', 'hidden', 'web', '$browser');";
			s($sql);
			if(!$result = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
				$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
			}
			if(!$debug){
				$log->loginfo($username,1010,false,basename(__FILE__),__LINE__);
				header("location:inactive_user.php");
			}else{
				echo "Header Location inactive_user.php",EOL;	
			}
		}
	} else {
		$sql="INSERT INTO users_logins (`uid`, `ip`, `domain`, `pwd`, `failed`, `type`, `browser`) VALUES ('$login_username', '$ip', '$domain', '$login_password', 'yes', 'web', '$browser');";
		s($sql);
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
		if(!$debug){
			$log->loginfo($login_username,1011,false,basename(__FILE__),__LINE__);
			header("location:login_incorrect.php");
		}else{
			echo "Header Location login_incorrect.php",EOL;	
		}
	}
}
ob_end_flush();
$mysqli->close();

?>
