<?php
//Update Completed 11/25/14
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

$myusername = $_SESSION["login"];

if(isset($_GET['archived'])){$archive_only = true;}else{$archive_only = false;}
  
if($archive_only){
	$sql="SELECT f.facility_id, f.name, CONCAT(f.address,' ',f.city,', ',f.state,' ',f.zip) AS address, f.unique_id FROM facilities AS f WHERE f.property = 'A' ORDER BY f.name ASC;";
}else{
	$sql="SELECT f.facility_id, f.name, CONCAT(f.address,' ',f.city,', ',f.state,' ',f.zip) AS address, f.unique_id FROM facilities AS f WHERE f.property = 'C' ORDER BY f.name ASC;";
}
if(!$resultAllFacilities = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
?>

<!doctype html>
<html>
<head>
<?php require_once($head_include);?>
<?php require_once($css_include);?>
<link rel="stylesheet" type="text/css" href="/resources/css/portal.css">
<style>
	.dataTable th, .dataTable td {
		max-width: 200px;
		min-width: 70px;
		overflow: hidden;
		text-overflow: ellipsis;
		white-space: nowrap;
	}
</style>
<?php require_once($js_include);?>
<script type="text/javascript">
$(document).ready(function() {
	allTable = $('#allTable').dataTable({
		"bJQueryUI": true,
		"bStateSave": false,
		"iCookieDuration": 60*60*24*365, // 1 year
		"sPaginationType": "full_numbers",
		"fnInitComplete": function () {
			 this.$('tr').click( function () {
				 var href = $(this).find("a").attr("href");
				 if(href) {
					//window.location = href;
				 }
			 });
		},
		"fnDrawCallback" : function(oSettings) {
			var total_count = oSettings.fnRecordsTotal();
			var columns_in_row = $(this).children('thead').children('tr').children('th').length;
			var show_num = oSettings._iDisplayLength;
			var tr_count = $(this).children('tbody').children('tr').length;
			var missing = show_num - tr_count;
			if (show_num < total_count && missing > 0){
				for(var i = 0; i < missing; i++){
					$(this).append('<tr class="space"><td colspan="' + columns_in_row + '">&nbsp;</td></tr>'); 
				}
			}
			if (show_num > total_count) {
				for(var i = 0; i < (total_count - tr_count); i++) {
					$(this).append('<tr class="space"><td colspan="' + columns_in_row + '">&nbsp;</td></tr>'); 
				}
			}
		}	
	});
	
	//$("div#allTable_filter > label > input").focus();
	
	parent.$('body',document).find("#allTable_filter > label > input").focus();
		
	$(document).keypress(function(e) {
		var c = String.fromCharCode(e.which)
		if($("div#allTable_filter").children("label").children("input:focus").size() == 0){
			$("div#allTable_filter").children("label").children("input").val(c);
			$("div#allTable_filter").children("label").children("input").focus();	
		}
		
		
	});	
	$(document).keyup(function(e) {
		if($(".sorting_1").length == 1 && e.which == 13){
			$(".sorting_1").children("a").click();
		}
	});
	
	$(".button_jquery_create").button({
		icons: {
			primary: "ui-icon-disk"
		}
	});	
	
});
</script>
</head>
<body>
<?php if(isset($_SESSION['perms']['perm_edit_facilities'])){ ?>
<div id="create_system" style="width:99%; text-align:center;">
	<div class="button_jquery_create" style="width:50%; margin-bottom:15px; margin-left:auto; margin-right:auto; margin-top:5px;"><a onclick="javascript: self.parent.location='/report/common/facilities.php';" href="#" >Create New Facility</a></div>
</div>
<?php } ?>
<div id="allDiv" style="width:99%;">
	<table width="100%" id="allTable">
		<thead>
			<tr>
				<th>Facility ID</th>
				<th>Name</th>
				<th>Address</th>
			</tr>
		</thead>
		<tbody>
			<?php
				while($rowAllFacilities = $resultAllFacilities->fetch_assoc())
				{
					echo "<tr>\n";
					echo "<td><a onclick=\"javascript: self.parent.location='facilities.php?e&unique_id=".$rowAllFacilities['unique_id']."&user_id=$myusername';\" href=\"#\">". $rowAllFacilities['facility_id']."</a></td>\n";
					echo "<td>". $rowAllFacilities['name']."</td>\n";
					echo "<td>". $rowAllFacilities['address']."</td>\n";
					echo "</tr>\n";
				}
			?>
		</tbody>
	</table>
</div>
</body>
</html>