<?php 
//Update Completed 11/25/14
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

$sql = "SELECT c.id, c.title, c.department, c.location, c.created_date, u.name AS created_name, c.unique_id, c.closed
FROM iso_car AS c
LEFT JOIN users AS u ON u.uid = c.created_uid
ORDER BY c.id ASC;";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

?>
<!doctype html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<style>
.dataTable th, .dataTable td {
	max-width: 200px;
	min-width: 70px;
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
}
</style>
<?php require_once($js_include);?>

<script type="text/javascript">
$(document).ready(function() {
	allTable = $('#allTable').dataTable({
		"bJQueryUI": true,
		"bStateSave": false,
		"sPaginationType": "full_numbers",
		"fnInitComplete": function () {
			 this.$('tr').click( function () {
				 var href = $(this).find("a").attr("href");
				 if(href) {
					window.location = href;
				 }
			 });
		},
		"fnDrawCallback" : function(oSettings) {
			var total_count = oSettings.fnRecordsTotal();
			var columns_in_row = $(this).children('thead').children('tr').children('th').length;
			var show_num = oSettings._iDisplayLength;
			var tr_count = $(this).children('tbody').children('tr').length;
			var missing = show_num - tr_count;
			if (show_num < total_count && missing > 0){
				for(var i = 0; i < missing; i++){
					$(this).append('<tr class="space"><td colspan="' + columns_in_row + '">&nbsp;</td></tr>'); 
				}
			}
			if (show_num > total_count) {
				for(var i = 0; i < (total_count - tr_count); i++) {
					$(this).append('<tr class="space"><td colspan="' + columns_in_row + '">&nbsp;</td></tr>'); 
				}
			}
		}	
	});	
});
</script>
</head>
<body>
 <table width="100%" id="allTable">
                <thead>
                    <tr>
                        <th>ID</th>
						<th>Title</th>
						<th>Department</th>
						<th>Location</th>
						<th>Created Date</th>
						<th>Creating User</th> 
						<th>Closed</th> 
                    </tr>
                </thead>
                <tbody>
                    <?php
						while($row = $result->fetch_assoc()){
							echo "<tr onclick=\"javascript: self.parent.location='iso_car.php?view&unique_id=".$row['unique_id']."';\">\n";	
							echo "<td>". $row['id']."</td>\n";
							echo "<td>". $row['title']."</td>\n";
							echo "<td>". $row['department']."</td>\n";
							echo "<td>". $row['location']."</td>\n";
							echo "<td>". date(phpdispfd,strtotime($row['created_date']))."</td>\n";
							echo "<td>". $row['created_name']."</td>\n";
							echo "<td>". $row['closed']."</td>\n";
							echo "</tr>\n";
						}
						?>     
                </tbody>
            </table>
</body>
</html>