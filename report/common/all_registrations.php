<?php
//Update Completed 7/13/15

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

$myusername = $_SESSION["login"];

?>
<!doctype html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<style>
.dataTable th, .dataTable td {
	max-width: 200px;
	min-width: 70px;
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
}
</style>
<?php require_once($js_include);?>

<script type="text/javascript">
	var allTable = undefined;
$(document).ready(function() {
	allTable = $('#allUsersTable').DataTable({
		"bJQueryUI": true,
		//"bStateSave": true,
		//"iCookieDuration": 60*60*24*365, // 1 year
		"sPaginationType": "full_numbers",
		"fnInitComplete": function () {
			this.$('tr').click(function () {
				var href = $(this).find(".request_link").attr("href");
				if (href) {
					window.location = href;
				}
			});
		},
		"fnDrawCallback": function (oSettings) {
			var total_count = oSettings.fnRecordsTotal();
			var columns_in_row = $(this).children('thead').children('tr').children('th').length;
			var show_num = oSettings._iDisplayLength;
			var tr_count = $(this).children('tbody').children('tr').length;
			var missing = show_num - tr_count;
			if (show_num < total_count && missing > 0) {
				for (var i = 0; i < missing; i++) {
					$(this).append('<tr class="space"><td colspan="' + columns_in_row + '">&nbsp;</td></tr>');
				}
			}
			if (show_num > total_count) {
				for (var i = 0; i < (total_count - tr_count); i++) {
					$(this).append('<tr class="space"><td colspan="' + columns_in_row + '">&nbsp;</td></tr>');
				}
			}
		}
	});
		
	$(".button_jquery_create").button({
		icons: {
			primary: "ui-icon-disk"
		}
	});

	$(".remove_req").click(function () {
		var parent_tr = $(this).parents('tr');
		var link_id = $(this).attr('id');
		link_id = link_id.split('_');
		var req_id = link_id[1];
		if (confirm("Remove the request?")) {
			$.ajax({
				type: 'POST',
				url: 'scripts/remove_reg_req.php',
				data: {id: req_id},
				success: function (data) {
					console.log(data);
					if (parseInt(data) == 0) {
						alert("Request Removed");
						var row = allTable.row(parent_tr);
						var rowNode = row.node();
						row.remove();
						allTable.draw();
					}else{
						alert("Remove request failed");
					}

				}
			});
		}
	});

});
</script>
</head>
<body>
<div id="allUsersDiv" style="width:99%;">
   <?php  
		$sql="SELECT * FROM registration_requests WHERE used = 'n';";
		if(!$resultAllReg = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
	?>  
	<table width="100%" id="allUsersTable">
		<thead>
			<tr>
				<th>Name</th>
				<th>Address</th>
				<th>Phone</th>
				<th>email</th>
				<th>System(s)</th>
				<th>Email List</th>
				<th>Remove</th>
			</tr>
		</thead>
		<tbody>
		<?php
		while($rowAllReg = $resultAllReg->fetch_assoc()){
			echo "<tr>\n";
			if(strtolower($rowAllReg['email_list']) == 'n'){
				echo "<td><a class='request_link' onclick=\"javascript: self.parent.location='users.php?regid=" . $rowAllReg['id'] . "';\" href=\"\">" . $rowAllReg['name'] . "</a></td>\n";
				echo "<td>". $rowAllReg['address']." ".$rowAllReg['city'].", ".$rowAllReg['state']." ".$rowAllReg['zip']."</td>\n";
			}else{
				echo "<td><a class='request_link' onclick=\"javascript: self.parent.location='email_subscriptions.php?regid=" . $rowAllReg['id'] . "';\" href=\"\">" . $rowAllReg['name'] . "</a></td>\n";
				echo "<td></td>\n";
			}

			echo "<td>". $rowAllReg['phone']."</td>\n";
			echo "<td>". $rowAllReg['email']."</td>\n";
			echo "<td>". $rowAllReg['system_id']."</td>\n";
			echo "<td>". $rowAllReg['email_list']."</td>\n";
			echo "<td><a class='remove_req' id='remove_".$rowAllReg['id']."'>Remove</a></td>\n";
			echo "</tr>\n";
		}
		?>     
		</tbody>
	</table>
</div>
</body>
</html>