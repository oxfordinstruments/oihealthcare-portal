<?php
//Update Completed 11/25/14
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

if(!isset($_GET['unique_id'])){
	$log->logerr('all_reports.php',1016);
	header("location:/error.php?n=1016&p=all_reports.php");
}
$system_unique_id = $_GET['unique_id'];


$comp_word_limt 	 = intval($settings->complaint_word_limit);
$serv_word_limt		 = intval($settings->service_word_limit);
$custact_word_limt	 = intval($settings->custact_word_limit);
$probrpt_word_limt	 = intval($settings->probrpt_word_limit);


$sql="SELECT * FROM users;";
if(!$resultEngineers = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$arr_engineers = array();
while($rowEngineers = $resultEngineers->fetch_assoc())
{
	array_push_assoc($arr_engineers, $rowEngineers['uid'], $rowEngineers['name']);
}

function array_push_assoc(&$array, $key, $value){
	$array[$key] = $value;
	return $array;
}
function limit_words($string, $word_limit)
{
	$words = explode(" ",$string);
	if(count($words) > $word_limit){
		return implode(" ",array_splice($words,0,$word_limit))."...(more)";
	}else{
		return implode(" ",array_splice($words,0,$word_limit));
	}
}

?>
<!doctype html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<style>
	.dataTable th, .dataTable td {
		max-width: 200px;
		min-width: 70px;
		overflow: hidden;
		text-overflow: ellipsis;
		white-space: nowrap;
	}
</style>
<?php require_once($js_include);?>

<script type="text/javascript">
$(document).ready(function() {
	allTable = $('#allTable').dataTable({
		"bJQueryUI": true,
		"bStateSave": true,
		"iCookieDuration": 60*60*24*365, // 1 year
		"sPaginationType": "full_numbers",
		"fnInitComplete": function () {
			 this.$('tr').click( function () {
				 var href = $(this).find("a").attr("href");
				 if(href) {
					window.location = href;
				 }
			 });
		},
		"fnDrawCallback" : function(oSettings) {
			var total_count = oSettings.fnRecordsTotal();
			var columns_in_row = $(this).children('thead').children('tr').children('th').length;
			var show_num = oSettings._iDisplayLength;
			var tr_count = $(this).children('tbody').children('tr').length;
			var missing = show_num - tr_count;
			if (show_num < total_count && missing > 0){
				for(var i = 0; i < missing; i++){
					$(this).append('<tr class="space"><td colspan="' + columns_in_row + '">&nbsp;</td></tr>'); 
				}
			}
			if (show_num > total_count) {
				for(var i = 0; i < (total_count - tr_count); i++) {
					$(this).append('<tr class="space"><td colspan="' + columns_in_row + '">&nbsp;</td></tr>'); 
				}
			}
		}	
	});
});
</script>
</head>
<body>
    <?php  
		$sql="SELECT unique_id, `date`, report_id, system_nickname, assigned_engineer, complaint, service FROM systems_reports WHERE system_unique_id = '$system_unique_id' AND deleted = 'n' AND property = 'C'";
		if(!$resultAllReports = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
	?>  
    <table width="100%" id="allTable">
		<thead>
			<tr>
				<!--<th>id</th>-->
				<th>Date</th>
				<th width="50px">Report ID</th>
				<th>System Name</th>
				<th width="100px">Engineer</th>
				<th width="200px">Complaint</th>
				<th>Service</th>
			</tr>
		</thead>
		<tbody>
			<?php
			//<a onclick="parent.closeFancyboxAndRedirectToUrl('http://www.domain.com/page/');">Close and go to page</a>
				while($rowAllReport = $resultAllReports->fetch_assoc())
				{
					echo "<tr>\n";
					//echo "<td>".$rowAllReport['id']."</td>\n";
					echo "<td><a onclick=\"javascript: self.parent.location='report_view.php?id=".$rowAllReport['unique_id']."&uid=$myusername';\" href=\"\">". date(phpdispfd,strtotime($rowAllReport['date']))."</a></td>\n";
					echo "<td>".$rowAllReport['report_id']."</td>\n";
					echo "<td>".$rowAllReport['system_nickname']."</td>\n";
					echo "<td>". $arr_engineers[$rowAllReport['assigned_engineer']]."</td>\n";
					echo "<td>".limit_words($rowAllReport['complaint'],$comp_word_limt)."</td>\n";
					echo "<td>".limit_words($rowAllReport['service'],$serv_word_limt)."</td>\n";
					echo "</tr>\n";
				}
				?>     
		</tbody>
	</table>
</body>
</html>