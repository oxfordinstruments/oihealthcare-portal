<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

$myusername = $_SESSION["login"];

if(isset($_GET['mri'])){$mri_only = true;}else{$mri_only = false;}
if(isset($_GET['all'])){$all = true;}else{$all = false;}
  
if($mri_only){
	$sql="SELECT sbc.system_id, sbc.nickname, sbc.unique_id, sbc.pre_paid, sbc.credit_hold, sbc.property, sbc.ver, sbc.ver_unique_id
	FROM systems_base_cont AS sbc
	WHERE (sbc.property = 'C' OR sbc.property = 'F') AND sbc.mr_lhe_list = 'y'
	ORDER BY sbc.system_id LIKE 'CT%' DESC, sbc.system_id LIKE 'MR%' DESC, sbc.system_id LIKE 'NM%' DESC, sbc.system_id LIKE 'PCT%' DESC, sbc.system_id ASC;";
}elseif($all){
	$sql="SELECT sbc.system_id, sbc.nickname, sbc.unique_id, sbc.pre_paid, sbc.credit_hold, sbc.property, sbc.ver, sbc.ver_unique_id
	FROM systems_base_cont AS sbc
	ORDER BY sbc.system_id LIKE 'CT%' DESC, sbc.system_id LIKE 'MR%' DESC, sbc.system_id LIKE 'NM%' DESC, sbc.system_id LIKE 'PCT%' DESC, sbc.system_id ASC;";
}else{
	switch($_SESSION['role']){
		case "10": //contractor
			$sql="SELECT sbc.system_id, sbc.nickname, sbc.unique_id, sbc.credit_hold, sbc.pre_paid, sbc.property, sbc.ver, sbc.ver_unique_id
					FROM systems_base_cont AS sbc
					LEFT JOIN systems_assigned_pri AS a1 ON a1.system_ver_unique_id = sbc.ver_unique_id
					WHERE (sbc.property = 'C' OR sbc.property = 'F') AND a1.uid='".$_SESSION['login']."'
					ORDER BY sbc.system_id LIKE 'CT%' DESC, sbc.system_id LIKE 'MR%' DESC, sbc.system_id LIKE 'NM%' DESC, sbc.system_id LIKE 'PCT%' DESC, sbc.system_id ASC;";
			break;
		case "1": //engineer
		case "2": //dispatch
		case "3": //finance
		case "4": //management
		case "5": //basic
		case "6": //?
		case "9": //admin
			$sql="SELECT sbc.system_id, sbc.nickname, sbc.unique_id, sbc.pre_paid, sbc.credit_hold, sbc.property, sbc.ver, sbc.ver_unique_id, a1.uid
			FROM systems_base_cont AS sbc
			LEFT JOIN systems_assigned_pri AS a1 ON a1.system_ver_unique_id = sbc.ver_unique_id
			WHERE (sbc.property = 'C' OR sbc.property = 'F')
			ORDER BY sbc.system_id LIKE 'CT%' DESC, sbc.system_id LIKE 'MR%' DESC, sbc.system_id LIKE 'NM%' DESC, sbc.system_id LIKE 'PCT%' DESC, sbc.system_id ASC;";
			break;
	}
	
}
if(!$resultAllSystems = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

if(isset($_GET['q'])){
	$new_system = true;
	if($_GET['q'] != '2'){
		$new_system = false;	
	}
}
?>

<!doctype html>
<html>
<head>
<?php require_once($head_include);?>
<?php require_once($css_include);?>
<link rel="stylesheet" type="text/css" href="/resources/css/portal.css">
<?php require_once($js_include);?>
<script type="text/javascript">
$(document).ready(function() {
	allTable = $('#allTable').dataTable({
			"bJQueryUI": true,
			"bStateSave": false,
			//"iCookieDuration": 60*60*24*365, // 1 year
			"sPaginationType": "full_numbers",
			"aaSorting": [],
			"columnDefs": [
				{
					className: "allTableCenterCol", "targets": [ 2 ]
				},
				{
					"targets": [ 4 ],
    	            "visible": false,	
				}
			],
			"fnInitComplete": function () {
           		 this.$('tr').click( function () {
               		 var href = $(this).find("a").attr("href");
					 if(href) {
						//window.location = href;
					 }
           		 });
      		},
			"fnDrawCallback" : function(oSettings) {
				var total_count = oSettings.fnRecordsTotal();
				var columns_in_row = $(this).children('thead').children('tr').children('th').length;
				var show_num = oSettings._iDisplayLength;
				var tr_count = $(this).children('tbody').children('tr').length;
				var missing = show_num - tr_count;
				if (show_num < total_count && missing > 0){
					for(var i = 0; i < missing; i++){
						$(this).append('<tr class="space"><td colspan="' + columns_in_row + '">&nbsp;</td></tr>'); 
					}
				}
				if (show_num > total_count) {
					for(var i = 0; i < (total_count - tr_count); i++) {
						$(this).append('<tr class="space"><td colspan="' + columns_in_row + '">&nbsp;</td></tr>'); 
					}
				}
			}			
	});
	//$("div#allTable_filter > label > input").focus();
	
	parent.$('body',document).find("#allTable_filter > label > input").focus();
		
	$(document).keypress(function(e) {
		var c = String.fromCharCode(e.which)
		if($("div#allTable_filter").children("label").children("input:focus").size() == 0){
			$("div#allTable_filter").children("label").children("input").val(c);
			$("div#allTable_filter").children("label").children("input").focus();	
		}
		
		
	});	
	$(document).keyup(function(e) {
		if($(".sorting_1").length == 1 && e.which == 13){
			$(".sorting_1").children("a").click();
		}
	});
	
	$(".button_jquery_create").button({
		icons: {
			primary: "ui-icon-disk"
		}
	});	
	
});
</script>
</head>
<body>
<?php if(isset($_SESSION['perms']['perm_edit_systems']) and $new_system){ ?>
<div id="create_system" style="width:99%; text-align:center;">
	<div class="button_jquery_create" style="width:50%; margin-bottom:15px; margin-left:auto; margin-right:auto; margin-top:5px;"><a onclick="javascript: self.parent.location='/report/common/systems.php';" href="#" >Create New System</a></div>
</div>
<?php } ?>
<div id="allDiv" style="width:99%;">
	<table width="100%" id="allTable">
		<thead>
			<tr>
				<th>System ID</th>
				<th>System Nickname</th>
				<th>Status</th>
				<th>Ver</th>
				<th>Eng</th>
			</tr>
		</thead>
		<tbody>
			<?php
			//<a onclick="parent.closeFancyboxAndRedirectToUrl('http://www.domain.com/page/');">Close and go to page</a>
				while($rowAllSystems = $resultAllSystems->fetch_assoc())
				{
					echo "<tr class='";
					if(strtolower($rowAllSystems['credit_hold']) == 'y'){
						echo "credit_hold ";
					}
					if(strtolower($rowAllSystems['pre_paid']) == 'y'){
						echo "pre_paid ";
					}
					if(strtolower($rowAllSystems['property']) == 'f'){
						echo "future ";
					}
					if(strtolower($rowAllSystems['property']) == 'a'){
						echo "archived ";
					}
					if(strtolower($rowAllSystems['property']) == 'd'){
						echo "deleted ";
					}
					echo "'>\n";
					if(strtolower($rowAllSystems['property']) != 'd'){
						if(isset($_GET['q'])){
							switch($_GET['q']){
								case 0:
									echo "<td><a onclick=\"javascript: self.parent.location='request_new_edit.php?unique_id=" . $rowAllSystems['unique_id'] . "&ver_unique_id=" . $rowAllSystems['ver_unique_id'] . "&version=" . $rowAllSystems['ver'] . "&user_id=$myusername';\" href=\"#\">" . $rowAllSystems['system_id'] . "</a></td>\n";
									break;
								case 1:
									echo "<td><a onclick=\"javascript: self.parent.location='systems_view.php?ver_unique_id=" . $rowAllSystems['ver_unique_id'] . "&user_id=$myusername';\" href=\"#\">" . $rowAllSystems['system_id'] . "</a></td>\n";
									break;
								case 2:
									echo "<td><a onclick=\"javascript: self.parent.location='systems.php?ver_unique_id=" . $rowAllSystems['ver_unique_id'] . "&e';\" href=\"#\">" . $rowAllSystems['system_id'] . "</a></td>\n";
									break;
								case 3: //set $mri_only to true before
									echo "<td><a onclick=\"javascript: self.parent.location='mri_readings_input.php?single&unique_id=" . $rowAllSystems['unique_id'] . "&version=" . $rowAllSystems['ver'] . "';\" href=\"#\">" . $rowAllSystems['system_id'] . "</a></td>\n";
									break;
								case 4: //set $mri_only to true before
									echo "<td><a onclick=\"javascript: self.parent.location='reports/mri_readings_system.php?&unique_id=" . $rowAllSystems['unique_id'] . "&version=" . $rowAllSystems['ver'] . "';\" href=\"#\">" . $rowAllSystems['system_id'] . "</a></td>\n";
									break;
								//case 5: //set $face_only to true before
								//echo "<td><a onclick=\"javascript: self.parent.location='face_signoff.php?&unique_id=".$rowAllSystems['unique_id']."&version=".$rowAllSystems['ver']."';\" href=\"#\">". $rowAllSystems['system_id']."</a></td>\n";
								//break;
								case 6:
									echo "<td><a onclick=\"javascript: self.parent.location='reports/face_sheets.php?ver_unique_id=" . $rowAllSystems['ver_unique_id'] . "';\" href=\"#\">" . $rowAllSystems['system_id'] . "</a></td>\n";
									//echo "<td><a onclick=\"javascript: self.parent.location='reports/face_sheets.php?ver_unique_id=".$rowAllSystems['ver_unique_id']."</a></td>\n";
									break;
								case 7:
									echo "<td><a onclick=\"javascript: self.parent.location='reports/system_service_history.php?&unique_id=" . $rowAllSystems['unique_id'] . "&version=" . $rowAllSystems['ver'] . "';\" href=\"#\">" . $rowAllSystems['system_id'] . "</a></td>\n";
									break;
							}
						}else{
							//echo "<td><a onclick=\"parent.closeFancyboxAndRedirectToUrl('request_new_edit.php?system_id=".$rowAllSystems['system_id']."&user_id=$myusername');\" href=\"#\">". $rowAllSystems['system_id']."</a></td>\n";
							echo "<td><a onclick=\"javascript: self.parent.location='request_new_edit.php?unique_id=" . $rowAllSystems['unique_id'] . "&version=" . $rowAllSystems['ver'] . "&user_id=$myusername';\" href=\"#\">" . $rowAllSystems['system_id'] . "</a></td>\n";
						}
					}else{
						echo "<td>" . $rowAllSystems['system_id'] . "</a></td>\n";
					}
					echo "<td>". $rowAllSystems['nickname']."</td>\n";
					switch (strtolower($rowAllSystems['property'])){
						case 'f':
							echo "<td>Future</td>\n";
							break;
						case 'c':
							echo "<td>Current</td>\n";
							break;
						case 'a':
							echo "<td>Archived</td>\n";
							break;
						case 'd':
							echo "<td>Deleted</td>\n";
							break;
					}
					echo "<td>".$rowAllSystems['ver']."</td>";
					echo "<td>".$rowAllSystems['uid']."</td>";
					echo "</tr>\n";
				}
			?>
		</tbody>
	</table>
</div>
<div id="legend" style="text-align:center; padding-top:15px;">
Color Legend:&nbsp;<span class="credit_hold">Credit Hold</span>&emsp;<span class="pre_paid">Pre-Paid</span>&emsp;<span class="future">Future</span><?php if($all){?>&emsp;<span class="archived">Archived</span>&emsp;<span class="deleted">Deleted</span><?php } ?>
</div>
</body>
</html>