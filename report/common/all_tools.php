<?php 
//Update Completed 11/25/14
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

$sql = "SELECT t.unique_id, t.id, t.active, t.calibrating, t.tool, t.model, t.serial, t.mfg, u.name, tc2.id as cal_id, tc2.date, tc2.expire
FROM
(
SELECT tc.unique_id, MAX(tc.id) AS tc_id
FROM tools_calibrations AS tc
GROUP BY tc.unique_id
) AS tc1
LEFT JOIN tools_calibrations AS tc2 ON tc2.id = tc1.tc_id
RIGHT JOIN tools AS t ON t.unique_id = tc1.unique_id
LEFT JOIN misc_offices AS mo ON mo.id = t.office
LEFT JOIN users AS u ON u.uid = t.uid
WHERE archived = 'n';";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

if(isset($_SESSION['perms']['perm_tool_cal_edit'])){
	$option = "edit";	
}else{
	$option = "view";	
}


?>
<!doctype html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<style>
	.dataTable th, .dataTable td {
		max-width: 200px;
		min-width: 70px;
		overflow: hidden;
		text-overflow: ellipsis;
		white-space: nowrap;
	}
</style>
<?php require_once($js_include);?>

<script type="text/javascript">
$(document).ready(function() {
	allTable = $('#allTable').dataTable({
		"bJQueryUI": true,
		"bStateSave": false,
		"sPaginationType": "full_numbers",
		"fnInitComplete": function () {
			 this.$('tr').click( function () {
				 var href = $(this).find("a").attr("href");
				 if(href) {
					window.location = href;
				 }
			 });
		},
		"fnDrawCallback" : function(oSettings) {
			var total_count = oSettings.fnRecordsTotal();
			var columns_in_row = $(this).children('thead').children('tr').children('th').length;
			var show_num = oSettings._iDisplayLength;
			var tr_count = $(this).children('tbody').children('tr').length;
			var missing = show_num - tr_count;
			if (show_num < total_count && missing > 0){
				for(var i = 0; i < missing; i++){
					$(this).append('<tr class="space"><td colspan="' + columns_in_row + '">&nbsp;</td></tr>'); 
				}
			}
			if (show_num > total_count) {
				for(var i = 0; i < (total_count - tr_count); i++) {
					$(this).append('<tr class="space"><td colspan="' + columns_in_row + '">&nbsp;</td></tr>'); 
				}
			}
		}	
	});	
	
	$(".button_jquery_create").button({
		icons: {
			primary: "ui-icon-disk"
		}
	});
});
</script>
</head>
<body>
<?php if(isset($_SESSION['perms']['perm_tool_cal_edit'])){ ?>
<div id="create_system" style="width:99%; text-align:center;">
	<div class="button_jquery_create" style="width:50%; margin-bottom:15px; margin-left:auto; margin-right:auto; margin-top:5px;"><a onclick="javascript: self.parent.location='/report/common/tools.php';" >Create New Tool</a></div>
</div>
<?php } ?>
 <table width="100%" id="allTable">
                <thead>
                    <tr>
                        <th>ID</th>
						<th>Tool</th>
						<th>Model</th>
						<th>Serial</th>
						<th>Calibrated</th>
						<th>Expire</th> 
						<th>Being Cal'd</th> 
						<th>Active</th> 
                    </tr>
                </thead>
                <tbody>
                    <?php
						while($row = $result->fetch_assoc()){
							echo "<tr onclick=\"javascript: self.parent.location='tools.php?".$option."&unique_id=".$row['unique_id']."';\">\n";	
							echo "<td>". $row['id']."</td>\n";
							echo "<td>". $row['tool']."</td>\n";
							echo "<td>". $row['model']."</td>\n";
							echo "<td>". $row['serial']."</td>\n";
							echo "<td>". date(phpdispfd,strtotime($row['date']))."</td>\n";
							echo "<td>". date(phpdispfd,strtotime($row['expire']))."</td>\n";
							if(strtolower($row['calibrating']) == 'y'){
								echo "<td>Yes</td>\n";
							}else{
								echo "<td>No</td>\n";
							}
							if(strtolower($row['active']) == 'y'){
								echo "<td>Yes</td>\n";
							}else{
								echo "<td>No</td>\n";
							}
							echo "</tr>\n";
						}
						?>     
                </tbody>
            </table>
</body>
</html>