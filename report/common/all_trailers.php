<?php 

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

$sql = "SELECT t.*, sb.system_id
FROM trailers AS t
LEFT JOIN systems_base AS sb ON sb.unique_id = t.system_unique_id;";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}



?>
<!doctype html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<style>
	.dataTable th, .dataTable td {
		max-width: 200px;
		min-width: 70px;
		overflow: hidden;
		text-overflow: ellipsis;
		white-space: nowrap;
	}
</style>
<?php require_once($js_include);?>

<script type="text/javascript">
$(document).ready(function() {
	allTable = $('#allTable').dataTable({
		"bJQueryUI": true,
		"bStateSave": false,
		"sPaginationType": "full_numbers",
		"fnInitComplete": function () {
			 this.$('tr').click( function () {
				 var href = $(this).find("a").attr("href");
				 if(href) {
					window.location = href;
				 }
			 });
		},
		"fnDrawCallback" : function(oSettings) {
			var total_count = oSettings.fnRecordsTotal();
			var columns_in_row = $(this).children('thead').children('tr').children('th').length;
			var show_num = oSettings._iDisplayLength;
			var tr_count = $(this).children('tbody').children('tr').length;
			var missing = show_num - tr_count;
			if (show_num < total_count && missing > 0){
				for(var i = 0; i < missing; i++){
					$(this).append('<tr class="space"><td colspan="' + columns_in_row + '">&nbsp;</td></tr>'); 
				}
			}
			if (show_num > total_count) {
				for(var i = 0; i < (total_count - tr_count); i++) {
					$(this).append('<tr class="space"><td colspan="' + columns_in_row + '">&nbsp;</td></tr>'); 
				}
			}
		}	
	});	
	
	$(".button_jquery_create").button({
		icons: {
			primary: "ui-icon-disk"
		}
	});
});
</script>
</head>
<body>
<?php if(isset($_SESSION['perms']['perm_edit_trailers'])){ ?>
<div id="create_system" style="width:99%; text-align:center;">
	<div class="button_jquery_create" style="width:50%; margin-bottom:15px; margin-left:auto; margin-right:auto; margin-top:5px;"><a onclick="javascript: self.parent.location='/report/common/trailers.php';" >Create New Trailer</a></div>
</div>
<?php } ?>
 <table width="100%" id="allTable">
                <thead>
                    <tr>
                        <th>ID</th>
						<th>Mfg</th>
						<th>VIN</th>
						<th>License</th>
						<th>Assigned System</th> 
						<th>Archived</th> 
                    </tr>
                </thead>
                <tbody>
                    <?php
						while($row = $result->fetch_assoc()){
							echo "<tr onclick=\"javascript: self.parent.location='trailers.php?e&unique_id=".$row['unique_id']."';\">\n";	
							echo "<td>". $row['trailer_id']."</td>\n";
							echo "<td>". $row['mfg']."</td>\n";
							echo "<td>". $row['vin']."</td>\n";
							echo "<td>". $row['license_numer']."</td>\n";
							echo "<td>". $row['system_id']."</td>\n";
							if(strtolower($row['property']) == 'A'){
								echo "<td>Yes</td>\n";
							}else{
								echo "<td>No</td>\n";
							}
							echo "</tr>\n";
						}
						?>     
                </tbody>
            </table>
</body>
</html>