<?php
//Update Completed 11/25/14
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

$myusername = $_SESSION["login"];

?>
<!doctype html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<style>
	.dataTable th, .dataTable td {
		max-width: 200px;
		min-width: 70px;
		overflow: hidden;
		text-overflow: ellipsis;
		white-space: nowrap;
	}
</style>
<?php require_once($js_include);?>

<script type="text/javascript">
$(document).ready(function() {
	allTable = $('#allUsersTable').dataTable({
			"bJQueryUI": true,
			//"bStateSave": true,
			//"iCookieDuration": 60*60*24*365, // 1 year
			"sPaginationType": "full_numbers",
			"fnInitComplete": function () {
           		 this.$('tr').click( function () {
               		 var href = $(this).find("a").attr("href");
					 if(href) {
						window.location = href;
					 }
           		 });
       		},
			"fnDrawCallback" : function(oSettings) {
				var total_count = oSettings.fnRecordsTotal();
				var columns_in_row = $(this).children('thead').children('tr').children('th').length;
				var show_num = oSettings._iDisplayLength;
				var tr_count = $(this).children('tbody').children('tr').length;
				var missing = show_num - tr_count;
				if (show_num < total_count && missing > 0){
					for(var i = 0; i < missing; i++){
						$(this).append('<tr class="space"><td colspan="' + columns_in_row + '">&nbsp;</td></tr>'); 
					}
				}
				if (show_num > total_count) {
					for(var i = 0; i < (total_count - tr_count); i++) {
						$(this).append('<tr class="space"><td colspan="' + columns_in_row + '">&nbsp;</td></tr>'); 
					}
				}
			}	
		});
		
		$(".button_jquery_create").button({
		icons: {
			primary: "ui-icon-disk"
		}
	});	
});
</script>
</head>
<body>
<?php if(isset($_SESSION['perms']['perm_edit_users'])){ ?>
<div id="create_user" style="width:99%; text-align:center;">
	<div class="button_jquery_create" style="width:50%; margin-bottom:15px; margin-left:auto; margin-right:auto; margin-top:5px;"><a onclick="javascript: self.parent.location='/report/common/users.php';" href="#" >Create New User</a></div>
</div>
<?php } ?>
<div id="allUsersDiv" style="width:99%;">
   <?php  
		$sql="SELECT * FROM users WHERE id >= 10 ORDER BY `name` ASC";
		if(!$resultAllUsers = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
	?>  
	<table width="100%" id="allUsersTable">
		<thead>
			<tr>
				<th>Login</th>
				<th>Full Name</th>
			</tr>
		</thead>
		<tbody>
		<?php
		while($rowAllUsers = $resultAllUsers->fetch_assoc()){
			echo "<tr>\n";
			if(isset($_GET['q'])){
				switch ($_GET['q']) {
					case 0:
						echo "<td><a onclick=\"javascript: self.parent.location='users.php?uid=".$rowAllUsers['uid']."';\" href=\"\">". $rowAllUsers['uid']."</a></td>\n";
						break;
					}
			}else{
				echo "<td><a onclick=\"javascript: self.parent.location='users.php?uid=".$rowAllUsers['uid']."';\" href=\"\">". $rowAllUsers['uid']."</a></td>\n";
			}
			echo "<td>". $rowAllUsers['name']."</td>\n";
			
			echo "</tr>\n";
		}
		?>     
		</tbody>
	</table>
</div>
</body>
</html>