<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

$sql="SELECT uid,name FROM users WHERE active='y' AND id > 5 ORDER BY firstname,lastname ASC;";
$resultCalUsers = $mysqli->query($sql);
?>

<!doctype html>
<head>
<?php require_once($head_include);?>
</head>
<script src="/resources/scheduler/codebase/dhtmlxscheduler.js" type="text/javascript" charset="utf-8"></script>
<script src="/resources/scheduler/codebase/ext/dhtmlxscheduler_minical.js" type="text/javascript" charset="utf-8"></script>
<script src="/resources/scheduler/codebase/ext/dhtmlxscheduler_readonly.js" type="text/javascript" charset="utf-8"></script>
<script src="/resources/scheduler/codebase/ext/dhtmlxscheduler_expand.js" type="text/javascript" charset="utf-8"></script>


<link rel="stylesheet" href="/resources/scheduler/codebase/dhtmlxscheduler.css" type="text/css" title="no title" charset="utf-8">
<link rel="stylesheet" href="/resources/css/calendar.css" type="text/css" title="no title" charset="utf-8">

<script type="text/javascript" charset="utf-8">
	function init() {		
		var users = [
			{ key:'',label:'None'},
			<?php 
			$output = array ();
			while($rowCalUsers = $resultCalUsers->fetch_assoc()){
				$temp = "{ key:'".$rowCalUsers['uid']."',label:'".$rowCalUsers['name']."'}";
				array_push($output,$temp);
			}
			echo implode(",",$output);
			?>
		];
		
		var subject = [
			{ key:'',label:'Misc'},
			{ key:'service-ct',label:'Service-CT'},
			{ key:'service-mr',label:'Service-MR'},
			{ key:'service-misc',label:'Service-MISC'},
			{ key:'pm',label:'PM'},
			{ key:'vacation',label:'Vacation'},
			{ key:'meeting',label:'Meeting'},
			{ key:'sick_day',label:'Sick Day'}
        ];

		scheduler.templates.event_class=function(start, end, event){
			if(event.subject){ // if event has subject property then special class should be assigned
				return "event_"+event.subject;}
			return "";
        };
		
		scheduler.filter_month = scheduler.filter_day = scheduler.filter_week = function(id, event) {
			if (document.getElementById("show_only_me").checked) {
				if(event.uid != "<?php echo $_SESSION['login']; ?>"){return false;}
			}
			if (!document.getElementById("service-mr").checked){
				if(event.subject == "service-mr"){return false;}
			}
			if (!document.getElementById("service-ct").checked){
				if(event.subject == "service-ct"){return false;}
			}
			if (!document.getElementById("service-misc").checked){
				if(event.subject == "service-misc"){return false;}
			}
			if (!document.getElementById("pm").checked){
				if(event.subject == "pm"){return false;}
			}
			if (!document.getElementById("meeting").checked){
				if(event.subject == "meeting"){return false;}
			}
			if (!document.getElementById("vacation").checked){
				if(event.subject == "vacation"){return false;}
			}
			if (!document.getElementById("misc").checked){
				if(event.subject == ""){return false;}
			}
			if (!document.getElementById("sick_day").checked){
				if(event.subject == "sick_day"){return false;}
			}

			return true;
		};

		
		
		scheduler.xy.menu_width = 0;
		scheduler.config.xml_date="%Y-%m-%d %H:%i";
		scheduler.config.prevent_cache = true;
		scheduler.config.show_loading = true;
		scheduler.config.lightbox.sections=[	
			{name:"description", height:40, type:"textarea", map_to:"text", focus:true},
			{name:"detail", height:130, type:"textarea", map_to:"detail"},
			{name:"location", height:40, type:"textarea", map_to:"location" },
			{name:"subject", height:20, type:"select", map_to:"subject", options:subject, },
			{name:"select", height:20, type:"select", map_to:"uid", options:users, default_value:"<?php echo $_SESSION['login']; ?>"},
			{name:"time", height:72, type:"time", map_to:"auto"}
		]
		scheduler.config.first_hour=0;
		scheduler.locale.labels.section_detail="Details";
		scheduler.locale.labels.section_subject="Event Type";
		scheduler.locale.labels.section_location="Location";
		scheduler.locale.labels.section_select = 'My Name';
		scheduler.config.start_on_monday = false;
		scheduler.config.multi_day = true;
		scheduler.config.max_month_events = 5;
		scheduler.config.time_step = 30;
		scheduler.config.full_day = true; // enable parameter to get full day event option on the lightbox form
		scheduler.config.details_on_dblclick=true;
		scheduler.config.details_on_create = true;
		scheduler.config.event_duration = 60; //specify event duration in minutes for auto end time
		scheduler.config.auto_end_date = true;
		function block_readonly(id){
					if (!id) return true;
					return !this.getEvent(id).readonly;
		}
		scheduler.attachEvent("onBeforeDrag",block_readonly)
		scheduler.attachEvent("onClick",block_readonly)		
		scheduler.attachEvent("onEventSave",function(id,data){
			if (!data.text) {
				alert("Description must not be empty");
				return false;
			}
			if (data.text == "New event") {
				alert("Description must not be \"New event\"");
				return false;
			}
			if (!data.uid) {
				alert("Must select a user");
				return false;
			}
			return true;
		});		
		scheduler.init('scheduler_here',new Date(),"month");
		scheduler.setLoadMode("month")
		scheduler.load("events.php");
		
		var dp = new dataProcessor("events.php");
		dp.init(scheduler);

	}
	function updateScheduler(){
		scheduler.updateView();
	}
	function show_minical(){
		if (scheduler.isCalendarVisible())
			scheduler.destroyCalendar();
		else
			scheduler.renderCalendar({
				position:"dhx_minical_icon",
				date:scheduler._date,
				navigation:true,
				handler:function(date,calendar){
					scheduler.setCurrentView(date);
					scheduler.destroyCalendar()
				}
			});
	}
</script>

<body onload="init();">
<div style='height:20px; padding:5px;'>
	<div class="filters_wrapper" id="filters_wrapper">
		<span>Display:</span>
		<label>
			<input type="checkbox" name="show_only_me" id="show_only_me" onChange="updateScheduler();"/>
			My Events Only
		</label>
		<label>
			<input type="checkbox" name="service-ct" id="service-ct" onChange="updateScheduler();" checked/>
			Service-CT
		</label>
		<label>
			<input type="checkbox" name="service-mr" id="service-mr" onChange="updateScheduler();" checked/>
			Service-MR
		</label>
		<label>
			<input type="checkbox" name="service-misc" id="service-misc" onChange="updateScheduler();" checked/>
			Service-MISC
		</label>
		<label>
			<input type="checkbox" name="pm" id="pm" onChange="updateScheduler();" checked/>
			PM
		</label>
		<label>
			<input type="checkbox" name="meeting" id="meeting" onChange="updateScheduler();" checked/>
			Meetings
		</label>
		<label>
			<input type="checkbox" name="vacation" id="vacation" onChange="updateScheduler();" checked/>
			Vacations
		</label>
		<label>
			<input type="checkbox" name="misc" id="misc" onChange="updateScheduler();" checked/>
			Misc
		</label>
		<label>
			<input type="checkbox" name="sick_day" id="sick_day" onChange="updateScheduler();" checked/>
			Sick Days
		</label>
	</div>
</div>
   <div id="scheduler_here" class="dhx_cal_container" style='width:100%; height:100%;'>
      <div class="dhx_cal_navline">
         <div class="dhx_cal_prev_button">&nbsp;</div>
         <div class="dhx_cal_next_button">&nbsp;</div>
         <div class="dhx_cal_today_button"></div>
         <div class="dhx_cal_date"></div>
         <div class="dhx_minical_icon" id="dhx_minical_icon" onclick="show_minical()">&nbsp;</div>
         <div class="dhx_cal_tab" name="day_tab" style="right:204px;"></div>
         <div class="dhx_cal_tab" name="week_tab" style="right:140px;"></div>
         <div class="dhx_cal_tab" name="month_tab" style="right:76px;"></div>
      </div>
      <div class="dhx_cal_header">
      </div>
      <div class="dhx_cal_data">
      </div>
   </div>
</body>