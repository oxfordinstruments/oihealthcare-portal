<?php
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Easy set variables
	 */
	$debug = false;
	if(isset($_GET['debug'])){
		$debug = true;	
	}
	
	require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
	if(!$debug){
		Kint::enabled(false);	
	}
	
	//die(print_r($_GET));
	
	/* Indexed column (used for fast and accurate table cardinality) */
	$sIndexColumn = "r.id";
	$settings = new SimpleXMLElement($_SERVER['DOCUMENT_ROOT'].'/settings.xml', null, true);
	require_once($_SERVER['DOCUMENT_ROOT'].'/define_inc.php');

	
	include($_SERVER['DOCUMENT_ROOT'].'/mysqlInfo.php');

	/* Array of database columns which should be read and sent back to DataTables. Use a space where
	 * you want to insert a non-database field (for example a counter or static image)
	 */
	$aColumns = array( 'r.id', 'r.`date`', 'r.report_id', 'r.system_id', 'r.system_nickname', 'r.assigned_engineer', 'r.complaint', 'r.service', 'r.unique_id', 'timezone');
	$aColumnsSelect = array( 'r.id', 'r.`date`', 'r.report_id', 'r.system_id', 'r.system_nickname', 'r.assigned_engineer', 'r.complaint', 'r.service', 'r.unique_id', "IFNULL(f.timezone,'UTC') AS timezone");
	$aColumnsName = array( 'id', 'date', 'report_id', 'system_id', 'system_nickname', 'assigned_engineer', 'complaint', 'service', 'unique_id', "timezone");
		
	/* DB table to use */
	$sTable = "systems_reports AS r";
	
	/* Database connection information */
	$gaSql['user']       = "$username";
	$gaSql['password']   = "$password";
	$gaSql['db']         = "$db_name";
	$gaSql['server']     = "$host";
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP server-side, there is
	 * no need to edit below this line
	 */
	
	/* 
	 * Local functions
	 */
	function fatal_error ( $sErrorMessage = '' )
	{
		header( $_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error' );
		die( $sErrorMessage );
	}

	
	/* 
	 * MySQL connection
	 */
	if ( ! $gaSql['link'] = mysql_pconnect( $gaSql['server'], $gaSql['user'], $gaSql['password']  ) )
	{
		fatal_error( 'Could not open connection to server' );
	}

	if ( ! mysql_select_db( $gaSql['db'], $gaSql['link'] ) )
	{
		fatal_error( 'Could not select database ' );
	}
	
	/*
	 * Get engineers' names JRD
	 */
	
	$resultEngineers = mysql_query("SELECT u.uid, u.name
	FROM users AS u
	LEFT JOIN users_roles AS ur ON ur.uid = u.uid
	LEFT JOIN users_role_id AS rid ON rid.id = ur.rid
	WHERE u.active = 'y' AND (rid.role = 'role_engineer' OR rid.role = 'role_contractor');");
	$arr_engineers = array();
	while($rowEngineers = mysql_fetch_array($resultEngineers))
	{
		array_push_assoc($arr_engineers, $rowEngineers['uid'], $rowEngineers['name']);
	}
	
	function array_push_assoc(&$array, $key, $value){
		$array[$key] = $value;
		return $array;
	}
	
	/* 
	 * Paging
	 */
	$sLimit = "";
	if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
	{
		$sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".
			intval( $_GET['iDisplayLength'] );
	}
	
	
	/*
	 * Ordering
	 */
	
	$sOrder = "ORDER BY r.report_id ASC";
	//$sOrder = "ORDER BY STR_TO_DATE(`date`,'%m/%d/%Y %H:%i') ASC";
	//$sOrder = "";
	if ( isset( $_GET['iSortCol_0'] ) )
	{
		$sOrder = "ORDER BY  ";
		for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
		{
			if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
			{
				$sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
			}
		}
		
		$sOrder = substr_replace( $sOrder, "", -2 );
		if ( $sOrder == "ORDER BY" )
		{
			$sOrder = "";			
		}
	}
	
	
	
	
	/* 
	 * Filtering
	 * NOTE this does not match the built-in DataTables filtering which does it
	 * word by word on any field. It's possible to do here, but concerned about efficiency
	 * on very large tables, and MySQL's regex functionality is very limited
	 */
	 	//$sWhere = "";
		//JRD add below if statement
		if($_GET['oiall'] == "1"){
			$sWhere = "WHERE (r.`status`='closed')";
		}else{
			$sWhere = "WHERE (r.`status`='closed' AND r.`engineer` = '".$_GET['oiuid']."' AND r.deleted = 'n')";	
		}
		if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
		{
			if($_GET['oiall'] == "1"){
				$sWhere = "WHERE (r.`status`='closed' AND r.deleted = 'n') AND (";
			}else{
				$sWhere = "WHERE (r.`status`='closed' AND r.`engineer` = '".$_GET['oiuid']."' AND r.deleted = 'n') AND (";	
			}
			//$sWhere = "WHERE (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$sWhere .= $aColumns[$i]." LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ") ";
		}
	
	/* Individual column filtering */
	for ( $i=0 ; $i<count($aColumns) ; $i++ )
	{
		if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
		{
			if ( $sWhere == "" )
			{
				$sWhere = "WHERE ";
			}
			else
			{
				$sWhere .= " AND ";
			}
			$sWhere .= $aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
		}
	}
	
	
	/*
	 * SQL queries
	 * Get data to display
	 */
	$sQuery = "
		SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumnsSelect))."
		FROM   $sTable
		LEFT JOIN systems_base_cont AS sbc ON sbc.ver_unique_id = r.system_ver_unique_id
		LEFT JOIN facilities AS f ON f.unique_id = sbc.facility_unique_id
		$sWhere
		$sOrder
		$sLimit
		";
	
	d($sQuery);
	$rResult = mysql_query( $sQuery, $gaSql['link'] ) or fatal_error( 'MySQL Error: ' . mysql_errno() . $sQuery );
	
	/* Data set length after filtering */
	$sQuery = "
		SELECT FOUND_ROWS()
	";
	$rResultFilterTotal = mysql_query( $sQuery, $gaSql['link'] ) or fatal_error( 'MySQL Error: ' . mysql_errno() . $sQuery);
	$aResultFilterTotal = mysql_fetch_array($rResultFilterTotal);
	$iFilteredTotal = $aResultFilterTotal[0];
	
	/* Total data set length */
	$sQuery = "
		SELECT COUNT(".$sIndexColumn.")
		FROM   $sTable
	";
	$rResultTotal = mysql_query( $sQuery, $gaSql['link'] ) or fatal_error( 'MySQL Error: ' . mysql_errno() . $sQuery );
	$aResultTotal = mysql_fetch_array($rResultTotal);
	$iTotal = $aResultTotal[0];
	
	
	/*
	 * Output
	 */
	$output = array(
		"sEcho" => intval($_GET['sEcho']),
		"iTotalRecords" => $iTotal,
		"iTotalDisplayRecords" => $iFilteredTotal,
		"aaData" => array()
	);
	
	while($aRow = mysql_fetch_array($rResult)){
		$row = array();
		for ( $i=0; $i<count($aColumns); $i++){
			if ($aColumnsName[$i] == "assigned_engineer"){
				$row[] = $arr_engineers[ $aRow[ $aColumnsName[$i] ] ];
			}else if($aColumns[$i] != ' '){
				/* General output */
				$row[] = limit_words($aRow[ $aColumnsName[$i] ],intval($settings->completed_report_table_word_limit));
			}
		}
		$output['aaData'][] = $row;
	}
	
	d($output);
	echo json_encode( $output );
	
	
function limit_words($string, $word_limit)
{
	$words = explode(" ",$string);
	if(count($words) > $word_limit){
		return implode(" ",array_splice($words,0,$word_limit))."...(truncated)";
	}else{
		return implode(" ",array_splice($words,0,$word_limit));
	}
}
?>