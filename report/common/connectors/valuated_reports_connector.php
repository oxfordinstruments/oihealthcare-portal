<?php
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Easy set variables
	 */
//SELECT * 
//FROM systems_reports 
//WHERE `status`='closed' AND invoice_required='y' AND valuation_complete='Y' AND valuation_do_not_invoice = 'n' AND deleted = 'n' 
//ORDER BY `valuation_date` DESC

$debug = false;
if(isset($_GET['debug'])){
	$debug = true;
}	

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}
	
	
/* Indexed column (used for fast and accurate table cardinality) */
$sIndexColumn = "r.id";

$settings = new SimpleXMLElement($_SERVER['DOCUMENT_ROOT'].'/settings.xml', null, true);
require_once($_SERVER['DOCUMENT_ROOT'].'/define_inc.php');


include($_SERVER['DOCUMENT_ROOT'].'/mysqlInfo.php');

/* Array of database columns which should be read and sent back to DataTables. Use a space where
 * you want to insert a non-database field (for example a counter or static image)
 */
$aColumns = array('r.`date`', 'r.report_id', 'r.system_id', 'r.system_nickname', 'u3.name', 'u1.name', 'r.valuation_date', 'u2.name', 'r.invoice_date', 'r.unique_id', 'f.timezone');
$aColumnsSelect = array('r.`date`', 'r.report_id', 'r.system_id', 'r.system_nickname', 'u3.name AS ename', 'u1.name AS vname', "DATE_FORMAT(r.valuation_date,'".mdispfd."') AS vdate", 'u2.name AS iname', "DATE_FORMAT(r.invoice_date,'".mdispfd."') AS idate", 'r.unique_id', "IFNULL(f.timezone,'UTC') AS timezone");
$aColumnsName = array('date', 'report_id', 'system_id', 'system_nickname', 'ename', 'vname', 'vdate', 'iname', 'idate', 'unique_id', 'timezone');

/* DB table to use */
$sTable = "systems_reports AS r";

/* Database connection information */
$gaSql['user'] = "$username";
$gaSql['password'] = "$password";
$gaSql['db'] = "$db_name";
$gaSql['server'] = "$host";

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP server-side, there is
 * no need to edit below this line
 */

/* 
 * Local functions
 */
function fatal_error($sErrorMessage = ''){
	header($_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error');
	die($sErrorMessage);
}


/* 
 * MySQL connection
 */
if(!$gaSql['link'] = mysql_pconnect($gaSql['server'], $gaSql['user'], $gaSql['password'])){
	fatal_error('Could not open connection to server');
}

if (!mysql_select_db($gaSql['db'], $gaSql['link'])){
	fatal_error('Could not select database ');
}


/* 
 * Paging
 */
$sLimit = "";
if(isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1'){
	$sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".intval( $_GET['iDisplayLength']);
}


/*
 * Ordering
 */

$sOrder = "ORDER BY r.report_id ASC";
//$sOrder = "ORDER BY STR_TO_DATE(`date`,'%m/%d/%Y %H:%i') ASC";
//$sOrder = "";
if(isset($_GET['iSortCol_0'])){
	$sOrder = "ORDER BY  ";
	for($i=0; $i<intval($_GET['iSortingCols']); $i++){
		if($_GET['bSortable_'.intval($_GET['iSortCol_'.$i])] == "true"){
			$sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".($_GET['sSortDir_'.$i]==='asc' ? 'ASC' : 'DESC') .", ";
		}
	}
	
	$sOrder = substr_replace( $sOrder, "", -2 );
	if($sOrder == "ORDER BY"){
		$sOrder = "";			
	}
}




/* 
 * Filtering
 * NOTE this does not match the built-in DataTables filtering which does it
 * word by word on any field. It's possible to do here, but concerned about efficiency
 * on very large tables, and MySQL's regex functionality is very limited
 */
	//$sWhere = "";
	//JRD add below if statement
	$sWhere = "WHERE r.`status`='closed' AND r.invoice_required='y' AND r.valuation_complete='Y' AND r.valuation_do_not_invoice = 'n' AND r.deleted = 'n' ";	
	if(isset($_GET['sSearch']) && $_GET['sSearch'] != "" ){
		$sWhere = "WHERE (r.`status`='closed' AND r.invoice_required='y' AND r.valuation_complete='Y' AND r.valuation_do_not_invoice = 'n' AND r.deleted = 'n') AND (";	
		for($i=0; $i<count($aColumns); $i++){
			$sWhere .= $aColumns[$i]." LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ") ";
	}

/* Individual column filtering */
for($i=0; $i<count($aColumns); $i++){
	if(isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != ''){
		if($sWhere == ""){
			$sWhere = "WHERE ";
		}else{
			$sWhere .= " AND ";
		}
		$sWhere .= $aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
	}
}


/*
 * SQL queries
 * Get data to display
 */
$sQuery = "
	SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumnsSelect))."
	FROM   $sTable
	LEFT JOIN users AS u1 ON r.valuation_uid = u1.uid
	LEFT JOIN users AS u2 ON r.invoiced_uid = u2.uid
	LEFT JOIN users AS u3 ON r.user_id = u3.uid
	LEFT JOIN systems_base_cont AS sbc ON sbc.ver_unique_id = r.system_ver_unique_id
	LEFT JOIN facilities AS f ON f.unique_id = sbc.facility_unique_id
	$sWhere
	$sOrder
	$sLimit
	";
d($sQuery);
$rResult = mysql_query($sQuery, $gaSql['link']) or fatal_error('MySQL Error: ' . mysql_errno() . $sQuery);

/* Data set length after filtering */
$sQuery = "
	SELECT FOUND_ROWS()
";
$rResultFilterTotal = mysql_query( $sQuery, $gaSql['link'] ) or fatal_error( 'MySQL Error: ' . mysql_errno() . $sQuery);
$aResultFilterTotal = mysql_fetch_array($rResultFilterTotal);
$iFilteredTotal = $aResultFilterTotal[0];

/* Total data set length */
$sQuery = "
	SELECT COUNT(".$sIndexColumn.")
	FROM   $sTable
";
$rResultTotal = mysql_query( $sQuery, $gaSql['link'] ) or fatal_error( 'MySQL Error: ' . mysql_errno() . $sQuery );
$aResultTotal = mysql_fetch_array($rResultTotal);
$iTotal = $aResultTotal[0];


/*
 * Output
 */
$output = array(
	"sEcho" => intval($_GET['sEcho']),
	"iTotalRecords" => $iTotal,
	"iTotalDisplayRecords" => $iFilteredTotal,
	"aaData" => array()
);

while($aRow = mysql_fetch_array($rResult)){
	$row = array();
	for($i=0; $i<count($aColumns); $i++){
		$row[] = $aRow[$aColumnsName[$i]];
	}
	$output['aaData'][] = $row;
}
dd($output);
echo json_encode( $output );
?>