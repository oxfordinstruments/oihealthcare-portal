<?php
//Update Completed 12/9/14

$debug = false;
if(isset($_GET['debug'])){
	$debug = true;
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

if(isset($_GET['e'])) {
	$edit = true;
	if(isset($_GET['unique_id'])) {
		$unique_id=$_GET['unique_id'];
	}else if(isset($_GET['customer_id'])){
		$sql="SELECT unique_id FROM customers WHERE customer_id = '".$_GET['customer_id']."';";
		if(!$resultId = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
		$rowId = $resultId->fetch_assoc();
		$unique_id = $rowId['unique_id'];
	}else{
		$log->logerr('Blank Customer UID',1055,true,basename(__FILE__),__LINE__);
	}
}else{
	$edit = false;	
	$unique_id = md5(uniqid());
	
	$sql="SELECT MAX(CAST(customer_id AS UNSIGNED)) + 1 AS next_id FROM customers;";
	$resultNextCustomerID = $mysqli->query($sql);
	$rowNextCustomerID = $resultNextCustomerID->fetch_assoc();
}

$uploadBtnValue = "Attach Documents";

$sql="SELECT * FROM misc_states;";
if(!$resultStates= $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$states = array();
while($rowStates = $resultStates->fetch_assoc()){
	$states[$rowStates['abv']] = $rowStates['name'];	
}

$sql = "SELECT c.*
FROM customers AS c
WHERE c.unique_id = '".$unique_id."';";
if(!$resultC = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowC = $resultC->fetch_assoc();

if($edit){
	if(strtolower($rowC['has_files']) == 'y'){
		$uploadBtnValue = "View/Modify Documents";	
	}
}


//Get facilities list and create facilities json
$sql="SELECT f.id,f.facility_id,f.name
FROM facilities AS f
WHERE f.property = 'C'
ORDER BY f.facility_id ASC;";
d($sql);
if(!$resultFacilities = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}	
$facilities_num_rows = $resultFacilities->num_rows;
$facilitiesArr = array(); //blank array to fill with facilities data
while($rowFacilities = $resultFacilities->fetch_assoc()) // loop to give you the data in an associative array so you can use it however.
{
     $facilitiesArr[$rowFacilities['id']] = array("facility_id"=>$rowFacilities['facility_id'],"name"=>$rowFacilities['name']);
}
d($facilitiesArr);

$facilities_json = "{\"rows\":[";
end($facilitiesArr);
$last_facilitiesArr_key = key($facilitiesArr);
foreach($facilitiesArr as $key_facility=>$value_facility){
	$facilities_json.="{ \"id\":".$key_facility.", \"data\":[\"".$value_facility['facility_id']."\",\"".$value_facility['name']."\"]}";
	if($key_facility != $last_facilitiesArr_key){
		$facilities_json.=", ";		
	}		
}
$facilities_json.="]}";
d($facilities_json);

//Get systems assigned and create json
$sql="SELECT f.id, f.facility_id, f.name
FROM facilities AS f
LEFT JOIN customers AS c ON c.unique_id = f.customer_unique_id
WHERE c.unique_id = '$unique_id';";
d($sql);
if(!$resultAsgnFacilities = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}	
$assigned_num_rows = $resultAsgnFacilities->num_rows;
$assignedFacilitiesArr = array(); //blank array to fill with assigned facilities data
$has_facilities_assigned = false;
if($assigned_num_rows > 0){
	$has_facilities_assigned = true;
	$assignedFacilitiesArr = array();
	while($rowAssignedFacilities = $resultAsgnFacilities->fetch_assoc()){
		$assignedFacilitiesArr[$rowAssignedFacilities['id']] = array("facility_id"=>$rowAssignedFacilities['facility_id'],"name"=>$rowAssignedFacilities['name']);
	}	
	d($assignedFacilitiesArr);
	
	$assigned_facilities_json = "{\"rows\":[";
	end($assignedFacilitiesArr);
	$last_assignedFacilitiesArr_key = key($assignedFacilitiesArr);
	foreach($assignedFacilitiesArr as $key_facility=>$value_facility){
		$assigned_facilities_json.="{ \"id\":".$key_facility.", \"data\":[\"".$value_facility['facility_id']."\",\"".$value_facility['name']."\"]}";
		if($key_facility != $last_assignedFacilitiesArr_key){
			$assigned_facilities_json.=", ";		
		}		
	}
	$assigned_facilities_json.="]}";
	d($assigned_facilities_json);
}


?>
<!DOCTYPE html>
<html>
<head>
<?php require_once($head_include);?>
<?php require_once($css_include);?>
<link rel="stylesheet" type="text/css" href="/resources/dhtmlx/dhtmlxGrid/dhtmlxgrid.css">
<link rel="stylesheet" type="text/css" href="/resources/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<?php require_once($js_include);?>
<script  src="/resources/dhtmlx/dhtmlxGrid/dhtmlxcommon.js"></script>
<script  src="/resources/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>        
<script  src="/resources/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>    
<script  src="/resources/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_drag.js"></script>
<script  src="/resources/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script> 
<script src="/resources/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
$(document).ready(function() {
 		
$( "#tabs" ).tabs({ 
<?php if(!$edit){ ?>	disabled: [ 6 ] <?php } ?>
});

/////////////////////////////////////////////////////////////////////////////////////		
		<?php
		if(strtolower($_SESSION['prefs']['pref_show_help']) != 'y' or $_SESSION['mobile_device'] == true){
		?>
		$('#tiplayer').hide()
		<?php
			}
		?>

	
	$(".button_jquery_upload").button({
		icons: {
			primary: "ui-icon-folder-open"
		}
	});
	
	$(".button_jquery_save").button({
		icons: {
			primary: "ui-icon-disk"
		}
	});
	
	$(".button_jquery_lookup").button({
		icons: {
			primary: "ui-icon-search"
		}
	});
	
	$("select").chosen({
		no_results_text: "Oops, nothing found!",
		disable_search_threshold: 10,
		placeholder_text_single: '  ',
		search_contains: true
	}); 
	$("div .chosen-container").each(function(index, element) {
		if($(this).attr('style') =='width: 0px;'){
			$(this).removeAttr('style');
			$(this).css('display','block');
			
		}
	});
	
	loadgrids();
	var grids_cnt = getRowCounts();
	if(grids_cnt != 0){
		getAssignedGrid(assigned_facilities_grid,'orig_assigned_facilities_ids');
	}
});

	$(function() {
		$(".iframeUpload").fancybox({
				'type'			: 'iframe',
				'height'		: 600,
				'fitToView'		: true,
				'maxWidth'		: 900,
				'maxHeight'		: 600,
				'autoSize'		: false,
				'closeBtn'		: true,
				'margin'		: [5,5,5,5]
		});
		
		$("#archived").change(function(){
			if(document.getElementById("archived").value == "Y"){
				$.prompt("<h3>Archiving a customer will archive all facilities associated with this customer along with all systems associated with those facilities.<br>"+
				"Associated systems will be unassigned and the customer's users will be disabled.</h3>",{
					title: "Caution"
				});
			}
		});
		
		if(<?php if($edit and strtolower($rowC['bill_customer']) == 'n'){echo 'true';}else{echo 'false';} ?>){
			$("#billDiv").hide();
		}
		
		$("#bill_customer").change(function(e) {
			if(document.getElementById("bill_customer").value == "N"){
				$("#billDiv").hide();				
			}else{
				$("#billDiv").show();
			}
		});
		
	});

function submitcheck()
{
	var debug = <?php echo($debug)?"true":"false"; ?>;
	var edit = <?php echo($edit)?"true":"false"; ?>;
	ids = [];
	tabs = [];
	errors = [];
	
	var facilities_cnt = getRowCounts();
	if(facilities_cnt != 0){
		getAssignedGrid(assigned_facilities_grid,'assigned_facilities_ids');
		console.log('Got assigned facilities');
	}
	
	if($("#lblCustomerId").html().indexOf("!") >= 0){ids.push("#customer_id");tabs.push("#li-tab-10");}	
	if($("#customer_id").val()==""){ids.push("#customer_id");tabs.push("#li-tab-10");}
	if($("#name").val()==""){ids.push("#name");tabs.push("#li-tab-10");}
	if($("#address").val()==""){ids.push("#address");tabs.push("#li-tab-10");}
	if($("#city").val()==""){ids.push("#city");tabs.push("#li-tab-10");}
	if($("#state").val()==""){ids.push("#state_chosen");tabs.push("#li-tab-10");}
	if($("#zip").val()==""){ids.push("#zip");tabs.push("#li-tab-10");}
	if($("#phone").val()==""){ids.push("#phone");tabs.push("#li-tab-10");}
	
	if($("#contact_name").val()==""){ids.push("#contact_name");tabs.push("#li-tab-30");}
	if($("#contact_phone").val()==""){ids.push("#contact_phone");tabs.push("#li-tab-30");}
	

	//if($("#email_list").val()==""){ids.push("#email_list");tabs.push("#li-tab-100");}

	var regex = new RegExp(/[\'@\"\$\\\#\+\?]/g);
	if(document.getElementById("name").value.match(regex)){ids.push("#name");errors.push("Customer Name contains invalid characters  ' @ $ \ # + ? ");}
	if(document.getElementById("bill_name").value.match(regex)){ids.push("#bill_name");errors.push("Billing Name contains invalid characters  ' @ $ \ # + ? ");}

	$("#notes").val(ConvChar($("#notes").val()));

	//if(debug){
		console.log("ids: " + ids);
		console.log("tabs: " + tabs);
		console.log("errors: " + errors);
	//}
	
	console.log("Archived:" + $("#archived").val());
	if(edit){
		if($("#archived").val().toLowerCase()=="n"){
			showErrors(ids,errors,tabs);
		}else{
			while(ids.length > 0) {
				ids.pop();
			}	
		}
	}else{
		showErrors(ids,errors,tabs);
	}
	
	if(ids.length <= 0){
		$.prompt("<h3><?php if($edit){echo "Update";}else{echo "Add New";} ?> Customer?</h3>",{
			title: "Question",
			buttons: { Yes: 1, No: -1 },
			focus: 1,
			submit:function(e,v,m,f){ 
				if(v == 1){
					$.prompt.close();
					<?php if($debug){?>
						if(!confirm('DEBUG: really submit?!')){
							return;	
						}
					<?php } ?>
					document.forms['form'].submit();
				}else{
					$.prompt.close();	
				}
				e.preventDefault();
			}
		});
	}

}

/////////////////////////////////////////////////////////////////////////////////////
function ConvChar( str ) {
  c = {'<':'&lt;', '>':'&gt;', '&':'&amp;', '"':'&quot;', "'":'&#039;',
       '#':'&#035;', '@':'&#64;' };
  return str.replace( /[<&>'"#@]/g, function(s) { return c[s]; } );
}

/////////////////////////////////////////////////////////////////////////////////////
function showErrors(ids,errors,tabs){
	if(tabs.length > 0){
		errors.unshift("<h1><b>Correct the red highlighted fields</b></h1>");
	}
	$("input, select, .chosen-container").each(function(index, element) { //set all inputs to not highlighted
		$(this).animate({
			borderColor: "#2C3594",
			boxShadow: 'none'
		});
	});
	
	$("#tabs > ul > li").each(function(index, element) { //set all inputs to not highlighted
		$(this).animate({
			borderColor: "#2C3594",
			boxShadow: 'none'
		});
	});
	
	$.each(ids,function(index,value){//highlight inputs
		$(value).animate({
			borderColor: "#cc0000",
			boxShadow: '0 0 5px 3px rgba(255,0,0,0.4)'
		});
	});
	
	$.each(tabs,function(index,value){ //highlight tabs
		$(value).animate({
			borderColor: "#cc0000",
			boxShadow: '0 0 5px 3px rgba(255,0,0,0.4)'
		});
	});
	
	$("#errors > span").html("");
	$.each(errors,function(index, value){
		$("#errors > span").append(value + "<br>");
	});
	$("#errors").show('slow');
	//$(document).scrollTop(0);
}
/////////////////////////////////////////////////////////////////////////////////////////////////
function checkCustomer(str){
	if (str==""){
	  return;
	}
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}else{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			if(str != "<?php echo $rowC['customer_id']; ?>"){
				document.getElementById("lblCustomerId").innerHTML=xmlhttp.responseText;
			}else{
				document.getElementById("lblCustomerId").innerHTML="Customer ID";	
			}
		}
	}
	xmlhttp.open("GET","scripts/check_customer_id_edit.php?q="+str,true);
	xmlhttp.send();
}
/////////////////////////////////////////////////////////////////////////////////////
function loadgrids(){
	var facilities = <?php echo $facilities_json; ?>;
	<?php echo($has_facilities_assigned)?"var assigned_facilities = ".$assigned_facilities_json:""; ?>;
	
	facilities_grid = new dhtmlXGridObject('facilities_div');
	facilities_grid.setImagePath("/resources/dhtmlx/dhtmlxGrid/imgs/");
	facilities_grid.setHeader("Facility ID,Facility Name");
	facilities_grid.setColumnIds("facility_id","name");
	facilities_grid.setInitWidths("50");
	facilities_grid.setColAlign("left,left");
	facilities_grid.setColTypes("ro,ro");
	facilities_grid.setColSorting("str,str");
	facilities_grid.setMultiLine(false);
	facilities_grid.enableDragAndDrop(true);
	facilities_grid.attachHeader("#text_filter,#text_filter");
	facilities_grid.enableAutoWidth(true);
		//facilities_grid.attachEvent("onDrag", getRowCounts);
		//facilities_grid.attachEvent("onDragIn", getRowCounts);
	facilities_grid.attachEvent("onDrop", getRowCounts);
	facilities_grid.init();
	facilities_grid.setSkin("dhx_skyblue");
	facilities_grid.parse(facilities,"json");
	
	assigned_facilities_grid = new dhtmlXGridObject('assigned_facilities_div');
	assigned_facilities_grid.setImagePath("/resources/dhtmlx/dhtmlxGrid/imgs/");
	assigned_facilities_grid.setHeader("Facility ID,Facility Name");
	assigned_facilities_grid.setColumnIds("facility_id","name");
	assigned_facilities_grid.setInitWidths("50");
	assigned_facilities_grid.setColAlign("left,left");
	assigned_facilities_grid.setColTypes("ro,ro");
	assigned_facilities_grid.setColSorting("str,str");
	assigned_facilities_grid.setMultiLine(false);
	assigned_facilities_grid.enableDragAndDrop(true);
	assigned_facilities_grid.attachHeader("#text_filter,#text_filter");
	assigned_facilities_grid.enableAutoWidth(true);
		//assigned_facilities_grid.attachEvent("onDrag", getRowCounts);
		//assigned_facilities_grid.attachEvent("onDragIn", getRowCounts);
	assigned_facilities_grid.attachEvent("onDrop", getRowCounts);
	assigned_facilities_grid.init();
	assigned_facilities_grid.setSkin("dhx_skyblue");
	<?php if($has_facilities_assigned){?>
	assigned_facilities_grid.parse(assigned_facilities,"json");
	<?php } ?>
	
	$("#facilities_div, #assigned_facilities_div").width("100%");
	
	getRowCounts();
}
/////////////////////////////////////////////////////////////////////////////////////
function getAssignedGrid(grid,input){
	var tmp = [];
	grid.forEachRow(function(id){ // function that gets id of the row as an incoming argument
		tmp.push(id)
		//console.log(id);
	})
	document.getElementById(input).value = tmp.join(',');
}
/////////////////////////////////////////////////////////////////////////////////////
function getRowCounts(){
	try {
		var result = assigned_facilities_grid.getRowsNum();
		$("#assigned_facilities_count").html(result);
		return result;
	}catch(e){
		console.log(e);
		return 0;
	}
}

</script>
</head>
<body>
<?php require_once($header_include); ?>
<div id="OIReportContent">
	<div id="styledForm">
		<form id="form" name="form" method="post" autocomplete="off" action="customers_do.php">
			<?php if($edit){ ?>
			<h1>Editing Customer: <?php echo $rowC['name'];?></h1>
			<span class="red">Required fields marked with an *</span>
			<?php }else{ ?>
			<h1>Create New Customer </h1>
			<span class="red">Required fields marked with an *</span>
			<?php } ?>
			<p>&nbsp;</p>
			<div id="errors" style="text-align:center;display:none"> 
				<span style="color:#F00"> </span> 
			</div>

			<?php if($edit){ ?>
			<table width="100%" cellpadding="5" cellspacing="5">
				<tr>
					<td width="30%"><label>Archived</label>
						<select name="archived" id="archived">
							<option value="Y"<?php if($edit){if(strtolower($rowC['property'])=="a"){echo " selected";}} ?>>Yes</option>
							<option value="N"<?php if($edit){if(strtolower($rowC['property'])=="c"){echo " selected";}}else{echo " selected";} ?>>No</option>
						</select></td>
					<td>&nbsp;</td>
				</tr>
			</table>
			<?php } ?>
			<div class="line"></div>
<div id="tabs">
	 <ul>
		<li id="li-tab-10"><a href="#tab-10">Customer</a></li>
		<li id="li-tab-20"><a href="#tab-20">Billing</a></li>
		<li id="li-tab-30"><a href="#tab-30">Customer Contact</a></li>
		<li id="li-tab-40"><a href="#tab-40">Assigned Facility</a></li>
<!--		<li id="li-tab-60"><a href="#tab-60">Credit</a></li>-->
		<li id="li-tab-70"><a href="#tab-70">Notes</a></li>
		<li id="li-tab-80"><a href="#tab-80">Docs</a></li>
	</ul>

<div id="tab-10" class="tab">
			<!--Facility-->
			<table width="100%" cellpadding="5" cellspacing="5">
				<tr>
					<td width="20%"><label id="lblCustomerId">Customer ID <span class="red">*</span></label>
						<input type="text" name="customer_id" id="customer_id" onblur="checkCustomer(this.value)" value="<?php if($edit){echo $rowC['customer_id'];}else{echo $rowNextCustomerID['next_id'];} ?>"/></td>
					<td><label>Name <span style="font-size:.7em;">(As shown on building/Suite)</span> <span class="red">*</span></label>
						<input type="text" name="name" id="name" value="<?php if($edit){echo $rowC['name'];} ?>"/></td>
				</tr>
			</table>
			<table width="100%" cellpadding="5" cellspacing="5">
				<tr>
					<td colspan="2"><label>Address <span class="red">*</span></label>
						<input type="text" name="address" id="address" value="<?php if($edit){echo $rowC['address'];} ?>" /></td>
					<td width="24%"><label>City <span class="red">*</span></label>
						<input type="text" name="city" id="city" value="<?php if($edit){echo $rowC['city'];} ?>" /></td>
					<td width="16%"><label>State <span class="red">*</span></label>
						<select name="state" id="state">
							<option value=""></option>
							<?php
								foreach($states as $state_key=>$state_name){
									echo "<option value='" . $state_key . "'";
									if($edit){
										if($rowC['state']==$state_key){echo " selected";}
									}
									echo">" . $state_name . "</option>\n";
								}
							  ?>
						</select></td>
					<td width="12%"><label>Zip <span class="red">*</span></label>
						<input type="text" name="zip" id="zip" value="<?php if($edit){echo $rowC['zip'];} ?>" /></td>
				</tr>
			</table>
			<table width="100%" cellpadding="5" cellspacing="5">
				<tr>
					<td width="25%"><label>Phone <span class="red">*</span></label>
						<input type="text" name="phone" id="phone" value="<?php if($edit){echo $rowC['phone'];} ?>" /></td>
					<td width="25%"><label>Fax</label>
						<input type="text" name="fax" id="fax" value="<?php if($edit){echo $rowC['fax'];} ?>" /></td>
				</tr>
			</table>

</div>

<div id="tab-20" class="tab">
			<!--Billing Info-->
			<table width="100%" cellpadding="5" cellspacing="5">
				<tr>
					<td width="30%"><label>Bill Customer</label>
						<select name="bill_customer" id="bill_customer">
							<option value="Y"<?php if($edit){if(strtolower($rowC['bill_customer'])=="y"){echo " selected";}} ?>>Yes</option>
							<option value="N"<?php if($edit){if(strtolower($rowC['bill_customer'])=="n"){echo " selected";}}else{echo " selected";} ?>>No</option>
						</select></td>
					<td>&nbsp;</td>
				</tr>
			</table>
			<div id="billDiv">
				<table width="100%" cellpadding="5" cellspacing="5">
					<tr>
						<td colspan="5"><label>Name</label>
							<input type="text" name="bill_name" id="bill_name" value="<?php if($edit){echo $rowC['bill_name'];} ?>" /></td>
					</tr>
					<tr>
						<td colspan="2"><label>Address</label>
							<input type="text" name="bill_address" id="bill_address" value="<?php if($edit){echo $rowC['bill_address'];} ?>" /></td>
						<td width="24%"><label>City</label>
							<input type="text" name="bill_city" id="bill_city" value="<?php if($edit){echo $rowC['bill_city'];} ?>" /></td>
						<td width="16%"><label>State</label>
							<select name="bill_state" id="bill_state">
								<option value=""></option>
								<?php
									foreach($states as $state_key=>$state_name){
										echo "<option value='" . $state_key . "'";
										if($edit){
											if($rowC['bill_state']==$state_key){echo " selected";}
										}
										echo">" . $state_name . "</option>\n";
									}
								  ?>
							</select></td>
						<td width="12%"><label>Zip</label>
							<input type="text" name="bill_zip" id="bill_zip" value="<?php if($edit){echo $rowC['bill_zip'];} ?>" /></td>
					</tr>
				</table>
				<table width="100%" cellpadding="5" cellspacing="5">
					<tr>
						<td width="25%"><label>Phone</label>
							<input type="text" name="bill_phone" id="bill_phone" value="<?php if($edit){echo $rowC['bill_phone'];} ?>" /></td>
					</tr>
					<tr>
						<td width="25%"><label>Email</label>
							<input type="text" name="bill_email" id="bill_email" value="<?php if($edit){echo $rowF['bill_email'];} ?>" /></td>
					</tr>
				</table>
			</div>	
</div>

<div id="tab-30" class="tab">
			<!--Contact-->
			<table width="100%" cellpadding="5" cellspacing="5">
				<tr>
					<td width="50%"><label>Name <span class="red">*</span></label>
						<input type="text" name="contact_name" id="contact_name" value="<?php if($edit){echo $rowC['contact_name'];} ?>" /></td>
					<td width="50%"><label>Title</label>
						<input type="text" name="contact_title" id="contact_title" value="<?php if($edit){echo $rowC['contact_title'];} ?>" /></td>
				</tr>
			</table>
			<table width="100%" cellpadding="5" cellspacing="5">
				<tr>
					<td width="33%"><label>Phone <span class="red">*</span></label>
						<input type="text" name="contact_phone" id="contact_phone" value="<?php if($edit){echo $rowC['contact_phone'];} ?>" /></td>
					<td width="33%"><label>Cell</label>
						<input type="text" name="contact_cell" id="contact_cell" value="<?php if($edit){echo $rowC['contact_cell'];} ?>" /></td>
					<td><label>Email</label>
						<input type="email" name="contact_email" id="contact_email" value="<?php if($edit){echo $rowC['contact_email'];} ?>" /></td>
				</tr>
			</table>
			</td>
</div>

<div id="tab-40" class="tab">
			<!--Systems-->

					
			
					<table width="100%" cellpadding="5" cellspacing="5" id="systems_table">
						<tr align="center">
							<td colspan="2"><h2 style="margin:0; padding:0">Facility Assignments</h2></td>
						</tr>
						<tr align="center">
							<td colspan="2"><h2 style="margin:0; padding:0">Drag and drop facilities to assign them</h2></td>
						</tr>
						<tr align="center">
							<td id="assigned_facilities_label_td"><h2 style="margin:0; padding:0">Assigned Facilities</h2></td>
							<td><h2 style="margin:0; padding:0">Facilities</h2></td>
						</tr>
						<tr>
							<td width="50%">
								<div id="assigned_facilities_div" style="width:100%; height:270px;background-color:white;"></div>
							</td>
							<td width="50%">
								<div id="facilities_div" style="width:100%; height:270px;background-color:white;"></div>
							</td>
						</tr>
						<tr align="center">
							<td id="assigned_facilities_count_td"><h2>Assigned: <span id="assigned_facilities_count">0</span></h2></td>
							<td>&nbsp;</td>
						</tr>
					</table>


</div>

<!--<div id="tab-60" class="tab">-->
			<!--Credit-->
<!--			<table width="100%" cellpadding="5" cellspacing="5">-->
<!--				<tr>-->
<!--					<td width="30%"><label>Credit Hold</label>-->
<!--						<select name="credit_hold" id="credit_hold">-->
<!--							<option value="Y"<?php //if($edit){if(strtolower($rowC['credit_hold'])=="y"){echo " selected";}} ?>Yes</option>-->
<!--							<option value="N"<?php //if($edit){if(strtolower($rowC['credit_hold'])=="n"){echo " selected";}}else{echo " selected";} ?>No</option>-->
<!--						</select></td>-->
<!--					<td width="70%"><label>Credit Hold Notes</label>-->
<!--						<textarea name="credit_hold_notes" id="credit_hold_notes"><?php //if($edit){echo $rowC['credit_hold_notes'];} ?></textarea>-->
<!--					</td>-->
<!--				</tr>-->
<!--			</table>-->
<!--</div>-->
<div id="tab-70" class="tab">
			<!--Notes-->
			<table width="100%" cellpadding="5" cellspacing="5">
				<tr>
					<td width="70%"><label>Important Customer Notes</label>
						<textarea name="notes" id="notes"><?php if($edit){echo $rowC['notes'];} ?></textarea>
					</td>
				</tr>
			</table>
</div>


<div id="tab-80" class="tab">
			<!--Docs-->
			<div id="uploadDocs">
				<a name="uploadX" class="iframeUpload button_jquery_upload"  href="/resources/kcfinder/kcfinder.php?unique_id=<?php echo $unique_id; ?>&table=customers_files"><?php echo $uploadBtnValue; ?></a>
			</div>

</div>

</div>
			<div class="line"></div>
			<table width="100%" cellpadding="5" cellspacing="5">
				<tr>
					<td width="50%"><a name="Submit" class="button_jquery_save" onclick="submitcheck()"><?php if(!$edit){echo "Create New Customer";}else{ echo "Save Customer Changes";} ?></a></td>
				</tr>
			</table>
			<div style="display:none">
<!--				<input type="hidden" name="credit_hold_change" id="credit_hold_change" value="" />-->
				<input type="hidden" name="assigned_facilities_ids" id="assigned_facilities_ids" value="" />
				<input type="hidden" name="orig_assigned_facilities_ids" id="orig_assigned_facilities_ids" value="" />
				<input name="edit_customer" id="edit_customer" type="hidden" value="<?php if($edit){echo "Y";}else{echo "N";} ?>" />
				<input name="unique_id" id="unique_id" type="text" value="<?php echo $unique_id; ?>" />
				<?php if($debug){ ?><input name="debug" id="debug" type="hidden" value="Y" /><?php } ?>
			</div>

			<br />
			<br />
		</form>
		
	</div>
</div>
<?php require_once($footer_include); ?>
