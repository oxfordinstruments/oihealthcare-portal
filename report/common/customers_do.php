<?php
$debug = false;
if(isset($_POST['debug'])){
	$debug = true;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

dd($_POST);

$send = true;
if($settings->disable_email == '1'){
	$send = false;	
}

if(!isset($_POST['unique_id'])){
	$log->logerr('Blank Unique Id',1016,true,basename(__FILE__),__LINE__);	
}else{
	$unique_id=$_POST['unique_id'];	
}

if(strtolower($_POST['edit_customer']) == "y"){
	$edit = true;	
}else{
	$edit = false;
}



if(strtolower($_POST['archived'])=="y"){
	$sql="UPDATE users AS u
	LEFT JOIN systems_assigned_customer AS sac ON sac.uid = u.uid
	LEFT JOIN systems_base_cont AS sbc ON sbc.ver_unique_id = sac.system_ver_unique_id
	LEFT JOIN facilities AS f ON f.unique_id = sbc.facility_unique_id
	LEFT JOIN customers AS c ON c.unique_id = f.customer_unique_id 
	SET u.active = 'N'
	WHERE c.unique_id = '".$_POST['unique_id']."';";
	d($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	
	$sql="DELETE a
	FROM systems_assigned_customer AS a
	LEFT JOIN systems_base_cont AS sbc ON sbc.ver_unique_id = a.system_ver_unique_id
	LEFT JOIN facilities AS f ON f.unique_id = sbc.facility_unique_id
	WHERE f.customer_unique_id = '".$_POST['unique_id']."';";
	d($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}

	$sql="UPDATE customer_email_list AS cel
	LEFT JOIN systems_base_cont AS sbc ON sbc.unique_id = cel.system_unique_id
	LEFT JOIN facilities AS f ON f.unique_id = sbc.facility_unique_id
	SET cel.active = 'N'
	WHERE f.customer_unique_id = '".$_POST['unique_id']."';";
	d($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
		
	$sql="UPDATE facilities AS f
	LEFT JOIN customers AS c ON c.unique_id = f.customer_unique_id 
	SET f.property = 'A'
	WHERE c.unique_id = '".$_POST['unique_id']."';";
	d($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	
	$sql="UPDATE customers AS c
	SET c.property = 'A'
	WHERE c.unique_id = '".$_POST['unique_id']."';";
	d($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	
	$log->loginfo($_POST['system_id'].' - '.$unique_id,107,false,basename(__FILE__),__LINE__);
}else{
	
	$sql="SELECT c.unique_id FROM customers AS c WHERE c.customer_id = '".$_POST['customer_id']."';";
	d($sql);
	if(!$resultCustomer = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$rowCustomer = $resultCustomer->fetch_assoc();
	
//	$credit_hold_date = '';
//	if(strtolower($_POST['credit_hold']) == 'y'){
//		$credit_hold_date = date(storef, time());
//	}
	
	$archived = 'C';
	if(strtolower($_POST['archived']) == 'y'){
		$archived = 'A';	
	}
	
	$values = array(
		'customer_id' => $_POST['customer_id'], 
		'name' => addslashes($_POST['name']), 
		'address' => addslashes($_POST['address']),
		'city' => addslashes($_POST['city']),
		'state' => $_POST['state'],
		'zip' => $_POST['zip'],
		'phone' => $_POST['phone'],
		'fax' => $_POST['fax'],
		'bill_facility' => $_POST['bill_customer'],
		'bill_name' => addslashes($_POST['bill_name']),
		'bill_address' => addslashes($_POST['bill_address']),
		'bill_city' => addslashes($_POST['bill_city']),
		'bill_state' => $_POST['bill_state'],
		'bill_zip' => $_POST['bill_zip'],
		'bill_phone' => addslashes($_POST['bill_phone']),
		'bill_email' => addslashes($_POST['bill_email']),
		'contact_name' => addslashes($_POST['contact_name']), 
		'contact_title' => addslashes($_POST['contact_title']), 
		'contact_phone' => $_POST['contact_phone'],
		'contact_cell' => $_POST['contact_cell'], 
		'contact_email' => $_POST['contact_email'],
//		'credit_hold' => $_POST['credit_hold'],
//		'credit_hold_date' => $credit_hold_date,
//		'credit_hold_notes' => addslashes($_POST['credit_hold_notes']),
		'notes' => $_POST['notes'], 
		'archived' => $archived,
		'unique_id' => $unique_id,
		'created_by' => $myusername,
		'created_date' => date(storef,time())
	);
	d($values);
	
	$sql="INSERT INTO customers (customer_id, name, address, city, state, zip, phone, fax, bill_customer, bill_name, bill_address, bill_city, bill_state,
		bill_zip, bill_phone, bill_email, contact_name, contact_title, contact_phone, contact_cell, contact_email, notes, property, 
		unique_id, created_by, created_date)
		VALUES ('". implode("', '", $values) . "') ON DUPLICATE KEY
		UPDATE customer_id = VALUES(customer_id), name = VALUES(name), address = VALUES(address), city = VALUES(city), 
		state = VALUES(state), zip = VALUES(zip), phone = VALUES(phone), fax = VALUES(fax), bill_customer = VALUES(bill_customer), bill_name = VALUES(bill_name), 
		bill_address = VALUES(bill_address), bill_city = VALUES(bill_city), bill_state = VALUES(bill_state), bill_zip = VALUES(bill_zip), bill_phone = VALUES(bill_phone), bill_email = VALUES(bill_email),
		contact_name = VALUES(contact_name), contact_title = VALUES(contact_title), contact_phone = VALUES(contact_phone), contact_cell = VALUES(contact_cell), 
		contact_email = VALUES(contact_email), 
		notes = VALUES(notes), property = VALUES(property), unique_id = VALUES(unique_id), 
		edited_by = '".$myusername."',  edited_date = '".date(storef,time())."';";
	
	d($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
		
	if(isset($_POST['orig_assigned_facilities_ids']) and strlen($_POST['orig_assigned_facilities_ids']) > 0){
		$sql="UPDATE facilities SET customer_unique_id = NULL
		WHERE id IN (".$_POST['orig_assigned_facilities_ids'].");";
		d($sql);
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
	}
	
	if(isset($_POST['assigned_facilities_ids']) and strlen($_POST['assigned_facilities_ids']) > 0){
		$sql="UPDATE facilities SET customer_unique_id = '$unique_id'
		WHERE id IN (".$_POST['assigned_facilities_ids'].");";
		d($sql);
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
	}
	
//	if(strtolower($_POST['credit_hold_change']) == 'y'){
//		$sql="SELECT unique_id FROM facilities WHERE customer_unique_id = '$unique_id';";
//		d($sql);
//		if(!$result = $mysqli->query($sql)){
//			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
//			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
//			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
//		}
//		if($result->num_rows > 0){
//			$facilitiesArr = array();
//			while($rowFacilities = $result->fetch_assoc()){
//				array_push($facilitiesArr, $rowFacilities['unique_id']);
//			}
//			d($facilitiesArr);
//
//			$sql="UPDATE facilities SET credit_hold = '".$_POST['credit_hold']."' WHERE customer_unique_id = '$unique_id';";
//			d($sql);
//			if(!$result = $mysqli->query($sql)){
//				$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
//				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
//				$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
//			}
//			foreach($facilitiesArr as $facility){
//				$sql="UPDATE systems_base_cont SET credit_hold = '".$_POST['credit_hold']."' WHERE facility_unique_id = '$facility';";
//				d($sql);
//				if(!$result = $mysqli->query($sql)){
//					$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
//					$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
//					$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
//				}
//			}
//
//		}
//	}
	
	if($edit){
		$log->loginfo($_POST['customer_id'].' - '.$unique_id,109,false,basename(__FILE__),__LINE__);	
	}else{
		$log->loginfo($_POST['customer_id'].' - '.$unique_id,108,false,basename(__FILE__),__LINE__);
	}
	
}//else archived

?>
<!DOCTYPE html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<?php require_once($js_include);?>

<script type="text/javascript">
function delayer(){
    <?php if(!$debug){ ?>window.location = "<?php echo $refering_uri; ?>"<?php } ?>
}
</script>
</head>
<body onLoad="setTimeout('delayer()', 3000)">
<?php require_once($header_include); ?>
<div id="OIReportContent">
	<h1>Customer <?php if($edit){if(strtolower($_POST['archived']) == 'y'){echo "Archived";}else{echo "Updated";}}else{echo "Created";} ?> Successfully</h1><br>
	Customer ID: <?php echo $_POST['customer_id']; ?><br>
	Customer Name: <?php echo $_POST['name']; ?><br><br>
    <h1>Page will return to main page in 3 seconds</h1>
</div>
<?php require_once($footer_include); ?>

