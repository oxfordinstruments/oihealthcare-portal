<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

$settings = new SimpleXMLElement($_SERVER['DOCUMENT_ROOT'].'/settings.xml', null, true);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>OISP Docs</title>
<style>
#container {
	width: 100%;
	margin-right: auto;
	margin-left: auto;
	padding-top: 15px;
	padding-left: 15px;
	clear: both;
}

#header {
	margin-bottom: 20px;
}
#header #title {
	text-align: left;
	font-size: 1.25em;
	color: #F79447;
}

#tree {
	width: 25%;
	float: left;
	padding-top: 15px;
}
#treeboxbox_tree {
	overflow: auto;
	width: 100%;
}

#content {
	width: 70%;
	margin-right: auto;
	margin-left: auto;
	float: left;
}
#dataFrame {
	width: 100%;
}
#clear {
	clear: both;
}
</style>

<link rel="stylesheet" type="text/css" href="/resources/js/jstree/themes/default/style.min.css">

<script src="/resources/js/jquery/jquery.min.js"></script>
<script src="/resources/js/jstree/jstree.min.js"></script>
<script src="/resources/js/jstree/jstree.search.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	var path = 'content/';
	$(function () { $('#_tree').jstree(
		{'core': {
		'multiple' : false,
	    'animation' : 1,
		"themes" : { "stripes" : false }, 
		<?php require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/docs/docs.json'); ?>
		},
		"plugins": ["search","types","sort"] 
		});
	});
	
	$('#_tree').on("changed.jstree", function (e, data) {
		console.log(data.selected);
		data.toggle_node;
		if(typeof data.node.a_attr.href === "undefined"){
			$('#dataFrame').attr('src', path + 'default.html');
			$('#dataFrame').load(function(e) {
				setIframeHeight(document.getElementById('dataFrame'));		
			});	
		}else{
			if(data.node.a_attr.href == "" || data.node.a_attr.href == "#"){
				$('#dataFrame').attr('src', path + 'default.html');
				$('#dataFrame').load(function(e) {
					setIframeHeight(document.getElementById('dataFrame'));		
				});
			}else{
				$('#dataFrame').attr('src', path + data.node.a_attr.href);
				$('#dataFrame').load(function(e) {
					setIframeHeight(document.getElementById('dataFrame'));		
				});
				
			}
		}
	});
	
	$("#_tree").bind("select_node.jstree", function (e, data) {
    	return data.instance.toggle_node(data.node);
	});
	
	var to = false;
	$('#_treesearch').keyup(function () {
		console.log("KEYUP");
		if(to) { clearTimeout(to); }
		to = setTimeout(function () {
			var v = $('#_treesearch').val();
			$('#_tree').jstree(true).search(v);
		}, 250);
	});
	
//	$('iframe').load(function() {
//		this.style.height =
//		this.contentWindow.document.body.offsetHeight + 'px';
//	});

//	$('iframe').load(function() {
//		setTimeout(iResize, 50);
//		// Safari and Opera need a kick-start.
//		var iSource = document.getElementById('dataFrame').src;
//		document.getElementById('dataFrame').src = '';
//		document.getElementById('dataFrame').src = iSource;
//	});
//	function iResize() {
//		document.getElementById('dataFrame').style.height = 
//		document.getElementById('dataFrame').contentWindow.document.body.offsetHeight + 'px';
//	}
	function setIframeHeight(iframe) {
		if (iframe) {
			var iframeWin = iframe.contentWindow || iframe.contentDocument.parentWindow;
			if (iframeWin.document.body) {
				iframe.height = iframeWin.document.documentElement.scrollHeight || iframeWin.document.body.scrollHeight;
			}
		}
	};

});
</script>
</head>

<body>
<div id="container">
	<div id="header">
		<div id="title"><h1><?php echo $settings->short_name; ?> Portal Documentation</h1></div>
	</div>
	<div id="tree">
		<div id="search_tree">
			<label id="search_label">Search
			<input type="text" id="_treesearch" name="_treesearch" value="" />
			</label>
		</div>
		<div id="_tree"></div>
	</div>
	<div id="content">
		<iframe id="dataFrame" frameborder="0" name="dataFrame" src="content/default.html"></iframe>
	</div>
	<div id="clear"></div>
</div>
</body>
</html>