<?php
//Update Completed 11/25/14
require_once($_SERVER['DOCUMENT_ROOT'] . '/report/common/session_control.php');

$debug = false;
if(isset($_GET['debug'])){
	$debug = true;
}

?>
	<!DOCTYPE html>
	<html>
	<head>

		<?php require_once($head_include); ?>
		<?php require_once($css_include); ?>


		<link rel="stylesheet" type="text/css" href="/resources/dhtmlx/dhtmlxGrid/dhtmlxgrid.css">
		<link rel="stylesheet" type="text/css" href="/resources/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
		<link href="/resources/js/iButton/css/jquery.ibutton.min.css" rel="stylesheet" type="text/css">

		<?php require_once($js_include); ?>


		<script src="/resources/dhtmlx/dhtmlxGrid/dhtmlxcommon.js"></script>
		<script src="/resources/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
		<script src="/resources/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
		<script src="/resources/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_drag.js"></script>
		<script src="/resources/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
		<script type="text/javascript" language="javascript"
		        src="/resources/js/iButton/lib/jquery.ibutton.min.js"></script>
		<script type="text/javascript" language="javascript"
		        src="/resources/js/iButton/lib/jquery.metadata.js"></script>
		<script type="text/javascript">
			function emailreport(href) {
				$.ajax({
					url: href,
					beforeSend: function (xhr) {
						alert("Sending email to your email address...");
					},
					success: function (data, textStatus, jqXHR) {
						alert("Email sent to your email address");
					},
					error: function (req, status, err) {
						console.log('report email failed" ', status, err);
						alert("FAILED TO SEND EMAIL");
					}
				});
			}

//			$(document).ready(function() {
//				$("#showallcb").iButton({
//					labelOn: "Show My Data&emsp;&emsp;",
//					labelOff: "Show All Data",
//					resizeHandle: true,
//					resizeContainer: true,
//					duration: 250,
//					easing: "swing"
//				});
//			});
		</script>

		<style type="text/css">
			.reportLnk {
				color: #1B2673;
			}

			.unstyledListBullet {
				height: 0px;
				width: 0px;
				background-image: url(../../resources/images/common/bullet-round-orange.png);
				padding: 4px;
				background-repeat: no-repeat;
				background-position: right center;
				margin-left: 10px;
			}

		</style>
	</head>
<body>
<?php require_once($header_include); ?>

	<div id="OIReportContent">
		<div style="text-align:center">
			<h1>Email Notifications</h1>
		</div>
		<br>

		<div class="reports" style="width:50%; margin-left:auto; margin-right:auto;">
			<form id="emailnotify" name="emailnotify" method="post" autocomplete="off" action="/report/common/scripts/change_email_notify.php">

					<?php if(isset($_SESSION['perms']['perm_rcv_service_requests'])){ ?>
						<label>Service Request Notifications</label>
						<input name="pref_rcv_service_requests" id="pref_rcv_service_requests" type="checkbox" class="menuiButton" value="Y" <?php if(strtolower($_SESSION['prefs']['pref_rcv_service_requests']) == "y"){echo "checked";}   ?>>
						<br />
					<?php } ?>

					<?php if(isset($_SESSION['perms']['perm_rcv_service_reports'])){ ?>
						<label>Copy of Service Report</label>
						<input name="pref_rcv_service_reports" id="pref_rcv_service_reports" type="checkbox" class="menuiButton" value="Y" <?php if(strtolower($_SESSION['prefs']['pref_rcv_service_reports']) == "y"){echo "checked";}   ?>>
						<br />
					<?php } ?>

					<?php if(isset($_SESSION['perms']['perm_rcv_status_updates'])){ ?>
						<label>System Status Updates</label>
						<input name="pref_rcv_status_updates" id="pref_rcv_status_updates" type="checkbox" class="menuiButton" value="Y" <?php if(strtolower($_SESSION['prefs']['pref_rcv_status_updates']) == "y"){echo "checked";}   ?>>
						<br />
					<?php } ?>

					<?php if(isset($_SESSION['perms']['perm_rcv_assignment_change'])){ ?>
						<label>Engineer Assignment Changes</label>
						<input name="pref_rcv_assignment_change" id="pref_rcv_assignment_change" type="checkbox" class="menuiButton" value="Y" <?php if(strtolower($_SESSION['prefs']['pref_rcv_assignment_change']) == "y"){echo "checked";}   ?>>
						<br />
					<?php } ?>


					<?php if(isset($_SESSION['perms']['perm_rcv_valuation_request'])){ ?>
						<label>Valuation Notify</label>
						<input name="pref_rcv_valuation_request" id="pref_rcv_valuation_request" type="checkbox" class="menuiButton" value="Y" <?php if(strtolower($_SESSION['prefs']['pref_rcv_valuation_request']) == "y"){echo "checked";}   ?>>
						<br />
					<?php } ?>

					<?php if(isset($_SESSION['perms']['perm_rcv_invoice_request'])){ ?>
						<label>Invoice Request Notify</label>
						<input name="pref_rcv_invoice_request" id="pref_rcv_invoice_request" type="checkbox" class="menuiButton" value="Y" <?php if(strtolower($_SESSION['prefs']['pref_rcv_invoice_request']) == "y"){echo "checked";}   ?>>
						<br />
					<?php } ?>

					<?php if(isset($_SESSION['perms']['perm_rcv_parcarfbc_submit'])){ ?>
						<label>PAR/CAR/FBC Submited</label>
						<input name="pref_rcv_parcarfbc_submit" id="pref_rcv_parcarfbc_submit" type="checkbox" class="menuiButton" value="Y" <?php if(strtolower($_SESSION['prefs']['pref_rcv_parcarfbc_submit']) == "y"){echo "checked";}   ?>>
						<br />
					<?php } ?>

					<?php if(isset($_SESSION['perms']['perm_rcv_sysdown'])){ ?>
						<label>Systems Down</label>
						<input name="pref_rcv_sysdown" id="pref_rcv_sysdown" type="checkbox" class="menuiButton" value="Y" <?php if(strtolower($_SESSION['prefs']['pref_rcv_sysdown']) == "y"){echo "checked";}   ?>>
						<br />
					<?php } ?>

					<?php if(isset($_SESSION['perms']['perm_rcv_pm_overdue'])){ ?>
						<label>Systems Overdue PMs</label>
						<input name="pref_rcv_pm_overdue" id="pref_rcv_pm_overdue" type="checkbox" class="menuiButton" value="Y" <?php if(strtolower($_SESSION['prefs']['pref_rcv_pm_overdue']) == "y"){echo "checked";}   ?>>
						<br />
					<?php } ?>

					<?php if(isset($_SESSION['perms']['perm_rcv_futsysmov'])){ ?>
						<label>Future Systems Moving</label>
						<input name="pref_rcv_futsysmov" id="pref_rcv_futsysmov" type="checkbox" class="menuiButton" value="Y" <?php if(strtolower($_SESSION['prefs']['pref_rcv_futsysmov']) == "y"){echo "checked";}   ?>>
						<br />
					<?php } ?>

					<?php if(isset($_SESSION['perms']['perm_rcv_nps_report'])){ ?>
						<label>NPS Report</label>
						<input name="pref_rcv_nps_report" id="pref_rcv_nps_report" type="checkbox" class="menuiButton" value="Y" <?php if(strtolower($_SESSION['prefs']['pref_rcv_nps_report']) == "y"){echo "checked";}   ?>>
						<br />
					<?php } ?>

					<?php if(isset($_SESSION['perms']['perm_rcv_cont_met'])){ ?>
						<label>Contracts Fulfilled KPI</label>
						<input name="pref_rcv_cont_met" id="pref_rcv_cont_met" type="checkbox" class="menuiButton" value="Y" <?php if(strtolower($_SESSION['prefs']['pref_rcv_cont_met']) == "y"){echo "checked";}   ?>>
						<br />
					<?php } ?>


				<a id="emailnotifySubmit" name="Submit" class="menu_button_jquery_save" onclick="emailNotifySubmit(); return false;" >Save Email Settings</a>

				<!--<input id="emailnotifySubmit" name="Submit" type="button" value="Save Email Settings" class="button" onclick="emailNotifySubmit(); return false;" />-->

			</form>
		</div>
		<br>
	</div>

<?php require_once($footer_include); ?>