<?php
/**
 * @package OiHealthcarePortal
 * @file email_subscriptions.php
 * @author Evotodi <evotodi@gmail.com>
 * @date 10/12/2016
 * @time 2:25 PM
 */

$debug = false;
if(isset($_GET['debug'])){
	$debug = true;
}

require_once($_SERVER['DOCUMENT_ROOT'] . '/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'] . '/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'] . '/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}


$unique_id = md5(uniqid());
if(!isset($_GET['regid'])){
	$log->logerr('Error occurred, contact support.', 1018, true, basename(__FILE__), __LINE__);
}
if(!is_numeric($_GET['regid'])){
	$log->logerr('Error occurred, contact support.', 4, true, basename(__FILE__), __LINE__);
}

$sql = "SELECT id, name, email, comment, system_id, ip, domain, browser, date, unique_id FROM registration_requests WHERE id = " . $_GET['regid'] . ";";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql, 1000, false, basename(__FILE__), __LINE__);
	$log->logerr('There was error running the query [' . $mysqli->error . ']', 1000, false, basename(__FILE__), __LINE__);
	$log->logerr('Error occurred, contact support.', 1000, true, basename(__FILE__), __LINE__);
}
$rowRequest = $result->fetch_assoc();
d($rowRequest);

//Check table for exsisting entries
$sql = "SELECT cel.id, cel.name, cel.email, cel.email_unique_id, cel.system_ver_unique_id, sbc.system_id, sbc.nickname, cel.rcv_service_report, cel.rcv_service_request
FROM customers_email_list AS cel
LEFT JOIN systems_base_cont AS sbc ON sbc.ver_unique_id = cel.system_ver_unique_id
WHERE cel.email = '" . $rowRequest['email'] . "' AND cel.active = 'y'
GROUP BY sbc.unique_id
ORDER BY sbc.system_id;";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql, 1000, false, basename(__FILE__), __LINE__);
	$log->logerr('There was error running the query [' . $mysqli->error . ']', 1000, false, basename(__FILE__), __LINE__);
	$log->logerr('Error occurred, contact support.', 1000, true, basename(__FILE__), __LINE__);
}

$current_subscription = false;
if($result->num_rows > 0){
	while($row = $result->fetch_assoc()){
		$current_subscription[$row['id']] = $row;
	}
}

$first_row = false;
$name = false;
$email = false;
if(is_array($current_subscription)){
	$first_row = reset($current_subscription);
	$unique_id = $first_row['email_unique_id'];
	$name = $first_row['name'];
	$email = $first_row['email'];
}
d($first_row);
d($unique_id);
d($current_subscription);


?>
<!DOCTYPE html>
<html>
<head>
	<?php require_once($head_include); ?>
	<?php require_once($css_include); ?>
	<?php require_once($js_include); ?>
	<script src="/resources/js/jquery-dynamic-form_MODIFIED.js"></script>
	<script src="/resources/js/jquery.ui.datepicker.js"></script>
	<script type="text/javascript">
		$(document).ready(function () {

			$("#tabs").tabs({
				<?php if(!is_array($current_subscription)){ ?>    disabled: [2] <?php } ?>
			});

			$(".button_jquery_remove").button({
				icons: {
					primary: "ui-icon-closethick"
				}
			});

			$(".button_jquery_save").button({
				icons: {
					primary: "ui-icon-disk"
				}
			});

			$(".button_jquery_lookup").button({
				icons: {
					primary: "ui-icon-search"
				}
			});

			$("select").chosen({
				no_results_text: "Oops, nothing found!",
				disable_search_threshold: 10,
				placeholder_text_single: '  ',
				search_contains: true
			});

			$("div .chosen-container").each(function (index, element) {
				if ($(this).attr('style') == 'width: 0px;') {
					$(this).removeAttr('style');
					$(this).css('display', 'block');

				}
			});

			$("#add_system").click(function () {
				var dupe = false;
				$("td input[origname='system_ids']").each(function () {
					if($(this).val() == $("#system_id").val()){
						dupe = true;
					}
				});

				if($("#system_id").val() != "" && !dupe){
					$("#plusH").click();
					var id = $("#systems").parent().children('tr').length - 2;
					$("#system_ids"+id).val($("#system_id").val());
				}
			});

			$("#systems").dynamicForm(
				"#plusH",
				"#minusH",
				{ limit:10,
					removeColor:"red",
					createColor:"green",
					normalizeFullForm: false
				});
		});
		function submitcheck(del) {
			var debug = <?php echo ($debug) ? "true" : "false"; ?>;

			ids = [];
			tabs = [];
			errors = [];
			if(!del) {
				if ($("#email").val() == "") {
					ids.push("#email");
					tabs.push("#li-tab-10");
				}
				if ($("#name").val() == "") {
					ids.push("#name");
					tabs.push("#li-tab-10");
				}

				var regex = new RegExp(/[\'@\"\$\\\#\+\?]/g);
				if (document.getElementById("name").value.match(regex)) {
					ids.push("#name");
					errors.push("Name contains invalid characters  ' @ $ \ # + ? ");
					tabs.push("#li-tab-10");
				}

				var system_count = $("#systems").parent().children('tr').length - 2;
				if (system_count == 0) {
					ids.push("#system_id");
					errors.push("No systems assigned");
					tabs.push("#li-tab-30");
				}

				//if(debug){
				console.log("ids: " + ids);
				console.log("tabs: " + tabs);
				console.log("errors: " + errors);
				//}

				showErrors(ids, errors, tabs);
			}
			var prompt_text = "Create Subscription?";
			if(del){
				prompt_text = "Delete Request?";
			}

			if (ids.length <= 0) {
				$.prompt("<h3>" + prompt_text + "</h3>", {
					title: "Question",
					buttons: {Yes: 1, No: -1},
					focus: 1,
					submit: function (e, v, m, f) {
						if (v == 1) {
							$.prompt.close();
							<?php if($debug){?>
							if (!confirm('DEBUG: really submit?!')) {
								return;
							}
							<?php } ?>
							if(del){
								$("#del").prop('checked', true);
							}
							document.forms['form'].submit();
						} else {
							$.prompt.close();
						}
						e.preventDefault();
					}
				});
			}

		}
		function showErrors(ids, errors, tabs) {
			if (tabs.length > 0) {
				errors.unshift("<h1><b>Correct the red highlighted fields</b></h1>");
			}
			$("input, select, .chosen-container").each(function (index, element) { //set all inputs to not highlighted
				$(this).animate({
					borderColor: "#2C3594",
					boxShadow: 'none'
				});
			});

			$("#tabs > ul > li").each(function (index, element) { //set all inputs to not highlighted
				$(this).animate({
					borderColor: "#2C3594",
					boxShadow: 'none'
				});
			});

			$.each(ids, function (index, value) {//highlight inputs
				$(value).animate({
					borderColor: "#cc0000",
					boxShadow: '0 0 5px 3px rgba(255,0,0,0.4)'
				});
			});

			$.each(tabs, function (index, value) { //highlight tabs
				$(value).animate({
					borderColor: "#cc0000",
					boxShadow: '0 0 5px 3px rgba(255,0,0,0.4)'
				});
			});

			$("#errors > span").html("");
			$.each(errors, function (index, value) {
				$("#errors > span").append(value + "<br>");
			});
			$("#errors").show('slow');
			//$(document).scrollTop(0);
		}

	</script>
</head>
<body>
<?php require_once($header_include); ?>
	<div id="OIReportContent">
		<div id="styledForm">
			<form id="form" name="form" method="post" autocomplete="off" action="email_subscriptions_do.php">
				<h1>Add new email subscription </h1>
				<span class="red">Required fields marked with an *</span>
				<p>&nbsp;</p>
				<div id="errors" style="text-align:center;display:none">
					<span style="color:#F00"> </span>
				</div>
				<div class="line"></div>
				<div id="tabs">
					<ul>
						<li id="li-tab-10"><a href="#tab-10">New Subscription</a></li>
						<li id="li-tab-30"><a href="#tab-30">New Assigned Systems</a></li>
						<li id="li-tab-20"><a href="#tab-20">Current Subscriptions</a></li>
					</ul>

					<div id="tab-10" class="tab">
						<!--New Subscription-->
						<table width="100%" cellpadding="5" cellspacing="5">
							<tr>
								<td width="50%"><label>Email Address <span class="red">*<?php if(is_array($current_subscription)){echo "&emsp;<i>Using email from other subscriptions</i>";} ?></span></label>
									<input type="text" name="email" id="email"
									       value="<?php if(is_array($current_subscription)){echo $email;}else{echo $rowRequest['email'];} ?>"/></td>
								<td><label>Name <span class="red">*<?php if(is_array($current_subscription)){echo "&emsp;<i>Using name from other subscriptions</i>";} ?></span></label>
									<input type="text" name="name" id="name"
									       value="<?php if(is_array($current_subscription)){echo $name;}else{echo $rowRequest['name'];} ?>"/></td>
							</tr>
						</table>
						<table width="100%" cellpadding="5" cellspacing="5">
							<tr>
								<td><label>Requested Systems: <?php echo $rowRequest['system_id']; ?></label></td>
							</tr>
						</table>
						<table width="100%" cellpadding="5" cellspacing="5">
							<tr>
								<td><label>Comments: <?php echo $rowRequest['comment']; ?></label></td>
							</tr>
						</table>
						<div class="line"></div>
						<table width="100%" cellpadding="5" cellspacing="5">
							<tr>
								<td><label>IP Address: <?php echo $rowRequest['ip']; ?></label></td>
								<td><label>Domain: <?php echo $rowRequest['domain']; ?></label></td>
							</tr>
							<tr>
								<td><label>Date: <?php echo $rowRequest['date']; ?></label></td>
								<td><label>Request Unique ID: <?php echo $rowRequest['unique_id']; ?></label></td>
							</tr>
							<tr>
								<td colspan="2"><label>Browser: <?php echo $rowRequest['browser']; ?></label></td>
							</tr>
						</table>
						<table width="100%" cellpadding="5" cellspacing="5">
							<tr>
								<td><label>Email Unique_id: <?php echo $unique_id; ?></label></td>
							</tr>
						</table>

					</div>

					<div id="tab-20" class="tab">
						<!--Current Subscriptions-->
						<table width="100%" cellpadding="5" cellspacing="5">
							<thead>
							<tr style="text-align: left;">
								<th>Email</th>
								<th>Name</th>
								<th>System ID</th>
								<th>System Nickname</th>
								<th>Reports</th>
								<th>Requests</th>
							</tr>
							</thead>
							<tbody>
								<?php foreach($current_subscription as $sub){
									echo "<tr>";
									echo "<td>".$sub['email']."</td>";
									echo "<td>".$sub['name']."</td>";
									echo "<td>".$sub['system_id']."</td>";
									echo "<td>".$sub['nickname']."</td>";
									echo "<td>".$sub['rcv_service_report']."</td>";
									echo "<td>".$sub['rcv_service_request']."</td>";
									echo "<tr>";
								}?>
							</tbody>

						</table>
					</div>

					<div id="tab-30" class="tab">
						<!--Selected Systems-->
						<table width="100%" cellpadding="5" cellspacing="5">
							<tr>
								<td>
									<a name="lookup_system" class="iframeView button_jquery_lookup" href="system_lookup.php">Lookup System</a>
									<input style="width: 25%;" type="text" readonly name="system_id" id="system_id" value="" />
									&nbsp;<a name="add_system" id="add_system" class="button_jquery_save">Add System to list</a>
								</td>
							</tr>
						</table>
						<br>
						<label>Assigned Systems List</label>
						<div style="border: solid; width: 30%;">
							<table width="100%" cellpadding="5" cellspacing="5">
								<tr id="systems">
									<td><input type="text" id="system_ids" name="system_ids" readonly style="border: hidden;" /></td>
								</tr>
								<tr>
									<td style="padding-top: 40px;"><a id="plusH" style="border:none;" href="">&nbsp;</a> <a id="minusH" style="border:none" class="button_jquery_remove">Remove Last</a></td>
								</tr>
							</table>
						</div>
					</div>

				</div>
				<div class="line"></div>
				<table width="100%" cellpadding="5" cellspacing="5">
					<tr>
						<td width="50%"><a name="Submit" class="button_jquery_save" onclick="submitcheck(false)">Create Subscription</a>
							<a name="Submit" class="button_jquery_save" onclick="submitcheck(true)">Delete Request</a></td>
					</tr>
				</table>
				<div style="display:none">
					<input name="unique_id" id="unique_id" type="text" value="<?php echo $unique_id; ?>"/>
					<?php if($debug){ ?><input name="debug" id="debug" type="hidden" value="Y"/><?php } ?>
					<input type="checkbox" name="del" id="del" value="del" />
					<input type="text" name="req_id" id="req_id" value="<?php echo $_GET['regid']; ?>"/>
				</div>

				<br/>
				<br/>
			</form>

		</div>
	</div>
<?php require_once($footer_include); ?>