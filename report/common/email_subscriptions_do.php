<?php
/**
 * @package OiHealthcarePortal
 * @file email_subscriptions_do.php
 * @author Evotodi <evotodi@gmail.com>
 * @date 10/13/2016
 * @time 2:31 PM
 */

$debug = false;
if(isset($_GET['debug']) or isset($_POST['debug'])){
	$debug = true;
}

require_once($_SERVER['DOCUMENT_ROOT'] . '/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'] . '/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'] . '/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

d($_POST);

if(isset($_POST['del'])){
	$sql="UPDATE registration_requests SET registered = 'N', used = 'Y' WHERE id = ".$_POST['req_id'].";";
	s($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
}else{
	$systems = $_POST['systems']['systems'];
	array_shift($systems);
	d($systems);
	$date = date(storef,time());
	foreach($systems as $system){
		$sql="SELECT ver_unique_id FROM systems_base_cont WHERE system_id = '".$system['system_ids']."' AND property = 'C';";
		s($sql);
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
		$row = $result->fetch_assoc();
		d($row);

		$sql="INSERT INTO customers_email_list (email, email_unique_id, name, system_ver_unique_id, system_id, created_date) 
		VALUES ('".trim($_POST['email'])."', '".$_POST['unique_id']."', '".trim(htmlspecialchars($_POST['name']))."','".$row['ver_unique_id']."', '".$system['system_ids']."', '".$date."');";
		s($sql);
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}

	}

	$sql="UPDATE registration_requests SET registered = 'Y', used = 'Y' WHERE id = ".$_POST['req_id'].";";
	s($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
}
?>

<!DOCTYPE html>
<html>
<head>

	<?php require_once($head_include);?>
	<?php require_once($css_include);?>
	<?php require_once($js_include);?>

	<script type="text/javascript">
		function delayer(){
			<?php if(!$debug){ ?>window.location = "<?php echo $refering_uri; ?>"<?php } ?>
		}
	</script>
</head>
<body onLoad="setTimeout('delayer()', 3000)">
<?php require_once($header_include); ?>
<div id="OIReportContent">
	<h1>Email Subscription <?php if(isset($_POST['del'])){echo "Deleted";}else{echo "Created";} ?> Successfully</h1><br><br>
	<h1>Page will return to main page in 3 seconds</h1>
</div>
<?php require_once($footer_include); ?>