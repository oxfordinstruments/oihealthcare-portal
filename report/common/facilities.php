<?php

$debug = false;
if(isset($_GET['debug'])){
	$debug = true;
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

if(isset($_GET['e'])) {
	$edit = true;
	if(isset($_GET['unique_id'])) {
		$unique_id=$_GET['unique_id'];
	}else if(isset($_GET['facility_id'])){
		$sql="SELECT unique_id FROM facilities WHERE facility_id = '".$_GET['facility_id']."';";
		if(!$resultId = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
		$rowId = $resultId->fetch_assoc();
		$unique_id = $rowId['unique_id'];
	}else{
		$log->logerr('Blank Facility UID',1054,true,basename(__FILE__),__LINE__);
	}
}else{
	$edit = false;	
	$unique_id = md5(uniqid());
	
	$sql="SELECT MAX(CAST(facility_id AS UNSIGNED)) + 1 AS next_id FROM facilities;";
	$resultNextFacilityID = $mysqli->query($sql);
	$rowNextFacilityID = $resultNextFacilityID->fetch_assoc();
}

$uploadBtnValue = "Attach Documents";

$sql="SELECT * FROM misc_states;";
if(!$resultStates= $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$states = array();
while($rowStates = $resultStates->fetch_assoc()){
	$states[$rowStates['abv']] = $rowStates['name'];	
}

$sql = "SELECT f.*, c.customer_id
FROM facilities AS f
LEFT JOIN customers AS c ON c.unique_id = f.customer_unique_id
WHERE f.unique_id = '".$unique_id."';";
if(!$resultF = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowF = $resultF->fetch_assoc();
d($rowF);

if($edit){
	if(strtolower($rowF['has_files']) == 'y'){
		$uploadBtnValue = "View/Modify Documents";	
	}
}

//Get systems list and create systems json
$sql="SELECT sbc.id, sbc.system_id, sbc.nickname, sbc.ver, IF(sbc.property = 'F','Y','N') AS future, sbc.pre_paid, sbc.credit_hold
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
WHERE sbc.property != 'A' AND sbc.system_id != '9999'
ORDER BY sbc.system_id ASC;";
d($sql);
if(!$resultSystems = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}	
$systems_num_rows = $resultSystems->num_rows;
$systemsArr = array(); //blank array to fill with systems data
while($rowSystems = $resultSystems->fetch_assoc()) // loop to give you the data in an associative array so you can use it however.
{
     $systemsArr[$rowSystems['id']] = array("system_id"=>$rowSystems['system_id'],"nickname"=>$rowSystems['nickname'],"version"=>$rowSystems['ver'],"future"=>$rowSystems['future'],"pre_paid"=>$rowSystems['pre_paid'],"credit_hold"=>$rowSystems['credit_hold']);
}
d($systemsArr);

$systems_json = "{\"rows\":[";
$systems_json_index = "\"index\":[{";
$x = 0;
foreach($systemsArr as $key_system=>$value_system){
	$systems_json.="{ \"id\":".$key_system.", ";
	$systems_json_index.="\"".$key_system."\":".$x.",";
	$systems_json.="\"userdata\":{\"future\":\"".$value_system['future']."\", \"version\":\"".$value_system['version']."\", \"pre_paid\":\"".$value_system['pre_paid']."\", \"credit_hold\":\"".$value_system['credit_hold']."\"},";
	$systems_json.="\"data\":[\"".$value_system['system_id']."\",\"".$value_system['nickname']."\",";
	$systems_json.="\"".$value_system['future']."\",\"".$value_system['pre_paid']."\",\"".$value_system['credit_hold']."\"]},";
	$x++;
}
$systems_json = preg_replace("/,$/","",$systems_json) . "], ".preg_replace("/,$/","",$systems_json_index)."}]}";
d($systems_json);

//Get systems assigned and create json
$sql="SELECT sbc.id, sbc.system_id, sbc.nickname, sbc.ver, IF(sbc.property = 'F','Y','N') AS future, sbc.pre_paid, sbc.credit_hold
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
LEFT JOIN facilities AS f ON f.unique_id = sbc.facility_unique_id
WHERE sbc.property != 'A' AND sbc.system_id != '9999' AND f.unique_id = '$unique_id';";
d($sql);
if(!$resultAsgnSystems = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}	
$assigned_num_rows = $resultAsgnSystems->num_rows;
$assignedSystemsArr = array(); //blank array to fill with assigned systems data
$has_systems_assigned = false;
if($assigned_num_rows > 0){
	$has_systems_assigned = true;
	$assignedSystemsArr = array();
	while($rowAssignedSystems = $resultAsgnSystems->fetch_assoc()){
		$assignedSystemsArr[$rowAssignedSystems['id']] = array("system_id"=>$rowAssignedSystems['system_id'],"nickname"=>$rowAssignedSystems['nickname'],"version"=>$rowAssignedSystems['ver'],"future"=>$rowAssignedSystems['future'],"pre_paid"=>$rowAssignedSystems['pre_paid'],"credit_hold"=>$rowAssignedSystems['credit_hold']);
	}	
	d($assignedSystemsArr);
	
	$assigned_systems_json = "{\"rows\":[";
	$assigned_systems_json_index = "\"index\":[{";
	$x = 0;
	
	foreach($assignedSystemsArr as $key_system=>$value_system){
		$assigned_systems_json.="{ \"id\":".$key_system.", ";
		$assigned_systems_json_index.="\"".$key_system."\":".$x.",";
		$assigned_systems_json.="\"userdata\":{\"future\":\"".$value_system['future']."\", \"version\":\"".$value_system['version']."\", \"pre_paid\":\"".$value_system['pre_paid']."\", \"credit_hold\":\"".$value_system['credit_hold']."\"}, ";
		$assigned_systems_json.="\"data\":[\"".$value_system['system_id']."\",\"".$value_system['nickname']."\",";
		$assigned_systems_json.="\"".$value_system['future']."\",\"".$value_system['pre_paid']."\",\"".$value_system['credit_hold']."\"]},";
		$x++;
	}
	$assigned_systems_json = preg_replace("/,$/","",$assigned_systems_json) . "], ".preg_replace("/,$/","",$assigned_systems_json_index)."}]}";
	d($assigned_systems_json);
}


?>
<!DOCTYPE html>
<html>
<head>
<?php require_once($head_include);?>
<?php require_once($css_include);?>
<link rel="stylesheet" type="text/css" href="/resources/dhtmlx/dhtmlxGrid/dhtmlxgrid.css">
<link rel="stylesheet" type="text/css" href="/resources/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<?php require_once($js_include);?>
<script  src="/resources/dhtmlx/dhtmlxGrid/dhtmlxcommon.js"></script>
<script  src="/resources/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>        
<script  src="/resources/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>    
<script  src="/resources/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_drag.js"></script>
<script  src="/resources/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script> 
<script src="/resources/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
var systems_grid;
var assigned_grid;

$(document).ready(function() {
 		
$( "#tabs" ).tabs({ 
<?php if(!$edit){ ?>	disabled: [ 8 ] <?php } ?>
});

/////////////////////////////////////////////////////////////////////////////////////		
		<?php
		if(strtolower($_SESSION['prefs']['pref_show_help']) != 'y' or $_SESSION['mobile_device'] == true){
		?>
		$('#tiplayer').hide()
		<?php
			}
		?>

//	 $("#credit_hold").change(function(){
//     	var sel = $(this).val();
//       	if(sel=='N'){
//			$.prompt("<h3>Removing this Facility from credit hold will also remove any associated Systems from credit hold.<br>"+
//						"Are you sure you want to removce this Facility from credit hold?</h3>",
//			{
//				title: 'Credit Hold Notice',
//				buttons: { "Yes, I'm Sure": true, "No": false },
//				submit: function(e,v,m,f){
//					e.preventDefault();
//					if(!v){
//						$("#credit_hold_div").show();
//						$("#credit_hold").val("Y");
//						$("#credit_hold").trigger("chosen:updated");
//						$.prompt.close();
//					}else{
//						$("#credit_hold_div").hide();
//						$("#credit_hold_change").val("Y");
//						$.prompt.close();
//					}
//				}
//			});
//
//        }else{
//			$.prompt("<h3>Placing this Facility on credit hold will also place any associated Systems on credit hold as well.<br>"+
//						"Are you sure you want to place this Facility on credit hold?</h3>",
//			{
//				title: 'Credit Hold Notice',
//				buttons: { "Yes, I'm Sure": true, "No": false },
//				submit: function(e,v,m,f){
//					e.preventDefault();
//					if(!v){
//						$("#credit_hold_div").hide();
//						$("#credit_hold").val("N");
//						$("#credit_hold").trigger("chosen:updated");
//						$.prompt.close();
//					}else{
//						$("#credit_hold_div").show();
//						$("#credit_hold_change").val("Y");
//						$.prompt.close();
//					}
//				}
//			});
//		}
//    });
	
	$(".button_jquery_upload").button({
		icons: {
			primary: "ui-icon-folder-open"
		}
	});
	
	$(".button_jquery_save").button({
		icons: {
			primary: "ui-icon-disk"
		}
	});
	
	$(".button_jquery_lookup").button({
		icons: {
			primary: "ui-icon-search"
		}
	});
	
	$("select").chosen({
		no_results_text: "Oops, nothing found!",
		disable_search_threshold: 10,
		placeholder_text_single: '  ',
		search_contains: true
	}); 
	$("div .chosen-container").each(function(index, element) {
		if($(this).attr('style') =='width: 0px;'){
			$(this).removeAttr('style');
			$(this).css('display','block');
			
		}
	});
	
	loadsystems();
	
	var systems_cnt = getRowCounts();
	if(systems_cnt != 0){
		getAssignedSystems(assigned_systems_grid,'orig_assigned_systems_ids');
	}
});

	$(function() {
//		$( "#contract_start_date" ).datepicker({
//			numberOfMonths: 1,
//			showButtonPanel: true,
//			changeYear: true,
//			duration: "fast",
//			dateFormat: "<?php echo dpdispfd; ?>"
//		});
		/////////////////////////////////////////////////////////////////////////////////////
		$(".iframeUpload").fancybox({
				'type'			: 'iframe',
				'height'		: 600,
				'fitToView'		: true,
				'maxWidth'		: 900,
				'maxHeight'		: 600,
				'autoSize'		: false,
				'closeBtn'		: true,
				'margin'		: [5,5,5,5]
		});
		
		$("#archived").change(function(){
			if(document.getElementById("archived").value == "Y"){
				$.prompt("<h3>Archiving a facility will un-assign all engineers assigned to the assoicaited systems, disable customer access to this facility and its associated systems, "+
						"and the customer's users for this facility will be disabled!</h3>",{
					title: "Caution"
				});
				//alert("Archiving a facility will un-assign all users assigned to this system, and disable customer access to this system!");	
			}
		});
		
		if(<?php if($edit and strtolower($rowF['bill_facility']) == 'n'){echo 'true';}else{echo 'false';} ?>){
			$("#billDiv").hide();
			console.log('first true')
		}
		
		$("#bill_facility").change(function(e) {
			if(document.getElementById("bill_facility").value == "N"){
				$("#billDiv").hide();				
			}else{
				$("#billDiv").show();
			}
		});
				
	});

function submitcheck()
{
	var debug = <?php echo($debug)?"true":"false"; ?>;
	var edit = <?php echo($edit)?"true":"false"; ?>;
	ids = [];
	tabs = [];
	errors = [];
	var systems_cnt = getRowCounts();
	if(systems_cnt != 0){
		getAssignedSystems(assigned_systems_grid,'assigned_systems_ids');
		console.log('Got assigned systems');
	}
	
	if($("#lblFacilityId").html().indexOf("!") >= 0){ids.push("#facility_id");tabs.push("#li-tab-10");}	
	if($("#facility_id").val()==""){ids.push("#facility_id");tabs.push("#li-tab-10");}
	if($("#name").val()==""){ids.push("#name");tabs.push("#li-tab-10");}
	if($("#address").val()==""){ids.push("#address");tabs.push("#li-tab-10");}
	if($("#city").val()==""){ids.push("#city");tabs.push("#li-tab-10");}
	if($("#state").val()==""){ids.push("#state_chosen");tabs.push("#li-tab-10");}
	if($("#zip").val()==""){ids.push("#zip");tabs.push("#li-tab-10");}
	if($("#phone").val()==""){ids.push("#phone");tabs.push("#li-tab-10");}
	
	if($("#contact_name").val()==""){ids.push("#contact_name");tabs.push("#li-tab-30");}
	if($("#contact_phone").val()==""){ids.push("#contact_phone");tabs.push("#li-tab-30");}
	
	if($("#customer_id").val()==""){ids.push("#customer_id");tabs.push("#li-tab-40");}

	var regex = new RegExp(/[\'@\"\$\\\#\+\?]/g);
	if(document.getElementById("name").value.match(regex)){ids.push("#name");errors.push("Facility Name contains invalid characters  ' @ $ \ # + ? ");}
	if(document.getElementById("bill_name").value.match(regex)){ids.push("#bill_name");errors.push("Billing Name contains invalid characters  ' @ $ \ # + ? ");}

	$("#notes").val(ConvChar($("#notes").val()));
//	$("#credit_hold_notes").val(ConvChar($("#credit_hold_notes").val()));
			
	//if(debug){
		console.log("ids: " + ids);
		console.log("tabs: " + tabs);
		console.log("errors: " + errors);
	//}
	
	console.log("Archived:" + $("#archived").val());
	if(edit){
		if($("#archived").val().toLowerCase()=="n"){
			showErrors(ids,errors,tabs);
		}else{
			while(ids.length > 0) {
				ids.pop();
			}	
		}
	}else{
		showErrors(ids,errors,tabs);
	}
	
	if(ids.length <= 0){
		$.prompt("<h3><?php if($edit){echo "Update";}else{echo "Add New";} ?> Facility?</h3>",{
			title: "Question",
			buttons: { Yes: 1, No: -1 },
			focus: 1,
			submit:function(e,v,m,f){ 
				if(v == 1){
					$.prompt.close();
					<?php if($debug){?>
						if(!confirm('DEBUG: really submit?!')){
							return;	
						}
					<?php } ?>
					document.forms['form'].submit();
				}else{
					$.prompt.close();	
				}
				e.preventDefault();
			}
		});
	}

}

/////////////////////////////////////////////////////////////////////////////////////
function ConvChar( str ) {
  c = {'<':'&lt;', '>':'&gt;', '&':'&amp;', '"':'&quot;', "'":'&#039;',
       '#':'&#035;', '@':'&#64;' };
  return str.replace( /[<&>'"#@]/g, function(s) { return c[s]; } );
}

/////////////////////////////////////////////////////////////////////////////////////
function showErrors(ids,errors,tabs){
	if(tabs.length > 0){
		errors.unshift("<h1><b>Correct the red highlighted fields</b></h1>");
	}
	$("input, select, .chosen-container").each(function(index, element) { //set all inputs to not highlighted
		$(this).animate({
			borderColor: "#2C3594",
			boxShadow: 'none'
		});
	});
	
	$("#tabs > ul > li").each(function(index, element) { //set all inputs to not highlighted
		$(this).animate({
			borderColor: "#2C3594",
			boxShadow: 'none'
		});
	});
	
	$.each(ids,function(index,value){//highlight inputs
		$(value).animate({
			borderColor: "#cc0000",
			boxShadow: '0 0 5px 3px rgba(255,0,0,0.4)'
		});
	});
	
	$.each(tabs,function(index,value){ //highlight tabs
		$(value).animate({
			borderColor: "#cc0000",
			boxShadow: '0 0 5px 3px rgba(255,0,0,0.4)'
		});
	});
	
	$("#errors > span").html("");
	$.each(errors,function(index, value){
		$("#errors > span").append(value + "<br>");
	});
	$("#errors").show('slow');
	//$(document).scrollTop(0);
}
/////////////////////////////////////////////////////////////////////////////////////////////////
function checkFacility(str){
	if (str==""){
	  return;
	}
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}else{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			if(str != "<?php echo $rowF['facility_id']; ?>"){
				document.getElementById("lblFacilityId").innerHTML=xmlhttp.responseText;
			}else{
				document.getElementById("lblFacilityId").innerHTML="Facility ID";	
			}
		}
	}
	xmlhttp.open("GET","scripts/check_facility_id_edit.php?q="+str,true);
	xmlhttp.send();
}
/////////////////////////////////////////////////////////////////////////////////////
function loadsystems(){
	var systems = <?php echo $systems_json; ?>;
	<?php echo($has_systems_assigned)?"var assigned_systems = ".$assigned_systems_json:""; ?>;
		 
	systems_grid = new dhtmlXGridObject('systems_div');
	systems_grid.setImagePath("/resources/dhtmlx/dhtmlxGrid/imgs/");
	systems_grid.setHeader("System ID,System Nickname,Future,Pre Paid,Credit Hold");
	systems_grid.setColumnIds("system_id","system_nickname","future","pre_paid","credit_hold");
	systems_grid.setInitWidthsP("12,52,12,12,12");
	systems_grid.setColAlign("left,left,center,center,center");
	systems_grid.setColTypes("ro,ro,ro,ro,ro");
	systems_grid.setColSorting("str,str,str,str,str");
	systems_grid.setMultiLine(false);
	systems_grid.enableDragAndDrop(true);
	systems_grid.attachHeader("#text_filter,#text_filter,#text_filter,#text_filter,#text_filter");
	systems_grid.enableAutoWidth(true);
	systems_grid.attachEvent("onDrop", function(sId,tId,dId,sObj,tObj,sCol,tCol){
		if(systems_grid.cells(sId,2).getValue().toLowerCase() == 'y'){ //Future
			tObj.setRowTextStyle(sId,'color:#7F00FF;');	
		}
		if(systems_grid.cells(sId,3).getValue().toLowerCase() == 'y'){ //Pre Paid
			tObj.setRowTextStyle(sId,'color:#FF8040;');	
		}
		if(systems_grid.cells(sId,4).getValue().toLowerCase() == 'y'){ //Credit Hold
			tObj.setRowTextStyle(sId,'color:#FF0000;');	
		}
		tObj.sortRows(0,"str","asc");
		getRowCounts();
	});
	systems_grid.init();
	systems_grid.setSkin("dhx_skyblue");
	systems_grid.parse(systems,"json");
	$.each(systems.rows, function(key, value){
		if(value.userdata.future.toLowerCase() == 'y'){ //Future
			systems_grid.setRowTextStyle(value.id,'color:#7F00FF;');	
		}
		if(value.userdata.pre_paid.toLowerCase() == 'y'){ //Pre Paid
			systems_grid.setRowTextStyle(value.id,'color:#FF8040;');	
		}
		if(value.userdata.credit_hold.toLowerCase() == 'y'){ //Credit Hold
			systems_grid.setRowTextStyle(value.id,'color:#FF0000;');	
		}
	})
	
	assigned_systems_grid = new dhtmlXGridObject('assigned_systems_div');
	assigned_systems_grid.setImagePath("/resources/dhtmlx/dhtmlxGrid/imgs/");
	assigned_systems_grid.setHeader("System ID,System Nickname,Future,Pre Paid,Credit Hold");
	assigned_systems_grid.setColumnIds("system_id","nickname","future","pre_paid","credit_hold");
	assigned_systems_grid.setInitWidthsP("12,52,12,12,12");
	assigned_systems_grid.setColAlign("left,left,center,center,center");
	assigned_systems_grid.setColTypes("ro,ro,ro,ro,ro");
	assigned_systems_grid.setColSorting("str,str,str,str,str");
	assigned_systems_grid.setMultiLine(false);
	assigned_systems_grid.enableDragAndDrop(true);
	assigned_systems_grid.attachHeader("#text_filter,#text_filter,#text_filter,#text_filter,#text_filter");
	assigned_systems_grid.enableAutoWidth(true);
	assigned_systems_grid.attachEvent("onDrop", function(sId,tId,dId,sObj,tObj,sCol,tCol){
		if(assigned_systems_grid.cells(sId,2).getValue().toLowerCase() == 'y'){ //Future
			tObj.setRowTextStyle(sId,'color:#7F00FF;');	
		}
		if(assigned_systems_grid.cells(sId,3).getValue().toLowerCase() == 'y'){ //Pre Paid
			tObj.setRowTextStyle(sId,'color:#FF8040;');	
		}
		if(assigned_systems_grid.cells(sId,4).getValue().toLowerCase() == 'y'){ //Credit Hold
			tObj.setRowTextStyle(sId,'color:#FF0000;');	
		}
		tObj.sortRows(0,"str","asc");
		getRowCounts();
	});
	assigned_systems_grid.init();
	assigned_systems_grid.setSkin("dhx_skyblue");
	<?php if($has_systems_assigned){?>
	assigned_systems_grid.parse(assigned_systems,"json");
	$.each(assigned_systems.rows, function(key, value){
		if(value.userdata.future.toLowerCase() == 'y'){ //Future
			assigned_systems_grid.setRowTextStyle(value.id,'color:#7F00FF;');	
		}
		if(value.userdata.pre_paid.toLowerCase() == 'y'){ //Pre Paid
			assigned_systems_grid.setRowTextStyle(value.id,'color:#FF8040;');	
		}
		if(value.userdata.credit_hold.toLowerCase() == 'y'){ //Credit Hold
			assigned_systems_grid.setRowTextStyle(value.id,'color:#FF0000;');	
		}
	})
	<?php } ?>
	
	$("#assigned_systems_div, #systems_div").width("100%");
	
	getRowCounts();
}
/////////////////////////////////////////////////////////////////////////////////////
function getAssignedSystems(grid,input){
	var systems = [];
	grid.forEachRow(function(id){ // function that gets id of the row as an incoming argument
		systems.push(id)
		//console.log(id);
	})
	document.getElementById(input).value = systems.join(',');
}
/////////////////////////////////////////////////////////////////////////////////////
function getRowCounts(){
	
	try {
		var result = assigned_systems_grid.getRowsNum()
		$("#assigned_systems_count").html(result);
		return result;
	}catch(e){
		console.log(e);
		return 0;
	}
	
	
}

function gotocustomer(){
	<?php if(!$_SESSION['mobile_device']){ ?>
	$.prompt("<h3>Your changes will not be saved if you proceed!</h3>",{
		title: "Caution",
		buttons: { Proceed: 1, No: -1 },
		focus: 1,
		submit:function(e,v,m,f){ 
			if(v == 1){
				$.prompt.close();
				window.location.href = "customers.php?e&unique_id=<?php echo $rowF['customer_unique_id'] ?>&user_id=<?php echo $_SESSION['login'] ?>";
			}else{
				$.prompt.close();	
			}
			e.preventDefault();
		}
	});
	<?php }else{ ?>
	if(confirm("Your changes will not be saved if you proceed!")){
		window.location.href = "customers.php?e&unique_id=<?php echo $rowF['customer_unique_id'] ?>&user_id=<?php echo $_SESSION['login'] ?>";
	}
	<?php } ?>
}
</script>
</head>
<body>
<?php require_once($header_include); ?>
<div id="OIReportContent">
	<div id="styledForm">
		<form id="form" name="form" method="post" autocomplete="off" action="facilities_do.php">
			<?php if($edit){ ?>
			<h1>Editing Facility: <?php echo $rowF['name'];?></h1>
			<span class="red">Required fields marked with an *</span>
			<?php }else{ ?>
			<h1>Create New Facility </h1>
			<span class="red">Required fields marked with an *</span>
			<?php } ?>
			<p>&nbsp;</p>
			<div id="errors" style="text-align:center;display:none"> 
				<span style="color:#F00"> </span> 
			</div>

			<?php if($edit){ ?>
			<table width="100%" cellpadding="5" cellspacing="5">
				<tr>
					<td width="30%"><label>Archived</label>
						<select name="archived" id="archived">
							<option value="Y"<?php if($edit){if(strtolower($rowF['property'])=="a"){echo " selected";}} ?>>Yes</option>
							<option value="N"<?php if($edit){if(strtolower($rowF['property'])=="c"){echo " selected";}}else{echo " selected";} ?>>No</option>
						</select></td>
					<td>&nbsp;</td>
				</tr>
			</table>
			<?php } ?>
			<div class="line"></div>
<div id="tabs">
	 <ul>
		<li id="li-tab-10"><a href="#tab-10">Facility</a></li>
		<li id="li-tab-20"><a href="#tab-20">Billing</a></li>
		<li id="li-tab-30"><a href="#tab-30">Facility Contact</a></li>
		<li id="li-tab-40"><a href="#tab-40">Assigned Customer</a></li>
		<li id="li-tab-50"><a href="#tab-50">Assigned Systems</a></li>
<!--		<li id="li-tab-60"><a href="#tab-60">Credit</a></li>-->
		<li id="li-tab-70"><a href="#tab-70">Notes</a></li>
		<li id="li-tab-80"><a href="#tab-80">Special</a></li>
		<li id="li-tab-80"><a href="#tab-90">Docs</a></li>
	</ul>

<div id="tab-10" class="tab">
			<!--Facility-->
			<table width="100%" cellpadding="5" cellspacing="5">
				<tr>
					<td width="20%"><label id="lblFacilityId">Facility ID <span class="red">*</span></label>
						<input type="text" name="facility_id" id="facility_id" onblur="checkFacility(this.value)" value="<?php if($edit){echo $rowF['facility_id'];}else{echo $rowNextFacilityID['next_id'];} ?>"/></td>
					<td><label>Name <span style="font-size:.7em;">(As shown on building/Suite)</span> <span class="red">*</span></label>
						<input type="text" name="name" id="name" value="<?php if($edit){echo $rowF['name'];} ?>"/></td>
				</tr>
			</table>
			<table width="100%" cellpadding="5" cellspacing="5">
				<tr>
					<td colspan="2"><label>Address <span class="red">*</span></label>
						<input type="text" name="address" id="address" value="<?php if($edit){echo $rowF['address'];} ?>" /></td>
					<td width="24%"><label>City <span class="red">*</span></label>
						<input type="text" name="city" id="city" value="<?php if($edit){echo $rowF['city'];} ?>" /></td>
					<td width="16%"><label>State <span class="red">*</span></label>
						<select name="state" id="state">
							<option value=""></option>
							<?php
								foreach($states as $state_key=>$state_name){
									echo "<option value='" . $state_key . "'";
									if($edit){
										if($rowF['state']==$state_key){echo " selected";}
									}
									echo">" . $state_name . "</option>\n";
								}
							  ?>
						</select></td>
					<td width="12%"><label>Zip <span class="red">*</span></label>
						<input type="text" name="zip" id="zip" value="<?php if($edit){echo $rowF['zip'];} ?>" /></td>
				</tr>
			</table>
			<table width="100%" cellpadding="5" cellspacing="5">
				<tr>
					<td width="25%"><label>Phone <span class="red">*</span></label>
						<input type="text" name="phone" id="phone" value="<?php if($edit){echo $rowF['phone'];} ?>" /></td>
					<td width="25%"><label>Fax</label>
						<input type="text" name="fax" id="fax" value="<?php if($edit){echo $rowF['fax'];} ?>" /></td>
				</tr>
			</table>

</div>

<div id="tab-20" class="tab">
			<!--Billing Info-->
			<table width="100%" cellpadding="5" cellspacing="5">
				<tr>
					<td width="30%"><label>Bill Facility</label>
						<select name="bill_facility" id="bill_facility">
							<option value="Y"<?php if($edit){if(strtolower($rowF['bill_facility'])=="y"){echo " selected";}} ?>>Yes</option>
							<option value="N"<?php if($edit){if(strtolower($rowF['bill_facility'])=="n"){echo " selected";}}else{echo " selected";} ?>>No</option>
						</select></td>
					<td><span id="bill_warn" style="color:#F00;">&nbsp;</span></td>
				</tr>
			</table>
			<div id="billDiv">
				<table width="100%" cellpadding="5" cellspacing="5">
					<tr>
						<td colspan="5"><label>Name</label>
							<input type="text" name="bill_name" id="bill_name" value="<?php if($edit){echo $rowF['bill_name'];} ?>" /></td>
					</tr>
					<tr>
						<td colspan="2"><label>Address</label>
							<input type="text" name="bill_address" id="bill_address" value="<?php if($edit){echo $rowF['bill_address'];} ?>" /></td>
						<td width="24%"><label>City</label>
							<input type="text" name="bill_city" id="bill_city" value="<?php if($edit){echo $rowF['bill_city'];} ?>" /></td>
						<td width="16%"><label>State</label>
							<select name="bill_state" id="bill_state">
								<option value=""></option>
								<?php
									foreach($states as $state_key=>$state_name){
										echo "<option value='" . $state_key . "'";
										if($edit){
											if($rowF['bill_state']==$state_key){echo " selected";}
										}
										echo">" . $state_name . "</option>\n";
									}
								  ?>
							</select></td>
						<td width="12%"><label>Zip</label>
							<input type="text" name="bill_zip" id="bill_zip" value="<?php if($edit){echo $rowF['bill_zip'];} ?>" /></td>
					</tr>
				</table>
				<table width="100%" cellpadding="5" cellspacing="5">
					<tr>
						<td width="25%"><label>Phone</label>
							<input type="text" name="bill_phone" id="bill_phone" value="<?php if($edit){echo $rowF['bill_phone'];} ?>" /></td>
					</tr>
					<tr>
						<td width="25%"><label>Email</label>
							<input type="text" name="bill_email" id="bill_email" value="<?php if($edit){echo $rowF['bill_email'];} ?>" /></td>
					</tr>
				</table>
			</div>	
</div>

<div id="tab-30" class="tab">
			<!--Contact-->
			<table width="100%" cellpadding="5" cellspacing="5">
				<tr>
					<td width="50%"><label>Name <span class="red">*</span></label>
						<input type="text" name="contact_name" id="contact_name" value="<?php if($edit){echo $rowF['contact_name'];} ?>" /></td>
					<td width="50%"><label>Title</label>
						<input type="text" name="contact_title" id="contact_title" value="<?php if($edit){echo $rowF['contact_title'];} ?>" /></td>
				</tr>
			</table>
			<table width="100%" cellpadding="5" cellspacing="5">
				<tr>
					<td width="33%"><label>Phone <span class="red">*</span></label>
						<input type="text" name="contact_phone" id="contact_phone" value="<?php if($edit){echo $rowF['contact_phone'];} ?>" /></td>
					<td width="33%"><label>Cell</label>
						<input type="text" name="contact_cell" id="contact_cell" value="<?php if($edit){echo $rowF['contact_cell'];} ?>" /></td>
					<td><label>Email</label>
						<input type="email" name="contact_email" id="contact_email" value="<?php if($edit){echo $rowF['contact_email'];} ?>" /></td>
				</tr>
			</table>
			</td>
</div>

<div id="tab-40" class="tab">
			<!--Customer-->
			<table width="100%" cellpadding="5" cellspacing="5">
				<tr>
					<td width="50%"><label>Assigned Customer <span class="red">*</span></label>
						<input type="text" name="customer_id" id="customer_id" value="<?php if($edit){echo $rowF['customer_id'];} ?>" /></td>
					<td width="50%" style="text-align:center;"><a name="lookup_customer" class="iframeView button_jquery_lookup" href="customer_lookup.php">Lookup Customer</a></td>
				</tr>
			</table>
</div>

<div id="tab-50" class="tab">
			<!--Systems-->

					
			
					<table width="100%" cellpadding="5" cellspacing="5" id="systems_table">
						<tr align="center">
							<td colspan="2"><h2 style="margin:0; padding:0">System Assignments</h2></td>
						</tr>
						<tr align="center">
							<td colspan="2"><h2 style="margin:0; padding:0">Drag and drop systems to assign them</h2></td>
						</tr>
						<tr align="center">
							<td id="assigned_systems_label_td"><h2 style="margin:0; padding:0">Assigned Systems</h2></td>
							<td><h2 style="margin:0; padding:0">Systems</h2></td>
						</tr>
						<tr>
							<td width="50%">
								<div id="assigned_systems_div" style="width:100%; height:270px;background-color:white;"></div>
							</td>
							<td width="50%">
								<div id="systems_div" style="width:100%; height:270px;background-color:white;"></div>
							</td>
						</tr>
						<tr align="center">
							<td id="assigned_systems_count_td"><h2>Assigned: <span id="assigned_systems_count">0</span></h2></td>
							<td>&nbsp;</td>
						</tr>
					</table>


</div>

<!--<div id="tab-60" class="tab">-->
			<!--Credit-->
<!--			<table width="100%" cellpadding="5" cellspacing="5">-->
<!--				<tr>-->
<!--					<td width="30%"><label>Credit Hold</label>-->
<!--						<select name="credit_hold" id="credit_hold">-->
<!--							<option value="Y"<?php //if($edit){if(strtolower($rowF['credit_hold'])=="y"){echo " selected";}} ?>Yes</option>-->
<!--							<option value="N"<?php //if($edit){if(strtolower($rowF['credit_hold'])=="n"){echo " selected";}}else{echo " selected";} ?>No</option>-->
<!--						</select></td>-->
<!--					<td width="70%"><label>Credit Hold Notes</label>-->
<!--						<textarea name="credit_hold_notes" id="credit_hold_notes"><?php //if($edit){echo $rowF['credit_hold_notes'];} ?></textarea>-->
<!--					</td>-->
<!--				</tr>-->
<!--			</table>-->
<!--</div>-->
<div id="tab-70" class="tab">
			<!--Notes-->
			<table width="100%" cellpadding="5" cellspacing="5">
				<tr>
					<td width="70%"><label>Important Facility Notes</label>
						<textarea name="notes" id="notes"><?php if($edit){echo $rowF['notes'];} ?></textarea>
					</td>
				</tr>
			</table>
</div>

<div id="tab-80" class="tab">
			<!--Special-->
			<table width="100%" cellpadding="5" cellspacing="5">
				<tr>
					<td colspan="3"><label>NPS Email</label>
						<input type="text" name="nps_email" id="nps_email" value="<?php if($edit){echo $rowF['nps_email'];} ?>" /></td>
					<td colspan="3"><label>NPS Email Name</label>
						<input type="text" name="nps_name" id="nps_name" value="<?php if($edit){echo $rowF['nps_name'];} ?>" /></td>
				</tr>
			</table>
</div>

<div id="tab-90" class="tab">
			<!--Docs-->
			<div id="uploadDocs">
				<a name="uploadX" class="iframeUpload button_jquery_upload"  href="/resources/kcfinder/kcfinder.php?unique_id=<?php echo $unique_id; ?>&table=facilities_files"><?php echo $uploadBtnValue; ?></a>
			</div>

</div>

</div>
			<div class="line"></div>
			<table width="100%" cellpadding="5" cellspacing="5">
				<tr>
					<td width="50%"><a name="Submit" class="button_jquery_save" onclick="submitcheck()"><?php if(!$edit){echo "Create New Facility";}else{ echo "Save Facility Changes";} ?></a></td>
				</tr>
				<tr>
					<td width="50%"><a name="Goto" class="button_jquery_lookup" onClick="gotocustomer()">Goto Customer</a></td>
				</tr>
			</table>
			<div style="display:none">
<!--				<input type="hidden" name="credit_hold_change" id="credit_hold_change" value="" />-->
				<input type="hidden" name="assigned_systems_ids" id="assigned_systems_ids" value="" />
				<input type="hidden" name="orig_assigned_systems_ids" id="orig_assigned_systems_ids" value="" />
				<input name="edit_facility" id="edit_facility" type="hidden" value="<?php if($edit){echo "Y";}else{echo "N";} ?>" />
				<input name="unique_id" id="unique_id" type="text" value="<?php echo $unique_id; ?>" />
				<?php if($debug){ ?><input name="debug" id="debug" type="hidden" value="Y" /><?php } ?>
			</div>

			<br />
			<br />
		</form>
		
	</div>
</div>
<?php require_once($footer_include); ?>
