<?php
//Update Completed 12/9/14
$debug = false;
if(isset($_POST['debug'])){
	$debug = true;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

dd($_POST);

$send = true;
if($settings->disable_email == '1'){
	$send = false;	
}

if(!isset($_POST['unique_id'])){
	$log->logerr('Blank Unique Id',1016,true,basename(__FILE__),__LINE__);	
}else{
	$unique_id=$_POST['unique_id'];	
}

if(strtolower($_POST['edit_facility']) == "y"){
	$edit = true;	
}else{
	$edit = false;
}

if(strtolower($_POST['archived'])=="y"){
	//Get users needed disabled
	$sql="SELECT u.uid, sbc.system_id
	FROM users AS u
	LEFT JOIN systems_assigned_customer AS sac ON sac.uid = u.uid
	LEFT JOIN systems_base_cont AS sbc ON sbc.unique_id = sac.system_unique_id
	LEFT JOIN facilities AS f ON f.unique_id = sbc.facility_unique_id
	WHERE f.customer_unique_id IN (SELECT customer_unique_id FROM facilities WHERE unique_id = '".$unique_id."')
	GROUP BY u.uid
	HAVING COUNT(u.uid) = 1;";
	d($sql);
	if(!$resultCUID = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$archive_users = array();
	while($rowCUID = $resultCUID->fetch_assoc()){
		array_push($archive_users, $rowCUID['uid']);
	}
	d($archive_users);
	$sql="UPDATE users SET active = 'N' WHERE uid IN ('".implode("', '",$archive_users)."');";
	d($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}

	$sql="DELETE a FROM systems_assigned_customer AS a 
	LEFT JOIN systems_base_cont AS sbc ON sbc.ver_unique_id = a.system_ver_unique_id
	WHERE sbc.facility_unique_id = '".$_POST['unique_id']."';";
	d($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}

	$sql="UPDATE customers_email_list AS cel
	LEFT JOIN systems_base_cont AS sbc ON sbc.ver_unique_id = cel.system_ver_unique_id 
	SET cel.active = 'N'
	WHERE sbc.facility_unique_id = '".$_POST['unique_id']."';";
	d($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}

	$sql="UPDATE facilities AS f SET f.property = 'A'
	WHERE f.unique_id ='".$_POST['unique_id']."';";
	d($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$log->loginfo($_POST['system_id'].' - '.$unique_id,110,false,basename(__FILE__),__LINE__);
}else{
	
	$sql="SELECT c.unique_id FROM customers AS c WHERE c.customer_id = '".$_POST['customer_id']."';";
	d($sql);
	if(!$resultCustomer = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$rowCustomer = $resultCustomer->fetch_assoc();
	
//	$credit_hold_date = '';
//	if(strtolower($_POST['credit_hold']) == 'y'){
//		$credit_hold_date = date(storef, time());
//	}
	
	$address = $_POST['address'];
	$city = $_POST['city'];
	$state = $_POST['state'];
	$zip = $_POST['zip'];
	
	$geo_code = true;
	if($address == "" or $city == "" or $state == "" or $zip == ""){
		$geo_code = false;	
		$log->logerr('users_do.php',1036);
	}
	if($geo_code){
		require($_SERVER['DOCUMENT_ROOT'].'/resources/php_geocode/GoogleMapsGeocoder.php');
		$Geocoder = new GoogleMapsGeocoder();	
		$geo_address = $address.",".$city.",".$state.",".$zip;
		$Geocoder->setAddress($geo_address);
		$geo_response = $Geocoder->geocode();
	
		if($geo_response['status'] == "OK"){
			$lat = $geo_response['results'][0]['geometry']['location']['lat'];
			$lng = $geo_response['results'][0]['geometry']['location']['lng'];		
		}else{
			$lat = 0;
			$lng = 0;	
		}
	}	
	
	$archived = 'C';
	if(strtolower($_POST['archived']) == 'y'){
		$archived = 'A';	
	}
	
	$values = array(
		'facility_id' => $_POST['facility_id'], 
		'customer_unique_id' => $rowCustomer['unique_id'],
		'name' => addslashes($_POST['name']), 
		'address' => addslashes($_POST['address']),
		'city' => addslashes($_POST['city']),
		'state' => $_POST['state'],
		'zip' => $_POST['zip'],
		'lat'=> $lat,
		'lng'=> $lng,
		'phone' => $_POST['phone'],
		'fax' => $_POST['fax'],
		'bill_facility' => $_POST['bill_facility'],
		'bill_name' => addslashes($_POST['bill_name']),
		'bill_address' => addslashes($_POST['bill_address']),
		'bill_city' => addslashes($_POST['bill_city']),
		'bill_state' => $_POST['bill_state'],
		'bill_zip' => $_POST['bill_zip'],
		'bill_phone' => addslashes($_POST['bill_phone']),
		'bill_email' => addslashes($_POST['bill_email']),
		'contact_name' => addslashes($_POST['contact_name']), 
		'contact_title' => addslashes($_POST['contact_title']), 
		'contact_phone' => $_POST['contact_phone'],
		'contact_cell' => $_POST['contact_cell'], 
		'contact_email' => $_POST['contact_email'],
//		'credit_hold' => $_POST['credit_hold'],
//		'credit_hold_date' => $credit_hold_date,
//		'credit_hold_notes' => addslashes($_POST['credit_hold_notes']),
		'notes' => $_POST['notes'], 
//		'email_list' => $_POST['email_list'],
		'nps_email' => $_POST['nps_email'],
		'nps_name' => addslashes($_POST['nps_name']),
		'archived' => $archived,
		'unique_id' => $unique_id,
		'created_by' => $myusername,
		'created_date' => date(storef,time())
	);
	d($values);	
	
	$sql="INSERT INTO facilities (facility_id, customer_unique_id, name, address, city, state, zip, lat, lng, phone, fax, bill_facility, bill_name, bill_address, bill_city, bill_state,
		bill_zip, bill_phone, bill_email, contact_name, contact_title, contact_phone, contact_cell, contact_email, notes,
		nps_email, nps_name, property, unique_id, created_by, created_date)
		VALUES ('". implode("', '", $values) . "') ON DUPLICATE KEY
		UPDATE facility_id = VALUES(facility_id), customer_unique_id = VALUES(customer_unique_id), name = VALUES(name), address = VALUES(address), city = VALUES(city), 
		state = VALUES(state), zip = VALUES(zip), lat = VALUES(lat), lng = VALUES(lng), phone = VALUES(phone), fax = VALUES(fax), bill_facility = VALUES(bill_facility), bill_name = VALUES(bill_name), 
		bill_address = VALUES(bill_address), bill_city = VALUES(bill_city), bill_state = VALUES(bill_state), bill_zip = VALUES(bill_zip), bill_phone = VALUES(bill_phone), bill_email = VALUES(bill_email),
		contact_name = VALUES(contact_name), contact_title = VALUES(contact_title), contact_phone = VALUES(contact_phone), contact_cell = VALUES(contact_cell), 
		contact_email = VALUES(contact_email), 
		notes = VALUES(notes), nps_email = VALUES(nps_email), nps_name = VALUES(nps_name), property = VALUES(property), unique_id = VALUES(unique_id), 
		edited_by = '".$myusername."',  edited_date = '".date(storef,time())."';";
	
	d($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
		
	if(isset($_POST['orig_assigned_systems_ids']) and strlen($_POST['orig_assigned_systems_ids']) > 0){
		$sql="UPDATE systems_base_cont SET facility_unique_id = NULL
		WHERE id IN (".$_POST['orig_assigned_systems_ids'].");";
		d($sql);
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
	}
	
	if(isset($_POST['assigned_systems_ids']) and strlen($_POST['assigned_systems_ids']) > 0){
		$sql="UPDATE systems_base_cont SET facility_unique_id = '$unique_id'
		WHERE id IN (".$_POST['assigned_systems_ids'].");";
		d($sql);
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
	}
	
//	if(strtolower($_POST['credit_hold_change']) == 'y'){
//		$sql="UPDATE systems_base_cont SET credit_hold = '".$_POST['credit_hold']."' WHERE facility_unique_id = '$unique_id';";
//		d($sql);
//		if(!$result = $mysqli->query($sql)){
//			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
//			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
//			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
//		}
//	}
	
	if($edit){
		$log->loginfo($_POST['facility_id'].' - '.$unique_id,112,false,basename(__FILE__),__LINE__);	
	}else{
		$log->loginfo($_POST['facility_id'].' - '.$unique_id,111,false,basename(__FILE__),__LINE__);
	}
}//else archived

?>
<!DOCTYPE html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<?php require_once($js_include);?>

<script type="text/javascript">
function delayer(){
    <?php if(!$debug){ ?>window.location = "<?php echo $refering_uri; ?>"<?php } ?>
}
</script>
</head>
<body onLoad="setTimeout('delayer()', 3000)">
<?php require_once($header_include); ?>
<div id="OIReportContent">
	<h1>Facility <?php if($edit){if(strtolower($_POST['archived']) == 'y'){echo "Archived";}else{echo "Updated";}}else{echo "Created";} ?> Successfully</h1><br>
	Facility ID: <?php echo $_POST['facility_id']; ?><br>
	Facility Name: <?php echo $_POST['name']; ?><br><br>
    <h1>Page will return to main page in 3 seconds</h1>
</div>
<?php require_once($footer_include); ?>

