<?php
//Update Completed 3/11/205
$debug = false;
if(isset($_GET['debug'])){
	$debug = true;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}
$show_data = false;

$view = false;
if(isset($_GET['view'])){
	$view = true;
}

$fbc_get_id = false;
if(isset($_GET['id'])){
	$fbc_get_id = $_GET['id'];
}

$edit = false;
if(isset($_GET['edit'])){
	$view = false;
	if(!isset($_SESSION['perms']['perm_close_parcarfbc'])){
		//$log->logerr("You do not have permission to edit/close PAR/CAR/FBC",1057,false,basename(__FILE__),__LINE__);
		$view = true;
	}else{
		$edit = true;
	}
}

if($edit or $view){
	$show_data = true;
	$users['none'] = array('name'=>'');
	if(!isset($_GET['unique_id'])){
		$log->logerr("Invalid CAR Unique ID",1059,true,basename(__FILE__),__LINE__);
	}
	$sql="SELECT u.uid, u.name, u.timezone
	FROM users AS u
	INNER JOIN users_groups AS ug ON ug.uid = u.uid
	INNER JOIN users_group_id AS grid ON grid.id = ug.gid AND grid.`group` = 'grp_employee'
	WHERE u.id >= 10
	ORDER BY u.uid ASC;";
	s($sql);
	if(!$resultUsers = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	while($rowUsers = $resultUsers->fetch_assoc()){
		$users[$rowUsers['uid']]=array('name'=>$rowUsers['name'], 'timezone'=>$rowUsers['timezone']);
	}
	d($users);
	$sql="SELECT car.*, fbc.id AS fbc_id, COUNT(icf.id) AS files_count
		FROM iso_car AS car
		LEFT JOIN iso_fbc AS fbc ON fbc.unique_id = car.fbc_unique_id
		LEFT JOIN iso_car_files AS icf ON icf.unique_id = car.unique_id
		WHERE car.unique_id = '".$_GET['unique_id']."';";
	s($sql);
	if(!$resultCar = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$rowCar = $resultCar->fetch_assoc();
	d($rowCar);
	$unique_id = $rowCar['unique_id'];	
	
	if(!$view){
		if(strtolower($rowCar['closed']) == 'y'){
			$log->logerr('You cannot edit a closed CAR/PAR/FBC',1063,true,basename(__FILE__),__LINE__);
		}
	}
}else{
	$unique_id = md5(uniqid());
	if($fbc_get_id){
		$sql="SELECT unique_id FROM iso_fbc WHERE id = '$fbc_get_id';";
		s($sql);
		if(!$resultFbcGet = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
		if($resultFbcGet->num_rows == 0){
			$log->logerr('Invalid CAR ID',1064,true,basename(__FILE__),__LINE__);	
		}
		$rowFbcGet = $resultFbcGet->fetch_assoc();	
		$fbc_get_id = $rowFbcGet['unique_id'];
	}
}


?>
<!DOCTYPE html>
<html>
<head>
<?php require_once($head_include);?>
<?php require_once($css_include);?>

<link href="/resources/css/form_report.css" rel="stylesheet" type="text/css" media="screen">

<?php require_once($js_include);?>

<script src="/resources/js/jquery.ui.datepicker.js"></script>
<script src="/resources/js/jquery-ui-timepicker-addon.js"></script>
<script src="/resources/js/jquery-dynamic-form_MODIFIED.js"></script>
<script type="text/javascript">
var users_json = <?php echo json_encode($users); ?>;

$(document).ready(function() {
	$(".iframeUpload").fancybox({
			'type'			: 'iframe',
			'height'		: 600,
			'fitToView'		: true,
			'maxWidth'		: 900,
			'maxHeight'		: 600,
			'autoSize'		: false,
			'closeBtn'		: true,
			'margin'		: [5,5,5,5]
	});
	
	$(".button_jquery_save").button({
		icons: {
			primary: "ui-icon-disk"
		}
	});
	
	$(".button_jquery_download").button({
		icons: {
			primary: "ui-icon-disk"
		}
	});
	
	$(".button_jquery_verify").button({
		icons: {
			primary: "ui-icon-check"
		}
	});
	
	$(".button_jquery_edit").button({
		icons: {
			primary: "ui-icon-pencil"
		}
	});
		

<?php if(!$view){ ?>
	$(function() {	
		$( ".date_uid" ).datepicker({
			numberOfMonths: 1,
			showButtonPanel: true,
			changeYear: true,
			constrainInput: true,
			duration: "fast",
			dateFormat: "<?php echo dpdispfd; ?>"
		});	
	});
	
	$(".chooser").chosen({
		no_results_text: "Oops, nothing found!",
		disable_search_threshold: 10,
		placeholder_text_single: '  ',
		width: '100%'
	});
<?php } ?>
/////////////////////////////////////////////////////////////////////////////////////
});
/////////////////////////////////////////////////////////////////////////////////////

function show_delete(){
	if(document.getElementById('delete').checked == false){
		$('.delete_reason_div').show();
		document.getElementById('delete').checked = true;
		<?php if(!$_SESSION['mobile_device']){ ?>
			$.prompt("<h3>Fill out the reason for deleting this CAR</h3>",{
				title: "Delete CAR Reason"
			});
		<?php }else{ ?>
			alert("Close this CAR?");
		<?php } ?>
	}else{
		submitcheck('delete');		
	}
}

function submitcheck(data){
	
	ids = [];
	errors = [];
	var find = ["\""];
	var replace = ["'"];
	var datere = /^20(1|2)\d-((1[0-2])|(0[1-9]))-(([0-2][0-9])|3(0|1))$/; //date yyyy-mm-dd
	var dtre = /^\d{1,10}(\.\d{1,10})?$/; //downtime 00000.00000  .00000 optional
	//var edit_car = <?php if($edit){ echo 'true';}else{ echo 'false';} ?>;
	
	if($('#title').val()==''){ids.push('#title'); errors.push('Title blank');}
	if($('#title').val().split(' ').length <= 2){ids.push('#title'); errors.push('Title less than 3 words');}
	if($('#department').val()==''){ids.push('#department'); errors.push('Description blank');}
	if($('#location').val()==''){ids.push('#location'); errors.push('Location blank');}
	if($('#finding').val()==''){ids.push('#finding'); errors.push('Finding blank');}
	if($('#finding').val().split(' ').length <= 4){ids.push('#finding'); errors.push('Finding less than 5 words');}
	if($('#correction').val()==''){ids.push('#correction'); errors.push('Correction blank');}
	if($('#correction').val().split(' ').length <= 4){ids.push('#correction'); errors.push('Correction less than 5 words');}
	
	var textarea = $('#finding').val();
	textarea = textarea.replaceArray(find, replace);
	$('#finding').val(textarea);
	
	var textarea = $('#correction').val();
	textarea = textarea.replaceArray(find, replace);
	$('#correction').val(textarea);
	
	if(document.getElementById("delete").checked == true){
		if($('#delete_reason').val()==''){ids.push('#delete_reason'); errors.push('Delete Reason blank');}
		if($('#delete_reason').val().split(' ').length <= 4){ids.push('#delete_reason'); errors.push('Delete Reason less than 5 words');}
		var textarea = $('#delete_reason').val();
		textarea = textarea.replaceArray(find, replace);
		$('#delete_reason').val(textarea);		
	}else{
		//if(edit_car == true){
		if(data == 'close'){
			if($('#root_cause').val()==''){ids.push('#root_cause'); errors.push('Root Cause blank');}
			if($('#root_cause').val().split(' ').length <= 4){ids.push('#root_cause'); errors.push('Root Cause less than 5 words');}
			if($('#root_cause_uid').val()=='' || $('#root_cause_uid').val()=='none'){ids.push('#root_cause_uid_chosen'); errors.push('Root Cause User blank');}
			if(!datere.test($('#root_cause_date').val())){ids.push("#root_cause_date"); errors.push("Root Cause Date Invalid");}
			
			
			if($('#corrective_action').val()==''){ids.push('#corrective_action'); errors.push('Corrective Action blank');}
			if($('#corrective_action').val().split(' ').length <= 4){ids.push('#corrective_action'); errors.push('Corrective Action less than 5 words');}
			if($('#corrective_action_uid').val()=='' || $('#corrective_action_uid').val()=='none'){ids.push('#corrective_action_uid_chosen'); errors.push('Corrective Action User blank');}
			if(!datere.test($('#correction_target_date').val())){ids.push("#correction_target_date"); errors.push("Correction Target Date Invalid");}
			if(!datere.test($('#corrected_date').val())){ids.push("#corrected_date"); errors.push("Corrected Date Invalid");}
			
			if($('#verification').val()==''){ids.push('#verification'); errors.push('Verification blank');}
			if($('#verification').val().split(' ').length <= 4){ids.push('#verification'); errors.push('Verification less than 5 words');}
			if($('#verification_uid').val()=='' || $('#verification_uid').val()=='none'){ids.push('#verification_uid_chosen'); errors.push('Verification User blank');}
			if(!datere.test($('#verification_date').val())){ids.push("#verification_date"); errors.push("Verification Date Invalid");}
			
			
			var textarea = $('#root_cause').val();
			textarea = textarea.replaceArray(find, replace);
			$('#root_cause').val(textarea);
			
			var textarea = $('#corrective_action').val();
			textarea = textarea.replaceArray(find, replace);
			$('#corrective_action').val(textarea);
			
			var textarea = $('#verification').val();
			textarea = textarea.replaceArray(find, replace);
			$('#verification').val(textarea);
		}			
	}
	
	console.log("ids: " + ids);
	console.log("errors: " + errors);
	showErrors(ids,errors);
	
	if(ids.length <= 0){
		if(data == 'close'){
			<?php if(!$_SESSION['mobile_device']){ ?>
				$.prompt("<h3>Close this CAR?</h3>",{
					title: "Close CAR",
					buttons: { Yes: 1, No: -1 },
					focus: 1,
					submit:function(e,v,m,f){ 
						e.preventDefault();
						showErrors(ids,errors);
						$("#errors").hide();
						if(v == 1){
							$.prompt.close();
							document.getElementById("close").checked = true;
							document.forms["form"].submit();
						}else{
							$.prompt.close();
						}
					}
				});
			<?php }else{ ?>
				if(confirm("Close this CAR?")){
					document.getElementById("close").checked = true;
					document.forms["form"].submit();
				}
			<?php } ?>
		}else if(data == 'delete'){
			<?php if(!$_SESSION['mobile_device']){ ?>
				$.prompt("<h3>Delete this CAR?</h3>",{
					title: "Delete CAR",
					buttons: { Yes: 1, No: -1 },
					focus: 1,
					submit:function(e,v,m,f){ 
						e.preventDefault();
						showErrors(ids,errors);
						$("#errors").hide();
						if(v == 1){
							$.prompt.close();
							document.forms["form"].submit();
						}else{
							$.prompt.close();
						}
					}
				});
			<?php }else{ ?>
				if(confirm("Delete this CAR?")){
					document.getElementById("delete").checked = true;
					document.forms["form"].submit();
				}
			<?php } ?>
		//}else if(edit_car == true){
		}else if(data == 'save'){
			<?php if(!$_SESSION['mobile_device']){ ?>
				$.prompt("<h3>Save changes to this CAR?</h3>",{
					title: "Save CAR",
					buttons: { Yes: 1, No: -1 },
					focus: 1,
					submit:function(e,v,m,f){ 
						e.preventDefault();
						showErrors(ids,errors);
						$("#errors").hide();
						if(v == 1){
							$.prompt.close();
							document.forms["form"].submit();
						}else{
							$.prompt.close();
						}
					}
				});
			<?php }else{ ?>
				if(confirm("Save changes to this CAR?")){
					document.forms["form"].submit();
				}
			<?php } ?>						
		}else{
			<?php if(!$_SESSION['mobile_device']){ ?>
				$.prompt("<h3>Submit this CAR?</h3>",{
					title: "Submit CAR",
					buttons: { Yes: 1, No: -1 },
					focus: 1,
					submit:function(e,v,m,f){ 
						e.preventDefault();
						showErrors(ids,errors);
						$("#errors").hide();
						if(v == 1){
							$.prompt.close();
							document.forms["form"].submit();
						}else{
							$.prompt.close();
						}
					}
				});
			<?php }else{ ?>
				if(confirm("Submit this CAR?")){
					document.forms["form"].submit();
				}
			<?php } ?>
	
			
		}
		
		
	}
}
/////////////////////////////////////////////////////////////////////////////////////
function showErrors(ids,errors){
	//thin solid #2C3594
	$("input, select, .chosen-container").each(function(index, element) {
		$(this).animate({
			borderColor: "#2C3594",
			boxShadow: 'none'
		});
	});
	
	$.each(ids,function(index,value){
		$(value).animate({
			borderColor: "#cc0000",
			boxShadow: '0 0 5px 3px rgba(255,0,0,0.4)'
		});
	});
	$("#errors > span").html("");
	$.each(errors,function(index, value){
		$("#errors > span").append(value + "<br>");
	});
	$("#errors").show('slow');
	$(document).scrollTop(0);
}

String.prototype.replaceArray = function(find, replace) {
	var replaceString = this;
	var regex; 
	for (var i = 0; i < find.length; i++) {
		regex = new RegExp(find[i], "g");
		replaceString = replaceString.replace(regex, replace[i]);
	}
	return replaceString;
};

function download_car(){
	$.ajax({
		type: 'POST',
		url: 'iso_car_download.php',
		data: { unique_id: '<?php echo $unique_id; ?>'},
		success:function(data){
			alert('downloading');
		}
	});	
	
}

/////////////////////////////////////////////////////////////////////////////////////
</script>

</head>
<body>
<?php require_once($header_include); ?>
<div id="OIReportContent"> 
<div id="stylized" class="myform">
<form id="form" name="form" method="post" enctype='multipart/form-data' action="iso_car_save.php">
<div id="srHeaderDiv">
	<?php if($view and strtolower($rowCar['closed']) == 'n'){ ?>
	<div align="center" style="margin-bottom:15px;">
		<h1 style="color:red">THIS CAR IS NOT CLOSED!</h1>
	</div>
	<?php } ?>
	<h1>Corrective Action Request</h1>
	<?php if($edit or $view){ ?><h1>ID: <?php echo $rowCar['id']; ?></h1><?php } ?>
	<br />
</div>
<div id="main"><!-- do not remove -->
<div id="errors" style="text-align:center;display:none; margin-bottom:25px; font-size:18px;">
	<h2 style="margin:0px; padding:0px; font-size:24px;">Errors to fix</h2>
	<span style="color:#F00">
	</span>
</div>

<div id="srDataDiv">
    <table id="srDataTable" class="srTable">
        <tr>
			<td class="rowLabelBig">Title of CAR:</td>
         	<td class="rowData"><input type="text" id="title" name="title" class="tooltip" <?php if($view){ echo 'readonly';} ?> value="<?php if($show_data){ echo $rowCar['title'];} ?>" /></td>
          	<td class="rowLabelBig">Department of Failure:</td>
          	<td class="rowData"><input type="text" id="department" name="department" class="tooltip" <?php if($view){ echo 'readonly';} ?> value="<?php if($show_data){ echo $rowCar['department'];} ?>" /></td>
        </tr>
        <tr>
         	<td class="rowLabelBig">Location of Failure:</td>
         	<td class="rowData"><input name="location" id="location" class="tooltip" <?php if($view){ echo 'readonly';} ?> value="<?php if($show_data){ echo $rowCar['location'];} ?>" /></td>
         	<td class="rowLabelBig">Customer complaint issued:</td>
			<?php if(!$view){ ?>
			<td class="rowData"><select class="chooser tooltip" name="fbc" id="fbc" >
				<option <?php if($show_data){ if(strtolower($rowCar['fbc']) == 'n'){ echo 'selected'; } } ?> value="N">No</option>
				<option <?php if($show_data){ if(strtolower($rowCar['fbc']) == 'y'){ echo 'selected'; } }else{if($fbc_get_id){echo 'selected';}}?> value="Y">Yes</option>
				</select></td> 
			<?php }else{ ?>
         	<td class="rowData"><input name="fbc" id="fbc" <?php if($view){ echo 'readonly';} ?> value="<?php if($show_data){ if(strtolower($rowCar['fbc']) == 'y'){echo 'Yes';}else{ echo 'No';}} ?>" /></td>
			<?php } ?>
        </tr>
		<?php if(strtolower($rowCar['fbc']) == 'y'){ ?>
		<tr>
			<td class="rowLabelBig">View Customer Complaint:</td>
			<?php if($rowCar['fbc_id'] != ''){ ?>
         	<td class="rowData"><a target="new" href="iso_fbc.php?view&unique_id=<?php echo $rowCar['fbc_unique_id']; ?>">Complaint&emsp;<?php echo $rowCar['fbc_id']; ?></a></td>
			<?php }else{ ?>
         	<td class="rowData"><span style="color:#FF0000;">FBC not linked to CAR</span></td>
			<?php } ?>			
        </tr>
		<?php } ?>
    </table>
</div>

<?php if(!$view){ ?>
<div id="srDataDiv">
    <table id="srDataTable" class="srTable">
        <tr>
			<td class="rowLink"><a class="iframeView" href="none.php">Form Example 1</a></td>
          	<td class="rowLink"><a class="iframeView" href="none.php">Form Example 2</a></td>
        </tr>
    </table>
</div>

<?php } ?>
<?php if($edit or $view){ ?>
<div id="srServiceDiv">
    <table id="srServiceTable" class="srTable"  >
		
		<?php if((!is_null($rowCar['edited_date']) or $rowCar['edited_date'] != '') and !$view){ ?>
		<tr>
          <td class="rowDataNote">Last edited by: <?php echo $users[$rowCar['edited_uid']]['name']."&emsp;".date(phpdispfd,strtotime($rowCar['edited_date'])); ?></td>
        </tr>
		<?php } ?>
    </table>
</div>
<?php } ?>

<div id="srServiceDiv">
    <table id="srServiceTable" class="srTable"  >
        <tr>
          <td class="rowLabelBig">Finding</td>
        </tr>
		<?php if(!$view){ ?>
		<tr>
          <td class="rowLabelNote">Brief description of what in the process failed</td>
        </tr>
		<?php } ?>
        <tr>
          <td class="rowData"><textarea id="finding" name="finding" <?php if($view){ echo 'readonly';} ?> maxlength="10000" ><?php if($show_data){ echo $rowCar['finding'];} ?></textarea></td>
        </tr>
		<?php if(!is_null($rowCar['created_date']) or $rowCar['created_date'] != ''){ ?>
		<tr>
          <td class="rowDataNote">Finding by: <?php echo $users[$rowCar['created_uid']]['name']."&emsp;".date(phpdispfd,strtotime($rowCar['created_date'])); ?></td>
        </tr>
		<?php } ?>
    </table>
</div>

<div id="srServiceDiv">
    <table id="srServiceTable" class="srTable"  >
        <tr>
          <td class="rowLabelBig">Correction</td>
        </tr>
		<?php if(!$view){ ?>
		<tr>
          <td class="rowLabelNote">Action taken to contain the failure when it was detected</td>
        </tr>
		<?php } ?>
        <tr>
          <td class="rowData"><textarea id="correction" name="correction" <?php if($view){ echo 'readonly';} ?> maxlength="10000" ><?php if($show_data){ echo $rowCar['correction'];} ?></textarea></td>
        </tr>
		<?php if(!is_null($rowCar['created_date']) or $rowCar['created_date'] != ''){ ?>
		<tr>
          <td class="rowDataNote">Correction by: <?php echo $users[$rowCar['created_uid']]['name']."&emsp;".date(phpdispfd,strtotime($rowCar['created_date'])); ?></td>
        </tr>
		<?php } ?>
    </table>
</div>

<?php if($edit or $view){ ?>
<div id="srServiceDiv">
    <table id="srServiceTable" class="srTable"  >
        <tr>
          <td colspan="4" class="rowLabelBig">Root Cause</td>
        </tr>
        <tr>
          <td colspan="4" class="rowData"><textarea id="root_cause" name="root_cause" <?php if($view){ echo 'readonly';} ?> maxlength="10000" ><?php if($show_data){ echo $rowCar['root_cause'];} ?></textarea></td>
        </tr>
		<tr>
			<td width="25%" class="rowLabel">Analysis By</td>
			<td width="25%" class="rowData"><select class="chooser" name="root_cause_uid" id="root_cause_uid">
			<?php 
				foreach($users as $uid=>$data){
					echo "<option value='".$uid."'";
					if($uid == $rowCar['root_cause_uid']){echo " selected";}
					echo ">" . $data['name'] . "</option>\n";		
				}
			?>
			</select></td>
			<td width="25%" class="rowLabel">Analysis Date</td>
			<td width="25%" class="rowData"><input type="text" name="root_cause_date" id="root_cause_date" class="date_uid" value="<?php if(!is_null($rowCar['root_cause_date']) or $rowCar['root_cause_date'] != ''){ echo date(phpdispfd,strtotime($rowCar['root_cause_date']));} ?>" /></td>
		</tr>
    </table>
</div>

<div id="srServiceDiv">
    <table id="srServiceTable" class="srTable"  >
        <tr>
          <td colspan="4" class="rowLabelBig">Corrective Action</td>
        </tr>
		<?php if(!$view){ ?>
		<tr>
          <td colspan="4" class="rowLabelNote">Action taken to eliminate the cause of failure in the future</td>
        </tr>
		<?php } ?>
        <tr>
          <td colspan="4" class="rowData"><textarea id="corrective_action" name="corrective_action" <?php if($view){ echo 'readonly';} ?> maxlength="10000" ><?php if($show_data){ echo $rowCar['corrective_action'];} ?></textarea></td>
        </tr>
		<tr>
			<td class="rowLabel">Corrected By</td>
			<td class="rowData"><select class="chooser" name="corrective_action_uid" id="corrective_action_uid">
			<?php 
				foreach($users as $uid=>$data){
					echo "<option value='".$uid."'";
					if($uid == $rowCar['corrective_action_uid']){echo " selected";}
					echo ">" . $data['name'] . "</option>\n";		
				}
			?>
			</select></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="rowLabel">Correction Target Date:</td>
         	<td class="rowData"><input type="text" id="correction_target_date" name="correction_target_date"  class="date_uid" <?php if($view){ echo 'readonly';} ?> value="<?php if($show_data and !is_null($rowCar['correction_target_date'])){ echo date(phpdispfd,strtotime($rowCar['correction_target_date']));} ?>" /></td>
          	<td class="rowLabel">Corrected Date:</td>
          	<td class="rowData"><input type="text" id="corrected_date" name="corrected_date"  class="date_uid" <?php if($view){ echo 'readonly';} ?> value="<?php if($show_data and !is_null($rowCar['corrected_date'])){ echo date(phpdispfd,strtotime($rowCar['corrected_date']));} ?>" /></td>
        </tr>
    </table>
</div>

<div id="srServiceDiv">
    <table id="srServiceTable" class="srTable"  >
        <tr>
          <td colspan="4" class="rowLabelBig">Verification</td>
        </tr>
        <tr>
          <td colspan="4" class="rowData"><textarea id="verification" name="verification" <?php if($view){ echo 'readonly';} ?> maxlength="10000" ><?php if($show_data){ echo $rowCar['verification'];} ?></textarea></td>
        </tr>
		<tr>
			<td width="25%" class="rowLabel">Verification By</td>
			<td width="25%" class="rowData"><select class="chooser" name="verification_uid" id="verification_uid">
			<?php 
				foreach($users as $uid=>$data){
					echo "<option value='".$uid."'";
					if($uid == $rowCar['verification_uid']){echo " selected";}
					echo ">" . $data['name'] . "</option>\n";		
				}
			?>
			</select></td>
			<td width="25%" class="rowLabel">Verification Date</td>
			<td width="25%" class="rowData"><input type="text" name="verification_date" id="verification_date" class="date_uid" value="<?php if(!is_null($rowCar['verification_date']) or $rowCar['verification_date'] != ''){ echo date(phpdispfd,strtotime($rowCar['verification_date']));} ?>" /></td>
		</tr>
    </table>
</div>

<?php if($edit){ ?>
<table id="srDataTable" class="srTable">

</table>
<?php } ?>

<div id="srServiceDiv" class="delete_reason_div" <?php if(!$view or strtolower($rowCar['deleted']) == 'n'){ ?>style="display:none;" <?php } ?>>
    <table id="srServiceTable" class="srTable"  >
        <tr>
          <td class="rowLabelBig">Delete Reason</td>
        </tr>
        <tr>
          <td class="rowData"><textarea id="delete_reason" name="delete_reason" <?php if($view){ echo 'readonly';} ?> maxlength="10000" ><?php if($show_data){ echo $rowCar['deleted_reason'];} ?></textarea></td>
        </tr>
		<?php if(($edit or $view) and !is_null($rowCar['deleted_date'])){ ?>
			<tr>
	          <td class="rowDataNote">Deleted by: <?php echo $users[$rowCar['deleted_uid']]['name']."&emsp;".date(phpdispfd,strtotime($rowCar['deleted_date'])); ?></td>
			</tr>
		<?php } ?>
    </table>
</div>

<?php } ?>

<div id="srFilesDiv">
	<table id="srFilesTable" class="srTable">
		<tr>
			<th colspan="2" scope="col">Attached Documents</th>
		</tr>
		<tr>
			<td class="rowData" width="50%"><div class="srBottomBtn"><a href="/resources/kcfinder/kcfinder.php?unique_id=<?php echo $unique_id; ?>&table=iso_car_files<?php if($view){echo '&ro';} ?>" class="button_jquery_save iframeUpload" id="attachDocs"><?php if($view){ echo 'View'; }else{ echo 'Attach';} ?> Files</a></div></td>
			<td class="rowData" style="text-align:center; color:#f79548">Attached Files: <?php if($show_data){echo $rowCar['files_count'];}else{ echo '0';} ?></td>
		</tr>
	</table>
</div>

<div style="display:none">
	<?php if($debug){ ?><input name="debug" id="debug" type="hidden" value="Y" /><?php } ?>
	<?php if($edit){ ?><input type="text" id="edit" name="edit" value="Y" /><?php } ?>
    <input type="text" id="user_id" name="user_id" value="<?php echo $_SESSION['login']; ?>" /> 
	<input type="text" id="unique_id" name="unique_id" value="<?php echo $unique_id; ?>" />	
	<input type="text" id="fbc_unique_id" name="fbc_unique_id" value="<?php if($show_data){ echo $rowFbc['fbc_unique_id'];}else{if($fbc_get_id){echo $fbc_get_id;}} ?>" />	
	<input type="checkbox" id="delete" name="delete" value="Y" />
	<input type="checkbox" id="close" name="close" value="Y" /> 
</div>
</div>
<div id="srFooterDiv">
	<?php if($view or $edit){ ?>
		<div class="srBottomBtn"><a class="button_jquery_download" href="iso_car_download.php?unique_id=<?php echo $unique_id; ?>" >Download CAR</a></div>
	<?php } ?>
	<?php if($edit){ ?>
		<div class="srBottomBtn"><a class="button_jquery_edit" onClick="submitcheck('save')">Save CAR</a></div>
		<div class="srBottomBtn"><a class="button_jquery_verify" onClick="submitcheck('close')">Close CAR</a></div>
		<div class="srBottomBtn"><a class="button_jquery_verify" onClick="show_delete()">Delete CAR</a></div>
	<?php }elseif(!$view){ ?>
		<div class="srBottomBtn"><a class="button_jquery_edit" onClick="submitcheck()">Submit CAR</a></div>
	<?php } ?>

</div>

</form>
</div> 
</div>
<?php require_once($footer_include); ?>