<?php
//Update Completed 04/02/2015
$debug = false;
if(isset($_GET['debug'])){
	$debug = true;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}
$show_data = false;

$view = false;
if(isset($_GET['view'])){
	$view = true;
}

$car_get_id = false;
if(isset($_GET['id'])){
	$car_get_id = $_GET['id'];
}

$system_unique_id = false;
$request_unique_id = false;
$report_unique_id = false;
if(isset($_GET['system_unique_id'])){
	if(isset($_GET['request_unique_id'])){
		$log->logmsg("User opened FBC from request",0,false,basename(__FILE__),__LINE__);
		$request_unique_id = $_GET['request_unique_id']; //request unique id
	}
	
	if(isset($_GET['report_unique_id'])){
		$log->logmsg("User opened FBC from report",0,false,basename(__FILE__),__LINE__);
		$report_unique_id = $_GET['report_unique_id']; //report unique id
	}
		
	$system_unique_id = $_GET['system_unique_id']; //system unique id
	$sql="SELECT c.unique_id, c.customer_id, c.name, CONCAT(c.address,' ',c.city,', ',c.state,' ',c.zip) AS address, c.contact_name, c.contact_phone, c.contact_email, sbc.system_id
	FROM systems_base_cont AS sbc
	LEFT JOIN facilities AS f ON f.unique_id = sbc.facility_unique_id
	LEFT JOIN customers AS c ON c.unique_id = f.customer_unique_id
	WHERE sbc.unique_id = '$system_unique_id';";
	s($sql);
	if(!$resultCust = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	if($resultCust->num_rows > 0){
		$rowCust = $resultCust->fetch_assoc();
		$system_unique_id = $rowCust;
	}else{
		$system_unique_id = false;	
	}
	d($system_unique_id);	
}

$edit = false;
if(isset($_GET['edit'])){
	$view = false;
	if(!isset($_SESSION['perms']['perm_close_parcarfbc'])){
		//$log->logerr("You do not have permission to edit/close PAR/FBC/FBC",1057,false,basename(__FILE__),__LINE__);
		$view = true;
	}else{
		$edit = true;
	}
}

if($edit or $view){
	$show_data = true;
	$users['none'] = array('name'=>'');
	if(!isset($_GET['unique_id'])){
		$log->logerr("Invalid FBC Unique ID",1061,true,basename(__FILE__),__LINE__);
	}
	$sql="SELECT u.uid, u.name, u.timezone
	FROM users AS u
	INNER JOIN users_groups AS ug ON ug.uid = u.uid
	INNER JOIN users_group_id AS grid ON grid.id = ug.gid AND grid.`group` = 'grp_employee'
	WHERE u.id >= 10
	ORDER BY u.uid ASC;";
	s($sql);
	if(!$resultUsers = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	while($rowUsers = $resultUsers->fetch_assoc()){
		$users[$rowUsers['uid']]=array('name'=>$rowUsers['name'], 'timezone'=>$rowUsers['timezone']);
	}
	d($users);
	$sql="SELECT fbc.*, COUNT(iff.id) AS files_count, car.id AS car_id, sr.report_id, srq.request_num
	FROM iso_fbc AS fbc
	LEFT JOIN iso_fbc_files AS iff ON iff.unique_id = fbc.unique_id
	LEFT JOIN iso_car AS car ON car.unique_id = fbc.car_unique_id
	LEFT JOIN systems_reports as sr ON sr.unique_id = fbc.report_unique_id
	LEFT JOIN systems_requests as srq ON srq.unique_id = fbc.request_unique_id
	WHERE fbc.unique_id = '".$_GET['unique_id']."';";
	s($sql);
	if(!$resultFbc = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$rowFbc = $resultFbc->fetch_assoc();
	d($rowFbc);
	$unique_id = $rowFbc['unique_id'];	
	
	if(!$view){
		if(strtolower($rowFbc['closed']) == 'y'){
			$log->logerr('You cannot edit a closed FBC/PAR/FBC',1063,true,basename(__FILE__),__LINE__);
		}
	}
}else{
	$unique_id = md5(uniqid());
	
	if($car_get_id){
		$sql="SELECT unique_id FROM iso_car WHERE id = '$car_get_id';";
		s($sql);
		if(!$resultCarGet = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
		if($resultCarGet->num_rows == 0){
			$log->logerr('Invalid CAR ID',1064,true,basename(__FILE__),__LINE__);	
		}
		$rowCarGet = $resultCarGet->fetch_assoc();	
		$car_get_id = $rowCarGet['unique_id'];
		s($car_get_id);
	}
}


?>
<!DOCTYPE html>
<html>
<head>
<?php require_once($head_include);?>
<?php require_once($css_include);?>

<link href="/resources/css/form_report.css" rel="stylesheet" type="text/css" media="screen">

<?php require_once($js_include);?>

<script src="/resources/js/jquery.ui.datepicker.js"></script>
<script src="/resources/js/jquery-ui-timepicker-addon.js"></script>
<script src="/resources/js/jquery-dynamic-form_MODIFIED.js"></script>
<script type="text/javascript">
var users_json = <?php echo json_encode($users); ?>;

$(document).ready(function() {
	<?php if(strtolower($rowFbc['car']) == 'n'){ ?>
		$('.no_action').show();
		$('.yes_action').hide();
	<?php }else{ ?>
		$('.no_action').hide();
		$('.yes_action').show();
	<?php } ?>
	
	$(".iframeUpload").fancybox({
			'type'			: 'iframe',
			'height'		: 600,
			'fitToView'		: true,
			'maxWidth'		: 900,
			'maxHeight'		: 600,
			'autoSize'		: false,
			'closeBtn'		: true,
			'margin'		: [5,5,5,5]
	});

	$(".button_jquery_save").button({
		icons: {
			primary: "ui-icon-disk"
		}
	});
	
	$(".button_jquery_download").button({
		icons: {
			primary: "ui-icon-disk"
		}
	});
	
	$(".button_jquery_verify").button({
		icons: {
			primary: "ui-icon-check"
		}
	});
	
	$(".button_jquery_edit").button({
		icons: {
			primary: "ui-icon-pencil"
		}
	});
		

<?php if(!$view){ ?>
	$(".chooser").chosen({
		no_results_text: "Oops, nothing found!",
		disable_search_threshold: 10,
		placeholder_text_single: '  ',
		width: '100%'
	});

	$(function() {
		$( ".date_uid" ).datepicker({
			numberOfMonths: 1,
			showButtonPanel: true,
			changeYear: true,
			constrainInput: true,
			duration: "fast",
			dateFormat: "<?php echo dpdispfd; ?>"
		});	
	});
	
	$('.customer_info').change(function(e) {
		alert("Make sure you update the customer info in the manage customers section.");
	});
	
	$('#car').on('change', function(evt, params) {
		if(params['selected'] == 'N'){
			$('.no_action').show();	
			$('.yes_action').hide();			
		}else{
			$('.no_action').hide();	
			$('.yes_action').show();
		}
	});	
<?php } ?>
/////////////////////////////////////////////////////////////////////////////////////
});
/////////////////////////////////////////////////////////////////////////////////////

function show_delete(){
	if(document.getElementById('delete').checked == false){
		$('.delete_reason_div').show();
		document.getElementById('delete').checked = true;
		<?php if(!$_SESSION['mobile_device']){ ?>
			$.prompt("<h3>Fill out the reason for deleting this FBC</h3>",{
				title: "Delete FBC Reason"
			});
		<?php }else{ ?>
			alert("Close this FBC?");
		<?php } ?>
	}else{
		submitcheck('delete');		
	}
}

function submitcheck(data){
	
	ids = [];
	errors = [];
	var find = ["\""];
	var replace = ["'"];
	var datere = /^20(1|2)\d-((1[0-2])|(0[1-9]))-(([0-2][0-9])|3(0|1))$/; //date yyyy-mm-dd
	var dtre = /^\d{1,10}(\.\d{1,10})?$/; //downtime 00000.00000  .00000 optional
	//var edit_car = <?php if($edit){ echo 'true';}else{ echo 'false';} ?>;
	
	if($('#customer').val()==''){ids.push('#customer'); errors.push('Customer name blank');}
	if($('#contact').val()==''){ids.push('#contact'); errors.push('Contact blank');}
	if($('#address').val()==''){ids.push('#address'); errors.push('Address blank');}
	if($('#email').val()==''){ids.push('#email'); errors.push('Email blank');}
	if($('#phone').val()==''){ids.push('#phone'); errors.push('Phone blank');}
	if($('#customer_id').val()==''){ids.push('#title'); errors.push('Customer ID blank');}
	if($('#description').val()==''){ids.push('#description'); errors.push('Description blank');}
	if($('#description').val().split(' ').length <= 4){ids.push('#description'); errors.push('Description less than 5 words');}

	var textarea = $('#description').val();
	textarea = textarea.replaceArray(find, replace);
	$('#description').val(textarea);
	
	if(document.getElementById("delete").checked == true){
		if($('#deleted_reason').val()==''){ids.push('#delete_reason'); errors.push('Delete Reason blank');}
		if($('#deleted_reason').val().split(' ').length <= 2){ids.push('#delete_reason'); errors.push('Delete Reason less than 3 words');}
		var textarea = $('#deleted_reason').val();
		textarea = textarea.replaceArray(find, replace);
		$('#deleted_reason').val(textarea);		
	}else{
		//if(edit_car == true){
		if(data == 'close'){
			if($('#resolution').val()==''){ids.push('#resolution'); errors.push('Resolution blank');}
			if($('#resolution').val().split(' ').length <= 4){ids.push('#resolution'); errors.push('Resolution less than 5 words');}
			if($('#resolution_uid').val()=='' || $('#resolution_uid').val()=='none'){ids.push('#resolution_uid_chosen'); errors.push('Resolution User blank');}
			if(!datere.test($('#resolution_target_date').val())){ids.push("#resolution_target_date"); errors.push("Resolution Target Date Invalid");}
			
			if($('#verification').val()==''){ids.push('#verification'); errors.push('Verification blank');}
			if($('#verification').val().split(' ').length <= 4){ids.push('#verification'); errors.push('Verification less than 5 words');}
			if($('#verification_uid').val()=='' || $('#verification_uid').val()=='none'){ids.push('#verification_uid_chosen'); errors.push('Verification User blank');}
			if(!datere.test($('#verification_date').val())){ids.push("#verification_date"); errors.push("Verification Date Invalid");}
			
			if($('#car').val() == 'N'){
				if($('#car_unneeded_reason').val()==''){ids.push('#car_unneeded_reason'); errors.push('No CAR Reason blank');}
				if($('#car_unneeded_reason').val().split(' ').length <= 4){ids.push('#car_unneeded_reason'); errors.push('No CAR Reason less than 5 words');}
				if($('#car_unneeded_uid').val()=='' || $('#car_unneeded_uid').val()=='none'){ids.push('#car_unneeded_uid_chosen'); errors.push('No CAR User blank');}
				if(!datere.test($('#car_unneeded_date').val())){ids.push("#car_unneeded_date"); errors.push("No CAR Date Invalid");}				
			}else{
				if($('#car_id').val()==''){ids.push('#car_id'); errors.push('CAR ID blank');}
			}
			
			var textarea = $('#resolution').val();
			textarea = textarea.replaceArray(find, replace);
			$('#resolution').val(textarea);
			
			var textarea = $('#car_unneeded_reason').val();
			textarea = textarea.replaceArray(find, replace);
			$('#car_unneeded_reason').val(textarea);
			
			var textarea = $('#verification').val();
			textarea = textarea.replaceArray(find, replace);
			$('#verification').val(textarea);
			
			var textarea = $('#car_unneeded_reason').val();
			textarea = textarea.replaceArray(find, replace);
			$('#car_unneeded_reason').val(textarea);
			
		}			
	}
	
	console.log("ids: " + ids);
	console.log("errors: " + errors);
	showErrors(ids,errors);
	
	if(ids.length <= 0){
		if(data == 'close'){
			<?php if(!$_SESSION['mobile_device']){ ?>
				$.prompt("<h3>Close this FBC?</h3>",{
					title: "Close FBC",
					buttons: { Yes: 1, No: -1 },
					focus: 1,
					submit:function(e,v,m,f){ 
						e.preventDefault();
						showErrors(ids,errors);
						$("#errors").hide();
						if(v == 1){
							$.prompt.close();
							document.getElementById("close").checked = true;
							document.forms["form"].submit();
						}else{
							$.prompt.close();
						}
					}
				});
			<?php }else{ ?>
				if(confirm("Close this FBC?")){
					document.getElementById("close").checked = true;
					document.forms["form"].submit();
				}
			<?php } ?>
		}else if(data == 'delete'){
			<?php if(!$_SESSION['mobile_device']){ ?>
				$.prompt("<h3>Delete this FBC?</h3>",{
					title: "Delete FBC",
					buttons: { Yes: 1, No: -1 },
					focus: 1,
					submit:function(e,v,m,f){ 
						e.preventDefault();
						showErrors(ids,errors);
						$("#errors").hide();
						if(v == 1){
							$.prompt.close();
							document.forms["form"].submit();
						}else{
							$.prompt.close();
						}
					}
				});
			<?php }else{ ?>
				if(confirm("Delete this FBC?")){
					document.getElementById("delete").checked = true;
					document.forms["form"].submit();
				}
			<?php } ?>
		//}else if(edit_car == true){
		}else if(data == 'save'){
			<?php if(!$_SESSION['mobile_device']){ ?>
				$.prompt("<h3>Save changes to this FBC?</h3>",{
					title: "Save FBC",
					buttons: { Yes: 1, No: -1 },
					focus: 1,
					submit:function(e,v,m,f){ 
						e.preventDefault();
						showErrors(ids,errors);
						$("#errors").hide();
						if(v == 1){
							$.prompt.close();
							document.forms["form"].submit();
						}else{
							$.prompt.close();
						}
					}
				});
			<?php }else{ ?>
				if(confirm("Save changes to this FBC?")){
					document.forms["form"].submit();
				}
			<?php } ?>						
		}else{
			<?php if(!$_SESSION['mobile_device']){ ?>
				$.prompt("<h3>Submit this FBC?</h3>",{
					title: "Submit FBC",
					buttons: { Yes: 1, No: -1 },
					focus: 1,
					submit:function(e,v,m,f){ 
						e.preventDefault();
						showErrors(ids,errors);
						$("#errors").hide();
						if(v == 1){
							$.prompt.close();
							document.forms["form"].submit();
						}else{
							$.prompt.close();
						}
					}
				});
			<?php }else{ ?>
				if(confirm("Submit this FBC?")){
					document.forms["form"].submit();
				}
			<?php } ?>
	
			
		}
		
		
	}
}
/////////////////////////////////////////////////////////////////////////////////////
function showErrors(ids,errors){
	//thin solid #2C3594
	$("input, select, .chosen-container").each(function(index, element) {
		$(this).animate({
			borderColor: "#2C3594",
			boxShadow: 'none'
		});
	});
	
	$.each(ids,function(index,value){
		$(value).animate({
			borderColor: "#cc0000",
			boxShadow: '0 0 5px 3px rgba(255,0,0,0.4)'
		});
	});
	$("#errors > span").html("");
	$.each(errors,function(index, value){
		$("#errors > span").append(value + "<br>");
	});
	$("#errors").show('slow');
	$(document).scrollTop(0);
}

String.prototype.replaceArray = function(find, replace) {
	var replaceString = this;
	var regex; 
	for (var i = 0; i < find.length; i++) {
		regex = new RegExp(find[i], "g");
		replaceString = replaceString.replace(regex, replace[i]);
	}
	return replaceString;
};

/////////////////////////////////////////////////////////////////////////////////////
</script>

</head>
<body>
<?php require_once($header_include); ?>
<div id="OIReportContent"> 
<div id="stylized" class="myform">
<form id="form" name="form" method="post" enctype='multipart/form-data' action="iso_fbc_save.php">
<div id="srHeaderDiv">
	<?php if($view and strtolower($rowFbc['closed']) == 'n'){ ?>
	<div align="center" style="margin-bottom:15px;">
		<h1 style="color:red">THIS FBC IS NOT CLOSED!</h1>
	</div>
	<?php } ?>
	<h1>Feedback Complaint</h1>
	<?php if($edit or $view){ ?><h1>ID: <?php echo $rowFbc['id']; ?></h1><?php } ?>
	<br />
</div>
<div id="main"><!-- do not remove -->
<div id="errors" style="text-align:center;display:none; margin-bottom:25px; font-size:18px;">
	<h2 style="margin:0px; padding:0px; font-size:24px;">Errors to fix</h2>
	<span style="color:#F00">
	</span>
</div>

<?php if(!$view and !$edit){ ?>
<div id="srDataDiv">
    <table id="srDataTable" class="srTable">
        <tr>
          	<td style="text-align:center;"><a class="iframeView button_jquery_verify" href="iso_fbc_customer_lookup.php">Lookup Customer</a></td>
        </tr>
    </table>
</div>
<?php } ?>

<div id="srDataDiv">
    <table id="srDataTable" class="srTable">
        <tr>
			<td class="rowLabel">Customer:</td>
         	<td class="rowData"><input type="text" id="customer" name="customer" class="customer_info tooltip" <?php if($view){ echo 'readonly';} ?> value="<?php if($show_data){ echo $rowFbc['customer'];}elseif($system_unique_id){echo $system_unique_id['name'];} ?>" /></td>
          	<td class="rowLabel">Contact:</td>
          	<td class="rowData"><input type="text" id="contact" name="contact" class="customer_info tooltip" <?php if($view){ echo 'readonly';} ?> value="<?php if($show_data){ echo $rowFbc['contact'];}elseif($system_unique_id){echo $system_unique_id['contact_name'];} ?>" /></td>
        </tr>
		<tr>
          	<td class="rowLabel">Customer ID:</td>
          	<td class="rowData"><input type="text" id="customer_id" name="customer_id" class="customer_info tooltip" readonly value="<?php if($show_data){ echo $rowFbc['customer_id'];}elseif($system_unique_id){echo $system_unique_id['customer_id'];} ?>" /></td>
          	<td class="rowLabel">Phone:</td>
          	<td class="rowData"><input type="text" id="phone" name="phone" class="customer_info tooltip" <?php if($view){ echo 'readonly';} ?> value="<?php if($show_data){ echo $rowFbc['phone'];}elseif($system_unique_id){echo $system_unique_id['contact_phone'];} ?>" /></td>
        </tr>
		<tr>
			<td class="rowLabel">Address:</td>
         	<td class="rowData"><input type="text" id="address" name="address" class="customer_info tooltip" <?php if($view){ echo 'readonly';} ?> value="<?php if($show_data){ echo $rowFbc['address'];}elseif($system_unique_id){echo $system_unique_id['address'];} ?>" /></td>
			<td class="rowLabel">Email:</td>
         	<td class="rowData"><input type="text" id="email" name="email" class="customer_info tooltip" <?php if($view){ echo 'readonly';} ?> value="<?php if($show_data){ echo $rowFbc['email'];}elseif($system_unique_id){echo $system_unique_id['contact_email'];} ?>" /></td>
        </tr>
		<tr>
			<td class="rowLabel">System ID:</td>
         	<td class="rowData"><input type="text" id="system_id" name="system_id" class="customer_info tooltip" <?php if($view){ echo 'readonly';} ?> value="<?php if($show_data){ echo $rowFbc['system_id'];}elseif($system_unique_id){echo $system_unique_id['system_id'];} ?>" /></td>
			<td>&nbsp;</td>
          	<td>&nbsp;</td>
        </tr>
    </table>
	<table id="srDataTable" class="srTable">
        <tr>
			<td class="rowLabel">Linked Service Report ID:</td>
         	<td class="rowData"><input type="text" id="report_id" name="report_id" class="report_info tooltip" <?php if($view){ echo 'readonly';} ?> value="<?php if($show_data){ echo $rowFbc['report_id'];}?>" /></td>
          	<td class="rowLabel"><a class="iframeView button_jquery_verify" href="iso_fbc_report_lookup.php">Find Report</a></td>
          	<td class="rowData">&nbsp;</td>
        </tr>
	</table>
	<table id="srDataTable" class="srTable">
        <tr>
			<td class="rowLabel">Linked Service Request ID:</td>
         	<td class="rowData"><input type="text" id="request_id" name="request_id" class="request_info tooltip" <?php if($view){ echo 'readonly';} ?> value="<?php if($show_data){ echo $rowFbc['request_num'];}?>" /></td>
          	<td class="rowLabel"><a class="iframeView button_jquery_verify" href="iso_fbc_request_lookup.php">Find Request</a></td>
          	<td class="rowData">&nbsp;</td>
        </tr>
	</table>
</div>

<?php if(!$view){ ?>
<!--<div id="srDataDiv">
    <table id="srDataTable" class="srTable">
        <tr>
			<td class="rowLink"><a class="iframeView" href="none.php">Form Example 1</a></td>
          	<td class="rowLink"><a class="iframeView" href="none.php">Form Example 2</a></td>
        </tr>
    </table>
</div>-->
<?php } ?>

<?php if($edit or $view){ ?>
<div id="srServiceDiv">
    <table id="srServiceTable" class="srTable"  >
		
		<?php if((!is_null($rowFbc['edited_date']) or $rowFbc['edited_date'] != '') and !$view){ ?>
		<tr>
          <td class="rowDataNote">Last edited by: <?php echo $users[$rowFbc['edited_uid']]['name']."&emsp;".date(phpdispfd,strtotime($rowFbc['edited_date'])); ?></td>
        </tr>
		<?php } ?>
    </table>
</div>
<?php } ?>

<div id="srServiceDiv">
    <table id="srServiceTable" class="srTable"  >
        <tr>
          <td class="rowLabelBig">Description</td>
        </tr>
		<?php if(!$view){ ?>
		<tr>
          <td class="rowLabelNote">Description of the customer complaint</td>
        </tr>
		<?php } ?>
        <tr>
          <td class="rowData"><textarea id="description" name="description" <?php if($view){ echo 'readonly';} ?> maxlength="10000" ><?php if($show_data){ echo $rowFbc['description'];} ?></textarea></td>
        </tr>
		<?php if(!is_null($rowFbc['description_date']) or $rowFbc['description_date'] != ''){ ?>
		<tr>
          <td class="rowDataNote">Initiated by: <?php echo $users[$rowFbc['description_uid']]['name']."&emsp;".date(phpdispfd,strtotime($rowFbc['description_date'])); ?></td>
        </tr>
		<?php } ?>
    </table>
</div>

<?php if($edit or $view){ ?>

<div id="srServiceDiv">
    <table id="srServiceTable" class="srTable"  >
        <tr>
          <td colspan="4" class="rowLabelBig">Resolution / Action Taken</td>
        </tr>
        <tr>
          <td colspan="4" class="rowData"><textarea id="resolution" name="resolution" <?php if($view){ echo 'readonly';} ?> maxlength="10000" ><?php if($show_data){ echo $rowFbc['resolution'];} ?></textarea></td>
        </tr>
		<tr>
			<td width="25%" class="rowLabel">Resolved By</td>
			<td width="25%" class="rowData"><select class="chooser" name="resolution_uid" id="resolution_uid">
			<?php 
				foreach($users as $uid=>$data){
					echo "<option value='".$uid."'";
					if($uid == $rowFbc['resolution_uid']){echo " selected";}
					echo ">" . $data['name'] . "</option>\n";		
				}
			?>
			</select></td>
			<td width="25%" class="rowLabel">Resolution Target Date</td>
			<td width="25%" class="rowData"><input type="text" name="resolution_target_date" id="resolution_target_date" class="date_uid" value="<?php if(!is_null($rowFbc['resolution_target_date']) or $rowFbc['resolution_target_date'] != ''){ echo date(phpdispfd,strtotime($rowFbc['resolution_target_date']));} ?>" /></td>
		</tr>
    </table>
</div>

<div id="srServiceDiv">
    <table id="srServiceTable" class="srTable"  >
        <tr>
          <td colspan="4" class="rowLabelBig">Verification</td>
        </tr>
        <tr>
          <td colspan="4" class="rowData"><textarea id="verification" name="verification" <?php if($view){ echo 'readonly';} ?> maxlength="10000" ><?php if($show_data){ echo $rowFbc['verification'];} ?></textarea></td>
        </tr>
		<tr>
			<td width="25%" class="rowLabel">Verification By</td>
			<td width="25%" class="rowData"><select class="chooser" name="verification_uid" id="verification_uid">
			<?php 
				foreach($users as $uid=>$data){
					echo "<option value='".$uid."'";
					if($uid == $rowFbc['verification_uid']){echo " selected";}
					echo ">" . $data['name'] . "</option>\n";		
				}
			?>
			</select></td>
			<td width="25%" class="rowLabel">Verification Date</td>
			<td width="25%" class="rowData"><input type="text" name="verification_date" id="verification_date" class="date_uid" value="<?php if(!is_null($rowFbc['verification_date']) or $rowFbc['verification_date'] != ''){ echo date(phpdispfd,strtotime($rowFbc['verification_date']));} ?>" /></td>
		</tr>
    </table>
</div>

<div id="srServiceDiv">
	<table id="srServiceTable" class="srTable"  >
		<tr>
          <td colspan="4" class="rowLabelBig">Corrective Action Needed</td>
        </tr>
		<tr>
			<td width="25%">&nbsp;</td>
			<td class="rowLabel">CAR Needed?</td>
			<td class="rowData"><select class="chooser" name="car" id="car" >
				<option <?php if($show_data){ if(strtolower($rowFbc['car']) == 'n'){ echo 'selected'; } } ?> value="N">No</option>
				<option <?php if($show_data){ if(strtolower($rowFbc['car']) == 'y'){ echo 'selected'; } }?> value="Y">Yes</option>
				</select></td> 
			<td width="25%">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<td class="yes_action">&nbsp;</td>
			<td class="rowLabel yes_action">CAR ID:</td>
			<td class="rowData yes_action"><input type="text" name="car_id" id="car_id" readonly value="<?php if($show_data){ echo $rowFbc['car_id'];} ?>" /></td>
			<td class="yes_action" style="text-align:center;"><?php if(!$view){ ?><a class="iframeView button_jquery_verify" href="iso_fbc_car_lookup.php">Lookup CAR</a><?php } ?></td>
		</tr>
		<tr>
		  <td colspan="4" class="rowLabelBig no_action">Reason for no corrective action</td>
		</tr>
		<tr>
		  <td colspan="4" class="rowData no_action"><textarea id="car_unneeded_reason" name="car_unneeded_reason" <?php if($view){ echo 'readonly';} ?> maxlength="10000" ><?php if($show_data){ echo $rowFbc['car_unneeded_reason'];} ?></textarea></td>
		</tr>
		<tr>
			<td width="25%" class="rowLabel no_action">Approved By</td>
			<td width="25%" class="rowData no_action"><select class="chooser" name="car_unneeded_uid" id="car_unneeded_uid">
			<?php 
				foreach($users as $uid=>$data){
					echo "<option value='".$uid."'";
					if($uid == $rowFbc['car_unneeded_uid']){echo " selected";}
					echo ">" . $data['name'] . "</option>\n";		
				}
			?>
			</select></td>
			<td width="25%" class="rowLabel no_action">Approved Date</td>
			<td width="25%" class="rowData no_action"><input type="text" name="car_unneeded_date" id="car_unneeded_date" class="date_uid" value="<?php if(!is_null($rowFbc['car_unneeded_date']) or $rowFbc['car_unneeded_date'] != ''){ echo date(phpdispfd,strtotime($rowFbc['car_unneeded_date']));} ?>" /></td>
		</tr>
	</table>
</div>

<?php if($edit){ ?>
<table id="srDataTable" class="srTable">

</table>
<?php } ?>

<div id="srServiceDiv" class="delete_reason_div" <?php if(!$view or strtolower($rowFbc['deleted']) == 'n'){ ?>style="display:none;" <?php } ?>>
    <table id="srServiceTable" class="srTable"  >
        <tr>
          <td class="rowLabelBig">Delete Reason</td>
        </tr>
        <tr>
          <td class="rowData"><textarea id="deleted_reason" name="deleted_reason" <?php if($view){ echo 'readonly';} ?> maxlength="10000" ><?php if($show_data){ echo $rowFbc['deleted_reason'];} ?></textarea></td>
        </tr>
		<?php if(($edit or $view) and !is_null($rowFbc['deleted_date'])){ ?>
			<tr>
	          <td class="rowDataNote">Deleted by: <?php echo $users[$rowFbc['deleted_uid']]['name']."&emsp;".date(phpdispfd,strtotime($rowFbc['deleted_date'])); ?></td>
			</tr>
		<?php } ?>
    </table>
</div>

<?php } ?>

<div id="srFilesDiv">
	<table id="srFilesTable" class="srTable">
		<tr>
			<th colspan="2" scope="col">Attached Documents</th>
		</tr>
		<tr>
			<td class="rowData" width="50%"><div class="srBottomBtn"><a href="/resources/kcfinder/kcfinder.php?unique_id=<?php echo $unique_id; ?>&table=iso_fbc_files<?php if($view){echo '&ro';} ?>" class="button_jquery_save iframeUpload" id="attachDocs"><?php if($view){ echo 'View'; }else{ echo 'Attach';} ?> Files</a></div></td>
			<td class="rowData" style="text-align:center; color:#f79548">Attached Files: <?php if($show_data){echo $rowFbc['files_count'];}else{ echo '0';} ?></td>
		</tr>
	</table>
</div>

<div style="display:none">
	<?php if($debug){ ?><input name="debug" id="debug" type="hidden" value="Y" /><?php } ?>
	<?php if($edit){ ?><input type="text" id="edit" name="edit" value="Y" /><?php } ?>
    <input type="text" id="user_id" name="user_id" value="<?php echo $_SESSION['login']; ?>" /> 
	<input type="text" id="unique_id" name="unique_id" value="<?php echo $unique_id; ?>" />	
	<input type="text" id="customer_unique_id" name="customer_unique_id" value="<?php if($show_data){ echo $rowFbc['customer_unique_id'];}elseif($system_unique_id){echo $system_unique_id['unique_id'];} ?>" />	
	<input type="text" id="request_unique_id" name="request_unique_id" value="<?php if($show_data){ echo $rowFbc['request_unique_id'];}elseif($request_unique_id){echo $request_unique_id;} ?>" />	
	<input type="text" id="report_unique_id" name="report_unique_id" class="report_info" value="<?php if($show_data){ echo $rowFbc['report_unique_id'];}elseif($report_unique_id){echo $report_unique_id;} ?>" />	
	<input type="text" id="car_unique_id" name="car_unique_id" value="<?php if($car_get_id){echo $car_get_id;}else{ if($show_data){ echo $rowFbc['car_unique_id'];}} ?>" />	
	<input type="text" id="car_get_id" name="car_get_id" value="<?php if(!$edit){if($car_get_id){echo $_GET['id'];}} ?>" />
	<input type="checkbox" id="delete" name="delete" value="Y" />
	<input type="checkbox" id="close" name="close" value="Y" />
</div>
</div>
<div id="srFooterDiv">
	<?php if($view or $edit){ ?>
		<div class="srBottomBtn"><a class="button_jquery_download" href="iso_fbc_download.php?unique_id=<?php echo $unique_id; ?>">Download FBC</a></div>
	<?php } ?>
	<?php if($edit){ ?>
		<div class="srBottomBtn"><a class="button_jquery_edit" onClick="submitcheck('save')">Save FBC</a></div>
		<div class="srBottomBtn"><a class="button_jquery_verify" onClick="submitcheck('close')">Close FBC</a></div>
		<div class="srBottomBtn"><a class="button_jquery_verify" onClick="show_delete()">Delete FBC</a></div>
	<?php }elseif(!$view){ ?>
		<div class="srBottomBtn"><a class="button_jquery_edit" onClick="submitcheck()">Submit FBC</a></div>
	<?php } ?>

</div>

</form>
</div> 
</div>
<?php require_once($footer_include); ?>