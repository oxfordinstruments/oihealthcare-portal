<?php 
//Update Completed 11/25/14
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

$sql = "SELECT c.id, c.title, c.department, c.location, c.created_date, u.name AS created_name, c.unique_id, c.closed
FROM iso_car AS c
LEFT JOIN users AS u ON u.uid = c.created_uid
ORDER BY c.created_date ASC;";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$cars = array();
while($row = $result->fetch_assoc()){
	$cars[$row['id']] = array('unique_id'=>$row['unique_id'], 'title'=>$row['title'], 'department'=>$row['department'], 'location'=>$row['location'], 'created_date'=>$row['created_date'], 'created_name'=>$row['created_name']);
}

?>
<!doctype html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<style>
.credit_hold {
	color: #FF0000;
}
.dataTable th, .dataTable td {
	max-width: 200px;
	min-width: 70px;
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
}
</style>
<?php require_once($js_include);?>

<script type="text/javascript">
$(document).ready(function() {
	allTable = $('#allTable').dataTable({
		"bJQueryUI": true,
		"bStateSave": false,
		"sPaginationType": "full_numbers",
		"fnInitComplete": function () {
			 this.$('tr').click( function () {
				 var href = $(this).find("a").attr("href");
				 if(href) {
					window.location = href;
				 }
			 });
		},
		"fnDrawCallback" : function(oSettings) {
			var total_count = oSettings.fnRecordsTotal();
			var columns_in_row = $(this).children('thead').children('tr').children('th').length;
			var show_num = oSettings._iDisplayLength;
			var tr_count = $(this).children('tbody').children('tr').length;
			var missing = show_num - tr_count;
			if (show_num < total_count && missing > 0){
				for(var i = 0; i < missing; i++){
					$(this).append('<tr class="space"><td colspan="' + columns_in_row + '">&nbsp;</td></tr>'); 
				}
			}
			if (show_num > total_count) {
				for(var i = 0; i < (total_count - tr_count); i++) {
					$(this).append('<tr class="space"><td colspan="' + columns_in_row + '">&nbsp;</td></tr>'); 
				}
			}
		}
	});	
});
function select_id(id){
	var cars_json = <?php echo json_encode($cars); ?>;
	//console.log("ID: " + id);
	//console.log("Unique ID: " + customers_json[id]['unique_id']);
	//console.log("Name: " + customers_json[id]['name']);
	
	$('#car_id', top.document).val(id);
	$('#car_unique_id', top.document).val(cars_json[id]['unique_id']);
	parent.$.fancybox.close();
}
</script>
</head>
<body>
 <table width="100%" id="allTable">
                <thead>
                    <tr>
                        <th>ID</th>
						<th>Title</th>
						<th>Department</th>
						<th>Location</th>
						<th>Created Date</th>
						<th>Creating User</th> 
                    </tr>
                </thead>
                <tbody>
                    <?php
						foreach($cars as $id=>$data){
							echo "<tr onclick=\"javascript: select_id(".$id.");\">\n";	
							echo "<td>". $id."</td>\n";
							echo "<td>". $data['title']."</td>\n";
							echo "<td>". $data['department']."</td>\n";
							echo "<td>". $data['location']."</td>\n";
							echo "<td>". date(phpdispfd,strtotime($data['created_date']))."</td>\n";
							echo "<td>". $data['created_name']."</td>\n";
							echo "</tr>\n";
						}
						?>     
                </tbody>
            </table>
</body>
</html>