<?php 
//Update Completed 11/25/14
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

$sql = "SELECT c.unique_id, c.customer_id, c.name, CONCAT(c.address,' ',c.city,', ',c.state,' ',c.zip) AS address, c.contact_name, c.contact_phone, c.contact_email
FROM customers AS c
ORDER BY c.customer_id ASC;";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$customers = array();
while($row = $result->fetch_assoc()){
	$customers[$row['customer_id']] = array('unique_id'=>$row['unique_id'], 'customer_id'=>$row['customer_id'], 'name'=>$row['name'], 'address'=>$row['address'], 'contact_name'=>$row['contact_name'], 'contact_phone'=>$row['contact_phone'], 'contact_email'=>$row['contact_email']);
}

?>
<!doctype html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<style>
.credit_hold {
	color: #FF0000;
}
.dataTable th, .dataTable td {
	max-width: 200px;
	min-width: 70px;
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
}
</style>
<?php require_once($js_include);?>

<script type="text/javascript">
$(document).ready(function() {
	allTable = $('#allTable').dataTable({
		"bJQueryUI": true,
		"bStateSave": false,
		"sPaginationType": "full_numbers",
		"fnInitComplete": function () {
			 this.$('tr').click( function () {
				 var href = $(this).find("a").attr("href");
				 if(href) {
					window.location = href;
				 }
			 });
		},
		"fnDrawCallback" : function(oSettings) {
			var total_count = oSettings.fnRecordsTotal();
			var columns_in_row = $(this).children('thead').children('tr').children('th').length;
			var show_num = oSettings._iDisplayLength;
			var tr_count = $(this).children('tbody').children('tr').length;
			var missing = show_num - tr_count;
			if (show_num < total_count && missing > 0){
				for(var i = 0; i < missing; i++){
					$(this).append('<tr class="space"><td colspan="' + columns_in_row + '">&nbsp;</td></tr>'); 
				}
			}
			if (show_num > total_count) {
				for(var i = 0; i < (total_count - tr_count); i++) {
					$(this).append('<tr class="space"><td colspan="' + columns_in_row + '">&nbsp;</td></tr>'); 
				}
			}
		}
	});	
});
function select_id(id){
	var customers_json = <?php echo json_encode($customers); ?>;
	console.log("ID: " + id);
	console.log("Unique ID: " + customers_json[id]['unique_id']);
	console.log("Name: " + customers_json[id]['name']);
	
	$('#customer', top.document).val(customers_json[id]['name']);
	$('#contact', top.document).val(customers_json[id]['contact_name']);
	$('#address', top.document).val(customers_json[id]['address']);
	$('#phone', top.document).val(customers_json[id]['contact_phone']);
	$('#email', top.document).val(customers_json[id]['contact_email']);
	$('#customer_id', top.document).val(customers_json[id]['customer_id']);
	$('#customer_unique_id', top.document).val(customers_json[id]['unique_id']);
	parent.$.fancybox.close();
}
</script>
</head>
<body>
 <table width="100%" id="allTable">
                <thead>
                    <tr>
                        <th width="15%">Customer ID</th>
                        <th width="40%">Name</th>
                        <th width="45%">Address</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
						foreach($customers as $id=>$data){
							echo "<tr onclick=\"javascript: select_id(".$id.");\">\n";	
							echo "<td>". $id."</td>\n";
							echo "<td>". $data['name']."</td>\n";
							echo "<td>". $data['address']."</td>\n";
							echo "</tr>\n";
						}
						?>     
                </tbody>
            </table>
</body>
</html>