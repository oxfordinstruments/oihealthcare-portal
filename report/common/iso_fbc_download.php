<?php
// The . will be replaced with the id number
$output_file = 'F821-02 Customer Feedback and Complaint.docx';

$debug = false;
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

if(!isset($_GET['unique_id']) and !isset($_POST['unique_id'])){
	$log->logerr("Error Downloading CAR",1064,true,basename(__FILE__),__LINE__);
}else{
	if(isset($_GET['unique_id'])){
		$unique_id = $_GET['unique_id'];	
	}else{
		$unique_id = $_POST['unique_id'];
	}
}


// Check if template exists
mysqli_select_db($mysqli, "$db_name_dms") or die("cannot select DB");
$sql="SELECT d.name, dc.`version`, dc.dir, dc.fileType, dc.orgFileName
FROM tblDocuments AS d
LEFT JOIN tblDocumentContent AS dc ON dc.document = d.id
WHERE d.name LIKE('%Portal F821-02%')
AND dc.fileType LIKE('%docx%')
ORDER BY dc.`version` DESC
LIMIT 1;";
if(!$formRresult = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowForm = $formRresult->fetch_assoc();
$template_file = $settings->dms_files_dir . $rowForm['dir'] . $rowForm['version'] . $rowForm['fileType']; //added for use with dms
if (!file_exists($template_file)) {
	$log->logerr("Error the template is missing: " . $template_file,1045,true,basename(__FILE__),__LINE__);
}
s("Template File: ".$template_file);


mysqli_select_db($mysqli, "$db_name") or die("cannot select DB");

$sql="SELECT u.uid, u.name, u.timezone
	FROM users AS u
	INNER JOIN users_groups AS ug ON ug.uid = u.uid
	INNER JOIN users_group_id AS grid ON grid.id = ug.gid AND grid.`group` = 'grp_employee'
	WHERE u.id >= 10
	ORDER BY u.uid ASC;";
s($sql);
if(!$resultUsers = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$users = array();
while($rowUsers = $resultUsers->fetch_assoc()){
	$users[$rowUsers['uid']]=array('name'=>$rowUsers['name'], 'timezone'=>$rowUsers['timezone']);
}
d($users);

$sql="SELECT fbc.*, car.id AS car_id
FROM iso_fbc AS fbc
LEFT JOIN iso_car AS car ON car.unique_id = fbc.car_unique_id
WHERE fbc.unique_id = '$unique_id';";
s($sql);
if(!$resultFbc = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowFbc = $resultFbc->fetch_assoc();
d($rowFbc);

//Fill the word doc
require_once $_SERVER['DOCUMENT_ROOT'].'/resources/PhpWord/Autoloader.php';
\PhpOffice\PhpWord\Autoloader::register(false);

$templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($template_file);


$created_date = '';
if(!is_null($rowFbc['created_date'])){
	$created_date = date(phpdispfd,strtotime($rowFbc['created_date']));
}

$resolution_date = '';
if(!is_null($rowFbc['resolution_date'])){
	$resolution_date = date(phpdispfd,strtotime($rowFbc['resolution_date']));
}

$resolution_target_date = '';
if(!is_null($rowFbc['resolution_target_date'])){
	$resolution_target_date = date(phpdispfd,strtotime($rowFbc['resolution_target_date']));
}

$corrected_date = '';
if(!is_null($rowFbc['corrected_date'])){
	$corrected_date = date(phpdispfd,strtotime($rowFbc['corrected_date']));
}

$verification_date = '';
if(!is_null($rowFbc['verification_date'])){
	$verification_date = date(phpdispfd,strtotime($rowFbc['verification_date']));
}


$templateProcessor->setValue('customer', $rowFbc['customer']);
$templateProcessor->setValue('address', $rowFbc['address']);
$templateProcessor->setValue('email', $rowFbc['email']);
$templateProcessor->setValue('contact', $rowFbc['contact']);
$templateProcessor->setValue('phone', $rowFbc['phone']);
$templateProcessor->setValue('system_id', $rowFbc['system_id']);
$templateProcessor->setValue('fbc_id', $rowFbc['id']);
$templateProcessor->setValue('date', $created_date);

$templateProcessor->setValue('description', replace_cr($rowFbc['description']));
$templateProcessor->setValue('created_name', $users[$rowFbc['created_uid']]['name']);
$templateProcessor->setValue('created_date', $created_date);
$templateProcessor->setValue('created_sign', (string)$rowFbc['created_uid']);

$templateProcessor->setValue('resolution', replace_cr($rowFbc['resolution']));
$templateProcessor->setValue('target_date', $resolution_target_date);
$templateProcessor->setValue('resolution_name', $users[$rowFbc['resolution_uid']]['name']);
$templateProcessor->setValue('resolution_date', $resolution_date);
$templateProcessor->setValue('resolution_sign', (string)$rowFbc['resolution_uid']);

$templateProcessor->setValue('verification', replace_cr($rowFbc['verification']));
$templateProcessor->setValue('verification_name', $users[$rowFbc['verification_uid']]['name']);
$templateProcessor->setValue('verification_date', $verification_date);
$templateProcessor->setValue('verification_sign', (string)$rowFbc['verification_uid']);

if(strtolower($rowFbc['car']) == 'y'){
	$templateProcessor->setValue('y', 'XXX');
	$templateProcessor->setValue('n', '');
	$templateProcessor->setValue('car_id', $rowFbc['car_id']);
	$templateProcessor->setValue('no_car_name', '');
	$templateProcessor->setValue('no_car_reason', '');
}else{
	$templateProcessor->setValue('n', 'XXX');
	$templateProcessor->setValue('y', '');
	$templateProcessor->setValue('car_id', '');
	$templateProcessor->setValue('no_car_name', $users[$rowFbc['car_unneeded_uid']]['name']);
	$templateProcessor->setValue('no_car_reason', replace_cr($rowFbc['car_unneeded_reason']));
}

d($templateProcessor);

if($debug){die('DONE');}

$temp_file = '/tmp/FBC-'.md5(uniqid()).'.docx';
$templateProcessor->saveAs($temp_file);

$output_file = str_replace('.',' '.$rowFbc['id'].'.',$output_file);
$output_file = str_replace(' ','-',$output_file);

header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename='.$output_file);
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Pragma: public');
header('Content-Length: ' . filesize($temp_file));
flush();
readfile($temp_file);
unlink($temp_file); // deletes the temporary file

function replace_cr($text){
	$order   = array("\r\n", "\n", "\r");
	$replace = '<w:br/>';
	return str_replace($order, $replace, trim($text));
}

exit;
?>