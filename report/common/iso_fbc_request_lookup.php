<?php
//Update Completed 11/25/14
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();


$comp_word_limt 	 = intval($settings->complaint_word_limit);
$serv_word_limt		 = intval($settings->service_word_limit);
$custact_word_limt	 = intval($settings->custact_word_limit);
$probrpt_word_limt	 = intval($settings->probrpt_word_limit);

$sql="SELECT * FROM users;";
if(!$resultEngineers = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$arr_engineers = array();
while($rowEngineers = $resultEngineers->fetch_assoc())
{
	array_push_assoc($arr_engineers, $rowEngineers['uid'], $rowEngineers['name']);
}

function array_push_assoc(&$array, $key, $value){
	$array[$key] = $value;
	return $array;
}
function limit_words($string, $word_limit)
{
	$words = explode(" ",$string);
	if(count($words) > $word_limit){
		return implode(" ",array_splice($words,0,$word_limit))."...(more)";
	}else{
		return implode(" ",array_splice($words,0,$word_limit));
	}
}

?>
<!doctype html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<?php require_once($js_include);?>

<script type="text/javascript">
$(document).ready(function() {
	allTable = $('#allSitesTable').dataTable({
			"bJQueryUI": true,
			"bStateSave": true,
			"iCookieDuration": 60*60*24*365, // 1 year
			"sPaginationType": "full_numbers",
			"fnInitComplete": function () {
           		 this.$('tr').click( function () {
               		 var href = $(this).find("a").attr("href");
					 if(href) {
						window.location = href;
					 }
           		 });
       		}
		});
});

function select_id(id,unique_id){
	console.log("ID: " + id);
	console.log("Unique ID: " + unique_id);
	$('#request_id', top.document).val(id);
	$('#request_unique_id', top.document).val(unique_id);
	parent.$.fancybox.close();
}
</script>
</head>
<body>
    <?php  
		$sql="SELECT unique_id, request_date, request_num, system_nickname, problem_reported, engineer, `status` FROM systems_requests WHERE deleted = 'n' AND property = 'C'";
		if(!$resultAllRequests = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
	?>  
    <table width="100%" id="allSitesTable">
		<thead>
			<tr>
				<!--<th>id</th>-->
				<th>Date</th>
				<th width="50px">Request ID</th>
				<th>System Name</th>
				<th width="100px">Engineer</th>
				<th width="250px">Prob Reported</th>
				<th>status</th>
			</tr>
		</thead>
		<tbody>
			<?php
			//<a onclick="parent.closeFancyboxAndRedirectToUrl('http://www.domain.com/page/');">Close and go to page</a>
				while($rowAllRequest = $resultAllRequests->fetch_assoc())
				{
					echo "<tr onclick=\"javascript: select_id('".$rowAllRequest['request_num']."','".$rowAllRequest['unique_id']."');\">\n";
					echo "<td>". date(phpdispfd,strtotime($rowAllRequest['request_date']))."</td>\n";
					echo "<td>".$rowAllRequest['request_num']."</td>\n";
					echo "<td>".$rowAllRequest['system_nickname']."</td>\n";
					echo "<td>". $arr_engineers[$rowAllRequest['engineer']]."</td>\n";
					echo "<td>".limit_words($rowAllRequest['problem_reported'],$comp_word_limt)."</td>\n";
					echo "<td>". $rowAllRequest['status']."</td>\n";
					echo "</tr>\n";
				}
				?>     
		</tbody>
	</table>
</body>
</html>