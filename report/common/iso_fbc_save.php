<?php 
//Update Completed 3/11/2015

$debug = false;
if(isset($_POST['debug'])){
	$debug = true;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/php_utils.php');
$php_utils = new phpUtils();

d($_POST);

$delete = false;
if(isset($_POST['delete'])){
	$delete = true;	
}

$close = false;
if(isset($_POST['close'])){
	$close = true;
}

$edit = false;
if(isset($_POST['edit'])){
	$edit = true;
}

$car_needed = false;
if(isset($_POST['car'])){
	if(strtolower($_POST['car']) == 'y'){
		if(isset($_POST['car_unique_id'])){	
			$sql="SELECT COUNT(id) AS cnt FROM iso_car WHERE unique_id = '".$_POST['car_unique_id']."';";
			s($sql);
			if(!$result = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
				$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
			}
			$row = $result->fetch_assoc();
			$car_needed = true;
			if(intval($row['cnt']) <= 0){
				$car_needed = false;	
			}
		}else{
			$car_needed = true;	
		}
	}
}
s($car_needed);

$system_unique_id = '';
$sql="SELECT unique_id FROM systems_base WHERE system_id = '".$_POST['system_id']."';";
s($sql);
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
if($result->num_rows > 0){
	$row = $result->fetch_assoc();
	$system_unique_id = $row['unique_id'];	
}


$unique_id = $_POST['unique_id'];
$action_string = "";
$fbc_id = 'N/A';
$submit = false;

if($delete){
	$action_string = 'deleted';
	$car_needed = false;
	delete_fbc();
}else if($close){
	$action_string = 'closed';
	edit_fbc();
	close_fbc();
}else if($edit){
	$action_string = 'edited';
	edit_fbc();	
}else{
	$submit = true;
	$action_string = 'submitted';
	$car_needed = false;
	submit_fbc();	
}

function delete_fbc(){
	global $mysqli, $log, $_POST, $unique_id;
	$sql="UPDATE iso_fbc SET closed = 'Y', deleted = 'Y', deleted_uid = '".$_POST['user_id']."', deleted_date = '".date(storef,time())."', deleted_reason = \"".$_POST['deleted_reason']."\" WHERE unique_id = '$unique_id';";
	s($sql);
	$log->loginfo('FBC '.$unique_id.' deleted by '.$_POST['user_id'],1062,false,basename(__FILE__),__LINE__);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
}

function close_fbc(){
	global $mysqli, $log, $_POST, $unique_id;
	$sql="UPDATE iso_fbc SET closed = 'Y', closed_uid = '".$_POST['user_id']."', closed_date = '".date(storef,time())."' WHERE unique_id = '$unique_id';";
	s($sql);
	$log->loginfo('FBC '.$unique_id.' closed by '.$_POST['user_id'],1062,false,basename(__FILE__),__LINE__);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
}

function edit_fbc(){
	global $mysqli, $log, $_POST, $unique_id, $system_unique_id, $car_needed;
#####	
	if(isset($_POST['verification_date']) and $_POST['verification_date'] != ''){
		$verification_date = "\"".date(storef,strtotime($_POST['verification_date']))."\"";
	}else{
		$verification_date='NULL';
	}
	
	if(isset($_POST['verification_uid']) and $_POST['verification_uid'] != '' and $_POST['verification_uid'] != 'none'){
		$verification_uid = "\"".$_POST['verification_uid']."\"";
	}else{
		$verification_uid='NULL';
	}
#####
	if(isset($_POST['description_date']) and $_POST['description_date'] != ''){
		$description_date = "\"".date(storef,strtotime($_POST['description_date']))."\"";
	}else{
		$description_date='NULL';
	}
	
	if(isset($_POST['description_uid']) and $_POST['description_uid'] != '' and $_POST['description_uid'] != 'none'){
		$description_uid = "\"".$_POST['description_uid']."\"";
	}else{
		$description_uid='NULL';
	}
######
	if(isset($_POST['resolution_target_date']) and $_POST['resolution_target_date'] != ''){
		$resolution_target_date = "\"".date(storef,strtotime($_POST['resolution_target_date']))."\"";
	}else{
		$resolution_target_date='NULL';
	}
	
	if(isset($_POST['resolution_uid']) and $_POST['resolution_uid'] != '' and $_POST['resolution_uid'] != 'none'){
		$resolution_uid = "\"".$_POST['resolution_uid']."\"";
	}else{
		$resolution_uid='NULL';
	}
#####	
	if(isset($_POST['car_unneeded_date']) and $_POST['car_unneeded_date'] != ''){
		$car_unneeded_date = "\"".date(storef,strtotime($_POST['car_unneeded_date']))."\"";
	}else{
		$car_unneeded_date='NULL';
	}
	
	if(isset($_POST['car_unneeded_uid']) and $_POST['car_unneeded_uid'] != '' and $_POST['car_unneeded_uid'] != 'none'){
		$car_unneeded_uid = "\"".$_POST['car_unneeded_uid']."\"";
	}else{
		$car_unneeded_uid='NULL';
	}
#####	
	$car_needed_data = 'N';
	if($car_needed){
		$car_needed_data = 'Y';	
	}
	
	$sql="UPDATE iso_fbc SET
		customer=\"".$mysqli->real_escape_string($_POST['customer'])."\",
		contact=\"".$mysqli->real_escape_string($_POST['contact'])."\",
		address=\"".$mysqli->real_escape_string($_POST['address'])."\",
		email=\"".$mysqli->real_escape_string($_POST['email'])."\",
		phone=\"".$mysqli->real_escape_string($_POST['phone'])."\",
		customer_id=\"".$mysqli->real_escape_string($_POST['customer_id'])."\",
		customer_unique_id=\"".$_POST['customer_unique_id']."\",
		system_unique_id=\"".$system_unique_id."\",
		request_unique_id=\"".$_POST['request_unique_id']."\",
		report_unique_id=\"".$_POST['report_unique_id']."\",
		description=\"".$mysqli->real_escape_string($_POST['description'])."\",
		description_date=$description_date,
		description_uid=$description_uid,
		resolution=\"".$mysqli->real_escape_string($_POST['resolution'])."\",
		resolution_target_date=$resolution_target_date,
		resolution_uid=$resolution_uid,
		verification=\"".$mysqli->real_escape_string($_POST['verification'])."\",
		verification_uid=$verification_uid,
		verification_date=$verification_date,
		car=\"".$car_needed_data."\",
		car_unique_id=\"".$_POST['car_unique_id']."\",
		car_unneeded_uid=\"".$_POST['car_unneeded_uid']."\",
		car_unneeded_date=$car_unneeded_date,
		car_unneeded_reason=\"".$mysqli->real_escape_string($_POST['car_unneeded_reason'])."\",
		edited_uid='".$_POST['user_id']."',
		edited_date='".date(storef,time())."'
		WHERE unique_id = '$unique_id';
	";
	s($sql);
	$log->loginfo('FBC '.$unique_id.' edited by '.$_POST['user_id'],1062,false,basename(__FILE__),__LINE__);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
}

function submit_fbc(){
	global $mysqli, $log, $_POST, $unique_id, $fbc_id, $system_unique_id;
	$sql="INSERT INTO iso_fbc (customer, contact, address, email, phone, customer_id, customer_unique_id, system_unique_id, request_unique_id, report_unique_id, description, description_uid, description_date, created_uid, created_date, car, car_unique_id, unique_id) 
		VALUES (\"".$_POST['customer']."\",
		\"".$_POST['contact']."\",
		\"".$_POST['address']."\",
		\"".$_POST['email']."\",
		\"".$_POST['phone']."\",
		\"".$_POST['customer_id']."\",
		\"".$_POST['customer_unique_id']."\",
		\"".$system_unique_id."\",
		\"".$_POST['request_unique_id']."\",
		\"".$_POST['report_unique_id']."\",
		\"".$_POST['description']."\",
		\"".$_POST['user_id']."\",
		\"".date(storef,time())."\",
		\"".$_POST['user_id']."\",
		\"".date(storef,time())."\",
		";
	if(strlen($_POST['car_unique_id']) > 10){
		$sql.="'Y', '".$_POST['car_unique_id']."', ";	
	}else{
		$sql.="'N', NULL, ";	
	}
	$sql.="
		'$unique_id')
		ON DUPLICATE KEY UPDATE
		customer=VALUES(customer),
		contact=VALUES(contact),
		address=VALUES(address),
		email=VALUES(email),
		phone=VALUES(phone),
		customer_id=VALUES(customer_id),
		customer_unique_id=VALUES(customer_unique_id),
		system_unique_id=VALUES(system_unique_id),
		request_unique_id=VALUES(request_unique_id),
		report_unique_id=VALUES(report_unique_id),
		description=VALUES(description),
		description_uid=VALUES(description_uid),
		description_date=VALUES(description_date),
		created_uid=VALUES(created_uid),
		created_date=VALUES(created_date),
		car=VALUES(car),
		car_unique_id=VALUES(car_unique_id);";
	s($sql);
	$log->loginfo('FBC '.$unique_id.' edited by '.$_POST['user_id'],1062,false,basename(__FILE__),__LINE__);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	
	$sql="SELECT id FROM iso_fbc WHERE unique_id='$unique_id';";
	s($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$row = $result->fetch_assoc();
	d($row);
	$fbc_id = $row['id'];
}

if($submit){
	$php_utils->email_action_add($mysqli, "/report/common/iso_fbc_send.php", array('unique_id' => $unique_id), $_SESSION['login']);
}

?>
<!DOCTYPE html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<?php require_once($js_include);?>

<script type="text/javascript">
<?php //if($submit){ ?>
//$(document).ready(function() {	
//	$.ajax({
//		type: 'POST',
//		url: 'iso_fbc_send.php',
//		data: { unique_id: '<?php //echo $unique_id; ?>//'},
//		success:function(data){
//			// successful request; do something with the data
//			$("#emailSent").text("Emails Sent");
//		}
//	});
//});
<?php //} ?>

function delayer(){
    <?php if(!$debug){ ?>
	window.location = "<?php if($car_needed and !$edit and !$close and !$delete){echo 'iso_car.php?id='.$fbc_id;}else{ echo $refering_uri;} ?>"
	<?php } ?>
}
</script>
</head>
<body onLoad="setTimeout('delayer()', <?php if($car_needed and !$edit and !$close and !$delete){ echo '5000';}else{ echo '3000';}?> )">
<?php require_once($header_include); ?>
<div id="OIReportContent"> 
	<h1>FBC has been <?php echo $action_string; ?></h1>
	<br />
	<h2><span id="emailSent"></span></h2>
	<br />
	<?php if($car_needed and !$edit and !$close and !$delete){ ?>
		<h1><span class="red">You selected that a Corrective Action Request is needed.<br>You will be directed to open a new CAR in 5 seconds.<br>Please reference FBC Id: <?php echo $fbc_id; ?></span></h1>
	<?php }else{ ?>
		<h1><span class="red">Page will return to home page in 3 seconds</span></h1>
	<?php } ?>
</div>
<?php require_once($footer_include); ?>