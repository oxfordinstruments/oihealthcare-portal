<?php
// The . will be replaced with the id number
$output_file = 'F85-03 Prevenative Action Request.docx';

$debug = false;
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

if(!isset($_GET['unique_id']) and !isset($_POST['unique_id'])){
	$log->logerr("Error Downloading CAR",1064,true,basename(__FILE__),__LINE__);
}else{
	if(isset($_GET['unique_id'])){
		$unique_id = $_GET['unique_id'];	
	}else{
		$unique_id = $_POST['unique_id'];
	}
}


// Check if template exists
mysqli_select_db($mysqli, "$db_name_dms") or die("cannot select DB");
$sql="SELECT d.name, dc.`version`, dc.dir, dc.fileType, dc.orgFileName
FROM tblDocuments AS d
LEFT JOIN tblDocumentContent AS dc ON dc.document = d.id
WHERE d.name LIKE('%Portal F85-03%')
AND dc.fileType LIKE('%docx%')
ORDER BY dc.`version` DESC
LIMIT 1;";
if(!$formRresult = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowForm = $formRresult->fetch_assoc();
d($rowForm);
$template_file = $settings->dms_files_dir . $rowForm['dir'] . $rowForm['version'] . $rowForm['fileType']; //added for use with dms
if (!file_exists($template_file)) {
	$log->logerr("Error the template is missing: " . $template_file,1045,true,basename(__FILE__),__LINE__);
}
s("Template File: ".$template_file);


mysqli_select_db($mysqli, "$db_name") or die("cannot select DB");

$sql="SELECT u.uid, u.name, u.timezone
	FROM users AS u
	INNER JOIN users_groups AS ug ON ug.uid = u.uid
	INNER JOIN users_group_id AS grid ON grid.id = ug.gid AND grid.`group` = 'grp_employee'
	WHERE u.id >= 10
	ORDER BY u.uid ASC;";
s($sql);
if(!$resultUsers = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$users = array();
while($rowUsers = $resultUsers->fetch_assoc()){
	$users[$rowUsers['uid']]=array('name'=>$rowUsers['name'], 'timezone'=>$rowUsers['timezone']);
}
d($users);

$sql="SELECT * FROM iso_par WHERE unique_id = '$unique_id';";
s($sql);
if(!$resultPar = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowPar = $resultPar->fetch_assoc();

if($debug){die('DONE');}

//Fill the word doc
require_once $_SERVER['DOCUMENT_ROOT'].'/resources/PhpWord/Autoloader.php';
\PhpOffice\PhpWord\Autoloader::register();

$templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($template_file);


$created_date = '';
if(!is_null($rowPar['created_date'])){
	$created_date = date(phpdispfd,strtotime($rowPar['created_date']));
}

$root_cause_date = '';
if(!is_null($rowPar['root_cause_date'])){
	$root_cause_date = date(phpdispfd,strtotime($rowPar['root_cause_date']));
}

$correction_target_date = '';
if(!is_null($rowPar['correction_target_date'])){
	$correction_target_date = date(phpdispfd,strtotime($rowPar['correction_target_date']));
}

$corrected_date = '';
if(!is_null($rowPar['corrected_date'])){
	$corrected_date = date(phpdispfd,strtotime($rowPar['corrected_date']));
}

$verification_date = '';
if(!is_null($rowPar['verification_date'])){
	$verification_date = date(phpdispfd,strtotime($rowPar['verification_date']));
}




$templateProcessor->setValue('par_id', $rowPar['id']);
$templateProcessor->setValue('created_date', $created_date);
$templateProcessor->setValue('location', $rowPar['location']);
$templateProcessor->setValue('department', $rowPar['department']);
$templateProcessor->setValue('title', $rowPar['title']);

$templateProcessor->setValue('finding', replace_cr($rowPar['finding']));
$templateProcessor->setValue('finding_name', $users[$rowPar['created_uid']]['name']);
$templateProcessor->setValue('finding_date', $created_date);
$templateProcessor->setValue('finding_sign', $rowPar['created_uid']);

$templateProcessor->setValue('root_cause', replace_cr($rowPar['root_cause']));
$templateProcessor->setValue('root_cause_name', $users[$rowPar['root_cause_uid']]['name']);
$templateProcessor->setValue('root_cause_date', $root_cause_date);
$templateProcessor->setValue('root_cause_sign', $rowPar['root_cause_uid']);

$templateProcessor->setValue('correction', replace_cr($rowPar['correction']));
$templateProcessor->setValue('target_date', $correction_target_date);
$templateProcessor->setValue('correction_name', $users[$rowPar['correction_uid']]['name']);
$templateProcessor->setValue('correction_date', $corrected_date);
$templateProcessor->setValue('correction_sign', $rowPar['created_uid']);

$templateProcessor->setValue('verification', replace_cr($rowPar['verification']));
$templateProcessor->setValue('verification_name', $users[$rowPar['verification_uid']]['name']);
$templateProcessor->setValue('verification_date', $verification_date);
$templateProcessor->setValue('verification_sign', $rowPar['verification_uid']);


$temp_file = '/tmp/PAR-'.md5(uniqid()).'.docx';
$templateProcessor->saveAs($temp_file);

$output_file = str_replace('.',' '.$rowPar['id'].'.',$output_file);
$output_file = str_replace(' ','-',$output_file);

header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename='.$output_file);
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Pragma: public');
header('Content-Length: ' . filesize($temp_file));
flush();
readfile($temp_file);
unlink($temp_file); // deletes the temporary file

function replace_cr($text){
	$order   = array("\r\n", "\n", "\r");
	$replace = '<w:br/>';
	return str_replace($order, $replace, trim($text));
}

exit;
?>