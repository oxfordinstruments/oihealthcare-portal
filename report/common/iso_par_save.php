<?php 
//Update Completed 3/11/205

$debug = false;
if(isset($_POST['debug'])){
	$debug = true;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/php_utils.php');
$php_utils = new phpUtils();

d($_POST);

$delete = false;
if(isset($_POST['delete'])){
	$delete = true;	
}

$close = false;
if(isset($_POST['close'])){
	$close = true;
}

$edit = false;
if(isset($_POST['edit'])){
	$edit = true;
}

$unique_id = $_POST['unique_id'];
$action_string = "";
$par_id = 'N/A';
$submit = false;

if($delete){
	$action_string = 'deleted';
	delete_par();
}else if($close){
	$action_string = 'closed';
	edit_par();
	close_par();
}else if($edit){
	$action_string = 'edited';
	edit_par();	
}else{
	$submit = true;
	$action_string = 'submitted';
	submit_par();	
}

function delete_par(){
	global $mysqli, $log, $_POST, $unique_id;
	$sql="UPDATE iso_par SET deleted = 'Y', deleted_uid = '".$_POST['user_id']."', deleted_date = '".date(storef,time())."', deleted_reason = \"".$_POST['delete_reason']."\" WHERE unique_id = '$unique_id';";
	s($sql);
	$log->loginfo('PAR '.$unique_id.' deleted by '.$_POST['user_id'],1062,false,basename(__FILE__),__LINE__);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
}

function close_par(){
	global $mysqli, $log, $_POST, $unique_id;
	$sql="UPDATE iso_par SET closed = 'Y', closed_uid = '".$_POST['user_id']."', closed_date = '".date(storef,time())."' WHERE unique_id = '$unique_id';";
	s($sql);
	$log->loginfo('PAR '.$unique_id.' closed by '.$_POST['user_id'],1062,false,basename(__FILE__),__LINE__);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
}

function edit_par(){
	global $mysqli, $log, $_POST, $unique_id;
#####	
	if(isset($_POST['verification_date']) and $_POST['verification_date'] != ''){
		$verification_date = "\"".date(storef,strtotime($_POST['verification_date']))."\"";
	}else{
		$verification_date='NULL';
	}
	
	if(isset($_POST['verification_uid']) and $_POST['verification_uid'] != '' and $_POST['verification_uid'] != 'none'){
		$verification_uid = "\"".$_POST['verification_uid']."\"";
	}else{
		$verification_uid='NULL';
	}
#####
	if(isset($_POST['root_cause_date']) and $_POST['root_cause_date'] != ''){
		$root_cause_date = "\"".date(storef,strtotime($_POST['root_cause_date']))."\"";
	}else{
		$root_cause_date='NULL';
	}
	
	if(isset($_POST['root_cause_uid']) and $_POST['root_cause_uid'] != '' and $_POST['root_cause_uid'] != 'none'){
		$root_cause_uid = "\"".$_POST['root_cause_uid']."\"";
	}else{
		$root_cause_uid='NULL';
	}
######
	if(isset($_POST['correction_target_date']) and $_POST['correction_target_date'] != ''){
		$correction_target_date = "\"".date(storef,strtotime($_POST['correction_target_date']))."\"";
	}else{
		$correction_target_date='NULL';
	}
	
	if(isset($_POST['corrected_date']) and $_POST['corrected_date'] != ''){
		$corrected_date = "\"".date(storef,strtotime($_POST['corrected_date']))."\"";
	}else{
		$corrected_date='NULL';
	}
	
	if(isset($_POST['correction_uid']) and $_POST['correction_uid'] != '' and $_POST['correction_uid'] != 'none'){
		$correction_uid = "\"".$_POST['correction_uid']."\"";
	}else{
		$correction_uid='NULL';
	}
#####
		
	$sql="UPDATE iso_par SET
		location=\"".$mysqli->real_escape_string($_POST['location'])."\",
		department=\"".$mysqli->real_escape_string($_POST['department'])."\",
		title=\"".$mysqli->real_escape_string($_POST['title'])."\",
		finding=\"".$mysqli->real_escape_string($_POST['finding'])."\",
		correction=\"".$mysqli->real_escape_string($_POST['correction'])."\",
		correction_uid=$correction_uid,
		correction_target_date=$correction_target_date,
		corrected_date=$corrected_date,
		root_cause=\"".$mysqli->real_escape_string($_POST['root_cause'])."\",
		root_cause_uid=$root_cause_uid,
		root_cause_date=$root_cause_date,
		verification=\"".$mysqli->real_escape_string($_POST['verification'])."\",
		verification_uid=$verification_uid,
		verification_date=$verification_date,
		edited_uid='".$_POST['user_id']."',
		edited_date='".date(storef,time())."'
		WHERE unique_id = '$unique_id';
	";
	s($sql);
	$log->loginfo('PAR '.$unique_id.' edited by '.$_POST['user_id'],1062,false,basename(__FILE__),__LINE__);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
}

function submit_par(){
	global $mysqli, $log, $_POST, $unique_id, $par_id;
	$sql="INSERT INTO iso_par (location, department, title, finding, created_uid, created_date, unique_id) 
		VALUES (\"".$_POST['location']."\",
		\"".$_POST['department']."\",
		\"".$_POST['title']."\",
		\"".$_POST['finding']."\",
		\"".$_POST['user_id']."\",
		\"".date(storef,time())."\",
		'$unique_id')
		ON DUPLICATE KEY UPDATE
		location=VALUES(location),
		department=VALUES(department),
		title=VALUES(title),
		finding=VALUES(finding),
		created_uid=VALUES(created_uid),
		created_date=VALUES(created_date);";
	s($sql);
	$log->loginfo('PAR '.$unique_id.' edited by '.$_POST['user_id'],1062,false,basename(__FILE__),__LINE__);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	
	$sql="SELECT id FROM iso_par WHERE unique_id='$unique_id';";
	s($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$row = $result->fetch_assoc();
	d($row);
	$par_id = $row['id'];
}

if($submit){
	$php_utils->email_action_add($mysqli, "/report/common/iso_par_send.php", array('unique_id' => $unique_id), $_SESSION['login']);
}

?>
<!DOCTYPE html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<?php require_once($js_include);?>

<script type="text/javascript">
<?php //if($submit){ ?>
//$(document).ready(function() {	
//	$.ajax({
//		type: 'POST',
//		url: 'iso_par_send.php',
//		data: { unique_id: '<?php //echo $unique_id; ?>//'},
//		success:function(data){
//			// successful request; do something with the data
//			$("#emailSent").text("Emails Sent");
//		}
//	});
//});
<?php //} ?>

function delayer(){
    <?php if(!$debug){ ?>
	window.location = "<?php echo $refering_uri; ?>"
	<?php } ?>
}
</script>
</head>
<body onLoad="setTimeout('delayer()',3000 )">
<?php require_once($header_include); ?>
<div id="OIReportContent"> 
	<h1>PAR has been <?php echo $action_string; ?></h1>
	<br />
	<h2><span id="emailSent"></span></h2>
	<h1><span class="red">Page will return to home page in 3 seconds</span></h1>
</div>
<?php require_once($footer_include); ?>