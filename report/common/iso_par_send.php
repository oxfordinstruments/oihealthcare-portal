<?php
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
ini_set('display_errors', 'On');
ignore_user_abort(true);
set_time_limit(30);

$debug = false;

if(substr(php_sapi_name(), 0, 3) == 'cli' || empty($_SERVER['REMOTE_ADDR'])) {
	//shell
	$cron = false;
	if(isset($_SERVER['argv'][1]) and $_SERVER['argv'][1] == '--cron'){
		$cron = true;
	}
	$docroot = preg_replace("/(public_html).*/",'public_html',dirname(__FILE__),2);
	$no_define_eol = true;
	define('EOL',PHP_EOL);
} else {
	//webserver
	$cron = false;
	$docroot = $_SERVER['DOCUMENT_ROOT'];
	define('EOL','<br />');
}

$settings = new SimpleXMLElement($docroot.'/settings.xml', null, true);
require_once($docroot.'/define_inc.php');
require_once($docroot.'/mysqlInfo.php');
require_once($docroot.'/log/log.php');
$log = new logger($docroot);
require_once($docroot.'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}
require_once($docroot.'/report/common/scripts/php_utils.php');
$php_utils = new phpUtils($docroot, $debug);

require_once($docroot.'/resources/smarty/libs/Smarty.class.php');
$smarty = new Smarty();
$smarty->setTemplateDir($docroot.$settings->email_templates);
$smarty->setCompileDir($docroot.$settings->email_template_c);
$smarty->setCacheDir($docroot.$settings->email_cache);
$smarty->setConfigDir($docroot.$settings->email_configs);
$smarty->use_sub_dirs = true;
$smarty->force_compile = true;
//$smarty->debugging = true;
$smarty->caching = false;


if(!isset($_POST['unique_id'])){
	$log->logerr('Blank ID',1060,true,basename(__FILE__),__LINE__);
}
$unique_id = $_POST['unique_id'];

$send = true;
if(intval($settings->disable_email) == 1){
	$send = false;	
}



$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno) {die('There was an error running the query [' . $mysqli->connect_error . ']');}

//Get the request info
$sql="SELECT p.*, u.name
FROM iso_par AS p
LEFT JOIN users AS u ON u.uid = p.created_uid
WHERE p.unique_id = '$unique_id';";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	exit("SQL ERROR");
}
$row = $result->fetch_assoc();


$smarty->assign('id',$row['id']);
$smarty->assign('location',$row['location']);
$smarty->assign('department',$row['department']);
$smarty->assign('created_by',$row['name']);
$smarty->assign('title',$row['title']);
$smarty->assign('finding',$row['finding']);
$smarty->assign('company_name',$settings->company_name);
$smarty->assign('copyright_date',$settings->copyright_date);
$smarty->assign('email_pics',$settings->email_pics);
$smarty->assign('oih_url',$settings->full_url);



//email addys to send request notification to
$emails = array();
$sql="SELECT u.name, u.email
FROM users AS u
LEFT JOIN users_roles AS ur ON ur.uid = u.uid
LEFT JOIN users_prefs AS pref ON pref.uid = u.uid
LEFT JOIN users_pref_id AS prefid ON prefid.id = pref.pid
WHERE prefid.pref = 'pref_rcv_parcarfbc_submit' AND u.active = 'y' AND ur.rid = 6;";
if(!$resultEmail = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	exit("SQL ERROR");
}
while($rowEmail = $resultEmail->fetch_assoc()){
	array_push($emails,array("email"=>$rowEmail['email'],"name"=>$rowEmail['name']));
}


if($debug){
	$emails = array(array('email'=>(string)$settings->email_support, 'name'=>'Support'));	
}


//load phpmailer
require_once($docroot.'/resources/PHPMailer/PHPMailerAutoload.php');

//Engineer Request Email
$mail = new PHPMailer;
$mail->IsSMTP();
$mail->Host = (string)$settings->email_host;
$mail->SMTPAuth = true;
$mail->Username = (string)$settings->email_quality;
$mail->Password = (string)$settings->email_password;
$mail->SMTPSecure = 'tls';
$mail->From = (string)$settings->email_quality;
$mail->FromName = 'Quality Control '.$settings->short_name.' Portal';
foreach($emails as $email){
	$mail->AddAddress(trim($email['email']),$email['name']);  // Add a recipient
}
$mail->AddReplyTo((string)$settings->email_support, 'Support '.$settings->short_name.' Portal');
$mail->AddCC((string)$settings->email_support);
$mail->IsHTML(true);
$mail->Subject = 'New Prevenative Action Submitted';
$mail->Body = $smarty->fetch('iso_par.tpl');

d($mail);

if($debug){
	echo "Subject: ",$mail->Subject,EOL;
	echo $mail->Body,EOL,"<hr>",EOL;	
}

if($send){
	if(!$mail->Send()) {
	   echo 'Email could not be sent.';
	   $email_result = "Email could not be sent!";
	   echo 'Mailer Error: ' . $mail->ErrorInfo;
	   $log->logerr($mail->ErrorInfo,1053,false,basename(__FILE__),__LINE__);
	   exit;
	}else{
		$log->loginfo( $row['id'].' '. implode('; ',array_keys($mail->getAllRecipientAddresses())),200,false,basename(__FILE__),__LINE__);
	}
}else{
	echo "Send False: Not sending email.<br />";
}
//echo "<hr>";
//echo "<br /><pre>Email Result\n";
//echo $email_result;
//echo "<br /></pre>";



function limit_words($string, $word_limit){
	$words = explode(" ",$string);
	if(count($words) > $word_limit){
		return implode(" ",array_splice($words,0,$word_limit))."(more)";
	}else{
		return implode(" ",array_splice($words,0,$word_limit));
	}
}

exit("OK");
?>