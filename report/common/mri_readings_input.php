<?php
//Update Completed 11/25/14
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

if(isset($_GET['single'])){$single_system = true;}else{$single_system = false;}
if($single_system){
	if(isset($_GET['unique_id'])){
		$unique_id=$_GET['unique_id'];
	}else{
		$log->logerr('mriReadingInput.php',1048,true,basename(__FILE__),__LINE__);
	}	
}

if($single_system){
	$sql="SELECT m.id, sbc.system_id, sbc.nickname, sbc.unique_id, m.he, m.vp, m.hours, m.contact, m.date, m.notes, m.uid, sbc.mr_lhe_contact, sbc.mr_lhe_phone, sbc.mr_lhe_email
FROM systems_base_cont AS sbc
LEFT JOIN systems_mri_readings AS m ON sbc.unique_id = m.system_unique_id 
AND DATE_FORMAT(m.`date`,'%U/%Y') = DATE_FORMAT('".date(storef,time())."','%U/%Y')
WHERE sbc.unique_id = '$unique_id'
ORDER BY sbc.system_id ASC;";
}else{
	$sql="SELECT m.id, sbc.system_id, sbc.nickname, sbc.unique_id, m.he, m.vp, m.hours, m.contact, m.date, m.notes, m.uid, sbc.mr_lhe_contact, sbc.mr_lhe_phone, sbc.mr_lhe_email
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
LEFT JOIN systems_mri_readings AS m ON sbc.unique_id = m.system_unique_id 
AND DATE_FORMAT(m.`date`,'%U/%Y') = DATE_FORMAT('".date(storef,time())."','%U/%Y')
WHERE sbc.mr_lhe_list = 'y' AND sbc.property = 'C'
ORDER BY sbc.system_id ASC;";	
}
if(!$resultSystemRead = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$systems_read = array();
while ($rowSiteRead = $resultSystemRead->fetch_assoc()){
	array_push($systems_read,$rowSiteRead);
}
?>
<!DOCTYPE html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>

<link href="/resources/css/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" media="screen" />

<?php require_once($js_include);?>

<script src="/resources/js/jquery-ui/jquery-ui.min.js"></script>
<script src="/resources/js/jquery.ui.widget.js"></script>
<script src="/resources/js/jquery.ui.datepicker.js"></script>
<script src="/resources/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript">
$(document).ready(function() {
 	
/////////////////////////////////////////////////////////////////////////////////////		
		<?php
		if(strtolower($_SESSION['prefs']['pref_show_help']) != 'y' or $_SESSION['mobile_device'] == true){
		?>
$('#tiplayer').hide()
		<?php
			}
		?>	
		
		$(".button_jquery_save").button({
			icons: {
				primary: "ui-icon-disk"
			}
		});
});
</script>
<script type="text/javascript">
$(document).ready(function() {
	$(".required").each(function(index, element) {
		if(this.value == ''){
			$(this).css("background","#FFFFCC");
		}else{
			$(this).css("background","#CCFFCC");
		}
	});
	//$(".required").css("background","#FFFFCC");
	$(".filled").css("background","#CCFFCC");
	$(".required").change(function(e) {
		if($(this).val() == ""){
			$(this).css("background","#FFFFCC");
		}else{
			$(this).css("background","#FFFFFF");
		}
	});
	
	$( ".picker" ).datetimepicker({
		numberOfMonths: 1,
		showButtonPanel: true,
		changeYear: false,
		duration: "fast",
		controlType: 'select',
		constrainInput: true,
		dateFormat: "<?php echo dpdispfd ?>",
		timeFormat: "<?php echo dpdispft ?>"
	});
	
	$("#hide_complete").click(function(e) {
		$(".inputDiv").each(function(index, element) {
			if(document.getElementById("he_reading"+index).value != "" && document.getElementById("vp_reading"+index).value != "" && document.getElementById("comp_hours"+index).value != "" && document.getElementById("reading_date"+index).value != "" ){
				$(this).toggle('fast');
			}
		});
	});
/////////////////////////////////////////////////////////////////////////////////////
});
/////////////////////////////////////////////////////////////////////////////////////
function submitcheck(id){
	$blank = "";
	$cfrm = "Input";
	
	if(document.getElementById("idx"+id).value != "") $cfrm = "Update";
	
	if(document.getElementById("he_reading"+id).value == "") $blank = "Helium Reading is required\n" + $blank;
	if(document.getElementById("vp_reading"+id).value == "") $blank = "Vessel Pressure is required\n" + $blank;
	if(document.getElementById("comp_hours"+id).value == "") $blank = "Compressor Hours are required\n" + $blank; 
	//if(document.getElementById("site_contact"+id).value == "") $blank = "System Contact is required\n" + $blank; 
	if(document.getElementById("reading_date"+id).value == "") $blank = "The date and time of the readings is required\n" + $blank; 
	
	if(document.getElementById("he_reading"+id).value == "" &&
		document.getElementById("vp_reading"+id).value == "" &&
		document.getElementById("comp_hours"+id).value == ""){
		if(confirm("Would you like to delete the reading for "+ $("#system_id"+id).val() +" ?")){
			$blank = "";
			$cfrm = "Delete";
		}
	}
	
	if($blank != ""){alert($blank);}
	if($blank == ""){
		if(confirm($cfrm + " Reading?")){
			//document.forms["form"+id].submit();
			$.ajax({
				type: 'POST',
				url: 'mri_readings_input_do.php',
				data: { system_id: $("#system_id"+id).val(),
						id: $("#unique_id"+id).val(),
						he: $("#he_reading"+id).val(),
						vp: $("#vp_reading"+id).val(),
						ch: $("#comp_hours"+id).val(),
						date: $("#reading_date"+id).val(),
						contact: $("#site_contact"+id).val(),
						notes: $("#reading_notes"+id).val(),
						idx: $("#idx"+id).val(),
						uid: '<?php echo $_SESSION['login']; ?>'},						
				success:function(data){
					// successful request; do something with the data
					//alert(data);
					switch($.trim(data)){
						case '1':
							//$("#divForm"+id).hide('fast');
							$("#he_reading"+id).css("background","#CCFFCC");
							$("#vp_reading"+id).css("background","#CCFFCC");
							$("#comp_hours"+id).css("background","#CCFFCC");
							$("#reading_date"+id).css("background","#CCFFCC");
							$("#site_contact"+id).css("background","#CCFFCC");
							$("#reading_notes"+id).css("background","#CCFFCC");
							console.log("GOOD");
							break;
						case '2':
							alert("Reading for "+$("#system_id"+id).val()+" has been updated.");
							break;
						case '3':
							alert("Reading for "+$("#system_id"+id).val()+" has been DELETED.");
							location.reload();
							break;	
						default:
							alert("There was a problem saving the data. Please contact support.");
						
					}
				}
			});
		}
	}
}
/////////////////////////////////////////////////////////////////////////////////////
</script>
</head>
<body>
<?php require_once($header_include); ?>
<div id="OIReportContent">
    <div id="styledForm">
		<h1>MRI Readings Form</h1>
		<br />
		<h2>Week <?php echo date('W/Y',time()); ?> readings</h2>
		<p><span style="color:#093">GREEN</span> = already input today. <span style="color:#FFFFCC">YELLOW</span> = required field.</p>
		<h2>Input date and time in YOUR timezone</h2>
		<div id="hideCompleteDiv" style="width:100%; text-align:center;">
			<h2><a id="hide_complete" >&lt;- Show/Hide Completed -&gt;</a></h2>
		</div>
		<div class="line"></div>
<?php
$dtnow = date(phpdispfdt,time());
$rnum = 0;
foreach($systems_read as $system_read){
	if($system_read['date'] != ''){
		$dtnow = date(phpdispfdt, strtotime($system_read['date']));
		$filled = 'filled';
		$button = 'Update Reading';
	}else{
		$dtnow = '';
		$filled = '';
		$button = 'Input Reading';
	}
$formReading=<<<_FORM
	<div id="divForm$rnum" class="inputDiv">
		<form id="form$rnum" name="form$rnum" method="post" autocomplete="off">
			<input type="hidden" name="arr_id$rnum" id="arr_id$rnum" value="$rnum" />
			<input type="hidden" name="unique_id$rnum" id="unique_id$rnum" value="{$system_read['unique_id']}" />
			<input type="hidden" name="system_id$rnum" id="system_id$rnum" value="{$system_read['system_id']}" />
			<input type="hidden" name="idx$rnum" id="idx$rnum" value="{$system_read['id']}" />
			<table width="100%" cellpadding="5" cellspacing="5">
				<tr>
					<td colspan="5"><h3 style="font-size:16px">{$system_read['system_id']}&emsp;{$system_read['nickname']}</h3></td>
				</tr>
				<tr>
					<td colspan="5"><h3 style="font-size:16px">{$system_read['mr_lhe_contact']}&emsp;{$system_read['mr_lhe_phone']}&emsp;{$system_read['mr_lhe_email']}</h3></td>
				</tr>
				<tr>
					<td width="20%"><label>Helium Level</label>
					<input name="he_reading$rnum" class="required" type="number" id="he_reading$rnum" value="{$system_read['he']}"  /></td>
					<td width="20%"><label>Vessel Pressure</label>
					<input name="vp_reading$rnum" class="required" type="number" id="vp_reading$rnum" value="{$system_read['vp']}"  /></td>
					<td width="20%"><label>Compressor Hours</label>
					<input name="comp_hours$rnum" class="required" type="number" id="comp_hours$rnum" value="{$system_read['hours']}"  /></td>
					<td width="20%"><label>Reading Date</label>
					<input name="reading_date$rnum" class="required picker" type="text" id="reading_date$rnum" value="$dtnow"  /></td>	
					<td width="20%"><label>Person Contacted</label>
					<input name="site_contact$rnum" class="{$filled}" type="text" id="site_contact$rnum" value="{$system_read['contact']}"  /></td>				
				</tr>
				<tr>
					<td colspan="4"><label>Notes</label>
					<textarea name="reading_notes$rnum" class="{$filled}" cols="1000" id="reading_notes$rnum" >{$system_read['notes']}</textarea></td>
					<!--<td><input name="submit$rnum" type="button" value="{$button}" class="button" onclick="submitcheck($rnum)" /></td>-->
					<td><a name="submit$rnum" class="button_jquery_save" onclick="submitcheck($rnum)">{$button}</a></td>
				</tr> 
			</table>
		</form>
		<div class="line"></div>
        <br /> 
	</div> 
_FORM;
echo $formReading;
$rnum++;
}
?>		         
    </div>
</div>
<?php require_once($footer_include); ?>