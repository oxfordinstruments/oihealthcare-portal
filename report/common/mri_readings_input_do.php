<?php
//Update Completed 11/25/14
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

$post_date = date(storef,strtotime($_POST['date'])-$_SESSION['tz_offset_sec']);

$he=preg_replace("/[^0-9.]/","",$_POST['he']);
$vp=preg_replace("/[^0-9.]/","",$_POST['vp']);
$ch=preg_replace("/[^0-9.]/","",$_POST['ch']);
$notes = addslashes($_POST['notes']);
$contact = addslashes($_POST['contact']);

$sql="SELECT * FROM systems_mri_readings WHERE id = '".$_POST['idx']."';";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$num_rows = $result->num_rows;
//$row = $result->fetch_assoc();
if($num_rows == 0){
	$good = 0;
$sql=<<<_SQL
	INSERT INTO systems_mri_readings (system_unique_id, he, vp, hours, contact, `date`, notes, uid) 
	VALUES ('{$_POST['id']}', '$he', '$vp', '$ch', '$contact', '$post_date', '$notes', '{$_POST['uid']}');
_SQL;
	if ($mysqli->query($sql) === TRUE) {
		$sql="UPDATE systems_base_cont AS s SET 
			s.mr_lhe_list_update = '".$post_date."'
			WHERE s.unique_id = '".$_POST['id']."';";
		if ($mysqli->query($sql) === TRUE) {
			$log->logmsg('ID: '.$_POST['id'],1051,false,basename(__FILE__),__LINE__);
			$good = 1;
		}else{
			$good = 10;
		}
	}else{
		$good = 11;
	}
	echo $good;
}elseif($num_rows == 1){
	if($he == '' && $vp == '' && $ch == ''){
		$sql="DELETE FROM systems_mri_readings WHERE id = ".$_POST['idx'].";";	
		if ($mysqli->query($sql) === TRUE) {
			$log->logmsg('ID: '.$_POST['idx'],1050,false,basename(__FILE__),__LINE__);
			$good = 3;
		}else{
			$good = 30;
		}
	}else{
$sql=<<<_SQL
		UPDATE systems_mri_readings AS m SET 
		`he` = '$he',
		`vp` = '$vp',
		`hours` = '$ch',
		`contact` = '$contact',
		`date` = '$post_date',
		`notes` = '$notes',
		`edited` = 'Y',
		`edited_by` = '{$_POST['uid']}'
		WHERE m.id = {$_POST['idx']};
_SQL;
		if ($mysqli->query($sql) === TRUE) {
			$sql="UPDATE systems_base_cont AS s SET 
				s.mr_lhe_list_update = '".$post_date."'
				WHERE s.unique_id = '".$_POST['id']."';";
			if ($mysqli->query($sql) === TRUE) {
				$log->logmsg('ID: '.$_POST['id'],1052,false,basename(__FILE__),__LINE__);
				$good = 2;
			}else{
				$good = 20;
			}
		}else{
			$good = 21;
		}
	}
	echo $good;
		
}else{
	echo "999";	
}
?>