<?php
//Update Completed 11/26/14
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

$nps_settings = new SimpleXMLElement($_SERVER['DOCUMENT_ROOT'].'/nps_settings.xml', null, true);

$sql="SELECT sbc.system_id, sbc.nickname, f.contact_name, f.contact_email, f.nps_name, f.nps_email, sbc.facility_unique_id, c.`type`, f.email_list
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
LEFT JOIN misc_contracts AS c ON sbc.contract_type = c.id
LEFT JOIN facilities AS f ON f.unique_id = sbc.facility_unique_id
WHERE sbc.contract_type IN (".$nps_settings->contracts.") AND sbc.property = 'C'
ORDER BY sbc.system_id ASC;";	

if(!$resultSiteNPS = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$sitesNPS = array();
while ($rowSiteNPS = $resultSiteNPS->fetch_assoc()){
	array_push($sitesNPS,$rowSiteNPS);
}
?>
<!DOCTYPE html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<?php require_once($js_include);?>

<script src="/resources/js/jquery-ui/jquery-ui.min.js"></script>
<script src="/resources/js/jquery.ui.widget.js"></script>
<script src="/resources/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
$(document).ready(function() { 	
/////////////////////////////////////////////////////////////////////////////////////		
	<?php if(strtolower($_SESSION['prefs']['pref_show_help']) != 'y' or $_SESSION['mobile_device'] == true){ ?>
	$('#tiplayer').hide()
	<?php }	?>	

	$(".required").css("background","#FFFFCC");
	$(".required").each(function(index, element) {
		if($(this).val() == ""){
			$(this).css("background","#FFFFCC");
		}else{
			$(this).css("background","#FFFFFF");
		}
	});
	$(".required").change(function(e) {
		if($(this).val() == ""){
			$(this).css("background","#FFFFCC");
		}else{
			$(this).css("background","#FFFFFF");
		}
	});
	
	$(".picker").datepicker({ dateFormat: 'mm/dd' });
	//$( ".picker" ).datepicker();

	$(".button_jquery_save").button({
		icons: {
			primary: "ui-icon-disk"
		}
	});
	
/////////////////////////////////////////////////////////////////////////////////////
});
/////////////////////////////////////////////////////////////////////////////////////
function submitcheckTop(){
	$blank = "";
	if(document.getElementById("send_days").value == "") $blank = "Days befor quarter end to send surveys is required\n" + $blank;
	if(document.getElementById("remind_days").value == "") $blank = "Days befor quarter end to send survey reminders is required\n" + $blank;
	if(document.getElementById("q1").value == "") $blank = "Quarter 1 end date is required.\n" + $blank;
	if(document.getElementById("q2").value == "") $blank = "Quarter 2 end date is required.\n" + $blank;
	if(document.getElementById("q3").value == "") $blank = "Quarter 3 end date is required.\n" + $blank;
	if(document.getElementById("q4").value == "") $blank = "Quarter 4 end date is required.\n" + $blank;
	
	if($blank != ""){alert($blank);}
	if($blank == ""){
		if(confirm("Save Settings?")){
			//document.forms["form"+id].submit();
			$.ajax({
				type: 'POST',
				url: 'nps_setup_do.php',
				data: { nps_settings: 'Yes',
						send_days: $("#send_days").val(),
						remind_days: $("#remind_days").val(),
						q1: $("#q1").val(),
						q2: $("#q2").val(),
						q3: $("#q3").val(),
						q4: $("#q4").val() },
				success:function(data){
					// successful request; do something with the data
					//alert(data);
					if($.trim(data)=='1'){
						$("#send_days").css("background","#CCFFCC");
						$("#remind_days").css("background","#CCFFCC");
						$("#q1").css("background","#CCFFCC");
						$("#q2").css("background","#CCFFCC");
						$("#q3").css("background","#CCFFCC");
						$("#q4").css("background","#CCFFCC");
					}else{
						alert("Setup: There was a problem saving the data. Please contact support.");
					}
				}
			});
		}
	}
}
/////////////////////////////////////////////////////////////////////////////////////
function submitcheck(id){
	$blank = "";
	if(document.getElementById("nps_email"+id).value == "") $blank = "Email is required\n" + $blank;
	if(document.getElementById("nps_name"+id).value == "") $blank = "Name is required\n" + $blank;
	
	if($blank != ""){alert($blank);}
	if($blank == ""){
		if(confirm("Save System Info?")){
			//document.forms["form"+id].submit();
			$.ajax({
				type: 'POST',
				url: 'nps_setup_do.php',
				data: { system_id: $("#system_id"+id).val(),
						unique_id: $("#unique_id"+id).val(),
						email: $("#nps_email"+id).val(),
						name: $("#nps_name"+id).val(),
						uid: '<?php echo $_SESSION['login']; ?>'},
				success:function(data){
					// successful request; do something with the data
					//alert(data);
					if($.trim(data)=='1'){
						//$("#divForm"+id).hide('fast');
						$("#nps_email"+id).css("background","#CCFFCC");
						$("#nps_name"+id).css("background","#CCFFCC");
					}else{
						alert("There was a problem saving the data. Please contact support.");
					}
				}
			});
		}
	}
}
/////////////////////////////////////////////////////////////////////////////////////
</script>
</head>
<body>
<?php require_once($header_include); ?>
<div id="OIReportContent">
    <div id="styledForm">
		<h1>NPS Settings Form</h1>
		<p>Saved = <span style="color:#3C0">Green</span>&nbsp;&nbsp;&nbsp;Missing Data = <span style="color:#FF0">Yellow</span></p>

		<form id="formSettings" name="formSettings" method="post" autocomplete="off">
			<input type="hidden" name="nps_settings" id="nps_settings" value="Y" />
			<table width="100%" cellpadding="2" cellspacing="2">
				<tr>
					<td width="40%"><label>Days befor quarter end to send surveys</label>
					<input name="send_days" class="required tooltip" type="number" id="send_days" value="<?php echo $nps_settings->send_days; ?>"  /></td>
					<td width="40%">&nbsp;</td>
					<td><!--<input name="submitTop" type="button" value="Save Settings" class="button" onclick="submitcheckTop()" />--><a name="submitTop" class="button_jquery_save" onclick="submitcheckTop()">Save Setings</a></td>
				</tr>
			</table>
			<table width="100%" cellpadding="2" cellspacing="2">
				<tr>
					<td width="40%"><label>Days befor quarter end to send survey reminder</label>
					<input name="remind_days" class="required tooltip" type="number" id="remind_days" value="<?php echo $nps_settings->remind_days; ?>"  /></td>
					<td width="40%">&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
			<table width="100%" cellpadding="2" cellspacing="2">
				<tr>
					<td><label>Q1 End (mm/dd)</label>
					<input name="q1" class="required picker tooltip" type="text" id="q1" value="<?php echo $nps_settings->q1_end; ?>"  /></td>
					<td><label>Q2 End (mm/dd)</label>
					<input name="q2" class="required picker tooltip" type="text" id="q2" value="<?php echo $nps_settings->q2_end; ?>"  /></td>
					<td><label>Q3 End (mm/dd)</label>
					<input name="q3" class="required picker tooltip" type="text" id="q3" value="<?php echo $nps_settings->q3_end; ?>"  /></td>
					<td><label>Q4 End (mm/dd)</label>
					<input name="q4" class="required picker tooltip" type="text" id="q4" value="<?php echo $nps_settings->q4_end; ?>"  /></td>
				</tr>
			</table>
		</form>
		<br />
		<br />
		<div class="line"></div>		
<?php
$dtnow = date("m/d/Y H:i",time());
$rnum = 0;
foreach($sitesNPS as $siteNPS){
	$site_info = '';
	if($siteNPS['contact_name'] != ''){$site_info .= "System Contact: ".$siteNPS['contact_name']."< ".$siteNPS['contact_email']." >   ";}
	if($siteNPS['contact_name'] == ''){$site_info .= "Other: ".$siteNPS['email_list'];}
$formNPS=<<<_FORM
	<div id="divForm$rnum">
		<form id="form$rnum" name="form$rnum" method="post" autocomplete="off">
			<input type="hidden" name="arr_id$rnum" id="arr_id$rnum" value="$rnum" />
			<input type="hidden" name="unique_id$rnum" id="unique_id$rnum" value="{$siteNPS['facility_unique_id']}" />
			<input type="hidden" name="system_id$rnum" id="system_id$rnum" value="{$siteNPS['system_id']}" />
			<table width="100%" cellpadding="2" cellspacing="2">
				<tr>
					<td colspan="5"><h3 style="font-size:16px">{$siteNPS['system_id']}  {$siteNPS['nickname']}</h3></td>
				</tr>
				<tr>
					<td width="40%"><label>Email</label>
					<input name="nps_email$rnum" class="required" type="email" id="nps_email$rnum" value="{$siteNPS['nps_email']}"  /></td>
					<td width="40%"><label>Name</label>
					<input name="nps_name$rnum" class="required" type="text" id="nps_name$rnum" value="{$siteNPS['nps_name']}"  /></td>
					<td><!--<input name="submit$rnum" type="button" value="Save" class="button" onclick="submitcheck($rnum)" />--><a name="submit$rnum" class="button_jquery_save" onclick="submitcheck($rnum)">Save</a></td>
				</tr>
				<tr>
					<td colspan="3">$site_info</td>
				</tr> 
			</table>
		</form>
		<div class="line"></div>
        <br /> 
	</div> 
_FORM;
echo $formNPS;
$rnum++;
}
?>		   



	
	
	
      
    </div>
</div>
<?php require_once($footer_include); ?>