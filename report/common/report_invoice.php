<?php
//Update Completed 12/22/14
$debug = false;
if(isset($_GET['debug'])){
	$debug = true;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

if(isset($_GET['uid'])){
	$user_id=$_GET['uid'];
}else{
	$log->logerr('report_invoice.php',1017);
	header("location:/error.php?n=1017&p=report_invoice.php");
}
if(isset($_GET['id'])){
	$unique_id=$_GET['id'];
}else{
	$log->logerr('report_invoice.php',1018);
	header("location:/error.php?n=1018&p=report_invoice.php");
}

if(isset($_GET['ed'])){$edit=true;}else{$edit=false;}

$sql="SELECT * FROM systems_requests 
	WHERE unique_id = '".$unique_id."';";
if(!$resultRequest = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowRequest = $resultRequest->fetch_assoc();
d($rowRequest);

if(!$edit){
	$sql="SELECT * FROM systems_reports 
	WHERE unique_id = '".$unique_id."';";
	if(!$resultReport = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
}else{
	$sql="SELECT r.*,u.name AS invoiced_by_name 
	FROM systems_reports AS r 
	LEFT JOIN users AS u ON u.uid = r.invoiced_uid 
	WHERE r.unique_id = '".$unique_id."';";
	if(!$resultReport = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
}
$rowReport = $resultReport->fetch_assoc();
d($rowReport);

$sql="SELECT sbc.system_id, sbc.nickname, sb.system_serial, sb.status, sb.system_type, sbc.contract_type, f.address, f.city, f.state, f.zip, sbc.ver_unique_id
	FROM systems_base_cont AS sbc
	LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
	LEFT JOIN facilities AS f ON f.unique_id = sbc.facility_unique_id
	WHERE sb.unique_id = '".$rowRequest['system_unique_id']."';";
if(!$resultSystem = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowSystem = $resultSystem->fetch_assoc();
d($rowSystem);

$sql="SELECT * FROM users WHERE uid = '".$rowRequest['engineer']."';";
if(!$resultAssigned = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowAssigned = $resultAssigned->fetch_assoc();

$sql="SELECT a.uid,u.name,u.email,u.cell,u.carrier
	FROM systems_assigned_pri AS a 
	LEFT JOIN users AS u ON a.uid = u.uid 
	WHERE a.system_ver_unique_id='".$rowSystem['ver_unique_id']."';";
if(!$resultEngPri = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowEngPri = $resultEngPri->fetch_assoc();

$sql="SELECT a.uid,u.name,u.email,u.cell,u.carrier
	FROM systems_assigned_sec AS a 
	LEFT JOIN users AS u ON a.uid = u.uid 
	WHERE a.system_ver_unique_id='".$rowSystem['ver_unique_id']."';";
if(!$resultEngSec = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowEngSec = $resultEngSec->fetch_assoc();

$sql="SELECT * FROM misc_contracts 
	WHERE id = ".$rowSystem['contract_type']." LIMIT 1;";
if(!$resultContract = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

$sql="SELECT * FROM misc_contracts;";
if(!$resultContracts = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

$sql="SELECT * FROM systems_types 
	WHERE `id` = '".$rowSystem['system_type']."' LIMIT 1;";
if(!$resultEquipment = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

$sql="SELECT u.uid, u.name
FROM users AS u
LEFT JOIN users_roles AS ur ON ur.uid = u.uid
LEFT JOIN users_role_id AS rid ON rid.id = ur.rid
WHERE rid.role = 'role_engineer' OR rid.role = 'role_contractor';";
if(!$resultEngineer = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

$sql="SELECT * FROM systems_status;";
if(!$resultStatus = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

$t=time();

$sql="SELECT * FROM systems_parts WHERE unique_id = '".$unique_id."';";
if(!$resultParts = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$numberParts = $resultParts->num_rows;

$uploadBtnVal = "Attach Documents";
$has_docs = false;
$num_docs = "Internal records only. Customer can not view documents.";
if(strtolower($rowReport['has_files']) == 'y'){
	$uploadBtnVal = "View/Modify Documents";
	$has_docs = true;
	$num_docs = "Approximately " . $rowReport['file_count'] . " Documents Attached";
}

?>
<!DOCTYPE html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>

<link href="/resources/css/form_report_view.css" rel="stylesheet" type="text/css" media="screen">

<?php require_once($js_include);?>

<!--<script src="/resources/js/jquery-ui/jquery-ui.min.js"></script>-->
<!--<script src="/resources/js/jquery.ui.widget.js"></script>-->
<script src="/resources/js/jquery.ui.datepicker.js"></script>
<script src="/resources/js/jquery-dynamic-form_MODIFIED.js"></script>
<script type="text/javascript">
$(document).ready(function() {
 	

	$(".price").css("background","#99FF99");	
	$(".needInvoice").css("background","#FFFF66");
	$(".needInvoice").change(function(e) {
		if($(this).val() == ""){
			$(this).css("background","#FFFF66");
		}else{
			$(this).css("background","#99FF99");
		}
	});	
	$("#invoice_date").datepicker({ 
		showButtonPanel: true,
		changeYear: true,
		constrainInput: true,
		duration: "fast",
		dateFormat: '<?php echo dpdispfd; ?>' 
	});
	
	$(".button_jquery_mark").button({
		icons: {
			primary: "ui-icon-check"
		}
	});
	
	$(".button_jquery_print").button({
		icons: {
			primary: "ui-icon-print"
		}
	});
	
	$(".button_jquery_save").button({
		icons: {
			primary: "ui-icon-disk"
		}
	});
	
});

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function submitcheck(){
	ids = [];
	errors = [];
	var datere = /^20(1|2)\d-((1[0-2])|(0[1-9]))-(([0-2][0-9])|3(0|1))$/; //date yyyy-mm-dd
	
	if($('#invoice_number').val()==""){ids.push('#invoice_number'); errors.push("Invoice Number");}			
	if($('#invoice_date').val()==""){ids.push('#invoice_date'); errors.push("Invoice Date");}
	
	var inp_val = $('#invoice_date').val();
	if(!datere.test(inp_val)){ids.push("#invoice_date"); errors.push("Invoice Date Invalid");}
	
	console.log("ids: " + ids);
	console.log("errors: " + errors);
	showErrors(ids,errors);
			
	if(ids.length <= 0){
			$.prompt("Are you sure you want to mark this service report as invoiced?",{
				title: "Invoiced",
				buttons: { Yes: 1, No: -1 },
				focus: 1,
				submit:function(e,v,m,f){ 
					e.preventDefault();
					$("#errors").hide();
					if(v == 1){
						$.prompt.close();
						$('#skipDiv').remove();
						document.forms["form"].submit();
					}else{
						$.prompt.close();	
					}					
				}
			});
			
	}
}
/////////////////////////////////////////////////////////////////////////////////////
function showErrors(ids,errors){
	//thin solid #2C3594
	$("input, select, .chosen-container").each(function(index, element) {
		$(this).animate({
			borderColor: "#2C3594",
			boxShadow: 'none'
		});
	});
	
	$.each(ids,function(index,value){
		$(value).animate({
			borderColor: "#cc0000",
			boxShadow: '0 0 5px 3px rgba(255,0,0,0.4)'
		});
	});
	$("#errors > span").html("");
	$.each(errors,function(index, value){
		$("#errors > span").append(value + "<br>");
	});
	$("#errors").show('slow');
	$(document).scrollTop(0);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
</script>
</head>
<body>
<?php require_once($header_include); ?>
<div id="OIReportContent"> 
<div id="stylized" class="myform">
<form id="form" name="form"  method="post" enctype='multipart/form-data' action="report_invoice_do.php">
<div id="srHeaderDiv"><h1>Service Report To Be Invoiced</h1></div>
<div id="main"><!-- do not remove -->
<div id="errors" style="text-align:center;display:none; margin-bottom:25px; margin-top:25px; font-size:18px;">
	<h2 style="margin:0px; padding:0px; font-size:24px;">Errors to fix</h2>
	<span style="color:#F00">
	</span>
</div>
<div id="srDataDiv">
    <table id="srDataTable" class="srTable">
        <tr>
			<td class="rowLabel">Invoice Number:</td>
			<td class="rowData"><input <?php if(!$edit){echo "class=\"needInvoice tooltip\"";} ?> type="text" id="invoice_number" name="invoice_number" value="<?php if($edit){echo $rowReport['invoice_number'];} ?>" /></td>
			<td class="rowLabel">Invoice Date:</td>
			<td class="rowData"><input <?php if(!$edit){echo "class=\"needInvoice tooltip\"";} ?> type="text" id="invoice_date" name="invoice_date" value="<?php if($edit){echo date(phpdispfd,strtotime($rowReport['invoice_date']));} ?>" /></td>
        </tr>
		<?php if($edit){?>
		<tr>
			<td class="rowLabel">Invoiced By:</td>
			<td class="rowData" colspan="3"><input type="text" disabled id="invoiced_by" name="invoiced_by" value="<?php if($edit){echo $rowReport['invoiced_by_name'];} ?>" /></td>
        </tr>
		<?php } ?>
    </table>
</div>

<div id="srInfoDiv">
	<table id="srInfoTable" class="srTable">
		<tr>
		  <td class="rowLabel">System&nbsp;ID: </td>
		  <td class="rowData"><?php echo $rowSystem['system_id']; ?></td>
		  <td class="rowLabel">Name: </td>
		  <td class="rowData"><?php echo $rowSystem['nickname']; ?></td>
		</tr>
		<tr>
		  <td class="rowLabel">Address: </td>
		  <td colspan="4" class="rowData"><?php echo $rowSystem['address'] ."&nbsp;&nbsp;&nbsp;". $rowSystem['city'] .", ". $rowSystem['state'] ."  ". $rowSystem['zip'];?></td>
		</tr>
		<tr>
		  <td class="rowLabel">Serial&nbsp;Number: </td>
		  <td class="rowData"><?php echo $rowSystem['system_serial']; ?></td>
		  <td class="rowLabel">System: </td>
		  <td class="rowData"><?php echo $resultEquipment->fetch_object()->name; ?></td>
		</tr>
		<tr>
		  <td class="rowLabel">Contract&nbsp;Type: </td>
		  <td class="rowData"><?php echo $resultContract->fetch_object()->type; ?></td>
		  <td class="rowLabel">Assigned&nbsp;Engineer: </td>
		  <td class="rowData"><?php echo $rowAssigned['name']; ?></td>
		</tr>
		<tr>
		  <td class="rowLabel">Service&nbsp;Request&nbsp;Num: </td>
		  <td class="rowData"><?php echo $rowRequest['request_num']; ?></td>
		  <td class="rowLabel">Primary&nbsp;Engineer: </td>
		  <td class="rowData"><?php echo $rowEngPri['name']; ?></td>
		</tr>
	</table>
</div>

<div id="srDataDiv">
	<table id="srDataTable" class="srTable">
		<tr>
			<td class="rowLabel"></td>
			<td class="rowData"></td>
			<td class="rowLabel">Purchase Order:</td>
			<td class="rowData"><input type="text" id="po_number" name="po_number" class="tooltip" <?php if(!$edit && $rowReport['po_number'] == ''){echo "class=\"needInvoice\"";}?> value="<?php echo $rowReport['po_number'];?>" /></td>
		</tr>
		<tr>
			<td class="rowLabel">Invoice Required:</td>
			<td class="rowData"><input name="invoice_req" id="invoice_req"  readonly value="<?php echo strtoupper($rowReport['invoice_required']) ?>" /></td> 
			<td class="rowLabel">Contract Type Override:</td>
			<?php
				while($rowContracts = $resultContracts->fetch_assoc())
				{
					if($rowReport['contract_override']==$rowContracts['id']){
						$contract_override = $rowContracts['type'];
					}
				}
			?>
			<td class="rowData"><input name="contract_override" id="contract_overide"  readonly value="<?php echo $contract_override; ?>" /></td>
		</tr>
	</table>
</div>
	
<div id="skipDiv">	
	<div id="srDataDiv">
		<table id="srDataTable" class="srTable">
			<tr>
			  <td class="rowLabel">Report Date:</td>
			  <td class="rowData"><input type="text" id="report_date" name="report_date" class="findme" readonly value="<?php echo date(phpdispfd,strtotime($rowReport['date'])); ?>" onmouseout="return hideTip();"/></td>
			  <?php
				while($rowEngineer = $resultEngineer->fetch_assoc())
				{
					if(strtolower($rowEngineer['uid']) == strtolower($rowReport['engineer'])){
						$engineer_name = $rowEngineer['name'];
					}	
				}
			  ?>
			  <td class="rowLabel">Service Engineer:</td>
			  <td class="rowData"><input name="engineer" id="engineer" readonly value="<?php echo $engineer_name; ?>" /></td>
			</tr>
			<tr>
			  
			  
			</tr>
			<tr>
			  <td class="rowLabel">Preventative Maintenance:</td>
			  <td class="rowData"><input name="pm" id="pm" readonly value="<?php echo strtoupper($rowReport['pm']); ?>" /></td>
			  <td class="rowLabel">Hours System was down:</td>
			  <td class="rowData"><input type="text" id="down_time" name="down_time" readonly value="<?php echo $rowReport['downtime']; ?>" /></td>
			</tr>
			<tr>
			<?php
				while($rowStatus = $resultStatus->fetch_assoc())
				{
					if($rowReport['system_status']==$rowStatus['id']){
						$equipment_status = $rowStatus['status'];
					}
				}
			  ?>
			  <td class="rowLabel">System Status:</td>
			  <td class="rowData"><input name="equipment_status" id="equipment_status" readonly value="<?php echo $equipment_status; ?>" /></td> 
			  
			  <td class="rowLabel" <?php if(strtolower($rowRequest['repeat_call']) == "n"){echo "style=\"display:none\"";} ?>>Repeat Call:</td>
			  <td class="rowData" <?php if(strtolower($rowRequest['repeat_call']) == "n"){echo "style=\"display:none\"";} ?>><?php 
				if($rowRequest['repeat_call_id'] != ""){
					$repeat_call_ids = explode(",", $rowRequest['repeat_call_id']);
					foreach($repeat_call_ids as $repeat_call_id) {
						echo "<a class=\"viewMini\" href='report_view_mini.php?id=".$repeat_call_id."&uid=".$user_id."'>".$repeat_call_id."</a>&nbsp;";
					}
					
				} 
				?></td> 
				 
			</tr>
		</table>
	</div>
	
	<div id="srDataDivCT" <?php if($resultEquipment->fetch_object()->type != "CT"){ echo" style='display:none'";} ?> >
		<table id="srDataTable">
			<tr>
			  <td class="rowLabel">Slice/mAs Count:</td>
			  <td class="rowData"><input type="text" id="slice_mas" readonly name="slice_mas" <?php echo "value=\"".$rowReport['slice_mas_count']."\"";?>/></td>
			  <td class="rowLabel">Gantry Revolutions:</td>
			  <td class="rowData"><input type="text" id="gantry_rev" readonly name="gantry_rev" <?php echo "value=\"".$rowReport['gantry_rev']."\"";?>/></td>
			</tr>
		</table>
	</div>
	
	<div id="srDataDivMR" <?php if($resultEquipment->fetch_object()->type != "MR"){ echo" style='display:none'";} ?>>
		<table id="srDataTable">
			<tr>
			  <td class="rowLabel">Helium Level:</td>
			  <td class="rowData"><input type="text" id="helium" readonly name="helium" <?php echo "value=\"".$rowReport['helium_level']."\"";?>/></td>
			  <td class="rowLabel">Vessel Pressure:</td>
			  <td class="rowData"><input type="text" id="pressure" readonly name="pressure" <?php echo "value=\"".$rowReport['vessel_pressure']."\"";?>/></td>
			</tr>
			<tr>
			  <td class="rowLabel">Compressor Pressure:</td>
			  <td class="rowData"><input type="text" id="compressor_pressure" readonly name="compressor_pressure" <?php echo "value=\"".$rowReport['compressor_pressure']."\"";?>/></td>
			  <td class="rowLabel">Compressor Hours:</td>
			  <td class="rowData"><input type="text" id="compressor_hours" readonly name="compressor_hours" <?php echo "value=\"".$rowReport['compressor_pressure']."\"";?>/></td>
			</tr>
			<tr>
			  <td class="rowLabel">Recon RUO:</td>
			  <td class="rowData"><input type="text" id="recon_ruo" readonly name="recon_ruo" <?php echo "value=\"".$rowReport['recon_ruo']."\"";?>/></td>
			  <td class="rowLabel">Coldhead RUO:</td>
			  <td class="rowData"><input type="text" id="coldhead_ruo" readonly name="coldhead_ruo" <?php echo "value=\"".$rowReport['cold_ruo']."\"";?>/></td>
			</tr>
		</table>
	</div>
	
	<div id="srComplaintDiv">
		<table id="srComplaintTable" class="srTable">
			<tr>
			  <td class="rowLabel">Customer Complaint</td>
			</tr>
			<tr>
			  <td class="rowData"><textarea id="complaint" name="complaint" maxlength="2000" readonly><?php echo $rowReport['complaint']; ?></textarea></td>
			</tr>
		</table>
	</div>
	
	<div id="srServiceDiv">
		<table id="srServiceTable" class="srTable">
			<tr>
			  <td class="rowLabel">Service Performed</td>
			</tr>
			<tr>
			  <td class="rowData"><textarea id="service" name="service" maxlength="4000" readonly><?php echo $rowReport['service'];?></textarea></td>
			</tr>
		</table>
	</div>
	
	<div id="srNotesDiv" <?php if(strtolower($_SESSION['userdata']['grp_customer']) == "y"){echo "style=\"display:none\"";} ?>>
		<table id="srNotesTable" class="srTable">
			<tr>
			  <td class="rowLabel">Engineer Notes</td>
			</tr>
			<tr>
			  <td class="rowData"><textarea id="notes" name="notes" maxlength="2000" readonly><?php echo $rowReport['notes'];?></textarea></td>
			</tr>
		</table>
	</div>
	
	<?php if($_SESSION['group'] == "1" and $has_docs){?>
	<div id="srFilesDiv">
		<table id="srFilesTable" class="srTable">
			<tr>
				<td class="rowLabel" colspan="2">Attached Documents</td>
			</tr>
			<tr>
				<td class="rowData" width="50%"><div class="srBottomBtn"><a href="/resources/kcfinder/kcfinder.php?unique_id=<?php echo $unique_id; ?>&table=systems_reports_files" class="button_jquery_save iframeUpload" id="attachDocs"><?php echo $uploadBtnVal; ?></a></div></td>
				<td class="rowData" style="text-align:center; color:#f79548"><?php echo $num_docs; ?></td>
			</tr>
		</table>
	</div>
	<?php } ?>
</div><!-- skipDiv -->
	<div style="display:none">
		<input type="text" id="user_id" name="user_id" value="<?php echo $_SESSION['login']; ?>" />
		<input type="text" id="system_id" name="system_id" value="<?php echo $rowSystem['system_id']; ?>" />
		<input type="text" id="system_nickname" name="system_nickname" value="<?php echo $rowSystem['nickname']; ?>" />
		<input type="text" id="engineer_assigned" name="engineer_assigned" value="<?php echo $rowRequest['engineer']; ?>" />
		<input type="text" id="report_id" name="report_id" value="<?php echo $rowRequest['request_num']; ?>" />
		<input type="text" id="unique_id" name="unique_id" value="<?php echo $rowRequest['unique_id']; ?>" />
	</div>

	<?php
		$sql="SELECT * FROM systems_hours WHERE unique_id = '".$unique_id."';";
		if(!$resultHours = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
		$numberHours = $resultHours->num_rows;
		
	?>
	<div id="srHoursDiv" <?php if(intval($numberHours) <= 0){echo " style=\"display:none\"";}?>>
		<table id="srHoursTable" class="srTable">
			<tr>
				<th scope="col">Date</th>
				<th colspan="2" scope="col">Hours Worked</th>
				<th colspan="2" scope="col">Hours Traveled</th>
				</tr>
			<tr>
		
				<td class="rowLabel"></td>
				<td class="rowLabel">Reg</td>
				<td class="rowLabel">OT</td>
				<td class="rowLabel">Reg</td>
				<td class="rowLabel">OT</td>
			</tr>
			<?php
			$hwr=0;
			$hwo=0;
			$htr=0;
			$hto=0;
			if(intval($numberHours) > 0){
				$i = 0;
				while($rowHours = $resultHours->fetch_assoc())
				{
					echo "<tr id=\"hours\">\n";
					echo "<td class=\"rowData\"><input readonly type=\"text\" id=\"hours_date\" name=\"hours[$i][hours_date]\" value=\"".date(phpdispfd,strtotime($rowHours['date']))."\" /></td>\n";
					echo "<td class=\"rowData\"><input readonly type=\"number\" id=\"hours_wreg\" name=\"hours[$i][hours_wreg]\" value=\"".$rowHours['reg_labor']."\" /></td>\n";
					echo "<td class=\"rowData\"><input readonly type=\"number\" id=\"hours_wot\" name=\"hours[$i][hours_wot]\" value=\"".$rowHours['ot_labor']."\" /></td>\n";
					echo "<td class=\"rowData\"><input readonly type=\"number\" id=\"hours_treg\" name=\"hours[$i][hours_treg]\" value=\"".$rowHours['reg_travel']."\" /></td>\n";
					echo "<td class=\"rowData\"><input readonly type=\"number\" id=\"hours_tot\" name=\"hours[$i][hours_tot]\" value=\"".$rowHours['ot_travel']."\" /></td>\n";
					echo "</tr>\n";
					$hwr += floatval($rowHours['reg_labor']);
					$hwo += floatval($rowHours['ot_labor']);
					$htr += floatval($rowHours['reg_travel']);
					$hto += floatval($rowHours['ot_travel']);
					$i++;
				}
				
				echo "<tr id=\"hours\">\n";
				echo "<td class=\"rowData\" style=\"text-align:center\">Total Hours</td>\n";
				echo "<td class=\"rowData\" style=\"text-align:center\">".$hwr."</td>\n";
				echo "<td class=\"rowData\" style=\"text-align:center\">".$hwo."</td>\n";
				echo "<td class=\"rowData\" style=\"text-align:center\">".$htr."</td>\n";
				echo "<td class=\"rowData\" style=\"text-align:center\">".$hto."</td>\n";
				echo "</tr>\n";
			}    
			?>
			<tr>
				<td class="rowLabel">Hourly Rates</td>
				<td class="rowData"><input type="number" readonly class="price" id="rate_labor_reg" name="rate_labor_reg" value="<?php echo $rowReport['invoice_labor_reg_rate']; ?>" /></td>
				<td class="rowData"><input type="number" readonly class="price" id="rate_labor_ot" name="rate_labor_ot" value="<?php echo $rowReport['invoice_labor_ot_rate']; ?>" /></td>
				<td class="rowData"><input type="number" readonly class="price" id="rate_travel_reg" name="rate_travel_reg" value="<?php echo $rowReport['invoice_travel_reg_rate']; ?>" /></td>
				<td class="rowData"><input type="number" readonly class="price" id="rate_travel_ot" name="rate_travel_ot" value="<?php echo $rowReport['invoice_travel_ot_rate']; ?>" /></td>
			</tr>
			<tr>
				<td class="rowLabel">Total Rate</td>
				<td class="rowData"><input type="number" readonly class="price" id="total_rate_labor_reg" name="total_rate_labor_reg" value="<?php echo $rowReport['invoice_labor_reg']; ?>" /></td>
				<td class="rowData"><input type="number" readonly class="price" id="total_rate_labor_ot" name="total_rate_labor_ot" value="<?php echo $rowReport['invoice_labor_ot']; ?>" /></td>
				<td class="rowData"><input type="number" readonly class="price" id="total_rate_travel_reg" name="total_rate_travel_reg" value="<?php echo $rowReport['invoice_travel_reg']; ?>" /></td>
				<td class="rowData"><input type="number" readonly class="price" id="total_rate_travel_ot" name="total_rate_travel_ot" value="<?php echo $rowReport['invoice_travel_ot']; ?>" /></td>
			</tr>
		</table>
	</div>
	
	<div id="srPartsDiv"<?php if(intval($numberParts) <= 0){echo " style=\"display:none\"";}?>>
		<table id="srPartsTable" class="srTable">
			<tr>
				<th scope="col">Part Number</th>
				<th scope="col">Serial Number</th>
				<th scope="col">Description</th>
				<th scope="col">Quantity</th>
				<th scope="col">Price Each</th>
			</tr>
			<?php
			if(intval($numberParts) > 0){
				$i=0;
				while($rowParts = $resultParts->fetch_assoc())
				{			
					echo "<tr id=\"parts\">\n";
					echo "<td class=\"rowData\"><input readonly type=\"text\" id=\"parts_number$i\" name=\"parts[$i][num]\" value=\"".$rowParts['number']."\" /><div style=\"display:none\"><input readonly type=\"text\" id=\"part_id$i\" name=\"parts[$i][id]\" value=\"".$rowParts['index']."\" /></div></td>\n";
					echo "<td class=\"rowData\"><input readonly type=\"text\" id=\"parts_serial$i\" name=\"parts[$i][ser]\" value=\"".$rowParts['serial']."\" /></td>\n";
					echo "<td class=\"rowData\"><input readonly type=\"text\" id=\"parts_description$i\" name=\"parts[$i][desc]\" value=\"".$rowParts['description']."\" /></td>\n";
					echo "<td class=\"rowData\"><input readonly class=\"price\" readonly type=\"number\" id=\"parts_quanity$i\" name=\"parts[$i][qty]\" value=\"".$rowParts['qty']."\" /></td>\n";
					echo "<td class=\"rowData\"><input readonly class=\"price\" type=\"number\" id=\"part_price$i\" name=\"parts[$i][price]\" value=\"".$rowParts['price']."\" /></td>\n";
					echo "</tr>\n";
					$i++;
				}
			}
			?>
		</table>
	</div>
	
	<div id="srShipDiv">
		<table id="srShipTable" class="srTable">
			<tr>
				<th scope="col">Shipping</th>
				<th scope="col">Expenses</th>
			</tr>
			<tr>
				<td class="rowData"><input readonly class="price" type="number" id="shipping" name="shipping" value="<?php echo $rowReport['invoice_shipping']; ?>" /></td>
				<td class="rowData"><input readonly class="price" type="number" id="expense" name="expense" value="<?php echo $rowReport['invoice_expense']; ?>" /></td>
			</tr>
		
		</table>
	</div>

<div id="srFooterDiv">
	<div class="srBottomBtn"><a class="button_jquery_mark" onClick="submitcheck()"><?php if($edit){echo "Save Changes";}else{echo "Mark Invoiced";} ?></a></div>
	<div class="srBottomBtn"><a class="button_jquery_print" target="new" href="<?php echo "/resources/print_sr/f70-14-15_rev-b.php?unique_id=$unique_id"; ?>">Print Report</a></div>
    <div align="right"><h4><?php echo "Unique ID: ".$unique_id; ?></h4></div>
</div>
<div style="display:none">
<input type="text" id="edit_invoice" name="edit_invoice" value="<?php if($edit){echo "Y";} ?>" />
<?php if($debug){ ?><input name="debug" id="debug" type="hidden" value="Y" /> <?php } ?>
</div>
</form>
</div>
</div>
<?php require_once($footer_include); ?>