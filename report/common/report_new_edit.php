<?php
$debug = false;
if(isset($_GET['debug'])){
	$debug = true;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/php_utils.php');
$php_utils = new phpUtils(NULL, $debug);

if(isset($_SESSION['perms']['perm_edit_reports']) or isset($_SESSION['roles']['role_engineer']) or isset($_SESSION['roles']['role_contractor'])){	
}else{
	$log->loginfo('report_new_edit.php',1023);
	header("location:/error.php?n=1023&p=report_new_edit.php");	
}

unset($_SESSION['service_report_refresh']);

$t=time();

if(isset($_GET['uid'])){
	$user_id=$_GET['uid'];
}else{
	$log->logerr('report_new_edit.php',1017);
	header("location:/error.php?n=1017&p=report_new_edit.php");
}
if(isset($_GET['id'])){
	$unique_id=$_GET['id'];
}else{
	$log->logerr('report_new_edit.php',1018);
	header("location:/error.php?n=1018&p=report_new_edit.php");
}

$sql="SELECT r.*, u.name AS engineer_name, u2.name AS dispatch_name
FROM systems_requests AS r
LEFT JOIN users AS u ON u.uid = r.engineer
LEFT JOIN users AS u2 ON u2.uid = r.sender
WHERE r.unique_id = '".$unique_id."';";
if(!$resultRequest = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowRequest = $resultRequest->fetch_assoc();
d($rowRequest);

$edit = false;
$mgt_edit = false;

if(strtolower($rowRequest['report_started'])=="y"){$edit = true;}

$uploadBtnVal = "Attach Documents";
$has_docs = false;
$num_docs = "Internal records only. Customer can not view documents.";

if($edit){
	$sql="SELECT r.*, COUNT(rf.id) AS file_count 
	FROM systems_reports AS r 
	LEFT JOIN systems_reports_files AS rf ON rf.unique_id = r.unique_id 
	WHERE r.unique_id = '".$unique_id."';";
	if(!$resultReport = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$rowReport = $resultReport->fetch_assoc();
	d($rowReport);
	
	if(strtolower($rowReport['status'])=="closed"){$mgt_edit = true;}
	
	if(strtolower($rowReport['has_files']) == 'y'){
		$uploadBtnVal = "View/Modify Documents";
		$has_docs = true;
		$num_docs = "Approximately " . $rowReport['file_count'] . " Documents Attached";
	}
}
		 
$sql="SELECT sb.*, sbc.*, f.address, f.city, f.state, f.zip, f.timezone, st.modality
	FROM systems_base_cont AS sbc
	LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
	LEFT JOIN facilities AS f on f.unique_id = sbc.facility_unique_id
	LEFT JOIN systems_types AS st ON st.id = sb.system_type
	WHERE sbc.ver_unique_id = '".$rowRequest['system_ver_unique_id']."';";
if(!$resultSystem = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowSystem = $resultSystem->fetch_assoc();
d($rowSystem);

$system_unique_id = $rowSystem['unique_id'];
$system_id = $rowSystem['system_id'];
$system_ver = $rowSystem['ver'];

$sql="SELECT notes, customer_unique_id FROM facilities WHERE unique_id = '".$rowSystem['facility_unique_id']."';";
if(!$resultFacility = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowFacility = $resultFacility->fetch_assoc();
d($rowFacility);

$sql="SELECT notes FROM customers WHERE unique_id = '".$rowFacility['customer_unique_id']."';";
if(!$resultCustomer = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowCustomer = $resultCustomer->fetch_assoc();
d($rowCustomer);

$sql="SELECT * FROM users WHERE `uid` = '".$user_id."';";
if(!$resultUser = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowUser = $resultUser->fetch_assoc();

$sql="SELECT * FROM users WHERE `uid` = '".$rowRequest['engineer']."';";
if(!$resultAssignedEng = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowAssignedEng = $resultAssignedEng->fetch_assoc();

$sql="SELECT a.uid,u.name
FROM systems_assigned_pri AS a 
LEFT JOIN users AS u ON a.uid = u.uid 
WHERE a.system_ver_unique_id='".$rowSystem['ver_unique_id']."';";
if(!$resultEngPri = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowEngPri = $resultEngPri->fetch_assoc();

$sql="SELECT * FROM misc_contracts WHERE `id` = '".$rowSystem['contract_type']."' LIMIT 1;";
if(!$resultContract = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowContract = $resultContract->fetch_assoc();

$sql="SELECT * FROM misc_contracts;";
if(!$resultContracts = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

$sql="SELECT * FROM systems_types WHERE `id` = '".$rowSystem['system_type']."' LIMIT 1;";
if(!$resultEquipment = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowEquipment = $resultEquipment->fetch_assoc();
d($rowEquipment);

$sql="SELECT u.uid, u.name
FROM users AS u
LEFT JOIN users_roles AS ur ON ur.uid = u.uid
LEFT JOIN users_role_id AS rid ON rid.id = ur.rid
WHERE (rid.role = 'role_engineer' OR rid.role = 'role_contractor') AND active = 'Y' 
ORDER BY name ASC;";
if(!$resultEngineer = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

$sql="SELECT * FROM `systems_status`;";
if(!$resultStatus = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$numResultPrevNotes = 0;

if($edit){
	$sql="SELECT notes, unique_id FROM systems_reports WHERE unique_id = '".$rowReport['prev_report_id']."' ORDER BY id DESC LIMIT 1;";
}else{
	$sql="SELECT notes, unique_id FROM systems_reports WHERE system_unique_id = '$system_unique_id' AND system_ver = $system_ver ORDER BY id DESC LIMIT 1;";
}
s($sql);
if(!$resultPrevNotes = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$numResultPrevNotes = $resultPrevNotes->num_rows;
d($numResultPrevNotes);
if($numResultPrevNotes > 0){
	$rowPrevNotes = $resultPrevNotes->fetch_assoc();
	d($rowPrevNotes);
}

$warranty_warn = false;
$contract_warn = false;
$now = new Moment\Moment('now', null, true);
try{
	if($rowSystem['warranty_end_date'] != ''){
		$warranty_end_date = new \Moment\Moment($rowSystem['warranty_end_date'], null, true);
		if($warranty_end_date->isBefore($now)){
			$warranty_warn = true;
		}
	}
}catch(\Moment\MomentException $momentException){
	d($momentException);
}
try{
	if($rowSystem['contract_end_date'] != ''){
		$contract_end_date = new Moment\Moment($rowSystem['contract_end_date'], null, true);
		if($contract_end_date->isBefore($now)){
			$contract_warn = true;
		}
	}
}catch(\Moment\MomentException $momentException){
	d($momentException);
}
d($warranty_warn);
d($contract_warn);

$collectInfoArr = array();
$collect = false;
$collectInfo = "";

foreach($rowSystem as $key=>$value){
	switch($key){
		case "address":
			if($value == ""){
				array_push($collectInfoArr,"Systems Address");
				$collect = true;
			}
			break;
		case "city":
			if($value == ""){
				array_push($collectInfoArr,"Systems City");
				$collect = true;
			}
			break;
		case "state":
			if($value == ""){
				array_push($collectInfoArr,"Systems State");
				$collect = true;
			}
			break;
		case "zip":
			if($value == ""){
				array_push($collectInfoArr,"Systems Zip");
				$collect = true;
			}
			break;
		case "phone":
			if($value == ""){
				array_push($collectInfoArr,"Systems Phone Number");
				$collect = true;
			}
			break;
		case "fax":
			if($value == ""){
				array_push($collectInfoArr,"Systems Fax Number");
				$collect = true;
			}
			break;
		case "contact_name":
			if($value == ""){
				array_push($collectInfoArr,"System Contact's Name");
				$collect = true;
			}
			break;
		case "contact_title":
			if($value == ""){
				array_push($collectInfoArr,"System Contact's Title");
				$collect = true;
			}
			break;	
		case "contact_phone":
			if($value == ""){
				array_push($collectInfoArr,"System Contact's Phone");
				$collect = true;
			}
			break;
//		case "site_contact_email":
//			if($value == ""){
//				array_push($collectInfoArr,"System Contact's Email");
//				$collect = true;
//			}
			break;
		case "mgr_name":
			if($value == ""){
				array_push($collectInfoArr,"System Manager's Name");
				$collect = true;
			}
			break;
		case "mgr_title":
			if($value == ""){
				array_push($collectInfoArr,"System Manager's Title");
				$collect = true;
			}
			break;
		case "mgr_phone":
			if($value == ""){
				array_push($collectInfoArr,"System Manager's Phone");
				$collect = true;
			}
			break;
//		case "mgr_email":
//			if($value == ""){
//				array_push($collectInfoArr,"System Manager's Email");
//				$collect = true;
//			}
//			break;
//		case "bill_name":
//			if($value == ""){
//				array_push($collectInfoArr,"Billing Name");
//				$collect = true;
//			}
//			break;
//		case "bill_address":
//			if($value == ""){
//				array_push($collectInfoArr,"Billing Address");
//				$collect = true;
//			}
//			break;
//		case "bill_phone":
//			if($value == ""){
//				array_push($collectInfoArr,"Billing Phone");
//				$collect = true;
//			}
//			break;
//		case "equipment_location":
//			if($value == ""){
//				array_push($collectInfoArr,"Gantry/Magnet Location");
//				$collect = true;
//			}
//			break;
		case "system_serial":
			if($value == ""){
				array_push($collectInfoArr,"Gantry/Magnet Serial Number");
				$collect = true;
			}
			break;
		case "date_installed":
			if($value == ""){
				array_push($collectInfoArr,"Gantry/Magnet Date Installed");
				$collect = true;
			}
			break;
		case "timezone":
			if($value == ""){
				array_push($collectInfoArr,"Facility Timezone");
				$collect = true;
			}
			break;
			
	}
}
$collectInfo = implode("<br>",$collectInfoArr);

$sql="SELECT CONCAT('/', GROUP_CONCAT(word SEPARATOR '|'), '/img') AS words FROM misc_2579_words;";
if(!$result2579Words = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$row2579Words = $result2579Words->fetch_assoc();
d($row2579Words);


?>
<!DOCTYPE html>
<html>
<head>
<?php require_once($head_include);?>
<?php require_once($css_include);?>

<link href="/resources/css/form_report.css" rel="stylesheet" type="text/css" media="screen">

<?php require_once($js_include);?>

<script type="text/javascript" src="/resources/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="/resources/js/jquery-dynamic-form_MODIFIED.js"></script>
<script type="text/javascript">
$(document).ready(function() {
 	<?php 
		if(!$mgt_edit){
			if($collect){
				if(!$_SESSION['mobile_device']){
			?>	
					$.prompt("<h3>Please Collect the following information for the System:<br><br><?php echo $collectInfo; ?><br><br>Then email the collected information to <?php echo $settings->email_support ?> so the system info can be updated.</h3>",
					{
						title: "Missing System Information"	
					});
			<?php	
				}else{
					?>
					alert("Please Collect the following information for the System:\n\n<?php echo $collectInfo; ?>\n\nThen email the collected information to <?php echo $settings->email_support ?> so the system info can be updated.");
					<?php
				}
			}
		}else{
			if(strtolower($rowReport['invoiced']) == 'y'){
				if(!$_SESSION['mobile_device']){
					?>
					$.prompt("<h3>This report has been invoiced and cannot be edited!</h3>",
					{
						title: "Caution"	
					});
					//alert("This report has been invoiced and cannot be edited!");
					window.location = "<?php echo $refering_uri; ?>"
					<?php			
				}else{
					?>
					alert("This report has been invoiced and cannot be edited!");
					<?php	
				}
			}else{
				if(!$_SESSION['mobile_device']){
					?>
					$.prompt("<h3>You are attempting to edit a closed report.<br>This will generate a new service report email to the customer.<br>Proceed?</h3>",{
						title: "Caution",
						buttons: { Yes: 1, No: -1 },
						focus: 1,
						submit:function(e,v,m,f){ 
							if(v < 1){
								$.prompt.close();
								window.location = "<?php echo $refering_uri; ?>";
							}else{
								$.prompt.close();	
							}
							e.preventDefault();
						}
					});
					<?php
				}else{
					?>
					if(!confirm("You are attempting to edit a closed report.<br>This will generate a new service report email to the customer.<br>Proceed?")){
						window.location = "<?php echo $refering_uri; ?>";
					}
					<?php	
				}
			}
		}
	?>
	$(".iframeUpload").fancybox({
			'type'			: 'iframe',
			'height'		: 600,
			'fitToView'		: true,
			'maxWidth'		: 900,
			'maxHeight'		: 600,
			'autoSize'		: false,
			'closeBtn'		: true,
			'margin'		: [5,5,5,5]
	});

	
	var notesTableDT = $('#notesTable')
	.on('xhr.dt', function ( e, settings, json, xhr ) {
        //console.log( json );
		if(json['data'].length == 0){
			$("#journalDiv").hide();	
		}else{
			$("#journalDiv").show();	
		}
    })
	.dataTable({
		"bJQueryUI": true,
		"searching": false,
		"lengthChange": false,
		"sPaginationType": "full_numbers",
		"aaSorting": [],
		"ajax": {
			'type': 'POST',
			'url': 'systems_service_journal.php',
			'data': {
			   get: 'true',
			   unique_id: '<?php echo $unique_id; ?>'
			}
		}
	});

});
</script>
<script type="text/javascript">
var num = "";
var hoursdata = [];
var partsdata = [];
<?php 
if($edit){
    $sql="SELECT * FROM systems_hours WHERE unique_id = '".$unique_id."' ORDER BY `date` ASC;";
	if(!$resultHours = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$numberHours = $resultHours->num_rows;
	if(intval($numberHours) > 0){
		$h = 1;
		echo "var hoursdata = [\n";
		while($rowHours = $resultHours->fetch_assoc())
		{
			echo "\t{\"hours_date\":\"".date(phpdispfd,strtotime($rowHours['date']))."\",\"hours_wreg\":\"".$rowHours['reg_labor']."\",\"hours_wot\":\"".$rowHours['ot_labor']."\",\"hours_treg\":\"".$rowHours['reg_travel']."\",\"hours_tot\":\"".$rowHours['ot_travel']."\",\"hours_phone\":\"".$rowHours['phone']."\"}";
			if($h < $numberHours){echo ",\n";}else{	echo "\n];\n";}
			$h++;
		}
	}
	
	$sql="SELECT * FROM systems_parts WHERE unique_id = '".$unique_id."';";
	if(!$resultParts = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$numberParts = $resultParts->num_rows;
	if(intval($numberParts) > 0){
		$p = 1;
		echo "var partsdata = [\n";
		while($rowParts = $resultParts->fetch_assoc())
		{
			
			echo "\t{\"parts_number\":\"".addslashes(htmlspecialchars_decode($rowParts['number']))."\",\"parts_serial\":\"".addslashes(htmlspecialchars_decode($rowParts['serial']))."\",\"parts_description\":\"".addslashes(htmlspecialchars_decode($rowParts['description']))."\",\"parts_quanity\":\"".$rowParts['qty']."\"}";
			if($p < $numberParts){echo ",\n";}else{	echo "\n];\n";}
			$p++;
		}
	}
} 

if($debug){ ?>				
console.log(hoursdata);
console.log(partsdata);
<?php } ?>  

$(document).ready(function(){
	
	$(".button_jquery_save").button({
		icons: {
			primary: "ui-icon-disk"
		}
	});
	
	$(".button_jquery_close").button({
		icons: {
			primary: "ui-icon-pause"
		}
	});
	
	$(".button_jquery_final").button({
		icons: {
			primary: "ui-icon-check"
		}
	});
	
	$(".button_jquery_new").button({
		icons: {
			primary: "ui-icon-document"
		}
	});	
/////////////////////////////////////////////////////////////////////////////////////	
	$("#hours").dynamicForm(
		"#plusH", 
		"#minusH", 
		{ limit:14,	
		  removeColor:"red",
		  afterAllDone:function(count){
            $('#hours_date'+count).datepicker({
              numberOfMonths: 1,
			showButtonPanel: true,
			changeYear: true,
			duration: "fast"
            });
		  },
		  createColor:"green",
		  data:hoursdata,
		  //formPrefix:"JRD_"
		  normalizeFullForm: false
	});
/////////////////////////////////////////////////////////////////////////////////////	
	$("#parts").dynamicForm(
		"#plusP",
		"#minusP",
		{ limit:10,
		  createColor:"green",
		  removeColor:"red",
		  data:partsdata,
		  normalizeFullForm: false
		});		
/////////////////////////////////////////////////////////////////////////////////////	
//	$('.findme').each(function()
//	{
//	   var currentId = $(this).attr('id');
//	   num = currentId.replace("report_date", "");
//	});
	
	$(function(){
		var current_id = $("input[id^='hours_date']:first").attr('id');
		num = current_id.replace("hours_date", "");
	});

/////////////////////////////////////////////////////////////////////////////////////
	$(".viewMini").fancybox({
		'width'				: '50%',
		'height'			: '75%',
        'autoScale'     	: false,
        'transitionIn'		: 'none',
		'transitionOut'		: 'none',
		'type'				: 'iframe'
	});
/////////////////////////////////////////////////////////////////////////////////////
	$(".hoursConversion").fancybox({
		'width'				: 600,
		'height'			: '75%',
		//'padding'			: '20',
        'autoScale'     	: false,
        'transitionIn'		: 'none',
		'transitionOut'		: 'none',
		'autoDimensions'	: false,
		'centerOnScroll'	: true,
		'type'				: 'iframe'
	});

	$("#saveReportProg").click(function(e) {
		enableDisabled();
		$('#save').attr('checked','checked');
		$.post($('#form').attr('action'), $('#form').serialize(), function(response){
			<?php if(!$_SESSION['mobile_device']){ ?>
				$.prompt("<h3>Report Progress Saved</h3>",{
					title: "Save"
				});
			<?php }else{ ?>
				alert("Report Progress Saved");
			<?php } ?>
			disableEnabled();
		},'json');
		$('#save').removeAttr('checked');
		return false;
	});

/////////////////////////////////////////////////////////////////////////////////////
	//$('#countdownHidden').countdown({until: '4h59m59s', format: 'HMS', 
	//    onTick: autoSave, tickInterval: 300}); 
///////////////////////////////////////////////////////////////////////////////////////
	//function autoSave(periods) { 
	//	$('#save').attr('checked','checked');
	//	$.post($('#form').attr('action'), $('#form').serialize(), function(response){
	//	},'json');
	//	$('#save').removeAttr('checked');
	//	return false;
	//}
/////////////////////////////////////////////////////////////////////////////////////
	$(function() {
		$( "#report_date" ).datepicker({
			numberOfMonths: 1,
			showButtonPanel: true,
			changeYear: false,
			constrainInput: true,
			duration: "fast",
			dateFormat: "<?php echo dpdispfd; ?>"
		});
		$( "#hours_date"+num ).datepicker({
			numberOfMonths: 1,
			showButtonPanel: true,
			changeYear: true,
			constrainInput: true,
			duration: "fast",
			dateFormat: "<?php echo dpdispfd; ?>"
		});
		$( "#pm_date" ).datepicker({
			numberOfMonths: 1,
			showButtonPanel: true,
			constrainInput: true,
			changeYear: true,
			duration: "fast",
			dateFormat: "<?php echo dpdispfd; ?>"
		});
		
	});
/////////////////////////////////////////////////////////////////////////////////////
	$("#edit_special").click(function(e) {
		$("#po_number").removeAttr("disabled");
		$("#contract_overide").removeAttr("disabled");
		$("#invoice_req").removeAttr("disabled");
	});
	
	$(".text_toggle").focus(function() {
		$(this).animate({height: 104}, "normal");
	}).blur(function() {
		$(this).animate({height: 40}, "normal");
	});
	
/////////////////////////////////////////////////////////////////////////////////////
});
/////////////////////////////////////////////////////////////////////////////////////
function enableDisabled(){
	$("#po_number").removeAttr("disabled");
	$("#contract_overide").removeAttr("disabled");
	$("#invoice_req").removeAttr("disabled");
}
/////////////////////////////////////////////////////////////////////////////////////
function disableEnabled(){
	$("#po_number").attr("disabled","disabled");
	$("#contract_overide").attr("disabled","disabled");
	$("#invoice_req").attr("disabled","disabled");
}
/////////////////////////////////////////////////////////////////////////////////////
function pm_date_enable_disable(){
	var pm_val = $("#pm"+" option:selected").text();
	if(pm_val.toLowerCase() == "yes"){
		$("#pm_date").removeAttr("disabled");	
	}else{
		$("#pm_date").val("");
		$("#pm_date").attr("disabled","true");	
	}
}
/////////////////////////////////////////////////////////////////////////////////////
function submitcheck($finish, $delete){
	ids = [];
	errors = [];
	var datere = /^20(1|2)\d-((1[0-2])|(0[1-9]))-(([0-2][0-9])|3(0|1))$/; //date yyyy-mm-dd
	var dtre = /^\d{1,10}(\.\d{1,10})?$/; //downtime 00000.00000  .00000 optional
	
	var first_hours_date = $("input[id^='hours_date']:first").attr('id');
	var first_hours_wreg = $("input[id^='hours_wreg']:first").attr('id');

	var status_mismatch = false;
	var status_get_cur = get_sys_cur_status();
	console.log("status_get_cur="+status_get_cur);
	var status_real = $("#system_status option[value='"+status_get_cur+"']").text();
	console.log("status_real="+status_real);
	if(status_get_cur !== $("#system_status").val()){
		status_mismatch = true;
	}

	if($finish == true){
		if($('#report_date').val()==""){ids.push("#report_date"); errors.push("Report Date");}
		var inp_val = $('#report_date').val();
		if(!datere.test(inp_val)){ids.push("#report_date"); errors.push("Report Date Invalid");}	
		if($('#fbc').val()==""){ids.push("#fbc_chosen"); errors.push("Customer Complaint");}	
		
		<?php if($mgt_edit){ ?>
		if($('#edit_notes').val()==""){ids.push("#edit_notes"); errors.push("Reason for editing/deleting this report is required");}
		<?php } ?>
		if(document.getElementById("down_time").value!=""){
			document.getElementById("down_time").value = parseFloat(document.getElementById("down_time").value);
		}
		if($('#down_time').val()==""){ids.push("#down_time"); errors.push("Hours Down");}
		var inp_val = $('#down_time').val();
		if(!dtre.test(inp_val)){ids.push("#down_time"); errors.push("Hours Down Invalid");}
		
		if($('#engineer').val()==""){ids.push("#engineer"); errors.push("Service Engineer");}
		if(document.getElementById("srDataDivCT").style.display != "none"){
			if($('#slice_mas').val()==""){ids.push("#slice_mas"); errors.push("Slice/mAs Count");}
			<?php if(strtolower($rowEquipment['mfg']) == 'siemens'){ ?>
				if($('#sys_scn_secs').val()==""){ids.push("#sys_scn_secs"); errors.push("Siemens System Scan Seconds");}
				if($('#tube_scn_secs').val()==""){ids.push("#tube_scn_secs"); errors.push("Siemens Tube Scan Seconds");}
			<?php } ?>
		}
		if(document.getElementById("srDataDivMR").style.display != "none"){
			if($('#helium').val()==""){ids.push("#helium"); errors.push("Helium Level");}
			if($('#pressure').val()==""){ids.push("#pressure"); errors.push("Vessel Pressure");}	
		}
		if($('#complaint').val()==""){ids.push('#complaint'); errors.push("Customer Complaint");}
		if($('#prob_found').val()==""){ids.push('#prob_found'); errors.push("Problem Found");}
		if($('#service').val()==""){ids.push("#service"); errors.push("Service Performed");}
		if($("input[id^='hours_date']:first").val()==""){ids.push(first_hours_date); errors.push("Hours Worked");}
		if($("input[id^='hours_wreg']:first").val()==""){ids.push(first_hours_wreg); errors.push("Hours Worked");}
		if(document.getElementById("pm").value == "Y"){
			if($('#pm_date').val()==""){ids.push("#pm_date"); errors.push("The date of the PM");}
			var inp_val = $('#pm_date').val();
			if(!datere.test(inp_val)){ids.push("#pm_date"); errors.push("PM Date Invalid");}
		}
		
		var z = 0;
		$("#srHoursTable tr[id^=hours]").each(function(index, element) {
			var sum = 0;
			
			sum = Number($("#hours_wreg" + z).val()) + Number($("#hours_wot" + z).val()) + Number($("#hours_treg" + z).val()) + Number($("#hours_tot" + z).val());
			if(sum > 24){
				ids.push("#hours_date"+z);
				ids.push("#hours_wreg"+z);
				ids.push("#hours_wot"+z);
				ids.push("#hours_treg"+z);
				ids.push("#hours_tot"+z);
				errors.push("Total hours greater than 24 for " + $("#hours_date" + z).val());
			}
			
			var inp_val = $("#hours_date" + z).val();
			if(!datere.test(inp_val)){ids.push("#hours_date"+z); errors.push("Invalid format for hours " + $("#hours_date" + z).val());}
			
			z++;
		});



		console.log("ids: " + ids);
		console.log("errors: " + errors);
		showErrors(ids,errors);
		
		if(ids.length <= 0){

			<?php if($rowSystem['modality'] == 1){ ?>
			console.log('Running 2579 check');
			var check2579_return = check_2579();
			<?php }else{ ?>
			check2579_return = false;
			<?php } ?>
			var elt = document.getElementById("system_status");
			var final_states = {
				state0: {
					html:"<h3><span style=\"color:#F00\">Finalizing the report with the system status of \""+elt.options[elt.selectedIndex].text+"\".<br>"+
					"The system status will be reflected on the customer dashboard.</span><br><br>"+
					"<h3>Finalize this report?</h3>",
					buttons: { Yes: 1, No: -1 },
					focus: 1,
					submit:function(e,v,m,f){
						e.preventDefault();
						$("#errors").hide();
						if(v == 1){
							$.prompt.close();
							document.getElementById("finalize").checked = true;
							enableDisabled();
							document.forms["form"].submit();
						}else{
							$.prompt.close();
						}
					}
				},
				state1: {
					html: "<h3><span style=\"color:#F00\">You may need to fill out a FDA 2579 form for this service event!</span>",
					submit:function(e,v,m,f){
						e.preventDefault();
						$.prompt.goToState('state0');
					}
				},
				state4: {
					html:"<h3><span style=\"color:#F00\">This report's system status of \""+elt.options[elt.selectedIndex].text+"\" does not match the current systems status of \""+status_real+"\"!</span><br><br>"+
					"Set the system status to \""+elt.options[elt.selectedIndex].text+"\"?</h3>",
					buttons: { Yes: 1, No: -1 },
					focus: 1,
					submit:function(e,v,m,f){
						e.preventDefault();
						if(v == 1){
							$.prompt.goToState('state1');
						}else{
							$.prompt.close();
						}
					}
				},
				state5: {
					html:"<h3><span style=\"color:#F00\">This report's system status of \""+elt.options[elt.selectedIndex].text+"\" does not match the current systems status of \""+status_real+"\"!</span><br><br>"+
					"Set the system status to \""+elt.options[elt.selectedIndex].text+"\"?</h3>",
					buttons: { Yes: 1, No: -1 },
					focus: 1,
					submit:function(e,v,m,f){
						e.preventDefault();
						if(v == 1){
							$.prompt.goToState('state0');
						}else{
							$.prompt.close();
						}
					}
				}
			};

			<?php if(!$_SESSION['mobile_device']){ ?>
				var istateA = 'state1';
				var istateB = 'state0';
				if(status_mismatch){
					istateA = 'state4';
					istateB = 'state5';
				}


				if(check2579_return != false){
					$.prompt(final_states,{
						initialState: istateA
					});
				}else{
					$.prompt(final_states,{
						initialState: istateB
					});
				}

			<?php }else{ ?>
				if(check2579_return != false){
					alert("You may need to fill out a FDA 2579 form for this service event!");
				}
				if(confirm("The current system status is \""+elt.options[elt.selectedIndex].text+"\"\nThe system status will be reflected on the customer dashboard.\nFinalize this report?")){
					document.getElementById("finalize").checked = true;
					enableDisabled();
					document.forms["form"].submit();
				}
			<?php } ?>

			
		}
	}else{  //not finalize
		ids = [];
		errors = [];
		document.getElementById("down_time").value = parseFloat(document.getElementById("down_time").value);
		if($('#engineer').val()==""){ids.push("#engineer"); errors.push("Service Engineer");}
		if($('#complaint').val()==""){ids.push("#complaint"); errors.push("Customer Complaint");}
		if($('#prob_found').val()==""){ids.push('#prob_found'); errors.push("Problem Found");}

		console.log("ids: " + ids);
		console.log("errors: " + errors);
		showErrors(ids,errors);
		
		if(ids.length <= 0){
			enableDisabled();
			var elt = document.getElementById("system_status");
			document.getElementById("finalize").checked = false;
			<?php if(!$_SESSION['mobile_device']){ ?>
				var save_states = {
					state0: {
						html:"<h3><span style=\"color:#F00\">Saving the report with the system status of \""+elt.options[elt.selectedIndex].text+"\".<br>"+
						"The system status will be reflected on the customer dashboard.</span><br><br>"+
						"<h3>Save this report for later?</h3>",
						buttons: { Yes: 1, No: -1 },
						focus: 1,
						submit:function(e,v,m,f){
							e.preventDefault();
							$("#errors").hide();
							if(v == 1){
								$.prompt.close();
								document.forms["form"].submit();
							}else{
								$.prompt.close();
							}
						}
					},
					state1: {
						html:"<h3><span style=\"color:#F00\">This report's system status of \""+elt.options[elt.selectedIndex].text+"\" does not match the current systems status of \""+status_real+"\"!</span><br><br>"+
						"Set the system status to \""+elt.options[elt.selectedIndex].text+"\"?</h3>",
						buttons: { Yes: 1, No: -1 },
						focus: 1,
						submit:function(e,v,m,f){
							e.preventDefault();
							$("#errors").hide();
							if(v == 1){
								$.prompt.goToState('state0');
							}else{
								$.prompt.close();
							}
						}
					}
				};

				if(status_mismatch) {
					$.prompt(save_states, {
						initialState: 'state1'
					});
				}else{
					$.prompt(save_states, {
						initialState: 'state0'
					});
				}

			<?php }else{ ?>
				$("#errors").hide();
				if(status_mismatch){
					if(confirm("This report's system status of \""+elt.options[elt.selectedIndex].text+"\" does not match the current systems status of \""+status_real+"\"! \nSet the system status to \""+elt.options[elt.selectedIndex].text+"\"?)){
						if(confirm("The current system status is \""+elt.options[elt.selectedIndex].text+"\"\nSave this report for later?")){
							document.forms["form"].submit();
						}
					}
				}else {
					if(confirm("The current system status is \""+elt.options[elt.selectedIndex].text+"\"\nSave this report for later?")){
						document.forms["form"].submit();
					}
				}


			<?php } ?>
		}	
	}
}
/////////////////////////////////////////////////////////////////////////////////////
function showErrors(ids,errors){
	//thin solid #2C3594
	$("input, select, .chosen-container").each(function(index, element) {
		$(this).animate({
			borderColor: "#2C3594",
			boxShadow: 'none'
		});
	});
	
	$.each(ids,function(index,value){
		$(value).animate({
			borderColor: "#cc0000",
			boxShadow: '0 0 5px 3px rgba(255,0,0,0.4)'
		});
	});
	$("#errors > span").html("");
	$.each(errors,function(index, value){
		$("#errors > span").append(value + "<br>");
	});
	$("#errors").show('slow');
	$(document).scrollTop(0);
}
/////////////////////////////////////////////////////////////////////////////////////
function iframeNoteClosed(){
	$('#notesTable').DataTable().ajax.reload();

	if(parseInt(get_sys_cur_status()) > 0){
		$("#system_status").val(parseInt(data));
		$("#system_status").trigger("chosen:updated");
	}
}

function get_sys_cur_status() {
	var rtn = 0;
	$.ajax({
		type: 'POST',
		url: 'systems_status_get.php',
		data: { unique_id: '<?php echo $rowSystem['unique_id'] ?>' },
		async: false,
		success:function(data){
			if(parseInt(data) == 0){
				console.log("ERROR: data = 0");
				rtn = 0;
			}
			console.log("get_sys_cur_status data: "+data);
			rtn = data;

		}
	});
	return rtn;
}
/////////////////////////////////////////////////////////////////////////////////////
function check_2579(){
	var re = <?php echo $row2579Words['words']; ?>;
	var matches = $("#service").val().match(re);

	$("input[name*='parts_description']").each(function () {
		var re = <?php echo $row2579Words['words']; ?>;
		var m = $(this).val().match(re);
		if(!is_null(m)){
			if(is_null(matches)){
				matches = []; //if matches is null set it to empty array
			}
			matches.push(m[0]);
		}
	});
	if( is_null(matches)){
		console.log('No 2579 words');
		return false;
	}else {
		console.log('Found 2579 words');
		console.log(matches);
		return matches;
	}
}
</script>

</head>
<body>
<?php require_once($header_include); ?>
<div id="OIReportContent"> 
<div id="stylized" class="myform">
<form id="form" name="form" method="post" enctype='multipart/form-data' action="report_save.php">
<div id="srHeaderDiv">
	<?php 
	if($mgt_edit){ 
		echo "<br /><h1 style=\"color:red\">Edit Closed Report</h1>";
	}else{
		if($edit){
			echo "<h1>Edit Report</h1>";
		}else{
			echo "<h1>New Report</h1>";
		}
	}
	?>
	<?php if(strtolower($rowSystem['credit_hold']) == "y"){ echo "<br /><h1 style=\"color:red\">This System is on Credit Hold</h1>";} ?>
	<?php if(strtolower($rowSystem['pre_paid']) == "y"){ echo "<br /><h1 style=\"color:red\">This System is Pre-Paid Service only</h1>";} ?>
	<?php if($warranty_warn){ ?><br><h1 class="credit_hold">Warranty Expired</h1><?php } ?>
	<?php if($contract_warn){ ?><br><h1 class="credit_hold">Contract Expired</h1><?php } ?>
	<br />
</div>
<div id="main"><!-- do not remove -->
<div id="errors" style="text-align:center;display:none; margin-bottom:25px; font-size:18px;">
	<h2 style="margin:0px; padding:0px; font-size:24px;">Errors to fix</h2>
	<span style="color:#F00">
	</span>
</div>
<div id="srInfoDiv">
    <table id="srInfoTable" class="srTable">
        <tr>
          <td class="rowLabel">System&nbsp;ID: </td>
          <td class="rowData"><a href="systems_view.php?ver_unique_id=<?php echo $rowSystem['ver_unique_id']; ?>" target="_blank"><?php echo $rowSystem['system_id']; ?></a></td>
          <td class="rowLabel">System Nickname: </td>
          <td class="rowData"><?php echo $rowSystem['nickname']; ?></td>
        </tr>
        <tr>
          <td class="rowLabel">Address: </td>
          <td colspan="4" class="rowData"><?php echo $rowSystem['address'] ."&nbsp;&nbsp;&nbsp;". $rowSystem['city'] .", ". $rowSystem['state'] ."  ". $rowSystem['zip'];?></td>
        </tr>
        <tr>
          <td class="rowLabel">Serial&nbsp;Number: </td>
          <td class="rowData"><?php echo $rowSystem['system_serial']; ?></td>
          <td class="rowLabel">System: </td>
          <td class="rowData"><?php echo $rowEquipment['name']; ?></td>
        </tr>
        <tr>
          <td class="rowLabel">Contract&nbsp;Type: </td>
          <td class="rowData"><?php echo $rowContract['type']; ?></td>
          <td class="rowLabel">Assigned&nbsp;Engineer: </td>
          <td class="rowData"><?php echo $rowAssignedEng['name']; ?></td>
        </tr>
        <tr>
          <td class="rowLabel">Service&nbsp;Request&nbsp;Num: </td>
          <td class="rowData"><?php echo $rowRequest['request_num']; ?></td>
          <td class="rowLabel">Primary&nbsp;Engineer: </td>
          <td class="rowData"><?php echo $rowEngPri['name']; ?></td>
        </tr>
		<tr>
          <td class="rowLabel">Initial&nbsp;Call&nbsp;Date/Time: </td>
          <td class="rowData"><?php echo date(phpdispfdt,strtotime($rowRequest['initial_call_date']) + $php_utils->tz_offset_sec($rowSystem['timezone'])); ?></td>
          <td class="rowLabel">Response&nbsp;Date/Time: </td>
          <td class="rowData"><?php echo date(phpdispfdt,strtotime($rowRequest['response_date']) + $php_utils->tz_offset_sec($rowSystem['timezone'])); ?></td>
        </tr>
		<tr>
		  <td class="rowLabel">Dispatcher: </td>
		  <td class="rowData"><?php echo $rowRequest['dispatch_name']; ?></td>
		  <td class="rowLabel">Onsite&nbsp;Date/Time: </td>
		  <td class="rowData"><?php echo date(phpdispfdt,strtotime($rowRequest['onsite_date'])); ?></td>
		</tr>
    </table>
</div>

<div id="srNotesDiv" <?php if($rowSystem['notes'] ==""){echo "style=\"display:none\"";}?>>
	<table id="srNotesTable" class="srTable" >
        <tr>
          <th class="rowLabel">System Notes</th>
        </tr>
        <tr>
          <td class="rowData"><textarea class="text_toggle" style="height:40px" id="system_notes" name="system_notes" maxlength="2000" readonly><?php echo $rowSystem['notes']; ?></textarea></td>
        </tr>
    </table>
</div>

<div id="srNotesDiv" <?php if($rowFacility['notes'] ==""){echo "style=\"display:none\"";}?>>
	<table id="srNotesTable" class="srTable" >
        <tr>
          <th class="rowLabel">Facility Notes</th>
        </tr>
        <tr>
          <td class="rowData"><textarea class="text_toggle" style="height:40px" id="facility_notes" name="facility_notes" maxlength="2000" readonly><?php echo $rowFacility['notes']; ?></textarea></td>
        </tr>
    </table>
</div>

<div id="srNotesDiv" <?php if($rowCustomer['notes'] ==""){echo "style=\"display:none\"";}?>>
	<table id="srNotesTable" class="srTable" >
        <tr>
          <th class="rowLabel">Customer Notes</th>
        </tr>
        <tr>
          <td class="rowData"><textarea class="text_toggle" style="height:40px" id="customer_notes" name="customer_notes" maxlength="2000" readonly><?php echo $rowCustomer['notes']; ?></textarea></td>
        </tr>
    </table>
</div>

<div id="srDataDiv">
    <table id="srDataTable" class="srTable">
        <tr>
			<td class="rowLabel">Purchase Order:</td>
			<td class="rowData"><input type="text" id="po_number" name="po_number" class="tooltip" value="<?php if($edit){echo $rowReport['po_number'];}else{echo $rowRequest['po_num'];} ?>"  /></td>
			<td class="rowLabel">&nbsp;</td>
			<td class="rowData">&nbsp;</td>
        </tr>
		<tr>
			<td class="rowLabel">Invoice Required:</td>
			<td class="rowData"><select class="chooser tooltip" name="invoice_req" id="invoice_req" >
			  <option <?php if($edit){if(strtolower($rowReport['invoice_required'])=="y"){echo "selected";}}else{if(strtolower($rowRequest['invoice_required'])=="y"){echo "selected";}} ?> value="Y">Yes</option>
              <option <?php if($edit){if(strtolower($rowReport['invoice_required'])!="y"){echo "selected";}}else{if(strtolower($rowRequest['invoice_required'])!="y"){echo "selected";}} ?> value="N">No</option>
			  </select></td> 
			<td class="rowLabel">Contract Type Override:</td>
			<td class="rowData"><select class="chooser tooltip" name="contract_override" id="contract_overide" >
			  <option value=""></option>
			  <?php
				while($rowContracts = $resultContracts->fetch_assoc())
				{
					if($edit){
						if($rowReport['contract_override']==$rowContracts['id']){
							echo "<option selected value='" . $rowContracts['id'] . "'>" . $rowContracts['type'] . "</option>\n";
						}else{
							echo "<option value='" . $rowContracts['id'] . "'>" . $rowContracts['type'] . "</option>\n";
						}
					}else{
						if($rowRequest['contract_override']==$rowContracts['id']){
							echo "<option selected value='" . $rowContracts['id'] . "'>" . $rowContracts['type'] . "</option>\n";
						}else{
							echo "<option value='" . $rowContracts['id'] . "'>" . $rowContracts['type'] . "</option>\n";
						}
					}
				}
			  ?>
			  </select></td>
		</tr>
    </table>
</div>


<div id="srDataDiv">
    <table id="srDataTable" class="srTable">
        <tr>
          <td class="rowLabel">Report Date:</td>
          <td class="rowData"><input type="text" id="report_date" name="report_date" class="findme tooltip" value="<?php if($edit){echo date(phpdispfd,strtotime($rowReport['date']));}else{echo date(phpdispfd,$t);} ?>" /></td>
          <td class="rowLabel">Service Engineer:</td>
		  <td class="rowData"><select class="chooser tooltip" name="engineer" id="engineer" >
		  <option value=""></option>
          <?php
		  	if($_SESSION['group'] == 1){
				while($rowEngineer = $resultEngineer->fetch_assoc())
				{
					if($edit){
						if(strtolower($rowEngineer['uid']) == strtolower($rowReport['engineer'])){
							echo "<option selected value='" . $rowEngineer['uid'] . "'>" . $rowEngineer['name'] . "</option>\n";
						}else{
							echo "<option value='" . $rowEngineer['uid'] . "'>" . $rowEngineer['name'] . "</option>\n";
						}	
					}else{
						if(strtolower($rowEngineer['name']) == strtolower($rowUser['name'])){
							echo "<option selected value='" . $rowEngineer['uid'] . "'>" . $rowEngineer['name'] . "</option>\n";
						}else{
							echo "<option value='" . $rowEngineer['uid'] . "'>" . $rowEngineer['name'] . "</option>\n";
						}
					}
				}
			}else{
				echo "<option selected value='" . $rowRequest['engineer'] . "'>" . $rowRequest['engineer_name'] . "</option>\n";	
			}
          ?>
		  </select></td>
        </tr>
        <tr>
          
          
        </tr>
        <tr>
			<td class="rowLabel">System Status:</td>
			  <td class="rowData"><select class="chooser tooltip" name="system_status" id="system_status" >
			  <option value=""></option>
			  <?php
				while($rowStatus = $resultStatus->fetch_assoc())
				{
					if($edit){
						if($rowReport['system_status']==$rowStatus['id']){
							echo "<option selected value='" . $rowStatus['id'] . "'>" . $rowStatus['status'] . "</option>\n";
						}else{
							echo "<option value='" . $rowStatus['id'] . "'>" . $rowStatus['status'] . "</option>\n";
						}
					}else{
						if($rowSystem['status']==$rowStatus['id']){
							echo "<option selected value='" . $rowStatus['id'] . "'>" . $rowStatus['status'] . "</option>\n";
						}else{
							echo "<option value='" . $rowStatus['id'] . "'>" . $rowStatus['status'] . "</option>\n";
						}
					}
				}
			  ?>
			  </select></td> 
          
          <td class="rowLabel">Hours System was down:</td>
          <td class="rowData"><input type="text" id="down_time" name="down_time" class="tooltip" value="<?php if($edit){if($rowReport['downtime'] != ""){echo $rowReport['downtime'];}}?>"  /></td>
        </tr>
        <tr>
          <td class="rowLabel">Preventive Maintenance:</td>
          <td class="rowData"><select class="chooser tooltip" name="pm" id="pm"  onChange="pm_date_enable_disable();">
          <option <?php if($edit){if(strtolower($rowReport['pm'])=="y"){echo "selected";}}else{if(strtolower($rowRequest['pm'])=="y"){echo "selected";}} ?> value="Y">Yes</option>
          <option <?php if($edit){if(strtolower($rowReport['pm'])!="y"){echo "selected";}}else{if(strtolower($rowRequest['pm'])!="y"){echo "selected";}} ?> value="N">No</option>
          </select></td>
		  <td class="rowLabel">PM Date:</td>
          <td class="rowData"><input <?php if($edit){if(strtolower($rowReport['pm'])!="y"){echo "disabled";}}else{if(strtolower($rowRequest['pm'])!="y"){echo "disabled";}} ?> type="text" id="pm_date" name="pm_date" class="tooltip" value="<?php if($edit and strlen($rowReport['pm_date']) > 2){echo date(phpdispfd,strtotime($rowReport['pm_date']));} ?>" /></td>
        </tr>
    </table>
</div>

<div id="srDataDivCT" <?php if($rowEquipment['type'] != "CT"){ echo" style='display:none'";} ?> >
    <table id="srDataTable">
		<?php if(strtolower($rowEquipment['mfg']) != 'siemens'){ ?>
        <tr>
          <td class="rowLabel">Slice/mAs Count:</td>
          <td class="rowData"><input type="number" id="slice_mas" name="slice_mas" class="tooltip" <?php if($edit){echo "value=\"".$rowReport['slice_mas_count']."\"";}?>  /></td>
          <td class="rowLabel">Gantry Revolutions:</td>
          <td class="rowData"><input type="number" id="gantry_rev" name="gantry_rev" class="tooltip" <?php if($edit){echo "value=\"".$rowReport['gantry_rev']."\"";}?>  /></td>
        </tr>
		<?php }else{ ?>
		<tr>
          <td class="rowLabel">Sys Scan Seconds:</td>
          <td class="rowData"><input type="number" id="sys_scn_secs" name="sys_scn_secs" class="tooltip" <?php if($edit){echo "value=\"".$rowReport['sys_scn_secs']."\"";}?>  /></td>
          <td class="rowLabel">Tube Scan Seconds:</td>
          <td class="rowData"><input type="number" id="tube_scn_secs" name="tube_scn_secs" class="tooltip" <?php if($edit){echo "value=\"".$rowReport['tube_scn_secs']."\"";}?>  /></td>
		</tr>
		<?php } ?>
    </table>
</div>

<div id="srDataDivMR" <?php if($rowEquipment['type'] != "MR"){ echo" style='display:none'";} ?>>
    <table id="srDataTable">
        <tr>
          <td class="rowLabel">Helium Level:</td>
          <td class="rowData"><input type="text" id="helium" name="helium" class="tooltip" <?php if($edit){echo "value=\"".$rowReport['helium_level']."\"";}?> /></td>
          <td class="rowLabel">Vessel Pressure:</td>
          <td class="rowData"><input type="text" id="pressure" name="pressure" class="tooltip" <?php if($edit){echo "value=\"".$rowReport['vessel_pressure']."\"";}?> /></td>
        </tr>
        <tr>
          <td class="rowLabel">Compressor Pressure:</td>
          <td class="rowData"><input type="text" id="compressor_pressure" name="compressor_pressure" class="tooltip" <?php if($edit){echo "value=\"".$rowReport['compressor_pressure']."\"";}?> /></td>
          <td class="rowLabel">Compressor Hours:</td>
          <td class="rowData"><input type="text" id="compressor_hours" name="compressor_hours" class="tooltip" <?php if($edit){echo "value=\"".$rowReport['compressor_hours']."\"";}?> /></td>
        </tr>
        <tr>
          <td class="rowLabel">Recon RUO:</td>
          <td class="rowData"><input type="text" id="recon_ruo" name="recon_ruo" class="tooltip" <?php if($edit){echo "value=\"".$rowReport['recon_ruo']."\"";}?> /></td>
          <td class="rowLabel">Coldhead RUO:</td>
          <td class="rowData"><input type="text" id="coldhead_ruo" name="coldhead_ruo" class="tooltip" <?php if($edit){echo "value=\"".$rowReport['cold_ruo']."\"";}?> /></td>
        </tr>
    </table>
</div>

<div id="srPhoneFixDiv">
    <table id="srPhoneFixTable">
    	<tr>
			<td class="rowLabel">Phone Fix:</td>
			<td class="rowData"><select class="chooser tooltip" name="phone_fix" id="phone_fix" >
			<option <?php if($edit){if(strtolower($rowReport['phone_fix'])!="y"){echo "selected";}} ?> value="N">No</option>
			<option <?php if($edit){if(strtolower($rowReport['phone_fix'])=="y"){echo "selected";}} ?> value="Y">Yes</option>
			</select></td>
			<td class="rowLabel">FBC Necessary:</td>
          	<td class="rowData"><select class="chooser tooltip" id="fbc" name="fbc" >
			<option value="N" <?php if($edit){if(strtolower($rowReport['fbc'])=='n'){echo "selected";}} ?>>No</option>
			<option value="Y" <?php if($edit){if(strtolower($rowReport['fbc'])=='y'){echo "selected";}} ?>>Yes</option>
			</select></td>
		</tr>
    </table>
</div>

<div id="srComplaintDiv">
    <table id="srComplaintTable" class="srTable" >
        <tr>
          <th class="rowLabel">Customer Complaint<span id="complaint_lbl" class="tooltip"></span></th>
        </tr>
        <tr>
          <td class="rowData"><textarea class="text_toggle" style="height:40px" id="complaint" name="complaint" maxlength="2000" ><?php if($edit){echo addslashes(htmlspecialchars_decode($rowReport['complaint']));}else{echo addslashes(htmlspecialchars_decode($rowRequest['problem_reported']));} ?></textarea></td>
        </tr>
    </table>
</div>

<div id="srNotesDiv" <?php if($numResultPrevNotes<=0){echo "style=\"display:none\"";}else{if($rowPrevNotes['notes'] == ""){echo "style=\"display:none\"";}} ?>>
    <table id="srNotesTable" class="srTable"  >
        <tr>
          <th class="rowLabel">Service Reminder<span id="prev_notes_lbl" class="tooltip"></span></th>
        </tr>
        <tr>
          <td class="rowData"><textarea class="text_toggle" style="height:40px" id="prev_notes" name="prev_notes" maxlength="2000" readonly><?php if($numResultPrevNotes > 0){echo addslashes(htmlspecialchars_decode($rowPrevNotes['notes']));}?></textarea></td>
        </tr>
    </table>
</div>

<div id="srServiceDiv">
    <table id="srServiceTable" class="srTable"  >
        <tr>
          <th class="rowLabel">Service Performed<span id="service_lbl" class="tooltip"></span></th>
        </tr>
        <tr>
          <td class="rowData"><textarea id="service" name="service" maxlength="60000" ><?php if($edit){echo addslashes(htmlspecialchars_decode($rowReport['service']));}?></textarea></td>
        </tr>
    </table>
</div>

<div id="srHoursDiv">
	<table id="srHoursTable" class="srTable">
    <tr>
        <th scope="col">Date<span id="hours_date_lbl" class="tooltip"></span></th>
        <th colspan="2" scope="col">Hours Onsite<span id="hours_onsite_lbl" class="tooltip"></span></th>
        <th colspan="2" scope="col">Hours Traveled<span id="hours_travel_lbl" class="tooltip"></span></th>
	    <th colspan="1" scope="col">Phone Hours<span id="hours_phone_lbl" class="tooltip"></span></th>
    </tr>
    <tr>
    	<td class="rowLabel"></td>
        <td class="rowLabel">Reg</td>
        <td class="rowLabel">OT</td>
        <td class="rowLabel">Reg</td>
        <td class="rowLabel">OT</td>
	    <td class="rowLabel">&nbsp;</td>
    </tr>
    <tr id="hours">
        <td class="rowData"><input type="text" id="hours_date" name="hours_date" /></td>
        <td class="rowData"><input type="text" id="hours_wreg" name="hours_wreg" /></td>
        <td class="rowData"><input type="text" id="hours_wot" name="hours_wot" /></td>
        <td class="rowData"><input type="text" id="hours_treg" name="hours_treg" /></td>
        <td class="rowData"><input type="text" id="hours_tot" name="hours_tot" /></td>
	    <td class="rowData"><input type="text" id="hours_phone" name="hours_phone" /></td>
    </tr>
    <tr>
    	<td><a id="plusH" style="border:none" href=""><img src="/resources/images/green-plus-sign.png" width="30" height="30"></a> <a id="minusH" style="border:none" href=""><img src="/resources/images/red-minus-sign.png" width="30" height="30"></a></td>
		<td colspan="4"><a href="hours_conversion.html" class="hoursConversion">Click here for Hours Conversion Help</a></td>
    </tr>
</table>


</div>

<div id="srPartsDiv">
	<table id="srPartsTable" class="srTable">
		<tr>
			<th scope="col" width="20%">Part Number<span id="parts_number_lbl" class="tooltip"></span></th>
			<th scope="col" width="20%">Serial Number<span id="parts_serial_lbl" class="tooltip"></span></th>
			<th scope="col">Description<span id="parts_description_lbl" class="tooltip"></span></th>
			<th scope="col" width="10%">Quantity<span id="parts_quanity_lbl" class="tooltip"></span></th>
		</tr>
		<tr id="parts">
			<td class="rowData"><input type="text" id="parts_number" name="parts_number" /></td>
			<td class="rowData"><input type="text" id="parts_serial" name="parts_serial" /></td>
			<td class="rowData"><input type="text" id="parts_description" name="parts_description" /></td>
			<td class="rowData"><input type="number" id="parts_quanity" name="parts_quanity" /></td>
		</tr>
		<tr>
			<td colspan="3"><a id="plusP" style="border:none" href=""><img src="/resources/images/green-plus-sign.png" width="30" height="30"></a> <a id="minusP" style="border:none" href=""><img src="/resources/images/red-minus-sign.png" width="30" height="30"></a></td>
		</tr>
	</table>
</div>

<div id="srNotesDiv">
    <table id="srNotesTable" class="srTable"  >
        <tr>
          <th class="rowLabel">Next Service Call Reminder<span id="notes_lbl" class="tooltip"></span></th>
        </tr>
        <tr>
          <td class="rowData"><textarea class="text_toggle" style="height:40px;" id="notes" name="notes" maxlength="2000" ><?php if($edit){echo addslashes(htmlspecialchars_decode($rowReport['notes']));}?></textarea></td>
        </tr>
    </table>
</div>

<?php if($_SESSION['group'] == "1"){?>
<div id="srFilesDiv">
	<table id="srFilesTable" class="srTable">
		<tr>
			<th colspan="2" scope="col" class="rowLabel">Attached Documents<span id="files_lbl" class="tooltip"></span></th>
		</tr>
		<tr>
			<td class="rowData" width="50%"><div class="srBottomBtn"><a href="/resources/kcfinder/kcfinder.php?unique_id=<?php echo $unique_id; ?>&table=systems_reports_files" class="button_jquery_save iframeUpload" id="attachDocs"><?php echo $uploadBtnVal; ?></a></div></td>
			<td class="rowData" style="text-align:left; color:#f79548"><?php echo $num_docs; ?></td>
		</tr>
	</table>
</div>
<?php } ?>
<table id="srFilesTable" class="srTable">
	<tr>
		<th scope="col" colspan="2" class="rowLabel">Service Journal<span id="notesTable_lbl" class="tooltip"></span></th>
	</tr>
	<tr>
		<td class="rowData" width="50%"><a class="iframeNote button_jquery_new" href="/report/common/systems_service_journal.php?new&unique_id=<?php echo $unique_id; ?>">New Journal Entry</a></td>
		<td class="rowData" style="text-align:left; color:#f79548">Internal records only. Customer can not view notes.</td>
	</tr>
	<tr>
		<td colspan="2" width="100%">
			<div id="journalDiv" style="padding-left:13px; padding-right:13px; margin-top:15px;">
			<table width="100%" id="notesTable" >
				<thead>
					<tr>
						<th width="17%">Date</th> 
						<th>Entry</th>
						<th width="10%">Status</th>
						<th width="15%">User</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
			</div>
		</td>
	</tr>
</table>

<?php if($mgt_edit){ ?>
<div id="srNotesDiv">
    <table id="srNotesTable" class="srTable"  >
        <tr>
          <td class="rowLabel" style="color:#F00">Reason For Report Edit</td>
        </tr>
        <tr>
          <td class="rowData"><textarea class="text_toggle edit_notes" style="height:15px;" id="edit_notes" name="edit_notes" maxlength="2000" ><?php echo addslashes(htmlspecialchars_decode($rowReport['report_edited_notes']));?></textarea></td>
        </tr>
    </table>
</div>
<?php } ?>

<div style="display:none">
	<?php if($debug){ ?><input name="debug" id="debug" type="hidden" value="Y" /> <?php } ?>
	<input type="checkbox" id="finalize" name="finalize" />
    <input type="checkbox" id="save" name="save" />
	<input type="checkbox" id="delete" name="delete" />
    <input type="text" id="user_id" name="user_id" value="<?php echo $rowUser['uid']; ?>" />
    <input type="text" id="system_id" name="system_id" value="<?php echo $rowSystem['system_id']; ?>" />
    <input type="text" id="system_unique_id" name="system_unique_id" value="<?php echo $rowSystem['unique_id']; ?>" />
	<input type="text" id="system_ver_unique_id" name="system_ver_unique_id" value="<?php echo $rowSystem['ver_unique_id']; ?>" />
	<input type="text" id="facility_unique_id" name="facility_unique_id" value="<?php echo $rowSystem['facility_unique_id']; ?>" />
    <input type="text" id="system_nickname" name="system_nickname" value="<?php echo $rowSystem['nickname']; ?>" />
    <input type="text" id="engineer_assigned" name="engineer_assigned" value="<?php echo $rowRequest['engineer']; ?>" />
    <input type="text" id="report_id" name="report_id" value="<?php echo $rowRequest['request_num']; ?>" />
    <input type="text" id="unique_id" name="unique_id" value="<?php echo $rowRequest['unique_id']; ?>" />
    <input type="text" id="prev_report_id" name="prev_report_id" value="<?php if($numResultPrevNotes<=0){echo "";}else{echo $resultPrevNotes->fetch_object()->unique_id;}?>" />
	<?php
	if($mgt_edit){
	?>
	<input type="text" id="report_edited" name="report_edited" value="Y" />
	<input type="text" id="orig_user_id" name="orig_user_id" value="<?php echo $rowReport['user_id']; ?>" />
	<?php } ?>
    
</div>
</div>

<!--<div id="countdownHidden" style="display:none"></div>-->
<div id="srFooterDiv">
	<?php if(!$mgt_edit){ ?>
<!--	<div class="srBottomBtn"><a class="button_jquery_save" id="saveReportProg">Save Report Progress</a></div>-->
<!--	<div class="srBottomBtn"><a class="button_jquery_close" onClick="submitcheck(false, false)">Save Report and Close</a></div>-->
<!--	<div class="srBottomBtn"><a class="button_jquery_final" onClick="submitcheck(true, false)">Finalize Report</a></div>-->
	<?php }else{ ?>
<!--	<div class="srBottomBtn"><a class="button_jquery_save" onClick="submitcheck(true, false)">Save Report</a></div>-->
	<!--<div class="srBottomBtn"><a class="button_jquery_save" onClick="submitcheck(true, true)">Delete Report</a></div>-->
	<?php } ?>
</div>

</form>
</div> 
</div>
<?php require_once($footer_include); ?>