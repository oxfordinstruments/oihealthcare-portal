<?php
//Update Completed 11/25/14
//Called by report_new_edit.php
$debug = false;
if(isset($_POST['debug'])){
	$debug = true;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/php_utils.php');
$php_utils = new phpUtils();

$save = false;
$finalize = false;
$main = $_POST;
if(isset($main['finalize'])){$status = "closed"; $newreport = "Y"; $finalize = true;}else{$status = "open"; $newreport="N";}
if(isset($main['save'])){$save = true;}
$system_unique_id = $main['system_unique_id'];
$system_status = $main['system_status'];
$report_id = $main['report_id'];
$unique_id = $main['unique_id'];
if(isset($main['report_edited'])){$edit = true;}else{$edit = false;}
if(isset($main['pm_date']) and strlen($main['pm_date']) > 0 ){
	$pm_date = date(storef,strtotime($main['pm_date']));
}else{
	$pm_date = "";
}
d($pm_date);
dd($_POST);

$hours = $_POST['hours']['hours'];
foreach($hours as $key=>$value){
	$hours[$key]['hours_date'] = date(storef,strtotime($value['hours_date']));
}
d($hours);

//$parts = $_POST['parts']['parts'];
//d($parts);

$parts = array();
if(count($_POST['parts']['parts']) > 0 ){
	foreach($_POST['parts']['parts'] as $part){
		array_push($parts,array('parts_number'=>htmlspecialchars($part['parts_number']), 'parts_serial'=>htmlspecialchars($part['parts_serial']), 'parts_description'=>htmlspecialchars($part['parts_description']), 'parts_quanity'=>$part['parts_quanity']));
	}
}
d($parts);

$downtime = preg_replace("/[^0-9.]/", "", $main['down_time']);
if(!is_numeric($downtime)){
	$downtime = 0;		
}

$fbc = false;
if(strtolower($main['fbc']) == 'y'){
	$fbc = true;	
}

$values = array(
	'report_id'=>$report_id,
	'system_id'=>$main['system_id'],
	'system_unique_id'=>$main['system_unique_id'],
	'system_ver_unique_id'=>$main['system_ver_unique_id'],
	'facility_unique_id'=>$main['facility_unique_id'],
	'status'=>$status,
	'system_nickname'=>addslashes(addslashes($main['system_nickname'])),
	'complaint'=>htmlspecialchars(addslashes($main['complaint'])),
	'service'=>htmlspecialchars(addslashes($main['service'])),
	'notes'=>htmlspecialchars(addslashes($main['notes'])),
	'prev_report_id'=>$main['prev_report_id'],
	'engineer'=>$main['engineer'],
	'engineer_assigned'=>$main['engineer_assigned'],
	'report_date'=>date(storef,strtotime($main['report_date'])),
	'po_number'=>$main['po_number'],									
	'contract_override'=>$main['contract_override'],
	'slice_mas'=>$main['slice_mas'],
	'gantry_rev'=>$main['gantry_rev'],
	'sys_scn_secs'=>$main['sys_scn_secs'],
	'tube_scn_secs'=>$main['tube_scn_secs'],
	'helium'=>$main['helium'],
	'pressure'=>$main['pressure'],		
	'recon_ruo'=>$main['recon_ruo'],
	'coldhead_ruo'=>$main['coldhead_ruo'],
	'compressor_hours'=>$main['compressor_hours'],		
	'compressor_pressure'=>$main['compressor_pressure'],
	'downtime'=>$downtime,
	'pm'=>$main['pm'],
	'pm_date'=>$pm_date,
	'system_status'=>$main['system_status'],		
	'invoice_req'=>$main['invoice_req'],				
	'phone_fix'=>$main['phone_fix'],
	'fbc'=>$main['fbc'],
);

if(!$edit){
		$values['user_id'] = $main['user_id'];
		$values['unique_id'] = $unique_id;
}else{
		$values['orig_user_id'] = $main['orig_user_id'];
		$values['edited'] = 'Y';
		$values['date'] = date(storef,time());
		$values['login'] = $_SESSION['login'];
		$values['edit_notes'] = addslashes($main['edit_notes']);
		$values['unique_id'] = $unique_id;	
}

d($values);

if(!$edit){
	$sql="INSERT INTO systems_reports (`report_id`, `system_id`, `system_unique_id`, `system_ver_unique_id`, `facility_unique_id`, `status`, `system_nickname`, `complaint`, `service`, `notes`, `prev_report_id`, 
	`engineer`, `assigned_engineer`, `date`, `po_number`, `contract_override`, `slice_mas_count`, `gantry_rev`, `sys_scn_secs`, `tube_scn_secs`, `helium_level`, `vessel_pressure`, `recon_ruo`, `cold_ruo`, 
	`compressor_hours`, `compressor_pressure`, `downtime`, `pm`, `pm_date`, `system_status`, `invoice_required`, `phone_fix`, `fbc`, `user_id`, `unique_id`) 
	VALUES ('". implode("', '", $values) . "') 
	ON DUPLICATE KEY UPDATE
	`status` = VALUES(`status`), complaint = VALUES(complaint), service = VALUES(service), notes = VALUES(notes), prev_report_id = VALUES(prev_report_id),
	engineer = VALUES(engineer), assigned_engineer = VALUES(assigned_engineer), `date` = VALUES(`date`), po_number = VALUES(po_number), contract_override = VALUES(contract_override),
	slice_mas_count = VALUES(slice_mas_count), gantry_rev = VALUES(gantry_rev), sys_scn_secs = VALUES(sys_scn_secs), tube_scn_secs = VALUES(tube_scn_secs), helium_level = VALUES(helium_level), vessel_pressure = VALUES(vessel_pressure), recon_ruo = VALUES(recon_ruo),
	cold_ruo = VALUES(cold_ruo), compressor_hours = VALUES(compressor_hours), compressor_pressure = VALUES(compressor_pressure), downtime = VALUES(downtime), pm = VALUES(pm),
	pm_date = VALUES(pm_date), system_status = VALUES(system_status), invoice_required = VALUES(invoice_required), phone_fix = VALUES(phone_fix), fbc = VALUES(fbc), user_id = VALUES(user_id), unique_id = VALUES(unique_id);";
}else{
	$sql="INSERT INTO systems_reports (`report_id`, `system_id`, `system_unique_id`, `system_ver_unique_id`, `facility_unique_id`, `status`, `system_nickname`, `complaint`, `service`, `notes`, `prev_report_id`, 
	`engineer`, `assigned_engineer`, `date`, `po_number`, `contract_override`, `slice_mas_count`, `gantry_rev`, `sys_scn_secs`, `tube_scn_secs`, `helium_level`, `vessel_pressure`, `recon_ruo`, `cold_ruo`, 
	`compressor_hours`, `compressor_pressure`, `downtime`, `pm`, `pm_date`, `system_status`, `invoice_required`, `phone_fix`, `fbc`, `user_id`, `report_edited`, `report_edited_date`, 
	`report_edited_by`, `report_edited_notes`, `unique_id`) 
	VALUES ('". implode("', '", $values) . "') 
	ON DUPLICATE KEY UPDATE
	`status` = VALUES(`status`), complaint = VALUES(complaint), service = VALUES(service), notes = VALUES(notes), prev_report_id = VALUES(prev_report_id),
	engineer = VALUES(engineer), assigned_engineer = VALUES(assigned_engineer), `date` = VALUES(`date`), po_number = VALUES(po_number), contract_override = VALUES(contract_override),
	slice_mas_count = VALUES(slice_mas_count), gantry_rev = VALUES(gantry_rev), sys_scn_secs = VALUES(sys_scn_secs), helium_level = VALUES(helium_level), vessel_pressure = VALUES(vessel_pressure), recon_ruo = VALUES(recon_ruo),
	cold_ruo = VALUES(cold_ruo), compressor_hours = VALUES(compressor_hours), compressor_pressure = VALUES(compressor_pressure), downtime = VALUES(downtime), pm = VALUES(pm),
	pm_date = VALUES(pm_date), system_status = VALUES(system_status), invoice_required = VALUES(invoice_required), phone_fix = VALUES(phone_fix), fbc = VALUES(fbc), user_id = VALUES(user_id), report_edited = VALUES(report_edited),
	report_edited_date = VALUES(report_edited_date), report_edited_by = VALUES(report_edited_by), report_edited_notes = VALUES(report_edited_notes), unique_id = VALUES(unique_id);";
}
d($sql);
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	die('MySQL ERROR!');	
}

$sql = "UPDATE systems_requests SET report_started = 'Y' WHERE unique_id = '$unique_id' LIMIT 1;";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

$sql="DELETE FROM systems_hours WHERE unique_id = '$unique_id';";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

foreach($hours as &$arr){
	$sql="INSERT INTO systems_hours(`report_id`, `date`, `reg_labor`, `ot_labor`, `reg_travel`, `ot_travel`, `phone`, `unique_id`, `system_unique_id`) 
	VALUES('$report_id','". implode("', '", $arr) . "','$unique_id', '".$main['system_unique_id']."');";
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
}

$sql="DELETE FROM systems_parts WHERE  `unique_id`='$unique_id';";
if(!$mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
	
if($parts[0]['parts_quanity'] != ""){
	foreach($parts as &$arr){
		$sql="INSERT INTO systems_parts(`report_id`, `number`, `serial`, `description`, `qty`, `unique_id`, `system_unique_id`, `date`) 
		VALUES('$report_id',\"". implode("\", \"", $arr) . "\",'$unique_id', '".$main['system_unique_id']."', '".date(storef,strtotime($main['report_date']))."');";
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
	}
}

if($finalize){
	$sql = "UPDATE systems_requests SET `status`='$status' WHERE `unique_id`='$unique_id' LIMIT 1;";
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$sql = "UPDATE systems_reports SET `finalize_date`='".date(storef,time())."' WHERE `unique_id`='$unique_id' LIMIT 1;";
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	if(strtolower($main['pm']) == "y"){
		$sql = "UPDATE systems_base SET `status`='$system_status', `last_pm`='".$pm_date."' WHERE `unique_id`='$system_unique_id' LIMIT 1;";
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
	}else{
		$sql = "UPDATE systems_base SET `status`='$system_status' WHERE `unique_id`='$system_unique_id' LIMIT 1;";
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
	}
	
	if(strtolower($main['invoice_req']) == "n"){
		if($main['contract_override'] !=""){
			$sql="SELECT c.invoiced FROM misc_contracts AS c WHERE c.id = '".$main['contract_override']."';";	
		}else{
			$sql="SELECT c.invoiced
				  FROM systems_base_cont AS sbc
				  LEFT JOIN misc_contracts AS c ON sbc.contract_type = c.id
				  WHERE sbc.unique_id = '".$main['system_unique_id']."';";	
		}
		if(!$resultInvoice = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
		$rowInvoice = $resultInvoice->fetch_assoc();
		if(strtolower($rowInvoice['invoiced']) == "y"){
			$sql="UPDATE systems_reports SET `invoice_required`='Y' WHERE unique_id = '$unique_id';";
			if(!$result = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
				$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
			}
			$php_utils->email_action_add($mysqli, "/report/common/report_valuation_send.php",array('rid'=>$unique_id),$_SESSION['login']);
		}
	}else{
		$php_utils->email_action_add($mysqli, "/report/common/report_valuation_send.php",array('rid'=>$unique_id),$_SESSION['login']);
	}

	$data = array('rid'=>$unique_id, 'edit'=>$edit?"y":"n");
	$php_utils->email_action_add($mysqli, "/report/common/report_send.php",$data,$_SESSION['login']);

}else{
	$sql = "UPDATE systems_base SET `status`='$system_status' WHERE `unique_id`='$system_unique_id' LIMIT 1;";
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
}



if(!$save){
?>
<form style="display:none" id="form" name="form" method="post" action="report_save_complete.php">
<input name="status" value="<?php echo $status; ?>" />
<input name="report_id" value="<?php echo $report_id; ?>" />
<input name="unique_id" value="<?php echo $unique_id; ?>" />
<input name="user_id" value="<?php echo $main['user_id']; ?>" />
<input name="assigned_engineer" value="<?php echo $main['assigned_engineer']; ?>" />
<?php if($edit){ ?>
<input name="edit" value="Y" />
<?php }?>
<?php if($finalize and $fbc){ ?>
<input name="fbc" value="Y" />
<?php } ?>
</form>
<script type="text/javascript">

document.forms["form"].submit();

</script>
<?php } ?>