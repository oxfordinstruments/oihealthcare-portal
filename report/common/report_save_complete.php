<?php
//Update Completed 11/25/14
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

$update_word_limit 	 = intval($settings->update_word_limit);

if(isset($_SESSION['service_report_refresh'])){
	$log->logmsg("You can not refresh this page!!!",100,true,basename(__FILE__),__LINE__);
}
$_SESSION['service_report_refresh'] = true;

if(isset($_POST['status'])){
	$status = $_POST['status'];
	if(strtolower($status)=="closed"){
		$finalize = true;
	}else{
		$finalize = false;	
	}
}else{
	$status = "na";
};
if(isset($_POST['report_id'])){$report_id = $_POST['report_id'];}else{$report_id = "na";};
if(isset($_POST['unique_id'])){$unique_id = $_POST['unique_id'];}else{$unique_id = "na";};
if(isset($_POST['user_id'])){$user_id = $_POST['user_id'];}else{$user_id = "na";};
if(isset($_POST['assigned_engineer'])){$assigned_engineer = $_POST['assigned_engineer'];}else{$assigned_engineer = "na";};
if(isset($_POST['edit'])){$edit = true;}else{$edit = false;};
if(isset($_POST['fbc'])){$fbc = true;}else{$fbc = false;};

$myusername = $_SESSION["login"];

$sql = "SELECT r.system_id, sb.unique_id, r.pm, u.name AS engineer_name, sbc.nickname AS system_nickname, st.`type` AS `type`
FROM systems_reports AS r
LEFT JOIN users AS u ON u.uid = r.engineer
LEFT JOIN systems_base AS sb ON sb.unique_id = r.system_unique_id
LEFT JOIN systems_base_cont AS sbc ON sb.unique_id = sbc.unique_id
LEFT JOIN systems_types AS st ON st.id = sb.system_type
WHERE r.unique_id = '$unique_id';"; 
if(!$resultUpdate = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowUpdate = $resultUpdate->fetch_assoc();

function limit_words($string, $word_limit)
{
	$words = explode(" ",$string);
	if(count($words) > $word_limit){
		return implode(" ",array_splice($words,0,$word_limit))."...(truncated)";
	}else{
		return implode(" ",array_splice($words,0,$word_limit));
	}
}

?>
<!DOCTYPE html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<?php require_once($js_include);?>

<script type="text/javascript">
$(document).ready(function() {

});

function delayer(){
    <?php if($fbc){ ?>
	window.location = "<?php echo 'iso_fbc.php?system_unique_id='.$rowUpdate['unique_id'].'&report_unique_id='.$unique_id;?>"
	<?php } ?>
}
</script>
</head>
<?php if($fbc and $finalize){ ?>
<body onLoad="setTimeout('delayer()', 1000)">
<?php }else{ ?>
<body>
<?php } ?>
<?php require_once($header_include); ?>
<div id="OIReportContent"> 
<div id="afterReportContainer">
        	<div id="afterReportHeader"><?php if($finalize){?> Report has been submitted <?php }else{echo "Report has been saved for later";} ?></div>
			<?php if($finalize and $fbc){?>
			<br>
			<div id="afterReportHeader">
				<p><span class="red">Please wait</span></p>
				<p><span class="red">Redirecting you to fill out a Feedback Complaint Form</span></p>
				<p>1 seconds</p>
			</div>
			<br>
			<?php } ?>
            <div id="afterReportButtons">
            	<span <?php if(!$finalize){echo "style=\"display:none\"";} ?> class="afterReportButton"><a href="report_view.php?id=<?php echo $unique_id; ?>&uid=<?php echo $myusername; ?>" target="_self">View Report</a></span>
            	<span <?php if(!$finalize){echo "style=\"display:none\"";} ?> class="afterReportButton"><a target="new" class="_iframeLarge" href="<?php echo "/resources/print_sr/f70-14-15_rev-a.php?unique_id=$unique_id"; ?>">Print Report</a></span>
            	<span class="afterReportButton"><a href="<?php echo $refering_uri; ?>" target="_self">Return Home</a></span>
            </div>
        </div> 
</div>
<?php require_once($footer_include); ?>
