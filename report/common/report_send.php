<?php
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
ini_set('display_errors', 'On');
ignore_user_abort(true);
set_time_limit(30);

if(substr(php_sapi_name(), 0, 3) == 'cli' || empty($_SERVER['REMOTE_ADDR'])) {
	//shell
	$cron = false;
	if(isset($_SERVER['argv'][1]) and $_SERVER['argv'][1] == '--cron'){
		$cron = true;
	}
	$docroot = preg_replace("/(public_html).*/",'public_html',dirname(__FILE__),2);
	$no_define_eol = true;
	define('EOL',PHP_EOL);
} else {
	//webserver
	$cron = false;
	$docroot = $_SERVER['DOCUMENT_ROOT'];
	define('EOL','<br />');
}

$settings = new SimpleXMLElement($docroot.'/settings.xml', null, true);
require_once($docroot.'/define_inc.php');
require_once($docroot.'/mysqlInfo.php');
require_once($docroot.'/log/log.php');
$log = new logger($docroot);

require_once($docroot.'/resources/PHPMailer/PHPMailerAutoload.php');

if(isset($_GET['debug'])){
	if(!isset($_GET['rid'])){
		$log->logerr('Blank RID',1025);
		exit("No report id");
	}
	if(isset($_GET['send'])){$send = true;}else{$send = false;}
	if(isset($_GET['edit'])){$edit = true;}else{$edit = false;}
	$rid=$_GET['rid'];
	$debug = true;
}else{
	$send = true;
	if(!isset($_POST['rid'])){
		exit("No report id");
	}
	if(isset($_POST['edit'])){
		if(strtolower($_POST['edit']) == 'y'){
			$edit = true;
		}else{
			$edit = false;	
		}
	}
	$rid=$_POST['rid'];
	$debug = false;
	if(intval($settings->disable_email) == 1){
		$send = false;
	}
}

require_once($docroot.'/report/common/scripts/php_utils.php');
$php_utils = new phpUtils($docroot, $debug);

require_once($docroot.'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

require_once($docroot.'/resources/smarty/libs/Smarty.class.php');
$smarty = new Smarty();
$smarty->setTemplateDir($docroot.$settings->email_templates);
$smarty->setCompileDir($docroot.$settings->email_template_c);
$smarty->setCacheDir($docroot.$settings->email_cache);
$smarty->setConfigDir($docroot.$settings->email_configs);
$smarty->use_sub_dirs = true;
$smarty->force_compile = true;
//$smarty->debugging = true;
$smarty->caching = false;

$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno) {die('There was an error running the query [' . $mysqli->connect_error . ']');}

//get service report info
$sql="SELECT r.*, u.name AS assigned_engineer_name, u.email AS assigned_engineer_email, 
MAX(IF(prid.pref = 'pref_rcv_service_reports','Y','N')) AS pref_rcv_service_reports, 
MAX(IF(pid.perm = 'perm_rcv_service_reports','Y','N')) AS perm_rcv_service_reports,
e.`type` AS equip_type, f.email_list, eu.name AS editor
FROM systems_reports AS r
LEFT JOIN users AS u ON r.assigned_engineer = u.uid
LEFT JOIN systems_base AS sb ON sb.unique_id = r.system_unique_id
LEFT JOIN systems_base_cont AS sbc ON sb.unique_id = sbc.unique_id
LEFT JOIN systems_types AS e ON e.id = sb.system_type
LEFT JOIN users AS eu ON r.report_edited_by = eu.uid
LEFT JOIN users_prefs AS upr ON upr.uid = u.uid
LEFT JOIN users_pref_id AS prid ON prid.id = upr.pid
LEFT JOIN users_perms AS up ON up.uid = u.uid
LEFT JOIN users_perm_id AS pid ON pid.id = up.pid
LEFT JOIN facilities AS f ON f.unique_id = sbc.facility_unique_id
WHERE r.unique_id ='$rid';";
d($sql);
if(!$result=$mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	exit("SQL ERROR");
}
$rowReportInfo = $result->fetch_assoc();
d($rowReportInfo);

//get file name type 14 or 15
switch($rowReportInfo['equip_type']){
			case "MR":
				$pdf_name="F70-15_Service_Report_".$rowReportInfo['report_id'].".pdf";
				break;
			case "CT":
				$pdf_name="F70-14_Service_Report_".$rowReportInfo['report_id'].".pdf";
				break;
}
$pdf_file = $settings->php_base_dir . $settings->uploads_dir . $settings->uploads->service_reports . '/' . $pdf_name;
d($pdf_file);

//create pdf if does not exist
if($edit){
	$report_unique_id = $rid;
	require($docroot.'/resources/print_sr/f70-14-15_rev-c.php');
}else{
	if(!file_exists($pdf_file)){
		$report_unique_id = $rid;
		require($docroot.'/resources/print_sr/f70-14-15_rev-c.php');
	}	
}

//assigned eng reciepents
$emails_report_emp = array();
if(strtolower($rowReportInfo['perm_rcv_service_reports']) == "y" and strtolower($rowReportInfo['pref_rcv_service_reports']) == "y"){
	array_push($emails_report_emp,array("email"=>$rowReportInfo['assigned_engineer_email'],"name"=>$rowReportInfo['assigned_engineer_name'], 'email_unique_id'=>'0'));
}

//employees wanting a copy of the report
$sql="SELECT u.name,u.email
FROM users AS u
INNER JOIN users_prefs AS upr ON upr.uid = u.uid
INNER JOIN users_pref_id AS prid ON prid.id = upr.pid AND prid.pref = 'pref_rcv_service_reports'
INNER JOIN users_perms AS up ON up.uid = u.uid
INNER JOIN users_perm_id AS pmid ON pmid.id = up.pid AND pmid.perm = 'perm_rcv_service_reports'
INNER JOIN users_groups AS ug ON ug.uid = u.uid
INNER JOIN users_group_id AS grid ON grid.id = ug.gid AND grid.`group` = 'grp_employee'
WHERE u.active = 'y' UNION
SELECT u2.name, u2.email
FROM users AS u2
RIGHT JOIN systems_assigned_customer AS sac ON sac.uid = u2.uid AND sac.system_ver_unique_id = '".$rowReportInfo['system_ver_unique_id']."'
INNER JOIN users_prefs AS upr2 ON upr2.uid = u2.uid
INNER JOIN users_pref_id AS prid2 ON prid2.id = upr2.pid AND prid2.pref = 'pref_rcv_service_reports'
INNER JOIN users_perms AS up2 ON up2.uid = u2.uid
INNER JOIN users_perm_id AS pmid2 ON pmid2.id = up2.pid AND pmid2.perm = 'perm_rcv_service_reports'
WHERE u2.active = 'y';";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	exit("SQL ERROR");
}
while($row = $result->fetch_assoc()){
	if($row['email'] != $rowReportInfo['assigned_engineer_email']){
		array_push($emails_report_emp,array("email"=>$row['email'],"name"=>$row['name'], 'email_unique_id'=>'0'));
	}
}
d($emails_report_emp);


//subscribers wanting a copy of the report
$emails_report_pub = array();
$sql="SELECT cel.email, cel.name, cel.email_unique_id
FROM customers_email_list AS cel
WHERE cel.system_ver_unique_id = '".$rowReportInfo['system_ver_unique_id']."' AND cel.active = 'y' AND cel.rcv_service_report = 'y'
GROUP BY cel.email_unique_id;";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	exit("SQL ERROR");
}
if($result->num_rows > 0){
	while($row = $result->fetch_assoc()){
		array_push($emails_report_pub,array("email"=>$row['email'],"name"=>$row['name'], 'email_unique_id'=>$row['email_unique_id']));
	}
}
d($emails_report_pub);


if(strtolower($rowReportInfo['report_edited']) == 'y'){
	$smarty->assign('edit',true);
}
$smarty->assign('system_nickname',$rowReportInfo['system_nickname']);
$smarty->assign('system_id',$rowReportInfo['system_id']);
$smarty->assign('editor',$rowReportInfo['editor']);
$smarty->assign('report_id',$rowReportInfo['report_id']);
$smarty->assign('asn_eng',$rowReportInfo['assigned_engineer_name']);
$smarty->assign('problem_reported',$rowReportInfo['complaint']);
$smarty->assign('service',$rowReportInfo['service']);
$smarty->assign('company_name',$settings->company_name);
$smarty->assign('copyright_date',$settings->copyright_date);
$smarty->assign('email_pics',$settings->email_pics);
$smarty->assign('oih_url_pretty',$settings->base_url_pretty);

//Employee Emails
if($emails_report_emp){
	$send_status = send_emails($emails_report_emp, $rowReportInfo['system_id'], $rowReportInfo['system_nickname'], $rowReportInfo['system_ver_unique_id'], 'service_report_emp.tpl',false, $pdf_file, $pdf_name);
	if(!$send_status and $debug){
		echo "<h1 style='color: red'>SEND ERROR SEE LOG !</h1>",EOL;
	}
}

//Subscriber Emails
if($emails_report_pub){
	$send_status = send_emails($emails_report_pub, $rowReportInfo['system_id'], $rowReportInfo['system_nickname'], $rowReportInfo['system_ver_unique_id'], 'service_report_pub.tpl', true, $pdf_file, $pdf_name);
	if(!$send_status and $debug){
		echo "<h1 style='color: red'>SEND ERROR SEE LOG !</h1>",EOL;
	}
}

//	echo date(storef, time()) . "<br />";

function send_emails($email_addresses = false, $system_id = false, $system_nickname = false, $system_ver_unique_id = false, $template = false, $subscriber = false, $attachment = false, $attachment_name = false){

	//$email_addresses is array of array( email, name, email_unique_id )   e.g. ('me@me.com', 'John Doe', 'aaff123f987d8e90e66ff7')

	global $settings, $debug, $send, $log, $edit, $delReq, $smarty, $rid;

	if(!$email_addresses){
		$log->logerr("Send email info missing",0,false,basename(__FILE__),__LINE__);
		return false;
	}
	if(!$template or !$system_id or !$system_nickname or !$system_ver_unique_id){
		$log->logerr("Template, System ID, System Nickname, or System Ver UID missing",0,false,basename(__FILE__),__LINE__);
		return false;
	}
	if(is_array($email_addresses) and count($email_addresses) == 0){
		$log->logerr("Email Addresses array empty",0,false,basename(__FILE__),__LINE__);
		return false;
	}
	if($email_addresses[0]['email_unique_id'] == "" and $subscriber){
		$log->logerr("email unique id empty",0,false,basename(__FILE__),__LINE__);
		return false;
	}
	if(!$attachment or !$attachment_name){
		$log->logerr("Attachment or attachment name empty",0,false,basename(__FILE__),__LINE__);
		return false;
	}

//	echo "<hr><h1>Sending Email(s) using ".$template." to:</h1>",EOL;
//	echo "<pre>",EOL;
//	foreach($email_addresses as $email){
//		echo $email['email']."\n";
//	}
//	echo "</pre>",EOL;

	$email_result = "";

	$mail = new PHPMailer;
	$mail->IsSMTP();
	$mail->Host = (string)$settings->email_host;
	$mail->SMTPAuth = true;
	$mail->Username = (string)$settings->email_requests;
	$mail->Password = (string)$settings->email_password;
	$mail->SMTPSecure = 'tls';
	$mail->From = (string)$settings->email_reports;
	$mail->FromName = 'Service Report '.$settings->short_name.' Portal';
	if($debug){
		$mail->AddAddress(trim((string)$settings->email_support),'Support');
		$email_result .= "Email sent to: ".trim((string)$settings->email_support)."\n";
		if($subscriber){
			$unsubscribe = $settings->full_url . $settings->email_unsubscribe . "?eid=" . $email_addresses[0]['email_unique_id'] . "&svuid=" . $system_ver_unique_id;
			$smarty->assign('unsubscribe',$unsubscribe);
		}
	}else{
		foreach($email_addresses as $email){
			if($subscriber){
				$unsubscribe = $settings->full_url . $settings->email_unsubscribe . "?eid=" . $email_addresses[0]['email_unique_id'] . "&svuid=" . $system_ver_unique_id;
				//echo $unsubscribe,EOL;
				$smarty->assign('unsubscribe',$unsubscribe);
			}
			$mail->AddAddress(trim($email['email']),$email['name']);  // Add a recipient
			$email_result .= "Email sent to: ".$email['email']."\n";
		}
	}
	$mail->AddReplyTo((string)$settings->email_support, 'Support '.$settings->short_name.' Portal');
	$mail->AddCC((string)$settings->email_support);
	$mail->IsHTML(true);
	$mail->AddAttachment($attachment, $attachment_name);
	$mail->Subject = 'Service Report for '.$system_nickname.' ( '.$system_id.' ) ';
	$mail->Body = $smarty->fetch($template);

	//d($mail);

	if($debug){
		echo "Subject: ",$mail->Subject,EOL;
		echo $mail->Body,EOL,"<hr>",EOL;
	}

	if($send){
		if(!$mail->Send()) {
			echo 'Email could not be sent.';
			$email_result = "Email could not be sent!";
			echo 'Mailer Error: ' . $mail->ErrorInfo;
			$log->logerr($mail->ErrorInfo,1053,false,basename(__FILE__),__LINE__);
			exit;
		}else{
			$log->loginfo($rid .' '. implode('; ',array_keys($mail->getAllRecipientAddresses())),200,false,basename(__FILE__),__LINE__);
		}
	}else{
		echo "Send False: Not sending email",EOL;
	}
//	echo "<hr>";
//	echo "<br /><pre>Email Result\n";
//	echo $email_result;
//	echo "<br /></pre>";
	return true;
}/*send_emails*/

function limit_words($string, $word_limit){
	$words = explode(" ",$string);
	if(count($words) > $word_limit){
		return implode(" ",array_splice($words,0,$word_limit))."(more)";
	}else{
		return implode(" ",array_splice($words,0,$word_limit));
	}
}/*limit_words*/
exit("OK");


?>