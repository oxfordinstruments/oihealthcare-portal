<?php
//Update Completed 11/25/14
//Called by report_save_complete.php

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

if(!isset($_POST['status'])){
	$log->logerr('report_update_send.php',1029);
	echo 0;
}

if(!isset($_POST['uid'])){
	$log->logerr('report_update_send.php',1030);
	echo 0;
}

if(!isset($_POST['system_unique_id'])){
	$log->logerr('report_update_send.php',1031);
	echo 0;
}


$report_unique_id = '';
if(isset($_POST['report_unique_id'])){
	$report_unique_id = $_POST['report_unique_id'];	
}

$sql = "INSERT INTO systems_status_updates (system_unique_id, uid, `status`, `date`, report_unique_id) 
VALUES ('".$_POST['system_unique_id']."', '".$_POST['uid']."', \"".$_POST['status']."\", '".date(storef, time())."', '$report_unique_id');"; 
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
?>
