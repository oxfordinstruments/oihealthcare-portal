<?php
//Update Completed 11/25/14
$debug = false;
if(isset($_GET['debug'])){
	$debug = true;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

if(isset($_GET['uid'])){
	$user_id=$_GET['uid'];
}else{
	$log->logerr('report_valuation.php',1017);
	header("location:/error.php?n=1017&p=report_valuation.php");
}
if(isset($_GET['id'])){
	$unique_id=$_GET['id'];
}else{
	$log->logerr('report_valuation.php',1018);
	header("location:/error.php?n=1018&p=report_valuation.php");
}
if(isset($_GET['v'])){$view = true;}else{$view = false;}


$sql="SELECT * FROM systems_requests WHERE unique_id = '".$unique_id."';";
if(!$resultRequest = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowRequest = $resultRequest->fetch_assoc();
d($rowRequest);

$sql="SELECT * FROM systems_reports WHERE unique_id = '".$unique_id."';";
if(!$resultReport = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowReport = $resultReport->fetch_assoc();
d($rowReport);

$sql="SELECT sbc.system_id, sbc.nickname, sb.system_serial, sb.status, sb.system_type, sbc.contract_type, f.address, f.city, f.state, f.zip, sbc.ver_unique_id
	FROM systems_base_cont AS sbc
	LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
	LEFT JOIN facilities AS f ON f.unique_id = sbc.facility_unique_id 
	WHERE sb.system_id = '".$rowReport['system_id']."';";
if(!$resultSystem = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowSystem = $resultSystem->fetch_assoc();
d($rowSystem);

$sql="SELECT * FROM users WHERE uid = '".$rowRequest['engineer']."';";
if(!$resultAssigned = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowAssigned = $resultAssigned->fetch_assoc();

$sql="SELECT a.uid,u.name,u.email,u.cell,u.carrier 
FROM systems_assigned_pri AS a 
LEFT JOIN users AS u ON a.uid = u.uid 
WHERE a.system_ver_unique_id='".$rowSystem['ver_unique_id']."';";
if(!$resultEngPri = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowEngPri = $resultEngPri->fetch_assoc();

$sql="SELECT a.uid,u.name,u.email,u.cell,u.carrier
FROM systems_assigned_sec AS a 
LEFT JOIN users AS u ON a.uid = u.uid 
WHERE a.system_ver_unique_id='".$rowSystem['ver_unique_id']."';";
if(!$resultEngSec = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowEngSec = $resultEngSec->fetch_assoc();

$sql="SELECT * FROM misc_contracts WHERE `id` = '".$rowSystem['contract_type']."' LIMIT 1;";
if(!$resultContract = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowContract = $resultContract->fetch_assoc();

$sql="SELECT * FROM misc_contracts;";
if(!$resultContracts = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

$sql="SELECT * FROM systems_types WHERE `id` = '".$rowSystem['system_type']."' LIMIT 1;";
if(!$resultEquipment = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowEquipment = $resultEquipment->fetch_assoc();

$sql="SELECT u.uid, u.name
FROM users AS u
LEFT JOIN users_roles AS ur ON ur.uid = u.uid
LEFT JOIN users_role_id AS rid ON rid.id = ur.rid
WHERE rid.role = 'role_engineer' OR rid.role = 'role_contractor';";
if(!$resultEngineer = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

$sql="SELECT * FROM systems_status;";
if(!$resultStatus = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

$sql="SELECT * FROM systems_parts WHERE unique_id = '".$unique_id."';";
if(!$resultParts = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$numberParts = $resultParts->num_rows;

$t=time();

$uploadBtnVal = "Attach Documents";
$has_docs = false;
$num_docs = "Internal records only. Customer can not view documents.";
if(strtolower($rowReport['has_files']) == 'y'){
	$uploadBtnVal = "View/Modify Documents";
	$has_docs = true;
	$num_docs = "Approximately " . $rowReport['file_count'] . " Documents Attached";
}

$edit = false;
if(strtolower($rowReport['valuation_complete']) == 'y'){
	$edit = true;
}
?>
<!DOCTYPE html>
<html>
<head>

<?php require_once($head_include);?>
<link rel="stylesheet" type="text/css" href="/resources/css/form_report.css">
<?php require_once($css_include);?>
<?php require_once($js_include);?>

<script src="/resources/js/jquery-ui/jquery-ui.min.js"></script>
<script src="/resources/js/jquery.ui.widget.js"></script>
<script src="/resources/js/jquery.ui.datepicker.js"></script>
<script src="/resources/js/jquery-dynamic-form_MODIFIED.js"></script>
<script type="text/javascript">
$(document).ready(function() {
 	

	<?php if(!$view){ ?>
	update_hours();
	<?php } ?>
	
	$(".needPrice").css("background","#FFFF66");
	
	<?php if($view or $edit){ ?>
	$(".needPrice").css("background","#99FF99");
	$(".priced").css("background","#99FF99");
	<?php } ?>
	
	$(".needPrice").change(function(e) {
		if($(this).val() == ""){
			$(this).css("background","#FFFF66");
		}else{
			$(this).css("background","#99FF99");
		}
	});
	
	$("#update_hours").click(function(e) {
		update_hours(); //bottom of page
	});
		
	$(".button_jquery_save").button({
		icons: {
			primary: "ui-icon-check"
		}
	});
	
	$(".button_jquery_not").button({
		icons: {
			primary: "ui-icon-flag"
		}
	});
	
	$(".button_jquery_attach").button({
		icons: {
			primary: "ui-icon-disk"
		}
	});
	
	
});

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function submitcheck(id){
	ids = [];
	errors = [];
	
	if(id == 1){
		<?php if(!$_SESSION['mobile_device']){ ?>
			$.prompt("Mark this service report as invoice not required?",{
				title: "Mark Report",
				buttons: { Yes: 1, No: -1 },
				focus: 1,
				submit:function(e,v,m,f){ 
					e.preventDefault();
					$("#errors").hide();
					if(v == 1){
						$.prompt.close();
						document.getElementById("no_invoice").checked = true;
						document.forms["form"].submit();
					}else{
						$.prompt.close();	
					}
				}
			});
		<?php }else{ ?>
			if(confirm("Mark this service report as invoice not required?")){
				document.getElementById("no_invoice").checked = true;
				document.forms["form"].submit();
			}
		<?php } ?>
	}else{
		if($('#rate_labor_reg').val()==""){ids.push('#rate_labor_reg'); errors.push("Rate for hours worked regular");}			
		if($('#rate_labor_ot').val()==""){ids.push('#rate_labor_ot'); errors.push("Rate for hours worked over-time");}			
		if($('#rate_travel_reg').val()==""){ids.push('#rate_travel_reg'); errors.push("Rate for hours traveled regular");}
		if($('#rate_travel_ot').val()==""){ids.push('#rate_travel_ot'); errors.push("Rate for hours traveled ovet-time");}			
		if($('#total_rate_labor_reg').val()==""){ids.push('#total_rate_labor_reg'); errors.push("Total rate for hours worked regular");}				
		if($('#total_rate_labor_ot').val()==""){ids.push('#total_rate_labor_ot'); errors.push("Total rate for hours worked over-time");}			
		if($('#total_rate_travel_reg').val()==""){ids.push('#total_rate_travel_reg'); errors.push("Total rate for hours traveled regular");}
		if($('#total_rate_travel_ot').val()==""){ids.push('#total_rate_travel_ot'); errors.push("Total rate for hours traveled ovet-time");}
		if($('#shipping').val()==""){ids.push('#shipping'); errors.push("Shipping amount. Zero is acceptable.");}
		if($('#expense').val()==""){ids.push('#expense'); errors.push("Expenses amount. Zero is acceptable");}
		
		for(var i=0;i<<?php echo $numberParts; ?>;i++){
			if($('#part_price'+i).val()==""){ids.push('#part_price'+i); errors.push("Price for part missing");}
		}			
		
		
		console.log("ids: " + ids);
		console.log("errors: " + errors);
		showErrors(ids,errors);
			
		if(ids.length <= 0){
			$("#errors").hide();
			<?php if(!$_SESSION['mobile_device']){ ?>		
				var states = {
					state0: {
						title: 'Update Rates',
						html:"<h3>Would you like to update the system info with these labor and travel rates?</h3>",
						buttons: { Yes: 1, No: -1 },
						focus: 1,
						submit:function(e,v,m,f){ 
							e.preventDefault();
							if(v == 1){
								document.getElementById("update_system").checked = true;
							}else{
								document.getElementById("update_system").checked = false;
							}
							$.prompt.goToState('state1');
						}
					},
					state1: {
						title: 'Valuate',
						html:  	"<h3>Submit this valuation?</h3>",
						buttons: { Yes: 1, No: -1 },
						focus: 1,
						submit:function(e,v,m,f){ 
							e.preventDefault();
							if(v == 1){
								$('.skip_post').remove();
								document.forms["form"].submit();
							}else{
								$.prompt.close();	
							}						
						}
					}
				};
				$.prompt(states);
			<?php }else{ ?>
				if(confirm("Would you like to update the system info with these labor and travel rates?")){
					document.getElementById("update_system").checked = true;
					if(confirm("Submit this valuation?")){
						$('.skip_post').remove();
						document.forms["form"].submit();
					}
				}else{
					document.getElementById("update_system").checked = false;
					if(confirm("Submit this valuation?")){
						$('.skip_post').remove();
						document.forms["form"].submit();
					}
				}
			<?php } ?>
		}
	}
}
/////////////////////////////////////////////////////////////////////////////////////
function showErrors(ids,errors){
	//thin solid #2C3594
	$("input, select, .chosen-container").each(function(index, element) {
		$(this).animate({
			borderColor: "#2C3594",
			boxShadow: 'none'
		});
	});
	
	$.each(ids,function(index,value){
		$(value).animate({
			borderColor: "#cc0000",
			boxShadow: '0 0 5px 3px rgba(255,0,0,0.4)'
		});
	});
	$("#errors > span").html("");
	$.each(errors,function(index, value){
		$("#errors > span").append(value + "<br>");
	});
	$("#errors").show('slow');
	$(document).scrollTop(0);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
</script>
</head>
<body>
<?php require_once($header_include); ?>
	<div id="OIReportContent"> 
		<div id="stylized" class="myform">
			<form id="form" name="form"  method="post" enctype='multipart/form-data' action="report_valuation_do.php">
				<div id="srHeaderDiv"><h1><?php if($edit){echo "Edit ";}?>Service Report Valuation</h1></div>
				<div id="main"><!-- do not remove -->
					<div id="errors" style="text-align:center;display:none; margin-bottom:25px; margin-top:25px; font-size:18px;">
						<h2 style="margin:0px; padding:0px; font-size:24px;">Errors to fix</h2>
						<span style="color:#F00">
						</span>
					</div>
					<div id="srInfoDiv">
						<table id="srInfoTable" class="srTable">
							<tr>
							  <td class="rowLabel">System&nbsp;ID: </td>
							  <td class="rowData"><?php echo $rowSystem['system_id']; ?></td>
							  <td class="rowLabel">Name: </td>
							  <td class="rowData"><?php echo $rowSystem['nickname']; ?></td>
							</tr>
							<tr>
							  <td class="rowLabel">Address: </td>
							  <td colspan="4" class="rowData"><?php echo $rowSystem['address'] ."&nbsp;&nbsp;&nbsp;". $rowSystem['city'] .", ". $rowSystem['state'] ."  ". $rowSystem['zip'];?></td>
							</tr>
							<tr>
							  <td class="rowLabel">Serial&nbsp;Number: </td>
							  <td class="rowData"><?php echo $rowSystem['system_serial']; ?></td>
							  <td class="rowLabel">System: </td>
							  <td class="rowData"><?php echo $rowEquipment['name']; ?></td>
							</tr>
							<tr>
							  <td class="rowLabel">Contract&nbsp;Type: </td>
							  <td class="rowData"><?php echo $rowContract['type']; ?></td>
							  <td class="rowLabel">Assigned&nbsp;Engineer: </td>
							  <td class="rowData"><?php echo $rowAssigned['name']; ?></td>
							</tr>
							<tr>
							  <td class="rowLabel">Service&nbsp;Request&nbsp;Num: </td>
							  <td class="rowData"><?php echo $rowReport['report_id']; ?></td>
							  <td class="rowLabel">Primary&nbsp;Engineer: </td>
							  <td class="rowData"><?php echo $rowEngPri['name']; ?></td>
							</tr>
						</table>
					</div>
					
					<div id="srDataDiv" class="skip_post">
						<table id="srDataTable" class="srTable">
							<tr>
								<td class="rowLabel"></td>
								<td class="rowData"></td>
								<td class="rowLabel">Purchase Order:</td>
								<td class="rowData"><input type="text" id="po_number" name="po_number" readonly value="<?php echo $rowReport['po_number'];?>" /></td>
							</tr>
							<tr>
								<td class="rowLabel">Invoice Required:</td>
								<td class="rowData"><input name="invoice_req" id="invoice_req"  readonly value="<?php echo strtoupper($rowReport['invoice_required']) ?>" /></td> 
								<td class="rowLabel">Contract Type Override:</td>
								<?php
									while($rowContracts = $resultContracts->fetch_assoc())
									{
										if($rowReport['contract_override']==$rowContracts['id']){
											$contract_override = $rowContracts['type'];
										}
									}
								?>
								<td class="rowData"><input name="contract_override" id="contract_overide"  readonly value="<?php echo $contract_override; ?>" /></td>
							</tr>
						</table>
					</div>
				
				
					<div id="srDataDiv" class="skip_post">
						<table id="srDataTable" class="srTable">
							<tr>
							  <td class="rowLabel">Report Date:</td>
							  <td class="rowData"><input type="text" id="report_date" name="report_date" class="findme" readonly value="<?php echo date(phpdispfd,strtotime($rowReport['date'])); ?>" onmouseout="return hideTip();"/></td>
							  <?php
								while($rowEngineer = $resultEngineer->fetch_assoc())
								{
									if(strtolower($rowEngineer['uid']) == strtolower($rowReport['engineer'])){
										$engineer_name = $rowEngineer['name'];
									}	
								}
							  ?>
							  <td class="rowLabel">Service Engineer:</td>
							  <td class="rowData"><input name="engineer" id="engineer" readonly value="<?php echo $engineer_name; ?>" /></td>
							</tr>
							<tr>
							  
							  
							</tr>
							<tr>
							  <td class="rowLabel">Preventave Maintaince:</td>
							  <td class="rowData"><input name="pm" id="pm" readonly value="<?php echo strtoupper($rowReport['pm']); ?>" /></td>
							  <td class="rowLabel">Hours System was down:</td>
							  <td class="rowData"><input type="text" id="down_time" name="down_time" readonly value="<?php echo $rowReport['downtime']; ?>" /></td>
							</tr>
							<tr>
							<?php
								while($rowStatus = $resultStatus->fetch_assoc())
								{
									if($rowReport['system_status']==$rowStatus['id']){
										$equipment_status = $rowStatus['status'];
									}
								}
							  ?>
							  <td class="rowLabel">System Status:</td>
							  <td class="rowData"><input name="equipment_status" id="equipment_status" readonly value="<?php echo $equipment_status; ?>" /></td> 
							  
							  <td class="rowLabel">&nbsp;</td>
							  <td class="rowData">&nbsp;</td> 
								 
							</tr>
						</table>
					</div>
				
					<div id="srDataDivCT" class="skip_post" <?php if($rowEquipment['type'] != "CT"){ echo" style='display:none'";} ?> >
						<table id="srDataTable">
							<tr>
							  <td class="rowLabel">Slice/mAs Count:</td>
							  <td class="rowData"><input type="text" id="slice_mas" readonly name="slice_mas" <?php echo "value=\"".$rowReport['slice_mas_count']."\"";?>/></td>
							  <td class="rowLabel">Gantry Revolutions:</td>
							  <td class="rowData"><input type="text" id="gantry_rev" readonly name="gantry_rev" <?php echo "value=\"".$rowReport['gantry_rev']."\"";?>/></td>
							</tr>
						</table>
					</div>
				
					<div id="srDataDivMR" class="skip_post" <?php if($rowEquipment['type'] != "MR"){ echo" style='display:none'";} ?>>
						<table id="srDataTable">
							<tr>
							  <td class="rowLabel">Helium Level:</td>
							  <td class="rowData"><input type="text" id="helium" readonly name="helium" <?php echo "value=\"".$rowReport['helium_level']."\"";?>/></td>
							  <td class="rowLabel">Vessel Pressure:</td>
							  <td class="rowData"><input type="text" id="pressure" readonly name="pressure" <?php echo "value=\"".$rowReport['vessel_pressure']."\"";?>/></td>
							</tr>
							<tr>
							  <td class="rowLabel">Compressor Pressure:</td>
							  <td class="rowData"><input type="text" id="compressor_pressure" readonly name="compressor_pressure" <?php echo "value=\"".$rowReport['compressor_pressure']."\"";?>/></td>
							  <td class="rowLabel">Compressor Hours:</td>
							  <td class="rowData"><input type="text" id="compressor_hours" readonly name="compressor_hours" <?php echo "value=\"".$rowReport['compressor_pressure']."\"";?>/></td>
							</tr>
							<tr>
							  <td class="rowLabel">Recon RUO:</td>
							  <td class="rowData"><input type="text" id="recon_ruo" readonly name="recon_ruo" <?php echo "value=\"".$rowReport['recon_ruo']."\"";?>/></td>
							  <td class="rowLabel">Coldhead RUO:</td>
							  <td class="rowData"><input type="text" id="coldhead_ruo" readonly name="coldhead_ruo" <?php echo "value=\"".$rowReport['cold_ruo']."\"";?>/></td>
							</tr>
						</table>
					</div>
				
					<div id="srComplaintDiv" class="skip_post">
						<table id="srComplaintTable" class="srTable">
							<tr>
							  <td class="rowLabel">Customer Complaint</td>
							</tr>
							<tr>
							  <td class="rowData"><textarea id="complaint" name="complaint" maxlength="2000" readonly><?php echo $rowReport['complaint']; ?></textarea></td>
							</tr>
						</table>
					</div>
				
					<div id="srServiceDiv" class="skip_post">
						<table id="srServiceTable" class="srTable">
							<tr>
							  <td class="rowLabel">Service Performed</td>
							</tr>
							<tr>
							  <td class="rowData"><textarea id="service" name="service" maxlength="4000" readonly><?php echo $rowReport['service'];?></textarea></td>
							</tr>
						</table>
					</div>
				
					<div id="srNotesDiv" class="skip_post" <?php if(strtolower($_SESSION['userdata']['grp_customer']) == "y"){echo "style=\"display:none\"";} ?>>
						<table id="srNotesTable" class="srTable">
							<tr>
							  <td class="rowLabel">Engineer Notes</td>
							</tr>
							<tr>
							  <td class="rowData"><textarea id="notes" name="notes" maxlength="2000" readonly><?php echo $rowReport['notes'];?></textarea></td>
							</tr>
						</table>
					</div>
				
					<?php if($_SESSION['group'] == "1" and $has_docs){?>
					<div id="srFilesDiv" class="skip_post">
						<table id="srFilesTable" class="srTable">
							<tr>
								<td class="rowLabel" colspan="2">Attached Documents</td>
							</tr>
							<tr>
								<td class="rowData" width="50%"><div class="srBottomBtn"><a href="/resources/kcfinder/kcfinder.php?unique_id=<?php echo $unique_id; ?>&table=systems_reports_files" class="button_jquery_save iframeUpload" id="attachDocs"><?php echo $uploadBtnVal; ?></a></div></td>
								<td class="rowData" style="text-align:center; color:#f79548"><?php echo $num_docs; ?></td>
							</tr>
						</table>
					</div>
					<?php } ?>
					
					<div style="display:none">
						<input type="checkbox" id="finalize" name="finalize" />
						<input type="checkbox" id="edit" name="edit" />
						<input type="text" id="user_id" name="user_id" value="<?php echo $_SESSION['login']; ?>" />
						<input type="text" id="system_id" name="system_id" value="<?php echo $rowSystem['system_id']; ?>" />
						<input type="text" id="system_nickname" name="system_nickname" value="<?php echo $rowSystem['nickname']; ?>" />
						<input type="text" id="engineer_assigned" name="engineer_assigned" value="<?php echo $rowRequest['engineer']; ?>" />
						<input type="text" id="report_id" name="report_id" value="<?php echo $rowRequest['request_num']; ?>" />
						<input type="text" id="unique_id" name="unique_id" value="<?php echo $rowRequest['unique_id']; ?>" />
					</div>
				</div><!-- div main -->
				<?php
					$sql="SELECT * FROM systems_hours WHERE unique_id = '".$unique_id."' ORDER BY `date` ASC;";
					if(!$resultHours = $mysqli->query($sql)){
						$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
						$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
						$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
					}
					$numberHours = $resultHours->num_rows;
					
				?>
				<div id="srHoursDiv" <?php if(intval($numberHours) <= 0){echo " style=\"display:none\"";}?>>
					<table id="srHoursTable" class="srTable">
					<tr>
						<th scope="col">date</th>
						<th colspan="2" scope="col">Hours Worked</th>
						<th colspan="2" scope="col">Hours Traveld</th>
						</tr>
					<tr>
				
						<td class="rowLabel"></td>
						<td class="rowLabel">Reg</td>
						<td class="rowLabel">OT</td>
						<td class="rowLabel">Reg</td>
						<td class="rowLabel">OT</td>
					</tr>
					<?php
					$hwr=0;
					$hwo=0;
					$htr=0;
					$hto=0;
					if(intval($numberHours) > 0){
						$i = 0;
						while($rowHours = $resultHours->fetch_assoc())
						{
							echo "<tr id=\"hours\">\n";
							echo "<td class=\"rowData\"><input readonly type=\"text\" id=\"hours_date\" name=\"hours[$i][hours_date]\" value=\"".date(phpdispfd,strtotime($rowHours['date']))."\" /></td>\n";
							echo "<td class=\"rowData\"><input readonly type=\"number\" id=\"hours_wreg\" name=\"hours[$i][hours_wreg]\" value=\"".$rowHours['reg_labor']."\" /></td>\n";
							echo "<td class=\"rowData\"><input readonly type=\"number\" id=\"hours_wot\" name=\"hours[$i][hours_wot]\" value=\"".$rowHours['ot_labor']."\" /></td>\n";
							echo "<td class=\"rowData\"><input readonly type=\"number\" id=\"hours_treg\" name=\"hours[$i][hours_treg]\" value=\"".$rowHours['reg_travel']."\" /></td>\n";
							echo "<td class=\"rowData\"><input readonly type=\"number\" id=\"hours_tot\" name=\"hours[$i][hours_tot]\" value=\"".$rowHours['ot_travel']."\" /></td>\n";
							echo "</tr>\n";
							$hwr += floatval($rowHours['reg_labor']);
							$hwo += floatval($rowHours['ot_labor']);
							$htr += floatval($rowHours['reg_travel']);
							$hto += floatval($rowHours['ot_travel']);
							$i++;
						}
						
						echo "<tr id=\"hours\">\n";
						echo "<td class=\"rowData\" style=\"text-align:center\">Total Hours</td>\n";
						echo "<td class=\"rowData\" style=\"text-align:center\">".$hwr."</td>\n";
						echo "<td class=\"rowData\" style=\"text-align:center\">".$hwo."</td>\n";
						echo "<td class=\"rowData\" style=\"text-align:center\">".$htr."</td>\n";
						echo "<td class=\"rowData\" style=\"text-align:center\">".$hto."</td>\n";
						echo "</tr>\n";
					}    
					?>
					<fieldset id="hours">
					<tr>
						<td class="rowLabel">Hourly Rates<span id="hr_rate_lbl" class="tooltip"></span></td>
						<td class="rowData"><input type="number" class="priced" id="rate_labor_reg" name="rate_labor_reg" <?php if($view){echo "readonly";}?> value="<?php if(!$edit){echo $rowSystem['labor_reg_rate'];}else{echo $rowReport['invoice_labor_reg_rate'];} ?>" /></td>
						<td class="rowData"><input type="number" class="priced" id="rate_labor_ot" name="rate_labor_ot" <?php if($view){echo "readonly";}?> value="<?php if(!$edit){echo $rowSystem['labor_ot_rate'];}else{echo $rowReport['invoice_labor_ot_rate'];} ?>" /></td>
						<td class="rowData"><input type="number" class="priced" id="rate_travel_reg" name="rate_travel_reg" <?php if($view){echo "readonly";}?> value="<?php if(!$edit){echo $rowSystem['travel_reg_rate'];}else{echo $rowReport['invoice_travel_reg_rate'];} ?>" /></td>
						<td class="rowData"><input type="number" class="priced" id="rate_travel_ot" name="rate_travel_ot" <?php if($view){echo "readonly";}?> value="<?php if(!$edit){echo $rowSystem['travel_ot_rate'];}else{echo $rowReport['invoice_travel_ot_rate'];} ?>" /></td>
					</tr>
					<tr>
						<td class="rowLabel">Total Rate<span id="tot_rate_lbl" class="tooltip"></span></td>
						<td class="rowData"><input type="number" class="priced" id="total_rate_labor_reg" name="total_rate_labor_reg" <?php if($view){echo "readonly";}?> value="<?php if($edit){echo $rowReport['invoice_labor_reg'];} ?>" /></td>
						<td class="rowData"><input type="number" class="priced" id="total_rate_labor_ot" name="total_rate_labor_ot" <?php if($view){echo "readonly";}?> value="<?php if($edit){echo $rowReport['invoice_labor_ot'];} ?>" /></td>
						<td class="rowData"><input type="number" class="priced" id="total_rate_travel_reg" name="total_rate_travel_reg" <?php if($view){echo "readonly";}?> value="<?php if($edit){echo $rowReport['invoice_travel_reg'];} ?>" /></td>
						<td class="rowData"><input type="number" class="priced" id="total_rate_travel_ot" name="total_rate_travel_ot" <?php if($view){echo "readonly";}?> value="<?php if($edit){echo $rowReport['invoice_travel_ot'];} ?>" /></td>
					</tr>
					</fieldset>
					<?php if(!$view){ ?>
					<tr>
					<td colspan="5">
						<table width="100%" border="0" cellpadding="5" cellspacing="5">
							<tr>
								<td width="33%">&nbsp;</td>
								<td><input type="button" id="update_hours" name="update_hours" value="Calculate Total Rates" /></td>
								<td width="33%"></td>
							</tr>
						</table>
					</tr>
					<?php } ?>
				</table>
				</div>
			
				<div id="srPartsDiv"<?php if(intval($numberParts) <= 0){echo " style=\"display:none\"";}?>>
					<table id="srPartsTable" class="srTable">
						<tr>
							<th scope="col">Part Number</th>
							<th scope="col">Description</th>
							<th scope="col">Quanity</th>
							<th scope="col">Price Each</th>
						</tr>
						<?php
						if(intval($numberParts) > 0){
							$i=0;
							while($rowParts = $resultParts->fetch_assoc())
							{			
								echo "<tr id=\"parts\">\n";
								echo "<td class=\"rowData\"><input readonly type=\"text\" id=\"parts_number$i\" name=\"parts[$i][num]\" value=\"".$rowParts['number']."\" /><div style=\"display:none\"><input readonly type=\"text\" id=\"part_id$i\" name=\"parts[$i][id]\" value=\"".$rowParts['id']."\" /></div></td>\n";
								echo "<td class=\"rowData\"><input readonly type=\"text\" id=\"parts_description$i\" name=\"parts[$i][desc]\" value=\"".$rowParts['description']."\" /></td>\n";
								echo "<td class=\"rowData\"><input readonly type=\"number\" id=\"parts_quanity$i\" name=\"parts[$i][qty]\" value=\"".$rowParts['qty']."\" /></td>\n";
								echo "<td class=\"rowData\"><input class=\"needPrice\" type=\"number\" id=\"part_price$i\" name=\"parts[$i][price]\" value=\"".$rowParts['price']."\" /></td>\n";
								echo "</tr>\n";
								$i++;
							}
						}
						?>
					</table>
				</div>
			
				<div id="srShipDiv">
					<table id="srShipTable" class="srTable">
						<tr>
							<th scope="col">Shipping<span id="shipping_lbl" class="tooltip"></span></th>
							<th scope="col">Expenses<span id="expenses_lbl" class="tooltip"></span></th>
						</tr>
						<tr>
							<td class="rowData"><input class="needPrice" type="number" id="shipping" name="shipping" <?php if($view){echo "readonly";}?> value="<?php if($edit){echo $rowReport['invoice_shipping'];}?>" /></td>
							<td class="rowData"><input class="needPrice" type="number" id="expense" name="expense" <?php if($view){echo "readonly";}?> value="<?php if($edit){echo $rowReport['invoice_expense'];}?>" /></td>
						</tr>
					
					</table>
				</div>
			
				<div id="srFooterDiv">
					<?php if(!$view){ ?>
					<div class="srBottomBtn"><a class="button_jquery_save" onClick="submitcheck(0)">Submit Valuation</a></div>
					<div class="srBottomBtn"><a class="button_jquery_not" onClick="submitcheck(1)">Invoice Not Required</a></div>
					<?php } ?>
					<div align="right"><h4><?php echo "Unique ID: ".$unique_id; ?></h4></div>
				</div>
				<div style="display:none">
					<input type="checkbox" id="update_system" name="update_system" value="Y" />
					<input type="checkbox" id="no_invoice" name="no_invoice" value="Y" />
					<input type="text" id="valuation_uid" name="valuation_uid" value="<?php echo $_SESSION['login']; ?>" />
					<?php if($debug){ ?><input name="debug" id="debug" type="hidden" value="Y" /> <?php } ?>
				</div>
			</form>
		</div>
	</div>
<script type="text/javascript">
function update_hours(){
	var hwr = parseFloat("<?php echo $hwr; ?>");
	if(hwr < 1.0){hwr = Math.ceil(hwr);}
	var hwo = parseFloat("<?php echo $hwo; ?>");
	if(hwo < 1.0){hwo = Math.ceil(hwo);}
	var htr = parseFloat("<?php echo $htr; ?>");
	if(htr < 1.0){htr = Math.ceil(htr);}
	var hto = parseFloat("<?php echo $hto; ?>");
	if(hto < 1.0){hto = Math.ceil(hto);}
	
	if(document.getElementById("rate_labor_reg").value == "" && hwr > 0.0){
		//document.getElementById("total_rate_labor_reg").value = 0;
		$("#total_rate_labor_reg").css("background","#FFFF66");
		$("#rate_labor_reg").css("background","#FFFF66");
	}else{
		$("#total_rate_labor_reg").css("background","#99FF99");
		$("#rate_labor_reg").css("background","#99FF99");
		if(document.getElementById("rate_labor_reg").value != ""){
			var rlr = parseFloat(document.getElementById("rate_labor_reg").value);
			document.getElementById("total_rate_labor_reg").value = rlr * hwr;
		}else{
			document.getElementById("total_rate_labor_reg").value = 0;
			document.getElementById("rate_labor_reg").value = 0;	
		}
	}
	
	if(document.getElementById("rate_labor_ot").value == "" && hwo > 0.0){
		//document.getElementById("total_rate_labor_ot").value = 0;
		$("#total_rate_labor_ot").css("background","#FFFF66");
		$("#rate_labor_ot").css("background","#FFFF66");
	}else{
		$("#total_rate_labor_ot").css("background","#99FF99");
		$("#rate_labor_ot").css("background","#99FF99");
		if(document.getElementById("rate_labor_ot").value != ""){
			var rlo = parseFloat(document.getElementById("rate_labor_ot").value);
			document.getElementById("total_rate_labor_ot").value = rlo * hwo;
		}else{
			document.getElementById("total_rate_labor_ot").value = 0;
			document.getElementById("rate_labor_ot").value = 0;	
		}
	}
	
	if(document.getElementById("rate_travel_reg").value == "" && htr > 0.0){
		//document.getElementById("total_rate_travel_reg").value = 0;
		$("#total_rate_travel_reg").css("background","#FFFF66");
		$("#rate_travel_reg").css("background","#FFFF66");
	}else{
		$("#total_rate_travel_reg").css("background","#99FF99");
		$("#rate_travel_reg").css("background","#99FF99");
		if(document.getElementById("rate_travel_reg").value != ""){
			var rtr = parseFloat(document.getElementById("rate_travel_reg").value);
			document.getElementById("total_rate_travel_reg").value = rtr * htr;
		}else{
			document.getElementById("total_rate_travel_reg").value = 0;	
			document.getElementById("rate_travel_reg").value = 0;	
		}
	}
	
	if(document.getElementById("rate_travel_ot").value == "" && hto > 0.0){
		//document.getElementById("total_rate_travel_ot").value = 0;
		$("#total_rate_travel_ot").css("background","#FFFF66");
		$("#rate_travel_ot").css("background","#FFFF66");
	}else{
		$("#total_rate_travel_ot").css("background","#99FF99");
		$("#rate_travel_ot").css("background","#99FF99");
		if(document.getElementById("rate_travel_ot").value != ""){
			var rto = parseFloat(document.getElementById("rate_travel_ot").value);
			document.getElementById("total_rate_travel_ot").value = rto * hto;
		}else{
			document.getElementById("total_rate_travel_ot").value = 0;
			document.getElementById("rate_travel_ot").value = 0;	
		}
	}
}
</script>
<?php require_once($footer_include); ?>