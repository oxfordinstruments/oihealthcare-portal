<?php
//Update Completed 11/25/14
$debug = false;
if(isset($_POST['debug'])){
	$debug = true;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/php_utils.php');
$php_utils = new phpUtils();

$t=time();
$sms_comp_word_limt = intval($settings->sms_complaint_word_limit);

d($_POST);

if(isset($_POST['no_invoice'])){
	$sql="UPDATE systems_reports SET
	`valuation_complete`='Y',
	`valuation_uid`='".$_POST['valuation_uid']."',
	`valuation_date`='".date(storef,$t)."',
	`valuation_do_not_invoice`='Y'
	WHERE unique_id='".$_POST['unique_id']."';";
	d($sql);
	if(! $result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
}else{
	if(isset($_POST['update_system'])){
		$sql="SELECT system_ver_unique_id FROM systems_reports WHERE unique_id='".$_POST['unique_id']."';";
		d($sql);
		if(!$resultReport = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
		$rowReport = $resultReport->fetch_assoc();
		$sql="UPDATE systems_base_cont SET `labor_reg_rate`='".preg_replace("#[^0-9/.]*#", "", $_POST['rate_labor_reg'])."',`labor_ot_rate`='".preg_replace("#[^0-9/.]*#", "", $_POST['rate_labor_ot'])."',`travel_reg_rate`='".preg_replace("#[^0-9/.]*#", "", $_POST['rate_travel_reg'])."',`travel_ot_rate`='".preg_replace("#[^0-9/.]*#", "", $_POST['rate_travel_ot'])."' 
		WHERE ver_unique_id='".$rowReport['system_ver_unique_id']."';";
		d($sql);
		if(! $result = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
	}
	
	if(isset($_POST['parts'])){
		$parts = $_POST['parts'];
		foreach($parts as $key => $value) {
			$sql="UPDATE systems_parts SET `price`='".preg_replace("#[^0-9/.]*#", "", $value['price'])."' WHERE id='".$value['id']."';";
			d($sql);
			if(! $result = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
				$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
			}
		}
	}
	
	$sql="UPDATE systems_reports SET 
	`valuation_complete`='Y',
	`valuation_uid`='".$_POST['valuation_uid']."',
	`valuation_date`='".date(storef,$t)."',
	`invoice_labor_reg_rate`='".preg_replace("#[^0-9/.]*#", "", $_POST['rate_labor_reg'])."',
	`invoice_labor_ot_rate`='".preg_replace("#[^0-9/.]*#", "", $_POST['rate_labor_ot'])."',
	`invoice_travel_reg_rate`='".preg_replace("#[^0-9/.]*#", "", $_POST['rate_travel_reg'])."',
	`invoice_travel_ot_rate`='".preg_replace("#[^0-9/.]*#", "", $_POST['rate_travel_ot'])."',
	`invoice_labor_reg`='".preg_replace("#[^0-9/.]*#", "", $_POST['total_rate_labor_reg'])."',
	`invoice_labor_ot`='".preg_replace("#[^0-9/.]*#", "", $_POST['total_rate_labor_ot'])."',
	`invoice_travel_reg`='".preg_replace("#[^0-9/.]*#", "", $_POST['total_rate_travel_reg'])."',
	`invoice_travel_ot`='".preg_replace("#[^0-9/.]*#", "", $_POST['total_rate_travel_ot'])."',
	`invoice_shipping`='".preg_replace("#[^0-9/.]*#", "", $_POST['shipping'])."',
	`invoice_expense`='".preg_replace("#[^0-9/.]*#", "", $_POST['expense'])."'
	WHERE unique_id='".$_POST['unique_id']."';";
	
	d($sql);
	if(! $result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
}

$php_utils->email_action_add($mysqli, "/report/common/report_invoice_send.php",array('rid'=>$_POST['unique_id']),$_SESSION['login']);

?>
<!DOCTYPE html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<?php require_once($js_include);?>

<script type="text/javascript">
//$(document).ready(function() {
// 	$.ajax({
//		type: 'POST',
//		url: 'report_invoice_send.php',
//		data: { rid: '<?php //echo $_POST['unique_id']; ?>//' },
//		success:function(data){
//			// successful request; do something with the data
//			//$("#emailSent").text("Emails Sent");
//		}
//	});
//});
</script>
<script type="text/javascript">
function delayer(){
    window.location = "<?php echo $refering_uri; ?>"
}
</script>
</head>
<body onLoad="setTimeout('delayer()', 1500)">
<?php require_once($header_include); ?>
<div id="OIReportContent">

<h1>Service Report Valuation Complete</h1>
<br>
    <h1><span class="red">Page will return to main page in 3 seconds</span></h1>
</div>
<?php require_once($footer_include); ?>
