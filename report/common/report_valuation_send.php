<?php
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
ini_set('display_errors', 'On');
ignore_user_abort(true);
set_time_limit(30);

if(substr(php_sapi_name(), 0, 3) == 'cli' || empty($_SERVER['REMOTE_ADDR'])) {
	//shell
	$cron = false;
	if(isset($_SERVER['argv'][1]) and $_SERVER['argv'][1] == '--cron'){
		$cron = true;
	}
	$docroot = preg_replace("/(public_html).*/",'public_html',dirname(__FILE__),2);
	$no_define_eol = true;
	define('EOL',PHP_EOL);
} else {
	//webserver
	$cron = false;
	$docroot = $_SERVER['DOCUMENT_ROOT'];
	define('EOL','<br />');
}

$settings = new SimpleXMLElement($docroot.'/settings.xml', null, true);
require_once($docroot.'/define_inc.php');
require_once($docroot.'/mysqlInfo.php');
require_once($docroot.'/log/log.php');
$log = new logger($docroot);

require_once($docroot.'/resources/smarty/libs/Smarty.class.php');
$smarty = new Smarty();
$smarty->setTemplateDir($docroot.$settings->email_templates);
$smarty->setCompileDir($docroot.$settings->email_template_c);
$smarty->setCacheDir($docroot.$settings->email_cache);
$smarty->setConfigDir($docroot.$settings->email_configs);
$smarty->use_sub_dirs = true;
$smarty->force_compile = true;
//$smarty->debugging = true;
$smarty->caching = false;


$debug = false;
if(isset($_GET['debug'])){
	$debug = true;
	if(!isset($_GET['rid'])){
		$log->logerr('report_valuation_send.php',1025);
		exit("No report id");
	}
	if(isset($_GET['send'])){$send = true;}else{$send = false;}
	$rid=$_GET['rid'];
}else{
	$send = true;
	if(!isset($_POST['rid'])){
		exit("No report id");
	}
	$rid=$_POST['rid'];
}

require_once($docroot.'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno) {die('There was an error running the query [' . $mysqli->connect_error . ']');}

//get service report info
$sql="SELECT r.*, u.name AS assigned_engineer_name, u.email as assigned_engineer_email, e.`type` AS equip_type, f.email_list
FROM systems_reports AS r
LEFT JOIN users AS u ON r.assigned_engineer = u.uid
LEFT JOIN systems_base AS sb ON sb.unique_id = r.system_unique_id
LEFT JOIN systems_base_cont AS sbc ON sb.unique_id = sbc.unique_id
LEFT JOIN systems_types AS e ON e.id = sb.system_type
LEFT JOIN facilities AS f ON f.unique_id = sbc.facility_unique_id
WHERE r.unique_id ='$rid';";
if(!$resultReportInfo=$mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	exit("SQL ERROR");
}
$rowReportInfo=$resultReportInfo->fetch_assoc();
if(strtolower($rowReportInfo['invoice_required'])!="y"){
	//No Valuation Required
	exit("OK");
}

//email reciepents
$sql="SELECT u.name,u.email
FROM users AS u
INNER JOIN users_prefs AS upr ON upr.uid = u.uid
INNER JOIN users_pref_id AS prid ON prid.id = upr.pid AND prid.pref = 'pref_rcv_valuation_request'
INNER JOIN users_perms AS up ON up.uid = u.uid
INNER JOIN users_perm_id AS pmid ON pmid.id = up.pid AND pmid.perm = 'perm_rcv_valuation_request'
WHERE u.active = 'y';";
if(!$resultEmailValuation = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	exit("SQL ERROR");
}
if($resultEmailValuation->num_rows == 0){
	//No one wants and email
	exit("OK");
}

$emails_valuation = array();
while($rowEmailValuation = $resultEmailValuation->fetch_assoc()){
	array_push($emails_valuation,array("email"=>$rowEmailValuation['email'],"name"=>$rowEmailValuation['name']));
}
d($emails_valuation);

$smarty->assign('system_nickname',$rowReportInfo['system_nickname']);
$smarty->assign('system_id',$rowReportInfo['system_id']);
$smarty->assign('report_id',$rowReportInfo['report_id']);
$smarty->assign('asn_eng',$rowReportInfo['assigned_engineer_name']);
$smarty->assign('problem_reported',$rowReportInfo['complaint']);
$smarty->assign('service',$rowReportInfo['service']);
$smarty->assign('company_name',$settings->company_name);
$smarty->assign('copyright_date',$settings->copyright_date);
$smarty->assign('email_pics',$settings->email_pics);

require_once($docroot.'/resources/PHPMailer/PHPMailerAutoload.php');

if($debug){
	$emails_valuation = array(array('email'=>(string)$settings->email_support, 'name'=>'Support'));	
}

$mail = new PHPMailer;
$mail->IsSMTP();
$mail->Host = (string)$settings->email_host;
$mail->SMTPAuth = true;
$mail->Username = (string)$settings->email_support;
$mail->Password = (string)$settings->email_password;
$mail->SMTPSecure = 'tls';
$mail->From = (string)$settings->email_support;
$mail->FromName = 'Support '.$settings->short_name.' Portal';
foreach($emails_valuation as $email){
	$mail->AddAddress(trim($email['email']),$email['name']);
}
$mail->AddReplyTo((string)$settings->email_support, 'Support '.$settings->short_name.' Portal');
$mail->AddCC((string)$settings->email_support);
$mail->IsHTML(true);
$mail->Subject = 'Valuation Request for '.$rowReportInfo['system_nickname'].' ( '.$rowReportInfo['system_id'].' ) ';
$mail->Body = $smarty->fetch('valuation_req.tpl');

d($mail);

if($debug){
	echo EOL,$mail->Body,EOL,"<hr>",EOL;	
}

if($send){
	if(!$mail->Send()) {
	   echo 'Email could not be sent.';
	   echo 'Mailer Error: ' . $mail->ErrorInfo;
	   $log->logerr($mail->ErrorInfo,1053,false,basename(__FILE__),__LINE__);
	   exit;
	}else{
		$log->loginfo(implode('; ',array_keys($mail->getAllRecipientAddresses())),200,false,basename(__FILE__),__LINE__);
	}
}else{
	echo "Send False: Not sending email.<br />";
}

//echo "<br /><pre>Email Result\n";
//echo $email_result;
//echo "<br /></pre>";


exit("OK");
?>