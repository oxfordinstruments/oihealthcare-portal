<?php
//Update Completed 11/25/14
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

if(isset($_GET['uid'])){
	$user_id=$_GET['uid'];
}else{
	$log->logerr('report_view.php',1017);	
	header("location:/error.php?n=1017&p=report_view.php");
}
if(isset($_GET['id'])){
	$unique_id=$_GET['id'];
}else{
	$log->logerr('report_view.php',1018);	
	header("location:/error.php?n=1018&p=report_view.php");
}

$sql="SELECT r.*, f.address, f.city, f.state, f.zip, sb.system_serial, u1.name AS eng, u2.name AS assigned_eng, ss.`status` AS equipment_status_name, 
c1.`type` AS contract, c2.`type` AS contract_override, e.name AS equipment_name, e.`type` as equipment_type, sb.system_serial
FROM systems_reports AS r
LEFT JOIN facilities AS f ON f.unique_id = r.facility_unique_id
LEFT JOIN users AS u1 ON r.engineer = u1.uid
LEFT JOIN users AS u2 ON r.assigned_engineer = u2.uid
LEFT JOIN systems_base AS sb ON sb.unique_id = r.system_unique_id
LEFT JOIN systems_base_cont AS sbc ON sb.unique_id = sbc.unique_id
LEFT JOIN systems_status AS ss ON ss.id = sb.`status`
LEFT JOIN misc_contracts AS c1 ON c1.id = sbc.contract_type
LEFT JOIN misc_contracts AS c2 ON c2.id = r.contract_override
LEFT JOIN systems_types AS e ON e.id = sb.system_type
WHERE r.unique_id = '$unique_id';";
if(!$resultReport = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowReport = $resultReport->fetch_assoc();

$uploadBtnVal = "Attach Documents";
$has_docs = false;
$num_docs = "Internal records only. Customer can not view documents.";
if(strtolower($rowReport['has_files']) == 'y'){
	$uploadBtnVal = "View/Modify Documents";
	$has_docs = true;
	$num_docs = "Approximately " . $rowReport['file_count'] . " Documents Attached";
}

$system_id = $rowRport['system_id'];

$t=time();
?>
<!DOCTYPE html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>

<link href="/resources/css/form_report_view.css" rel="stylesheet" type="text/css" media="screen">

<?php require_once($js_include);?>

<script src="/resources/js/jquery-ui/jquery-ui.min.js"></script>
<script src="/resources/js/jquery.ui.widget.js"></script>
<script src="/resources/js/jquery.ui.datepicker.js"></script>
<script src="/resources/js/jquery-dynamic-form_MODIFIED.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	
	$(".button_jquery_download").button({
		icons: {
			primary: "ui-icon-disk"
		}
	});
	
	$(".button_jquery_edit").button({
		icons: {
			primary: "ui-icon-pencil"
		}
	});
	
	$(".button_jquery_save").button({
		icons: {
			primary: "ui-icon-disk"
		}
	});
	
	$(".iframeUpload").fancybox({
			'type'			: 'iframe',
			'height'		: 600,
			'fitToView'		: true,
			'maxWidth'		: 900,
			'maxHeight'		: 600,
			'autoSize'		: false,
			'closeBtn'		: true,
			'margin'		: [5,5,5,5]
	});
	
	var notesTableDT = $('#notesTable').dataTable({
		"bJQueryUI": true,
		"searching": false,
		"lengthChange": false,
		"sPaginationType": "full_numbers",
		"aaSorting": [],
		"ajax": {
			'type': 'POST',
			'url': 'systems_service_journal.php',
			'data': {
			   get: 'true',
			   unique_id: '<?php echo $unique_id; ?>'
			}
		}
	});
	
});
</script>
</head>
<body>
<?php require_once($header_include); ?>
<div id="OIReportContent"> 
<div id="stylized" class="myform">
<form id="form" name="form" method="post" action="#">
<div id="srHeaderDiv"><h1>View Report</h1></div>
<div id="main"><!-- do not remove -->
<div id="srInfoDiv">
    <table id="srInfoTable" class="srTable">
        <tr>
          <td class="rowLabel">System&nbsp;ID: </td>
          <td class="rowData"><?php echo $rowReport['system_id']; ?></td>
          <td class="rowLabel">System Nickname: </td>
          <td class="rowData"><?php echo $rowReport['system_nickname']; ?></td>
        </tr>
        <tr>
          <td class="rowLabel">Address: </td>
          <td colspan="4" class="rowData"><?php echo $rowReport['address'] ."&nbsp;&nbsp;&nbsp;". $rowReport['city'] .", ". $rowReport['state'] ."  ". $rowReport['zip'];?></td>
        </tr>
        <tr>
          <td class="rowLabel">Serial&nbsp;Number: </td>
          <td class="rowData"><?php echo $rowReport['system_serial']; ?></td>
          <td class="rowLabel">System: </td>
          <td class="rowData"><?php echo $rowReport['equipment_name']; ?></td>
        </tr>
        <tr>
          <td class="rowLabel">Contract&nbsp;Type: </td>
          <td class="rowData"><?php echo $rowReport['contract']; ?></td>
          <td class="rowLabel">Assigned&nbsp;Engineer: </td>
          <td class="rowData"><?php echo $rowReport['assigned_eng']; ?></td>
        </tr>
        <tr>
          <td class="rowLabel">Service&nbsp;Request&nbsp;Num: </td>
          <td class="rowData"><?php echo $rowReport['report_id']; ?></td>
          <td class="rowLabel">Primary&nbsp;Engineer: </td>
          <td class="rowData"><?php echo $rowReport['eng']; ?></td>
        </tr>
    </table>
</div>

<div id="srDataDiv">
    <table id="srDataTable" class="srTable">
        <tr>
          <td class="rowLabel">Report Date:</td>
          <td class="rowData"><input type="text" id="report_date" name="report_date" class="findme" readonly value="<?php echo date(phpdispfd,strtotime($rowReport['date']));?>" /></td>
          <td class="rowLabel">Purchase Order:</td>
          <td class="rowData"><input type="text" id="po_number" name="po_number" readonly value="<?php echo $rowReport['po_number'];?>" /></td>
        </tr>
        <tr>
          <td class="rowLabel">Service Engineer:</td>
          <td class="rowData"><input readonly name="engineer" id="engineer" value="<?php echo $rowReport['assigned_eng'] ?>" /></td>
          <td class="rowLabel">Contract Type Override:</td>
          <td class="rowData"><input readonly name="contract_override" id="contract_overide" value="<?php echo $rowReport['contract_override'] ?>" /></td>
        </tr>
        <tr>
          <td class="rowLabel">Preventive Maintenance:</td>
          <td class="rowData"><input id="pm" name="pm" readonly value="<?php if(strtolower($rowReport['pm'])=="y"){echo "Yes";}else{echo "No";} ?>"/></td>
          <td class="rowLabel">Hours System was down:</td>
          <td class="rowData"><input type="text" id="down_time" readonly name="down_time" <?php echo "value=\"".$rowReport['downtime']."\"";?>/></td>
        </tr>
        <tr>
          <td class="rowLabel">System Status:</td>
          <td class="rowData"><input readonly name="equipment_status" id="equipment_status" value="<?php echo $rowReport['equipment_status_name'] ?>" /></td>
		  <td class="rowLabel">Phone Fix:</td>
		  <td class="rowData"><input id="phone_fix" name="phone_fix" readonly value="<?php if(strtolower($rowReport['phone_fix'])=="y"){echo "Yes";}else{echo "No";} ?>"/></td>      
        </tr>
    </table>
</div>

<div id="srDataDivCT" <?php if($rowReport['equipment_type'] != "CT"){ echo" style='display:none'";} ?> >
    <table id="srDataTable">
        <tr>
          <td class="rowLabel">Slice/mAs Count:</td>
          <td class="rowData"><input type="text" id="slice_mas" readonly name="slice_mas" <?php echo "value=\"".$rowReport['slice_mas_count']."\"";?>/></td>
          <td class="rowLabel">Gantry Revolutions:</td>
          <td class="rowData"><input type="text" id="gantry_rev" readonly name="gantry_rev" <?php echo "value=\"".$rowReport['gantry_rev']."\"";?>/></td>
        </tr>
    </table>
</div>

<div id="srDataDivMR" <?php if($rowReport['equipment_type'] != "MR"){ echo" style='display:none'";} ?>>
    <table id="srDataTable">
        <tr>
          <td class="rowLabel">Helium Level:</td>
          <td class="rowData"><input type="text" id="helium" readonly name="helium" <?php echo "value=\"".$rowReport['helium_level']."\"";?>/></td>
          <td class="rowLabel">Vessel Pressure:</td>
          <td class="rowData"><input type="text" id="pressure" readonly name="pressure" <?php echo "value=\"".$rowReport['vessel_pressure']."\"";?>/></td>
        </tr>
        <tr>
          <td class="rowLabel">Compressor Pressure:</td>
          <td class="rowData"><input type="text" id="compressor_pressure" readonly name="compressor_pressure" <?php echo "value=\"".$rowReport['compressor_pressure']."\"";?>/></td>
          <td class="rowLabel">Compressor Hours:</td>
          <td class="rowData"><input type="text" id="compressor_hours" readonly name="compressor_hours" <?php echo "value=\"".$rowReport['compressor_hours']."\"";?>/></td>
        </tr>
        <tr>
          <td class="rowLabel">Recon RUO:</td>
          <td class="rowData"><input type="text" id="recon_ruo" readonly name="recon_ruo" <?php echo "value=\"".$rowReport['recon_ruo']."\"";?>/></td>
          <td class="rowLabel">Coldhead RUO:</td>
          <td class="rowData"><input type="text" id="coldhead_ruo" readonly name="coldhead_ruo" <?php echo "value=\"".$rowReport['cold_ruo']."\"";?>/></td>
        </tr>
    </table>
</div>

<div id="srComplaintDiv">
    <table id="srComplaintTable" class="srTable">
        <tr>
          <td class="rowLabel">Customer Complaint</td>
        </tr>
        <tr>
          <td class="rowData"><textarea id="complaint" name="complaint" maxlength="2000" readonly><?php echo $rowReport['complaint']; ?></textarea></td>
        </tr>
    </table>
</div>

<div id="srServiceDiv">
    <table id="srServiceTable" class="srTable">
        <tr>
          <td class="rowLabel">Service Performed</td>
        </tr>
        <tr>
          <td class="rowData"><textarea id="service" name="service" maxlength="4000" readonly><?php echo $rowReport['service'];?></textarea></td>
        </tr>
    </table>
</div>

<?php if($_SESSION['group'] != "3"){ ?>
	<div id="srNotesDiv">
		<table id="srNotesTable" class="srTable">
			<tr>
			  <td class="rowLabel">Next Service Call Reminder</td>
			</tr>
			<tr>
			  <td class="rowData"><textarea id="notes" name="notes" maxlength="2000" readonly><?php echo $rowReport['notes'];?></textarea></td>
			</tr>
		</table>
	</div>
<?php } ?>

<?php if($_SESSION['group'] == "1"){?>
<div id="srFilesDiv">
	<table id="srFilesTable" class="srTable">
		<tr>
			<td class="rowLabel" colspan="2">Attached Documents</td>
		</tr>
		<tr>
			<td class="rowData" width="50%"><div class="srBottomBtn"><a href="/resources/kcfinder/kcfinder.php?unique_id=<?php echo $unique_id; ?>&table=systems_reports_files" class="button_jquery_save iframeUpload" id="attachDocs"><?php echo $uploadBtnVal; ?></a></div></td>
			<td class="rowData" style="text-align:center; color:#f79548"><?php echo $num_docs; ?></td>
		</tr>
	</table>
</div>
<?php } ?>
<?php if($_SESSION['group'] != "3"){ ?>
<table id="srNotesTable" class="srTable">
	<tr>
		<td width="100%">
			<div id="notesFrame" style="text-align:center">
				<div id="notesFrame">     
				<div style="rowLabel"> Service Journal</div>
				<div id="notesDiv">
					<table width="100%" id="notesTable" >
						<thead>
							<tr>
								<th width="15%">Date</th> 
								<th>Note</th>
								<th width="10%">Status</th>
								<th width="15%">User</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			  </div>
			</div>
		</td>
	</tr>
</table>
<?php } ?>
</div>
<?php
    $sql="SELECT * FROM systems_hours WHERE unique_id = '".$unique_id."' ORDER BY `date` ASC;";
	if(!$resultHours = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$numberHours = $resultHours->num_rows;	
?>
<div id="srHoursDiv" <?php if(intval($numberHours) <= 0){echo " style=\"display:none\"";}?>>
	<table id="srHoursTable" class="srTable">
    <tr>
        <th scope="col">date</th>
        <th colspan="2" scope="col">Hours Worked</th>
        <th colspan="2" scope="col">Hours Traveld</th>
	    <th colspan="1" scope="col">Phone Hours</th>
        </tr>
    <tr>
    	<td class="rowLabel"></td>
        <td class="rowLabel">Reg</td>
        <td class="rowLabel">OT</td>
        <td class="rowLabel">Reg</td>
        <td class="rowLabel">OT</td>
	    <td class="rowLabel">&nbsp;</td>
    </tr>
    <?php
	$hwr=0;
	$hwo=0;
	$htr=0;
	$hto=0;
	$hp=0;
	if(intval($numberHours) > 0){

		while($rowHours = $resultHours->fetch_assoc())
		{
			echo "<tr id=\"hours\">\n";
			echo "<td class=\"rowData\"><input readonly type=\"text\" id=\"hours_date\" name=\"hours_date\" value=\"".date(phpdispfd,strtotime($rowHours['date']))."\" /></td>\n";
			echo "<td class=\"rowData\"><input readonly type=\"text\" id=\"hours_wreg\" name=\"hours_wreg\" value=\"".$rowHours['reg_labor']."\" /></td>\n";
			echo "<td class=\"rowData\"><input readonly type=\"text\" id=\"hours_wot\" name=\"hours_wot\" value=\"".$rowHours['ot_labor']."\" /></td>\n";
			echo "<td class=\"rowData\"><input readonly type=\"text\" id=\"hours_treg\" name=\"hours_treg\" value=\"".$rowHours['reg_travel']."\" /></td>\n";
			echo "<td class=\"rowData\"><input readonly type=\"text\" id=\"hours_tot\" name=\"hours_tot\" value=\"".$rowHours['ot_travel']."\" /></td>\n";
			echo "<td class=\"rowData\"><input readonly type=\"text\" id=\"hours_phone\" name=\"hours_phone\" value=\"".$rowHours['phone']."\" /></td>\n";
			echo "</tr>\n";
			$hwr += floatval($rowHours['reg_labor']);
			$hwo += floatval($rowHours['ot_labor']);
			$htr += floatval($rowHours['reg_travel']);
			$hto += floatval($rowHours['ot_travel']);
			$hp += floatval($rowHours['phone']);
		}
		
		echo "<tr id=\"hours\">\n";
		echo "<td class=\"rowData\" style=\"text-align:center\">Totals</td>\n";
		echo "<td class=\"rowData\" style=\"text-align:center\">".$hwr."</td>\n";
		echo "<td class=\"rowData\" style=\"text-align:center\">".$hwo."</td>\n";
		echo "<td class=\"rowData\" style=\"text-align:center\">".$htr."</td>\n";
		echo "<td class=\"rowData\" style=\"text-align:center\">".$hto."</td>\n";
		echo "<td class=\"rowData\" style=\"text-align:center\">".$hp."</td>\n";
		echo "</tr>\n";
	}    
	?>
</table>
</div>

<?php
	$sql="SELECT * FROM systems_parts WHERE unique_id = '".$unique_id."';";
	if(!$resultParts = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$numberParts = $resultParts->num_rows;	
?>
<div id="srPartsDiv"<?php if(intval($numberParts) <= 0){echo " style=\"display:none\"";}?>>
	<table id="srPartsTable" class="srTable">
    <tr>
        <th scope="col">Part Number</th>
        <th scope="col">Description</th>
        <th scope="col">Quanity</th>
    </tr>
    <?php
	if(intval($numberParts) > 0){

		while($rowParts = $resultParts->fetch_assoc())
		{			
			echo "<tr id=\"parts\">\n";
			echo "<td class=\"rowData\"><input readonly type=\"text\" id=\"parts_number\" name=\"parts_number\" value=\"".$rowParts['number']."\" /></td>\n";
			echo "<td class=\"rowData\"><input readonly type=\"text\" id=\"parts_description\" name=\"parts_description\" value=\"".$rowParts['description']."\" /></td>\n";
			echo "<td class=\"rowData\"><input readonly type=\"text\" id=\"parts_quanity\" name=\"parts_quanity\" value=\"".$rowParts['qty']."\" /></td>\n";
    		echo "</tr>\n";
		}
	}
	?>
</table>


</div>

<div id="srFooterDiv">
	<?php if(!isset($_GET['view'])) {?>
	<div class="srBottomBtn"><a class="button_jquery_download" target="new" href="<?php echo "/resources/print_sr/f70-14-15_rev-c.php?unique_id=$unique_id"; ?>">Download PDF</a></div>
	<?php } if(isset($_SESSION['perms']['perm_edit_reports']) and !isset($_GET['view'])){ ?>
<!--		<div class="srBottomBtn"><a class="button_jquery_edit" href="--><?php //echo "/report/common/report_new_edit.php?id=$unique_id&uid=$myusername"; ?><!--">Edit Report</a></div>-->
	<?php } ?>
</div>

</form>
</div> 
</div>
<?php require_once($footer_include); ?>
