<?php
//Update Completed 11/25/14

$debug = false;
if(isset($_GET['debug'])){
	$debug = true;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

if(isset($_GET['uid'])){
	$user_id=$_GET['uid'];
}else{
	header("location:/error.php?n=1017&p=report_view_mini.php");
}
if(isset($_GET['id'])){
	$unique_id=$_GET['id'];
}else{
	header("location:/error.php?n=1018&p=report_view_mini.php");
}

$sql="SELECT * FROM systems_requests WHERE unique_id = '".$unique_id."';";
$resultRequest = $mysqli->query($sql);
$rowRequest = $resultRequest->fetch_assoc();

$sql="SELECT * FROM systems_reports WHERE unique_id = '".$unique_id."';";
$resultReport = $mysqli->query($sql);
$rowReport = $resultReport->fetch_assoc();


$sql="SELECT sb.*, sbc.*, f.address, f.city, f.state, f.zip
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
LEFT JOIN facilities AS f ON f.unique_id = sbc.facility_unique_id
WHERE sb.unique_id = '".$rowRequest['system_unique_id']."';";
d($sql);
$resultSystem = $mysqli->query($sql);
$rowSystem = $resultSystem->fetch_assoc();

$sql="SELECT * FROM users WHERE `uid` = '".$user_id."';";
$resultUser = $mysqli->query($sql);
$rowUser = $resultUser->fetch_assoc();

$sql="SELECT * FROM users WHERE `uid` = '".$rowRequest['engineer']."';";
$resultAssigned = $mysqli->query($sql);
$rowAssigned = $resultAssigned->fetch_assoc();

$sql="SELECT a.uid,u.name,u.email,u.cell,u.carrier,u.pref_rcv_sms
FROM systems_assigned_pri AS a
LEFT JOIN users AS u ON a.uid = u.uid
WHERE a.system_ver_unique_id='".$rowSystem['ver_unique_id']."';";
d($sql);
$resultEngPri = $mysqli->query($sql);
$rowEngPri = $resultEngPri->fetch_assoc();

$sql="SELECT a.uid,u.name,u.email,u.cell,u.carrier,u.pref_rcv_sms
FROM systems_assigned_sec AS a
LEFT JOIN users AS u ON a.uid = u.uid
WHERE a.system_ver_unique_id='".$rowSystem['ver_unique_id']."';";
d($sql);
$resultEngSec = $mysqli->query($sql);
$rowEngSec = $resultEngSec->fetch_assoc();

$sql="SELECT * FROM misc_contracts WHERE id = '".$rowSystem['contract_type']."' LIMIT 1;";
$resultContract = $mysqli->query($sql);

$sql="SELECT * FROM misc_contracts;";
$resultContracts = $mysqli->query($sql);

$sql="SELECT * FROM systems_types WHERE id = '".$rowSystem['system_type']."' LIMIT 1;";
$resultEquipment = $mysqli->query($sql);

$sql="SELECT * FROM users WHERE role_engineer = 'y' OR role_contractor = 'y';";
$resultEngineer = $mysqli->query($sql);

$sql="SELECT * FROM systems_status;";
$resultStatus = $mysqli->query($sql);

$t=time();

?>
<!DOCTYPE html>
<html>
<head>
<?php require_once($head_include);?>
<?php require_once($css_include);?>

<link href="/resources/css/form_report_view.css" rel="stylesheet" type="text/css" media="screen">

<?php require_once($js_include);?>

<script src="/resources/js/jquery-1.7.2.min.js"></script>
<script src="/resources/js/jquery-ui/jquery-ui.min.js"></script>
<script src="/resources/js/jquery.ui.widget.js"></script>
<script src="/resources/js/jquery.ui.datepicker.js"></script>
<script src="/resources/js/jquery-dynamic-form_MODIFIED.js"></script>

</head>
<body>
<div id="stylized" class="myform">
<form id="form" name="form" method="post" action="#">
<div id="srHeaderDiv"><h1>View Report</h1></div>
<div id="main"><!-- do not remove -->
<div id="srInfoDiv">
    <table id="srInfoTable" class="srTable">
        <tr>
          <td class="rowLabel">System&nbsp;ID: </td>
          <td class="rowData"><?php echo $rowSystem['system_id']; ?></td>
          <td class="rowLabel">System Nickname: </td>
          <td class="rowData"><?php echo $rowSystem['nickname']; ?></td>
        </tr>
        <tr>
          <td class="rowLabel">Address: </td>
          <td colspan="4" class="rowData"><?php echo $rowSystem['address'] ."&nbsp;&nbsp;&nbsp;". $rowSystem['city'] .", ". $rowSystem['state'] ."  ". $rowSystem['zip'];?></td>
        </tr>
        <tr>
          <td class="rowLabel">Serial&nbsp;Number: </td>
          <td class="rowData"><?php echo $rowSystem['system_serial']; ?></td>
          <td class="rowLabel">System: </td>
          <td class="rowData"><?php echo $resultEquipment->fetch_object()->name; ?></td>
        </tr>
        <tr>
          <td class="rowLabel">Contract&nbsp;Type: </td>
          <td class="rowData"><?php echo $resultContract->fetch_object()->type; ?></td>
          <td class="rowLabel">Assigned&nbsp;Engineer: </td>
          <td class="rowData"><?php echo $rowAssigned['name']; ?></td>
        </tr>
        <tr>
          <td class="rowLabel">Service&nbsp;Request&nbsp;Num: </td>
          <td class="rowData"><?php echo $rowRequest['request_num']; ?></td>
          <td class="rowLabel">Primary&nbsp;Engineer: </td>
          <td class="rowData"><?php echo $rowSystem['engineer_pri']; ?></td>
        </tr>
    </table>
</div>

<div id="srDataDiv">
    <table id="srDataTable" class="srTable">
        <tr>
          <td class="rowLabel">Report Date:</td>
          <td class="rowData"><input type="date" id="report_date" name="report_date" class="findme" readonly value="<?php echo $rowReport['date'];?>" /></td>
          <td class="rowLabel">Purchase Order:</td>
          <td class="rowData"><input type="number" id="po_number" name="po_number" readonly value="<?php echo $rowReport['po_number'];?>" /></td>
        </tr>
        <tr>
          <td class="rowLabel">Service Engineer:</td>
          <td class="rowData">
          <?php
			while($rowEngineer = $resultEngineer->fetch_assoc())
  			{				
				if(strtolower($rowEngineer['uid']) == strtolower($rowReport['engineer'])){
					echo "<input readonly name=\"engineer\" id=\"engineer\" value=\"".$rowEngineer['name']."\" />";
				}				
  			}
          ?></td>
          <td class="rowLabel">Contract Type Override:</td>
          <td class="rowData">
          <?php
		  	$foundContract = false;
			while($rowContracts = $resultContracts->fetch_assoc())
  			{				
				if($rowReport['contract_override']==$rowContracts['id']){
					echo "<input readonly name=\"contract_override\" id=\"contract_overide\" value=\"". $rowContracts['type'] ."\" />";
					$foundContract = true;
				}				
  			}
			if(!$foundContract){echo "<input readonly name=\"contract_override\" id=\"contract_overide\" />";}
          ?></td>
        </tr>
        <tr>
          <td class="rowLabel">Preventave Maintaince:</td>
          <td class="rowData"><input id="pm" name="pm" readonly value="<?php if(strtolower($rowReport['pm'])=="y"){echo "Yes";}else{echo "No";} ?>"/></td>
          <td class="rowLabel">Hours System was down:</td>
          <td class="rowData"><input type="text" id="down_time" readonly name="down_time" <?php echo "value=\"".$rowReport['downtime']."\"";?>/></td>
        </tr>
        <tr>
          <td class="rowLabel">System Status:</td>
          <td class="rowData">
          <?php
			while($rowStatus = $resultStatus->fetch_assoc())
  			{				
				if($rowReport['equipment_status']==$rowStatus['id']){
					echo "<input readonly name=\"equipment_status\" id=\"equipment_status\" value=\"". $rowStatus['status'] ."\" />";
				}				
  			}
          ?></td>          
        </tr>
    </table>
</div>

<div id="srDataDivCT" <?php if($resultEquipment->fetch_object()->type != "CT"){ echo" style='display:none'";} ?> >
    <table id="srDataTable">
        <tr>
          <td class="rowLabel">Slice/mAs Count:</td>
          <td class="rowData"><input type="number" id="slice_mas" readonly name="slice_mas" <?php echo "value=\"".$rowReport['slice_mas_count']."\"";?>/></td>
          <td class="rowLabel">Gantry Revolutions:</td>
          <td class="rowData"><input type="number" id="gantry_rev" readonly name="gantry_rev" <?php echo "value=\"".$rowReport['gantry_rev']."\"";?>/></td>
        </tr>
    </table>
</div>

<div id="srDataDivMR" <?php if($resultEquipment->fetch_object()->type != "MR"){ echo" style='display:none'";} ?>>
    <table id="srDataTable">
        <tr>
          <td class="rowLabel">Helium Level:</td>
          <td class="rowData"><input type="number" id="helium" readonly name="helium" <?php echo "value=\"".$rowReport['helium_level']."\"";?>/></td>
          <td class="rowLabel">Vessel Pressure:</td>
          <td class="rowData"><input type="number" id="pressure" readonly name="pressure" <?php echo "value=\"".$rowReport['vessel_pressure']."\"";?>/></td>
        </tr>
        <tr>
          <td class="rowLabel">Compressor Pressure:</td>
          <td class="rowData"><input type="number" id="compressor_pressure" readonly name="compressor_pressure" <?php echo "value=\"".$rowReport['compressor_pressure']."\"";?>/></td>
          <td class="rowLabel">Compressor Hours:</td>
          <td class="rowData"><input type="number" id="compressor_hours" readonly name="compressor_hours" <?php echo "value=\"".$rowReport['compressor_pressure']."\"";?>/></td>
        </tr>
        <tr>
          <td class="rowLabel">Recon RUO:</td>
          <td class="rowData"><input type="number" id="recon_ruo" readonly name="recon_ruo" <?php echo "value=\"".$rowReport['recon_ruo']."\"";?>/></td>
          <td class="rowLabel">Coldhead RUO:</td>
          <td class="rowData"><input type="number" id="coldhead_ruo" readonly name="coldhead_ruo" <?php echo "value=\"".$rowReport['cold_ruo']."\"";?>/></td>
        </tr>
    </table>
</div>

<div id="srComplaintDiv">
    <table id="srComplaintTable" class="srTable">
        <tr>
          <td class="rowLabel">Customer Complaint</td>
        </tr>
        <tr>
          <td class="rowData"><textarea id="complaint" name="complaint" maxlength="2000" readonly><?php echo $rowReport['complaint']; ?></textarea></td>
        </tr>
    </table>
</div>

<div id="srServiceDiv">
    <table id="srServiceTable" class="srTable">
        <tr>
          <td class="rowLabel">Service Performed</td>
        </tr>
        <tr>
          <td class="rowData"><textarea id="service" name="service" maxlength="4000" readonly><?php echo $rowReport['service'];?></textarea></td>
        </tr>
    </table>
</div>
<div style="display:none">
	<input type="checkbox" id="finalize" name="finalize" />
    <input type="checkbox" id="edit" name="edit" />
    <input type="text" id="user_id" name="user_id" value="<?php echo $rowUser['uid']; ?>" />
    <input type="text" id="system_id" name="system_id" value="<?php echo $rowSystem['system_id']; ?>" />
    <input type="text" id="system_name" name="system_name" value="<?php echo $rowSystem['nickname']; ?>" />
    <input type="text" id="engineer_assigned" name="engineer_assigned" value="<?php echo $rowRequest['engineer']; ?>" />
    <input type="text" id="report_id" name="report_id" value="<?php echo $rowRequest['request_num']; ?>" />
    <input type="text" id="unique_id" name="unique_id" value="<?php echo $rowRequest['unique_id']; ?>" />
</div>
</div>
<?php
    $sql="SELECT * FROM systems_hours WHERE unique_id = '".$unique_id."' ORDER BY str_to_date(`date`,'%m/%d/%Y') ASC;";
	$resultHours = $mysqli->query($sql);
	$numberHours = $resultHours->num_rows;
	
?>
<div id="srHoursDiv" <?php if(intval($numberHours) <= 0){echo " style=\"display:none\"";}?>>
	<table id="srHoursTable" class="srTable">
    <tr>
        <th scope="col">date</th>
        <th colspan="2" scope="col">Hours Worked</th>
        <th colspan="2" scope="col">Hours Traveld</th>
        </tr>
    <tr>
    	<td class="rowLabel"></td>
        <td class="rowLabel">Reg</td>
        <td class="rowLabel">OT</td>
        <td class="rowLabel">Reg</td>
        <td class="rowLabel">OT</td>
    </tr>
    <?php
	$hwr=0;
	$hwo=0;
	$htr=0;
	$hto=0;
	if(intval($numberHours) > 0){

		while($rowHours = $resultHours->fetch_assoc())
		{
			echo "<tr id=\"hours\">\n";
			echo "<td class=\"rowData\"><input readonly type=\"date\" id=\"hours_date\" name=\"hours_date\" value=\"".$rowHours['date']."\" /></td>\n";
			echo "<td class=\"rowData\"><input readonly type=\"number\" id=\"hours_wreg\" name=\"hours_wreg\" value=\"".$rowHours['reg_labor']."\" /></td>\n";
			echo "<td class=\"rowData\"><input readonly type=\"number\" id=\"hours_wot\" name=\"hours_wot\" value=\"".$rowHours['ot_labor']."\" /></td>\n";
			echo "<td class=\"rowData\"><input readonly type=\"number\" id=\"hours_treg\" name=\"hours_treg\" value=\"".$rowHours['reg_travel']."\" /></td>\n";
			echo "<td class=\"rowData\"><input readonly type=\"number\" id=\"hours_tot\" name=\"hours_tot\" value=\"".$rowHours['ot_travel']."\" /></td>\n";
			echo "</tr>\n";
			$hwr += intval($rowHours['reg_labor']);
			$hwo += intval($rowHours['ot_labor']);
			$htr += intval($rowHours['reg_travel']);
			$hto += intval($rowHours['ot_travel']);
		}
		
		echo "<tr id=\"hours\">\n";
		echo "<td class=\"rowData\" style=\"text-align:center\">Totals</td>\n";
		echo "<td class=\"rowData\" style=\"text-align:center\">".$hwr."</td>\n";
		echo "<td class=\"rowData\" style=\"text-align:center\">".$hwo."</td>\n";
		echo "<td class=\"rowData\" style=\"text-align:center\">".$htr."</td>\n";
		echo "<td class=\"rowData\" style=\"text-align:center\">".$hto."</td>\n";
		echo "</tr>\n";
	}    
	?>
</table>
</div>

<?php
	$sql="SELECT * FROM systems_parts WHERE unique_id = '".$unique_id."';";
	$resultParts = $mysqli->query($sql);
	$numberParts = $resultParts->num_rows;
	
?>
<div id="srPartsDiv"<?php if(intval($numberParts) <= 0){echo " style=\"display:none\"";}?>>
	<table id="srPartsTable" class="srTable">
    <tr>
        <th scope="col">Part Number</th>
        <th scope="col">Description</th>
        <th scope="col">Quanity</th>
    </tr>
    <?php
	if(intval($numberParts) > 0){

		while($rowParts = $resultParts->fetch_assoc())
		{			
			echo "<tr id=\"parts\">\n";
			echo "<td class=\"rowData\"><input readonly type=\"text\" id=\"parts_number\" name=\"parts_number\" value=\"".$rowParts['number']."\" /></td>\n";
			echo "<td class=\"rowData\"><input readonly type=\"text\" id=\"parts_description\" name=\"parts_description\" value=\"".$rowParts['description']."\" /></td>\n";
			echo "<td class=\"rowData\"><input readonly type=\"number\" id=\"parts_quanity\" name=\"parts_quanity\" value=\"".$rowParts['qty']."\" /></td>\n";
    		echo "</tr>\n";
		}
	}
	?>
</table>


</div>
</form>
</div>
</body>
</html>
