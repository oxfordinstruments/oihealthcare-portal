<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

if(!isset($_GET['report'])){
	$log->logerr('GET report was not set',4,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',4,true,basename(__FILE__),__LINE__);
}else{
	$report = ucwords(str_replace('_',' ',$_GET['report']));
}
$year = intval(date('Y',time()));

?>

<!doctype html>
<html xmlns="http://www.w3.org/1999/html">
	<head>
		<?php require_once($head_include);?>
		<?php require_once($css_include);?>
		<link rel="stylesheet" type="text/css" href="/resources/css/portal.css">
		<link rel="stylesheet" type="text/css" href="/resources/css/form_request.css">
		<?php require_once($js_include);?>
		<script type="text/javascript">
		$(document).ready(function() {
			$("select").chosen({
				no_results_text: "Oops, nothing found!",
				disable_search_threshold: 10,
				placeholder_text_single: '  '
			});

			$(".button_jquery_new").button({
				icons: {
					primary: "ui-icon-document"
				}
			});

		});

		function submitcheck() {
			switch ("<?php echo $_GET['report']; ?>"){
				case 'pm_contract_fulfill':
					emailreport('/report/scripts/pm_contract_fulfill.php?single&year='+$("#year").val());
					break;
				case 'tm_reports_fy':
					excelreport('/report/common/reports/tm_reports_fy.php', {'year': $("#year").val()});
					break;
				case 'systems_reports_fy':
					excelreport('/report/common/reports/systems_reports_fy.php', {'year': $("#year").val()});
					break;
				default:
					console.log("UNKNOWN REPORT");
					break;
			}
		}


		function emailreport(href){
			$.ajax({
				url: href,
				beforeSend: function( xhr ) {
					alert("Sending email to your email address...");
				},
				success: function( data, textStatus, jqXHR ) {
					alert( "Email sent to your email address" );
				},
				error: function( req, status, err ) {
					console.log( 'report email failed" ', status, err );
					alert ("FAILED TO SEND EMAIL");
				}
			});
		}

		function excelreport(href, params) {
			var formid = makeid();
			//console.log(formid);
			var form = document.createElement("form");
			form.setAttribute("method", "post");
			form.setAttribute("action", href);
			form.setAttribute("id", formid);
			form._submit_function_ = form.submit;

			for(var key in params) {
				if(params.hasOwnProperty(key)) {
					var hiddenField = document.createElement("input");
					hiddenField.setAttribute("type", "hidden");
					hiddenField.setAttribute("name", key);
					hiddenField.setAttribute("value", params[key]);

					form.appendChild(hiddenField);
				}
			}

			document.body.appendChild(form);
			//console.log(form);
			form._submit_function_();
			remove(formid);
			alert("Please Wait. Gathering your report...");

		}

		function makeid()
		{
			var text = "";
			var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

			for( var i=0; i < 10; i++ )
				text += possible.charAt(Math.floor(Math.random() * possible.length));

			return text;
		}

		function remove(id) {
			var elem = document.getElementById(id);
			return elem.parentNode.removeChild(elem);
		}
		</script>
	</head>
	<body>
		<div id="allDiv" style="width:99%;">
			<div style="text-align: center; margin-top: 10px; margin-bottom: 100px;">
				<p><h1>Select the year to run the report on.</h1></p>
				<select id="year" style="width: 100px;">
					<?php
					for($i=$year; $i >= intval(date('Y',strtotime($settings->rollout_date))); $i--){
						echo "<option>".$i."</option>";
					}
					?>
				</select><br><br>
				<div class="srBottomBtn"><a class="button_jquery_new" id="run_btn" onClick="submitcheck()">Run Report <?php echo $report; ?></a></div>
			</div>
		</div>
	</body>
</html>