<?php
//Update Completed 11/25/14
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

$debug=false;
if(isset($_GET['debug'])){
	$debug=true;
}

?>
<!DOCTYPE html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>


<link rel="stylesheet" type="text/css" href="/resources/dhtmlx/dhtmlxGrid/dhtmlxgrid.css">
<link rel="stylesheet" type="text/css" href="/resources/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">


<?php require_once($js_include);?>


<script  src="/resources/dhtmlx/dhtmlxGrid/dhtmlxcommon.js"></script>
<script  src="/resources/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>        
<script  src="/resources/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>    
<script  src="/resources/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_drag.js"></script>
<script  src="/resources/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script> 
<script type="text/javascript">
	function emailreport(href){
		$.ajax({
			url: href,
			beforeSend: function( xhr ) {
				alert("Sending email to your email address...");
			},
			success: function( data, textStatus, jqXHR ) {
				alert( "Email sent to your email address" );
			},
			error: function( req, status, err ) {
				console.log( 'report email failed" ', status, err );
				alert ("FAILED TO SEND EMAIL");
			}	
		});
	}

</script>

<style type="text/css">
.reportLnk  {
	color: #1B2673;
}
.unstyledListBullet {
	height: 0px;
	width: 0px;
	background-image: url(../../resources/images/common/bullet-round-orange.png);
	padding: 4px;
	background-repeat: no-repeat;
	background-position: right center;
	margin-left: 10px;
}
</style>
</head>
<body>
<?php require_once($header_include); ?>

<div id="OIReportContent">
	<div style="text-align:center">
		<h1>Reports</h1>
	</div>
	<br>

	<div class="reports" style="width:50%; margin-left:auto; margin-right:auto;">
		<div>System Related</div>
		<ul class="unstyledList">
			<li> <img class="unstyledListBullet" src="../../resources/images/common/blocker.png" /><a class="reportLnk ctaLink ctaLink" id="sites_map_rpt" href="/report/common/reports/systems_map.php" target="new"> <span>Systems Map</span> </a> </li>
			<li> <img class="unstyledListBullet" src="../../resources/images/common/blocker.png" /><a class="reportLnk ctaLink" id="contract_warranty_rpt" href="/report/common/reports/contract_warranties.php"> <span>Active Systems Contract and Warranty Dates (Excel)</span> </a> </li>
			<li> <img class="unstyledListBullet" src="../../resources/images/common/blocker.png" /><a class="reportLnk ctaLink" id="engineer_sr_turnaround1_rpt" href="/report/common/reports/engineer_sr_turnaround.php"> <span>Engineer Service Report Turnaround <?php echo date('Y',time()); ?> (Excel)</span> </a> </li>
			<li> <img class="unstyledListBullet" src="../../resources/images/common/blocker.png" /><a class="reportLnk ctaLink" id="engineer_sr_turnaround2_rpt" href="/report/common/reports/engineer_sr_turnaround.php?year=<?php echo date('Y',strtotime("-1 year", time())); ?>"> <span>Engineer Service Report Turnaround <?php echo date('Y',strtotime("-1 year", time())); ?> (Excel)</span> </a> </li>
			<li> <img class="unstyledListBullet" src="../../resources/images/common/blocker.png" /><a class="reportLnk ctaLink" id="engineer_utilize2_rpt" href="/report/common/reports/engineer_utilize.php?year=<?php echo date('Y', time()); ?>"> <span>Engineer Utilization <?php echo date('Y',time()); ?> (Excel) <em> Be patient slow to run</em></span> </a> </li>
			<li> <img class="unstyledListBullet" src="../../resources/images/common/blocker.png" /><a class="reportLnk ctaLink" id="engineer_utilize2_rpt" href="/report/common/reports/engineer_utilize.php?year=<?php echo date('Y',strtotime("-1 year", time())); ?>"> <span>Engineer Utilization <?php echo date('Y',strtotime("-1 year", time())); ?> (Excel) <em> Be patient slow to run</em></span> </a> </li>		
			<li> <img class="unstyledListBullet" src="../../resources/images/common/blocker.png" /><a class="reportLnk ctaLink" id="contracts_billing_rpt" href="/report/common/reports/contracts_billing.php"> <span>Scott's Special Report (Excel)</span> </a> </li>
			<li> <img class="unstyledListBullet" src="../../resources/images/common/blocker.png" /><a class="reportLnk ctaLink" id="empty_service_rpt_ct" href="/resources/print_sr/f70-14-15_rev-c.php?empty&type=ct"> <span>Blank CT Service Report (PDF)</span> </a> </li>
			<li> <img class="unstyledListBullet" src="../../resources/images/common/blocker.png" /><a class="reportLnk ctaLink" id="empty_service_rpt_ct" href="/resources/print_sr/f70-14-15_rev-c.php?empty&type=ct&mfg=siemens"> <span>Blank Siemens CT Service Report (PDF)</span> </a> </li>
			<li> <img class="unstyledListBullet" src="../../resources/images/common/blocker.png" /><a class="reportLnk ctaLink" id="empty_service_rpt_ct" href="/resources/print_sr/f70-14-15_rev-c.php?empty&type=mr"> <span>Blank MR Service Report (PDF)</span> </a> </li>
			<li> <img class="unstyledListBullet" src="../../resources/images/common/blocker.png" /><a class="reportLnk ctaLink" id="systems_down" onClick="emailreport('/report/scripts/systems_down.php?single')"> <span>Systems Down Report (eMail)</span> </a> </li>
			<li> <img class="unstyledListBullet" src="../../resources/images/common/blocker.png" /><a class="reportLnk ctaLink" id="futures_check" onClick="emailreport('/report/scripts/futures_check.php?single')"> <span>Future Systems Moving Report (eMail)</span> </a> </li>
			<?php if(isset($_SESSION['perms']['perm_rcv_pm_overdue'])){ ?>
				<li> <img class="unstyledListBullet" src="../../resources/images/common/blocker.png" /><a class="reportLnk ctaLink" id="futures_check" onClick="emailreport('/report/scripts/overdue_pms.php?single')"> <span>Overdue/Late PMs Report (eMail)</span> </a> </li>
			<?php } ?>
			
		</ul>
	</div>
	
	<br>
	<div class="reports" style="width:50%; margin-left:auto; margin-right:auto;">
		<div>Assignments</div>
		<ul class="unstyledList">
			<li> <img class="unstyledListBullet" src="../../resources/images/common/blocker.png" /><a class="reportLnk ctaLink" id="eng_assignments_rpt" href="/report/common/reports/eng_assignments.php"> <span>Engineer Assignments (Excel)</span> </a> </li>
			<li> <img class="unstyledListBullet" src="../../resources/images/common/blocker.png" /><a class="reportLnk ctaLink" id="engineer_sites_rpt" href="/report/common/reports/engineer_systems.php"> <span>Engineer's System IDs (Excel)</span> </a> </li>
			<li> <img class="unstyledListBullet" src="../../resources/images/common/blocker.png" /><a class="reportLnk ctaLink" id="unassigned_sites_rpt" href="/report/common/reports/unassigned_systems.php"> <span>Un-Assigned Systems (Excel)</span> </a> </li>
			<li> <img class="unstyledListBullet" src="../../resources/images/common/blocker.png" /><a class="reportLnk ctaLink" id="customer_sites_rpt" href="/report/common/reports/customer_systems.php"> <span>Customer's System IDs (Excel)</span> </a> </li>
		</ul>
	</div>
	
	<br>
	<div class="reports" style="width:50%; margin-left:auto; margin-right:auto;">
		<div>Reports &amp; Requests</div>
		<ul class="unstyledList">
			<li> <img class="unstyledListBullet" src="../../resources/images/common/blocker.png" /><a class="reportLnk ctaLink" id="service_requests_rpt" href="/report/common/reports/service_requests.php"> <span>Service Requests (Excel)</span> </a> </li>
			<li> <img class="unstyledListBullet" src="../../resources/images/common/blocker.png" /><a class="reportLnk ctaLink" id="corrupt_sites_rpt" href="/report/common/reports/corrupt_systems.php"> <span>Incomplete System Data (Excel) <em> Be patient slow to run</em></span> </a> </li>
			<li> <img class="unstyledListBullet" src="../../resources/images/common/blocker.png" /><a class="reportLnk ctaLink" id="edited_reports" href="/report/common/reports/edited_reports.php"> <span>Edited Service Reports (Excel) </span> </a> </li>
			<li> <img class="unstyledListBullet" src="../../resources/images/common/blocker.png" /><a class="iframeAllSites reportLnk ctaLink" id="site_service_history" href="/report/common/all_systems.php?q=7&all"> <span>System Service History (Excel)</span> </a> </li>
			<li> <img class="unstyledListBullet" src="../../resources/images/common/blocker.png" /><a class="reportLnk ctaLink" id="open_requests" href="/report/common/reports/open_requests.php"> <span>Open Service Requests (Excel)</span> </a> </li>
		</ul>
	</div>
	
	<?php if(isset($_SESSION['perms']['perm_rcv_nps_report'])){ ?>
	<br>
	<div class="reports" style="width:50%; margin-left:auto; margin-right:auto;">
		<div>NPS</div>
		<ul class="unstyledList">
			<li> <img class="unstyledListBullet" src="../../resources/images/common/blocker.png" /><a class="reportLnk ctaLink" id="nps_rpt" href="/report/common/reports/nps.php"> <span>NPS Report (Excel)</span> </a> </li>
		</ul>
	</div>
	<?php } ?>
	
	<?php if(isset($_SESSION['perms']['perm_face_print'])){ ?>
	<br>
	<div class="reports" style="width:50%; margin-left:auto; margin-right:auto;">
		<div>Face Sheets</div>
		<ul class="unstyledList">
			<li> <img class="unstyledListBullet" src="../../resources/images/common/blocker.png" /><a class="iframeAllSites reportLnk ctaLink" id="face_single_rpt" href="/report/common/all_systems.php?q=6"> <span>Print Face Sheet (PDF)</span> </a> </li>
			<li> <img class="unstyledListBullet" src="../../resources/images/common/blocker.png" /><a class="reportLnk ctaLink" id="face_blank_rpt" href="/report/common/reports/face_sheets.php?blank"> <span>Print Blank Face Sheet (DOCX)</span> </a> </li>
		</ul>
	</div>
	<?php } ?>
	
	<?php if(isset($_SESSION['perms']['perm_edit_systems']) or isset($_SESSION['perms']['perm_edit_facilities']) or isset($_SESSION['perms']['perm_edit_customers'])){ ?>
	<br>
	<div class="reports" style="width:50%; margin-left:auto; margin-right:auto;">
		<div>Contact</div>
		<ul class="unstyledList">
			<li> <img class="unstyledListBullet" src="../../resources/images/common/blocker.png" /><a class="reportLnk ctaLink" id="systems_contact_info" href="/report/common/reports/systems_addresses.php"> <span>Systems Contact Info (Excel)</span> </a> </li>
		</ul>
	</div>
	<?php } ?>
	
	<?php if(isset($_SESSION['perms']['perm_tool_cal_edit']) or isset($_SESSION['perms']['perm_tool_cal_view'])){ ?>
	<br>
	<div class="reports" style="width:50%; margin-left:auto; margin-right:auto;">
		<div>Tools</div>
		<ul class="unstyledList">
			<li> <img class="unstyledListBullet" src="../../resources/images/common/blocker.png" /><a class="reportLnk ctaLink" id="tools_calibrations" href="/report/common/reports/tools_calibrations.php"> <span>Tool Calibration (Excel)</span> </a> </li>
		</ul>
	</div>
	<?php } ?>
	
	<?php if(isset($_SESSION['roles']['role_quality'])){ ?>
	<br>
	<div class="reports" style="width:50%; margin-left:auto; margin-right:auto;">
		<div>ISO / Quality</div>
		<ul class="unstyledList">
			<li> <img class="unstyledListBullet" src="../../resources/images/common/blocker.png" /><a class="reportLnk ctaLink" id="open_parcar" href="/report/common/reports/open_parcar.php"> <span>Open PARs CARs (Excel)</span> </a> </li>
			<?php if(isset($_SESSION['perms']['perm_rcv_cont_met'])){ ?>
				<li> <img class="unstyledListBullet" src="../../resources/images/common/blocker.png" /><a class="iframeAllSites reportLnk ctaLink" id="pm_contract_fulfill" href="/report/common/report_years.php?report=pm_contract_fulfill"> <span>PM Contract Fulfillment (eMail)</span> </a> </li>
			<?php } ?>
		</ul>
	</div>
	<?php } ?>

	<?php if(isset($_SESSION['perms']['perm_edit_financial'])){ ?>
		<br>
		<div class="reports" style="width:50%; margin-left:auto; margin-right:auto;">
			<div>Finance</div>
			<ul class="unstyledList">
				<li> <img class="unstyledListBullet" src="../../resources/images/common/blocker.png" /><a class="reportLnk ctaLink" id="open_parcar" href="/report/common/reports/active_systems_finance.php"> <span>Active Systems Contract & Warranties (Excel)</span> </a> </li>
				<li> <img class="unstyledListBullet" src="../../resources/images/common/blocker.png" /><a class="iframeAllSites reportLnk ctaLink" id="pm_contract_fulfill" href="/report/common/report_years.php?report=tm_reports_fy"> <span>T&M Service (Excel)</span> </a> </li>
				<li> <img class="unstyledListBullet" src="../../resources/images/common/blocker.png" /><a class="iframeAllSites reportLnk ctaLink" id="pm_contract_fulfill" href="/report/common/report_years.php?report=systems_reports_fy"> <span>Systems Service (Excel) <em> Be patient slow to run</em></span> </a> </li>
			</ul>
		</div>
	<?php } ?>

	<br>
</div>

<?php require_once($footer_include); ?>