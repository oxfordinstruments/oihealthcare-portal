<?php
//
// Error reporting
//
//error_reporting(E_ALL);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
ini_set('display_errors', 'On');

$debug = false;
if(isset($_GET['debug'])){
	$debug = true;
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/php_utils.php');
$php_utils = new phpUtils();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/moment_php/Moment.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/resources/moment_php/MomentLocale.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/resources/moment_php/MomentException.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/resources/moment_php/MomentHelper.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/resources/moment_php/MomentFromVo.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/resources/moment_php/MomentPeriodVo.php');

//Get Systems
$ct_systems = array();
$sql="SELECT sbc.system_id, sbc.nickname, mc.`type` AS contract_type, sbc.contract_start_date, sbc.contract_end_date, sbc.warranty_start_date, sbc.warranty_end_date,
st.name AS system_type, sbc.contract_month_amt, sbc.credit_hold, sbc.credit_hold_date, up.name AS eng_pri, us.name AS eng_sec
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
LEFT JOIN misc_contracts AS mc ON mc.id = sbc.contract_type
LEFT JOIN systems_types AS st ON st.id = sb.system_type
LEFT JOIN misc_modalities AS mm ON mm.id = st.modality
LEFT JOIN systems_assigned_pri AS sap ON sap.system_ver_unique_id = sbc.ver_unique_id
LEFT JOIN users AS up ON up.uid = sap.uid
LEFT JOIN systems_assigned_sec AS sas ON sas.system_ver_unique_id = sbc.ver_unique_id
LEFT JOIN users AS us ON us.uid = sas.uid
WHERE sbc.property = 'c' 
AND mc.finance_calc = 'y' 
AND mm.modality = 'ct'
ORDER BY sbc.system_id;";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

while($row = $result->fetch_assoc()){
	array_push($ct_systems,$row);
}
d($ct_systems);

$mr_systems = array();
$sql="SELECT sbc.system_id, sbc.nickname, mc.`type` AS contract_type, sbc.contract_start_date, sbc.contract_end_date, sbc.warranty_start_date, sbc.warranty_end_date,
st.name AS system_type, sbc.contract_month_amt, sbc.credit_hold, sbc.credit_hold_date, up.name AS eng_pri, us.name AS eng_sec
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
LEFT JOIN misc_contracts AS mc ON mc.id = sbc.contract_type
LEFT JOIN systems_types AS st ON st.id = sb.system_type
LEFT JOIN misc_modalities AS mm ON mm.id = st.modality
LEFT JOIN systems_assigned_pri AS sap ON sap.system_ver_unique_id = sbc.ver_unique_id
LEFT JOIN users AS up ON up.uid = sap.uid
LEFT JOIN systems_assigned_sec AS sas ON sas.system_ver_unique_id = sbc.ver_unique_id
LEFT JOIN users AS us ON us.uid = sas.uid
WHERE sbc.property = 'c' 
AND mc.finance_calc = 'y' 
AND mm.modality = 'mr'
ORDER BY sbc.system_id;";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

while($row = $result->fetch_assoc()){
	array_push($mr_systems,$row);
}
d($mr_systems);

require_once ($_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/Classes/PHPExcel.php');
require_once ($_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/Classes/PHPExcel/IOFactory.php');

$begin_row = 8;

$template_file = $_SERVER['DOCUMENT_ROOT'].'/resources/report_templates/active_systems_finance.xls';
if (!file_exists($template_file)) {
	$log->logerr('active_systems_finance.php',1045);
	exit("Error the template is missing." . EOL);
}
$objPHPExcel = new PHPExcel();
$objPHPExcel = PHPExcel_IOFactory::load($template_file);

$style_FillBG_Green = new PHPExcel_Style();
$style_FillBG_Green->applyFromArray(
	array('fill' => array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color' => array('argb' => 'FFBBD490')))
);

$style_FillBG_Red = new PHPExcel_Style();
$style_FillBG_Red->applyFromArray(
	array('fill' => array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color' => array('argb' => 'FFFFC7CE')),
		'font'  => array('color' => array('rgb' => '800000')))
);

$style_FillBG_Yellow = new PHPExcel_Style();
$style_FillBG_Yellow->applyFromArray(
	array('fill' => array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color' => array('argb' => 'FFE8F2B0')),
		'font'  => array('color' => array('rgb' => '800000')))
);

$objPHPExcel->setActiveSheetIndex(0);
$report_date = date(phpdispfdt,time()+$_SESSION['tz_offset_sec']);
$objPHPExcel->getActiveSheet()->setCellValue('B3', $report_date);

$now = new Moment\Moment('','UTC', true);
d($now);

$year_end = new Moment\Moment($now->getYear().'-'.$settings->fiscal->year_end_month.'-'.$settings->fiscal->year_end_day.' 00:00:00', 'UTC', true);
d($year_end);

if($now > $year_end){
	$year_end = intval($now->getYear()) + 1;
	$year_end = new Moment\Moment($year_end.'-'.$settings->fiscal->year_end_month.'-'.$settings->fiscal->year_end_day.' 00:00:00', 'UTC', true);
	$fiscal_year = $now->getYear().'/'.$year_end->getYear();
}else{
	$year_end = new Moment\Moment(intval($now->getYear()).'-'.$settings->fiscal->year_end_month.'-'.$settings->fiscal->year_end_day.' 00:00:00', 'UTC', true);
	$fiscal_year = intval($now->getYear())-1;
	$fiscal_year = $fiscal_year.'/'.$now->getYear();
}
d($year_end);
d($fiscal_year);
$objPHPExcel->getActiveSheet()->setCellValue('B4', $fiscal_year);

$fiscal_months_left = $php_utils->getMonths($now->format(DATE_ATOM), $year_end->format(DATE_ATOM))['total'];
d($fiscal_months_left);

$_num = $begin_row;
$tot_cont_mon_amt = 0;
$tot_cur_mon_amt = 0;
$tot_amt_left_yr = 0;
$tot_backlog = 0;
$tot_exp_backlog = 0;

$objPHPExcel->getActiveSheet()->setCellValue('F4', 'Credit Hold');
$objPHPExcel->getActiveSheet()->setSharedStyle($style_FillBG_Red, "F4");
$objPHPExcel->getActiveSheet()->setCellValue('G4', 'Warranty');
$objPHPExcel->getActiveSheet()->setSharedStyle($style_FillBG_Green, "G4");
$objPHPExcel->getActiveSheet()->setCellValue('H4', 'Contract Amt 0');
$objPHPExcel->getActiveSheet()->setSharedStyle($style_FillBG_Yellow, "H4");

foreach($ct_systems as $key => $system){
	if($system['warranty_start_date'] == trim('1970-01-01T00:00:00Z')){
		$system['warranty_start_date'] = '';
	}
	if($system['warranty_end_date'] == trim('1970-01-01T00:00:00Z')){
		$system['warranty_end_date'] = '';
	}
	if($system['contract_start_date'] == trim('1970-01-01T00:00:00Z')){
		$system['contract_start_date'] = '';
	}
	if($system['contract_end_date'] == trim('1970-01-01T00:00:00Z')){
		$system['contract_end_date'] = '';
	}
	$system_data = calc_data($system);

	$objPHPExcel->getActiveSheet()->setCellValue('A'.$_num, $system['system_id']);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$_num, $system['nickname']);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$_num, $system['contract_type']);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$_num, $system['contract_start_date']);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$_num, $system['contract_end_date']);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$_num, $system_data['contract_days_till_expire']);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$_num, $system['warranty_start_date']);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$_num, $system['warranty_end_date']);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$_num, $system['system_type']);
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$_num, $system_data['contract_month_amt']);
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$_num, round($system_data['current_month_amt'],2));
	$objPHPExcel->getActiveSheet()->setCellValue('L'.$_num, round($system_data['fiscal_amount'],2));
	$objPHPExcel->getActiveSheet()->setCellValue('M'.$_num, round($system_data['total_back_log'],2));
	$objPHPExcel->getActiveSheet()->setCellValue('N'.$_num, round($system_data['exp_back_log'],2));
	$objPHPExcel->getActiveSheet()->setCellValue('O'.$_num, round($fiscal_months_left,2));
	$objPHPExcel->getActiveSheet()->setCellValue('P'.$_num, round($system_data['contract_months_left'],2));
	$objPHPExcel->getActiveSheet()->setCellValue('R'.$_num, $system['eng_pri']);
	$objPHPExcel->getActiveSheet()->setCellValue('S'.$_num, $system['eng_sec']);



	if($system_data['credit_hold']){
		$objPHPExcel->getActiveSheet()->setSharedStyle($style_FillBG_Red, "A".$_num.":P".$_num);
	}
	if($system_data['under_warranty']){
		$objPHPExcel->getActiveSheet()->setSharedStyle($style_FillBG_Green, "A".$_num.":P".$_num);
	}
	if($system_data['contract_month_amt'] == 0){
		$objPHPExcel->getActiveSheet()->setSharedStyle($style_FillBG_Yellow, "A".$_num.":P".$_num);
	}

	
	$_num++;
}

if($debug){echo "------------------------ END ------------------------",EOL;}
d($tot_cont_mon_amt);
d($tot_cur_mon_amt);
d($tot_amt_left_yr);
d($tot_backlog);
dd($tot_exp_backlog);

//Fix cell alignment
$lastrow = $objPHPExcel->getActiveSheet()->getHighestRow();
$objPHPExcel->getActiveSheet()
	->getStyle('F'.$begin_row.':F'.$lastrow)
	->getAlignment()
	->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()
	->getStyle('J'.$begin_row.':N'.$lastrow)
	->getNumberFormat()
	->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD);



//Fill in totals
$objPHPExcel->getActiveSheet()->setCellValue('J4', round($tot_cont_mon_amt,2));
$objPHPExcel->getActiveSheet()->setCellValue('K4', round($tot_cur_mon_amt,2));
$objPHPExcel->getActiveSheet()->setCellValue('L4', round($tot_amt_left_yr,2));
$objPHPExcel->getActiveSheet()->setCellValue('M4', round($tot_backlog,2));
$objPHPExcel->getActiveSheet()->setCellValue('N4', round($tot_exp_backlog,2));

//Move cursor to first cell
$objPHPExcel->getActiveSheet()->setSelectedCell('A'.$begin_row);

//MR SHEET ----------------------------------------------------------------------------------------------

$_num = $begin_row;
$tot_cont_mon_amt = 0;
$tot_cur_mon_amt = 0;
$tot_amt_left_yr = 0;
$tot_backlog = 0;
$tot_exp_backlog = 0;

$objPHPExcel->setActiveSheetIndex(1);
$objPHPExcel->getActiveSheet()->setCellValue('B3', $report_date);
$objPHPExcel->getActiveSheet()->setCellValue('B4', $fiscal_year);

$objPHPExcel->getActiveSheet()->setCellValue('F4', 'Credit Hold');
$objPHPExcel->getActiveSheet()->setSharedStyle($style_FillBG_Red, "F4");
$objPHPExcel->getActiveSheet()->setCellValue('G4', 'Warranty');
$objPHPExcel->getActiveSheet()->setSharedStyle($style_FillBG_Green, "G4");
$objPHPExcel->getActiveSheet()->setCellValue('H4', 'Contract Amt 0');
$objPHPExcel->getActiveSheet()->setSharedStyle($style_FillBG_Yellow, "H4");

foreach($mr_systems as $key => $system){
	if($system['warranty_start_date'] == trim('1970-01-01T00:00:00Z')){
		$system['warranty_start_date'] = '';
	}
	if($system['warranty_end_date'] == trim('1970-01-01T00:00:00Z')){
		$system['warranty_end_date'] = '';
	}
	if($system['contract_start_date'] == trim('1970-01-01T00:00:00Z')){
		$system['contract_start_date'] = '';
	}
	if($system['contract_end_date'] == trim('1970-01-01T00:00:00Z')){
		$system['contract_end_date'] = '';
	}
	$system_data = calc_data($system);

	$objPHPExcel->getActiveSheet()->setCellValue('A'.$_num, $system['system_id']);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$_num, $system['nickname']);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$_num, $system['contract_type']);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$_num, $system['contract_start_date']);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$_num, $system['contract_end_date']);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$_num, $system_data['contract_days_till_expire']);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$_num, $system['warranty_start_date']);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$_num, $system['warranty_end_date']);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$_num, $system['system_type']);
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$_num, $system_data['contract_month_amt']);
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$_num, round($system_data['current_month_amt'],2));
	$objPHPExcel->getActiveSheet()->setCellValue('L'.$_num, round($system_data['fiscal_amount'],2));
	$objPHPExcel->getActiveSheet()->setCellValue('M'.$_num, round($system_data['total_back_log'],2));
	$objPHPExcel->getActiveSheet()->setCellValue('N'.$_num, round($system_data['exp_back_log'],2));
	$objPHPExcel->getActiveSheet()->setCellValue('O'.$_num, round($fiscal_months_left,2));
	$objPHPExcel->getActiveSheet()->setCellValue('P'.$_num, round($system_data['contract_months_left'],2));
	$objPHPExcel->getActiveSheet()->setCellValue('R'.$_num, $system['eng_pri']);
	$objPHPExcel->getActiveSheet()->setCellValue('S'.$_num, $system['eng_sec']);



	if($system_data['credit_hold']){
		$objPHPExcel->getActiveSheet()->setSharedStyle($style_FillBG_Red, "A".$_num.":P".$_num);
	}
	if($system_data['under_warranty']){
		$objPHPExcel->getActiveSheet()->setSharedStyle($style_FillBG_Green, "A".$_num.":P".$_num);
	}
	if($system_data['contract_month_amt'] == 0){
		$objPHPExcel->getActiveSheet()->setSharedStyle($style_FillBG_Yellow, "A".$_num.":P".$_num);
	}


	$_num++;
}

if($debug){echo "------------------------ END ------------------------",EOL;}
d($tot_cont_mon_amt);
d($tot_cur_mon_amt);
d($tot_amt_left_yr);
d($tot_backlog);
dd($tot_exp_backlog);

//Fix cell alignment
$lastrow = $objPHPExcel->getActiveSheet()->getHighestRow();
$objPHPExcel->getActiveSheet()
	->getStyle('F'.$begin_row.':F'.$lastrow)
	->getAlignment()
	->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()
	->getStyle('J'.$begin_row.':N'.$lastrow)
	->getNumberFormat()
	->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD);



//Fill in totals
$objPHPExcel->getActiveSheet()->setCellValue('J4', round($tot_cont_mon_amt,2));
$objPHPExcel->getActiveSheet()->setCellValue('K4', round($tot_cur_mon_amt,2));
$objPHPExcel->getActiveSheet()->setCellValue('L4', round($tot_amt_left_yr,2));
$objPHPExcel->getActiveSheet()->setCellValue('M4', round($tot_backlog,2));
$objPHPExcel->getActiveSheet()->setCellValue('N4', round($tot_exp_backlog,2));

//Move cursor to first cell
$objPHPExcel->getActiveSheet()->setSelectedCell('A'.$begin_row);


$objPHPExcel->setActiveSheetIndex(0);


ini_set('zlib.output_compression','Off');
header('Pragma: public');
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");                  // Date in the past
//header('Last-Modified: '.gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');     // HTTP/1.1
header('Cache-Control: pre-check=0, post-check=0, max-age=0');    // HTTP/1.1
header ("Pragma: no-cache");
header("Expires: 0");
header('Content-Transfer-Encoding: none');
header('Content-Type: application/vnd.ms-excel;');                 // This should work for IE & Opera
header("Content-type: application/x-msexcel");                    // This should work for the rest
header('Content-Disposition: attachment; filename="Active System Contract and Warranty Dates '.date(savefdt,time()+$_SESSION['tz_offset_sec']).'.xls"');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');

ob_end_flush();


/**
 * @param $needle
 * @param $haystack
 * @return int|null|string
 */
function search($needle, $haystack){
	foreach($haystack as $id => $val)
	{
		if($val['system_id'] == $needle) {
			return $id;			
		}
	}
	return null;
}

/**
 * @param array $system
 * @return array
 */
function calc_data(array $system){
	global $tot_cont_mon_amt, $tot_cur_mon_amt,	$tot_amt_left_yr, $tot_backlog, $tot_exp_backlog, $debug, $php_utils, $now, $year_end, $fiscal_months_left;

	if($debug){echo "-------- ",$system['system_id']," --------",EOL;}

	$system_data = array();

	$system_data['csd'] = false;
	if($system['contract_start_date'] != ''){
		$system_data['csd'] = new Moment\Moment($system['contract_start_date'],'UTC', true);
	}

	$system_data['ced'] = false;
	$system_data['contract_days_till_expire'] = false;
	$system_data['contract_months_left'] = false;
	if($system['contract_end_date'] != ''){
		$system_data['ced'] = new Moment\Moment($system['contract_end_date'],'UTC', true);
		$system_data['contract_days_till_expire'] = round($now->from($system_data['ced'])->getDays() + 1);
		$system_data['contract_months_left'] = $php_utils->getMonths($now->format(DATE_ATOM), $system_data['ced']->format(DATE_ATOM))['total'];
	}

	$system_data['wsd'] = false;
	if($system['warranty_start_date'] != ''){
		$system_data['wsd'] = new Moment\Moment($system['warranty_start_date'],'UTC', true);
	}

	$system_data['wed'] = false;
	if($system['warranty_end_date'] != ''){
		$system_data['wed'] = new Moment\Moment($system['warranty_end_date'],'UTC', true);
	}

	$system_data['chd'] = false;
	if($system['credit_hold_date'] != ''){
		$system_data['chd'] = new Moment\Moment($system['credit_hold_date'],'UTC', true);
	}

	$system_data['credit_hold'] = false;
	if(strtolower($system['credit_hold']) == 'y'){
		$system_data['credit_hold'] = true;
	}

	$system_data['contract_month_amt'] = false;
	$system_data['current_month_amt'] = false;
	if($system['contract_month_amt'] != '' or !is_null($system['contract_month_amt'])){
		$system_data['contract_month_amt'] = floatval($system['contract_month_amt']);
		$system_data['current_month_amt'] = $system_data['contract_month_amt'];
	}


	if($system_data['wed'] != false){
		if($system_data['wed'] > $now){
			$system_data['under_warranty'] = true;
			if($system_data['contract_days_till_expire'] >= 0){
				$system_data['exp_back_log'] = $system_data['contract_month_amt'] * $system_data['contract_months_left'];
			}
			$system_data['current_month_amt'] = 0;
		}else{
			$system_data['under_warranty'] = false;
			if($system_data['contract_days_till_expire'] >= 0){
				$system_data['exp_back_log'] = $system_data['contract_month_amt'] * $system_data['contract_months_left'];
			}
		}
	}else{
		$system_data['under_warranty'] = false;
		if($system_data['contract_days_till_expire'] >= 0){
			$system_data['exp_back_log'] = $system_data['contract_month_amt'] * $system_data['contract_months_left'];
		}
	}

	if($system_data['credit_hold']){
		$system_data['current_month_amt'] = 0;
	}

	$system_data['total_back_log'] = $system_data['current_month_amt'] * $system_data['contract_months_left'];

	if($system_data['ced']->isBetween($now,$year_end)){
		$system_data['fiscal_amount'] = $system_data['current_month_amt'] * $system_data['contract_months_left'];
	}else{
		$system_data['fiscal_amount'] = $system_data['current_month_amt'] * $fiscal_months_left;
	}

	$tot_cont_mon_amt += $system_data['contract_month_amt'];
	$tot_cur_mon_amt += $system_data['current_month_amt'];
	$tot_amt_left_yr += $system_data['fiscal_amount'];
	$tot_backlog += $system_data['total_back_log'];
	$tot_exp_backlog += $system_data['exp_back_log'];


	d($system_data);
	return $system_data;
}

exit("DONE");
?>