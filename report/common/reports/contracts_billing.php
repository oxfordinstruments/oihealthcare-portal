<?php
//Update Completed 6/17/2015
$debug = false;

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

//Get Systems
$ct_systems = array();
$sql="SELECT mm.modality, sb.system_id, sbc.nickname AS system_nickname, sbc.contract_start_date, sbc.contract_end_date, e.name AS equipment_name, mc.`type` AS contract_type, 
f.bill_facility, f.bill_name as f_name, f.bill_address as f_address, f.bill_city as f_city, f.bill_state as f_state, f.bill_zip as f_zip,
c.bill_customer, c.bill_name as c_name, c.bill_address as c_address, c.bill_city as c_city, c.bill_state as c_state, c.bill_zip as c_zip
FROM systems_base AS sb
LEFT JOIN systems_base_cont AS sbc ON sb.unique_id = sbc.unique_id
LEFT JOIN systems_types AS e ON e.id = sb.system_type
LEFT JOIN misc_modalities as mm ON mm.id = e.modality
LEFT JOIN misc_contracts AS mc ON mc.id = sbc.contract_type
LEFT JOIN facilities AS f ON f.unique_id = sbc.facility_unique_id
LEFT JOIN customers AS c ON c.unique_id = f.customer_unique_id
WHERE sbc.property = 'C' AND e.modality = 1
ORDER BY sb.system_id ASC;";
s($sql);
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

while($row = $result->fetch_assoc()){
	array_push($ct_systems,$row);
}
d($ct_systems);

$mr_systems = array();
$sql="SELECT mm.modality, sb.system_id, sbc.nickname AS system_nickname, sbc.contract_start_date, sbc.contract_end_date, e.name AS equipment_name, mc.`type` AS contract_type, 
f.bill_facility, f.bill_name as f_name, f.bill_address as f_address, f.bill_city as f_city, f.bill_state as f_state, f.bill_zip as f_zip,
c.bill_customer, c.bill_name as c_name, c.bill_address as c_address, c.bill_city as c_city, c.bill_state as c_state, c.bill_zip as c_zip
FROM systems_base AS sb
LEFT JOIN systems_base_cont AS sbc ON sb.unique_id = sbc.unique_id
LEFT JOIN systems_types AS e ON e.id = sb.system_type
LEFT JOIN misc_modalities as mm ON mm.id = e.modality
LEFT JOIN misc_contracts AS mc ON mc.id = sbc.contract_type
LEFT JOIN facilities AS f ON f.unique_id = sbc.facility_unique_id
LEFT JOIN customers AS c ON c.unique_id = f.customer_unique_id
WHERE sbc.property = 'C' AND e.modality = 2
ORDER BY sb.system_id ASC;";
s($sql);
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

while($row = $result->fetch_assoc()){
	array_push($mr_systems,$row);
}
dd($mr_systems);

// Include PHPExcel
require_once ($_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/Classes/PHPExcel.php');

// Include PHPExcel_IOFactory
require_once ($_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/Classes/PHPExcel/IOFactory.php');

// Check if template exists
//echo "Check if Excel2007 template file exists<br />";
$template_file = $_SERVER['DOCUMENT_ROOT'].'/resources/report_templates/contracts_billing.xls';
if (!file_exists($template_file)) {
	$log->logerr('corrupt_systems.php',1045);	
	exit("Error the template is missing." . EOL);
}

// Create new PHPExcel object
//echo "Start PHPExcell", EOL;
$objPHPExcel = new PHPExcel();

//echo "Load from Excel2007 template file", EOL;
$objPHPExcel = PHPExcel_IOFactory::load($template_file);

$sharedStyle1 = new PHPExcel_Style();

$sharedStyle1->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('argb' => 'FFFFCCCC')
							),
		 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
		 
));

$sharedStyle1a = new PHPExcel_Style();

$sharedStyle1a->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('argb' => 'FFFFCCCC')
							),
		 'alignment' => array(
		 						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		 					  	'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
							)
		 
));

$sharedStyle2 = new PHPExcel_Style();

$sharedStyle2->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('argb' => 'FFD7E4BC')
							),
		 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
		 
));

$sharedStyle2a = new PHPExcel_Style();

$sharedStyle2a->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('argb' => 'FFD7E4BC')
							),
		 'alignment' => array(
		 						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		 					  	'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
							)
		 
));

$sharedStyle3 = new PHPExcel_Style();

$sharedStyle3->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('argb' => 'FFB8CCE4')
							),
		 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
		 
));

$sharedStyle3a = new PHPExcel_Style();

$sharedStyle3a->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('argb' => 'FFB8CCE4')
							),
		 'alignment' => array(
		 						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		 					  	'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
							)
		 
));

//Select ct sheet
$objPHPExcel->setActiveSheetIndex(0);

$report_date = date(phpdispfdt,time()+$_SESSION['tz_offset_sec']);

//Set report date
$objPHPExcel->getActiveSheet()->setCellValue('b3', $report_date);

//add rows
$_num = 5;
$last_key = end(array_keys($ct_systems));
foreach($ct_systems as $key => $system){
	$csd = '';
	$ced = '';

	if($system['contract_start_date'] != ''){
		$csd = date(phpdispfd,strtotime($system['contract_start_date']));
	}
	if($system['contract_end_date'] != ''){
		$ced = date(phpdispfd,strtotime($system['contract_end_date']));
	}
	
	if(strtolower($system['bill_customer']) == 'y'){
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$_num, $system['c_state']);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$_num, $system['c_address']);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$_num, $system['c_city']);	
	}else{
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$_num, $system['f_state']);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$_num, $system['f_address']);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$_num, $system['f_city']);
	}
	
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$_num, $system['system_id'] . ' - ' . $system['system_nickname']);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$_num, $system['equipment_name']);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$_num, $system['modality']);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$_num, $csd);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$_num, $ced);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$_num, $system['contract_type']);
		
	$_num++;
}

//Select mr sheet
$objPHPExcel->setActiveSheetIndex(1);

//Set report date
$objPHPExcel->getActiveSheet()->setCellValue('b3', $report_date);

//add rows
$_num = 5;
$last_key = end(array_keys($mr_systems));
foreach($mr_systems as $key => $system){
	$csd = '';
	$ced = '';

	if($system['contract_start_date'] != ''){
		$csd = date(phpdispfd,strtotime($system['contract_start_date']));
	}
	if($system['contract_end_date'] != ''){
		$ced = date(phpdispfd,strtotime($system['contract_end_date']));
	}
	
	if(strtolower($system['bill_customer']) == 'y'){
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$_num, $system['c_state']);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$_num, $system['c_address']);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$_num, $system['c_city']);	
	}else{
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$_num, $system['f_state']);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$_num, $system['f_address']);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$_num, $system['f_city']);
	}
	
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$_num, $system['system_id'] . ' - ' . $system['system_nickname']);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$_num, $system['equipment_name']);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$_num, $system['modality']);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$_num, $csd);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$_num, $ced);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$_num, $system['contract_type']);
		
	$_num++;}

$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
ini_set('zlib.output_compression','Off');
header('Pragma: public');
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");                  // Date in the past
//header('Last-Modified: '.gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');     // HTTP/1.1
header('Cache-Control: pre-check=0, post-check=0, max-age=0');    // HTTP/1.1
header ("Pragma: no-cache");
header("Expires: 0");
header('Content-Transfer-Encoding: none');
header('Content-Type: application/vnd.ms-excel;');                 // This should work for IE & Opera
header("Content-type: application/x-msexcel");                    // This should work for the rest
header('Content-Disposition: attachment; filename="Contracts Billing '.date(savefdt,time()+$_SESSION['tz_offset_sec']).'.xls"');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');

ob_end_flush();

function search($needle, $haystack){
	foreach($haystack as $id => $val)
	{
		if($val['system_id'] == $needle) {
			return $id;			
		}
	}
}
exit("DONE");
?>