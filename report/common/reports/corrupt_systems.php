<?php
//Update Completed 12/09/2014
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

//Get Systems
$sites = array();
$cols = array();
$required = array('system_id','site_name','site_nickname','address','city','state','zip','phone','contact_name','contact_phone','equipment_name','system_serial','contract_type','contract_terms','contract_start_date','contract_end_date','contract_hours','pm_freq','email_list',);


$sql=<<<SQL
DROP TEMPORARY TABLE IF EXISTS systems_temp;

CREATE TEMPORARY TABLE systems_temp 
SELECT sb.*, sbc.*, e.name AS system, c.`type` AS contract_type_name, st.`status` as system_status,
(SELECT uu.name FROM systems_assigned_pri AS aa left JOIN users AS uu ON uu.uid = aa.uid WHERE aa.system_ver_unique_id=sbc.ver_unique_id ) AS engineer_pri,
(SELECT uu.name FROM systems_assigned_sec AS aa LEFT JOIN users AS uu ON uu.uid = aa.uid WHERE aa.system_ver_unique_id=sbc.ver_unique_id ) AS engineer_sec
FROM systems_base AS sb
LEFT JOIN systems_base_cont AS sbc ON sb.unique_id = sbc.unique_id
LEFT JOIN systems_types AS e ON e.id = sb.system_type
LEFT JOIN misc_contracts AS c ON c.id = sbc.contract_type
LEFT JOIN systems_status AS st ON st.id = sb.`status`
WHERE sbc.contract_start_date = '' 
OR sbc.contract_end_date = '' 
OR sbc.contract_hours = '' 
OR sb.system_type = '' 
OR sb.system_serial = '' 
AND sbc.property = 'C';

ALTER TABLE systems_temp 
DROP archived,
DROP has_files,
DROP mr_lhe_list_update,
DROP mr_lhe_list,
DROP mr_lhe_press,
DROP mr_lhe_contact,
DROP mr_lhe_phone,
DROP travel_ot_rate,
DROP travel_reg_rate,
DROP labor_ot_rate,
DROP labor_reg_rate,
DROP mobile,
DROP log_book,
DROP he_monitor_serial,
DROP he_monitor,
DROP tube_covered,
DROP remote_access,
DROP credit_hold_notes,
DROP notes,
MODIFY COLUMN system VARCHAR(50) AFTER system_type,
MODIFY COLUMN contract_type_name VARCHAR(50) AFTER contract_type,
MODIFY COLUMN system_status VARCHAR(50) AFTER `status`,
MODIFY COLUMN engineer_pri VARCHAR(50) AFTER `status`,
MODIFY COLUMN engineer_sec VARCHAR(50) AFTER engineer_pri;

ALTER TABLE systems_temp 
DROP contract_type,
DROP system_type,
DROP `status`,
CHANGE contract_type_name contract_type VARCHAR(50);

UPDATE systems_temp SET system = NULL WHERE system = 'Unknown CT' OR system = 'Unknown MR';

SELECT * FROM systems_temp;

SHOW COLUMNS FROM systems_temp;

DROP TEMPORARY TABLE IF EXISTS systems_temp;
SQL;

if (mysqli_multi_query($mysqli,$sql)) {
	mysqli_next_result($mysqli); //drop if exist
	mysqli_next_result($mysqli); //create temp
	mysqli_next_result($mysqli); //alter temp
	mysqli_next_result($mysqli); //alter temp
	mysqli_next_result($mysqli); //update equip name
	mysqli_next_result($mysqli); //select all
	if ($result = mysqli_store_result($mysqli)) {
			if(sizeof($result) != 0){
				while ($row = mysqli_fetch_row($result)) {
					array_push($sites,$row);
				}
			mysqli_free_result($result);
		}
	}
	mysqli_next_result($mysqli);//show columns
	if ($result = mysqli_store_result($mysqli)) {
			if(sizeof($result) != 0){
				while ($row = mysqli_fetch_row($result)) {
					array_push($cols,$row[0]);
				}
			mysqli_free_result($result);
		}
	}
}else{
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

mysqli_close($mysqli);
//echo "<pre>",print_r($sites),"</pre>",EOL,EOL,EOL;
//echo "<pre>",print_r($cols),"</pre>",EOL;
//die();

if(empty($sites)){
	$no_report = true;
}else{
	$no_report = false;
}

// Include PHPExcel
require_once ($_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/Classes/PHPExcel.php');

// Include PHPExcel_IOFactory
require_once ($_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/Classes/PHPExcel/IOFactory.php');

$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory_serialized;

PHPExcel_Settings::setCacheStorageMethod($cacheMethod);

PHPExcel_Shared_Font::setAutoSizeMethod(PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);



// Check if template exists
//echo "Check if Excel2007 template file exists<br />";
$template_file = $_SERVER['DOCUMENT_ROOT'].'/resources/report_templates/corrupt_systems.xls';
if (!file_exists($template_file)) {
	$log->logerr('corrupt_systems.php',1045);	
	header("location:/error.php?n=1045&p=viewSystem.php");
}

// Create new PHPExcel object
//echo "Start PHPExcell", EOL;
$objPHPExcel = new PHPExcel();

//echo "Load from Excel2007 template file", EOL;
$objPHPExcel = PHPExcel_IOFactory::load($template_file);


$sharedStyle2 = new PHPExcel_Style();

$sharedStyle2->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_PATTERN_GRAY0625,
								'color'		=> array('argb' => 'FFFF0000')
							),
		 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
		 
));

//Select ct sheet
$objPHPExcel->setActiveSheetIndex(0);

//Set report date
$objPHPExcel->getActiveSheet()->setCellValue('b3', date(phpdispfdt,time()+$_SESSION['tz_offset_sec']));
//add cols
$objPHPExcel->getActiveSheet()->fromArray($cols, NULL, 'A4');


$maxRow = count($sites);
$maxCol = count($cols);

//fix col names
for ($col = 0; $col <= $maxCol; ++$col) {
	$val = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, 4)->getValue();
	$newval = ucwords(preg_replace("/_/"," ",$val));
	$val = $objPHPExcel->getActiveSheet()->setCellValue(getNameFromNumber($col) . 4,$newval);
}

//add rows
$start_row = 5;
$_num = $start_row;
if(!$no_report){
	foreach($sites as $sitekey => $site){
		
		$objPHPExcel->getActiveSheet()->fromArray($site, NULL, 'A'.$_num);
		$_num++;
	}

	for ($row = $start_row; $row < $maxRow + $start_row; ++$row) {
		for ($col = 0; $col <= $maxCol; ++$col) {
			if($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getValue() == ""){
				if(in_array($cols[$col],$required)){
					$objPHPExcel->getActiveSheet()->getStyle(getNameFromNumber($col).$row)->getFill()
						->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
						->getStartColor()->setARGB('22880000');	
				}else{
					$objPHPExcel->getActiveSheet()->getStyle(getNameFromNumber($col).$row)->getFill()
						->setFillType(PHPExcel_Style_Fill::FILL_PATTERN_GRAY0625)
						->getStartColor()->setARGB(PHPExcel_Style_Color::COLOR_DARKYELLOW);
				}
			}
			
			if($col != 0){
				//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(getNameFromNumber($col))->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension(getNameFromNumber($col))->setAutoSize(true);
			}
		}
	}
	
	$_num = $_num + 2;
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$_num,'Required');
	$objPHPExcel->getActiveSheet()->getStyle('B'.$_num)->getFill()
						->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
						->getStartColor()->setARGB('22880000');	
	$_num = $_num + 2;
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$_num,'Preferred');
	$objPHPExcel->getActiveSheet()->getStyle('B'.$_num)->getFill()
						->setFillType(PHPExcel_Style_Fill::FILL_PATTERN_GRAY0625)
						->getStartColor()->setARGB(PHPExcel_Style_Color::COLOR_DARKYELLOW);
	
}else{
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$_num, 'No report available');	
}


//die();

// Redirect output to a client’s web browser (Excel2007)
ini_set('zlib.output_compression','Off');
header('Pragma: public');
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");                  // Date in the past
//header('Last-Modified: '.gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');     // HTTP/1.1
header('Cache-Control: pre-check=0, post-check=0, max-age=0');    // HTTP/1.1
header ("Pragma: no-cache");
header("Expires: 0");
header('Content-Transfer-Encoding: none');
header('Content-Type: application/vnd.ms-excel;');                 // This should work for IE & Opera
header("Content-type: application/x-msexcel");                    // This should work for the rest
header('Content-Disposition: attachment; filename="Corrupt Systems Report '.date(savefdt,time()+$_SESSION['tz_offset_sec']).'.xls"');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');

ob_end_flush();

function getNameFromNumber($num) {
    $numeric = $num % 26;
    $letter = chr(65 + $numeric);
    $num2 = intval($num / 26);
    if ($num2 > 0) {
        return getNameFromNumber($num2 - 1) . $letter;
    } else {
        return $letter;
    }
}
exit("DONE");
?>