<?php
//Update Completed 12/09/2014
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

//Get requests
$assignment_type = 'a';
$systems = array();
$sql=<<<SQL
SELECT u.name, GROUP_CONCAT(sbc.system_id SEPARATOR ', ') AS `systems`
FROM systems_assigned_customer AS a
LEFT JOIN systems_base_cont AS sbc ON sbc.ver_unique_id = a.system_ver_unique_id
LEFT JOIN users AS u ON u.uid = a.uid
WHERE sbc.property = 'C' AND sbc.system_id != '9999'
GROUP BY a.uid
ORDER BY a.uid;
SQL;

if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

while($row = $result->fetch_assoc()){
	array_push($systems,$row);
}

mysqli_close($mysqli);

if(empty($systems)){
	$no_report = true;
}else{
	$no_report = false;
}

// Include PHPExcel
require_once ($_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/Classes/PHPExcel.php');

// Include PHPExcel_IOFactory
require_once ($_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/Classes/PHPExcel/IOFactory.php');

$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory_serialized;

PHPExcel_Settings::setCacheStorageMethod($cacheMethod);

// Check if template exists
//echo "Check if Excel2007 template file exists<br />";
$template_file = $_SERVER['DOCUMENT_ROOT'].'/resources/report_templates/customer_systems.xls';
if (!file_exists($template_file)) {
	$log->logerr('customer_systems.php',1045);	
	header("location:/error.php?n=1045&p=customer_systems.php");
}

// Create new PHPExcel object
//echo "Start PHPExcell", EOL;
$objPHPExcel = new PHPExcel();

//echo "Load from Excel2007 template file", EOL;
$objPHPExcel = PHPExcel_IOFactory::load($template_file);


$sharedStyle2 = new PHPExcel_Style();

$sharedStyle2->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_PATTERN_GRAY0625,
								'color'		=> array('argb' => 'FFFF0000')
							),
		 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
		 
));

//Select ct sheet
$objPHPExcel->setActiveSheetIndex(0);

//Set report date
$objPHPExcel->getActiveSheet()->setCellValue('b3', date(phpdispfdt,time()+$_SESSION['tz_offset_sec']));

//add rows
$_num = 5;
$type = "";
if(!$no_report){
	$last_key = end(array_keys($systems));
	foreach($systems as $systemkey => $system){
		switch($assignment_type){
			case 'p':
				$type = "Primary";
				break;
			case 's':
				$type = "Secondary";
				break;	
			case 'a':
				$type = "Assigned";
				break;
		}
		$objPHPExcel->getActiveSheet()->setCellValue('A' . $_num, $type);
		$objPHPExcel->getActiveSheet()->setCellValue('B' . $_num, $system['name']);
		$objPHPExcel->getActiveSheet()->setCellValue('C' . $_num, $system['systems']);
		$_num++;
	}
}else{
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$_num, 'No report available');	
}

// Redirect output to a client’s web browser (Excel2007)
ini_set('zlib.output_compression','Off');
header('Pragma: public');
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");                  // Date in the past
//header('Last-Modified: '.gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');     // HTTP/1.1
header('Cache-Control: pre-check=0, post-check=0, max-age=0');    // HTTP/1.1
header ("Pragma: no-cache");
header("Expires: 0");
header('Content-Transfer-Encoding: none');
header('Content-Type: application/vnd.ms-excel;');                 // This should work for IE & Opera
header("Content-type: application/x-msexcel");                    // This should work for the rest
header('Content-Disposition: attachment; filename="Customer Systems Report '.date(savefdt,time()+$_SESSION['tz_offset_sec']).'.xls"');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');

ob_end_flush();

function search($needle, $haystack){
	foreach($haystack as $id => $val)
	{
		if($val['system_id'] == $needle) {
			return $id;			
		}
	}
}
exit("DONE");
?>