<?php
//Update Completed 12/11/14
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

//Get requests
$reports = array();
$sql="SELECT r.*,u.name AS assigned, e.name AS editor
FROM systems_reports AS r
LEFT JOIN users AS u ON r.assigned_engineer = u.uid
LEFT JOIN users AS e ON r.report_edited_by = e.uid
WHERE r.report_edited = 'y'
ORDER BY r.report_id DESC;";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

while($row = $result->fetch_assoc()){
	array_push($reports,$row);
}


if(empty($reports)){
	$no_report = true;
}else{
	$no_report = false;
}


// Include PHPExcel
require_once ($_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/Classes/PHPExcel.php');

// Include PHPExcel_IOFactory
require_once ($_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/Classes/PHPExcel/IOFactory.php');

// Check if template exists
//echo "Check if Excel2007 template file exists<br />";
$template_file = $_SERVER['DOCUMENT_ROOT'].'/resources/report_templates/edited_reports.xls';
if (!file_exists($template_file)) {
	$log->logerr('edited_reports.php',1045);
	exit("Error the template is missing." . EOL);
}

// Create new PHPExcel object
//echo "Start PHPExcell", EOL;
$objPHPExcel = new PHPExcel();

//echo "Load from Excel2007 template file", EOL;
$objPHPExcel = PHPExcel_IOFactory::load($template_file);

$sharedStyle1 = new PHPExcel_Style();

$sharedStyle1->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('argb' => 'FFFFCCCC')
							),
		 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
		 
));

$sharedStyle1a = new PHPExcel_Style();

$sharedStyle1a->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('argb' => 'FFFFCCCC')
							),
		 'alignment' => array(
		 						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		 					  	'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
							)
		 
));

$sharedStyle2 = new PHPExcel_Style();

$sharedStyle2->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('argb' => 'FFD7E4BC')
							),
		 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
		 
));

$sharedStyle2a = new PHPExcel_Style();

$sharedStyle2a->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('argb' => 'FFD7E4BC')
							),
		 'alignment' => array(
		 						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		 					  	'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
							)
		 
));

$sharedStyle3 = new PHPExcel_Style();

$sharedStyle3->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('argb' => 'FFB8CCE4')
							),
		 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
		 
));

$sharedStyle3a = new PHPExcel_Style();

$sharedStyle3a->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('argb' => 'FFB8CCE4')
							),
		 'alignment' => array(
		 						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		 					  	'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
							)
		 
));

//Select ct sheet
$objPHPExcel->setActiveSheetIndex(0);



//Set report date
$objPHPExcel->getActiveSheet()->setCellValue('b3', date(phpdispfdt,time()+$_SESSION['tz_offset_sec']));

//add rows
$_num = 5;
if(!$no_report){
	$last_key = end(array_keys($reports));
	foreach($reports as $key => $report){
				
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$_num, $report['report_id']);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$_num, $report['system_nickname']);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$_num, $report['assigned']);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$_num, $report['editor']);
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$_num, $report['report_edited_date']);
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$_num, $report['report_edited_notes']);
		
		$_num++;
	}
}else{
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$_num, 'No report available');	
}

// Redirect output to a client’s web browser (Excel2007)
ini_set('zlib.output_compression','Off');
header('Pragma: public');
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");                  // Date in the past
//header('Last-Modified: '.gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');     // HTTP/1.1
header('Cache-Control: pre-check=0, post-check=0, max-age=0');    // HTTP/1.1
header ("Pragma: no-cache");
header("Expires: 0");
header('Content-Transfer-Encoding: none');
header('Content-Type: application/vnd.ms-excel;');                 // This should work for IE & Opera
header("Content-type: application/x-msexcel");                    // This should work for the rest
header('Content-Disposition: attachment; filename="Edited Service Reports '.date(savefdt,time()+$_SESSION['tz_offset_sec']).'.xls"');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');

ob_end_flush();

function search($needle, $haystack){
	foreach($haystack as $id => $val)
	{
		if($val['system_id'] == $needle) {
			return $id;			
		}
	}
}
exit("DONE");
?>