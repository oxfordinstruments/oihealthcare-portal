<?php
//Template is in the dms

$debug = false;

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

//Get Primary list
$pri_eng = array();
$sql="SELECT sb.system_id, sbc.nickname, f.city, f.state, sbc.contact_name AS system_contact_name, sbc.contact_phone AS system_contact_phone, f.contact_name, 
f.contact_phone, st.name AS equipment_name, st.`type` AS equipment_type, 
sb.sw_ver, u.name AS engineer_name, u.cell, sbc.ver_unique_id
FROM systems_assigned_pri AS a
LEFT JOIN users AS u ON a.uid = u.uid
LEFT JOIN systems_base_cont AS sbc ON a.system_ver_unique_id = sbc.ver_unique_id
LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
LEFT JOIN facilities AS f ON f.unique_id = sbc.facility_unique_id
LEFT JOIN systems_types AS st ON sb.system_type = st.id
WHERE sbc.property = 'C'
ORDER BY sbc.system_id ASC;";
d($sql);
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

while($row = $result->fetch_assoc()){
    //echo $row['system_id'] . "  " . $row['pri']. '<br />';
	//array_push($pri_eng,$row);
	$pri_eng[$row['ver_unique_id']] = array('system_id'=>$row['system_id'],
											'nickname'=>$row['nickname'],
											'city'=>$row['city'],
											'state'=>$row['state'],
											'system_contact_name'=>$row['system_contact_name'],
											'system_contact_phone'=>$row['system_contact_phone'],
											'contact_name'=>$row['contact_name'],
											'contact_phone'=>$row['contact_phone'],
											'equipment_name'=>$row['equipment_name'],
											'equipment_type'=>$row['equipment_type'],
											'sw_ver'=>$row['sw_ver'],
											'engineer_name'=>$row['engineer_name'],
											'cell'=>$row['cell']);
}
d($pri_eng);

//Get Secondary list
$sec_eng = array();
$sql="SELECT sbc.system_id, u.name, u.cell, sbc.ver_unique_id
FROM systems_assigned_sec AS a
LEFT JOIN users AS u ON a.uid = u.uid
LEFT JOIN systems_base_cont AS sbc ON a.system_ver_unique_id = sbc.ver_unique_id
WHERE sbc.property = 'C'
ORDER BY sbc.system_id ASC;";
d($sql);
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

while($row = $result->fetch_assoc()){
    //echo $row['system_id'] . "  " . $row['pri']. '<br />';
	//array_push($sec_eng,$row);
	$sec_eng[$row['ver_unique_id']] = array('system_id'=>$row['system_id'],
											'name'=>$row['name'],
											'cell'=>$row['cell']);
}
d($sec_eng);

//create master list
$eng_asmt = array();
foreach ($pri_eng as $key => $value) {
	if($value['system_id'] != "9999"){
		array_push($eng_asmt,array("system_id"=>$value['system_id'],
								   "nickname"=>$value['nickname'],
								   "city"=>$value['city'],
								   "state"=>$value['state'],
								   "contact_name"=>$value['contact_name'],
								   "contact_phone"=>$value['contact_phone'],
								   "system_contact_name"=>$value['system_contact_name'],
								   "system_contact_phone"=>$value['system_contact_phone'],							   
								   "equipment_name"=>$value['equipment_name'],
								   "equipment_type"=>$value['equipment_type'],
								   "equipment_sw_ver"=>$value['sw_ver'],
								   "pri_name"=>$value['engineer_name'],
								   "pri_cell"=>$value['cell'],
								   "sec_name"=>$sec_eng[$key]['name'],
								   "sec_cell"=>$sec_eng[$key]['cell']
								   ));
	}
}
d($eng_asmt);

//Create CT and MR list
$eng_asmt_ct = array();
$eng_asmt_mr = array();
$eng_asmt_nm = array();
$eng_asmt_pct = array();
foreach ($eng_asmt as $key => $value) {
	switch(strtolower($value['equipment_type'])){
		case 'ct':
			array_push($eng_asmt_ct,$value);
			break;
		case 'mr':
			array_push($eng_asmt_mr,$value);
			break;
		case 'nm':
			array_push($eng_asmt_nm,$value);
			break;
		case 'pet':
			array_push($eng_asmt_pct,$value);
			break;
	}
}
d($eng_asmt_ct);
d($eng_asmt_mr);
d($eng_asmt_nm);
dd($eng_asmt_pct);

//change to dms database
mysqli_select_db($mysqli, "$db_name_dms") or die("cannot select DB");
$sql="SELECT d.name, dc.`version`, dc.dir, dc.fileType, dc.orgFileName
FROM tblDocuments AS d
LEFT JOIN tblDocumentContent AS dc ON dc.document = d.id
WHERE d.name LIKE('%Portal F70-23%')
AND dc.fileType LIKE('%xls%')
ORDER BY dc.`version` DESC
LIMIT 1;";
if(!$formRresult = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowForm = $formRresult->fetch_assoc();

// Include PHPExcel
require_once ($_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/Classes/PHPExcel.php');

// Include PHPExcel_IOFactory
require_once ($_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/Classes/PHPExcel/IOFactory.php');

// Check if template exists
//echo "Check if Excel2007 template file exists<br />";
//$template_file = $_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/templates/CT- MRI System Contract List.xls';
if(!$testing_server){
	$template_file = $settings->dms_files_dir . $rowForm['dir'] . $rowForm['version'] . $rowForm['fileType']; //added for use with dms
}else{
	$template_file = $settings->dms_files_dir_testing . $rowForm['dir'] . $rowForm['version'] . $rowForm['fileType']; //added for use with dms
}

if (!file_exists($template_file)) {
	$log->logerr($template_file,1045);
	exit("Error the template is missing: " . $template_file . EOL);
}


// Create new PHPExcel object
//echo "Start PHPExcell", EOL;
$objPHPExcel = new PHPExcel();

//echo "Load from Excel2007 template file", EOL;
$objPHPExcel = PHPExcel_IOFactory::load($template_file);

//Select ct sheet
$objPHPExcel->setActiveSheetIndex(0);

//update header
$gen_time = time() + (int)$_SESSION['tz_offset_sec'];
$gen_date = date(phpdispfdt,$gen_time);

$objPHPExcel->getActiveSheet()->setCellValue('D1',$gen_date);

//add rows
$ct_num = 5;
foreach($eng_asmt_ct as $asmt_ct){
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$ct_num, $asmt_ct['system_id']);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$ct_num, $asmt_ct['nickname']);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$ct_num, $asmt_ct['city']);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$ct_num, $asmt_ct['state']);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$ct_num, $asmt_ct['contact_name']);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$ct_num, $asmt_ct['contact_phone']);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$ct_num, $asmt_ct['system_contact_name']);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$ct_num, $asmt_ct['system_contact_phone']);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$ct_num, $asmt_ct['equipment_name']);
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$ct_num, $asmt_ct['pri_name']);
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$ct_num, $asmt_ct['pri_cell']);
	$objPHPExcel->getActiveSheet()->setCellValue('L'.$ct_num, $asmt_ct['sec_name']);
	$objPHPExcel->getActiveSheet()->setCellValue('M'.$ct_num, $asmt_ct['sec_cell']);
	$ct_num++;
}

//Select mr sheet
$objPHPExcel->setActiveSheetIndex(1);

//update header
$objPHPExcel->getActiveSheet()->setCellValue('D1',$gen_date);

//add rows
$mr_num = 5;
foreach($eng_asmt_mr as $asmt_mr){
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$mr_num, $asmt_mr['system_id']);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$mr_num, $asmt_mr['nickname']);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$mr_num, $asmt_mr['city']);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$mr_num, $asmt_mr['state']);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$mr_num, $asmt_mr['contact_name']);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$mr_num, $asmt_mr['contact_phone']);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$mr_num, $asmt_mr['system_contact_name']);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$mr_num, $asmt_mr['system_contact_phone']);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$mr_num, $asmt_mr['equipment_name']);
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$mr_num, $asmt_mr['equipment_sw_ver']);
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$mr_num, $asmt_mr['pri_name']);
	$objPHPExcel->getActiveSheet()->setCellValue('L'.$mr_num, $asmt_mr['pri_cell']);
	$objPHPExcel->getActiveSheet()->setCellValue('M'.$mr_num, $asmt_mr['sec_name']);
	$objPHPExcel->getActiveSheet()->setCellValue('N'.$mr_num, $asmt_mr['sec_cell']);
	$mr_num++;
}

//Select nm sheet
$objPHPExcel->setActiveSheetIndex(2);

//update header
$objPHPExcel->getActiveSheet()->setCellValue('D1',$gen_date);

//add rows
$nm_num = 5;
foreach($eng_asmt_nm as $asmt_nm){
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$nm_num, $asmt_nm['system_id']);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$nm_num, $asmt_nm['nickname']);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$nm_num, $asmt_nm['city']);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$nm_num, $asmt_nm['state']);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$nm_num, $asmt_nm['contact_name']);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$nm_num, $asmt_nm['contact_phone']);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$nm_num, $asmt_nm['system_contact_name']);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$nm_num, $asmt_nm['system_contact_phone']);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$nm_num, $asmt_nm['equipment_name']);
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$nm_num, $asmt_nm['equipment_sw_ver']);
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$nm_num, $asmt_nm['pri_name']);
	$objPHPExcel->getActiveSheet()->setCellValue('L'.$nm_num, $asmt_nm['pri_cell']);
	$objPHPExcel->getActiveSheet()->setCellValue('M'.$nm_num, $asmt_nm['sec_name']);
	$objPHPExcel->getActiveSheet()->setCellValue('N'.$nm_num, $asmt_nm['sec_cell']);
	$nm_num++;
}

//Select pct sheet
$objPHPExcel->setActiveSheetIndex(3);

//update header
$objPHPExcel->getActiveSheet()->setCellValue('D1',$gen_date);

//add rows
$pct_num = 5;
foreach($eng_asmt_pct as $asmt_pct){
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$pct_num, $asmt_pct['system_id']);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$pct_num, $asmt_pct['nickname']);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$pct_num, $asmt_pct['city']);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$pct_num, $asmt_pct['state']);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$pct_num, $asmt_pct['contact_name']);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$pct_num, $asmt_pct['contact_phone']);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$pct_num, $asmt_pct['system_contact_name']);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$pct_num, $asmt_pct['system_contact_phone']);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$pct_num, $asmt_pct['equipment_name']);
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$pct_num, $asmt_pct['equipment_sw_ver']);
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$pct_num, $asmt_pct['pri_name']);
	$objPHPExcel->getActiveSheet()->setCellValue('L'.$pct_num, $asmt_pct['pri_cell']);
	$objPHPExcel->getActiveSheet()->setCellValue('M'.$pct_num, $asmt_pct['sec_name']);
	$objPHPExcel->getActiveSheet()->setCellValue('N'.$pct_num, $asmt_pct['sec_cell']);
	$pct_num++;
}

//Select ct sheet so file opens to first sheet
$objPHPExcel->setActiveSheetIndex(0);

//write file
//echo "Write to Excel2007 format", EOL;
//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
//echo "<a href=\"http://secure.jrdhome.com/report/common/reports/".str_replace('.php', '.xlsx', pathinfo(__FILE__, PATHINFO_BASENAME))."\">".str_replace('.php', '.xlsx', pathinfo(__FILE__, PATHINFO_BASENAME))."</a>", EOL;

// Redirect output to a client’s web browser (Excel2007)
ini_set('zlib.output_compression','Off');
header('Pragma: public');
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");                  // Date in the past
//header('Last-Modified: '.gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');     // HTTP/1.1
header('Cache-Control: pre-check=0, post-check=0, max-age=0');    // HTTP/1.1
header ("Pragma: no-cache");
header("Expires: 0");

header('Content-Transfer-Encoding: none');
header('Content-Type: application/vnd.ms-excel;');                 // This should work for IE & Opera
header("Content-type: application/x-msexcel");                    // This should work for the rest
header('Content-Disposition: attachment; filename="F70-23 Engineer System Assignments List '.date(savefdt,$gen_time).'.xls"');


$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');

ob_end_flush();
exit("DONE");
?>