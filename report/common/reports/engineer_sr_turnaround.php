<?php
//Update Completed 12/11/14
$debug = false;

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

if(isset($_GET['year'])){
	$year = $_GET['year'];	
}else{
	$year = date('Y',time());	
}
	

//Get Service Report Turnaround times
$sr_turn = array();
$sql="SELECT r.report_id, r.user_id, u.name, r.system_nickname, r.report_edited, r.report_edited_date, r.report_edited_by, u1.name as edit_name,
r.`date`, r.finalize_date, 
DATEDIFF(r.finalize_date, r.`date`) AS diff_days
FROM systems_reports AS r
LEFT JOIN users as u ON u.uid = r.user_id
LEFT JOIN users as u1 ON u1.uid = r.report_edited_by
WHERE CAST(r.report_id AS UNSIGNED) >= 60070 
AND r.`status` = 'closed'
AND YEAR(r.`date`) = '".$year."'
ORDER BY r.report_id DESC;";
d($sql);
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

while($row = $result->fetch_assoc()){
	array_push($sr_turn,$row);
}
d($sr_turn);

//Get engineers
$engineers = array();
$sql="SELECT u.uid, u.name
FROM users AS u
LEFT JOIN users_groups AS ug ON ug.uid = u.uid
LEFT JOIN users_group_id AS gid ON gid.id = ug.gid
LEFT JOIN users_roles AS ur ON ur.uid = u.uid
LEFT JOIN users_role_id AS rid ON rid.id = ur.rid
WHERE (rid.role = 'role_engineer' OR rid.role = 'role_contractor') AND u.id > 9 AND u.active = 'y'
ORDER BY gid.`group` DESC, u.uid ASC;";
d($sql);
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

while($row = $result->fetch_assoc()){
	array_push($engineers,$row);
}
d($engineers);

// Include PHPExcel
require_once ($_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/Classes/PHPExcel.php');

// Include PHPExcel_IOFactory
require_once ($_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/Classes/PHPExcel/IOFactory.php');

// Check if template exists
//echo "Check if Excel2007 template file exists<br />";
$template_file = $_SERVER['DOCUMENT_ROOT'].'/resources/report_templates/engineer_sr_turnaround.xls';
if (!file_exists($template_file)) {
	$log->logerr($template_file,1045);
	exit("Error the template is missing." . EOL);
}

// Create new PHPExcel object
//echo "Start PHPExcell", EOL;
$objPHPExcel = new PHPExcel();

//echo "Load from Excel2007 template file", EOL;
$objPHPExcel = PHPExcel_IOFactory::load($template_file);

$sharedStyle1 = new PHPExcel_Style();

$sharedStyle1->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('argb' => 'FFFFCCCC')
							),
		 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
		 
));

$sharedStyle1a = new PHPExcel_Style();

$sharedStyle1a->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('argb' => 'FFFF5555')
							),
		 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
		 
));

$sharedStyle2 = new PHPExcel_Style();

$sharedStyle2->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('argb' => 'FFD7E4BC')
							),
		 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
		 
));

$sharedStyle2a = new PHPExcel_Style();

$sharedStyle2a->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('argb' => 'FFD7E4BC')
							),
		 'alignment' => array(
		 						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		 					  	'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
							)
		 
));

$sharedStyle3 = new PHPExcel_Style();

$sharedStyle3->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('argb' => 'FFB8CCE4')
							),
		 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
		 
));

$sharedStyle3a = new PHPExcel_Style();

$sharedStyle3a->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('argb' => 'FFB8CCE4')
							),
		 'alignment' => array(
		 						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		 					  	'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
							)
		 
));

//Select ct sheet
$objPHPExcel->setActiveSheetIndex(0);

$report_date = date(phpdispfdt,time()+$_SESSION['tz_offset_sec']);

//Set report date
$objPHPExcel->getActiveSheet()->setCellValue('b3', $report_date);
//Set reporting year
$objPHPExcel->getActiveSheet()->setCellValue('b4', $year);

//Set legend Colors
$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1, "G2");
$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1a, "H2");
$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle2, "I2");

//add rows
$_num = 6;
$last_key = end(array_keys($sr_turn));
foreach($sr_turn as $key => $report){
			
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$_num, $report['report_id']);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$_num, $report['system_nickname']);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$_num, $report['name']);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$_num, date(phpdispfd,strtotime($report['date'])));
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$_num, date(phpdispfd,strtotime($report['finalize_date'])));
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$_num, $report['diff_days']);
	if(strtolower($report['report_edited']) == 'y'){
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$_num, "X");
	}
	if($rowReport['report_edited_date'] != ''){
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$_num, date(phpdispfd,strtotime($report['report_edited_date'])));
	}
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$_num, $report['edit_name']);
	
	if(intval($report['diff_days']) > 7){
		$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1, "A".$_num.":I".$_num);
	}
	
	if(intval($report['diff_days']) > 30){
		$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1a, "A".$_num.":I".$_num);
	}
	
	if(strtolower($report['report_edited']) == 'y'){
		$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle2, "A".$_num.":I".$_num);
	}
	
	$_num++;
}


//Do engineers avgs
$objPHPExcel->setActiveSheetIndex(1);
$baseSheet = $objPHPExcel->getActiveSheet();

foreach($engineers as $key=>$engineer){
	
	if($key != 0){
		$newSheet = clone $baseSheet;
		$newSheet->setTitle($engineer['name']);
		$objPHPExcel->addSheet($newSheet,$key + 1);
		$objPHPExcel->setActiveSheetIndex($key + 1);	
	}else{
		$objPHPExcel->getActiveSheet()->setTitle($engineer['name']);	
	}
	
	//Set report date
	$objPHPExcel->getActiveSheet()->setCellValue('b3', $report_date);
	
	//Get engineer avg turnaround
	$sql="SELECT r.user_id, AVG(DATEDIFF(r.finalize_date, r.`date`)) AS average
	FROM systems_reports AS r
	WHERE r.user_id = '".$engineer['uid']."' 
	AND CAST(r.report_id AS UNSIGNED) >= 60070 
	AND r.`status` = 'closed' 
	AND YEAR(r.`date`) = '".$year."'
	AND r.report_edited = 'n';";
	if(!$resultAvg = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$rowAvg = $resultAvg->fetch_assoc();
	
	//Get engineer over 7 turnaround
	$sql="SELECT r.report_id, r.user_id, r.system_nickname, r.`date`, r.finalize_date,
	DATEDIFF(r.finalize_date, r.`date`) AS diff,
	r.report_edited, r.report_edited_date, r.report_edited_by, u1.name as edit_name
	FROM systems_reports AS r
	LEFT JOIN users as u1 ON u1.uid = r.report_edited_by
	WHERE r.user_id = '".$engineer['uid']."' 
	AND CAST(r.report_id AS UNSIGNED) >= 60070 
	AND r.`status` = 'closed' 
	AND YEAR(r.`date`) = '".$year."'
	AND DATEDIFF(r.finalize_date, r.`date`) > 7
	ORDER BY r.report_id DESC;";
	if(!$resultReports = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$num_reports = $resultReports->num_rows;
	
	$objPHPExcel->getActiveSheet()->setCellValue('B3', $report_date);
	$objPHPExcel->getActiveSheet()->setCellValue('C4', $year);
	$objPHPExcel->getActiveSheet()->setCellValue('C5', $engineer['name']);
	$objPHPExcel->getActiveSheet()->setCellValue('C6', $rowAvg['average']);
	$objPHPExcel->getActiveSheet()->setCellValue('C7', $num_reports);
	
	//add rows
	$_num = 10;
	while($rowReport = $resultReports->fetch_assoc()){
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$_num, $rowReport['report_id']);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$_num, $rowReport['system_nickname']);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$_num, date(phpdispfd,strtotime($rowReport['date'])));
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$_num, date(phpdispfd,strtotime($rowReport['finalize_date'])));
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$_num, $rowReport['diff']);
		if(strtolower($rowReport['report_edited']) == 'y'){
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$_num, "X");	
		}
		if($rowReport['report_edited_date'] != ''){
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$_num, date(phpdispfd,strtotime($rowReport['report_edited_date'])));
		}
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$_num, $rowReport['edit_name']);
		
		$_num++;	
	}
	
}


$objPHPExcel->setActiveSheetIndex(0);

dd($objPHPExcel);

// Redirect output to a client’s web browser (Excel2007)
ini_set('zlib.output_compression','Off');
header('Pragma: public');
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");                  // Date in the past
//header('Last-Modified: '.gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');     // HTTP/1.1
header('Cache-Control: pre-check=0, post-check=0, max-age=0');    // HTTP/1.1
header ("Pragma: no-cache");
header("Expires: 0");
header('Content-Transfer-Encoding: none');
header('Content-Type: application/vnd.ms-excel;');                 // This should work for IE & Opera
header("Content-type: application/x-msexcel");                    // This should work for the rest
header('Content-Disposition: attachment; filename="Service Report Turnaround '.date(savefdt,time()+$_SESSION['tz_offset_sec']).'.xls"');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');

ob_end_flush();

function search($needle, $haystack){
	foreach($haystack as $id => $val)
	{
		if($val['system_id'] == $needle) {
			return $id;			
		}
	}
}
exit("DONE");
?>