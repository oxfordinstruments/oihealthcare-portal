<?php
//Update Completed 12/11/14

//error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);
//ini_set("display_errors", true);

error_reporting(0);
ini_set("display_errors", false);


$debug = false;
if(isset($_GET['debug'])){
	$debug = true;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

		
require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

if(!isset($_GET['year'])){
	$log->logerr('engineer_utilize.php',1046);	
	header("location:/error.php?n=1046&p=engineer_utilize.php");
}
$fiscal_year = $_GET['year']; //fiscal 1st period year

$no_report = false; //sets true when there is report data

$complete_year = true; //sets false if get[year] = cur year
if($fiscal_year == date('Y')){
	if(date('m/d/Y') != '12/31/'.$fiscal_year){
		$complete_year = false;
	}
}

//Get engineers
$engineers_names = array();
$engineers_data = array();
$sql="SELECT u.uid, u.name
FROM users AS u
LEFT JOIN users_groups AS ug ON ug.uid = u.uid
LEFT JOIN users_group_id AS gid ON gid.id = ug.gid
LEFT JOIN users_roles AS ur ON ur.uid = u.uid
LEFT JOIN users_role_id AS rid ON rid.id = ur.rid
WHERE (rid.role = 'role_engineer' OR rid.role = 'role_contractor') AND u.id > 9 AND u.active = 'y'
ORDER BY gid.`group` DESC, u.uid ASC;";
d($sql);
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
while($row = $result->fetch_assoc()){
	$engineers_names[$row['uid']] = $row['name'];
	$engineers_data[$row['uid']] = array('name'=>$row['name'],'rl'=>0,'ol'=>0,'rt'=>0,'ot'=>0,'dy'=>0,'dt'=>0,'rp'=>0);
}
d($engineers_names);
d($engineers_data);


//Get sql data
$sql_data = array();
$year_systems_count = 0;
for($i = 1; $i < 13; $i++){
	$temp_month_data = array();
	//get distinct systems count for year
	$sql="SELECT count(distinct(rp.system_id)) as `count`
	FROM systems_reports AS rp
	WHERE rp.`status` = 'closed' 
	AND YEAR(rp.`date`) = '$fiscal_year';";
	d($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	while($row = $result->fetch_assoc()){
		$year_systems_count = intval($row['count']);
	}
	d($year_systems_count);
	
	$sql="SELECT rp.assigned_engineer,
	(SELECT COUNT(systems_hours.id) FROM systems_hours WHERE systems_hours.unique_id = rp.unique_id) AS days,
	(SELECT SUM(systems_hours.reg_labor) FROM systems_hours WHERE systems_hours.unique_id = rp.unique_id) AS reg_labor,
	(SELECT SUM(systems_hours.ot_labor) FROM systems_hours WHERE systems_hours.unique_id = rp.unique_id) AS ot_labor,
	(SELECT SUM(systems_hours.reg_travel) FROM systems_hours WHERE systems_hours.unique_id = rp.unique_id) AS reg_travel,
	(SELECT SUM(systems_hours.ot_travel) FROM systems_hours WHERE systems_hours.unique_id = rp.unique_id) AS ot_travel,
	rp.downtime
	FROM systems_reports AS rp
	WHERE rp.`status` = 'closed' 
	AND rp.`date` BETWEEN DATE_FORMAT(STR_TO_DATE('$i/$fiscal_year','%m/%Y'), '%Y-%m-01') AND LAST_DAY(STR_TO_DATE('$i/$fiscal_year','%m/%Y'))
	ORDER BY rp.assigned_engineer ASC;";
	d($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	while($row = $result->fetch_assoc()){
		array_push($temp_month_data,$row);
	}
	$sql_data[$i]['data'] = $temp_month_data;
	
	//get distinct systems count for month
	$sql="SELECT count(distinct(rp.system_id)) as `count`
	FROM systems_reports AS rp
	WHERE rp.`status` = 'closed' 
	AND rp.`date` BETWEEN DATE_FORMAT(STR_TO_DATE('$i/$fiscal_year','%m/%Y'), '%Y-%m-01') AND LAST_DAY(STR_TO_DATE('$i/$fiscal_year','%m/%Y'));";
	d($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	while($row = $result->fetch_assoc()){
		$sql_data[$i]['systems_count'] = intval($row['count']);
	}
}
d($sql_data);

//Tally up data per month and add year total to engineers_names
$final_data = array();
foreach($sql_data as $month_key=>$month_data){
	
	//make empty engineers_data
	$temp_data = array();
	foreach($engineers_names as $uid=>$name){
		$temp_data[$uid] = array('name'=>$name,'rl'=>0,'ol'=>0,'rt'=>0,'ot'=>0,'dy'=>0,'dt'=>0,'rp'=>0);	
	}	
	if($month_data['systems_count'] > 0){
		foreach($month_data['data'] as $data){
			//month totals per eng
			if(!isset($engineers_names[$data['assigned_engineer']])){
				$temp_data[$data['assigned_engineer']] = array('name'=>$data['assigned_engineer'].' - non user','rl'=>0,'ol'=>0,'rt'=>0,'ot'=>0,'dy'=>0,'dt'=>0,'rp'=>0);
			}
			$temp_data[$data['assigned_engineer']]['rl'] += $data['reg_labor'];
			$temp_data[$data['assigned_engineer']]['ol'] += $data['ot_labor'];
			$temp_data[$data['assigned_engineer']]['rt'] += $data['reg_travel'];
			$temp_data[$data['assigned_engineer']]['ot'] += $data['ot_travel'];
			$temp_data[$data['assigned_engineer']]['dt'] += $data['downtime'];
			$temp_data[$data['assigned_engineer']]['dy'] += $data['days'];
			$temp_data[$data['assigned_engineer']]['rp'] ++;
			
			//add to engineer year totals
			if(!isset($engineers_names[$data['assigned_engineer']])){
				$engineers_data[$data['assigned_engineer']] = array('name'=>$data['assigned_engineer'].' - non user','rl'=>0,'ol'=>0,'rt'=>0,'ot'=>0,'dy'=>0,'dt'=>0,'rp'=>0);
			}
			$engineers_data[$data['assigned_engineer']]['rl'] += $data['reg_labor'];
			$engineers_data[$data['assigned_engineer']]['ol'] += $data['ot_labor'];
			$engineers_data[$data['assigned_engineer']]['rt'] += $data['reg_travel'];
			$engineers_data[$data['assigned_engineer']]['ot'] += $data['ot_travel'];
			$engineers_data[$data['assigned_engineer']]['dt'] += $data['downtime'];
			$engineers_data[$data['assigned_engineer']]['dy'] += $data['days'];
			$engineers_data[$data['assigned_engineer']]['rp'] ++;
		}
		$final_data[$month_key]['data'] = $temp_data;
		$final_data[$month_key]['systems_count'] = $month_data['systems_count'];
	}else{
		$final_data[$month_key] = false;	
	}	
}
//Kint::dump($final_data);

//Clean up tallied data and gather totals
$totals = array('total_reg_labor' => 0.0, 'total_ot_labor' => 0.0, 'total_reg_travel' => 0.0, 'total_ot_travel' => 0.0, 'total_days' => 0.0, 'total_downtime' => 0.0, 'total_reports' => 0,'systems_count' => $year_systems_count);
foreach($final_data as $key=>$months){
	if($months){
		foreach($months['data'] as $uid=>$data){
			$totals['total_reg_labor'] += $data['rl'];
			$totals['total_ot_labor'] += $data['ol'];
			$totals['total_reg_travel'] += $data['rt'];
			$totals['total_ot_travel'] += $data['ot'];
			$totals['total_days'] += $data['dy'];
			$totals['total_downtime'] += $data['dt'];
			$totals['total_reports']++;
			if(array_sum(array_slice($data,1)) == 0){
				unset($final_data[$key]['data'][$uid]);	
			}
		}
	}
}
$totals['total_labor'] = intval($totals['total_reg_labor']) + intval($totals['total_ot_labor']);
$totals['total_travel'] = intval($totals['total_reg_travel']) + intval($totals['total_ot_travel']);

//Clean up engineers_data
foreach($engineers_data as $uid=>$data){
	if(array_sum(array_slice($data,1)) == 0){
		unset($engineers_data[$uid]);	
	}	
}


d($engineers_data);
d($final_data);
d($totals);

// Include PHPExcel
//require_once ($_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/Classes/PHPExcel.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/Classes/PHPExcel.php');

// Include PHPExcel_IOFactory
//require_once ($_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/Classes/PHPExcel/IOFactory.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/Classes/PHPExcel/IOFactory.php');

// Check if template exists
//echo "Check if Excel2007 template file exists<br />";
$template_file = $_SERVER['DOCUMENT_ROOT'].'/resources/report_templates/engineer_utilize.xls';
if (!file_exists($template_file)) {
	$log->logerr($template_file,1045);
	exit("Error the template is missing." . EOL);
}

// Create new PHPExcel object
//echo "Start PHPExcell", EOL;
$objPHPExcel = new PHPExcel();

//echo "Load from Excel2007 template file", EOL;
$objPHPExcel = PHPExcel_IOFactory::load($template_file);

$sharedStyle1 = new PHPExcel_Style();

$sharedStyle1->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('argb' => 'FFFFCCCC')
							),
		 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
		 
));

$sharedStyle1a = new PHPExcel_Style();

$sharedStyle1a->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('argb' => 'FFFF5555')
							),
		 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
		 
));

$sharedStyle2 = new PHPExcel_Style();

$sharedStyle2->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('argb' => 'FFD7E4BC')
							),
		 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
		 
));

$sharedStyle2a = new PHPExcel_Style();

$sharedStyle2a->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('argb' => 'FFD7E4BC')
							),
		 'alignment' => array(
		 						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		 					  	'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
							)
		 
));

$sharedStyle3 = new PHPExcel_Style();

$sharedStyle3->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('argb' => 'FFB8CCE4')
							),
		 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
		 
));

$sharedStyle3a = new PHPExcel_Style();

$sharedStyle3a->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('argb' => 'FFB8CCE4')
							),
		 'alignment' => array(
		 						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		 					  	'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
							)
		 
));

$objPHPExcel->setActiveSheetIndex(0);
$report_date = date(phpdispfdt,time()+$_SESSION['tz_offset_sec']);


	
//Year Page
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setCellValue('B3', $report_date);
$objPHPExcel->getActiveSheet()->setCellValue('E2', $fiscal_year);

if(!$complete_year){
	$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Caution reported year is the current year, data may be incomplete');	
}

$objPHPExcel->getActiveSheet()->setCellValue('B5', $totals['total_reg_labor']);
$objPHPExcel->getActiveSheet()->setCellValue('B6', $totals['total_ot_labor']);
$objPHPExcel->getActiveSheet()->setCellValue('B7', $totals['total_labor']);
$objPHPExcel->getActiveSheet()->setCellValue('B9', $totals['total_reg_travel']);
$objPHPExcel->getActiveSheet()->setCellValue('B10', $totals['total_ot_travel']);
$objPHPExcel->getActiveSheet()->setCellValue('B11', $totals['total_travel']);
$objPHPExcel->getActiveSheet()->setCellValue('B13', $totals['total_days']);
$objPHPExcel->getActiveSheet()->setCellValue('B15', $totals['total_reports']);
$objPHPExcel->getActiveSheet()->setCellValue('B17', $totals['systems_count']);


//Engineer Page
$objPHPExcel->setActiveSheetIndex(1);
$objPHPExcel->getActiveSheet()->setCellValue('B3', $report_date);

$_num = 5;
foreach($engineers_data as $key => $data){				
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$_num, $data['name']);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$_num, $data['rl']);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$_num, $data['ol']);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$_num, percentTotal($data['rl'] + $data['ol'],$totals['total_labor']));
	
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$_num, $data['rt']);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$_num, $data['ot']);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$_num, percentTotal($data['rt'] + $data['ot'],$totals['total_travel']));
	
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$_num, $data['dy']);
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$_num, percentTotal($data['dy'],$totals['total_days']));
	
	$objPHPExcel->getActiveSheet()->setCellValue('M'.$_num, $data['rp']);
	$objPHPExcel->getActiveSheet()->setCellValue('N'.$_num, percentTotal($data['rp'],$totals['total_reports']));
	
	$_num++;
}

//Month Pages
for($i = 2; $i < 14; $i++){
	$objPHPExcel->setActiveSheetIndex($i);
	$objPHPExcel->getActiveSheet()->setCellValue('B3', $report_date);
	
	$month_totals = array('rl' => 0.0, 'ol' => 0.0, 'rt' => 0.0, 'ot' => 0.0, 'dy' => 0.0, 'dt' => 0.0, 'rp' => 0);
	if($final_data[$i -1]){
		foreach($final_data[$i -1]['data'] as $data){
			$month_totals['rl'] += $data['rl'];
			$month_totals['ol'] += $data['ol'];
			$month_totals['rt'] += $data['rt'];
			$month_totals['ot'] += $data['ot'];
			$month_totals['dy'] += $data['dy'];
			$month_totals['dt'] += $data['dt'];
			$month_totals['rp'] += $data['rp'];
		}
		$month_totals['total_labor'] = $month_totals['rl'] + $month_totals['ol'];
		$month_totals['total_travel'] = $month_totals['rt'] + $month_totals['ot'];
		Kint::dump($month_totals);
		
		$objPHPExcel->getActiveSheet()->setCellValue('D3', $month_totals['total_labor']);
		$objPHPExcel->getActiveSheet()->setCellValue('H3', $month_totals['total_travel']);
		$objPHPExcel->getActiveSheet()->setCellValue('K3', $month_totals['dy']);
		$objPHPExcel->getActiveSheet()->setCellValue('N3', $month_totals['rp']);
		
				
		$_num = 5;
		foreach($final_data[$i -1]['data'] as $data){
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$_num, $data['name']);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$_num, $data['rl']);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$_num, $data['ol']);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$_num, percentTotal($data['rl'] + $data['ol'],$month_totals['total_labor']));
			
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$_num, $data['rt']);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$_num, $data['ot']);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$_num, percentTotal($data['rt'] + $data['ot'],$month_totals['total_travel']));
			
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$_num, $data['dy']);
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$_num, percentTotal($data['dy'],$month_totals['dy']));
			
			$objPHPExcel->getActiveSheet()->setCellValue('M'.$_num, $data['rp']);
			$objPHPExcel->getActiveSheet()->setCellValue('N'.$_num, percentTotal($data['rp'],$month_totals['rp']));	
			
			$_num++;
		}
	}
}
$objPHPExcel->setActiveSheetIndex(0);

if($debug){
	die('DEBUG DIE');
}

// Redirect output to a client’s web browser (Excel2007)
ini_set('zlib.output_compression','Off');
header('Pragma: public');
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");                  // Date in the past
//header('Last-Modified: '.gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');     // HTTP/1.1
header('Cache-Control: pre-check=0, post-check=0, max-age=0');    // HTTP/1.1
header ("Pragma: no-cache");
header("Expires: 0");
header('Content-Transfer-Encoding: none');
header('Content-Type: application/vnd.ms-excel;');                 // This should work for IE & Opera
header("Content-type: application/x-msexcel");                    // This should work for the rest
header('Content-Disposition: attachment; filename="Engineer Utilization '.date(savefdt,time()+$_SESSION['tz_offset_sec']).'.xls"');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');

ob_end_flush();

function search($needle, $haystack){
	foreach($haystack as $id => $val)
	{
		if($val['system_id'] == $needle) {
			return $id;			
		}
	}
}

function percentTotal($value,$total){
	$val = round(floatval($value) / floatval($total),4);
	return $val;
}
exit("DONE");
?>