<?php
$debug = false;
if(isset($_GET['debug'])){
	$debug = true;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

if(isset($_SESSION['perms']['perm_face_print']) or isset($_SESSION['roles']['role_engineer']) or isset($_SESSION['roles']['role_contractor'])){	
}else{
	$log->logerr('Permission Error.',1076,true,basename(__FILE__),__LINE__);
}
	
$blank = false;
if(isset($_GET['ver_unique_id'])){
	$system_ver_unique_id=$_GET['ver_unique_id'];
}elseif(isset($_GET['blank'])){
	$blank = true;
}else{
	$log->logerr('Ver Unique ID Missing.',1019,true,basename(__FILE__),__LINE__);	
}


$template_file = $settings->php_base_dir . $settings->report_templates . '/F70-07_Rev_H.docx';
$template_file_incomplete = $settings->php_base_dir . $settings->report_templates . '/F70-07_Rev_H_incomplete.docx';
$temp_dir = $settings->temp . '/';
$output_file_pre = $temp_dir.'F70-07_Rev-H_System-';
$output_file_post = "_".date(savefdt,time()).".pdf";
$output_file_blank = 'F70-07_Rev-H.docx';
$temp_file = $temp_dir.'f70-07_'.md5(uniqid()).'.docx';
$warermark_pic = $settings->php_base_dir . '/resources/images/';
if(!$blank){
	//Get data
	$sql="SELECT 
	sbc.ver_unique_id, sbc.nickname, sb.system_id, e.mfg AS system_mfg, e.`type` AS system_type, e.name AS system_name, sb.system_serial, sbc.date_installed,
	sb.sw_ver, sb.mobile,
	(SELECT tr.trailer_id
	FROM trailers AS tr
	WHERE tr.system_unique_id = sbc.unique_id) AS trailer_id,
	
	sbc.acceptance_date, sbc.tube_covered, sb.pm_freq,
	mc.`type` AS contract_type, sbc.contract_terms, sbc.arrival_date,
	sbc.warranty_start_date, sbc.warranty_end_date, sbc.contract_start_date, sbc.contract_end_date,
	sbc.arrival_date, sbc.property, sbc.pre_paid, sbc.credit_hold, sbc.ver, 
	sbc.mr_lhe_list, sbc.mr_lhe_press, sbc.mr_lhe_contact, sbc.mr_lhe_email, sbc.mr_lhe_phone,
	sb.remote_access, sb.he_monitor, 
	
	(SELECT apriu.name
	FROM systems_assigned_pri AS apri
	LEFT JOIN users AS apriu ON apriu.uid = apri.uid
	WHERE apri.system_ver_unique_id = sbc.ver_unique_id) AS eng_pri,
	
	(SELECT asecu.name
	FROM systems_assigned_sec AS asec
	LEFT JOIN users AS asecu ON asecu.uid = asec.uid
	WHERE asec.system_ver_unique_id = sbc.ver_unique_id) AS eng_sec,
	
	f.name AS f_name, f.facility_id,
	f.address AS f_address, f.city AS f_city, f.state AS f_state, f.zip AS f_zip, f.phone AS f_phone, f.fax AS f_fax,
	f.contact_name AS f_contact, f.contact_title AS f_contact_title, f.contact_phone AS f_contact_phone, f.contact_cell AS f_contact_cell, f.contact_email AS f_contact_email,
	f.timezone AS f_timezone, 
	f.bill_facility, 
	f.bill_name AS fb_name, f.bill_address AS fb_address, f.bill_city AS fb_city, f.bill_state AS fb_state, f.bill_zip AS fb_zip, f.bill_phone AS fb_phone, f.bill_email AS fb_email,
	
	c.name AS c_name, c.customer_id,
	c.address AS c_address, c.city AS c_city, c.state AS c_state, c.zip AS c_zip, c.phone AS c_phone, c.fax AS c_fax,
	c.contact_name AS c_contact, c.timezone AS c_timezone, 
	c.bill_customer,
	c.bill_name AS cb_name, c.bill_address AS cb_address, c.bill_city AS cb_city, c.bill_state AS cb_state, c.bill_zip AS cb_zip, c.bill_phone AS cb_phone, c.bill_email AS cb_email,
	
	fs.completed
	
	FROM systems_base AS sb
	LEFT JOIN systems_base_cont AS sbc ON sb.unique_id = sbc.unique_id
	LEFT JOIN systems_face_sheet AS fs ON fs.system_unique_id = sb.unique_id
	LEFT JOIN systems_types AS e ON e.id = sb.system_type
	LEFT JOIN misc_contracts AS mc ON mc.id = sbc.contract_type
	LEFT JOIN facilities AS f ON f.unique_id = sbc.facility_unique_id
	LEFT JOIN customers AS c ON c.unique_id = f.customer_unique_id
	
	WHERE sbc.ver_unique_id = '".$system_ver_unique_id."';";
	
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$row = $result->fetch_assoc();
	d($row);
	
	$output_file = $output_file_pre . $row['system_id'] . $output_file_post;
	d($output_file);
	
	//Pretty print dates and check for invalid
	if($row['date_installed'] != '' and intval(date('Y',strtotime($row['date_installed']))) > 1980){
		$row['date_installed'] = date(phpdispfd,strtotime($row['date_installed']));
	}else{
		$row['date_installed'] = '';	
	}
	if($row['acceptance_date'] != '' and intval(date('Y',strtotime($row['acceptance_date']))) > 1980){
		$row['acceptance_date'] = date(phpdispfd,strtotime($row['acceptance_date']));
	}else{
		$row['acceptance_date'] = '';	
	}
	if($row['arrival_date'] != '' and intval(date('Y',strtotime($row['arrival_date']))) > 1980){
		$row['arrival_date'] = date(phpdispfd,strtotime($row['arrival_date']));
	}else{
		$row['arrival_date'] = '';	
	}
	if($row['arrival_date'] != '' and intval(date('Y',strtotime($row['arrival_date']))) > 1980){
		$row['arrival_date'] = date(phpdispfd,strtotime($row['arrival_date']));
	}else{
		$row['arrival_date'] = '';	
	}
	if($row['contract_start_date'] != '' and intval(date('Y',strtotime($row['contract_start_date']))) > 1980){
		$row['contract_start_date'] = date(phpdispfd,strtotime($row['contract_start_date']));
	}else{
		$row['contract_start_date'] = '';	
	}
	if($row['contract_end_date'] != '' and intval(date('Y',strtotime($row['contract_end_date']))) > 1980){
		$row['contract_end_date'] = date(phpdispfd,strtotime($row['contract_end_date']));
	}else{
		$row['contract_end_date'] = '';	
	}
	if($row['warranty_start_date'] != '' and intval(date('Y',strtotime($row['warranty_start_date']))) > 1980){
		$row['warranty_start_date'] = date(phpdispfd,strtotime($row['warranty_start_date']));
	}else{
		$row['warranty_start_date'] = '';	
	}
	if($row['warranty_end_date'] != '' and intval(date('Y',strtotime($row['warranty_end_date']))) > 1980){
		$row['warranty_end_date'] = date(phpdispfd,strtotime($row['warranty_end_date']));
	}else{
		$row['warranty_end_date'] = '';	
	}
	if($row['contract_start_date'] != '' and $row['contract_end_date'] != ''){
		$row['c_start_end'] = ($row['contract_start_date'] == ''?'NA':$row['contract_start_date']) .'  '. ($row['contract_end_date'] == ''?'NA':$row['contract_end_date']);
	}
	if($row['warranty_start_date'] != '' and $row['warranty_end_date'] != ''){
		$row['w_start_end'] = ($row['warranty_start_date'] == ''?'NA':$row['warranty_start_date']) .'  '. ($row['warranty_end_date'] == ''?'NA':$row['warranty_end_date']);
	}
	
	
	//Change Y/N to Yes/No
	if(strtolower($row['mobile']) == 'y'){$row['mobile'] = 'Yes';}else{$row['mobile'] = 'No';}
	if(strtolower($row['mr_lhe_list']) == 'y'){$row['mr_lhe_list'] = 'Yes';}else{$row['mr_lhe_list'] = 'No';}
	if(strtolower($row['bill_facility']) == 'y'){$row['bill_facility'] = 'Yes';}else{$row['bill_facility'] = 'No';}
	if(strtolower($row['bill_customer']) == 'y'){$row['bill_customer'] = 'Yes';}else{$row['bill_customer'] = 'No';}
	if(strtolower($row['credit_hold']) == 'y'){$row['credit_hold'] = 'Yes';}else{$row['credit_hold'] = 'No';}
	if(strtolower($row['pre_paid']) == 'y'){$row['pre_paid'] = 'Yes';}else{$row['pre_paid'] = 'No';}
	if(strtolower($row['remote_access']) == 'y'){$row['remote_access'] = 'Yes';}else{$row['remote_access'] = 'No';}
	if(strtolower($row['he_monitor']) == 'y'){$row['he_monitor'] = 'Yes';}else{$row['he_monitor'] = 'No';}
	if(strtolower($row['tube_covered']) == 'y'){$row['tube_covered'] = 'Yes';}else{$row['tube_covered'] = 'No';}
	
	//Check for incomplete data
	$complete = true;
	$incomplete = array();
	
	$cols = array('nickname','system_id','system_mfg','system_type','system_name','system_serial','date_installed','sw_ver','acceptance_date','contract_terms','arrival_date',
	'contract_start_date','contract_end_date','eng_pri','eng_sec','f_name','f_address','f_city','f_state','f_zip','f_phone','f_contact','f_contact_phone',
	'f_timezone','c_name','c_address','c_city','c_state','c_zip','c_phone','c_contact','c_timezone');
	foreach($cols as $col){
		if($row[$col] == ''){
			$complete = false;
			array_push($incomplete,$col);
		}
	}
	
	if(strtolower($row['mobile']) == 'y'){
		if($row['trailer_id'] == ''){
			$complete = false;
			array_push($incomplete,'trailer_id');
		}
	}
	
	if(strtolower($row['mr_lhe_list']) == 'n'){
		$cols = array('mr_lhe_press','mr_lhe_contact','mr_lhe_phone');
		foreach($cols as $col){
			if($row[$col] == ''){
				$complete = false;
				array_push($incomplete,$col);
			}
		}
	}
	
	if(strtolower($row['bill_facility']) == 'n'){
		$cols = array('fb_name','fb_address','fb_city','fb_state','fb_zip','fb_phone');
		foreach($cols as $col){
			if($row[$col] == ''){
				$complete = false;
				array_push($incomplete,$col);
			}
		}
	}
	
	if(strtolower($row['bill_customer']) == 'n'){
		$cols = array('cb_name','cb_address','cb_city','cb_state','cb_zip','cb_phone');
		foreach($cols as $col){
			if($row[$col] == ''){
				$complete = false;
				array_push($incomplete,$col);
			}
		}
	}
	
	d($complete);
	d($incomplete);
	
	if(!$complete){
		$template_file = $template_file_incomplete;	
	}
	d($template_file);
	
	//Concatate address and phone and email
	$row['cb_address'] = $row['cb_address'].' '.$row['cb_city'].', '.$row['cb_state'].'  '.$row['cb_zip'];
	$row['cb_phone_email'] = trim($row['cb_phone'].'  '.$row['cb_email']);
	$row['fb_address'] = $row['fb_address'].' '.$row['fb_city'].', '.$row['fb_state'].'  '.$row['fb_zip'];
	$row['fb_phone_email'] = trim($row['fb_phone'].'  '.$row['fb_email']);
	
	d($row);
}else{
	$output_file = $output_file_blank;	
}
//tax => sql col
$tags = array(
	'nickname' => 'nickname',
	'system_id' => 'system_id',
	'system_mfg' => 'system_mfg',
	'system_type' => 'system_type',
	'system_name' => 'system_name',
	'system_serial' => 'system_serial',
	'date_installed' => 'date_installed',
	'sw_ver' => 'sw_ver',
	'mobile' => 'mobile',
	'trailer_id' => 'trailer_id',
	'accept_date' => 'acceptance_date',
	'remote_acc' => 'remote_access',
	'he_monitor' => 'he_monitor',
	'pm_freq' => 'pm_freq',
	'mr_lhe_list' => 'mr_lhe_list',
	'mr_lhe_press' => 'mr_lhe_press',
	'mr_lhe_contact' => 'mr_lhe_contact',
	'mr_lhe_email' => 'mr_lhe_email',
	'mr_lhe_phone' => 'mr_lhe_phone',
	'eng_pri' => 'eng_pri',
	'eng_sec' => 'eng_sec',
	'f_name' => 'f_name',
	'facility_id' => 'facility_id',
	'f_address' => 'f_address',
	'f_city' => 'f_city',
	'f_state' => 'f_state',
	'f_zip' => 'f_zip',
	'f_phone' => 'f_phone',
	'f_fax' => 'f_fax',
	'f_contact' => 'f_contact',
	'f_contact_title' => 'f_contact_title',
	'f_contact_phone' => 'f_contact_phone',
	'f_contact_cell' => 'f_contact_cell',
	'f_contact_email' => 'f_contact_email',
	'f_timezone' => 'f_timezone',
	'bill_facility' => 'bill_facility',
	'fb_name' => 'fb_name',
	'fb_address' => 'fb_address',
	'fb_phone_email' => 'fb_phone_email',
	'c_name' => 'c_name',
	'customer_id' => 'customer_id',
	'c_address' => 'c_address',
	'c_city' => 'c_city',
	'c_state' => 'c_state',
	'c_zip' => 'c_zip',
	'c_contact' => 'c_contact',
	'c_phone' => 'c_phone',
	'c_fax' => 'c_fax',
	'c_timezone' => 'c_timezone',
	'bill_customer' => 'bill_customer',
	'cb_name' => 'cb_name',
	'cb_address' => 'cb_address',
	'cb_phone_email' => 'cb_phone_email',
	'contract_type' => 'contract_type',
	'arrival_date' => 'arrival_date',
	'c_start_end' => 'c_start_end',
	'w_start_end' => 'w_start_end',
	'credit_hold' => 'credit_hold',
	'pre_paid' => 'pre_paid',
	'tube_covered' => 'tube_covered',
	'contract_terms' => 'contract_terms',
	'ver_unique_id' => 'ver_unique_id'
);
dd($tags);
require_once $_SERVER['DOCUMENT_ROOT'].'/resources/PhpWord/Autoloader.php';
\PhpOffice\PhpWord\Autoloader::register();

$templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($template_file);
if($blank){
	foreach($tags as $tag=>$col){
		$templateProcessor->setValue($tag, '');
	}

	$templateProcessor->saveAs($temp_file);

	header('Content-Description: File Transfer');
	header('Content-Type: application/octet-stream');
	header('Content-Disposition: attachment; filename='.basename($output_file));
	header('Content-Transfer-Encoding: binary');
	header('Expires: 0');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	header('Pragma: public');
	header('Content-Length: ' . filesize($temp_file));
	flush();
	readfile($temp_file);
}else{
	foreach($tags as $tag=>$col){
		$templateProcessor->setValue($tag, $row[$col]);
	}

	$templateProcessor->saveAs($temp_file);
	//shell_exec("unoconv -f pdf -o ".$output_file." ".$temp_file);
	shell_exec("unoconv -f pdf -o ".$output_file." -e RestrictPermissions=true -e PermissionPassword=OxfordInstruments -e Printing=2 -e Changes=0 ".$temp_file);
	
	header('Content-Description: File Transfer');
	header('Content-Type: application/octet-stream');
	header('Content-Disposition: attachment; filename='.basename($output_file));
	header('Content-Transfer-Encoding: binary');
	header('Expires: 0');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	header('Pragma: public');
	header('Content-Length: ' . filesize($output_file));
	flush();
	readfile($output_file);
}

//unlink($temp_file); // deletes the temporary file
exit;



function trim_all( $str , $what = NULL , $with = ' ' ){
	if( $what === NULL ){
		//  Character      Decimal      Use
		//  "\0"            0           Null Character
		//  "\t"            9           Tab
		//  "\n"           10           New line
		//  "\x0B"         11           Vertical Tab
		//  "\r"           13           New Line in Mac
		//  " "            32           Space
	   
		$what   = "\\x00-\\x1F";    //all white-spaces and control chars
	}   
	return trim( preg_replace( "/[".$what."]+/" , $with , $str ) , $what );
}


?>