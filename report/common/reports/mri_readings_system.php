<?php
//Magnet Pressures Min/Max for 1 psi and 4psi
$press_1_min = 0.8;
$press_1_max = 1.2;
$press_4_min = 3.8;
$press_4_max = 4.2;

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/php_utils.php');
$php_utils = new phpUtils();

if(isset($_GET['unique_id'])){
	$unique_id = $_GET['unique_id'];
}else{
	$log->logerr('mriReadingSystem',1016,true,basename(__FILE__),__LINE__);
}

//Get

//Get readings for system
$readings = array();
$sql="SELECT sb.system_id, m.he, m.vp, m.`date`, m.`hours`, m.notes, sbc.nickname, sbc.mr_lhe_press, f.timezone
FROM systems_mri_readings AS m
LEFT JOIN systems_base AS sb ON sb.unique_id = m.system_unique_id
LEFT JOIN systems_base_cont AS sbc ON sb.unique_id = sbc.unique_id AND sbc.property = 'C'
LEFT JOIN facilities AS f ON f.unique_id = sbc.facility_unique_id
WHERE m.system_unique_id = '$unique_id' 
AND DATEDIFF(CURDATE(), m.`date`) < 365
ORDER BY m.`date` DESC;";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

while($row = $result->fetch_assoc()){
	array_push($readings,$row);
}

if(empty($readings)){
	$no_report = true;
}else{
	$no_report = false;
}

//echo "<pre>", EOL;
//print_r($readings);
//echo "</pre>", EOL;
//die();

// Include PHPExcel
require_once ($_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/Classes/PHPExcel.php');

// Include PHPExcel_IOFactory
require_once ($_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/Classes/PHPExcel/IOFactory.php');

// Check if template exists
//echo "Check if Excel2007 template file exists<br />";
$template_file = $_SERVER['DOCUMENT_ROOT'].'/resources/report_templates/mri_history.xls';
if (!file_exists($template_file)) {
	$log->logerr($template_file,1045);	
	exit("Error the template is missing." . EOL);
}

// Create new PHPExcel object
//echo "Start PHPExcell", EOL;
$objPHPExcel = new PHPExcel();

//echo "Load from Excel2007 template file", EOL;
$objPHPExcel = PHPExcel_IOFactory::load($template_file);

$sharedStyle1 = new PHPExcel_Style();

$sharedStyle1->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('argb' => 'FFFFCCCC')
							)
));

$_num = 5;

//Select ct sheet
$objPHPExcel->setActiveSheetIndex(0);
if(!$no_report){
	//update header
	$objPHPExcel->getActiveSheet()->setCellValue('D2', $readings[0]['system_id']);
	$objPHPExcel->getActiveSheet()->setCellValue('D3', $readings[0]['nickname']);
	
	//add rows
	
	$last_key = end(array_keys($readings));
	foreach($readings as $key => $reading){
		
		if($key != 0){
			$objPHPExcel->getActiveSheet()->setCellValue('E' . ($_num - 1), '=B' . ($_num - 1) . '-B' . $_num);
		}
		
		$reading_date_offset = $php_utils->tz_offset_sec($reading['timezone']);
		
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$_num, date(phpdispfdt,strtotime($reading['date'])+$reading_date_offset));
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$_num, $reading['he']);
		if(floatval($reading['he']) <= 50.00){
			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1, "B".$_num);
		}
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$_num, $reading['vp']);
		switch($readings['mr_lhe_press']){
			case 0;
				if(floatval($reading['vp']) <= 0 || floatval($reading['vp']) >= 100){
					$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1, "C".$_num);
				}
				break;
			case 1:
				if(floatval($reading['vp']) <= $press_1_min || floatval($reading['vp']) >= $press_1_max){
					$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1, "C".$_num);
				}
				break;
			case 4:
				if(floatval($reading['vp']) <= $press_4_min || floatval($reading['vp']) >= $press_4_max){
					$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1, "C".$_num);
				}
				break;
		}
		
		
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$_num, $reading['hours']);
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$_num, $reading['notes']);
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$_num, $reading['timezone']);
		$_num++;
	}
}else{
	$objPHPExcel->getActiveSheet()->setCellValue('A5', 'No readings for this system');
	$readings[0]['system_id'] = '0000';
}

$objPHPExcel->getActiveSheet()->setCellValue('B3', date(phpdispfdt,time()+$_SESSION['tz_offset_sec']));

//write file
//echo "Write to Excel2007 format", EOL;
//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
//echo "<a href=\"http://secure.jrdhome.com/report/common/reports/".str_replace('.php', '.xlsx', pathinfo(__FILE__, PATHINFO_BASENAME))."\">".str_replace('.php', '.xlsx', pathinfo(__FILE__, PATHINFO_BASENAME))."</a>", EOL;

// Redirect output to a client’s web browser (Excel2007)
ini_set('zlib.output_compression','Off');
header('Pragma: public');
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");                  // Date in the past
//header('Last-Modified: '.gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');     // HTTP/1.1
header('Cache-Control: pre-check=0, post-check=0, max-age=0');    // HTTP/1.1
header ("Pragma: no-cache");
header("Expires: 0");
header('Content-Transfer-Encoding: none');
header('Content-Type: application/vnd.ms-excel;');                 // This should work for IE & Opera
header("Content-type: application/x-msexcel");                    // This should work for the rest
header('Content-Disposition: attachment; filename="MRI History System '. $readings[0]['system_id'].' '.date(savefdt,time()+$_SESSION['tz_offset_sec']).'.xls"');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');

ob_end_flush();
exit("DONE");
?>