<?php
$debug = false;

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/php_utils.php');
$php_utils = new phpUtils();

if(isset($_GET['lastweek'])){
	$now_week = 1;
	$last_week = 2;	
}else{
	$now_week = 0;
	$last_week = 1;	
}

//Get readings for this week
$readings = array();
$sql="SELECT sb.system_id, m.he, m.vp, m.`date`, m.`hours`, m.notes, sbc.nickname, f.timezone, sbc.mr_lhe_press
FROM systems_mri_readings AS m
LEFT JOIN systems_base AS sb ON sb.unique_id = m.system_unique_id
LEFT JOIN systems_base_cont AS sbc ON sb.unique_id = sbc.unique_id AND sbc.property = 'C'
LEFT JOIN facilities AS f ON f.unique_id = sbc.facility_unique_id
WHERE WEEKOFYEAR(m.`date`)= WEEKOFYEAR(NOW()) - $now_week
AND YEAR(m.`date`) = YEAR(NOW())
ORDER BY sb.system_id ASC;";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

while($row = $result->fetch_assoc()){
	array_push($readings,$row);
}
d($readings);

//Get readings for last week
$readings_past = array();
$sql="SELECT sb.system_id, m.he, m.vp, m.`date`, m.`hours`, m.notes, sbc.nickname
FROM systems_mri_readings AS m
LEFT JOIN systems_base AS sb ON sb.unique_id = m.system_unique_id
LEFT JOIN systems_base_cont AS sbc ON sb.unique_id = sbc.unique_id
WHERE WEEKOFYEAR(m.`date`)= WEEKOFYEAR(NOW()) - $last_week
AND YEAR(m.`date`) = YEAR(NOW())
ORDER BY sb.system_id ASC;";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

while($row = $result->fetch_assoc()){
	array_push($readings_past,$row);
}
d($readings_past);

if(empty($readings)){
	$no_report = true;
}else{
	$no_report = false;
}

if(empty($readings_past)){
	$no_report_past = true;
}else{
	$no_report_past = false;
}

dd($no_report);

//echo $no_report;
//echo $no_report_past;
//echo "<pre>", EOL;
//print_r($readings);
//echo "</pre>", EOL;
//die();

// Include PHPExcel
require_once ($_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/Classes/PHPExcel.php');

// Include PHPExcel_IOFactory
require_once ($_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/Classes/PHPExcel/IOFactory.php');

// Check if template exists
//echo "Check if Excel2007 template file exists<br />";
$template_file = $_SERVER['DOCUMENT_ROOT'].'/resources/report_templates/mri_readings_report_weekly.xls';
if (!file_exists($template_file)) {
	$log->logerr($template_file,1045);
	exit("Error the template is missing." . EOL);
}

// Create new PHPExcel object
//echo "Start PHPExcell", EOL;
$objPHPExcel = new PHPExcel();

//echo "Load from Excel2007 template file", EOL;
$objPHPExcel = PHPExcel_IOFactory::load($template_file);

$sharedStyle1 = new PHPExcel_Style();

$sharedStyle1->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('argb' => 'FFFFCCCC')
							)
));


//Select ct sheet
$objPHPExcel->setActiveSheetIndex(0);



//update header
$objPHPExcel->getActiveSheet()->setCellValue('D3', date("W",time()));
$objPHPExcel->getActiveSheet()->setCellValue('B3', date(phpdispfdt,time()+$_SESSION['tz_offset_sec']));

//add rows
$_num = 5;
if(!$no_report){
	$last_key = end(array_keys($readings));
	foreach($readings as $key => $reading){
		
		if(!$no_report_past){
			$last_he_key = search($reading['system_id'], $readings_past);
			$last_he = floatval($readings_past[$last_he_key]['he']);		
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$_num, floatval($reading['he']) - $last_he);
		}
		$reading_date_offset = $php_utils->tz_offset_sec($reading['timezone']);
		
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$_num, $reading['system_id']);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$_num, $reading['nickname']);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$_num, date(phpdispfdt,strtotime($reading['date'])+$reading_date_offset));
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$_num, $reading['he']);
		if(floatval($reading['he']) <= floatval($settings->mr_lhe_min)){
			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1, "D".$_num);
		}
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$_num, $reading['vp']);
		switch($reading['mr_lhe_press']){
			case '1':
					if(floatval($reading['vp']) <= floatval($settings->mr_vp_1_min) || floatval($reading['vp']) >= floatval($settings->mr_vp_1_max)){
						$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1, "E".$_num);	
					}
					break;
			case '4':
					if(floatval($reading['vp']) <= floatval($settings->mr_vp_4_min) || floatval($reading['vp']) >= floatval($settings->mr_vp_4_max)){
						$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1, "E".$_num);	
					}
					break;			
		}

		
//		if(floatval($reading['vp']) <= 3.8 || floatval($reading['vp']) >= 4.20){
//			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1, "E".$_num);
//		}
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$_num, $reading['hours']);
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$_num, $reading['notes']);
		$objPHPExcel->getActiveSheet()->setCellValue('I'.$_num, $reading['timezone']);
		$_num++;
	}
}else{
	$objPHPExcel->getActiveSheet()->setCellValue('B5', 'No report for this week yet');	
}

//write file
//echo "Write to Excel2007 format", EOL;
//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
//echo "<a href=\"http://secure.jrdhome.com/report/common/reports/".str_replace('.php', '.xlsx', pathinfo(__FILE__, PATHINFO_BASENAME))."\">".str_replace('.php', '.xlsx', pathinfo(__FILE__, PATHINFO_BASENAME))."</a>", EOL;

// Redirect output to a client’s web browser (Excel2007)
ini_set('zlib.output_compression','Off');
header('Pragma: public');
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");                  // Date in the past
//header('Last-Modified: '.gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');     // HTTP/1.1
header('Cache-Control: pre-check=0, post-check=0, max-age=0');    // HTTP/1.1
header ("Pragma: no-cache");
header("Expires: 0");
header('Content-Transfer-Encoding: none');
header('Content-Type: application/vnd.ms-excel;');                 // This should work for IE & Opera
header("Content-type: application/x-msexcel");                    // This should work for the rest
header('Content-Disposition: attachment; filename="MRI Readings Weekly Report '.date(savefdt,time()+$_SESSION['tz_offset_sec']).'.xls"');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');

ob_end_flush();

function search($needle, $haystack){
	foreach($haystack as $id => $val)
	{
		if($val['system_id'] == $needle) {
			return $id;			
		}
	}
}
exit("DONE");
?>