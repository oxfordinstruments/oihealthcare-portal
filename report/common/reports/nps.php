<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

$time_now = time() + $_SESSION['tz_offset_sec'];

//Update NPS Data
update_data($mysqli);

//Get requests
$nps_data = array();
$sql="SELECT * FROM nps_data ORDER BY id DESC;";
if(!$resultNPSData = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
while($row = $resultNPSData->fetch_assoc()){
	array_push($nps_data,$row);
}

$nps_settings = new SimpleXMLElement($_SERVER['DOCUMENT_ROOT'].'/nps_settings.xml', null, true);

// Include PHPExcel
require_once ($_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/Classes/PHPExcel.php');

// Include PHPExcel_IOFactory
require_once ($_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/Classes/PHPExcel/IOFactory.php');

// Check if template exists
//echo "Check if Excel2007 template file exists<br />";
$template_file = $_SERVER['DOCUMENT_ROOT'].'/resources/report_templates/nps.xls';
if (!file_exists($template_file)) {
	$log->logerr($template_file,1045);
	exit("Error the template is missing." . EOL);
}

// Create new PHPExcel object
//echo "Start PHPExcell", EOL;
$objPHPExcel = new PHPExcel();

//echo "Load from Excel2007 template file", EOL;
$objPHPExcel = PHPExcel_IOFactory::load($template_file);

$style_algnLeft_FillBG = new PHPExcel_Style();
$style_algnLeft_FillBG->applyFromArray(
	array('fill' => array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color' => array('argb' => 'FFEDEDEE')),
	'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT))
);

$style_FillBG_Red = new PHPExcel_Style();
$style_FillBG_Red->applyFromArray(
	array('fill' => array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color' => array('argb' => 'FFFFC7CE')),
	'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT),
	'font'  => array('color' => array('rgb' => '800000')))
);

$style_Head_Warn = new PHPExcel_Style();
$style_Head_Warn->applyFromArray(
	array('fill' => array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color' => array('argb' => 'FFEDEDEE')),
	'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT),
	'font'  => array('bold' => true, 'size' => 15, 'color' => array('rgb' => 'FF0000')))
);

//Select ct sheet
$objPHPExcel->setActiveSheetIndex(0);

//get last row of table nps_data
$last_key = end(array_keys($nps_data));

//For each row in table nps_data make an excell sheet
$baseSheet = $objPHPExcel->getActiveSheet();
$baseSheetQtr = "";
$baseSheetYr = "";
foreach($nps_data as $key => $data){
	if($key != 0){
		$newSheet = clone $baseSheet;
		$newSheet->setTitle("Q".$data['fiscal_quarter']."-".$data['fiscal_year']);
		$objPHPExcel->addSheet($newSheet,$key);
		$objPHPExcel->setActiveSheetIndex($key);
		$objPHPExcel->getActiveSheet()->setCellValue('I2', date(phpdispfdt,$time_now));
		$objPHPExcel->getActiveSheet()->setCellValue('f2', date(phpdispfd,strtotime($data['survey_send_date'])));
		$objPHPExcel->getActiveSheet()->setCellValue('f3', $data['fiscal_year']);
		$baseSheetYr = $data['fiscal_year'];
		$objPHPExcel->getActiveSheet()->setCellValue('f4', $data['fiscal_quarter']);
		$baseSheetQtr = $data['fiscal_quarter'];
		$objPHPExcel->getActiveSheet()->setSharedStyle($style_algnLeft_FillBG, "F2:F4");
	}else{
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setCellValue('I2', date(phpdispfdt,$time_now));
		$baseSheet->setTitle("Q".$data['fiscal_quarter']."-".$data['fiscal_year']);	
		$objPHPExcel->getActiveSheet()->setCellValue('f2', date(phpdispfd,strtotime($data['survey_send_date'])));
		$objPHPExcel->getActiveSheet()->setCellValue('f3', $data['fiscal_year']);
		$objPHPExcel->getActiveSheet()->setCellValue('f4', $data['fiscal_quarter']);
		$objPHPExcel->getActiveSheet()->setSharedStyle($style_algnLeft_FillBG, "F2:F4");
	}
}

$warn = true;

if(date("Y",time()) > $baseSheetYr){
	$warn = false;	
}else{
	$nps_q1 = strtotime($nps_settings->q1_end);
	$nps_q2 = strtotime($nps_settings->q2_end);
	$nps_q3 = strtotime($nps_settings->q3_end);
	$nps_q4 = strtotime($nps_settings->q4_end);
	$now = strtotime(date("m/d",time()));	
	$qnow = 0;
	
	if($now <= $nps_q1){
		$qnow = 1;	
	}elseif($now > $nps_q1 && $now <= $nps_q2){
		$qnow = 2;	
	}elseif($now > $nps_q2 && $now <= $nps_q3){
		$qnow = 3;	
	}elseif($now > $nps_q3 && $now <= $nps_q4){
		$qnow = 4;
	}
	if($qnow > intval($baseSheetQtr)){
		$warn = false;	
	}
}

foreach($nps_data as $key => $data){
	$total_resp = $data['promoters'] + $data['passives'] + $data['demoters'];
	
	if($data['promoters'] != 0){
		$promoters = number_format(round(($data['promoters'] / $total_resp)*100, 2),2);
	}else{ 
		$promoters = 0; 
	}
	
	if($data['passives'] != 0){
		$passives = number_format(round(($data['passives'] / $total_resp)*100, 2),2);
	}else{ 
		$passives = 0; 
	}
	
	if($data['demoters'] != 0){
		$demoters = number_format(round(($data['demoters'] / $total_resp)*100, 2),2);
	}else{ 
		$demoters = 0; 
	}
	$objPHPExcel->setActiveSheetIndex($key);	
	$objPHPExcel->getActiveSheet()->setCellValue('c6', number_format($data['nps_score'],2)."%");
	$objPHPExcel->getActiveSheet()->setCellValue('e6', $promoters."%");
	$objPHPExcel->getActiveSheet()->setCellValue('g6', $passives."%");
	$objPHPExcel->getActiveSheet()->setCellValue('i6', $demoters."%");
	$objPHPExcel->getActiveSheet()->setCellValue('c8', $data['surveys_sent']);
	$objPHPExcel->getActiveSheet()->setCellValue('e8', $data['surveys_received']);
	$objPHPExcel->getActiveSheet()->setCellValue('g8', $data['response_percent']."%");
	
	$row = 12;
	$sql="SELECT sb.system_id, sbc.nickname, nr.score, nr.`comment`, nr.contact_info, nc.email
	FROM nps_responses AS nr
	LEFT JOIN nps_codes AS nc ON nc.unique_id = nr.unique_id
	LEFT JOIN systems_base AS sb ON sb.unique_id = nc.system_unique_id
	LEFT JOIN systems_base_cont AS sbc ON sb.unique_id = sbc.unique_id
	WHERE nr.quarter_unique_id = '".$data['unique_id']."';";
	
	if(!$resultNPSResp = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	while($rowResp = $resultNPSResp->fetch_assoc()){
		$objPHPExcel->getActiveSheet()->setCellValue('b'.$row, $rowResp['system_id']);
		$objPHPExcel->getActiveSheet()->setCellValue('c'.$row, $rowResp['nickname']);
		$objPHPExcel->getActiveSheet()->setCellValue('f'.$row, $rowResp['score']);
		$objPHPExcel->getActiveSheet()->setCellValue('g'.$row, $rowResp['comment']);
		$objPHPExcel->getActiveSheet()->setCellValue('m'.$row, $rowResp['contact_info']);
		$objPHPExcel->getActiveSheet()->setCellValue('n'.$row, $rowResp['email']);
		if($rowResp['score'] < 7){
			$objPHPExcel->getActiveSheet()->setSharedStyle($style_FillBG_Red, "B".$row.":N".$row);
		}
		$row++;
	}
	
	$objPHPExcel->getActiveSheet()->setCellValue('a1', "");
	
}

$objPHPExcel->setActiveSheetIndex(0);

if($warn){
	$objPHPExcel->getActiveSheet()->setCellValue('j1', "This fiscal quarter is not over yet. The data may not be complete.");
	$objPHPExcel->getActiveSheet()->setSharedStyle($style_Head_Warn, "j1");
	$objPHPExcel->getActiveSheet()->setCellValue('a1', "");
}

//$objPHPExcel->getActiveSheet()->setCellValue('I2', date("m/d/Y H:i T",time()));

// Redirect output to a client’s web browser (Excel2007)
ini_set('zlib.output_compression','Off');
header('Pragma: public');
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");                  // Date in the past
//header('Last-Modified: '.gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');     // HTTP/1.1
header('Cache-Control: pre-check=0, post-check=0, max-age=0');    // HTTP/1.1
header ("Pragma: no-cache");
header("Expires: 0");
header('Content-Transfer-Encoding: none');
header('Content-Type: application/vnd.ms-excel;');                 // This should work for IE & Opera
header("Content-type: application/x-msexcel");                    // This should work for the rest
header('Content-Disposition: attachment; filename="NPS Report '.date(savefdt,time()).'.xls"');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');

ob_end_flush();

exit("DONE");

function update_data(&$mysqli){
	global $log;
	$sql="SELECT * FROM nps_data ORDER BY id DESC LIMIT 2;";
	if(!$resultNPSData = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	while($rowNPSData = $resultNPSData->fetch_assoc()){
		//echo "Updating nps_data id: ".$rowNPSData['unique_id'],EOL;
		$quarter_unique_id = $rowNPSData['unique_id'];
		
		$sql = "SELECT score, count(*) AS count FROM nps_responses WHERE quarter_unique_id = '$quarter_unique_id' GROUP BY score ORDER BY COUNT ASC;";
		if(!$resultNPSRespScores = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
		if($resultNPSRespScores->num_rows > 0){
			$scores = array(0,0,0,0,0,0,0,0,0,0,0);
			//VALUES('". implode("', '", $values) . "');";
			while($rowNPSRespScores = $resultNPSRespScores->fetch_assoc()){
				$scores[intval($rowNPSRespScores['score'])] = intval($rowNPSRespScores['count']);
			}
			//echo "Scores: ";print_r($scores);echo EOL;
			
			$nps_promoters = $scores[10] + $scores[9];
			$nps_passives = $scores[8] + $scores[7];
			$nps_demoters = array_sum(array_slice($scores,0,7));
			
			//echo "Promoters: ".round(($nps_promoters/array_sum($scores))*100,2)."%  (".$nps_promoters.")",EOL;
			//echo "Passives: ". round(($nps_passives/array_sum($scores))*100,2)."%  (".$nps_passives.")",EOL;
			//echo "Demoters: ".round(($nps_demoters/array_sum($scores))*100,2)."%  (".$nps_demoters.")",EOL;
			
			$nps_score = round((($nps_promoters - $nps_demoters)/array_sum($scores))*100,2);
			//echo "NPS Score: ". $nps_score."%",EOL;
			
			$surveys_sent = $rowNPSData['surveys_sent'];
			//echo "Surveys Sent: ". $surveys_sent,EOL;
			$sql="SELECT COUNT(*) AS count FROM nps_responses WHERE quarter_unique_id = '$quarter_unique_id';";
			if(!$resultNPSResp = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
				$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
			}
			$rowNPSResp = $resultNPSResp->fetch_assoc();
			$survey_response =  $rowNPSResp['count'];
			//echo "Survey Responses: ". $survey_response,EOL;
			
			$survey_response_percent = round(($survey_response / $surveys_sent)*100,2);
			//echo "Survey Response Percent: ". $survey_response_percent."%",EOL;
			
			$sql = "UPDATE nps_data SET 
				`surveys_received`='$survey_response',
				`response_percent`='$survey_response_percent',
				`nps_score`='$nps_score',
				`promoters`='$nps_promoters',
				`passives`='$nps_passives',
				`demoters`='$nps_demoters',
				`nps_0`='".$scores[0]."',
				`nps_1`='".$scores[1]."',
				`nps_2`='".$scores[2]."',
				`nps_3`='".$scores[3]."',
				`nps_4`='".$scores[4]."',
				`nps_5`='".$scores[5]."',
				`nps_6`='".$scores[6]."',
				`nps_7`='".$scores[7]."',
				`nps_8`='".$scores[8]."',
				`nps_9`='".$scores[9]."',
				`nps_10`='".$scores[10]."'
				WHERE unique_id = '$quarter_unique_id';";
			if(!$result = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
				$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
			}	
			//echo "nps_data table updated",EOL,EOL;
		}			
	}
	return true;		
}
?>