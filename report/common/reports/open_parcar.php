<?php

$debug = false;

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

//Get open cars
$open_cars = array();
$sql="SELECT c.id, c.title, c.department, c.location, c.created_date, u.name AS created_name, c.unique_id
FROM iso_car AS c
LEFT JOIN users AS u ON u.uid = c.created_uid
WHERE c.closed = 'n' AND c.deleted = 'n'
ORDER BY c.created_date ASC;";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

while($row = $result->fetch_assoc()){
	$overdue = false;
	$dtc = new DateTime($row['created_date']);
	$dtn = new DateTime();
	$dtc->add(new DateInterval('P'.$settings->iso_report_open_parcar_due.'D'));
	if($dtn > $dtc){
		$overdue = true;
	}
	$open_cars[$row['id']] = array( 'title'=>$row['title'], 
									'department'=>$row['department'], 
									'location'=>$row['location'], 
									'created_date'=>$row['created_date'], 
									'created_name'=>$row['created_name'], 
									'unique_id'=>$row['unique_id'],
									'overdue'=>$overdue,
									'due'=>$dtc->format(storef));
}
d($open_cars);

//Get open pars
$open_pars = array();
$sql="SELECT p.id, p.title, p.department, p.location, p.created_date, u.name AS created_name, p.unique_id
FROM iso_par AS p
LEFT JOIN users AS u ON u.uid = p.created_uid
WHERE p.closed = 'n' AND p.deleted = 'n'
ORDER BY p.created_date ASC;";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

while($row = $result->fetch_assoc()){
	$overdue = false;
	$dtc = new DateTime($row['created_date']);
	$dtn = new DateTime();
	$dtc->add(new DateInterval('P'.$settings->iso_report_open_parcar_due.'D'));
	if($dtn > $dtc){
		$overdue = true;
	}
	$open_pars[$row['id']] = array( 'title'=>$row['title'], 
									'department'=>$row['department'], 
									'location'=>$row['location'], 
									'created_date'=>$row['created_date'], 
									'created_name'=>$row['created_name'], 
									'unique_id'=>$row['unique_id'],
									'overdue'=>$overdue,
									'due'=>$dtc->format(storef));	
}
dd($open_pars);


// Include PHPExcel
require_once ($_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/Classes/PHPExcel.php');

// Include PHPExcel_IOFactory
require_once ($_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/Classes/PHPExcel/IOFactory.php');

// Check if template exists
//echo "Check if Excel2007 template file exists<br />";
$template_file = $_SERVER['DOCUMENT_ROOT'].'/resources/report_templates/open_parcar.xls';
if (!file_exists($template_file)) {
	$log->logerr('open_requests.php',1045);	
	exit("Error the template is missing." . EOL);
}

// Create new PHPExcel object
//echo "Start PHPExcell", EOL;
$objPHPExcel = new PHPExcel();

//echo "Load from Excel2007 template file", EOL;
$objPHPExcel = PHPExcel_IOFactory::load($template_file);

$style_FillBG_Green = new PHPExcel_Style();
$style_FillBG_Green->applyFromArray(
	array('fill' => array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color' => array('argb' => 'FFD7E4BC')),
	'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT))
);

$style_FillBG_Red = new PHPExcel_Style();
$style_FillBG_Red->applyFromArray(
	array('fill' => array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color' => array('argb' => 'FFFFC7CE')),
	'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT),
	'font'  => array('color' => array('rgb' => '800000')))
);

//Select car sheet
$objPHPExcel->setActiveSheetIndex(0);

$report_date = date(phpdispfdt,time()+$_SESSION['tz_offset_sec']);

//Set report date
$objPHPExcel->getActiveSheet()->setCellValue('b3', $report_date);

//add rows
$_num = 5;
$last_key = end(array_keys($open_cars));
foreach($open_cars as $key => $value){

	$created_date = date(phpdispfd,strtotime($value['created_date']));
	$due_date = date(phpdispfd,strtotime($value['due']));
	
	
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$_num, $key);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$_num, $value['title']);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$_num, $value['department']);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$_num, $value['location']);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$_num, $created_date);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$_num, $value['created_name']);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$_num, $due_date);

	if($value['overdue'] == true){
		$objPHPExcel->getActiveSheet()->setSharedStyle($style_FillBG_Red, "A".$_num.":G".$_num);
	}

	$_num++;
}

//Select par sheet
$objPHPExcel->setActiveSheetIndex(1);

$report_date = date(phpdispfdt,time()+$_SESSION['tz_offset_sec']);

//Set report date
$objPHPExcel->getActiveSheet()->setCellValue('b3', $report_date);

//add rows
$_num = 5;
$last_key = end(array_keys($open_pars));
foreach($open_pars as $key => $value){

	$created_date = date(phpdispfd,strtotime($value['created_date']));
	$due_date = date(phpdispfd,strtotime($value['due']));
	
	
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$_num, $key);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$_num, $value['title']);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$_num, $value['department']);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$_num, $value['location']);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$_num, $created_date);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$_num, $value['created_name']);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$_num, $due_date);

	if($value['overdue'] == true){
		$objPHPExcel->getActiveSheet()->setSharedStyle($style_FillBG_Red, "A".$_num.":G".$_num);
	}

	$_num++;
}

$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
ini_set('zlib.output_compression','Off');
header('Pragma: public');
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");                  // Date in the past
//header('Last-Modified: '.gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');     // HTTP/1.1
header('Cache-Control: pre-check=0, post-check=0, max-age=0');    // HTTP/1.1
header ("Pragma: no-cache");
header("Expires: 0");
header('Content-Transfer-Encoding: none');
header('Content-Type: application/vnd.ms-excel;');                 // This should work for IE & Opera
header("Content-type: application/x-msexcel");                    // This should work for the rest
header('Content-Disposition: attachment; filename="Open PAR CAR '.date(savefdt,time()+$_SESSION['tz_offset_sec']).'.xls"');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');

ob_end_flush();
exit("DONE");
?>