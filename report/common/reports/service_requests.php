<?php
//Update Completed 12/09/2014
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

//Get requests
$requests = array();
$sql="SELECT r.*, u1.name AS engineer_name, u2.name AS requestor, sys.`status` as equipment_status, u3.name AS editor, u4.name AS deletor
FROM systems_requests AS r
LEFT JOIN users AS u1 ON r.engineer = u1.uid
LEFT JOIN users AS u2 ON r.sender = u2.uid
LEFT JOIN users AS u3 ON r.edited_by = u3.uid
LEFT JOIN users AS u4 ON r.deleted_by = u4.uid
LEFT JOIN systems_status AS sys ON r.system_status = sys.id
WHERE r.request_date BETWEEN CURDATE() - INTERVAL 180 DAY AND CURDATE()
ORDER BY r.request_num DESC;";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

while($row = $result->fetch_assoc()){
	array_push($requests,$row);
}


if(empty($requests)){
	$no_report = true;
}else{
	$no_report = false;
}

// Include PHPExcel
require_once ($_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/Classes/PHPExcel.php');

// Include PHPExcel_IOFactory
require_once ($_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/Classes/PHPExcel/IOFactory.php');

// Check if template exists
//echo "Check if Excel2007 template file exists<br />";
$template_file = $_SERVER['DOCUMENT_ROOT'].'/resources/report_templates/service_requests.xls';
if (!file_exists($template_file)) {
	$log->logerr($template_file,1045);
	exit("Error the template is missing." . EOL);
}

// Create new PHPExcel object
//echo "Start PHPExcell", EOL;
$objPHPExcel = new PHPExcel();

//echo "Load from Excel2007 template file", EOL;
$objPHPExcel = PHPExcel_IOFactory::load($template_file);

$sharedStyle1 = new PHPExcel_Style();

$sharedStyle1->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('argb' => 'FFFFCCCC')
							),
		 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
		 
));

$sharedStyle1a = new PHPExcel_Style();

$sharedStyle1a->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('argb' => 'FFFFCCCC')
							),
		 'alignment' => array(
		 						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		 					  	'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
							)
		 
));

$sharedStyle2 = new PHPExcel_Style();

$sharedStyle2->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('argb' => 'FFD7E4BC')
							),
		 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
		 
));

$sharedStyle2a = new PHPExcel_Style();

$sharedStyle2a->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('argb' => 'FFD7E4BC')
							),
		 'alignment' => array(
		 						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		 					  	'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
							)
		 
));

$sharedStyle3 = new PHPExcel_Style();

$sharedStyle3->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('argb' => 'FFB8CCE4')
							),
		 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
		 
));

$sharedStyle3a = new PHPExcel_Style();

$sharedStyle3a->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('argb' => 'FFB8CCE4')
							),
		 'alignment' => array(
		 						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		 					  	'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
							)
		 
));

//Select ct sheet
$objPHPExcel->setActiveSheetIndex(0);



//Set report date
$objPHPExcel->getActiveSheet()->setCellValue('b3', date(phpdispfdt,time()+$_SESSION['tz_offset_sec']));

$objPHPExcel->getActiveSheet()->setCellValue('H2', "> 30 Mins");
$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1a, "H2");
$objPHPExcel->getActiveSheet()->setCellValue('I2', "Edited");
$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle2a, "I2");
$objPHPExcel->getActiveSheet()->setCellValue('J2', "Deleted");
$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle3a, "J2");

//add rows
$_num = 5;
if(!$no_report){
	$last_key = end(array_keys($requests));
	foreach($requests as $key => $request){
				
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$_num, $request['request_num']);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$_num, $request['status']);
		if(strtolower($request['report_started']) == "y"){
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$_num, 'Yes');
		}
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$_num, $request['engineer_name']);
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$_num, $request['requestor']);
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$_num, $request['system_id']);
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$_num, $request['system_nickname']);
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$_num, $request['request_date']);
		$objPHPExcel->getActiveSheet()->setCellValue('I'.$_num, $request['onsite_date']);
		$objPHPExcel->getActiveSheet()->setCellValue('J'.$_num, $request['initial_call_date']);
		$objPHPExcel->getActiveSheet()->setCellValue('K'.$_num, $request['response_date']);
		$objPHPExcel->getActiveSheet()->setCellValue('L'.$_num, $request['response_minutes']);
		$objPHPExcel->getActiveSheet()->setCellValue('M'.$_num, $request['problem_reported']);
		$objPHPExcel->getActiveSheet()->setCellValue('N'.$_num, $request['customer_actions']);
		$objPHPExcel->getActiveSheet()->setCellValue('O'.$_num, $request['equipment_status_name']);
		$objPHPExcel->getActiveSheet()->setCellValue('P'.$_num, $request['po_num']);
		if(strtolower($request['pm']) == "y"){
			$objPHPExcel->getActiveSheet()->setCellValue('Q'.$_num, 'Yes');
		}
		if(strtolower($request['under_30min']) == "n"){
			$objPHPExcel->getActiveSheet()->setCellValue('R'.$_num, 'No');
		}
		$objPHPExcel->getActiveSheet()->setCellValue('S'.$_num, $request['editor']);
		$objPHPExcel->getActiveSheet()->setCellValue('T'.$_num, $request['edited_date']);
		
		$objPHPExcel->getActiveSheet()->setCellValue('U'.$_num, $request['deletor']);
		$objPHPExcel->getActiveSheet()->setCellValue('V'.$_num, $request['deleted_date']);
		
		
		if($request['edited_by'] != ""){
			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle2, "A".$_num.":V".$_num);
		}
		
		if(strtolower($request['deleted']) == "y"){
			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle3, "A".$_num.":V".$_num);
		}
		
		if(strtolower($request['under_30min']) == "n"){
			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1, "A".$_num.":V".$_num);
		}
		
		$_num++;
	}
}else{
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$_num, 'No report available');	
}

// Redirect output to a client’s web browser (Excel2007)
ini_set('zlib.output_compression','Off');
header('Pragma: public');
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");                  // Date in the past
//header('Last-Modified: '.gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');     // HTTP/1.1
header('Cache-Control: pre-check=0, post-check=0, max-age=0');    // HTTP/1.1
header ("Pragma: no-cache");
header("Expires: 0");
header('Content-Transfer-Encoding: none');
header('Content-Type: application/vnd.ms-excel;');                 // This should work for IE & Opera
header("Content-type: application/x-msexcel");                    // This should work for the rest
header('Content-Disposition: attachment; filename="Service Requests Report '.date(savefdt,time()+$_SESSION['tz_offset_sec']).'.xls"');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');

ob_end_flush();

function search($needle, $haystack){
	foreach($haystack as $id => $val)
	{
		if($val['system_id'] == $needle) {
			return $id;			
		}
	}
}
exit("DONE");
?>