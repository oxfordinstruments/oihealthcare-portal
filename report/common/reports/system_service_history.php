<?php
//Update Completed 12/11/14
$debug = false;

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

if(isset($_GET['unique_id'])){
	$unique_id = $_GET['unique_id'];
}else{
	$log->logerr('site_service_history - a',1016,true,basename(__FILE__),__LINE__);
}

d($_GET);

$reports = array();

$sql="SELECT rp.report_id, rp.`date`, rp.finalize_date, rp.complaint, rp.prob_found, rp.service, u1.name AS assigned_eng,
(SELECT SUM(hl.reg_labor) + SUM(hl.ot_labor) FROM systems_hours AS hl WHERE unique_id = rp.unique_id) AS labor,
(SELECT SUM(ht.reg_travel) + SUM(ht.ot_travel) FROM systems_hours AS ht WHERE unique_id = rp.unique_id) AS travel,
(SELECT GROUP_CONCAT(p.description SEPARATOR '\r\n') FROM systems_parts AS p WHERE p.unique_id = rp.unique_id) AS parts,
rp.unique_id, rp.system_unique_id, rp.system_id, f.name AS facility_name
FROM systems_reports AS rp
LEFT JOIN users AS u1 ON u1.uid = rp.user_id
LEFT JOIN facilities AS f ON f.unique_id = rp.facility_unique_id
WHERE rp.`status` = 'closed' AND rp.system_unique_id = '$unique_id'
ORDER BY rp.report_id DESC;";

if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

while($row = $result->fetch_assoc()){
	array_push($reports,$row);
}
d($reports);


if(empty($reports)){
	$no_report = true;
}else{
	$no_report = false;
}
dd($no_report);

// Include PHPExcel
require_once ($_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/Classes/PHPExcel.php');

// Include PHPExcel_IOFactory
require_once ($_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/Classes/PHPExcel/IOFactory.php');

// Check if template exists
//echo "Check if Excel2007 template file exists<br />";
$template_file = $_SERVER['DOCUMENT_ROOT'].'/resources/report_templates/system_service_history.xls';
if (!file_exists($template_file)) {
	$log->logerr($template_file,1045);	
	exit("Error the template is missing." . EOL);
}

// Create new PHPExcel object
//echo "Start PHPExcell", EOL;
$objPHPExcel = new PHPExcel();

//echo "Load from Excel2007 template file", EOL;
$objPHPExcel = PHPExcel_IOFactory::load($template_file);

$sharedStyle1 = new PHPExcel_Style();

$sharedStyle1->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('argb' => 'FFFFCCCC')
							)
));

$_num = 6;

//Select ct sheet
$objPHPExcel->setActiveSheetIndex(0);



if(!$no_report){
		
	//add rows
	foreach($reports as $key => $report){
		
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$_num, $report['report_id']);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$_num, date(phpdispfd,strtotime($report['date'])));
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$_num, date(phpdispfd,strtotime($report['finalize_date'])));
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$_num, $report['assigned_eng']);
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$_num, $report['facility_name']);
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$_num, $report['complaint']);
		//$objPHPExcel->getActiveSheet()->setCellValue('G'.$_num, $report['prob_found']);
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$_num, $report['service']);
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$_num, $report['labor']);
		$objPHPExcel->getActiveSheet()->setCellValue('I'.$_num, $report['travel']);
		$objPHPExcel->getActiveSheet()->setCellValue('J'.$_num, $report['parts']);
		
		$_num++;
	}
}else{
	$objPHPExcel->getActiveSheet()->setCellValue('A6', 'No history for this site');
	$reports[0]['system_id'] = 0000;
}

//Set report date
$objPHPExcel->getActiveSheet()->setCellValue('B3', date(phpdispfdt,time()+$_SESSION['tz_offset_sec']));
//Set site id
$objPHPExcel->getActiveSheet()->setCellValue('B4', $reports[0]['system_id']);

//write file
//echo "Write to Excel2007 format", EOL;
//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
//echo "<a href=\"http://secure.jrdhome.com/report/common/reports/".str_replace('.php', '.xlsx', pathinfo(__FILE__, PATHINFO_BASENAME))."\">".str_replace('.php', '.xlsx', pathinfo(__FILE__, PATHINFO_BASENAME))."</a>", EOL;

// Redirect output to a client’s web browser (Excel2007)
ini_set('zlib.output_compression','Off');
header('Pragma: public');
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");                  // Date in the past
//header('Last-Modified: '.gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');     // HTTP/1.1
header('Cache-Control: pre-check=0, post-check=0, max-age=0');    // HTTP/1.1
header ("Pragma: no-cache");
header("Expires: 0");
header('Content-Transfer-Encoding: none');
header('Content-Type: application/vnd.ms-excel;');                 // This should work for IE & Opera
header("Content-type: application/x-msexcel");                    // This should work for the rest
header('Content-Disposition: attachment; filename="Service History '. $reports[0]['system_id'].' '.date(savefdt,time()+$_SESSION['tz_offset_sec']).'.xls"');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');

ob_end_flush();
exit("DONE");
?>