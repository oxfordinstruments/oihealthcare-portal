<?php
//Map icons list https://sites.google.com/site/gmapsdevelopment/
//Update completed 12/9/2014
$debug = false;
if(isset($_GET['debug'])){
	$debug = true;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

define('NL',PHP_EOL);

$sql="SELECT sb.id, f.lat, f.lng, sb.system_id, sbc.nickname, f.address, f.city, f.state, f.zip, sbc.contact_name, sbc.contact_phone, sbc.contact_email, sb.unique_id, 
c.`type`, c.invoiced, sbc.contract_type, sb.unique_id, sbc.property, sbc.ver
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sbc.unique_id = sb.unique_id
LEFT JOIN misc_contracts AS c ON c.id = sbc.contract_type
LEFT JOIN facilities AS f ON f.unique_id = sbc.facility_unique_id
WHERE sbc.property = 'C' OR sbc.property = 'F'
ORDER BY sbc.system_id ASC;";
s($sql);
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$num_systems = $result->num_rows;
$systems = array();
while($row = $result->fetch_assoc()){
	array_push($systems,$row);	
}
$result->free();
d($systems);
s($num_systems);

$sql="SELECT `office`, `lat`, `lng` FROM misc_offices WHERE active = 'y';";
s($sql);
if(!$result=$mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$num_offices = $result->num_rows;
$offices = array();
while($row = $result->fetch_assoc()){
	$offices[$row['office']] = array('lat'=>$row['lat'], 'lng'=>$row['lng']);	
}
$result->free();
d($offices);
s($num_offices);

//$sql="SELECT u.name, u.lat, u.lng,
//(SELECT GROUP_CONCAT(CONCAT(modality, ' - ', mfg) SEPARATOR '<br>')
//FROM users_quals AS uq
//LEFT JOIN users_qual_id AS uqid ON uqid.id = uq.qid
//WHERE uq.uid = u.uid) AS quals
//FROM users AS u
//LEFT JOIN users_perms AS up ON up.uid = u.uid
//LEFT JOIN users_perm_id AS pid ON pid.id = up.pid
//WHERE pid.perm = 'perm_remote_engineer' AND u.active = 'y';";

$sql="SELECT u.name, u.lat, u.lng,
(SELECT GROUP_CONCAT(CONCAT(modality, ' - ', mfg) SEPARATOR '<br>')
FROM users_quals AS uq
LEFT JOIN users_qual_id AS uqid ON uqid.id = uq.qid
WHERE uq.uid = u.uid) AS quals
FROM users AS u
LEFT JOIN users_roles AS ur ON ur.uid = u.uid
LEFT JOIN users_role_id AS rid ON rid.id = ur.rid
WHERE rid.role = 'role_engineer' AND u.active = 'y';";
s($sql);
if(!$result=$mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$num_engineers = $result->num_rows;
$engineers = array();
while($row = $result->fetch_assoc()){
	if($row['quals'] == ''){
		$row['quals'] = 'N/A';	
	}
	$engineers[$row['name']] = array('lat'=>$row['lat'], 'lng'=>$row['lng'], 'quals'=>$row['quals']);
}
$result->free();
d($engineers);
s($num_engineers);

$sql="SELECT u.name, u.lat, u.lng
FROM users AS u
LEFT JOIN users_roles AS ur ON ur.uid = u.uid
LEFT JOIN users_role_id AS rid ON rid.id = ur.rid
WHERE (rid.role = 'role_sales' OR rid.role = 'role_sales_manager') AND u.active = 'y';";
s($sql);
if(!$result=$mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$num_sales = $result->num_rows;
$sales = array();
while($row = $result->fetch_assoc()){
	$sales[$row['name']] = array('lat'=>$row['lat'], 'lng'=>$row['lng']);	
}
$result->free();
d($sales);
s($num_sales);

$sql="SELECT id, `type`, map_dot, map_name FROM misc_contracts;";
s($sql);
if(!$result=$mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$num_contracts = $result->num_rows;
$contracts = array();
while($row = $result->fetch_assoc()){
	$contracts[$row['id']] = array('type'=>$row['type'], 'map_dot'=>$row['map_dot'], 'map_name'=>$row['map_name']);	
}
$result->free();
d($contracts);
s($num_contracts);

if($debug){
	die();
}
?>
<!DOCTYPE html>
<html>
<head>

<?php require_once($head_include);?>

<script type="text/javascript" src="/resources/js/jquery/jquery.min.js"></script>
<script type="text/javascript" src="/resources/js/jquery.animate-shadow.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDvmB-Iiut3rCFwWrvTKooa6H88GAw4rWs&sensor=false"></script>

<style>
html, body, #map_canvas {
    width: 100%;
    height: 100%;
    margin: 0;
    padding: 0;
}
#map_canvas {
    position: relative;
}
#legend {
        font-family: Arial, sans-serif;
        background: #fff;
        padding: 10px;
        margin: 10px;
        border: 3px solid #000;
      }
      #legend h3 {
        margin-top: 0;
      }
      #legend img {
        vertical-align: middle;
      }
</style>

<script type="text/javascript">
	var iconBase = 'https://maps.google.com/mapfiles/ms/icons/';
	var geocoder;
	var map;
	var infowindow = new google.maps.InfoWindow();
	var markers = [];
	var office_circles = [];
	var office_markers = [];
	var engineer_circles = [];
	var engineer_markers = [];
	var sales_circles = [];
	var sales_markers = [];

	var systems = [
		<?php 
		$i = 1;
		$systems_array = "";
		foreach($systems as $system){
			$systems_array .= "[\"".$system['system_id']." - ".htmlspecialchars($system['nickname'])."\",\"".$system['lat']."\",\"".$system['lng']."\",".$system['contract_type'].",$i,\"".$system['unique_id']."\",\"".$system['property']."\",".$system['ver'].",\"\"],".NL;
			$i++;
		}
		$systems_array = rtrim($systems_array,','.NL);
		echo $systems_array;
		?>
		];
	var offices = [
		<?php
		$i =1;
		$offices_array = "";
		foreach($offices as $key=>$data){
			$offices_array .= "[\"".$key."\",\"".$data['lat']."\",\"".$data['lng']."\",$i],".NL;
			$i++;	
		}
		$offices_array = rtrim($offices_array, ','.NL);
		echo $offices_array;
		?>
		];
	var engineers = [
		<?php
		$i =1;
		$engineers_array = "";
		foreach($engineers as $key=>$data){
			$engineers_array .= "[\"".$key."\",\"".$data['lat']."\",\"".$data['lng']."\",\"".$data['quals']."\",$i],".NL;
			$i++;	
		}
		$engineers_array = rtrim($engineers_array,','.NL);
		echo $engineers_array;
		?>
		];
	var sales = [
		<?php
		$i =1;
		$sales_array = "";
		foreach($sales as $key=>$data){
			$sales_array .= "[\"".$key."\",\"".$data['lat']."\",\"".$data['lng']."\",$i],".NL;
			$i++;	
		}
		$sales_array = rtrim($sales_array,','.NL);
		echo $sales_array;
		?>
		];
	
	var icons = {
		<?php
		foreach($contracts as $key=>$data){
			$str .= $data['map_name'].": {name: '".$data['type']."', icon: iconBase + '".$data['map_dot']."', id: ".$key."},".NL;
		}
		$str = rtrim($str,',');
		echo $str;
		//die();
		?>
	};
	
	//check to see if any of the existing markers match the latlng of the new marker
	if (systems.length != 0) {
		for (i=0; i < systems.length; i++) {
			var a_system = systems[i];
			var a_lat = a_system[1];
			var a_lng = a_system[2];
			var a_id = a_system[4]
			//console.log("A ID:" + a_id);
			for (x=0; x < systems.length; x++) {
				var b_system = systems[x];
				var b_lat = b_system[1];
				var b_lng = b_system[2];
				var b_id = b_system[4]
				//console.log("B ID:" + b_id);
				//if a marker already exists in the same position as this marker
				if(a_id != b_id){
					//console.log("A ID abd B ID not equal");
					if (a_lat == b_lat && a_lng == b_lng) {
						//console.log("Matching LAT LONG");
						//update the position of the coincident marker by applying a small multipler to its coordinates
						var oldLat = parseFloat(a_lat);
						var oldLng = parseFloat(a_lng);
						var newLat = oldLat + (Math.random() * -.5) / 2500;
						var newLng = oldLng + (Math.random() * -.5) / 2500;
						//console.log("Old LAT:" + systems[i][1] + " Old Parsed LAT:" + oldLat + " New LAT:" + newLat);
						//console.log("Old LNG:" + systems[i][2] + " Old Parsed LNG:" + oldLng + " New LNG:" + newLng);
						systems[i][1] = newLat;
						systems[i][2] = newLng;
					}
				}
			}
		}
	}
	
	//check to see if any of the existing engineer markers match the latlng of the new marker
	if (engineers.length != 0) {
		for (i=0; i < engineers.length; i++) {
			var a_engineer = engineers[i];
			var a_lat = a_engineer[1];
			var a_lng = a_engineer[2];
			var a_id = a_engineer[4]
			//console.log("A ID:" + a_id);
			for (x=0; x < engineers.length; x++) {
				var b_engineer = engineers[x];
				var b_lat = b_engineer[1];
				var b_lng = b_engineer[2];
				var b_id = b_engineer[4]
				//console.log("B ID:" + b_id);
				//if a marker already exists in the same position as this marker
				if(a_id != b_id){
					//console.log("A ID abd B ID not equal");
					if (a_lat == b_lat && a_lng == b_lng) {
						//console.log("Matching LAT LONG");
						//update the position of the coincident marker by applying a small multipler to its coordinates
						var oldLat = parseFloat(a_lat);
						var oldLng = parseFloat(a_lng);
						var newLat = oldLat + (Math.random() * -.5) / 2500;
						var newLng = oldLng + (Math.random() * -.5) / 2500;
						//console.log("Old LAT:" + systems[i][1] + " Old Parsed LAT:" + oldLat + " New LAT:" + newLat);
						//console.log("Old LNG:" + systems[i][2] + " Old Parsed LNG:" + oldLng + " New LNG:" + newLng);
						engineers[i][1] = newLat;
						engineers[i][2] = newLng;
					}
				}
			}
		}
	}
	
	function initialize() {
		geocoder = new google.maps.Geocoder();
		var latlng = new google.maps.LatLng(39.1135216,-94.6268234);  //39.1135216,-94.6268234 kansas city
		var myOptions = {
			zoom: 5,
			center: latlng,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
		}
		map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
		
		
		for (var i = 0; i < systems.length; i++) {
			var system = systems[i];
			var myLatLng = new google.maps.LatLng(system[1], system[2]);
			var icn;
			switch(system[3]){
				<?php
				foreach($contracts as $key=>$data){
					echo "case ".$key.":",NL;
					echo "    icn = iconBase + '".$data['map_dot']."';",NL;
					echo "    break;",NL;
				}
				
				?>
				default:
					icn = iconBase + 'blue-dot.png';
					break;					
			}
			
			if(system[6] == "F"){
				icn = iconBase + 'flag.png';	
			}
			
			var marker = new google.maps.Marker({
				position: myLatLng,
				map: map,	
				icon: icn,
				title: system[0],
				zIndex: system[3],
				temp: system[5],
				ver: system[7]
			});
			google.maps.event.addListener(marker, 'click', function() {
				infowindow.setContent(
				<?php 
				if(strtolower($_SESSION['userdata']['grp_employee']) == 'y'){
					echo "\"<h3>\" + this.title + \"</h3><br><a href='/report/common/systems_view.php?unique_id=\" + this.temp + \"&version=\" + this.ver + \"&user_id=".$_SESSION['login']."' target='new'>View System Info</a>\"";
				}else{
					echo "this.title";
				}?>
				);
				infowindow.open(map, this);
			});
			markers.push(marker);
		}
		
		// Add the circles to the map.
		for(var key in offices){
			var circleLatLng = new google.maps.LatLng(offices[key][1],offices[key][2]);
			var circle = new google.maps.Circle({
				strokeColor: '#FF0000',
				strokeOpacity: 0.2,
				strokeWeight: 1,
				fillColor: '#FF0000',
				fillOpacity: 0.1,
				map: map,
				center: new google.maps.LatLng(offices[key][1],offices[key][2]),
				radius: 402336
			});
			marker = new google.maps.Marker({
					position: circleLatLng,
					map: map,	
					icon: iconBase + 'homegardenbusiness.png',
					title: offices[key][0],
					zIndex: 0
				});
			
			google.maps.event.addListener(marker, 'click', function() {
				infowindow.setContent(
						this.title
				);
				infowindow.open(map, this);
			});			
			office_circles.push(circle);
			office_markers.push(marker);
		}
		
		for (var key in engineers) {
			var engineerLatLng = new google.maps.LatLng(engineers[key][1], engineers[key][2]);
			var circle = new google.maps.Circle({
				strokeColor: '#0000FF',
				strokeOpacity: 0.2,
				strokeWeight: 1,
				fillColor: '#0000FF',
				fillOpacity: 0.1,
				map: map,
				center: engineerLatLng,
				radius: 402336
			});
			var marker = new google.maps.Marker({
				position: engineerLatLng,
				map: map,	
				icon: iconBase + 'mechanic.png',
				title: 'Engineer: ' + engineers[key][0],
				other: engineers[key][3],
				zIndex: 0
			});
			google.maps.event.addListener(marker, 'click', function() {
				infowindow.setContent(
					this.title + "<br><br>Qualifications:<br>" + this.other 
				);
				infowindow.open(map, this);
			});
			engineer_markers.push(marker);
			engineer_circles.push(circle);
		}
		
		for (var key in sales) {
			var salesLatLng = new google.maps.LatLng(sales[key][1], sales[key][2]);
			var circle = new google.maps.Circle({
				strokeColor: '#0000FF',
				strokeOpacity: 0.2,
				strokeWeight: 1,
				fillColor: '#0000FF',
				fillOpacity: 0.1,
				map: map,
				center: salesLatLng,
				radius: 402336
			});
			var marker = new google.maps.Marker({
				position: salesLatLng,
				map: map,	
				icon: iconBase + 'golfer.png',
				title: 'Sales: ' + sales[key][0],
				zIndex: 0
			});
			google.maps.event.addListener(marker, 'click', function() {
				infowindow.setContent(
						this.title
				);
				infowindow.open(map, this);
			});
			sales_markers.push(marker);
			sales_circles.push(circle);
		}
				
		var legend = document.getElementById('legend');
        for (var key in icons) {
          var type = icons[key];
          var name = type.name;
          var icon = type.icon;
		  var id = type.id;
          var div = document.createElement('div');
          //div.innerHTML = '<img src="' + icon + '"> ' + name;
		  div.innerHTML = "<input type=\"checkbox\" id=\"legend_" + id + "\" onclick=\"markers_toggle('" + icon + "',this.id);\" checked=\"checked\" /><img src=\"" + icon + "\"> " + name;
          legend.appendChild(div);
        }
		
		var div = document.createElement('div');
		div.innerHTML = "<input type=\"checkbox\" id=\"legend_future\" onclick=\"markers_toggle('" + iconBase + "flag.png',this.id);\" checked=\"checked\" /><img src=\"" + iconBase + "flag.png\"> Future";
		legend.appendChild(div);
		
		var div = document.createElement('div');
		div.innerHTML = "<input type=\"checkbox\" id=\"legend_engineer\" onclick=\"engineers_toggle(this.id);\" checked=\"checked\" /><img src=\"" + iconBase + "mechanic.png\"> Engineers";
		legend.appendChild(div);

		var div = document.createElement('div');
		div.innerHTML = "<input type=\"checkbox\" id=\"legend_sales\" onclick=\"sales_toggle(this.id);\" checked=\"checked\" /><img src=\"" + iconBase + "golfer.png\"> Sales";
		legend.appendChild(div);

		var div = document.createElement('div');
		div.innerHTML = "<input type=\"checkbox\" id=\"legend_office\" onclick=\"offices_toggle(this.id);\" checked=\"checked\" /><img src=\"" + iconBase + "homegardenbusiness.png\"> Offices";
		legend.appendChild(div);
		
		var div = document.createElement('div');
		div.innerHTML = "<br><a href='#' onClick=\"uncheck_all();\"><span>Un-check All</span></a>";
		legend.appendChild(div);
		
		var div = document.createElement('div');
		div.innerHTML = "<br><span>Circle 250 Miles Radius</span>";
		legend.appendChild(div);
				
        map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(legend);
	}

google.maps.event.addDomListener(window, 'load', initialize);


function markers_toggle(icon,check_id){
	if(document.getElementById(check_id).checked){
		for (var key in markers){
			if(markers[key]['icon'] == icon){
				markers[key].setMap(map);
			}
		}
	}else{
		for (var key in markers){
			if(markers[key]['icon'] == icon){
				markers[key].setMap(null);
			}
		}
	}
}

function uncheck_all(){
	$("input[id^='legend_'").each(function(index, element) {
		$("#legend_"+index).attr('checked', false);
	});
	$("#legend_future").attr('checked', false);
	$("#legend_office").attr('checked', false);
	$("#legend_engineer").attr('checked', false);
	$("#legend_sales").attr('checked', false);
	for (var key in markers){
		markers[key].setMap(null);
	}
	for (var key in office_circles){
		office_circles[key].setMap(null);
	}
	for (var key in office_markers){
		office_markers[key].setMap(null)
	}
	for (var key in engineer_circles){
		engineer_circles[key].setMap(null);
	}
	for (var key in engineer_markers){
		engineer_markers[key].setMap(null)
	}
	for (var key in sales_circles){
		sales_circles[key].setMap(null);
	}
	for (var key in sales_markers){
		sales_markers[key].setMap(null)
	}
}

function offices_toggle(check_id){
	if(document.getElementById(check_id).checked){
		for (var key in office_circles){
			office_circles[key].setMap(map);
		}
		for (var key in office_markers){
			office_markers[key].setMap(map)
		}
	}else{
		for (var key in office_circles){
			office_circles[key].setMap(null);
		}
		for (var key in office_markers){
			office_markers[key].setMap(null)
		}
	}
}

function engineers_toggle(check_id){
	if(document.getElementById(check_id).checked){
		for (var key in engineer_circles){
			engineer_circles[key].setMap(map);
		}
		for (var key in engineer_markers){
			engineer_markers[key].setMap(map)
		}
	}else{
		for (var key in engineer_circles){
			engineer_circles[key].setMap(null);
		}
		for (var key in engineer_markers){
			engineer_markers[key].setMap(null)
		}
	}
}

function sales_toggle(check_id){
	if(document.getElementById(check_id).checked){
		for (var key in sales_circles){
			sales_circles[key].setMap(map);
		}
		for (var key in sales_markers){
			sales_markers[key].setMap(map)
		}
	}else{
		for (var key in sales_circles){
			sales_circles[key].setMap(null);
		}
		for (var key in sales_markers){
			sales_markers[key].setMap(null)
		}
	}
}

</script>
</head>
<body>
<div id="map_canvas"> </div>
<div id="legend"><h3>Legend</h3></div>
</body>
</html>