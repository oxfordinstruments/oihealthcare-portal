<?php
//
// Error reporting
//
//error_reporting(E_ALL);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
ini_set('display_errors', 'On');

$debug = false;
$send = false;

if(isset($_GET['debug'])){
	$debug = true;
	if(isset($_GET['send'])){$send = true;}else{$send = false;}
}else{
	$send = true;
}

$no_define_eol = true;
if(substr(php_sapi_name(), 0, 3) == 'cli' || empty($_SERVER['REMOTE_ADDR'])) {
	//shell
	$docroot = preg_replace("/(public_html).*/",'public_html',dirname(__FILE__),2);
	$no_define_eol = true;
	define('EOL',PHP_EOL);
} else {
	//webserver
	$docroot = $_SERVER['DOCUMENT_ROOT'];
	define('EOL','<br />');
}

$settings = new SimpleXMLElement($docroot.'/settings.xml', null, true);
require_once($docroot.'/define_inc.php');

require_once($docroot.'/log/log.php');
$log = new logger();

require_once($docroot.'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

require_once($docroot.'/resources/moment_php/Moment.php');
require_once($docroot.'/resources/moment_php/MomentException.php');
require_once($docroot.'/resources/moment_php/MomentHelper.php');
require_once($docroot.'/resources/moment_php/MomentLocale.php');
require_once($docroot.'/resources/moment_php/MomentFromVo.php');

require_once($docroot.'/report/common/scripts/php_utils.php');
$php_utils =  new phpUtils();

d($_GET);
d($_POST);


$get_year = false;
if(isset($_POST['year']) and $_POST['year'] != ''){
	$get_year = $_POST['year'];
}elseif(isset($_GET['year']) and $_GET['year'] != ''){
	$get_year = $_GET['year'];
}else{
	die('GET/POST year not set');
}

if($debug){$php_utils->message('Begin');}

require($docroot.'/mysqlInfo.php');
$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno) {die('There was an error running the query [' . $mysqli->connect_error . ']');}

$cur_date = new \Moment\Moment();
d($cur_date);
$fiscal_begin = new \Moment\Moment($get_year . '-' . $settings->fiscal->year_end_month . '-' . $settings->fiscal->year_end_day . 'T00:00:00Z');
$fiscal_begin->addDays(1);
$fiscal_begin->setImmutableMode(true);
d($fiscal_begin);
$fiscal_end = $fiscal_begin->addYears(1);
d($fiscal_end);
$fiscal_year = $fiscal_begin->getYear() . '/' . $fiscal_end->getYear();
d($fiscal_year);

$full_year_complete = false;
if($cur_date > $fiscal_end){
	$full_year_complete = true;
}
d($full_year_complete);


$reports = array();

$sql="SELECT sr.report_id, sr.system_id, sr.system_nickname, mc.`type` AS contract_type, st.name AS equipment_name, st.`type` AS equipment_type, st.mfg AS equipment_mfg,
ueng.name AS eng_assigned, srq.initial_call_date, srq.onsite_date, 
(SELECT SUM(sh1.reg_labor) FROM systems_hours AS sh1 WHERE sh1.report_id = sr.report_id) AS reg_labor,
(SELECT SUM(sh2.ot_labor) FROM systems_hours AS sh2 WHERE sh2.report_id = sr.report_id) AS ot_labor,
(SELECT SUM(sh3.reg_travel) FROM systems_hours AS sh3 WHERE sh3.report_id = sr.report_id) AS reg_travel,
(SELECT SUM(sh4.ot_travel) FROM systems_hours AS sh4 WHERE sh4.report_id = sr.report_id) AS ot_travel,
(SELECT SUM(sh5.phone) FROM systems_hours AS sh5 WHERE sh5.report_id = sr.report_id) AS phone,
(SELECT GROUP_CONCAT(sh6.date SEPARATOR '   ') FROM systems_hours AS sh6 WHERE sh6.report_id = sr.report_id ORDER BY sh6.date DESC) AS dates
FROM systems_reports AS sr
LEFT JOIN systems_base_cont AS sbc ON sbc.ver_unique_id = sr.system_ver_unique_id
LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
LEFT JOIN users AS ueng ON ueng.uid = sr.assigned_engineer
LEFT JOIN systems_requests AS srq ON srq.unique_id = sr.unique_id
LEFT JOIN misc_contracts AS mc ON mc.id = sbc.contract_type
LEFT JOIN systems_types AS st ON st.id = sb.system_type
WHERE sr.`status` = 'closed'
AND (STR_TO_DATE(srq.initial_call_date, '".$settings->mysql_store_datetime."') BETWEEN STR_TO_DATE('".$fiscal_begin->format()."','".$settings->mysql_moment_datetime."') AND STR_TO_DATE('".$fiscal_end->format()."','".$settings->mysql_moment_datetime."') )
ORDER BY sr.report_id;";
s($sql);
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$cols = $result->fetch_fields();
d($cols[0]->name);
while($row = $result->fetch_assoc()){
	//array_push($reports,$row);
	$reports[$row['report_id']] = $row;
}
d($reports);

$no_report = false;
if(count($reports) == 0){
	$no_report = true;
}

if($debug){die('HALT');}

// Include PHPExcel
require_once ($docroot.'/resources/phpexcell/Classes/PHPExcel.php');

// Include PHPExcel_IOFactory
require_once ($docroot.'/resources/phpexcell/Classes/PHPExcel/IOFactory.php');

// Check if template exists
//echo "Check if Excel2007 template file exists<br />";
$template_file = $docroot.'/resources/report_templates/systems_report_fy.xls';
if (!file_exists($template_file)) {
	$log->logerr($template_file,1045);	
	exit("Error the template is missing." . EOL);
}

// Create new PHPExcel object
//echo "Start PHPExcell", EOL;
$objPHPExcel = new PHPExcel();

//echo "Load from Excel2007 template file", EOL;
$objPHPExcel = PHPExcel_IOFactory::load($template_file);

$_num = 8;

//Select ct sheet
$objPHPExcel->setActiveSheetIndex(0);

if(!$no_report){
		
	//add rows
	foreach($reports as $report_id => $report){
		
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$_num, $report_id);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$_num, $report['system_id']);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$_num, $report['system_nickname']);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$_num, $report['contract_type']);
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$_num, $report['equipment_name']);
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$_num, $report['equipment_type']);
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$_num, $report['equipment_mfg']);
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$_num, $report['eng_assigned']);
		$objPHPExcel->getActiveSheet()->setCellValue('I'.$_num, date(phpdispfd,strtotime($report['initial_call_date'])));
		$objPHPExcel->getActiveSheet()->setCellValue('J'.$_num, date(phpdispfd,strtotime($report['onsite_date'])));
		$objPHPExcel->getActiveSheet()->setCellValue('K'.$_num, $report['reg_labor']);
		$objPHPExcel->getActiveSheet()->setCellValue('L'.$_num, $report['ot_labor']);
		$objPHPExcel->getActiveSheet()->setCellValue('M'.$_num, $report['reg_travel']);
		$objPHPExcel->getActiveSheet()->setCellValue('N'.$_num, $report['ot_travel']);
		$objPHPExcel->getActiveSheet()->setCellValue('O'.$_num, $report['phone']);
		$objPHPExcel->getActiveSheet()->setCellValue('P'.$_num, $report['dates']);



		
		$_num++;
	}
}else{
	$objPHPExcel->getActiveSheet()->setCellValue('A6', 'No history for this fiscal year');
}

//Set report date
$objPHPExcel->getActiveSheet()->setCellValue('B3', date(phpdispfdt,time()+$_SESSION['tz_offset_sec']));
//Set fiscal year
$objPHPExcel->getActiveSheet()->setCellValue('B4', $fiscal_year);
//Full Year Messaage
if($full_year_complete){
	$objPHPExcel->getActiveSheet()->setCellValue('A6', "");
}


//write file
//echo "Write to Excel2007 format", EOL;
//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
//echo "<a href=\"http://secure.jrdhome.com/report/common/reports/".str_replace('.php', '.xlsx', pathinfo(__FILE__, PATHINFO_BASENAME))."\">".str_replace('.php', '.xlsx', pathinfo(__FILE__, PATHINFO_BASENAME))."</a>", EOL;

// Redirect output to a client’s web browser (Excel2007)
ini_set('zlib.output_compression','Off');
header('Pragma: public');
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");                  // Date in the past
//header('Last-Modified: '.gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');     // HTTP/1.1
header('Cache-Control: pre-check=0, post-check=0, max-age=0');    // HTTP/1.1
header ("Pragma: no-cache");
header("Expires: 0");
header('Content-Transfer-Encoding: none');
header('Content-Type: application/vnd.ms-excel;');                 // This should work for IE & Opera
header("Content-type: application/x-msexcel");                    // This should work for the rest
header('Content-Disposition: attachment; filename="Systems Service '.$fiscal_year. ' ' . $reports[0]['system_id'].' '.date(savefdt,time()+$_SESSION['tz_offset_sec']).'.xls"');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');

ob_end_flush();
exit("DONE");

function yn($val){
	if(strtolower($val) == 'y'){
		return true;
	}else{
		return false;
	}
}
?>