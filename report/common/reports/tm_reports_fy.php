<?php
//
// Error reporting
//
//error_reporting(E_ALL);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
ini_set('display_errors', 'On');

$debug = false;
$send = false;

if(isset($_GET['debug'])){
	$debug = true;
	if(isset($_GET['send'])){$send = true;}else{$send = false;}
}else{
	$send = true;
}

$no_define_eol = true;
if(substr(php_sapi_name(), 0, 3) == 'cli' || empty($_SERVER['REMOTE_ADDR'])) {
	//shell
	$docroot = preg_replace("/(public_html).*/",'public_html',dirname(__FILE__),2);
	$no_define_eol = true;
	define('EOL',PHP_EOL);
} else {
	//webserver
	$docroot = $_SERVER['DOCUMENT_ROOT'];
	define('EOL','<br />');
}

$settings = new SimpleXMLElement($docroot.'/settings.xml', null, true);
require_once($docroot.'/define_inc.php');

require_once($docroot.'/log/log.php');
$log = new logger();

require_once($docroot.'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

require_once($docroot.'/resources/moment_php/Moment.php');
require_once($docroot.'/resources/moment_php/MomentException.php');
require_once($docroot.'/resources/moment_php/MomentHelper.php');
require_once($docroot.'/resources/moment_php/MomentLocale.php');
require_once($docroot.'/resources/moment_php/MomentFromVo.php');

require_once($docroot.'/report/common/scripts/php_utils.php');
$php_utils =  new phpUtils();

d($_GET);
d($_POST);


$get_year = false;
if(isset($_POST['year']) and $_POST['year'] != ''){
	$get_year = $_POST['year'];
}elseif(isset($_GET['year']) and $_GET['year'] != ''){
	$get_year = $_GET['year'];
}else{
	die('GET/POST year not set');
}

if($debug){$php_utils->message('Begin');}

require($docroot.'/mysqlInfo.php');
$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno) {die('There was an error running the query [' . $mysqli->connect_error . ']');}

$cur_date = new \Moment\Moment();
d($cur_date);
$fiscal_begin = new \Moment\Moment($get_year . '-' . $settings->fiscal->year_end_month . '-' . $settings->fiscal->year_end_day . 'T00:00:00Z');
$fiscal_begin->addDays(1);
$fiscal_begin->setImmutableMode(true);
d($fiscal_begin);
$fiscal_end = $fiscal_begin->addYears(1);
d($fiscal_end);
$fiscal_year = $fiscal_begin->getYear() . '/' . $fiscal_end->getYear();
d($fiscal_year);

$full_year_complete = false;
if($cur_date > $fiscal_end){
	$full_year_complete = true;
}
d($full_year_complete);


$reports = array();

$sql="SELECT sr.report_id, sr.system_id, sr.system_nickname, mc.`type` AS contract_type, st.name AS equipment_name, st.`type` AS equipment_type, st.mfg AS equipment_mfg,
ueng.name AS eng_assigned, upri.name AS eng_pri, usec.name AS eng_sec, srq.initial_call_date, srq.onsite_date, 
srq.invoice_required AS request_invoice, srq.po_num AS request_po, sr.invoice_required AS report_invoice, sr.po_number AS report_po,
sr.valuation_complete, sr.valuation_do_not_invoice, sr.valuation_uid, sr.invoice_labor_reg, sr.invoice_labor_ot, sr.invoice_travel_reg, sr.invoice_travel_ot,
sr.invoice_shipping, sr.invoice_expense, sr.invoiced, sr.invoiced_uid, sr.invoice_number, sr.invoice_date,
(SELECT SUM(sp.price) FROM systems_parts AS sp WHERE sp.unique_id = sr.unique_id) AS parts_total_price,
(SELECT COUNT(sp.id) FROM systems_parts AS sp WHERE sp.unique_id = sr.unique_id) AS parts_used
FROM systems_reports AS sr
LEFT JOIN systems_base_cont AS sbc ON sbc.ver_unique_id = sr.system_ver_unique_id
LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
LEFT JOIN users AS ueng ON ueng.uid = sr.assigned_engineer
LEFT JOIN systems_requests AS srq ON srq.unique_id = sr.unique_id
LEFT JOIN systems_assigned_pri AS sap ON sap.system_ver_unique_id = sbc.ver_unique_id
LEFT JOIN users AS upri ON upri.uid = sap.uid
LEFT JOIN systems_assigned_sec AS sas ON sas.system_ver_unique_id = sbc.ver_unique_id
LEFT JOIN users AS usec ON usec.uid = sas.uid
LEFT JOIN misc_contracts AS mc ON mc.id = sbc.contract_type
LEFT JOIN systems_types AS st ON st.id = sb.system_type
WHERE sr.`status` = 'closed'
AND mc.id in(2,5,6,8,9)
AND (STR_TO_DATE(srq.initial_call_date, '".$settings->mysql_store_datetime."') BETWEEN STR_TO_DATE('".$fiscal_begin->format()."','".$settings->mysql_moment_datetime."') AND STR_TO_DATE('".$fiscal_end->format()."','".$settings->mysql_moment_datetime."') )
ORDER BY sr.report_id;";
s($sql);
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$cols = $result->fetch_fields();
d($cols[0]->name);
while($row = $result->fetch_assoc()){
	//array_push($reports,$row);
	$reports[$row['report_id']] = $row;
}
d($reports);

foreach($reports as $report_id=>$report){
	$reports[$report_id]['error'] = array();
	$reports[$report_id]['labor_travel_fail'] = false;
	try{
		$invoice_labor_reg = intval($report['invoice_labor_reg']);
		$invoice_labor_ot = intval($report['invoice_labor_ot']);
		$reports[$report_id]['labor_total'] = $invoice_labor_reg + $invoice_labor_ot;
		$invoice_travel_reg = intval($report['invoice_travel_reg']);
		$invoice_travel_ot = intval($report['invoice_travel_ot']);
		$reports[$report_id]['travel_total'] = $invoice_travel_reg + $invoice_travel_ot;
		$invoice_shipping = intval($report['invoice_shipping']);
		$invoice_expense = intval($report['invoice_expense']);
		$parts_total_price = intval($report['parts_total_price']);
		$reports[$report_id]['invoiced_total'] = $reports[$report_id]['labor_total'] + $reports[$report_id]['travel_total'] + $invoice_shipping + $invoice_expense + $parts_total_price;
	}
	catch(Exception $e){
		$reports[$report_id]['labor_travel_fail'] = true;
		array_push($reports[$report_id]['error'],'Labor/Travel Data Invalid Number');
	}

	$reports[$report_id]['po_missing'] = false;
	$reports[$report_id]['po_mismatch'] = false;
	$reports[$report_id]['invoice_required'] = false;
	$reports[$report_id]['po_number'] = '';
	if(strtolower($report['request_invoice']) == 'y' or strtolower($report['report_invoice']) == 'y'){
		$reports[$report_id]['invoice_required'] = true;
		if($report['request_po'] != '' or $report['report_po'] != ''){
			if($report['request_po'] != '' and $report['report_po'] != ''){
				if($report['request_po'] != $report['report_po']){
					$reports[$report_id]['po_mismatch'] = true;
					array_push($reports[$report_id]['error'], 'Report/Request PO Mismatch; Report: '.$report['report_po'].'  Request: '.$report['request_po']);
				}
			}else{
				$reports[$report_id]['po_number'] = $report['request_po'];
			}
		}else{
			$reports[$report_id]['po_missing'] = true;
			array_push($reports[$report_id]['error'], 'PO Missing');
		}
	}

	$reports[$report_id]['invoiced'] = yn($reports[$report_id]['invoiced']);
	$reports[$report_id]['valuation_do_not_invoice'] = yn($reports[$report_id]['valuation_do_not_invoice']);
	$reports[$report_id]['valuation_complete'] = yn($reports[$report_id]['valuation_complete']);


}

d($reports);

$no_report = false;
if(count($reports) == 0){
	$no_report = true;
}

if($debug){die('HALT');}

// Include PHPExcel
require_once ($docroot.'/resources/phpexcell/Classes/PHPExcel.php');

// Include PHPExcel_IOFactory
require_once ($docroot.'/resources/phpexcell/Classes/PHPExcel/IOFactory.php');

// Check if template exists
//echo "Check if Excel2007 template file exists<br />";
$template_file = $docroot.'/resources/report_templates/tm_report_fy.xls';
if (!file_exists($template_file)) {
	$log->logerr($template_file,1045);	
	exit("Error the template is missing." . EOL);
}

// Create new PHPExcel object
//echo "Start PHPExcell", EOL;
$objPHPExcel = new PHPExcel();

//echo "Load from Excel2007 template file", EOL;
$objPHPExcel = PHPExcel_IOFactory::load($template_file);

$style_FillBG_White = new PHPExcel_Style();
$style_FillBG_White->applyFromArray(
	array('fill' => array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color' => array('argb' => 'FFFFFFFF')))
);

$style_FillBG_Red = new PHPExcel_Style();
$style_FillBG_Red->applyFromArray(
	array('fill' => array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color' => array('argb' => 'FFFFC7CE')),
		'font'  => array('color' => array('rgb' => '800000')))
);

$style_FillBG_Yellow = new PHPExcel_Style();
$style_FillBG_Yellow->applyFromArray(
	array('fill' => array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color' => array('argb' => 'FFE8F2B0')),
		'font'  => array('color' => array('rgb' => '800000')))
);

$style_FillTXT_Red = new PHPExcel_Style();
$style_FillTXT_Red->applyFromArray(
	array('font'  => array('color' => array('rgb' => '800000')))
);

$_num = 8;

//Select ct sheet
$objPHPExcel->setActiveSheetIndex(0);



if(!$no_report){
		
	//add rows
	foreach($reports as $report_id => $report){
		
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$_num, $report_id);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$_num, $report['system_id']);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$_num, $report['system_nickname']);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$_num, $report['contract_type']);
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$_num, $report['equipment_name']);
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$_num, $report['equipment_type']);
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$_num, $report['equipment_mfg']);
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$_num, $report['eng_pri']);
		$objPHPExcel->getActiveSheet()->setCellValue('I'.$_num, $report['eng_sec']);
		$objPHPExcel->getActiveSheet()->setCellValue('J'.$_num, $report['eng_assigned']);
		$objPHPExcel->getActiveSheet()->setCellValue('K'.$_num, date(phpdispfd,strtotime($report['initial_call_date'])));
		$objPHPExcel->getActiveSheet()->setCellValue('L'.$_num, date(phpdispfd,strtotime($report['onsite_date'])));
		$objPHPExcel->getActiveSheet()->setCellValue('M'.$_num, $report['invoice_required']);
		$objPHPExcel->getActiveSheet()->setCellValue('N'.$_num, $report['po_number']);
		$objPHPExcel->getActiveSheet()->setCellValue('O'.$_num, $report['valuation_complete']);
		$objPHPExcel->getActiveSheet()->setCellValue('P'.$_num, $report['valuation_do_not_invoice']);
		$objPHPExcel->getActiveSheet()->setCellValue('Q'.$_num, $report['valuation_uid']);
		$objPHPExcel->getActiveSheet()->setCellValue('R'.$_num, $report['invoiced']);
		$objPHPExcel->getActiveSheet()->setCellValue('S'.$_num, $report['invoiced_uid']);
		$objPHPExcel->getActiveSheet()->setCellValue('T'.$_num, $report['invoice_number']);
		if($report['invoiced']){ $objPHPExcel->getActiveSheet()->setCellValue('U' . $_num, date(phpdispfd, strtotime($report['invoice_date']))); }
		$objPHPExcel->getActiveSheet()->setCellValue('V'.$_num, $report['labor_total']);
		$objPHPExcel->getActiveSheet()->setCellValue('W'.$_num, $report['travel_total']);
		$objPHPExcel->getActiveSheet()->setCellValue('X'.$_num, $report['parts_used']);
		$objPHPExcel->getActiveSheet()->setCellValue('Y'.$_num, $report['parts_total_price']);
		$objPHPExcel->getActiveSheet()->setCellValue('Z'.$_num, $report['invoice_shipping']);
		$objPHPExcel->getActiveSheet()->setCellValue('AA'.$_num, $report['invoice_expense']);
		$objPHPExcel->getActiveSheet()->setCellValue('AB'.$_num, $report['invoiced_total']);
		$objPHPExcel->getActiveSheet()->setCellValue('AC'.$_num, implode('   ', $report['error']));

		if(count($report['error']) > 0){
			$objPHPExcel->getActiveSheet()->setSharedStyle($style_FillBG_Red, "A".$_num.":AC".$_num);
		}elseif(!$report['invoiced']){
			$objPHPExcel->getActiveSheet()->setSharedStyle($style_FillBG_Yellow, "A".$_num.":AC".$_num);
		}else{
			$objPHPExcel->getActiveSheet()->setSharedStyle($style_FillBG_White, "A".$_num.":AC".$_num);
		}

		
		$_num++;
	}
}else{
	$objPHPExcel->getActiveSheet()->setCellValue('A6', 'No history for this fiscal year');
}

//Set Legend
$objPHPExcel->getActiveSheet()->setCellValue('F4', 'Error');
$objPHPExcel->getActiveSheet()->setSharedStyle($style_FillBG_Red, "F4");
$objPHPExcel->getActiveSheet()->setCellValue('G4', 'Not Invoiced');
$objPHPExcel->getActiveSheet()->setSharedStyle($style_FillBG_Yellow, "G4");


//Set report date
$objPHPExcel->getActiveSheet()->setCellValue('B3', date(phpdispfdt,time()+$_SESSION['tz_offset_sec']));
//Set fiscal year
$objPHPExcel->getActiveSheet()->setCellValue('B4', $fiscal_year);
//Full Year Messaage
if($full_year_complete){
	$objPHPExcel->getActiveSheet()->setCellValue('A6', "");
}


//write file
//echo "Write to Excel2007 format", EOL;
//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
//echo "<a href=\"http://secure.jrdhome.com/report/common/reports/".str_replace('.php', '.xlsx', pathinfo(__FILE__, PATHINFO_BASENAME))."\">".str_replace('.php', '.xlsx', pathinfo(__FILE__, PATHINFO_BASENAME))."</a>", EOL;

// Redirect output to a client’s web browser (Excel2007)
ini_set('zlib.output_compression','Off');
header('Pragma: public');
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");                  // Date in the past
//header('Last-Modified: '.gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');     // HTTP/1.1
header('Cache-Control: pre-check=0, post-check=0, max-age=0');    // HTTP/1.1
header ("Pragma: no-cache");
header("Expires: 0");
header('Content-Transfer-Encoding: none');
header('Content-Type: application/vnd.ms-excel;');                 // This should work for IE & Opera
header("Content-type: application/x-msexcel");                    // This should work for the rest
header('Content-Disposition: attachment; filename="TM Service '.$fiscal_year. ' ' . $reports[0]['system_id'].' '.date(savefdt,time()+$_SESSION['tz_offset_sec']).'.xls"');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');

ob_end_flush();
exit("DONE");

function yn($val){
	if(strtolower($val) == 'y'){
		return true;
	}else{
		return false;
	}
}
?>