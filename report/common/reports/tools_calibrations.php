<?php
$debug = false;
if(isset($_GET['debug'])){
	$debug = true;
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

//Get offices
$offices = array();
$sql="SELECT id, office FROM misc_offices;";
s($sql);
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
while($row = $result->fetch_assoc()){
	$offices[$row['id']] = array('office'=>$row['office']);	
}
d($offices);

$data = array();
$sql="SELECT t.id, t.calibrating, t.tool, t.model, t.serial, t.mfg, t.location, t.office, u.name, t.notes, tc.date, tc.expire, tc.calibrated_by, tc.notes AS cal_notes
FROM tools AS t
LEFT JOIN misc_offices AS mo ON mo.id = t.office
LEFT JOIN users AS u ON u.uid = t.uid
LEFT JOIN tools_calibrations AS tc ON tc.unique_id = t.unique_id
AND tc.id = (SELECT MAX(tc2.id) FROM tools_calibrations AS tc2 WHERE tc2.unique_id = t.unique_id)
WHERE t.active = 'y' AND archived = 'n';";
s($sql);
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
while($row = $result->fetch_assoc()){
	$data[$row['office']][$row['id']] = $row;
}
d($data);

if($debug){
	die("DEBUG DONE");	
}

// Include PHPExcel
require_once ($_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/Classes/PHPExcel.php');

// Include PHPExcel_IOFactory
require_once ($_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/Classes/PHPExcel/IOFactory.php');

// Check if template exists
//echo "Check if Excel2007 template file exists<br />";
$template_file = $_SERVER['DOCUMENT_ROOT'].'/resources/report_templates/tools_calibrations.xls';
if (!file_exists($template_file)) {
	$log->logerr($template_file,1045);	
	exit("Error the template is missing." . EOL);
}

// Create new PHPExcel object
//echo "Start PHPExcell", EOL;
$objPHPExcel = new PHPExcel();

//echo "Load from Excel2007 template file", EOL;
$objPHPExcel = PHPExcel_IOFactory::load($template_file);

$style_FillBG_Green = new PHPExcel_Style();
$style_FillBG_Green->applyFromArray(
	array('fill' => array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color' => array('argb' => 'FFD7E4BC')),
	'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT))
);

$style_FillBG_Red = new PHPExcel_Style();
$style_FillBG_Red->applyFromArray(
	array('fill' => array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color' => array('argb' => 'FFFFC7CE')),
	'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT),
	'font'  => array('color' => array('rgb' => '800000')))
);

$style_FillBG_Yellow = new PHPExcel_Style();
$style_FillBG_Yellow->applyFromArray(
	array('fill' => array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color' => array('argb' => 'FFE8F2B0')),
	'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT),
	'font'  => array('color' => array('rgb' => '800000')))
);




//Set report date
$report_date = date(phpdispfdt,time()+$_SESSION['tz_offset_sec']);

//Set base sheet
$baseSheet = $objPHPExcel->getActiveSheet();

//Create sheets
$sheetId = 0;
foreach($data as $key=>$value){
	if($sheetId != 0){
		$newSheet = clone $baseSheet;
		$newSheet->setTitle($offices[$key]['office']);
		$objPHPExcel->addSheet($newSheet,$sheetId);
		$objPHPExcel->setActiveSheetIndex($sheetId);
		$objPHPExcel->getActiveSheet()->setCellValue('b3', $report_date);

	}else{
		$objPHPExcel->setActiveSheetIndex(0);
		$baseSheet->setTitle($offices[$key]['office']);	
		$objPHPExcel->getActiveSheet()->setCellValue('b3', $report_date);
	}
	$sheetId++;	
}

//Fill in data
$sheetId = 0;
foreach($data as $office){
	$objPHPExcel->setActiveSheetIndex($sheetId);
	$sheetId++;
	
	//add rows
	$_num = 5;
	foreach($office as $value){		
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$_num, $value['tool']);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$_num, $value['model']);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$_num, (string)$value['serial']);
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$_num, $value['mfg']);
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$_num, date(phpdispfd,strtotime($value['date'])));
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$_num, date(phpdispfd,strtotime($value['expire'])));
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$_num, $value['calibrated_by']);
		$objPHPExcel->getActiveSheet()->setCellValue('I'.$_num, $value['name']);
		$objPHPExcel->getActiveSheet()->setCellValue('J'.$_num, $value['location']);
		$objPHPExcel->getActiveSheet()->setCellValue('K'.$_num, $value['notes']);
		$objPHPExcel->getActiveSheet()->setCellValue('L'.$_num, $value['cal_notes']);
		$objPHPExcel->getActiveSheet()->setCellValue('M'.$_num, $value['id']);
		
		if(strtolower($value['calibrating']) == 'y'){
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$_num, "Calibrating");
			$objPHPExcel->getActiveSheet()->setSharedStyle($style_FillBG_Green, "A".$_num.":M".$_num);
		}elseif(strtotime($value['expire']) <= time()){
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$_num, "Expired");
			$objPHPExcel->getActiveSheet()->setSharedStyle($style_FillBG_Red, "A".$_num.":M".$_num);
		}elseif(strtotime($value['expire']) <= strtotime("+1 month", time())){
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$_num, "Calibration Due");
			$objPHPExcel->getActiveSheet()->setSharedStyle($style_FillBG_Yellow, "A".$_num.":M".$_num);
		}else{
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$_num, "");
		}

		$objPHPExcel->getActiveSheet()->getStyle('D'.$_num)->getNumberFormat()->setFormatCode('0');
		$objPHPExcel->getActiveSheet()->getStyle('D'.$_num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		
		$_num++;
	}
	
}

$objPHPExcel->setActiveSheetIndex(0);

if($debug){
	die("DONE");	
}

// Redirect output to a client’s web browser (Excel2007)
ini_set('zlib.output_compression','Off');
header('Pragma: public');
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");                  // Date in the past
//header('Last-Modified: '.gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');     // HTTP/1.1
header('Cache-Control: pre-check=0, post-check=0, max-age=0');    // HTTP/1.1
header ("Pragma: no-cache");
header("Expires: 0");
header('Content-Transfer-Encoding: none');
header('Content-Type: application/vnd.ms-excel;');                 // This should work for IE & Opera
header("Content-type: application/x-msexcel");                    // This should work for the rest
header('Content-Disposition: attachment; filename="Tool Calibrations '.date(savefdt,time()+$_SESSION['tz_offset_sec']).'.xls"');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');

ob_end_flush();

function search($needle, $haystack){
	foreach($haystack as $id => $val)
	{
		if($val['system_id'] == $needle) {
			return $id;			
		}
	}
}
exit("DONE");
?>