<?php
//Update Completed 12/09/2014
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

//Get requests
$systems = array();
$sql=<<<SQL
DROP TEMPORARY TABLE IF EXISTS engineer_assignments;

CREATE TEMPORARY TABLE engineer_assignments 
SELECT sbc.system_id, sbc.nickname, 
(SELECT uu.name FROM systems_assigned_pri AS apri LEFT JOIN users AS uu ON uu.uid = apri.uid WHERE apri.system_ver_unique_id=sbc.ver_unique_id ) AS engineer_pri,
(SELECT uu.name FROM systems_assigned_sec AS asec LEFT JOIN users AS uu ON uu.uid = asec.uid WHERE asec.system_ver_unique_id=sbc.ver_unique_id ) AS engineer_sec
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sbc.unique_id = sb.unique_id
WHERE sbc.property = 'C';

DELETE FROM engineer_assignments WHERE system_id = '9999';

SELECT system_id, nickname, engineer_pri, engineer_sec 
FROM engineer_assignments
WHERE engineer_pri ='Unassigned'
OR engineer_pri =''
OR engineer_pri IS NULL
OR engineer_sec = 'Unassigned'
OR engineer_sec = ''
OR engineer_sec IS NULL
ORDER BY system_id ASC;

DROP TEMPORARY TABLE IF EXISTS engineer_assignments;
SQL;

if (mysqli_multi_query($mysqli,$sql)) {
    do {
        if ($result = mysqli_store_result($mysqli)) {
			if(sizeof($result) != 0){
				while ($row = mysqli_fetch_row($result)) {
					//printf($row[2]);
					array_push($systems,$row);
					//echo EOL;
				}
				mysqli_free_result($result);
			}
        }
        if (mysqli_more_results($mysqli)) {
            //printf("-----------------\n");
        }
    } while (mysqli_next_result($mysqli));
}else{
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

mysqli_close($mysqli);

if(empty($systems)){
	$no_report = true;
}else{
	$no_report = false;
}

// Include PHPExcel
require_once ($_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/Classes/PHPExcel.php');

// Include PHPExcel_IOFactory
require_once ($_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/Classes/PHPExcel/IOFactory.php');

$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory_serialized;

PHPExcel_Settings::setCacheStorageMethod($cacheMethod);

// Check if template exists
$template_file = $_SERVER['DOCUMENT_ROOT'].'/resources/report_templates/unassigned_systems.xls';
if (!file_exists($template_file)) {
	$log->logerr($template_file,1045);
	exit("Error the template is missing." . EOL);
}

// Create new PHPExcel object
//echo "Start PHPExcell", EOL;
$objPHPExcel = new PHPExcel();

//echo "Load from Excel2007 template file", EOL;
$objPHPExcel = PHPExcel_IOFactory::load($template_file);


$sharedStyle2 = new PHPExcel_Style();

$sharedStyle2->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_PATTERN_GRAY0625,
								'color'		=> array('argb' => 'FFFF0000')
							),
		 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
		 
));

//Select ct sheet
$objPHPExcel->setActiveSheetIndex(0);

//Set report date
$objPHPExcel->getActiveSheet()->setCellValue('b3', date(phpdispfdt,time()+$_SESSION['tz_offset_sec']));

//create array of excel columns
$letters = array();
foreach (range('A', 'D') as $letter) {
    //echo $letter;
	array_push($letters,$letter);
}

//add rows
$_num = 5;
if(!$no_report){
	$last_key = end(array_keys($systems));
	foreach($systems as $systemkey => $system){
		foreach($letters as $letterkey => $letter){		
			$objPHPExcel->getActiveSheet()->setCellValue($letter . $_num, $system[$letterkey]);
		}		
		$_num++;
	}
}else{
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$_num, 'No report available');	
}

// Redirect output to a client’s web browser (Excel2007)
ini_set('zlib.output_compression','Off');
header('Pragma: public');
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");                  // Date in the past
//header('Last-Modified: '.gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');     // HTTP/1.1
header('Cache-Control: pre-check=0, post-check=0, max-age=0');    // HTTP/1.1
header ("Pragma: no-cache");
header("Expires: 0");
header('Content-Transfer-Encoding: none');
header('Content-Type: application/vnd.ms-excel;');                 // This should work for IE & Opera
header("Content-type: application/x-msexcel");                    // This should work for the rest
header('Content-Disposition: attachment; filename="Un-Assigned Systems Report '.date(savefdt,time()+$_SESSION['tz_offset_sec']).'.xls"');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');

ob_end_flush();

function search($needle, $haystack){
	foreach($haystack as $id => $val)
	{
		if($val['system_id'] == $needle) {
			return $id;			
		}
	}
}
exit("DONE");
?>