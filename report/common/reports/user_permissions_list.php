<?php
//Update Completed 12/12/14
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

//Get User Permissions
$users = array();
$sql="CALL `users_data_get_all`();";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

while($row = $result->fetch_assoc()){
	$users[$row['uid']] = $row;
}

//Clear the remaining results from the above mysql call statement
while ($mysqli->next_result());

//echo "<pre>",print_r($users),"</pre>",EOL;
echo "<a name='top'></a>",EOL;
echo "<h3>Active Users</h3>",EOL;
$i = 0;
foreach($users as $uid=>$values){
	if(strtolower($values['active']) == 'y'){
		if($i == 10){
			echo EOL;
			$i = 0;	
		}
		echo "<a href='#$uid'>$uid</a>&nbsp;&nbsp;";
		$i++;
	}
}
echo EOL,EOL,"<hr>",EOL;
echo "<h3>Inactive Users</h3>",EOL;
$i = 0;
foreach($users as $uid=>$values){
	if(strtolower($values['active']) == 'n'){
		if($i == 10){
			echo EOL;
			$i = 0;	
		}
		echo "<a href='#$uid'>$uid</a>&nbsp;&nbsp;";
		$i++;
	}
}


echo EOL,EOL,"<hr>",EOL,EOL;
foreach($users as $uid=>$values){
	echo "<a name='$uid'></a>",$uid;
	if(strtolower($values['active']) == 'n'){
		echo '&emsp;&ndash;INACTIVE',EOL;	
	}else{
		echo EOL;
	}
	foreach($values as $grp=>$value){
		if(preg_match("/grp_/i",$grp)){
			if(strtolower($value) == 'y'){
				echo "&emsp;&#43;",$grp,EOL;
			}
		}
	}
	foreach($values as $perm=>$value){
		if(preg_match("/perm_/i",$perm)){
			if(strtolower($value) == 'y'){
				echo "&emsp;&ndash;",$perm,EOL;
			}
		}
	}
	echo "<a href='#top'>_TOP_</a>",EOL;
	echo EOL;
}

?>
