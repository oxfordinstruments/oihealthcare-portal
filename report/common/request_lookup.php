<?php 
//Update Completed 11/25/14
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();


if(!isset($_GET['system_id'])){
	$log->logerr('request_lookup.php',1022,true,basename(__FILE__),__LINE__);	
}

$sql="SELECT r.*, u.name AS engineer_name
FROM systems_reports AS r
LEFT JOIN users AS u ON u.uid = r.engineer
WHERE r.system_id = '".$_GET['system_id']."'
ORDER BY r.id
LIMIT 50;";
if(!$resultReports = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

?>
<!doctype html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<style>
.dataTable th, .dataTable td {
	max-width: 200px;
	min-width: 70px;
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
}
</style>
<?php require_once($js_include);?>

<script type="text/javascript">
$(document).ready(function() {
	allTable = $('#allTable').dataTable({
		"bJQueryUI": true,
		"bStateSave": false,
		"sPaginationType": "full_numbers",
		"fnInitComplete": function () {
			 this.$('tr').click( function () {
				 var href = $(this).find("a").attr("href");
				 if(href) {
					window.location = href;
				 }
			 });
		},
		"fnDrawCallback" : function(oSettings) {
			var total_count = oSettings.fnRecordsTotal();
			var columns_in_row = $(this).children('thead').children('tr').children('th').length;
			var show_num = oSettings._iDisplayLength;
			var tr_count = $(this).children('tbody').children('tr').length;
			var missing = show_num - tr_count;
			if (show_num < total_count && missing > 0){
				for(var i = 0; i < missing; i++){
					$(this).append('<tr class="space"><td colspan="' + columns_in_row + '">&nbsp;</td></tr>'); 
				}
			}
			if (show_num > total_count) {
				for(var i = 0; i < (total_count - tr_count); i++) {
					$(this).append('<tr class="space"><td colspan="' + columns_in_row + '">&nbsp;</td></tr>'); 
				}
			}
		}
	});	
});
function select_id(id){
	$('#repeat_call_id',top.document).val(id);
	parent.$.fancybox.close();
}
</script>
</head>
<body>
 <table width="100%" id="allTable">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Report ID</th>
                        <th>Engineer</th>
                        <th>Complaint</th>
                        <th>Service</th>
                        <th>Report Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
						while($rowReports = $resultReports->fetch_assoc())
						{
							echo "<tr onclick=\"javascript: select_id(".$rowReports['report_id'].");\">\n";	
							echo "<td>". date(phpdispfd,strtotime($rowReports['date']))."</td>\n";	
							echo "<td>". $rowReports['report_id']."</td>\n";
							echo "<td>". $rowReports['engineer_name']."</td>\n";
							echo "<td>". $rowReports['complaint']."</td>\n";
							echo "<td>". $rowReports['service']."</td>\n";
							echo "<td>". $rowReports['status']."</td>\n";
							echo "</tr>\n";
						}
						?>     
                </tbody>
            </table>
</body>
</html>