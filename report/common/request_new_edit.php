<?php
//Update Completed 04/02/2015
$debug = false;
if(isset($_GET['debug'])){
	$debug = true;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/moment_php/Moment.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/resources/moment_php/MomentException.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/resources/moment_php/MomentHelper.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/resources/moment_php/MomentLocale.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/resources/moment_php/MomentFromVo.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/php_utils.php');
$php_utils = new phpUtils(NULL, $debug);

unset($_SESSION['service_request_refresh']);

$probrpt_word_limt	 = intval($settings->probrpt_word_limit);

$edit_request = false;
$journal_only = false;
if(isset($_GET['e'])){
	$edit_request=true;
	if(isset($_GET['req_id'])){
		$req_id = $_GET['req_id']; //request unique id
	}else{
		$log->logerr('request_new_edit.php',1024);
		header("Location:/error.php?n=1024&p=request_new_edit.php");
	}
}elseif(isset($_GET['j'])){
	$edit_request = true;
	$journal_only = true;
	if(isset($_GET['req_id'])){
		$req_id = $_GET['req_id']; //request unique id
	}else{
		$log->logerr('request_new_edit.php',1024);
		header("Location:/error.php?n=1024&p=request_new_edit.php");
	}
}

$unique_id = md5(uniqid()); //Create a new unique id for request. If editing then update the var later

$view = false;
if(isset($_GET['v'])){
	$view = true;	
}

if(isset($_GET['user_id'])){
	$user_id=$_GET['user_id'];
}else{
	$log->logerr('Blank User ID',1017,true,basename(__FILE__),__LINE__);
}
if(isset($_GET['ver_unique_id'])){
	$ver_unique_id=$_GET['ver_unique_id']; //system ver unique id
}else{
	$log->logerr('Blank Unique ID',1016,true,basename(__FILE__),__LINE__);
}
if(isset($_GET['unique_id'])){
	$sys_unique_id=$_GET['unique_id']; //system unique id
}else{
	$log->logerr('Blank Unique ID',1016,true,basename(__FILE__),__LINE__);
}
if(isset($_GET['version'])) {
	$version = $_GET['version'];	
}else{
	$log->logerr('Blank Version',4,true,basename(__FILE__),__LINE__);
}

$pm=false;
if(isset($_GET['pm'])){
	$pm=true;
}

if($edit_request){
	$sql="SELECT sr.*, u.name AS dispatch_name 
	FROM systems_requests AS sr
	LEFT JOIN users AS u ON u.uid = sr.sender
	WHERE unique_id = '".$req_id."';";
	if(!$resultReq = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$rowReq = $resultReq->fetch_assoc();
	d($rowReq);
	
	$unique_id = $rowReq['unique_id'];
	d($unique_id);
}

$sql="SELECT sb.*, sbc.*
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sbc.unique_id = sb.unique_id
WHERE sbc.ver_unique_id = '".$ver_unique_id."';";
if(!$resultSystem = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowSystem = $resultSystem->fetch_assoc();
d($rowSystem);

$archived_system_error = false;
if(strtolower($rowSystem['property']) == 'a'){
	//$log->logerr("Cannot create request for archived system",0,true,basename(__FILE__),__LINE__);
	$journal_only = true;
	$archived_system_error = true;
}

$sql="SELECT sn.id, sn.note, u.name, sn.date
FROM systems_notes AS sn
LEFT JOIN users AS u ON u.uid = sn.uid
WHERE sn.system_unique_id = '".$rowSystem['unique_id']."';";
if(!$resultSystemNotes = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
d($sql);

$sql="SELECT * FROM facilities WHERE unique_id = '".$rowSystem['facility_unique_id']."';";
if(!$resultFacility = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowFacility = $resultFacility->fetch_assoc();
d($rowFacility);

$sql="SELECT notes FROM customers WHERE unique_id = '".$rowFacility['customer_unique_id']."';";
if(!$resultCustomer = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowCustomer = $resultCustomer->fetch_assoc();
d($rowCustomer);

$sql="SELECT * FROM misc_contracts WHERE `id` = '".$rowSystem['contract_type']."' LIMIT 1;";
if(!$resultContract = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowContract = $resultContract->fetch_assoc();

$sql="SELECT * FROM misc_contracts;";
if(!$resultContracts = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

$sql="SELECT * FROM systems_types WHERE `id` = '".$rowSystem['system_type']."' LIMIT 1;";
if(!$resultEquipment = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowEquipment = $resultEquipment->fetch_assoc();

$sql="SELECT * FROM systems_status;";
if(!$resultStatus = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

$sql="SELECT ur.uid, u.name
FROM users_roles AS ur
LEFT JOIN users_role_id AS rid ON rid.id = ur.rid
LEFT JOIN users AS u ON u.uid = ur.uid
WHERE (rid.role = 'role_engineer' OR rid.role = 'role_contractor') AND u.active = 'y' AND u.id > 9
ORDER BY u.name ASC;";
if(!$resultEngineer = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

$sql="SELECT a.uid,u.name,u.email,u.cell,c.address
FROM systems_assigned_pri AS a 
LEFT JOIN users AS u ON a.uid = u.uid 
LEFT JOIN misc_carrier AS c ON c.carrierid = u.carrier 
WHERE a.system_ver_unique_id='".$rowSystem['ver_unique_id']."'
ORDER BY u.name ASC;";
if(!$resultEngPri = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowEngPri = $resultEngPri->fetch_assoc();
d($rowEngPri);

$sql="SELECT a.uid,u.name,u.email,u.cell,c.address
FROM systems_assigned_sec AS a 
LEFT JOIN users AS u ON a.uid = u.uid 
LEFT JOIN misc_carrier AS c ON c.carrierid = u.carrier 
WHERE a.system_ver_unique_id='".$rowSystem['ver_unique_id']."'
ORDER BY u.name ASC;";
if(!$resultEngSec = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowEngSec = $resultEngSec->fetch_assoc();
d($rowEngSec);

$sql="SELECT COUNT(*) AS `count` 
FROM systems_requests 
WHERE status='open' 
AND `deleted` = 'n' 
AND system_id='".$resultSystem->fetch_object()->system_id."';";
if(!$resultOpenRequestCount=$mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

$numResultPrevNotes = 0;
$sql="SELECT notes, unique_id 
FROM systems_reports 
WHERE system_id = '".$rowSystem['system_id']."' 
ORDER BY id DESC LIMIT 1;";
if(!$resultPrevNotes = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowPrevNotes = $resultPrevNotes->fetch_assoc();
$numResultPrevNotes = $resultPrevNotes->num_rows;
d($rowPrevNotes);
d($numResultPrevNotes);

$sql="UPDATE users SET timezone_reminder = timezone_reminder + 1 WHERE uid = '$myusername';";
if(!$resultTzRemind = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$sql="SELECT timezone_reminder FROM users WHERE uid = '$myusername';";
if(!$resultTzRemind = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$timezone_reminder = true;
if($resultTzRemind->fetch_object()->timezone_reminder >= $settings->timezone_reminder_max){
	$timezone_reminder = false;
}

$facility_timezone_utc = false;
if($rowFacility['timezone'] == '' or strtolower($rowFacility['timezone']) == 'utc'){
	$facility_timezone = 'UTC';
	$facility_timezone_utc = true;	
}else{
	$facility_timezone = $rowFacility['timezone'];	
}
$t=time() + $php_utils->tz_offset_sec($facility_timezone);

$facility_timezone_offset_sec = $php_utils->tz_offset_sec($facility_timezone);

$warranty_warn = false;
$contract_warn = false;
$now = new Moment\Moment('now', null, true);
try{
	if($rowSystem['warranty_end_date'] != ''){
		$warranty_end_date = new \Moment\Moment($rowSystem['warranty_end_date'], null, true);
		if($warranty_end_date->isBefore($now)){
			$warranty_warn = true;
		}
	}
}catch(\Moment\MomentException $momentException){
	d($momentException);
}
try{
	if($rowSystem['contract_end_date'] != ''){
		$contract_end_date = new Moment\Moment($rowSystem['contract_end_date'], null, true);
		if($contract_end_date->isBefore($now)){
			$contract_warn = true;
		}
	}
}catch(\Moment\MomentException $momentException){
	d($momentException);
}
d($warranty_warn);
d($contract_warn);

function tz_list() {
	$zones_array = array();
	foreach(timezone_identifiers_list() as $key => $zone) {
		date_default_timezone_set($zone);
		$zones_array[$key]['zone'] = $zone;
		$zones_array[$key]['diff_from_GMT'] = 'UTC/GMT ' . date('P', time());
	}
	return $zones_array;
}

function limit_words($string, $word_limit){
	$words = explode(" ",$string);
	if(count($words) > $word_limit){
		return implode(" ",array_splice($words,0,$word_limit))."...(more)";
	}else{
		return implode(" ",array_splice($words,0,$word_limit));
	}
}
?>
<!DOCTYPE html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<link rel="stylesheet" type="text/css" href="/resources/css/portal.css">
<link href="/resources/css/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" href="/resources/css/form_request.css">

<?php require_once($js_include);?>

<script src="/resources/js/jquery.ui.datepicker.js"></script>
<script src="/resources/js/jquery-ui-timepicker-addon.js"></script>
<script src="/resources/js/jquery-dynamic-form_MODIFIED.js"></script>
<script type="text/javascript">
var num = "";
var $checkVar = "";
var $addCheckVar = "";
$(document).ready(function(){
	$('.findme').each(function()
	{
	   var currentId = $(this).attr('id');
	   num = currentId.replace("request_date", "");
	});
/////////////////////////////////////////////////////////////////////////////////////		
		/*todo-evo Remove this block from all pages*/
		<?php
		if(strtolower($_SESSION['prefs']['pref_show_help']) != 'y' or $_SESSION['mobile_device'] == true){
		?>
		//$('#tiplayer').hide()
		<?php
			}
		?>
/////////////////////////////////////////////////////////////////////////////////////			
		<?php 
		if(!$edit_request){
			if(intval($resultOpenRequestCount->fetch_object()->count) > 0){
				if(intval($resultOpenRequestCount->fetch_object()->count) == 1){
					echo "alert(\"There is one other request open for this system already!\")";
				}else{
					echo "alert(\"There are ".$resultOpenRequestCount->fetch_object()->count." other requests open for this system already.\")";
				}
			} 
		}
		?>
/////////////////////////////////////////////////////////////////////////////////////

	$(".button_jquery_save").button({
		icons: {
			primary: "ui-icon-disk"
		}
	});
	
	$(".button_jquery_delete").button({
		icons: {
			primary: "ui-icon-closethick"
		}
	});
	
	$(".button_jquery_new").button({
		icons: {
			primary: "ui-icon-document"
		}
	});	
	
	$(".button_jquery_note").button({
		icons: {
			primary: "ui-icon-note"
		}
	});	
	
	$("select").chosen({
		no_results_text: "Oops, nothing found!",
		disable_search_threshold: 10,
		placeholder_text_single: '  '
	}); 
	
	$(".text_toggle").focus(function() {
		$(this).animate({height: 104}, "normal");
	}).blur(function() {
		$(this).animate({height: 15}, "normal");
	});
/////////////////////////////////////////////////////////////////////////////////////		
});

$(document).ready(function(){
	pm_date_enable_disable();

	var timezone = "<?php echo $facility_timezone; ?>";
	var timezone_utc = <?php if($facility_timezone_utc){echo 'true';}else{echo 'false';} ?>;
	var update_zone = 'NA';
	var tz_reminder = <?php if($timezone_reminder){echo 'true';}else{echo 'false';} ?>;
	var view = <?php if($view){echo 'true';}else{echo 'false';} ?>;
	if(!view){
		if(timezone_utc){
			$("#create_edit_btn").remove();
			$("#delete_btn").remove();
			var statestz = {
				state0: {
					title: 'Timezone Error!',
					html:'<h2>The facility timezone is not set!</h2><br>'+
						 '<h3>Click next to update the facilities timezone and reload the page.</h3>',
					buttons: { Next: 1 },
					focus: 1,
					submit:function(e,v,m,f){ 
						e.preventDefault();
						$.prompt.goToState('state1');
					}
				},
				state1: {
					title: 'Select Timezone',
					html:  	"<table><tr><td align='right'>Search List</td>"+
							"<td><input type='text' id='tzSearch' /></td></tr>"+
							"<tr><td align='right'>Timezone List</td>"+
							"<td><select id='zones' name='zone'><option value='0'>No Timezone</option>"+
							<?php foreach(tz_list() as $tzl){ ?>
								"<option value='<?php echo $tzl['zone']; ?>'><?php echo $tzl['diff_from_GMT'] . ' - ' . str_replace('_',' ',$tzl['zone']); ?></option>"+
							<?php } ?>
							"</select></td></tr></table>",
					buttons: { Update: 1 },
					focus: 1,
					submit:function(e,v,m,f){ 
						if(v==1){
							if(f['zone']=='0'){
								$.prompt.goToState('state2');
							}else{
								update_zone = f['zone'];
								update_facility_timezone('<?php echo $rowFacility['unique_id']; ?>',update_zone)
								$.prompt.close();
							}
						}
						e.preventDefault();
					}
				},
				state2: {
					title: 'No Zone',
					html:  	"<h2>No Timezone Selected</h2><br>"+
							"<h3>You have selected no timezone. Please go back and select a timezone for the facility.<h3>",
					buttons: {Back: -1 },
					focus: 1,
					submit:function(e,v,m,f){ 
						e.preventDefault();
						$.prompt.goToState('state1');
					}
				}
			};
			$.prompt(statestz)
				.on('impromptu:statechanged', function(e,v,m,f){
					$('#zones').filterByText($('#tzSearch'), true);
				});
		}else{
			if(tz_reminder){
				$.prompt("<h3>Please remember to ask the caller to give times in the facilities timezone.</h3>",
				{
					title: "Reminder"	
				});	
			}
		}
	}
	
	var notesTableDT = $('#notesTable')
	.on('xhr.dt', function ( e, settings, json, xhr ) {
        //console.log( json );
		if(json['data'].length == 0){
			$("#notesDiv").hide();	
		}else{
			$("#notesDiv").show();	
		}
    })
	.dataTable({
		"bJQueryUI": true,
		"searching": false,
		"lengthChange": false,
		"sPaginationType": "full_numbers",
		"aaSorting": [],
		"ajax": {
			'type': 'POST',
			'url': 'systems_service_journal.php',
			'data': {
			   get: 'true',
			   unique_id: '<?php echo $rowReq['unique_id'] ?>'
			}
		}
	});

	var systemNotesTableDT = $('#systemNotesTable')
		.on('xhr.dt', function ( e, settings, json, xhr ) {
			//console.log( json );
			if(json['data'].length == 0){
				$("#systemNotesDiv").hide();
			}else{
				$("#systemNotesDiv").show();
			}
		})
		.dataTable({
			"bJQueryUI": true,
			"searching": false,
			"lengthChange": false,
			"sPaginationType": "full_numbers",
			"aaSorting": [],
			"autoWidth": false,
			"ajax": {
				'type': 'POST',
				'url': 'systems_notes.php',
				'data': {
					method: 'get',
					system_unique_id: '<?php echo $rowSystem['unique_id']; ?>'
				}
			}
		});


});

function testme(el){
	console.log(el);
	console.log($(el).prev().val());
}
function pm_date_enable_disable(){
	var pm_val = $("#pm"+" option:selected").text();
	if(pm_val.toLowerCase() == "yes"){
		$("#pm_date").removeAttr("disabled");
	}else{
		$("#pm_date").val("");
		$("#pm_date").attr("disabled","true");
	}
}

/////////////////////////////////////////////////////////////////////////////////////
function change_facility_timezone(){
	var timezone = "<?php echo $facility_timezone; ?>";
	var update_zone = 'NA';
	var statestz = {
		state0: {
			title: 'Timezone Change',
			html:'<h2>The facility timezone is ' + timezone + '</h2><br>'+
				 '<h3>Click next to update the facilities timezone and reload the page.</h3>',
			buttons: { Next: 1 },
			focus: 1,
			submit:function(e,v,m,f){ 
				e.preventDefault();
				$.prompt.goToState('state1');
			}
		},
		state1: {
			title: 	'Select Timezone',
			html:  	"<table><tr><td align='right'>Search List</td>"+
					"<td><input type='text' id='tzSearch' /></td></tr>"+
					"<tr><td align='right'>Timezone List</td>"+
					"<td><select id='zones' name='zone'><option value='0'>No Timezone</option>"+
					<?php foreach(tz_list() as $tzl){ ?>
						"<option value='<?php echo $tzl['zone']; ?>'><?php echo $tzl['diff_from_GMT'] . ' - ' . str_replace('_',' ',$tzl['zone']); ?></option>"+
					<?php } ?>
					"</select></td></tr></table>",
			buttons: { Update: 1 },
			focus: 1,
			submit:function(e,v,m,f){ 
				if(v==1){
					if(f['zone']=='0'){
						$.prompt.goToState('state2');
					}else{
						update_zone = f['zone'];
						update_facility_timezone('<?php echo $rowFacility['unique_id']; ?>',update_zone)
						$.prompt.close();
					}
				}
				e.preventDefault();
			}
		},
		state2: {
			title: 'No Zone',
			html:  	"<h2>No Timezone Selected</h2><br>"+
					"<h3>You have selected no timezone. Please go back and select a timezone for the facility.<h3>",
			buttons: {Back: -1 },
			focus: 1,
			submit:function(e,v,m,f){ 
				e.preventDefault();
				$.prompt.goToState('state1');
			}
		}
	};
	$.prompt(statestz)
		.on('impromptu:statechanged', function(e,v,m,f){
			$('#zones').filterByText($('#tzSearch'), true);
		});
}//end func

function update_facility_timezone(unique_id,tzone){
	$.ajax({
		type: 'POST',
		url: 'scripts/change_facility_timezone.php',
		data: { id: unique_id, t: tzone },
		success:function(data){
			console.log("ajax return");
			console.log(data);
			if(parseInt(data) == 999){
				$.prompt("<h3>Facility Timezone Update Failed. Call Support.</h3>", {
					title: "Error"
				});
			}
			if(parseInt(data) == 666){
				$.prompt("<h3>This system is not linked to a facility.<br>Please edit the system data and select the apporiate facility.<br>"+
						 "You will not be able to create/update/delete a service request until the system is linked to a facility.</h3>", 
				{
					title: "Error"
				});	
				$("#create_edit_btn").remove();
			}
			if(parseInt(data) < 10){
				location.reload();	
			}
			
		}
	});	
}

/////////////////////////////////////////////////////////////////////////////////////	
$(function() {
	$( "#onsite_date" ).datepicker({
		numberOfMonths: 1,
		showButtonPanel: true,
		changeYear: false,
		duration: "fast",
		controlType: 'select',
		constrainInput: true,
		dateFormat: "<?php echo dpdispfd; ?>"
	});
	$( "#initial_call_date" ).datetimepicker({
		numberOfMonths: 1,
		showButtonPanel: true,
		changeYear: false,
		duration: "fast",
		controlType: 'select',
		constrainInput: true,
		maxDate: "<?php echo date(phpdispfd,$t); ?>",
		dateFormat: "<?php echo dpdispfd; ?>",
		timeFormat: "<?php echo dpdispft; ?>"
	});
	$( "#response_date" ).datetimepicker({
		numberOfMonths: 1,
		showButtonPanel: true,
		changeYear: false,
		duration: "fast",
		controlType: 'select',
		constrainInput: true,
		maxDate: "<?php echo date(phpdispfd,$t); ?>",
		dateFormat: "<?php echo dpdispfd; ?>",
		timeFormat: "<?php echo dpdispft; ?>"
	});
	$( "#pm_date" ).datetimepicker({
		numberOfMonths: 1,
		showButtonPanel: true,
		constrainInput: true,
		changeYear: true,
		controlType: 'select',
		duration: "fast",
		dateFormat: "<?php echo dpdispfd; ?>",
		timeFormat: "<?php echo dpdispft; ?>"
	});
});

jQuery.fn.filterByText = function(textbox, selectSingleMatch) {
  return this.each(function() {
    var select = this;
    var options = [];
    $(select).find('option').each(function() {
      options.push({value: $(this).val(), text: $(this).text()});
    });
    $(select).data('options', options);
    $(textbox).bind('change keyup', function() {
      var options = $(select).empty().scrollTop(0).data('options');
      var search = $.trim($(this).val());
      var regex = new RegExp(search,'gi');
 
      $.each(options, function(i) {
        var option = options[i];
        if(option.text.match(regex) !== null) {
          $(select).append(
             $('<option>').text(option.text).val(option.value)
          );
        }
      });
      if (selectSingleMatch === true && 
          $(select).children().length === 1) {
        $(select).children().get(0).selected = true;
      }
    });
  });
};
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function submitcheck(param){
	
	ids = [];
	errors = [];
	
	var dtpattern = /^(19|20)\d\d-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01]) [0-2]?[0-9]:[0-5][0-9](?![\s|\D|\d])/; //2014-12-31 23:58
	var datere = /^20(1|2)\d-((1[0-2])|(0[1-9]))-(([0-2][0-9])|3(0|1))$/; //date yyyy-mm-dd
	
	if(param=='del'){
		$.prompt("Are you sure you want to delete this service request?",
		{
			title: 'Delete Request',
			buttons: { "Yes, I'm Sure": true, "No": false },
			submit: function(e,v,m,f){
				//e.preventDefault();
				console.log(v);
				if(!v){
					$.prompt.close();
				}else{
					$.prompt.close();
					document.getElementById("del").value="Y";
					document.forms["form"].submit();
				}
			}
		});
		return;
	}

	var journal_only = false;
	if(param == 'jou'){
		journal_only = true;
		$("#journal_only").attr("checked",true);
		document.forms["form"].submit();
		return;
	}

	var initDT = moment(document.getElementById("initial_call_date").value,'YYYY-M-D H:m');
	var responseDT = moment(document.getElementById("response_date").value,'YYYY-M-D H:m');
	var onsiteDT = moment(document.getElementById("onsite_date").value + ' 23:59','YYYY-M-D H:m');
	var diffMins = responseDT.diff(initDT, 'minutes');
	
	if($('#engineer').val()==""){ids.push("#engineer_chosen"); errors.push(" Assigned Service Engineer");}
	if($('#problem_reported').val()==""){ids.push("#problem_reported"); errors.push("Problem Reported");}
	if($('#request_num').val()==""){ids.push("#request_num"); errors.push("Service Request Number");}
	<?php if($edit_request){ ?>
		if($('#cur_system_status').val()==""){ids.push("#cur_system_status_chosen"); errors.push("Curent System Status");}
	<?php }else{ ?>
		if($('#init_system_status').val()==""){ids.push("#init_system_status_chosen"); errors.push("Initial System Status");}
	<?php } ?>
	if($('#pm').val()==""){ids.push("#pm_chosen"); errors.push("Will a PM be done for this request?");}
	if($('#refrig_service').val()==""){ids.push("#refrig_service_chosen"); errors.push("Will refrigeration service be done for this request?");}
	if($('#initial_call_date').val()==""){ids.push("#initial_call_date"); errors.push("Initial Call Date/Time");}
	if($('#response_date').val()==""){ids.push("#response_date"); errors.push("Response Date/Time");}
	if($('#onsite_date').val()==""){ids.push("#onsite_date"); errors.push("Engineer Onsite Date");}
	if($('#fbc').val()==""){ids.push("#fbc_chosen"); errors.push("Indicate customer complaint");}
	
	var inp_val = $('#initial_call_date').val();
	if(!dtpattern.test(inp_val)){ids.push("#initial_call_date"); errors.push("Initial Call Date/Time Invalid");}
	
	var inp_val = $('#response_date').val();
	if(!dtpattern.test(inp_val)){ids.push("#response_date"); errors.push("Response Date/Time Invalid");}
	
	var inp_val = $('#onsite_date').val();
	if(!datere.test(inp_val)){ids.push("#onsite_date"); errors.push("Engineer Onsite Date Invalid");}

	var pm_val = $("#pm"+" option:selected").text();
	if(pm_val.toLowerCase() == "yes") {
		var pmDT = moment(document.getElementById("pm_date").value, 'YYYY-M-D H:m');
		var inp_val = $('#pm_date').val();
		if (!dtpattern.test(inp_val)) {
			ids.push("#pm_date");
			errors.push("PM Scheduled Date/Time Invalid");
		}
		if(pmDT.isAfter(onsiteDT, 'day') || pmDT.isBefore(onsiteDT, 'day')){
			ids.push("#pm_date");
			ids.push("#onsite_date");
			errors.push("PM Scheduled Date must be equal to Engineer Onsite Date");
		}
	}
	
	if(responseDT.isBefore(initDT)){ 
		ids.push("#response_date"); 
		ids.push("#initial_call_date");
		errors.push("Response Date/Time is before Initial Call Date/Time");
	}
	
	if(onsiteDT.isBefore(initDT)){

		ids.push("#onsite_date"); 
		ids.push("#initial_call_date");
		errors.push("Onsite Date/Time is before Initial Call Date/Time");
	}
	
	if($('#customer_actions').val()==""){
		$('#customer_actions').val("None");
	}

	console.log("ids: " + ids);
	console.log("errors: " + errors);
	showErrors(ids,errors);

	if($("#po_number").val().length > 0){
		$("#invoice_req").val('Y');
		$("#invoice_req").trigger("chosen:updated");
	}

	if(ids.length <= 0){
		$("#errors").hide();
		if(document.getElementById("initial_call_date").value == document.getElementById("response_date").value){
			$.prompt("<h3>The Initial Call Date and Response Date are the same.<br>"+
					 "This should only happen when the engineer took the call directly from the customer.<br>"+
					 "Are you sure this is correct?</h3>",
			{
				title: 'Dates/Times Same',
				buttons: { "Yes, <?php if(!$edit_request){echo "submit";}else{echo "update";} ?> Request": true, "No": false },
				submit: function(e,v,m,f){
					//e.preventDefault();
					console.log(v);
					if(!v){
						$.prompt.close();
					}else{
						$.prompt.close();
						document.forms["form"].submit();
					}
				}
			});
		}else if(diffMins > 30){
			$.prompt("<h3>WHOA THERE!!!<br>"+
			"The response time is greater than 30 minutes.<br>"+
			"Are you sure you want to disregard KPI #1 ?</h3>",
			{
				title: 'KPI #1 FAIL',
				buttons: { "Yes, <?php if(!$edit_request){echo "submit";}else{echo "update";} ?> Request": true, "No": false },
				submit: function(e,v,m,f){
					//e.preventDefault();
					console.log(v);
					if(!v){
						$.prompt.close();
					}else{
						$.prompt.close();
						document.forms["form"].submit();
					}
				}
			});
		}else{
			$.prompt("Are you sure you want to <?php if(!$edit_request){echo "submit";}else{echo "update";} ?> this service request?",
			{
				title: '<?php if(!$edit_request){echo "submit";}else{echo "update";} ?> Request',
				buttons: { "Yes, I'm Sure": true, "No": false },
				submit: function(e,v,m,f){
					//e.preventDefault();
					console.log(v);
					if(!v){
						$.prompt.close();
					}else{
						$.prompt.close();
						document.forms["form"].submit();
					}
				}
			});				
		}
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function checkDateTime(dt){
	// 12/12/2014 23:59
	console.log(dt);
	var date_re = RegExp('/^(19|20)\d\d-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01]) [0-2]?[0-9]:[0-5][0-9](?![\s|\D|\d])/'); //2014-12-31 23:58
	if(date_re.test(dt) == false){
		console.log("Date Check Failed");
		return false;
	}	
	console.log("Date Check Passed");
	return true;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function show_lookup_repeat() {
	if(document.getElementById("repeat_call").value.toLowerCase() == "y"){
		document.getElementById("add_repeat_lable").style.display = "";	
		document.getElementById("add_repeat_button").style.display = "";	
	}else{
		document.getElementById("add_repeat_lable").style.display = "none";	
		document.getElementById("add_repeat_button").style.display = "none";	
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function lookup_repeat_call() {
	alert("Not finished yet");
}
/////////////////////////////////////////////////////////////////////////////////////
function showErrors(ids,errors){
	//thin solid #2C3594
	$("input, select, .chosen-container").each(function(index, element) {
		$(this).animate({
			borderColor: "#2C3594",
			boxShadow: 'none'
		});
	});
	
	$.each(ids,function(index,value){
		$(value).animate({
			borderColor: "#cc0000",
			boxShadow: '0 0 5px 3px rgba(255,0,0,0.4)'
		});
	});
	$("#errors > span").html("");
	$.each(errors,function(index, value){
		$("#errors > span").append(value + "<br>");
	});
	$("#errors").show('slow');
	$(document).scrollTop(0);
}
/////////////////////////////////////////////////////////////////////////////////////
function iframeNoteClosed(){
	$('#notesTable').DataTable().ajax.reload();
	$.ajax({
		type: 'POST',
		url: 'systems_status_get.php',
		data: { unique_id: '<?php echo $rowSystem['unique_id'] ?>' },
		success:function(data){
			console.log("ajax return");
			console.log(data);
			if(parseInt(data) == 0){
				console.log("ERROR: data = 0")
			}
			if(parseInt(data) > 0){
				$("#cur_system_status").val(parseInt(data));
				$("#cur_system_status").trigger("chosen:updated");
			}

		}
	});
}
</script>
</head>
<body>
<?php require_once($header_include); ?>
<div id="OIReportContent"> 
<div id="stylized" class="myform">
<form id="form" name="form" method="post" enctype='multipart/form-data' action="request_save.php">
<div id="srHeaderDiv">
	<h1><?php $warn = false; if($view){echo "View";}else{if(!$edit_request){echo "New";}else{echo "Edit";}} ?> Service Request</h1>
	<?php if(strtolower($rowSystem['credit_hold']) == "y"){ $warn = true; ?><br><h1 class="credit_hold">System is on Credit Hold. Inform Engineer!</h1><?php } ?>
	<?php if(strtolower($rowSystem['pre_paid']) == 'y'){ $warn = true; ?><br><h1 class="pre_paid">System is pre-paid service only. Inform Engineer!</h1><?php } ?>
	<?php if(strtolower($rowSystem['property']) == 'f'){ $warn = true; ?><br><h1 class="future">Caution: Future system</h1><?php } ?>
	<?php if($archived_system_error){ $warn = true; ?><br><h1 class="credit_hold">CONTACT SUPPORT! This system has been archived and there is an open service request!</h1><?php } ?>
	<?php if($warranty_warn){ $warn = true; ?><br><h1 class="credit_hold">Warranty Expired</h1><?php } ?>
	<?php if($contract_warn){ $warn = true; ?><br><h1 class="credit_hold">Contract Expired</h1><?php } ?>
	<?php if($warn){?><br><?php } ?>

</div>
<div id="main"><!-- do not remove -->
<div id="errors" style="text-align:center;display:none; margin-bottom:25px; margin-top:25px; font-size:18px;">
	<h2 style="margin:0px; padding:0px; font-size:24px;">Errors to fix</h2>
	<span style="color:#F00">
	</span>
</div>
<div id="srInfoDiv">
    <table id="srInfoTable" class="srTable">
        <tr>
          <td class="rowLabel">System&nbsp;ID: </td>
          <td class="rowData"><a href="systems_view.php?ver_unique_id=<?php echo $rowSystem['ver_unique_id']; ?>" target="_blank"><?php echo $rowSystem['system_id']; ?></a></td>
          <td class="rowLabel">System Nickname: </td>
          <td class="rowData"><?php echo $rowSystem['nickname']; ?></td>
        </tr>
		<tr>
		  <td class="rowLabel">Facility&nbsp;ID: </td>
          <td class="rowData"><?php echo $rowFacility['facility_id']; ?></td>
          <td class="rowLabel">Facility&nbsp;Name: </td>
          <td class="rowData"><?php echo $rowFacility['name']; ?></td>
		</tr>
        <tr>
          <td class="rowLabel">Facility&nbsp;Address: </td>
          <td colspan="4" class="rowData"><?php echo $rowFacility['address'] ."&nbsp;&nbsp;&nbsp;". $rowFacility['city'] .", ". $rowFacility['state'] ."  ". $rowFacility['zip'];?></td>
        </tr>
        <tr>
          <td class="rowLabel">Serial&nbsp;Number: </td>
          <td class="rowData"><?php echo $rowSystem['system_serial']; ?></td>
          <td class="rowLabel">System: </td>
          <td class="rowData"><?php echo $rowEquipment['name']; ?></td>
        </tr>
        <tr>
          <td class="rowLabel">Contract&nbsp;Type: </td>
          <td class="rowData"><?php echo $rowContract['type']; ?></td>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
        </tr>
        <tr>
          <td class="rowLabel">Primary&nbsp;Engineer: </td>
          <td class="rowData"><a  href="<?php echo "mailto:".$rowEngPri['email']; ?>"><?php echo $rowEngPri['name'] . "&nbsp;&nbsp;(" . $rowEngPri['cell'] .")"; ?></a></td>
          <td class="rowLabel">Secondary&nbsp;Engineer: </td>
          <td class="rowData"><a  href="<?php echo "mailto:".$rowEngSec['email']; ?>"><?php echo $rowEngSec['name'] . "&nbsp;&nbsp;(" . $rowEngSec['cell'] .")"; ?></a></td>
        </tr>
		<tr>
			<td class="rowLabel">Facility&nbsp;Timezone: </td>
			<td class="rowData" colspan="3"><div style="float:left"><span id="tzinfo"><?php if($facility_timezone_utc){echo "<span style='color:#F00;'>ERROR: FACILITY TIMEZONE IS NOT SET!&emsp;&emsp;</span>";} echo 'Abv: ', $php_utils->tz_abbreviation($facility_timezone), '&emsp;&emsp;GMT Offset: ', $php_utils->tz_offset($facility_timezone), '&emsp;&emsp;Zone: ', $facility_timezone; ?></span></div><div style="float:right"><a class="small_link" onClick="change_facility_timezone();">Update Timezone</a></div></td>
		</tr>
		<?php if($edit_request or $view){ ?>
		<tr>
			<td class="rowLabel">Dispatcher: </td>
			<td class="rowData" colspan="3"><?php echo $rowReq['dispatch_name']; ?></td>
		</tr>
		<?php } ?>
    </table>
</div>

<div id="srSystemNotesDiv" >
	<table id="srSystemNotesTable" class="srTable" >
        <tr>
          <td class="rowLabel">System Notes<span id="system_notes_lbl" class="tooltip"></span></td>
        </tr>
        <tr>
          <td class="rowData">
	          <div id="systemNotesDiv" >
		          <table width="100%" id="systemNotesTable" >
			          <thead>
			          <tr>
				          <th width="20%">Date</th>
				          <th width="65%">Entry</th>
				          <th width="15%">User</th>
			          </tr>
			          </thead>
			          <tbody>
			          <?php
			          if($resultSystemNotes->num_rows != 0){
				          while($rowSystemNotes = $resultSystemNotes->fetch_assoc())
				          {
					          echo "<tr>\n";
					          echo "<td><a class='iframeNote' href='/report/common/systems_notes.php?method=view&id=".$rowSystemNotes['id']."'>".date(phpdispfdt, strtotime($rowSystemNotes['date']))."</a></td>\n";
					          echo "<td>". limit_words($rowSystemNotes['note'],$probrpt_word_limt)."</td>\n";
					          echo "<td>". $rowSystemNotes['name']."</td>\n";
					          echo "</tr>\n";
				          }
			          }
			          ?>
			          </tbody>
		          </table>
	          </div>
          </td>
        </tr>
    </table>
</div>

<div id="srSystemNotesDiv" <?php if($rowFacility['notes'] ==""){echo "style=\"display:none\"";}?>>
	<table id="srSystemNotesTable" class="srTable" >
        <tr>
          <td class="rowLabel">Facility Notes<span id="facility_notes_lbl" class="tooltip"></span></td>
        </tr>
        <tr>
          <td class="rowData"><textarea class="text_toggle" style="height:15px;" id="facility_notes" name="facility_notes" maxlength="2000" readonly><?php echo $rowFacility['notes']; ?></textarea></td>
        </tr>
    </table>
</div>

<div id="srSystemNotesDiv" <?php if($rowCustomer['notes'] ==""){echo "style=\"display:none\"";}?>>
	<table id="srSystemNotesTable" class="srTable" >
        <tr>
          <td class="rowLabel">Customer Notes<span id="customer_notes_lbl" class="tooltip"></span></td>
        </tr>
        <tr>
          <td class="rowData"><textarea class="text_toggle" style="height:15px;" id="customer_notes" name="customer_notes" maxlength="2000" readonly><?php echo $rowCustomer['notes']; ?></textarea></td>
        </tr>
    </table>
</div>

<div id="srDataDiv">
    <table id="srDataTable" class="srTable">
        <tr>
          <td class="rowLabel">Contract Type Override:</td>
          <td class="rowData"><select name="contract_override" id="contract_override" class="tooltip">
		  <option value=""></option>
          <?php
			while($rowContracts = $resultContracts->fetch_assoc())
  			{
				if(!$edit_request){
	    			echo "<option value='" . $rowContracts['id'] . "'>" . $rowContracts['type'] . "</option>\n";
				}else{
					echo "<option value='" . $rowContracts['id'] . "' ";
					if($rowReq['contract_override'] == $rowContracts['id']){echo "selected ";}
					echo ">" . $rowContracts['type'] . "</option>\n";	
				}
  			}
          ?>
		  </select></td>
		  <td class="rowLabel">Invoice Required:</td>
          <td class="rowData"><select name="invoice_req" id="invoice_req" class="tooltip">
		  	  <option value="" <?php if(!$edit_request){echo "selected";} ?>></option>
			  <option value="N" <?php if($edit_request){if(strtolower($rowReq['invoice_required'])=='n'){echo "selected";}} ?>>No</option>
          	  <option value="Y" <?php if($edit_request){if(strtolower($rowReq['invoice_required'])=='y'){echo "selected";}} ?>>Yes</option>
			  </select></td>
        </tr>
    </table>
</div>

<div id="srDateTimeDiv">
    <table id="srDateTimeTable" class="srTable">
        <tr>
          <td class="rowLabel">Initial Call Date/Time:</td>
          <td class="rowData"><input type="text" id="initial_call_date" name="initial_call_date" class="findme tooltip" value="<?php if(!$edit_request){echo date(phpdispfdt,$t);}else{echo date(phpdispfdt,strtotime($rowReq['initial_call_date']) + $facility_timezone_offset_sec);} ?>"  /></td>
          <td class="rowLabel">Eng Onsite Date:</td>
          <td class="rowData"><input type="text" id="onsite_date" name="onsite_date" class="tooltip" value="<?php if(!$edit_request){echo date(phpdispfd,$t);}else{echo date(phpdispfd,strtotime($rowReq['onsite_date']));} ?>"  /></td>
        </tr>
        <tr>
          <td class="rowLabel">Response Date/Time:</td>
          <td class="rowData"><input type="text" id="response_date" name="response_date" class="tooltip" value="<?php if(!$edit_request){echo date(phpdispfdt,$t);}else{echo date(phpdispfdt,strtotime($rowReq['response_date']) + $facility_timezone_offset_sec);} ?>"  /></td>
          <td class="rowLabel"><?php if(!$edit_request){echo "Facility";}else{echo "Request";} ?> Date/Time:</td>
          <td class="rowData"><input type="text" id="request_date" name="request_date" class="tooltip" readonly value="<?php if(!$edit_request){echo date(phpdispfdt,$t);}else{echo date(phpdispfdt,strtotime($rowReq['request_date']) + $facility_timezone_offset_sec);} ?>"  /></td>
        </tr>
    </table>
</div>

<div id="srDataDiv">
    <table id="srDataTable" class="srTable">
        
        <tr>
	        <?php if(!$edit_request){ ?>
		        <td class="rowLabel">Inital System Status:</td>
		        <td class="rowData"><select name="init_system_status" id="init_system_status" class="tooltip">
			        <option value=""></option>
			        <?php
			        while($rowStatus = $resultStatus->fetch_assoc())
			        {
				        echo "<option value='" . $rowStatus['id'] . "'>" . $rowStatus['status'] . "</option>\n";
			        }
			        ?>
		        </select></td>
	        <?php }else{ ?>
			  <td class="rowLabel">Current System Status:</td>
			  <td class="rowData"><select name="cur_system_status" id="cur_system_status" class="tooltip">
			  <option value=""></option>
			  <?php
				while($rowStatus = $resultStatus->fetch_assoc())
				{
					echo "<option value='" . $rowStatus['id'] . "' ";
					if($rowSystem['status'] == $rowStatus['id']){echo "selected ";}
					echo ">" . $rowStatus['status'] . "</option>\n";
				}
			  ?>
			  </select></td>
			<?php } ?>
		  <td class="rowLabel">Assigned Engineer:</td>
		  <td class="rowData"><select name="engineer" id="engineer" class="tooltip">
		  <option value=""></option>
		  <?php
			while($rowEngineer = $resultEngineer->fetch_assoc())
			{
				if(!$edit_request){
					echo "<option value='" . $rowEngineer['uid'] . "'>" . $rowEngineer['name'] . "</option>\n";
				}else{
					
					echo "<option value='" . $rowEngineer['uid'] . "'";
					if($rowEngineer['uid'] == $rowReq['engineer']){echo " selected";}
					echo ">" . $rowEngineer['name'] . "</option>\n";
					

				}

			}
		  ?>
		  </select></td>
        </tr>
        <tr>
	        <td class="rowLabel">Refrigeration Service:</td>
	        <td class="rowData"><select id="refrig_service" name="refrig_service" class="tooltip">
				<option value=""></option>
			    <option value="N" <?php if($edit_request){if(strtolower($rowReq['refrig_service'])=='n'){echo "selected";}} ?>>No</option>
			    <option value="Y" <?php if($edit_request){if(strtolower($rowReq['refrig_service'])=='y'){echo "selected";}} ?>>Yes</option>
		    </select></td>
         	<td class="rowLabel">Purchase Order:</td>
         	<td class="rowData"><input type="text" id="po_number" name="po_number" class="tooltip" value="<?php if($edit_request){echo $rowReq['po_num'];} ?>" /></td>
        </tr>
		    <td class="rowLabel">Preventive Maintenance:</td>
		    <td class="rowData"><select id="pm" name="pm" class="tooltip" onChange="pm_date_enable_disable();">
			    <option value=""></option>
			    <option value="N" <?php if($edit_request){if(strtolower($rowReq['pm'])=='n'){echo "selected";}} ?>>No</option>
			    <option value="Y" <?php if($edit_request){if(strtolower($rowReq['pm'])=='y'){echo "selected";}} ?>>Yes</option>
		    </select></td>

		    <td class="rowLabel">PM Scheduled Date/Time:</td>
		    <td class="rowData"><input <?php if($edit_request){if(strtolower($rowReq['pm'])!="y"){echo "disabled";}}else{if(strtolower($rowReq['pm'])!="y"){echo "disabled";}} ?> type="text" id="pm_date" name="pm_date" class="tooltip" value="<?php if($edit_request and strlen($rowReq['pm_date']) > 2){echo date(phpdispfdt,strtotime($rowReq['pm_date']));} ?>" /></td>

	    </tr>
    </table>
</div>

<div id="srDataDiv">
	<table id="srDataTable" class="srTable">
	    <tr>
			<td class="rowLabel">FBC Necessary:</td>
    	    <td class="rowData"><select id="fbc" name="fbc" >
			<option value="N" <?php if($edit_request){if(strtolower($rowReq['fbc'])=='n'){echo "selected";}} ?>>No</option>
    	    <option value="Y" <?php if($edit_request){if(strtolower($rowReq['fbc'])=='y'){echo "selected";}} ?>>Yes</option>
	        </select>
			</td>
	        <td colspan="2">Yes if customer lodged a complaint or stated this is a recurring issue.<br>
	        	You will then be asked to fill out a Feedback Complaint Form.</td>
        </tr>
    </table>
</div>

<div id="srPrevSystemNotesDiv" <?php if($numResultPrevNotes<=0){echo "style=\"display:none\"";}else{if($rowPrevNotes['notes'] == ""){echo "style=\"display:none\"";}} ?>>
    <table id="srPrevSystemNotesTable" class="srTable"  >
        <tr>
          <td class="rowLabel red">Engineer's Previous Report Notes. Inform engineer please.<span id="prev_notes_lbl" class="tooltip"></span></td>
        </tr>
        <tr>
          <td class="rowData"><textarea id="prev_notes" name="prev_notes" maxlength="2000" readonly><?php if($numResultPrevNotes > 0){echo addslashes(htmlspecialchars_decode($rowPrevNotes['notes']));}?></textarea></td>
        </tr>
    </table>
</div>

<div id="srComplaintDiv">
    <table id="srComplaintTable" class="srTable" >
        <tr>
          <td class="rowLabel">Problem Reported<span id="problem_reported_lbl" class="tooltip"></span></td>
        </tr>
        <tr>
          <td class="rowData"><textarea id="problem_reported" name="problem_reported" maxlength="2000"><?php if(!$edit_request){if($pm){echo "Scheduled PM";}}else{echo addslashes(htmlspecialchars_decode($rowReq['problem_reported']));} ?></textarea></td>
        </tr>
    </table>
</div>

<div id="srActionsDiv">
    <table id="srActionsTable" class="srTable" >
        <tr>
          <td class="rowLabel">Customer Actions<span id="customer_actions_lbl" class="tooltip"></span></td>
        </tr>
        <tr>
          <td class="rowData"><textarea id="customer_actions" name="customer_actions" class="text_toggle" maxlength="2000"><?php if(!$edit_request){if($pm){echo "None";}}else{echo addslashes(htmlspecialchars_decode($rowReq['customer_actions']));} ?></textarea></td>
        </tr>
    </table>
</div>
<?php if($edit_request){ ?>
<table id="srDataTable" class="srTable">
	<tr>
		<td width="100%">
			<div id="notesFrame" style="text-align:center">
				<?php  
					$sql = "SELECT ssj.note, ssj.`date`, u.name, ssj.id, IF(ssj.system_status IS NULL OR ssj.system_status = '', 'N/A', ss.abv) AS status_abv
					FROM systems_service_journal AS ssj
					LEFT JOIN systems_requests AS sr ON sr.unique_id = ssj.request_report_unique_id
					LEFT JOIN users AS u ON u.uid = ssj.uid
					LEFT JOIN systems_status AS ss ON ss.id = ssj.system_status
					WHERE sr.unique_id = '".$rowReq['unique_id']."'
					ORDER BY ssj.`date` DESC;";
					if(!$resultNotes = $mysqli->query($sql)){
						$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
						$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
						$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
					}
				?>  
				<div id="notesFrame">     
				<div style="rowLabel"> Service Journal<span id="newNote_lbl" class=""></span></div>
				<div id="create_note" style="width:99%; text-align:center;">
					<div id="newNote" style="margin-bottom:15px; margin-top:5px;"><a class="iframeNote button_jquery_new" href="/report/common/systems_service_journal.php?new&unique_id=<?php echo $unique_id; ?>">New Journal Entry</a></div>
				</div>
				<div id="notesDiv" <?php if($resultNotes->num_rows == 0){echo "style=\"display:none\"";} ?>>
					<table width="100%" id="notesTable" >
						<thead>
							<tr>
								<th width="15%">Date</th> 
								<th>Entry</th>
								<th width="10%">Status</th>
								<th width="15%">User</th>
							</tr>
						</thead>
						<tbody>
							<?php
							if($resultNotes->num_rows != 0){
								while($rowNotes = $resultNotes->fetch_assoc())
								{
									echo "<tr>\n";
									echo "<td><a class='iframeNote' href='/report/common/systems_service_journal.php?id=".$rowNotes['id']."'>". date(phpdispfdt,strtotime($rowNotes['date']))."</a></td>\n";
									echo "<td>". limit_words($rowNotes['note'],$probrpt_word_limt)."</td>\n";
									echo "<td>". $rowNotes['status_abv']."</td>\n";
									echo "<td>". $rowNotes['name']."</td>\n";
									echo "</tr>\n";
								}
							}
							?>                        
						</tbody>
					</table>
				</div>
			  </div>
			</div>
		</td>
	</tr>
</table>
<?php } ?>
<div style="display:none">
    <input type="text" id="user_id" name="user_id" value="<?php echo $_SESSION['login']; ?>" />
    <input type="text" id="system_id" name="system_id" value="<?php echo $rowSystem['system_id']; ?>" />
    <input type="text" id="system_nickname" name="system_nickname" value="<?php echo addslashes(htmlspecialchars_decode($rowSystem['nickname'])); ?>" />
    <input type="text" id="sender" name="sender" value="<?php echo $_SESSION['userdata']['name']; ?>" />
    <input type="text" id="request_num" name="request_num" value="Change ME" />
    <input type="text" id="pri_eng_name" name="pri_eng_name" value="<?php echo $rowEngPri['name'] . "  (" . $rowEngPri['cell'] .")"; ?>" />
    <input type="text" id="pri_eng_email" name="pri_eng_email" value="<?php echo $rowEngPri['email']; ?>" />
    <input type="text" id="sec_eng_name" name="sec_eng_name" value="<?php echo $rowEngSec['name'] . "  (" . $rowEngSec['cell'] .")"; ?>" />
    <input type="text" id="sec_eng_email" name="sec_eng_email" value="<?php echo $rowEngSec['email']; ?>" />
    <input type="text" id="system_full_address" name="system_full_address" value="<?php echo $rowFacility['address'] .", ". $rowFacility['city'] .", ". $rowFacility['state'] .", ". $rowFacility['zip'];?>" />
    <input type="text" id="system_unique_id" name="system_unique_id" value="<?php echo $sys_unique_id; ?>" />
	<input type="text" id="system_ver_unique_id" name="system_ver_unique_id" value="<?php echo $ver_unique_id; ?>" />
	<input type="text" id="system_ver" name="system_ver" value="<?php echo $version; ?>" />
	<input type="text" id="facility_unique_id" name="facility_unique_id" value="<?php echo $rowSystem['facility_unique_id']; ?>" />
	<input type="text" id="request_num" name="request_num" value="<?php if($edit_request){echo $rowReq['request_num'];}else{echo "";} ?>" />
	<input type="text" id="editReq" name="editReq" value="<?php if($edit_request){echo "Y";}else{echo "N";} ?>" />
	<input type="text" id="req_unique_id" name="req_unique_id" value="<?php echo $unique_id; ?>" />
	<input type="text" id="del" name="del" value="" />
	<input type="text" id="system_type" name="system_type" value="<?php echo strtolower($rowEquipment['type']); ?>" />
	<input type="text" id="facility_timezone" name="facility_timezone" value="<?php echo $rowFacility['timezone']; ?>" />
	<input type="checkbox" id="journal_only" name="journal_only" value="Y" />
	<?php if($debug){ ?><input name="debug" id="debug" type="hidden" value="Y" /> <?php } ?>
</div>
</div>



<div id="srFooterDiv">
	<?php if(!$view){ ?>
	<div id="srFooterButtons" <?php if($edit_request){echo "style=\"width:100%\"";} ?>>
		<?php if(!$journal_only){ ?>
<!--			<div class="srBottomBtn"><a class="button_jquery_save" id="create_edit_btn" onClick="submitcheck('')">--><?php //if(!$edit_request){echo "Create";}else{echo "Edit";} ?><!-- Service Request</a></div>-->
		<?php } ?>
		<?php if($edit_request){?>
			<?php if(!$journal_only){ ?>
<!--				<div class="srBottomBtn"><a class="button_jquery_delete" id="delete_btn" onClick="submitcheck('del')">Delete Service Request</a></div>-->
			<?php } ?>
			<div class="srBottomBtn"><a class="button_jquery_note" id="journal_done_btn" onClick="submitcheck('jou')">New Journal Entry Only</a></div>
		<?php } ?>
	</div>
	<?php } ?>
</div>

</form>
</div>
</div>
<?php require_once($footer_include); ?>