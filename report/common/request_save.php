<?php
$debug = false;
if(isset($_POST['debug'])){
	$debug = true;	
	$no_sql = true;
}

ignore_user_abort(true);
set_time_limit(30);

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/php_utils.php');
$php_utils = new phpUtils();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

if(!$debug){
	if(isset($_SESSION['service_request_refresh'])){
		$log->logerr("Cannot refresh the request save page!",100,true,basename(__FILE__),__LINE__);	
	}
}
$_SESSION['service_request_refresh'] = true;

if(isset($_POST['editReq'])){
	if(strtolower($_POST['editReq'])=='y'){
		$editReq=true;
	}else{
		$editReq=false;
	}
}else{
	$editReq=false;
}
d($editReq);

$sms_comp_word_limt = intval($settings->sms_complaint_word_limit);

$main = $_POST;
d($_POST);
if(!$debug){
	unset($_POST);	
}
d($main);

$tz_offset_sec = $php_utils->tz_offset_sec($main['facility_timezone']);
$new_request = "Y";
$status = "open";
$report_started = "N";

$under30min = 'Y';
$initDate = new DateTime(date(storef,strtotime($main['initial_call_date'])));
d($initDate);
$respDate = new DateTime(date(storef,strtotime($main['response_date'])));
d($respDate);
$interval = date_diff($initDate, $respDate);
d($interval);
$minutes = $interval->days * 24 * 60;
$minutes += $interval->h * 60;
$minutes += $interval->i;
d($minutes);
if($minutes > 30){
	$under30min = 'N';
}

if(!isset($main['req_unique_id'])){
	$log->logerr("Request Unique ID Invalid",1019,true,basename(__FILE__),__LINE__);	
}
$unique_id=$main['req_unique_id'];

$fbc = false;
if(strtolower($main['fbc']) == 'y'){
	$fbc = true;	
}

$journal_only = false;
if(isset($main['journal_only'])){
	$journal_only = true;
	$no_sql = true;
}
d($journal_only);
d($no_sql);

$system_unique_id = $main['system_unique_id'];
$system_ver_unique_id = $main['system_ver_unique_id'];
$system_version = $main['system_version'];

$values = array(
	'engineer'=>$main['engineer'],
	'user_id'=>$main['user_id'],
	'system_id'=>$main['system_id'],
	'system_unique_id'=>$main['system_unique_id'],
	'system_ver_unique_id'=>$main['system_ver_unique_id'],
	'system_ver'=>$main['system_ver'],
	'facility_unique_id'=>$main['facility_unique_id'],
	'system_nickname'=>htmlspecialchars($main['system_nickname']),
	'request_date'=>date(storef,strtotime($main['request_date']) - $tz_offset_sec),
	'onsite_date'=>date(storef,strtotime($main['onsite_date'])),
	'initial_call_date'=>date(storef,strtotime($main['initial_call_date']) - $tz_offset_sec),
	'response_date'=>date(storef,strtotime($main['response_date']) - $tz_offset_sec),
	'fbc'=>$main['fbc'],
	'problem_reported'=>htmlspecialchars($main['problem_reported'],ENT_COMPAT),
	'customer_actions'=>htmlspecialchars($main['customer_actions'],ENT_COMPAT),
	'po_number'=>$main['po_number'],
	'contract_override'=>$main['contract_override'],
	'pm'=>$main['pm'],
	'pm_date'=>date(storef,strtotime($main['pm_date'])),
	'invoice_req'=>$main['invoice_req'],
	'under_30'=>$under30min,
	'minutes'=>$minutes,
	'unique_id'=>$unique_id,
	'new_request'=>$new_request,
	'status'=>$status,
	'report_started'=>$report_started,
	'repeat_call'=>$main['repeat_call'],
	'repeat_call_id'=>$main['repeat_call_id']);

if($editReq){
	$values['system_status'] = $main['cur_system_status'];
}else{
	$values['system_status'] = $main['init_system_status'];
}
d($values);

$sql="SELECT `name` FROM users WHERE uid='".$main['engineer']."';";
d($sql);
if(!$resultEngineer = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowEngineer = $resultEngineer->fetch_assoc();
$request_num = $main['request_num'];
if(strtolower($main['del'])!='y'){
	if(!$editReq){
		$sql = "INSERT INTO systems_requests_ids (`unique_id`) VALUES ('$unique_id');";
		d($sql);
		if(!$no_sql){
			if(!$result = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
				$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
			}
		}
		
		$sql = "SELECT LAST_INSERT_ID() AS id;";
		if(!$resultId = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
		$request_num = $resultId->fetch_object()->id;
		
		$sql="INSERT INTO systems_requests (`request_num`, `engineer`, `sender`, `system_id`, `system_unique_id`, `system_ver_unique_id`, `system_ver`, `facility_unique_id`, `system_nickname`, `request_date`, `onsite_date`, 
			`initial_call_date`, `response_date`, `fbc`, `problem_reported`, `customer_actions`, `po_num`, `contract_override`, `pm`, `pm_date`, `invoice_required`, `under_30min`, 
			`response_minutes`, `unique_id`, `new_request`, `status`, `report_started`, `repeat_call`, `repeat_call_id`, `system_status`) 
			 VALUES('".$request_num."',\"". implode("\", \"", $values) . "\");";
		d($sql);
		if(!$no_sql){
			if(!$result = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
				$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
			}
		}

		$sql = "UPDATE systems_base SET `status`='".$main['init_system_status']."' WHERE `unique_id`='".$main['system_unique_id']."' LIMIT 1;";
		d($sql);
		if(!$no_sql){
			if(!$result = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
				$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
			}
		}

		if(strtolower($main['pm'] == "y")){
			$cal_txt = $rowEngineer['name']." - PM @ System ".$main['system_id'];
			$subject = "pm-".$main['system_type'];
			
		}else{
			$cal_txt = $rowEngineer['name']." - Service @ System ".$main['system_id'];
			$subject = "service-".$main['system_type'];
		}
		$sql="INSERT INTO users_calendar (`start_date`,`end_date`,`text`,`detail`,`location`,`uid`,`request_unique_id`,`readonly`,`subject`) 
			VALUES ('".date(calstoref,strtotime($main['onsite_date']))."','".date(calstoref,strtotime('+1 hours', strtotime($main['onsite_date'])))."','".$cal_txt."','".addslashes($main['problem_reported'])."\r\nReferance Number: ".$request_num."','".addslashes($main['system_nickname'])."','".$main['engineer']."','".$unique_id."','X','".$subject."');";
		d($sql);
		if(!$no_sql){
			if(!$result = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
				$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
			}
		}
	}else{
		if(strtolower($main['pm'] == "y")){
			$cal_txt = $rowEngineer['name']." - PM @ System ".$main['system_id']." Ref ".$request_num;
			$subject = "pm-".$main['system_type'];
		}else{
			$cal_txt = $rowEngineer['name']." - Service @ System ".$main['system_id']." Ref ".$request_num;
			$subject = "service-".$main['system_type'];
		}
		$sql="UPDATE users_calendar SET
		`start_date`='".date(calstoref,strtotime($main['onsite_date']))."',
		`end_date`='".date(calstoref,strtotime('+1 hours', strtotime($main['onsite_date'])))."',
		`text`='".$cal_txt."',
		`detail`=\"".htmlspecialchars($main['problem_reported'],ENT_COMPAT)."\r\nReferance Number: ".$request_num."\",
		`location`=\"".htmlspecialchars($main['system_nickname'],ENT_COMPAT)."\",
		`uid`='".$main['engineer']."',
		`subject`='".$subject."'
		WHERE request_unique_id='".$unique_id."';";
		d($sql);
		if(!$no_sql){
			if(!$result = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
				$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
			}
		}

		$sql="UPDATE systems_requests SET
		`engineer`='".$main['engineer']."',
		`request_date`='".date(storef,strtotime($main['request_date']) - $tz_offset_sec)."',
		`onsite_date`='".date(storef,strtotime($main['onsite_date']))."',
		`initial_call_date`='".date(storef,strtotime($main['initial_call_date']) - $tz_offset_sec)."',
		`response_date`='".date(storef,strtotime($main['response_date']) - $tz_offset_sec)."',
		`fbc`='".$main['fbc']."',
		`problem_reported`=\"".htmlspecialchars($main['problem_reported'],ENT_COMPAT)."\",
		`customer_actions`=\"".htmlspecialchars($main['customer_actions'],ENT_COMPAT)."\",
		`po_num`='".$main['po_number']."',
		`contract_override`='".$main['contract_override']."',
		`pm`='".$main['pm']."',
		`pm_date`='".date(storef,strtotime($main['pm_date']))."',
		`invoice_required`='".$main['invoice_req']."',
		`under_30min`='".$under30min."',
		`response_minutes`='".$minutes."',
		`repeat_call`='".$main['repeat_call']."',
		`repeat_call_id`='".$main['repeat_call_id']."',
		`edited_by`='".$main['user_id']."',
		`edited_date`='".date(storef,time())."'
		WHERE unique_id='".$unique_id."';";
		d($sql);
		if(!$no_sql){
			if(!$result = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
				$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
			}
		}

		$sql = "UPDATE systems_base SET `status`='".$main['cur_system_status']."' WHERE `unique_id`='".$main['system_unique_id']."' LIMIT 1;";
		d($sql);
		if(!$no_sql){
			if(!$result = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
				$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
			}
		}
	}
}else{
	$sql="UPDATE systems_requests SET `deleted`='Y', `deleted_by`='".$_SESSION['login']."', `deleted_date`='".date(storef,time())."' WHERE unique_id='".$unique_id."';";
	d($sql);
	if(!$no_sql){
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
	}

	$sql="UPDATE users_calendar SET	`text`= CONCAT(`text`,' - DELETED')	WHERE request_unique_id='".$unique_id."';";
	d($sql);
	if(!$no_sql){
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
	}
}

$data = array('rid'=>$unique_id, 'ed'=>$editReq?"y":"n", 'del'=>strtolower($main['del'])=='y'?"y":"n");
$php_utils->email_action_add($mysqli, "/report/common/request_send.php",$data,$_SESSION['login']);

function limit_words($string, $word_limit)
{
	$words = explode(" ",$string);
	if(count($words) > $word_limit){
		return implode(" ",array_splice($words,0,$word_limit))."(more)";
	}else{
		return implode(" ",array_splice($words,0,$word_limit));
	}
}
?>
<!DOCTYPE html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<?php require_once($js_include);?>

<script type="text/javascript">
//$(document).ready(function() {
//	$.ajax({
//		type: 'POST',
//		url: 'request_send.php',
//		async:true,
//		data: { rid: '<?php //echo $unique_id; ?>//', ed: '<?php //if($editReq){echo "y";}else{echo "n";} ?>//', del:'<?php //if(strtolower($main['del']) == "y"){echo "y";}else{echo "n";} ?>//' <?php //if($debug){echo ", debug:'YES'";} ?>// },
//		success:function(data){
//			// successful request; do something with the data
//			$("#emailSent").text("Emails Sent");
//		},
//		error: function(jqXHR, textStatus, errorThrown){
//			console.log("Ajax Email Error");
//			var txtSts = textStatus;
//			var errT = errorThrown;
//			var jqX = jqXHR;
//			$.ajax({
//				type: 'POST',
//				url: '/log/log.php',
//				async: true,
//				data: {type: 'error', text: "<?php //echo 'Request ID: '.$request_num.' Unique ID: '.$unique_id; ?>// " + " jqXHR: " + JSON.stringify(jqX, null, 4) + " textStatus: " + txtSts + " errorThrown: " + errT, location: 'request_save.php'},
//				complete: function(jqXHR, textStatus){
//					console.log(txtSts + "   " + errT);
//				}
//			});
//		}
//	});
//});

function delayer(){
    <?php if(!$debug){ ?>
	window.location = "<?php if($fbc and !$editReq){ echo 'iso_fbc.php?system_unique_id='.$system_unique_id.'request_unique_id='.$unique_id;}else{ echo $refering_uri;} ?>"
	<?php } ?>
}

</script>
</head>
<body onLoad="setTimeout('delayer()', 1500)">
<?php require_once($header_include); ?>
<div id="OIReportContent"> 
<h1>Service Request has been <?php if(!$editReq){echo "created";}else{echo "updated";} ?></h1>
<br />
<h2>Request Number: <?php if(!$editReq){echo $request_num;}else{echo $main['request_num'];} ?></h2>
<br />
<h2><span id="emailSent"></span></h2>
<br />
<?php if($fbc and !$editReq){ ?>
<h1><span class="red">Redirecting to FBC form page</span></h1>
<?php }else{ ?>
<h1><span class="red">Page will return to dashboard in 3 seconds</span></h1>
<?php } ?>

</div>
<?php require_once($footer_include); ?>