<?php
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
ini_set('display_errors', 'On');
ignore_user_abort(true);
set_time_limit(30);

if(substr(php_sapi_name(), 0, 3) == 'cli' || empty($_SERVER['REMOTE_ADDR'])) {
	//shell
	$cron = false;
	if(isset($_SERVER['argv'][1]) and $_SERVER['argv'][1] == '--cron'){
		$cron = true;
	}
	$docroot = preg_replace("/(public_html).*/",'public_html',dirname(__FILE__),2);
	$no_define_eol = true;
	define('EOL',PHP_EOL);
} else {
	//webserver
	$cron = false;
	$docroot = $_SERVER['DOCUMENT_ROOT'];
	define('EOL','<br />');
}

$settings = new SimpleXMLElement($docroot.'/settings.xml', null, true);
require_once($docroot.'/define_inc.php');
require_once($docroot.'/mysqlInfo.php');
require_once($docroot.'/log/log.php');
$log = new logger($docroot);
require_once($docroot.'/report/common/scripts/php_utils.php');
$php_utils = new phpUtils($docroot, $debug);

//get request id from post
$debug = false;
if(isset($_GET['debug'])){
	$debug = true;
	$send = false;
	$delReq = false;
	$editReq = false;

	if(!isset($_GET['rid'])){
		$log->logerr('Blank RID',1024);
		exit("No request id");
	}else{
		$rid=$_GET['rid'];
	}
	if(!isset($_GET['del'])){
		$log->logerr('request_send.php',1027);
		exit("No delete info");
	}
	if(!isset($_GET['ed'])){
		$log->logerr('request_send.php',1028);
		exit("No edit info");
	}
	if(isset($_GET['send'])){
		$send = true;
	}
	if(strtolower($_GET['del']) == "y"){$delReq = true;}
	if(strtolower($_GET['ed']) == "y"){$editReq = true;}
}else{
	$send = true;
	$delReq = false;
	$editReq = false;

	if(!isset($_POST['rid'])){
		$log->logerr('Blank RID',1024);
		exit("No request id");
	}
	if(!isset($_POST['del'])){
		$log->logerr('Blank DEL',1027);
		exit("No delete info");
	}
	if(!isset($_POST['ed'])){
		$log->logerr('Blank ED',1028);
		exit("No edit info");
	}
	
	$rid=$_POST['rid'];
	if(strtolower($_POST['del']) == "y"){$delReq = true;}
	if(strtolower($_POST['ed']) == "y"){$editReq = true;}
	if(intval($settings->disable_email) == 1){
		$send = false;	
	}
}

require_once($docroot.'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

//load phpmailer
require_once($docroot.'/resources/PHPMailer/PHPMailerAutoload.php');

require_once($docroot.'/resources/smarty/libs/Smarty.class.php');
$smarty = new Smarty();
$smarty->setTemplateDir($docroot.$settings->email_templates);
$smarty->setCompileDir($docroot.$settings->email_template_c);
$smarty->setCacheDir($docroot.$settings->email_cache);
$smarty->setConfigDir($docroot.$settings->email_configs);
$smarty->use_sub_dirs = true;
$smarty->force_compile = true;
//$smarty->debugging = true;
$smarty->caching = false;

$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno) {die('There was an error running the query [' . $mysqli->connect_error . ']');}

//Get the request info
$sql="SELECT sr.*, u.name AS dispatch_name FROM systems_requests AS sr
LEFT JOIN users AS u ON u.uid = sr.sender
WHERE unique_id = '".$rid."';";
if(!$resultRequest = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	exit("SQL ERROR");
}
$rowRequest = $resultRequest->fetch_assoc();
d($rowRequest);

//get pri engineer info
$sql="SELECT a.uid,u.name,u.email,u.cell,c.address,
CONCAT_WS('', MAX(IF(pmid.perm = 'perm_rcv_sms', 'Y', 'N'))) AS perm_rcv_sms, 
CONCAT_WS('', MAX(IF(prid.pref = 'pref_rcv_sms', 'Y', 'N'))) AS pref_rcv_sms
FROM systems_assigned_pri AS a
LEFT JOIN users AS u ON a.uid = u.uid
LEFT JOIN users_perms AS upm ON upm.uid = u.uid
LEFT JOIN users_perm_id AS pmid ON pmid.id = upm.pid
LEFT JOIN users_prefs AS upr ON upr.uid = u.uid
LEFT JOIN users_pref_id AS prid ON prid.id = upr.pid
LEFT JOIN misc_carrier AS c ON c.carrierid = u.carrier
WHERE a.system_ver_unique_id='".$rowRequest['system_ver_unique_id']."';";
if(!$resultEngPri = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	exit("SQL ERROR");
}
$rowEngPri = $resultEngPri->fetch_assoc();
d($rowEngPri);

//get sec engineer info
$sql="SELECT a.uid,u.name,u.email,u.cell,c.address,
CONCAT_WS('', MAX(IF(pmid.perm = 'perm_rcv_sms', 'Y', 'N'))) AS perm_rcv_sms, 
CONCAT_WS('', MAX(IF(prid.pref = 'pref_rcv_sms', 'Y', 'N'))) AS pref_rcv_sms
FROM systems_assigned_sec AS a
LEFT JOIN users AS u ON a.uid = u.uid
LEFT JOIN users_perms AS upm ON upm.uid = u.uid
LEFT JOIN users_perm_id AS pmid ON pmid.id = upm.pid
LEFT JOIN users_prefs AS upr ON upr.uid = u.uid
LEFT JOIN users_pref_id AS prid ON prid.id = upr.pid
LEFT JOIN misc_carrier AS c ON c.carrierid = u.carrier
WHERE a.system_ver_unique_id='".$rowRequest['system_ver_unique_id']."';";
if(!$resultEngSec = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	exit("SQL ERROR");
}
$rowEngSec = $resultEngSec->fetch_assoc();
d($rowEngSec);

//get system info
$sql="SELECT sbc.*, sb.*, ss.`status` AS system_status_name, c1.`type` AS contract, c2.`type` AS contract_override, st.name AS system_type_name,
u.name AS engineer, u.email AS engineer_email, u.cell AS engineer_cell, ca.address AS engineer_cell_address,
f.timezone, f.name AS facility_name, f.facility_id, f.address, f.city, f.state, f.zip,
CONCAT_WS('', MAX(IF(pmid.perm = 'perm_rcv_sms', 'Y', 'N'))) AS perm_rcv_sms, 
CONCAT_WS('', MAX(IF(prid.pref = 'pref_rcv_sms', 'Y', 'N'))) AS pref_rcv_sms
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
LEFT JOIN users AS u ON u.uid = '".$rowRequest['engineer']."'
LEFT JOIN misc_carrier AS ca ON ca.carrierid = u.carrier
LEFT JOIN misc_contracts AS c1 ON c1.id = sbc.contract_type
LEFT JOIN misc_contracts AS c2 ON c2.id = '".$rowRequest['contract_override']."'
LEFT JOIN systems_status AS ss ON ss.id = '".$rowRequest['system_status']."'
LEFT JOIN systems_types AS st ON st.id = sb.system_type
LEFT JOIN facilities AS f ON f.unique_id = sbc.facility_unique_id
LEFT JOIN users_perms AS upm ON upm.uid = u.uid
LEFT JOIN users_perm_id AS pmid ON pmid.id = upm.pid
LEFT JOIN users_prefs AS upr ON upr.uid = u.uid
LEFT JOIN users_pref_id AS prid ON prid.id = upr.pid
WHERE sbc.ver_unique_id = '".$rowRequest['system_ver_unique_id']."';";
d($sql);
if(!$resultSiteInfo = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	exit("SQL ERROR");
}
$rowSystemInfo = $resultSiteInfo->fetch_assoc();
d($rowSystemInfo);

$tz_offset_sec = 0;
if($rowSystemInfo['timezone'] != ''){
	$tz_offset_sec = $php_utils->tz_offset_sec($rowSystemInfo['timezone']);
}
d($rowSystemInfo['timezone']);
d($tz_offset_sec);

$initial_call_date = date(phpdispfdt,strtotime($rowRequest['initial_call_date']) + $tz_offset_sec);
$response_date = date(phpdispfdt,strtotime($rowRequest['response_date']) + $tz_offset_sec);
$onsite_date = date(phpdispfd,strtotime($rowRequest['onsite_date']));
if(strtolower($rowRequest['pm']) == 'y'){
	$pm_date = date(phpdispfdt,strtotime($rowRequest['pm_date']));
}else{
	$pm_date = "";
}
d($pm_date);

//adjust the contract type for this request
if($main['contract_override'] != ''){
	$contract_type = $rowSystemInfo['contract_override'];
}else{
	$contract_type = $rowSystemInfo['contract'];
}

$journal = false;
$sql="SELECT ssj.note, ssj.date, u.name
FROM systems_service_journal AS ssj
LEFT JOIN users AS u ON u.uid = ssj.uid
WHERE ssj.request_report_unique_id = '".$rid."';";
d($sql);
if(!$resultJournal = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	exit("SQL ERROR");
}
if($resultJournal->num_rows > 0){
	$journal = array();
	while($rowJournal = $resultJournal->fetch_assoc()){
		array_push($journal,$rowJournal);
	}
}
d($journal);


//make full address var
$address = $rowSystemInfo['address'].", ".$rowSystemInfo['city'].", ".$rowSystemInfo['state'].", ".$rowSystemInfo['zip'];

$smarty->assign('system_nickname',$rowSystemInfo['nickname']);
$smarty->assign('system_id',$rowSystemInfo['system_id']);
$smarty->assign('facility_id',$rowSystemInfo['facility_id']);
$smarty->assign('facility_name',$rowSystemInfo['facility_name']);
$smarty->assign('request_id',$rowRequest['request_num']);
$smarty->assign('status',$rowSystemInfo['system_status_name']);
if(strtolower($rowRequest['pm']) == 'y'){$smarty->assign('pm',true);}else{$smarty->assign('pm',false);}
$smarty->assign('pm_date', $pm_date);
if(strtolower($rowRequest['refrig_service']) == 'y'){$smarty->assign('refrig',true);}else{$smarty->assign('refrig',false);}
$smarty->assign('full_address',$address);
$smarty->assign('contact_name',$rowSystemInfo['contact_name']);
$smarty->assign('contact_title',$rowSystemInfo['contact_title']);
$smarty->assign('contact_phone',$rowSystemInfo['contact_phone']);
$smarty->assign('contact_cell',$rowSystemInfo['contact_cell']);
$smarty->assign('contact_email',$rowSystemInfo['contact_email']);
$smarty->assign('system_type_name',$rowSystemInfo['system_type_name']);
$smarty->assign('system_serial',$rowSystemInfo['system_serial']);
$smarty->assign('contract_name',$contract_type);
$smarty->assign('contract_terms',$rowSystemInfo['contract_terms']);
$smarty->assign('asn_eng',$rowSystemInfo['engineer']);
$smarty->assign('asn_eng_email',$rowSystemInfo['engineer_email']);
$smarty->assign('pri_eng',$rowEngPri['name']);
$smarty->assign('pri_eng_email',$rowEngPri['email']);
$smarty->assign('sec_eng',$rowEngSec['name']);
$smarty->assign('sec_eng_email',$rowEngSec['email']);
$smarty->assign('initial_call_date',$initial_call_date);
$smarty->assign('response_date',$response_date);
$smarty->assign('onsite_date',$onsite_date);
$smarty->assign('dispatcher',$rowRequest['dispatch_name']);
$smarty->assign('problem_reported',$rowRequest['problem_reported']);
$smarty->assign('customer_actions',$rowRequest['customer_actions']);
$smarty->assign('journal',$journal);
$smarty->assign('company_name',$settings->company_name);
$smarty->assign('copyright_date',$settings->copyright_date);
$smarty->assign('email_pics',$settings->email_pics);
$smarty->assign('oih_url',$settings->full_url);
$smarty->assign('oih_url_pretty',$settings->base_url_pretty);


//SMS Text to send
if(!$editReq){
	$smstext = $rowRequest['system_nickname']."\nID: ".$rowRequest['system_id']."\nCall Date: ".$response_date."\nProb: ".limit_words($rowRequest['problem_reported'],intval($settings->sms_complaint_word_limit));
}else{
	if($delReq){
		$smstext = "REQUEST DELETED\n".$rowRequest['system_nickname']."\nID: ".$rowRequest['system_id']."\nCall Date: ".$response_date."\nProb: ".limit_words($rowRequest['problem_reported'],intval($settings->sms_complaint_word_limit));
	}else{
		$smstext = "REQUEST UPDATE\n".$rowRequest['system_nickname']."\nID: ".$rowRequest['system_id']."\nCall Date: ".$response_date."\nProb: ".limit_words($rowRequest['problem_reported'],intval($settings->sms_complaint_word_limit));
	}
}
d($smstext);

//email addys of employees to send request notification to
$emails_request_emp = array();
$sql="SELECT u.name,u.email
FROM users AS u
LEFT JOIN systems_assigned_customer AS a ON a.uid = u.uid
INNER JOIN users_prefs AS upr ON upr.uid = u.uid
INNER JOIN users_pref_id AS prid ON prid.id = upr.pid AND prid.pref = 'pref_rcv_service_requests'
INNER JOIN users_perms AS up ON up.uid = u.uid
INNER JOIN users_perm_id AS pmid ON pmid.id = up.pid AND pmid.perm = 'perm_rcv_service_requests'
INNER JOIN users_groups AS ug ON ug.uid = u.uid
INNER JOIN users_group_id AS gid ON gid.id = ug.gid AND gid.`group` = 'grp_employee'
WHERE (u.active = 'y') 
OR (a.system_ver_unique_id = '".$rowSystemInfo['ver_unique_id']."' AND u.active = 'y');";
if(!$resultEmailRequest = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	exit("SQL ERROR");
}
while($rowEmailRequest = $resultEmailRequest->fetch_assoc()){
	array_push($emails_request_emp,array("email"=>$rowEmailRequest['email'],"name"=>$rowEmailRequest['name']));
}
d($emails_request_emp);

//email addys of subscriber to send request notification to
$emails_request_pub = array();
$sql="SELECT cel.email, cel.name, cel.email_unique_id
FROM customers_email_list AS cel
WHERE cel.system_ver_unique_id = '".$rowSystemInfo['ver_unique_id']."' AND cel.active = 'y' AND cel.rcv_service_request = 'y'
GROUP BY cel.email_unique_id;";
if(!$resultEmailRequest = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	exit("SQL ERROR");
}
while($rowEmailRequest = $resultEmailRequest->fetch_assoc()){
	array_push($emails_request_pub,array("email"=>$rowEmailRequest['email'],"name"=>$rowEmailRequest['name'], 'email_unique_id'=>$rowEmailRequest['email_unique_id']));
}
d($emails_request_pub);



//Engineer Email
$emails_request_eng = array();
array_push($emails_request_eng, array('email'=>$rowSystemInfo['engineer_email'], 'name'=>$rowSystemInfo['engineer']));
d($emails_request_eng);
$send_status = send_emails($emails_request_eng, $rowRequest['system_id'], $rowRequest['system_nickname'], $rowSystemInfo['ver_unique_id'], 'service_request_eng.tpl');
if(!$send_status and $debug){
	echo "<h1 style='color: red'>Engineer Email: SEND ERROR SEE LOG !</h1>",EOL;
}

//Engineer SMS
if(strtolower($rowSystemInfo['perm_rcv_sms']) == "y" and strtolower($rowSystemInfo['pref_rcv_sms']) == "y"){
	$sms_request_eng = array();
	array_push($sms_request_eng, array('cell'=>preg_replace("/[^0-9]/", "", $rowSystemInfo['engineer_cell']), 'address'=>$rowSystemInfo['engineer_cell_address']));
	$send_status = send_sms($sms_request_eng, $smstext);
	if(!$send_status and $debug){
		echo "<h1 style='color: red'>Engineer SMS: SEND ERROR SEE LOG !</h1>",EOL;
	}
}

//Employee Emails
if($emails_request_emp){
	$send_status = send_emails($emails_request_emp, $rowRequest['system_id'], $rowRequest['system_nickname'], $rowSystemInfo['ver_unique_id'], 'service_request_emp.tpl');
	if(!$send_status and $debug){
		echo "<h1 style='color: red'>Employee Emails: SEND ERROR SEE LOG !</h1>",EOL;
	}
}

//Subscriber Emails
if($emails_request_pub){
	$send_status = send_emails($emails_request_pub, $rowRequest['system_id'], $rowRequest['system_nickname'], $rowSystemInfo['ver_unique_id'], 'service_request_pub.tpl', true);
	if(!$send_status and $debug){
		echo "<h1 style='color: red'>Subscriber Emails: SEND ERROR SEE LOG !</h1>",EOL;
	}
}


function send_emails($email_addresses = false, $system_id = false, $system_nickname = false, $system_ver_unique_id = false, $template = false, $subscriber = false){

	//$email_addresses is array of array( email, name, email_unique_id )   e.g. ('me@me.com', 'John Doe', 'aaff123f987d8e90e66ff7')

	global $settings, $debug, $send, $log, $editReq, $delReq, $smarty, $rid;

	if(!$email_addresses){
		$log->logerr("Send email info missing",0,false,basename(__FILE__),__LINE__);
		return false;
	}
	if(!$template or !$system_id or !$system_nickname or !$system_ver_unique_id){
		$log->logerr("Template, System ID, System Nickname, or System Ver UID missing",0,false,basename(__FILE__),__LINE__);
		$log->logerr("\$template=".$template,0,false,basename(__FILE__),__LINE__);
		$log->logerr("\$system_id=".$system_id,0,false,basename(__FILE__),__LINE__);
		$log->logerr("\$system_nickname=".$system_nickname,0,false,basename(__FILE__),__LINE__);
		$log->logerr("\$system_ver_unique_id=".$system_ver_unique_id,0,false,basename(__FILE__),__LINE__);
		return false;
	}
	if(is_array($email_addresses) and count($email_addresses) == 0){
		$log->logerr("Email Addresses array empty",0,false,basename(__FILE__),__LINE__);
		return false;
	}
	if($email_addresses[0]['email_unique_id'] == "" and $subscriber){
		$log->logerr("email unique id empty",0,false,basename(__FILE__),__LINE__);
		return false;
	}

//	echo "<hr><h1>Sending Email(s) using ".$template." to:</h1>",EOL;
//	echo "<pre>",EOL;
//	foreach($email_addresses as $email){
//		echo $email['email']."\n";
//	}
//	echo "</pre>",EOL;

	$email_result = "";

	$mail = new PHPMailer;
	$mail->IsSMTP();
	$mail->Host = (string)$settings->email_host;
	$mail->SMTPAuth = true;
	$mail->Username = (string)$settings->email_requests;
	$mail->Password = (string)$settings->email_password;
	$mail->SMTPSecure = 'tls';
	$mail->From = (string)$settings->email_requests;
	$mail->FromName = 'Request '.$settings->short_name.' Portal';
	if($debug){
		$mail->AddAddress(trim((string)$settings->email_support),'Support');
		$email_result .= "Email sent to: ".trim((string)$settings->email_support)."\n";
		if($subscriber){
			$unsubscribe = $settings->full_url . $settings->email_unsubscribe . "?eid=" . $email_addresses[0]['email_unique_id'] . "&svuid=" . $system_ver_unique_id;
			$smarty->assign('unsubscribe',$unsubscribe);
		}
	}else{
		foreach($email_addresses as $email){
			if($subscriber){
				$unsubscribe = $settings->full_url . $settings->email_unsubscribe . "?eid=" . $email_addresses[0]['email_unique_id'] . "&svuid=" . $system_ver_unique_id;
				//echo $unsubscribe,EOL;
				$smarty->assign('unsubscribe',$unsubscribe);
			}
			$mail->AddAddress(trim($email['email']),$email['name']);  // Add a recipient
			$email_result .= "Email sent to: ".$email['email']."\n";
		}
	}
	$mail->AddReplyTo((string)$settings->email_support, 'Support '.$settings->short_name.' Portal');
	$mail->AddCC((string)$settings->email_support);
	$mail->IsHTML(true);
	if(!$editReq){
		$mail->Subject = 'New service request for ('.$system_id.') '.$system_nickname;
	}else{
		if($delReq){
			$mail->Subject = 'Deleted service request for ('.$system_id.') '.$system_nickname;
		}else{
			$mail->Subject = 'Updated service request for ('.$system_id.') '.$system_nickname;
		}
	}
	$mail->Body = $smarty->fetch($template);

	//d($mail);

	if($debug){
		echo "Subject: ",$mail->Subject,EOL;
		echo $mail->Body,EOL,"<hr>",EOL;
	}

	if($send){
		if(!$mail->Send()) {
			echo 'Email could not be sent.';
			$email_result = "Email could not be sent!";
			echo 'Mailer Error: ' . $mail->ErrorInfo;
			$log->logerr($mail->ErrorInfo,1053,false,basename(__FILE__),__LINE__);
			exit("FAIL");
		}else{
			$log->loginfo($rid .' '. implode('; ',array_keys($mail->getAllRecipientAddresses())),200,false,basename(__FILE__),__LINE__);
		}
	}else{
		echo "Send False: Not sending email.<br />";
	}
//	echo "<hr>";
//	echo "<br /><pre>Email Result\n";
//	echo $email_result;
//	echo "<br /></pre>";
	return true;
}/*send_emails*/
function send_sms($sms_addresses = false, $sms_text = false){

	//$sms_addresses is array( cell, address )  e.g. ('123-123-1234', '@vztext.com')

	global $settings, $debug, $send, $log, $editReq, $delReq, $smarty, $rid;

	if(!$sms_addresses){
		$log->logerr("Send email info missing",0,false,basename(__FILE__),__LINE__);
		return false;
	}
	if(!$sms_text){
		$log->logerr("SMS text missing",0,false,basename(__FILE__),__LINE__);
		return false;
	}
	if(is_array($sms_addresses) and count($sms_addresses) == 0){
		$log->logerr("Email Addresses array empty",0,false,basename(__FILE__),__LINE__);
		return false;
	}

//	echo "<hr><h1>Sending SMS using to:</h1>",EOL;
//	echo "<pre>",EOL;
//	foreach($sms_addresses as $sms){
//		echo $sms['cell'].$sms['address']."\n";
//	}
//	echo "</pre>",EOL;

	$sms_result = "";

	$mail = new PHPMailer;
	$mail->IsSMTP();
	$mail->Host = (string)$settings->email_host;
	$mail->SMTPAuth = true;
	$mail->Username = (string)$settings->email_requests;
	$mail->Password = (string)$settings->email_password;
	$mail->SMTPSecure = 'tls';
	$mail->From = (string)$settings->email_requests;
	$mail->FromName = 'Request '.$settings->short_name.' Portal';
	if($debug){
		$mail->AddAddress(trim((string)$settings->email_support),'Support');
		$sms_result .= "SMS sent to: ".trim((string)$settings->email_support)."\n";
	}else{
		foreach($sms_addresses as $sms){
			$mail->AddAddress(trim($sms['email']),$sms['name']);  // Add a recipient
			$sms_result .= "SMS sent to: ".$sms['cell'].$sms['address']."\n";
		}
	}
	$mail->AddReplyTo((string)$settings->email_support, 'Support '.$settings->short_name.' Portal');
	$mail->Body = $sms_text;

	//d($mail);

	if($debug){
		echo $mail->Body,EOL,"<hr>",EOL;
	}

	if($send){
		if(!$mail->Send()) {
			echo 'Email could not be sent.';
			$sms_result = "Email could not be sent!";
			echo 'Mailer Error: ' . $mail->ErrorInfo;
			$log->logerr($mail->ErrorInfo,1053,false,basename(__FILE__),__LINE__);
			exit;
		}else{
			$log->loginfo($rid .' '. implode('; ',array_keys($mail->getAllRecipientAddresses())),200,false,basename(__FILE__),__LINE__);
		}
	}else{
		echo "Send False: Not sending sms.<br />";
	}
//	echo "<hr>";
//	echo "<br /><pre>SMS Result\n";
//	echo $sms_result;
//	echo "<br /></pre>";
	return true;
}/*send_sms*/
function limit_words($string, $word_limit){
	$words = explode(" ",$string);
	if(count($words) > $word_limit){
		return implode(" ",array_splice($words,0,$word_limit))."(more)";
	}else{
		return implode(" ",array_splice($words,0,$word_limit));
	}
}/*limit_words*/

exit("OK");
?>