<?php
//Update Completed 5/6/2015

$debug = false;
if(isset($_GET['debug'])){
	$debug = true;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

$edit = false;
$view = false;
$system_type = false;
$revision_view = false;
$revision = 1;
$revision_prev = NULL;
$revision_next = NULL;

if(isset($_GET['view'])){
	$view = true;	
}

if(isset($_GET['edit']) or $view){
	if(!isset($_GET['unique_id'])){
		$log->logerr("Unique ID not set",1048,true,basename(__FILE__),__LINE__);
	}
	$edit = true;
	$unique_id = $_GET['unique_id'];
	if(isset($_GET['rev'])){
		if(is_numeric($_GET['rev'])){
			$revision = intval($_GET['rev']);
			if($revision > 1){
				$revision_prev = $revision - 1;	
			}
			$revision_view = true;
		}
	}
}else{
	$unique_id = md5(uniqid());	
	
	if(!isset($_GET['system'])){
		$log->logerr("System Type not set",1068,true,basename(__FILE__),__LINE__);
	}
	$system_type = $_GET['system'];
}
s('Unique ID '.$unique_id);
s('System Type '.$system_type);

$options_cur = array();
$coils_cur = array();
$addons_cur = array();
$accessories_cur = array();
$addons_cur_count = 0;

if($edit){
	//Get the greatest sales quote revision and check if converted to a contract
	$sql="SELECT sq.revision AS max_rev, contracted
	FROM sales_quotes AS sq
	WHERE sq.unique_id = '123'
	ORDER BY revision DESC
	LIMIT 1;";
	s($sql);
	if(!$resultRevMax = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$rowRevMax = $resultRevMax->fetch_assoc();
	d($rowRevMax);
	
	
	//Check if converted to contract
	if(strtolower($rowRevMax['contracted']) == 'y' and !$view){
		$log->logmsg("Quote cannot be edited after it has been converted to a contract!",1070,true,basename(__FILE__),__LINE__);	
	}
	
	//Get sales quote data
	$sql="SELECT sq.*, st.name AS system_type_name, mt.name AS table_name, mc.name AS console_name, u.name AS rep_name
	FROM sales_quotes AS sq
	LEFT JOIN systems_types AS st ON st.id = sq.system_type
	LEFT JOIN misc_tables AS mt ON mt.id = sq.system_table
	LEFT JOIN misc_consoles AS mc ON mc.id = sq.system_console
	LEFT JOIN users AS u ON u.uid = sq.created_by
	WHERE sq.unique_id = '$unique_id'";
	
	if($revision_view){
		$sql.=" AND sq.revision = '$revision';";	
	}else{	
		$sql.=" ORDER BY sq.revision DESC LIMIT 1;";
	}
	s($sql);
	if(!$resultQuote = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$rowQuote = $resultQuote->fetch_assoc();
	d($rowQuote);
	$system_type = $rowQuote['system_type'];
	s($system_type);
	
	if(!$revision_view){
		$revision = intval($rowQuote['revision']);
		s('Revision '.$revision);
		if($revision > 1){
			$revision_prev = $revision - 1;	
		}
	}
	
	//Set $revision_next
	if($revision < (intval($rowRevMax['max_rev'])-1)){
		$revision_next = $revision + 1;	
	}
		
	//Current Accessories
	$sql="SELECT ma.id, ma.name, ma.notes, sqa.price, sqa.detail
	FROM sales_quotes_accessories AS sqa
	LEFT JOIN misc_accessories AS ma ON ma.id = sqa.accessories_id
	WHERE sqa.sales_quotes_id = ".$rowQuote['id'].";";
	s($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$accessories_cur_count = $result->num_rows;
	while($row = $result->fetch_assoc()){
		$accessories_cur[$row['id']] = array('name'=>$row['name'],'notes'=>$row['notes'],'price'=>$row['price'],'detail'=>$row['detail']);
	}
	d($accessories_cur);
	
	//Current Addons
	$sql="SELECT ma.id, ma.name, ma.notes, sqa.price, sqa.detail
	FROM sales_quotes_addons sqa
	LEFT JOIN misc_addons AS ma ON ma.id = sqa.addon_id
	WHERE sqa.sales_quotes_id = ".$rowQuote['id'].";";
	s($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$addons_cur_count = $result->num_rows;
	while($row = $result->fetch_assoc()){
		$addons_cur[$row['id']] = array('name'=>$row['name'],'notes'=>$row['notes'],'price'=>$row['price'],'detail'=>$row['detail']);
	}
	d($addons_cur);
	
	//Current Coils
	$sql="SELECT mc.id, mc.name, mc.notes
	FROM sales_quotes_coils AS sqc
	LEFT JOIN misc_coils AS mc ON mc.id = sqc.coil_id
	WHERE sqc.sales_quotes_id = ".$rowQuote['id'].";";
	s($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	while($row = $result->fetch_assoc()){
		$coils_cur[$row['id']] = array('name'=>$row['name'],'notes'=>$row['notes']);
	}
	d($coils_cur);
	
	//Current Options
	$sql="SELECT mso.id, mso.name, mso.description, mso.notes
	FROM sales_quotes_sw_options AS sqso
	LEFT JOIN misc_sw_options AS mso ON mso.id = sqso.option_id
	WHERE sqso.sales_quotes_id = ".$rowQuote['id'].";";
	s($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	while($row = $result->fetch_assoc()){
		$options_cur[$row['id']] = array('name'=>$row['name'],'notes'=>$row['notes'],'description'=>$row['description']);
	}
	d($options_cur);
}


//
//Load the aaaessories, coils, addons, and options into arrays for given system.
//x in the array shows that the item is also in the '_cur' array
//

$accessories = array();
$sql="SELECT ma.id, ma.name, ma.notes
FROM systems_accessories AS sa
LEFT JOIN misc_accessories AS ma ON ma.id = sa.accessories_id
WHERE sa.systems_types_id = $system_type;";
s($sql);
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
while($row = $result->fetch_assoc()){
	$x = 0;
	$price = NULL;
	$detail = NULL;
	if(array_key_exists($row['id'],$accessories_cur)){
		$x = 1;	
		$price = $accessories_cur[$row['id']]['price'];
		$detail = $accessories_cur[$row['id']]['detail'];
	}
	$accessories[$row['id']] = array('name'=>$row['name'],'notes'=>$row['notes'],'x'=>$x,'price'=>$price,'detail'=>$detail);
}
d($accessories);

$options = array();
$sql="SELECT mso.id, mso.name, mso.description, mso.notes
FROM systems_sw_options AS sso
LEFT JOIN misc_sw_options AS mso ON mso.id = sso.option_id
WHERE sso.systems_types_id = $system_type;";
s($sql);
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
while($row = $result->fetch_assoc()){
	$x = 0;
	if(array_key_exists($row['id'],$options_cur)){
		$x = 1;	
	}
	$options[$row['id']] = array('name'=>$row['name'],'notes'=>$row['notes'],'description'=>$row['description'],'x'=>$x);
}
d($options);

$coils = array();
$sql="SELECT mc.id, mc.name, mc.notes
FROM systems_coils AS sc
LEFT JOIN misc_coils AS mc ON mc.id = sc.coil_id
WHERE sc.systems_types_id = $system_type;";
s($sql);
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
while($row = $result->fetch_assoc()){
	$x = 0;
	if(array_key_exists($row['id'],$coils_cur)){
		$x = 1;	
	}
	$coils[$row['id']] = array('name'=>$row['name'],'notes'=>$row['notes'],'x'=>$x);
}
d($coils);

$sql="SELECT id, name, modality FROM systems_types WHERE id = $system_type;";
s($sql);
if(!$resultSystemTypes = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowSystemTypes = $resultSystemTypes->fetch_assoc();
d($rowSystemTypes);

$addons = array();
$sql="SELECT id, name, notes FROM misc_addons WHERE modality = ".$rowSystemTypes['modality'].";";
s($sql);
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
while($row = $result->fetch_assoc()){
	$x = 0;
	$price = NULL;
	$detail = NULL;
	if(array_key_exists($row['id'],$addons_cur)){
		$x = 1;
		$price = $addons_cur[$row['id']]['price'];
		$detail = $addons_cur[$row['id']]['detail'];
	}
	$addons[$row['id']] = array('name'=>$row['name'],'notes'=>$row['notes'],'x'=>$x,'price'=>$price,'detail'=>$detail);
}
d($addons);


$sql = "SELECT mc.id, mc.name, mc.notes
FROM systems_consoles AS sc
LEFT JOIN misc_consoles AS mc ON mc.id = sc.console_id
WHERE sc.systems_types_id = $system_type;";
s($sql);
if(!$resultConsoles = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

$sql = "SELECT mt.id, mt.name, mt.notes
FROM systems_tables AS st
LEFT JOIN misc_tables AS mt ON mt.id = st.table_id
WHERE st.systems_types_id = $system_type;";
s($sql);
if(!$resultTables = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

$sql="SELECT * FROM misc_states;";
s($sql);
if(!$resultStates = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}



?>
<!DOCTYPE html>
<html>
<head>
<?php require_once($head_include);?>
<?php require_once($css_include);?>

<link href="/resources/css/form_report.css" rel="stylesheet" type="text/css" media="screen">

<?php require_once($js_include);?>

<script src="/resources/js/jquery.ui.datepicker.js"></script>
<script src="/resources/js/jquery-ui-timepicker-addon.js"></script>
<script src="/resources/js/jquery-dynamic-form_MODIFIED.js"></script>
<script src="/resources/js/autoNumeric.js"></script>

<script type="text/javascript">
var sales_quote_json = <?php if($edit){echo json_encode($rowQuote);}else{echo 'undefined';} ?>;
var accessories_json = <?php echo json_encode($accessories); ?>;
var addons_json = <?php echo json_encode($addons); ?>;
var coils_json = <?php echo json_encode($coils); ?>;
var options_json = <?php echo json_encode($options); ?>;
var edit = <?php if($edit){echo 'true';}else{echo 'false';} ?>;
var quote_revision = <?php if($edit){echo $rowQuote['revision'];}else{echo 'undefined';} ?>;
var autoNumeric_opts_price = {aSign: '$ ', wEmpty: 'zero'};
var autoNumeric_opts_percent = {aSign: ' %', pSign: 's', vMin: '0', vMax: '100', mDec: '0', wEmpty: 'zero'};
var autoNumeric_opts_blank = {aSign: '', aSep: '', wEmpty: 'zero'};

var addon_data = [
<?php 
	$addon_data = "";
	if($addons_cur_count > 0){
		foreach($addons_cur as $id=>$addon){
			$addon_data .= '{"addon":"'.$id.'-addon", "detail":"'.$addon['detail'].'", "price":"'.$addon['price'].'" },';
		}	
	}
	if($accessories_cur_count > 0){
		foreach($accessories_cur as $id=>$accessory){
			$addon_data .= '{"addon":"'.$id.'-acc", "detail":"'.$accessory['detail'].'", "price":"'.$accessory['price'].'" },';
		}				
	}
	$addon_data = rtrim($addon_data, ",");
	echo $addon_data;
?>
];
//console.log(addon_data);

$(document).ready(function() {
	$('.price').autoNumeric('init',autoNumeric_opts_price);
	$('.percent').autoNumeric('init',autoNumeric_opts_percent);

	$(".button_jquery_save").button({ icons: { primary: "ui-icon-disk" }});	
	$(".button_jquery_convert").button({ icons: { primary: "ui-icon-transferthick-e-w" }});
	$(".button_jquery_invalidate").button({	icons: { primary: "ui-icon-trash" }});
	$(".button_jquery_prev").button({ icons: { primary: "ui-icon-seek-prev"}});
	$(".button_jquery_next").button({ icons: { primary: "ui-icon-blank", secondary: "ui-icon-seek-next"	}});
	$(".button_jquery_current").button({ icons: { primary: "ui-icon-seek-end" }});
	$(".button_jquery_calc").button({ icons: { primary: "ui-icon-refresh" }});
					
	var addonDynamicForm = $("#addon_template").dynamicForm(
	"#addon_plus", 
	"#addon_minus", 
	{ 	limit:14,
		removeColor:"red",
		createColor:"green",
		normalizeFullForm: false,
		data:addon_data,
		afterAllDone:function(count){
			$('.price').autoNumeric('update',{aSign: '$ '});
			$(".chooser").chosen({
				no_results_text: "Oops, nothing found!",
				disable_search_threshold: 10,
				placeholder_text_single: '  ',
				width: '100%'
			});
		}
	});
		
	$(".chooser").trigger("chosen:updated");
	
	$('.price').autoNumeric('update',autoNumeric_opts_price);
		

	$(".chooser").chosen({
		no_results_text: "Oops, nothing found!",
		disable_search_threshold: 10,
		placeholder_text_single: '  ',
		width: '100%'
	});

	$(function() {
		$( ".date_uid" ).datepicker({
			numberOfMonths: 1,
			showButtonPanel: true,
			changeYear: true,
			constrainInput: true,
			duration: "fast",
			dateFormat: "<?php echo dpdispfd; ?>"
		});	
	});


calc_prices();
/////////////////////////////////////////////////////////////////////////////////////
});
/////////////////////////////////////////////////////////////////////////////////////

function check_warranty(){
	if($('#warranty').val() == 'N'){
		$('#warranty_months').val('');	
	}
}

function submitcheck(contract){
	
	//return;//---------------------------------------------------------------------------------------------<<<<<<<<<<<<<<<<<<<<<<<<
	
	ids = [];
	errors = [];
	var find = ["\""];
	var replace = ["'"];
	var datere = /^20(1|2)\d-((1[0-2])|(0[1-9]))-(([0-2][0-9])|3(0|1))$/; //date yyyy-mm-dd
	var dtre = /^\d{1,10}(\.\d{1,10})?$/; //downtime 00000.00000  .00000 optional
	//var edit_car = <?php if($edit){ echo 'true';}else{ echo 'false';} ?>;
	
	//if($('#customer').val()==''){ids.push('#customer'); errors.push('Customer name blank');}
	
	console.log("ids: " + ids);
	console.log("errors: " + errors);
	showErrors(ids,errors);
	
	if(ids.length <= 0){
		if(contract){
			<?php if(!$_SESSION['mobile_device']){ ?>
				$.prompt("<h3>Convert this Sales Quote to a contract?</h3>",{
					title: "Convert Sales Quote",
					buttons: { Yes: 1, No: -1 },
					focus: 1,
					submit:function(e,v,m,f){ 
						e.preventDefault();
						showErrors(ids,errors);
						$("#errors").hide();
						if(v == 1){
							$.prompt.close();
							document.getElementById("contract").checked = true;
							$(".price, .percent").autoNumeric('update',autoNumeric_opts_blank);
							document.forms["form"].submit();
						}else{
							$.prompt.close();
						}
					}
				});
			<?php }else{ ?>
				if(confirm("Convert this Sales Quote to a contract?")){
					document.getElementById("contract").checked = true;
					$(".price, .percent").autoNumeric('update',autoNumeric_opts_blank);
					document.forms["form"].submit();
				}
			<?php } ?>					
		}else{
			<?php if(!$_SESSION['mobile_device']){ ?>
				$.prompt("<h3>Save this Sales Quote?</h3>",{
					title: "Save Sales Quote",
					buttons: { Yes: 1, No: -1 },
					focus: 1,
					submit:function(e,v,m,f){ 
						e.preventDefault();
						showErrors(ids,errors);
						$("#errors").hide();
						if(v == 1){
							$.prompt.close();
							$(".price, .percent").autoNumeric('update',autoNumeric_opts_blank);
							document.forms["form"].submit();
						}else{
							$.prompt.close();
						}
					}
				});
			<?php }else{ ?>
				if(confirm("Save this Sales Quote?")){
					$(".price, .percent").autoNumeric('update',autoNumeric_opts_blank);
					document.forms["form"].submit();
				}
			<?php } ?>	
		}		
	}
}
/////////////////////////////////////////////////////////////////////////////////////
function showErrors(ids,errors){
	//thin solid #2C3594
	$("input, select, .chosen-container").each(function(index, element) {
		$(this).animate({
			borderColor: "#2C3594",
			boxShadow: 'none'
		});
	});
	
	$.each(ids,function(index,value){
		$(value).animate({
			borderColor: "#cc0000",
			boxShadow: '0 0 5px 3px rgba(255,0,0,0.4)'
		});
	});
	$("#errors > span").html("");
	$.each(errors,function(index, value){
		$("#errors > span").append(value + "<br>");
	});
	$("#errors").show('slow');
	$(document).scrollTop(0);
}

String.prototype.replaceArray = function(find, replace) {
	var replaceString = this;
	var regex; 
	for (var i = 0; i < find.length; i++) {
		regex = new RegExp(find[i], "g");
		replaceString = replaceString.replace(regex, replace[i]);
	}
	return replaceString;
};

function delete_quote(){
	<?php if(!$_SESSION['mobile_device']){ ?>
		$.prompt("<h3>Invalidate this Sales Quote?</h3>",{
			title: "Invalidate Sales Quote",
			buttons: { Yes: 1, No: -1 },
			focus: 1,
			submit:function(e,v,m,f){ 
				e.preventDefault();
				if(v == 1){
					$.prompt.close();
					document.getElementById("delete").checked = true;
					document.forms["form"].submit();
				}else{
					$.prompt.close();
				}
			}
		});
	<?php }else{ ?>
		if(confirm("Invalidate this Sales Quote?")){
			document.getElementById("delete").checked = true;
			document.forms["form"].submit();
		}
	<?php } ?>
}

function download_quote(watermark){
	if(watermark){
		alert("DEBUG: Download not implemented yet. Watermarked.");	
	}else{
		alert("DEBUG: Download not implemented yet.");	
	}
	return;	
}

function calc_prices(){
	var ids = [];
	var errors = [];
	var payments_total_percent = 100;
	//console.log($("#fees").autoNumeric('get'));
	
	if($("#system_price").val() == ""){ ids.push('#system_price'); errors.push('System Price blank'); }
	if($("#fees").val() == ""){ ids.push('#fees'); errors.push('Fees blank'); }
	if($("#sales_tax_percent").val() == ""){ ids.push('#sales_tax_percent'); errors.push('Sales Tax blank'); }
	if($("#agreement_percent").val() == ""){ ids.push('#agreement_percent'); errors.push('Agreement Percent blank'); }
	if($("#shippment_percent").val() == ""){ ids.push('#shippment_percent'); errors.push('Install Percent blank'); }
	if($("#final_percent").val() == ""){ ids.push('#final_percent'); errors.push('Final Percent blank'); }
	$('#addon_template .calc_find').each(function(index, element) {
		if( $(this).find(".price").val() == "" ){ ids.push('#' + $(this).find(".price").attr('id')); errors.push('Addon Price ' + (index + 1) + ' Missing blank'); }
	});
		
	var agreement_percent = Number(parseInt($("#agreement_percent").autoNumeric('get')));
	var shippment_percent = Number(parseInt($("#shippment_percent").autoNumeric('get')));
	var final_percent = Number(parseInt($("#final_percent").autoNumeric('get')));
	
	if( (agreement_percent + shippment_percent + final_percent) != 100 ){
		ids.push('#agreement_percent'); errors.push('Agreement + Shipment + Final Percent is not 100%');
		ids.push('#shippment_percent');
		ids.push('#final_percent');
	}
	
	console.log("ids: " + ids);
	console.log("errors: " + errors);
	
	if(ids.length <= 0){
		$("#errors").hide();
		var fees = Number(parseFloat($("#fees").autoNumeric('get')).toFixed(2));
		var system_price = Number(parseFloat($("#system_price").autoNumeric('get')).toFixed(2));
		var sales_tax_percent = Number(parseInt($("#sales_tax_percent").autoNumeric('get'))) * 0.01;
		var agreement_percent = Number(parseInt($("#agreement_percent").autoNumeric('get'))) * 0.01;
		var shippment_percent = Number(parseInt($("#shippment_percent").autoNumeric('get'))) * 0.01;
		var final_percent = Number(parseInt($("#final_percent").autoNumeric('get'))) * 0.01;
		
		
		var addons_total = 0.0;
		$('.calc_find').each(function(index, element) {
			addons_total += Number($(this).find(".price").autoNumeric('get'));
		});
		//console.log("addons = " + String(addons_total));
		
		var sub_total = system_price + addons_total;
		//console.log("sub-total = " + String(sub_total));
		$("#sub_total").html(String(sub_total));
		
		var tax_rate = sales_tax_percent + 1;
		//console.log("tax rate = " + String(tax_rate));
		
		var total = (sub_total * tax_rate) + fees;
		//console.log("totoal = " + String(total));
		$("#total").html(String(total));
		
		$("#agreement_percent_payment").html(String(total * agreement_percent));
		$("#shippment_percent_payment").html(String(total * shippment_percent));
		$("#final_percent_payment").html(String(total * final_percent));
		
		$('.price').autoNumeric('update',autoNumeric_opts_price);
	}else{
		showErrors(ids,errors);
	}
}

/////////////////////////////////////////////////////////////////////////////////////
</script>

</head>
<body>

<?php require_once($header_include); ?>
<div id="OIReportContent"> 
<div id="stylized" class="myform">
<form id="form" name="form" method="post" enctype='multipart/form-data' action="sales_quote_do.php">
<div id="srHeaderDiv">
	<?php if(!$revision_view){?>
		<?php if($view){ ?>
			<h1>View Sales Quote</h1>
		<?php }else{ ?>
			<h1>Edit Sales Quote</h1>
		<?php } ?>
	<?php }else{ ?>
		<h1>View Sales Quote</h1>
	<?php } ?>
	<?php if($edit or $view){
		 if(strtolower($rowRevMax['contracted']) == 'y'){ ?>
	<br>
	<h1 style="color:#FF0000">!! Converted to Contract !!</h1>
	<br>
	<?php } } ?>
</div>
<div id="main"><!-- do not remove -->
<div id="errors" style="text-align:center;display:none; margin-bottom:25px; font-size:18px;">
	<h2 style="margin:0px; padding:0px; font-size:24px;">Errors to fix</h2>
	<span style="color:#F00">
	</span>
</div>

<div id="srDataDiv">
    <table id="srDataTable" class="srTable">
		<?php if($revision_view){ ?>
			<tr>
				<td colspan="2" align="right"><?php if($revision_prev != NULL){?><div class="srBottomBtn"><a class="button_jquery_prev" href="sales_quote.php?view&unique_id=<?php echo $unique_id; ?>&rev=<?php echo $revision_prev; ?>">Previous</a></div><?php } ?></td>
				<td colspan="2"><?php if($revision_next != NULL){?><div class="srBottomBtn"><a class="rightIcon button_jquery_next" href="sales_quote.php?view&unique_id=<?php echo $unique_id; ?>&rev=<?php echo $revision_next; ?>">Next</a></div><?php } ?></td>
			</tr>
		<?php } ?>
        <tr>
			<td class="rowLabel">Revision:</td>
         	<td class="rowData"><span class="boldTxt"><?php if($edit){echo intval($rowQuote['revision']);}else{echo '1';} ?><?php if($revision > 1 and !$revision_view){ ?></span>&emsp;&emsp;<a target="new" href="sales_quote.php?view&unique_id=<?php echo $unique_id; ?>&rev=<?php echo $revision_prev; ?>">View Revisions</a><?php } ?></td>
          	<td class="rowLabel">Quote ID:</td>
          	<td class="rowData"><span class="boldTxt"><?php echo $rowQuote['quote_id']; ?></span></td>
        </tr>
		<tr>
			<td class="rowLabel">Quote Title:</td>
         	<td class="rowDataWide" colspan="3"><input type="text" id="title" name="title" value="<?php if($edit){echo $rowQuote['title'];} ?>" /></td>
        </tr>
    </table>
</div>

<div id="srDataDiv">
    <table id="srDataTable" class="srTable">
		<tr>
        	<td class="rowLabelBig" colspan="4">Customer</td>
        </tr>
        <tr>
			<td class="rowLabel">Customer:</td>
         	<td class="rowData" colspan="3"><input type="text" id="customer" name="customer" value="<?php if($edit){echo $rowQuote['customer_name'];} ?>" /></td>
        </tr>
		<tr>
          	<td class="rowLabel">Address:</td>
          	<td class="rowData" colspan="3"><input type="text" id="address" name="address" value="<?php if($edit){echo $rowQuote['customer_address'];} ?>" /></td>		
        </tr>
		<tr>
			<td class="rowLabel">City:</td>
         	<td class="rowData"><input type="text" id="city" name="city" value="<?php if($edit){echo $rowQuote['customer_city'];} ?>" /></td>
			<td class="rowLabel">State:</td>
         	<td class="rowData"><select name="state" id="state" class="chooser">
									<option value=""></option>
									<?php
										while($rowStates = $resultStates->fetch_assoc()){
											echo "<option value='" . $rowStates['abv'] . "'";
											if($edit){
												if($rowQuote['customer_state']==$rowStates['abv']){echo " selected";}
											}
											echo">" . $rowStates['name'] . "</option>\n";
										}
									  ?>
								</select></td>
        </tr>
		<tr>
			<td class="rowLabel">Zip:</td>
         	<td class="rowData"><input type="text" id="zip" name="zip" value="<?php if($edit){echo $rowQuote['customer_zip'];} ?>" /></td>
          	<td class="rowLabel">Email:</td>
          	<td class="rowData"><input type="text" id="email" name="email" value="<?php if($edit){echo $rowQuote['customer_email'];} ?>" /></td>		
        </tr>
    </table>
</div>



<div id="srDataDiv">
    <table id="srDataTable" class="srTable">
        <tr>
        	<td class="rowLabelBig" colspan="4">System Info</td>
        </tr>
		<tr>
			<td class="rowLabel">System Type:</td>
         	<td class="rowData"><input type="text" id="system_type_ro" name="system_type_ro" readonly value="<?php echo $rowSystemTypes['name']; ?>" /></td>
          	<td class="rowLabel">Console:</td>
          	<td class="rowData"><select id="system_console" name="system_console" class="chooser">
								<option value=""></option>
									<?php
										while($rowConsoles = $resultConsoles->fetch_assoc()){
											echo "<option value='" . $rowConsoles['id'] . "'";
											if($edit){
												if($rowQuote['system_console']==$rowConsoles['id']){echo " selected";}
											}
											echo">" . $rowConsoles['name'] . "</option>\n";
										}
									  ?>
								</select></td>		
        </tr>
		<tr>
          	<td class="rowLabel">Table:</td>
          	<td class="rowData"><select id="system_table" name="system_table" class="chooser">
								<option value=""></option>
									<?php
										while($rowTables = $resultTables->fetch_assoc()){
											echo "<option value='" . $rowTables['id'] . "'";
											if($edit){
												if($rowQuote['system_table']==$rowTables['id']){echo " selected";}
											}
											echo">" . $rowTables['name'] . "</option>\n";
										}
									  ?>
								</select></td>
			<td class="rowLabel">System Price:</td>
         	<td class="rowData"><input class="price" type="text" id="system_price" name="system_price" value="<?php if($edit){echo $rowQuote['system_price'];} ?>" /></td>
        </tr>
		<tr>
			<td class="rowLabel">Warranty Included:</td>
         	<td class="rowData"><select id="warranty" name="warranty" class="chooser" onChange="check_warranty();">
									<option value="Y" <?php if($edit){if(strtolower($rowQuote['warranty']) == 'y'){echo 'selected';}} ?> >Yes</option>
									<option value="N" <?php if($edit){if(strtolower($rowQuote['warranty']) == 'n'){echo 'selected';}} ?> >No</option>
								</select></td>
          	<td class="rowLabel">Warranty Months:</td>
          	<td class="rowData"><input type="text" id="warranty_months" name="warranty_months" value="<?php if($edit){echo $rowQuote['warranty_months'];} ?>" /></td>		
        </tr>
    </table>
</div>

<div id="srDataDiv">
	<table id="srDataTable" class="srTable">
		<tr>
        	<td class="rowLabelBig" colspan="5">System Options</td>
        </tr>
		<tr>
			<td width="100%">
				<ul class="checkbox-grid">
					<?php
						foreach($options as $id=>$option){
							echo '<li><input type="checkbox" name="options['.$id.']" style="width:auto;" ';
							if($option['x'] == 1){echo 'checked ';}
							echo '/> '.$option['description'].'</li>';	
						}
					?>
				</ul>
			</td>	
        </tr>
    </table>
</div>

<?php if($rowSystemTypes['modality'] == '2'){ ?>
<div id="srDataDiv">
	<table id="srDataTable" class="srTable">
		<tr>
        	<td class="rowLabelBig" colspan="5">System Coils</td>
        </tr>
		<tr>
			<td width="100%">
				<ul class="checkbox-grid">
					<?php
						foreach($coils as $id=>$coil){
							echo '<li><input type="checkbox" name="coils['.$id.']" style="width:auto;" ';
							if($coil['x'] == 1){echo 'checked ';}
							echo '/> '.$coil['name'].'</li>';	
						}
					?>
				</ul>
			</td>	
        </tr>
    </table>
</div>
<?php } ?>

<div id="srDataDiv">
	<table id="srDataTable" class="srTable">
		<tr>
        	<td class="rowLabelBig" colspan="5">Add-Ons</td>
        </tr>
		<tr id="addon_template" class="calc_find">
			<td class="rowDataWide" width="250px"><label for="addon">Add-on</label>
				<select id="addon" name="addon" class="chooser">
					<option value=""></option>
					<?php 
					foreach($addons as $id=>$addon){
						echo "<option value=\"".$id."-addon\">".$addon['name']."</option>";	
					}
					foreach($accessories as $id=>$accessory){
						echo "<option value=\"".$id."-acc\">".$accessory['name']."</option>";	
					}
					?>
				</select>
			</td>
			<td class="rowDataWide"><label for="detail">Description:</label><input id="detail" name="detail" value="" /></td>
			<td class="rowDataWide" width="150px"><label for="price">Price:</label><input id="price" name="price" class="price" value="" /></td>
			<td width="50px" align="center" valign="middle"><a id="addon_minus" href=""><img src="/resources/images/red-minus-sign-25.png" width="20px" height="20px"></a> <a id="addon_plus" href=""><img src="/resources/images/green-plus-sign-25.png" width="20px" height="20px"></a></td>
        </tr>
    </table>
</div>

<div id="srServiceDiv">
    <table id="srServiceTable" class="srTable"  >
        <tr>
          <td class="rowLabelBig">Customer Notes</td>
        </tr>
        <tr>
          <td class="rowData"><textarea id="details" name="details" maxlength="4000" ><?php if($edit){echo addslashes(htmlspecialchars_decode($rowQuote['details']));}?></textarea></td>
        </tr>
    </table>
</div>

<div id="srDataDiv">
    <table id="srDataTable" class="srTable">
        <tr>
        	<td class="rowLabelBig" colspan="4">Costs</td>
        </tr>
		<tr>
          	<td colspan="2">&nbsp;</td>
			<td class="rowLabel">Sub Total:</td>
         	<td class="rowData"><span class="price" id="sub_total"></span></td>
        </tr>
		<tr>
			<td colspan="2" align="center">
			<?php if(!$revision_view){
					 if(!$view){ ?>
			<div class="srBottomBtn"><a onClick="calc_prices();" class="button_jquery_calc" >Recalculate Values</a></div>
			<?php 	 }
				   } ?>				   
			</td>
			<td class="rowLabel">Sales Tax:</td>
         	<td class="rowData"><input type="text" id="sales_tax_percent" name="sales_tax_percent" class="percent" value="<?php if($edit){echo $rowQuote['sales_tax_percent'];}else{echo "0";} ?>" /></td>	
        </tr>
		<tr>
          	<td colspan="2">&nbsp;</td>
			<td class="rowLabel">Fees:</td>
         	<td class="rowData"><input class="price" type="text" id="fees" name="fees" value="<?php if($edit){echo $rowQuote['fees'];}else{echo "0";} ?>" /></td>
        </tr>
		<tr>
			<td colspan="2">&nbsp;</td>
          	<td class="rowLabel">Total:</td>
          	<td class="rowData"><span id="total" class="price"></span></td>		
        </tr>
    </table>
	
</div>

<div id="srDataDiv">
	<table id="srDataTable" class="srTable">
		<tr>
        	<td class="rowLabelBig" colspan="6">Payments</td>
        </tr>
		<tr>
			<td>Agreement %:</td>
			<td style="padding-right:15px;"><input id="agreement_percent" name="agreement_percent" class="percent" value="<?php if($edit){ echo $rowQuote['agreement_percent']; }else{ echo $settings->agreement_percent;} ?>" /></td>
			<td>Shippment %:</td>
			<td style="padding-right:15px;"><input id="shippment_percent" name="shippment_percent" class="percent" value="<?php if($edit){ echo $rowQuote['shippment_percent']; }else{ echo $settings->shippment_percent;} ?>" /></td>
			<td>Final %:</td>
			<td style="padding-right:15px;"><input id="final_percent" name="final_percent" class="percent" value="<?php if($edit){ echo $rowQuote['final_percent']; }else{ echo $settings->final_percent;} ?>" /></td>
		</tr>
		<tr>
			<td>Payment:</td>
			<td><span class="price" id="agreement_percent_payment"></span></td>
			<td>Payment:</td>
			<td><span class="price" id="shippment_percent_payment"></span></td>
			<td>Payment:</td>
			<td><span class="price" id="final_percent_payment"></span></td>
		</tr>
	</table>
</div>

<div style="display:none">
	<?php if($debug){ ?><input name="debug" id="debug" type="hidden" value="Y" /><?php } ?>
	<?php if($edit){ ?><input type="text" id="edit" name="edit" value="Y" /><?php } ?>
    <input type="text" id="user_id" name="user_id" value="<?php echo $_SESSION['login']; ?>" />
	<input type="text" id="initials" name="initials" value="<?php echo $_SESSION['initials']; ?>" /> 
	<input type="text" id="unique_id" name="unique_id" value="<?php echo $unique_id; ?>" />	
	<input type="text" id="revision" name="revision" value="<?php echo $revision; ?>" />
	<input type="text" id="system_type" name="system_type" value="<?php echo $system_type; ?>" />	
	<input type="checkbox" id="delete" name="delete" />
	<input type="checkbox" id="contract" name="contract" />
</div>
</div>
<div id="srFooterDiv">
	<?php if(!$revision_view){
	 		if(!$view){ ?>
		<div class="srBottomBtn"><a class="button_jquery_save" onClick="submitcheck(false)">Save Quote</a></div>
		<div class="srBottomBtn"><a class="button_jquery_invalidate" onClick="delete_quote()">Invalidate Quote</a></div>
		<div class="srBottomBtn"><a class="button_jquery_convert" onClick="submitcheck(true)">Convert to Contract</a></div>
	<?php 
			}
		} ?>
		<div class="srBottomBtn" style="float:right"><a class="button_jquery_save" onClick="download_quote(<?php if($revision_view){echo 'true';} ?>)">Download</a></div>
</div>

</form>
</div> 
</div>
<?php require_once($footer_include); ?>