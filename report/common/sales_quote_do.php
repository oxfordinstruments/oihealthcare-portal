<?php
//Update Completed 05/19/2015
$debug = false;
if(isset($_GET['debug']) or $_POST['debug'] == "Y"){ 
	$debug=true; 
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}


d($_POST);

$unique_id = $_POST['unique_id'];
$delete = false;
$contract = false;
$save = false;
$action_string = "";

function delete(){
	global $mysqli, $log, $_POST, $unique_id;
	
	$sql="UPDATE sales_quotes 
	SET deleted = 'Y', deleted_by = '".$_POST['user_id']."', deleted_date = '".date(storef,time())."'
	WHERE unique_id = '$unique_id';";
	s($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	return;
}

function save($contract = false){
	global $mysqli, $log, $_POST, $unique_id;
	
	$revision = intval($_POST['revision']) + 1;
	$quote_id = $_SESSION['initials'].'-'.date('Ymd',time()).'-'.$revision;
	
	//
	//Insert Quote
	//
	$sql="INSERT INTO sales_quotes (revision, quote_id, title, customer_name, customer_address, customer_city, customer_state,
	customer_zip, customer_email, emailed_quote, system_type, system_table, system_console, system_price, warranty, warranty_months, 
	agreement_percent, shippment_percent, final_percent, sales_tax_percent, fees, details, created_by, created_date, contracted, unique_id) 
	VALUES (".$revision.",
	\"".$quote_id."\",
	\"".htmlspecialchars(addslashes($_POST['title']))."\",
	\"".htmlspecialchars(addslashes($_POST['customer']))."\",
	\"".htmlspecialchars(addslashes($_POST['address']))."\",
	\"".htmlspecialchars(addslashes($_POST['city']))."\",
	\"".$_POST['state']."\",
	\"".$_POST['zip']."\",
	\"".htmlspecialchars(addslashes($_POST['email']))."\",
	\"N\",
	\"".$_POST['system_type']."\",
	\"".$_POST['system_table']."\",
	\"".$_POST['system_console']."\",
	\"".$_POST['system_price']."\",
	\"".$_POST['warranty']."\",
	\"".$_POST['warranty_months']."\",
	\"".$_POST['agreement_percent']."\",
	\"".$_POST['shippment_percent']."\",
	\"".$_POST['final_percent']."\",
	\"".$_POST['sales_tax_percent']."\",
	\"".$_POST['fees']."\",
	\"".htmlspecialchars(addslashes($_POST['details']))."\",
	\"".$_POST['user_id']."\",
	\"".date(storef,time())."\",";
	
	if($contract){
		$sql .= "'Y',";	
	}else{
		$sql .= "'N',";
	}
	
	$sql .= "\"".$unique_id."\");";
	s($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	
	//
	//Get the inserted quote id number
	//
	$sales_quote_id = NULL;
	$sql="SELECT id FROM sales_quotes WHERE unique_id = '".$unique_id."' ORDER BY revision DESC LIMIT 1;";
	s($sql);
	if(!$resultId = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$rowId = $resultId->fetch_assoc();
	if($resultId->num_rows == 0){
		$log->logerr('Error Call Support',1069,true,basename(__FILE__),__LINE__);
	}
	$sales_quote_id = $rowId['id'];
	
	//
	//Insert options
	//
	if(count($_POST['options']) > 0){
		$sql="INSERT INTO sales_quotes_sw_options (option_id, sales_quotes_id)
		VALUES ";
		foreach($_POST['options'] as $id=>$val){
			$sql .= "(".$id.",".$sales_quote_id."),";	
		}
		//Remove the last comma
		$sql = rtrim($sql,',');
		$sql .= ";";
		s($sql);
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);	
		}		
	}
	
	//
	//Insert coils
	//
	if(count($_POST['coils']) > 0){
		$sql="INSERT INTO sales_quotes_coils (coil_id, sales_quotes_id)
		VALUES ";
		foreach($_POST['coils'] as $id=>$val){
			$sql .= "(".$id.",".$sales_quote_id."),";	
		}
		//Remove the last comma
		$sql = rtrim($sql,',');
		$sql .= ";";
		s($sql);
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}		
	}
	
	//
	//Insert Addons and Accessories
	//
	if(count($_POST['addon_template']['addon_template']) > 0){
		$sql="INSERT INTO sales_quotes_addons (addon_id, sales_quotes_id, detail, price)
		VALUES ";
		foreach($_POST['addon_template']['addon_template'] as $id=>$val){
			if(strpos($val['addon'],'-addon')){
				$sql .= "(".str_replace('-addon','',$val['addon']).",".$sales_quote_id.",\"".htmlspecialchars(addslashes($val['detail']))."\",\"".htmlspecialchars(addslashes($val['price']))."\"),";
			}
		}
		//Remove the last comma
		$sql = rtrim($sql,',');
		$sql .= ";";
		s($sql);
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
		
		$sql="INSERT INTO sales_quotes_accessories (accessories_id, sales_quotes_id, detail, price)
		VALUES ";
		foreach($_POST['addon_template']['addon_template'] as $id=>$val){
			if(strpos($val['addon'],'-acc')){
				$sql .= "(".str_replace('-acc','',$val['addon']).",".$sales_quote_id.",\"".htmlspecialchars(addslashes($val['detail']))."\",\"".htmlspecialchars(addslashes($val['price']))."\"),";
			}
		}
		//Remove the last comma
		$sql = rtrim($sql,',');
		$sql .= ";";
		s($sql);
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}	
	}
	
	return;	
}

if(isset($_POST['delete'])){
	delete();
	$action_string = "invalidated!";
	$delete = true;
}else{
	if(isset($_POST['contract'])){
		save(true);
		$action_string = "converted to a contract.";
		$contract = true;
	}else{
		save();
		$action_string = "saved.";
		$save = true;
	}
}

?>
<!DOCTYPE html>
<html>
<head>
<?php require_once($head_include);?>
<?php require_once($css_include);?>
<?php require_once($js_include);?>

<script type="text/javascript">
$(document).ready(function() {
 		
});
</script>
<script type="text/javascript">
function delayer(){
	<?php if(!$debug){?>
		window.location = "<?php echo $refering_uri; ?>"
	<?php } ?>    
}
</script>
</head>
<body onLoad="setTimeout('delayer()',10000);">
<?php require_once($header_include); ?>
<div id="OIReportContent"> 

	<h1>Sales Quote has been <?php echo $action_string; ?></h1>
	<br />
	<h1><span class="red">Page will return to home page in 3 seconds</span></h1>

</div>
<?php require_once($footer_include); ?>
