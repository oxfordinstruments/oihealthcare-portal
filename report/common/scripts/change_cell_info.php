<?php
//Update Completed 12/9/14
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/php_utils.php');

$php_utils = new phpUtils();

$fail = false;

$data = array('menu_get_sms'=>'N','menu_cell'=>NULL,'menu_carrier'=>NULL);

foreach($_POST as $key=>$value){
	if(array_key_exists($key,$data)){
		$data[$key] = $value;	
	}
}

$sql="UPDATE users SET ";
if(!is_null($data['menu_cell'])){
	$sql .= "`cell`='".$data['menu_cell']."',";
}
if(!is_null($data['menu_carrier'])){
	$sql .="`carrier`='".$data['menu_carrier']."',";
}
$sql = rtrim($sql, ',');
$sql .= " WHERE uid = '".$_SESSION['login']."';";
if(!$result = $mysqli->query($sql)){
	$fail = true;
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

$sql="DELETE
FROM users_prefs
WHERE uid = '".$_SESSION['login']."' AND pid = (
SELECT id
FROM users_pref_id
WHERE pref = 'pref_rcv_sms');";
if (!$result = $mysqli->query($sql)) {
	$fail = true;
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}


if(strtolower($data['menu_get_sms']) == 'y'){
	$sql="INSERT INTO users_prefs (uid,pid)
	SELECT '".$_SESSION['login']."', id
	FROM users_pref_id
	WHERE pref = 'pref_rcv_sms';";
	if (!$result = $mysqli->query($sql)) {
		$fail = true;
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
}

if($fail){
	echo "0";
}else{
	$php_utils->session_update($mysqli);
	echo "1";
}
//echo print_r($_POST),"\n\n",$sql;
?>