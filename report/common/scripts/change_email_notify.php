<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/php_utils.php');

$php_utils = new phpUtils();

$fail = false;

$sql="SELECT pref FROM users_pref_id;";
if(!$result = $mysqli->query($sql)){
	$fail = true;
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

while($row = $result->fetch_assoc()){
	$data[$row['pref']] = 'N';
}

foreach($_POST as $key=>$value){
	if(array_key_exists($key,$data)){
		$data[$key] = "Y";	
	}
}

$sql="DELETE
FROM users_prefs
WHERE uid = '".$_SESSION['login']."' AND pid IN (
SELECT CONCAT_WS(',', id)
FROM users_pref_id
WHERE ";
foreach($data as $key=>$value){
	$sql .= "pref = '$key' OR ";
}
$sql = rtrim($sql,' OR ');
$sql .= ");";
//echo $sql.'<br><br>';
if(!$result = $mysqli->query($sql)){
	$fail = true;
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

foreach($data as $key=>$value){
	if(strtolower($value) == 'y'){
		$sql="INSERT INTO users_prefs (uid,pid)
		SELECT '".$_SESSION['login']."', id
		FROM users_pref_id
		WHERE pref = '$key';";
		//echo $sql.'<br><br>';
		if(!$result = $mysqli->query($sql)){
			$fail = true;
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
	}
}

if($fail){
	echo "0";
}else{
	$php_utils->session_update($mysqli);
	echo "1";
}
//echo print_r($_POST);
?>