<?php
//Update Completed 12/9/14
$debug = false;

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/ldap.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$logger = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

$pwd_new_1=$_POST['password1'];
$pwd_new_2=$_POST['password2'];
$login_uid=$_SESSION["login"];

if($debug){
	s('DEBUGGING!');
	$pwd_new_1 = randStrGen(8);
	$pwd_new_2 = $pwd_new_1;
	$_SESSION['user_settings'] = '';	
	s($pwd_new_1);
}

$pwd_chg_date = strtotime('+6 months', time());
$pwd_chg_date = date(storef,$pwd_chg_date);

$refering_uri = preg_replace("/[?&](blank|requirement|pwdchg|old|match|emailchg|error)/", "", $refering_uri);
$refering_uri_temp = strrev($refering_uri);
if( $refering_uri_temp == "/"){
	$refering_uri = preg_replace(strrev("#/#"),strrev("/index.php"),strrev($refering_uri),1);
	$refering_uri = strrev($refering_uri);
}  
s($refering_uri);

$_SESSION['user_settings']='';

$salt = "STyf1anSTydajgnmd";
$encrypted_password_new = "{MD5}" . base64_encode( pack( "H*", md5( $pwd_new_1 . $salt ) ) );
//$encrypted_password_old = "{MD5}" . base64_encode( pack( "H*", md5( $pwd_old . $salt ) ) );

if($pwd_new_1!=$pwd_new_2){
	$_SESSION['user_settings']='match';
	if($debug){
		echo EOL,EOL,"location:$refering_uri",EOL;
		d($_SESSION['user_settings']);
	}else{
		header("location:$refering_uri");
	}
	return false;
}
	  

if(!preg_match('/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{6,100}$/', $pwd_new_1)) {
	$_SESSION['user_settings']='requirement';
	if($debug){
		echo EOL,EOL,"location:$refering_uri",EOL;
		d($_SESSION['user_settings']);
	}else{
		header("location:$refering_uri");
	}
	return false;
}

$sql="SELECT u.uid, u.name, u.lastname, u.email, 
CONCAT_WS('', MAX(IF(pid.perm = 'perm_dms_access', 'Y', 'N'))) AS perm_dms_access, 
CONCAT_WS('', MAX(IF(pid.perm = 'perm_kb_access', 'Y', 'N'))) AS perm_kb_access
FROM users AS u
LEFT JOIN users_perms AS up ON up.uid = u.uid
LEFT JOIN users_perm_id AS pid ON pid.id = up.pid
LEFT JOIN users_groups AS ug ON ug.uid = u.uid
LEFT JOIN users_group_id AS gid ON gid.id = ug.gid
WHERE u.uid = '$login_uid'
GROUP BY u.uid;";
d($sql);
if(!$result = $mysqli->query($sql)){
	$logger->logerr($sql,1000);
	$logger->logerr('There was error running the query ['.$mysqli->error.']',1000,true,basename(__FILE__),__LINE__);	
}
$count=$result->num_rows;
d($count);
$row = $result->fetch_assoc(); 
d($row);

if($count == 1){
	if($row['pwd'] == $encrypted_password_new){
		$_SESSION['user_settings']='old';
		if($debug){
			echo EOL,EOL,"location:$refering_uri",EOL;
			d($_SESSION['user_settings']);
		}else{
			header("location:$refering_uri");
		}
		return false;
	}else{
		$sql="UPDATE users SET pwd = '$encrypted_password_new', pwd_chg_date = '$pwd_chg_date', pwd_chg_remind = 0 WHERE uid = '$login_uid';";
		d($sql);
		if(!$result = $mysqli->query($sql)){
			$logger->logerr($sql,1000);
			$logger->logerr('There was error running the query ['.$mysqli->error.']',1000,true,basename(__FILE__),__LINE__);	
		}
		$_SESSION['user_settings']='pwdchg';
		
		$ldap = new ldapUtil();
	
		$logger->logmsg('Running ldap ops in pwdChange');
		
		if(strtolower($row['perm_kb_access']) == 'y'){
			$kb = true;	
		}else{
			$kb = false;
		}
		
		if(strtolower($row['perm_dms_access']) == 'y'){
			$dms = true;	
		}else{
			$dms = false;
		}
		d($row);
		s('Running ldap operations');
		$ldap->user($_SESSION['login'],$encrypted_password_new,$row['name'],$row['lastname'],$row['email'],$kb,$dms);
		if($debug){
			echo EOL,EOL,"location:$refering_uri",EOL;
			d($_SESSION['user_settings']);
		}else{
			header("location:$refering_uri");
		}
		return false;
	}
}else{
	$_SESSION['user_settings']='error';
	if($debug){
		echo EOL,EOL,"location:$refering_uri",EOL;
		d($_SESSION['user_settings']);
	}else{
		header("location:$refering_uri");
	}
	return false;
}


function randStrGen($len){
    $result = "";
    $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
    $charArray = str_split($chars);
    for($i = 0; $i < $len; $i++){
	    $randItem = array_rand($charArray);
	    $result .= "".$charArray[$randItem];
    }
    return $result;
}

?>
