<?php

/*
0 	LDAP_DEREF_NEVER
1 	LDAP_DEREF_SEARCHING
2 	LDAP_DEREF_FINDING
3 	LDAP_DEREF_ALWAYS
*/

//error_reporting(E_ALL);
if(substr(php_sapi_name(), 0, 3) == 'cli' || empty($_SERVER['REMOTE_ADDR'])) {
    //shell
	$cron = false;
	if(isset($_SERVER['argv'][1]) and $_SERVER['argv'][1] == '--cron'){
		$cron = true;
	}
	$docroot = preg_replace("/(public_html).*/",'public_html',dirname(__FILE__),2);
	$no_define_eol = true;
	define('EOL',PHP_EOL);
} else {
    //webserver
	$cron = false;
	$docroot = $_SERVER['DOCUMENT_ROOT'];
	define('EOL','<br />');
}

require_once($docroot.'/log/log.php');

class ldapUtil{
	
	public $logger;
	protected $addUserUIDNum = 0;
	protected $debug = false;
	protected $ldapconn = NULL;
	
	public $options = array(
		'host'	=> 	'localhost',
		'adminDN'	=>	'cn=admin,dc=oihealthcareportal,dc=com',
		'adminPW'	=>	'Plat1num',
		'baseDN'	=>	'dc=oihealthcareportal,dc=com',
		'dmsDN'		=>	'ou=dms,ou=portal,dc=oihealthcareportal,dc=com',
		'kbDN'		=>	'ou=kb,ou=portal,dc=oihealthcareportal,dc=com',
		'deref'		=>	0,
		'version'	=>	3,
		'debug'		=>	false
	);
	
	//
	//Constructor called when class is created
	//
	public function __construct($docroot = NULL){
		if(is_null($docroot)){
			$docroot = $_SERVER['DOCUMENT_ROOT'];	
		}
		$this->logger = new logger($docroot);
		$this->logger->logldapblankline(3);
		if($this->debug){
			$this->options['debug'] = true;
		}
		$settings = new SimpleXMLElement($docroot.'/settings.xml', null, true);
		$this->options['host'] = (string)$settings->ldap->host;
		$this->options['adminDN'] = (string)$settings->ldap->admin_dn;
		$this->options['adminPW'] = (string)$settings->ldap->admin_dn_pwd;
		$this->options['baseDN'] = (string)$settings->ldap->base_dn;
		$this->options['dmsDN'] = (string)$settings->ldap->dms_dn;
		$this->options['kbDN'] = (string)$settings->ldap->kb_dn;
		$this->options['deref'] = (string)$settings->ldap->deref;
		$this->options['version'] = (string)$settings->ldap->version;

	}
	
	//
	//ldap_conn
	//
	protected function ldap_conn(){	
		if(!$ldapconn = ldap_connect($this->options['host'])){
			$this->logger->logldap('Connected to LDAP server');
			die();
		}
		ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, $this->options['version']);
		ldap_set_option($ldapconn, LDAP_OPT_DEREF, $this->options['deref']);
		//ldap_set_option($ldapconn, LDAP_OPT_REFERRALS, 0);
		
		if($ldapconn){
			$this->ldapconn = $ldapconn;
			return true;		
		}
		$this->logger->logldap('Connect failed');
		return false;		
	}
	
	public function ldap_disconnect(){
		$this->logger->logldap(__FUNCTION__);
		if($this->ldapconn != NULL){
			ldap_close($this->ldapconn);	
		}
		$this->ldapconn = NULL;
		//$this->logger->logldap('Connection to LDAP server closed');
	}
	
	public function ldapBindAdmin(){
		if($this->ldapconn === NULL){
			if(!$this->ldap_conn()){
				$this->logger->logldap('No connection. trying to connect failed');
				return false;
			}			
		}
		$ldapbind = ldap_bind($this->ldapconn,$this->options['adminDN'],$this->options['adminPW']);
		if($ldapbind){
				return true;
		}else{
			$this->logger->logldap(__FUNCTION__.': Bound admin failed');
			return false;	
		}
	}
	
	//
	//user add/update
	//
	public function user($uid,$pwd,$fullname,$lastname,$email,$kb = false,$dms = false){
		if($this->options['debug']){echo EOL,EOL,"Function: ",__FUNCTION__,EOL;}
		if($this->options['debug']){echo __FUNCTION__." args: ",print_r(func_get_args()),EOL;}
		
		$this->logger->logldapblankline(1);
		$this->logger->logldap("Adding/Updating uid: ".$uid);
		
		if($uid == NULL or $pwd == NULL or $fullname == NULL or $lastname == NULL or $email == NULL){
			$this->logger->logldap('Missing data in user');
			$this->logger->logldap('Data: uid='.$uid.' pwd='.$pwd.' fullname='.$fullname.' lastname='.$lastname.' email='.$email);
			return false;	
		}
		
		if($this->userExist($this->options['baseDN'],$uid)){
			$this->logger->logldap('User exists '.$uid);
			$this->updateuser($uid,$pwd,$fullname,$lastname,$email);
		}else{		
			
			if(!$this->ldapBindAdmin()){
				$this->logger->logldap(__FUNCTION__.': Bound admin failed');
				return false;	
			}else{
				if($this->debug){
					$this->logger->logldap(__FUNCTION__.': Bound to admin');
				}
			}		
			
			$largestUID = $this->findLargestUidNumber($this->options['baseDN']);
			$uidNumber = intval($largestUID) + 1;
			$this->addUserUIDNum = $uidNumber;
			
			// prepare data
			$info['objectClass'][0] = "posixAccount";
			$info['objectClass'][1] = "top";
			$info['objectClass'][2] = "inetOrgPerson";
			$info['gidNumber'] = "1";//required
			$info['givenName'] = $fullname;
			$info['sn'] = $lastname;//required
			$info['displayName'] = $fullname;
			$info['uid'] = $uid;
			$info['homeDirectory'] = "/tmp";//required
			$info['mail'] = $email;
			$info['cn'] = $fullname;//required //displayed name on dms
			$info['uidNumber'] = $uidNumber;//required
			$info['userPassword'] = $pwd;
			
			// add data to directory
			if(ldap_add($this->ldapconn, "uid=".$uid.','.$this->options['baseDN'],$info)){
				$this->logger->logldap('User '.$uid.' added');
			}else{
				$this->logger->logldap('Error user '.$uid.' exists cannot add');
				return false;
			}	
		}
		$this->userKB($uid,$pwd,$fullname,$lastname,$email,$kb);
		$this->userDMS($uid,$dms);
		return true;
	}
	
	//
	//userDMS
	//
	public function userDMS($uid,$add = false){
		if($this->options['debug']){echo EOL,EOL,"Function: ",__FUNCTION__,EOL;}
		if($this->options['debug']){echo __FUNCTION__." args: ",print_r(func_get_args()),EOL;}
		
		$this->logger->logldap("Adding/Updating/Deleting DMS uid: ".$uid);
		if($add){
			if($this->userExist($this->options['dmsDN'],$uid)){
				$this->logger->logldap('DMS User '.$uid.' exists. No need to add.');
				return true;	
			}
	
			if(!$this->ldapBindAdmin()){
				$this->logger->logldap(__FUNCTION__.': Bound admin failed');
				return false;	
			}else{
				if($this->debug){
					$this->logger->logldap(__FUNCTION__.': Bound to admin');
				}
			}
			
			// prepare data
			$info['objectClass'][0] = "top";
			$info['objectClass'][1] = "alias";
			$info['objectClass'][2] = "extensibleObject";
			$info['aliasedObjectName'] = "uid=".$uid.','.$this->options['baseDN'];
			$info['uid'] = $uid;
	
			// add data to directory
			if(ldap_add($this->ldapconn,"uid=".$uid.','.$this->options['dmsDN'],$info)){
				$this->logger->logldap('DMS user '.$uid.' added');
				return true;
			}else{
				$this->logger->logldap('DMS user '.$uid.' alrady exists');
				return false;
			}
		}else{
			$this->logger->logldap("Removing DMS uid: ".$uid);
				
			if(!$this->ldapBindAdmin()){
				$this->logger->logldap(__FUNCTION__.': Bound admin failed');
				return false;	
			}else{
				if($this->debug){
					$this->logger->logldap(__FUNCTION__.': Bound to admin');
				}
			}
			
			if(!$this->userExist($this->options['dmsDN'],$uid)){
				$this->logger->logldap('DMS User '.$uid.' does not exist');
				return true;	
			}
			
			if(ldap_delete($this->ldapconn,'uid='.$uid.','.$this->options['dmsDN'])){
				$this->logger->logldap('DMS User '.$uid.' removed');
				return true;
			}else{
				$this->logger->logldap('Error DMS User '.$uid.' delete failed');
				return false;
			}
		}
	}
	
	//
	//userKB
	//
	public function userKB($uid,$pwd = NULL,$fullname = NULL,$lastname = NULL,$email = NULL,$add = false){
		if($this->options['debug']){echo EOL,EOL,"Function: ",__FUNCTION__,EOL;}
		if($this->options['debug']){echo __FUNCTION__." args: ",print_r(func_get_args()),EOL;}
		
		$this->logger->logldap("Adding/Updating/Deleting KB uid: ".$uid);
		if($add){
			if($this->userExist($this->options['kbDN'],$uid)){
				$this->logger->logldap('KB User '.$uid.' exists');
				$this->updateuserKB($uid,$pwd,$fullname,$lastname,$email);
				return true;	
			}
	
			if(!$this->ldapBindAdmin()){
				$this->logger->logldap(__FUNCTION__.': Bound admin failed');
				return false;	
			}else{
				if($this->debug){
					$this->logger->logldap(__FUNCTION__.': Bound to admin');
				}
			}
			
			if($pwd == NULL or $fullname == NULL or $lastname == NULL or $email == NULL){
				$this->logger->logldap('Missing data in userKB');
				$this->logger->logldap('Data: pwd='.$pwd.' fullname='.$fullname.' lastname='.$lastname.' email='.$email);
				return false;	
			}
			
			$largestUID = $this->findLargestUidNumber($this->options['baseDN']);
			$uidNumber = intval($largestUID) + 1;
			$this->logger->logldap('uidNumber = '.$uidNumber);
			
			// prepare data
			$info['objectClass'][0] = "posixAccount";
			$info['objectClass'][1] = "top";
			$info['objectClass'][2] = "inetOrgPerson";
			$info['gidNumber'] = "1";//required
			$info['givenName'] = $fullname;
			$info['sn'] = $lastname;//required
			$info['displayName'] = $fullname;
			$info['uid'] = $uid;
			$info['homeDirectory'] = "/tmp";//required
			$info['mail'] = $email;
			$info['cn'] = $fullname;//required //displayed name on dms
			$info['uidNumber'] = $uidNumber;//required
			$info['userPassword'] = $pwd;
			
			if($this->options['debug']){echo "KB Info: ",print_r($info),EOL; };
					
			if(!$this->ldapBindAdmin()){
				$this->logger->logldap(__FUNCTION__.': Bound admin failed');
				return false;	
			}else{
				if($this->debug){
					$this->logger->logldap(__FUNCTION__.': Bound to admin');
				}
			}
			
			// add data to directory
			if(ldap_add($this->ldapconn, "uid=".$uid.','.$this->options['kbDN'],$info)){
				$this->logger->logldap('KB user ',$uid,' added');
				return true;
			}else{
				$this->logger->logldap('Error KB user ',$uid.' already exists');
				return false;
			}
		}else{
			$this->logger->logldap("Removing KB uid: ".$uid);
				
			if(!$this->ldapBindAdmin()){
				$this->logger->logldap(__FUNCTION__.': Bound admin failed');
				return false;	
			}else{
				if($this->debug){
					$this->logger->logldap(__FUNCTION__.': Bound to admin');
				}
			}
			
			if(!$this->userExist($this->options['kbDN'],$uid)){
				$this->logger->logldap('KB User '.$uid.' does not exist');
				return true;	
			}
			
			if(ldap_delete($this->ldapconn,'uid='.$uid.','.$this->options['kbDN'])){
				$this->logger->logldap('KB User '.$uid.' removed');
				return true;
			}else{
				$this->logger->logldap('Error KB User '.$uid.' delete failed');
				return false;
			}
		}
	}
	
	//
	//updateuser
	//
	public function updateuser($uid,$pwd,$fullname,$lastname,$email){
		if($this->options['debug']){echo EOL,EOL,"Function: ",__FUNCTION__,EOL;}
		if($this->options['debug']){echo __FUNCTION__." args: ",print_r(func_get_args()),EOL;}
		
		$this->logger->logldap("Updating user: ".$uid);

		if(!$this->ldapBindAdmin()){
			$this->logger->logldap(__FUNCTION__.': Bound admin failed');
			return false;	
		}else{
			if($this->debug){
				$this->logger->logldap(__FUNCTION__.': Bound to admin');
			}
		}
		
		// prepare data
		$info['gidNumber'] = "1";//required
		$info['givenName'] = $fullname;
		$info['sn'] = $lastname;//required
		$info['displayName'] = $fullname;
		$info['uid'] = $uid;
		$info['homeDirectory'] = "/tmp";//required
		$info['mail'] = $email;
		$info['cn'] = $fullname;//required //displayed name on dms
		$info['userPassword'] = $pwd;
			
		if(ldap_modify($this->ldapconn,  "uid=".$uid.','.$this->options['baseDN'],$info)){
			$this->logger->logldap('User '.$uid.' info updated');
			return true;
		}else{
			$this->logger->logldap('Error User ',$uid.' update failed');
			return false;
		}	
	}
	
	//
	//updateuserKB
	//
	public function updateuserKB($uid,$pwd,$fullname,$lastname,$email){
		if($this->options['debug']){echo EOL,EOL,"Function: ",__FUNCTION__,EOL;}
		if($this->options['debug']){echo __FUNCTION__." args: ",print_r(func_get_args()),EOL;}
		
		$this->logger->logldap("Updating KB uid: ".$uid);
		if(!$this->userExist($this->options['kbDN'],$uid)){
			$this->logger->logldap('User '.$uid.' does not exist');
			return false;	
		}

		if(!$this->ldapBindAdmin()){
			$this->logger->logldap(__FUNCTION__.': Bound admin failed');
			return false;	
		}else{
			if($this->debug){
				$this->logger->logldap(__FUNCTION__.': Bound to admin');
			}
		}
		
		$info['gidNumber'] = "1";//required
		$info['givenName'] = $fullname;
		$info['sn'] = $lastname;//required
		$info['displayName'] = $fullname;
		$info['uid'] = $uid;
		$info['homeDirectory'] = "/tmp";//required
		$info['mail'] = $email;
		$info['cn'] = $fullname;//required //displayed name on dms
		$info['userPassword'] = $pwd;
			
		if(ldap_modify($this->ldapconn, "uid=".$uid.','.$this->options['kbDN'],$info)){
			$this->logger->logldap('KB user '.$uid.' info updated');
			return true;
		}else{
			$this->logger->logldap('KB user '.$uid.' update failed');
			return false;
		}	
	}

		
	//
	//deluser
	//
	public function deluser($uid){
		if($this->options['debug']){echo EOL,EOL,"Function: ",__FUNCTION__,EOL;}
		if($this->options['debug']){echo __FUNCTION__." args: ",print_r(func_get_args()),EOL;}
		
		$this->logger->logldap("Delete user: ".$uid);
				
		if(!$this->ldapBindAdmin()){
			$this->logger->logldap(__FUNCTION__.': Bound admin failed');
			return false;	
		}else{
			if($this->debug){
				$this->logger->logldap(__FUNCTION__.': Bound to admin');
			}
		}
		
		$this->userDMS($uid,false);
		$this->userKB($uid,false);
		if(ldap_delete($this->ldapconn,'uid='.$uid.','.$this->options['baseDN'])){
			$this->logger->logldap('User '.$uid.' removed');
			return true;
		}else{
			$this->logger->logldap('Error User '.$uid.' delete failed');
			return false;
		}		
	}
	

	
	//
	//userList
	//
	public function userList($dn,$filter,$attr = array(),$ref = 0){
		if($this->options['debug']){echo EOL,EOL,"Function: ",__FUNCTION__,EOL;}
		if($this->options['debug']){echo __FUNCTION__." args: ",print_r(func_get_args()),EOL;}
		
		if($ref != 0){
			$cur_ref = $this->options['deref'];
			$this->options['deref'] = $ref;
			if($this->options['debug']){
				$this->logger->logldap("changing deref from: ",$cur_ref," to: ",$ref);
			}
			$this->ldap_disconnect();
		}else{
			$cur_ref = 0;	
		}

		if(!$this->ldapBindAdmin()){
			//if($this->options['debug']){echo "Bind admin failed.",EOL;}
			$this->logger->logldap(__FUNCTION__.': Bound admin failed');
			$this->options['deref'] = $cur_ref;
			$this->ldap_disconnect();
			return false;	
		}else{
			if($this->debug){
				$this->logger->logldap(__FUNCTION__.': Bound to admin');
			}
		}
		
		if(!$sr = ldap_list($this->ldapconn, $dn, $filter, $attr)){
			if($this->options['debug']){
				$this->logger->logldap($sr);
				$this->logger->logldap('ldap_list failed');
			}
			$this->options['deref'] = $cur_ref;
			$this->ldap_disconnect();
			return false;	
		}
		

		$info = ldap_get_entries($this->ldapconn, $sr);
		if($this->options['debug']){
			echo "<pre>";
			print_r($info);	
			echo "</pre>",EOL;
		}
		
		
		$this->options['deref'] = $cur_ref;
		return $info;
	}
	
	//
	//userExist
	//
	public function userExist($dn,$uid,$count = false,$deref = false){
		if($this->options['debug']){echo EOL,EOL,"Function: ",__FUNCTION__,EOL;}
		if($this->options['debug']){echo __FUNCTION__." args: ",print_r(func_get_args()),EOL;}
		
		$listArr = $this->userList($dn,'(uid='.$uid.')',array('uid'),($deref)?3:0);	
		if(count($listArr) == 0){
			//if($this->options['debug']){echo "listArr is empty.",EOL;}
			$this->logger->logldap('listArr is empty');
			return false;
		}
		if($count){
			if(intval($listArr['count']) > 0){
				//if($this->options['debug']){echo "users found: ",$listArr['count'],EOL;}
				$this->logger->logldap('Users found: '.$listArr['count']);
				return intval($listArr['count']);	
			}else{
				//if($this->options['debug']){echo "users found: 0",EOL;}
				$this->logger->logldap('Users found: 0');
				return 0;
			}
		}else{
			if(intval($listArr['count']) > 0){
				//if($this->options['debug']){echo "user found",EOL;}
				$this->logger->logldap('User '.$uid.' found');
				return true;	
			}else{
				//if($this->options['debug']){echo "user not found",EOL;}
				$this->logger->logldap('User '.$uid.' not found');
				return false;
			}	
		}
		
	}
	
	//
	//findLargestUidNumber
	//
	private function findLargestUidNumber($dn){
		if($this->options['debug']){echo EOL,EOL,"Function: ",__FUNCTION__,EOL;}
		if($this->options['debug']){echo __FUNCTION__." args: ",print_r(func_get_args()),EOL;}
		
		$this->logger->logldap("findLargestUidNumber dn: ".$dn);
				
		if(!$this->ldapBindAdmin()){
			//if($this->options['debug']){echo "Bind admin failed.",EOL;}
			$this->logger->logldap(__FUNCTION__.': Bound admin failed');
			return false;	
		}else{
			if($this->debug){
				$this->logger->logldap(__FUNCTION__.': Bound to admin');
			}
		}
		
		if($s = ldap_search($this->ldapconn,$dn,'uidNumber=*')){
			if(!ldap_sort($this->ldapconn, $s, "uidNumber")){
				//if($this->options['debug']){echo "ldap_sort failed.",EOL;}
				$this->logger->logldap('ldap_sort failed');
				return false;
			}
			
			if($result = ldap_get_entries($this->ldapconn, $s)){
				//if($this->options['debug']){echo "ldap_get_entries failed.",EOL;}
				$this->logger->logldap('ldap_get_entries failed');
				return false;	
			}
			$count = $result['count'];
			$biguid = $result[$count-1]['uidnumber'][0];
			return $biguid;
		}
		//if($this->options['debug']){echo "largest uidNumner not found.",EOL;}
		$this->logger->logldap('largest uidNumber not found');
		return null;
   	}
}

?>