<?php 
//Update Completed 12/9/14
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();


$sql="SELECT rc.id, rc.eth0_mac, rc.eth0_ip, rc.geo_state, rc.geo_country, rc.geo_timezone FROM remote_computers AS rc;";
if(!$resultRemote = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);	
}

?>
<!doctype html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<?php require_once($js_include);?>

<script type="text/javascript">
$(document).ready(function() {
	allTable = $('#allSitesTable').dataTable({
		"bJQueryUI": true,
		"bStateSave": false,
		"sPaginationType": "full_numbers",
		"fnInitComplete": function () {
           		 this.$('tr').click( function () {
               		 var href = $(this).find("a").attr("href");
					 if(href) {
						window.location = href;
					 }
           		 });
       		}
	});	
});
function select_id(id){
	$('#remote_eth0_mac',top.document).val(id);
	parent.$.fancybox.close();
}
</script>
</head>
<body>
<div id="allSitesDiv" style="width:100%;">
 <table width="100%" id="allSitesTable">
	<thead>
		<tr>
			<th>Serial</th>
			<th>Remote LAN IP</th>
			<th>Geo State</th>
			<th>Geo Country</th>
			<th>Geo Timezone</th>
		</tr>
	</thead>
	<tbody>
		<?php
			while($rowRemote = $resultRemote->fetch_assoc())
			{
				echo "<tr onclick=\"javascript: select_id('".$rowRemote['eth0_mac']."');\">\n";	
				echo "<td>". $rowRemote['eth0_mac']."</td>\n";
				echo "<td>". $rowRemote['eth0_ip']."</td>\n";	
				echo "<td>". $rowRemote['geo_state']."</td>\n";
				echo "<td>". $rowRemote['geo_country']."</td>\n";
				echo "<td>". $rowRemote['geo_timezone']."</td>\n";
				echo "</tr>\n";
			}
			?>     
	</tbody>
</table>
</div>
</body>
</html>