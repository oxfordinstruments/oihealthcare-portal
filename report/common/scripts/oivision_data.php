<?php
/*
This is for retreiving data from the oi-vision.com website
*/
//error_reporting(E_ALL);

$no_define_eol = true;
if(substr(php_sapi_name(), 0, 3) == 'cli' || empty($_SERVER['REMOTE_ADDR'])) {
    //shell
	$cron = false;
	if(isset($_SERVER['argv'][1]) and $_SERVER['argv'][1] == '--cron'){
		$cron = true;
	}
	$docroot = preg_replace("/(public_html).*/",'public_html',dirname(__FILE__),2);
	$no_define_eol = true;
	define('EOL',PHP_EOL);
} else {
    //webserver
	$cron = false;
	$docroot = $_SERVER['DOCUMENT_ROOT'];
	define('EOL','<br />');
}


require_once($docroot.'/log/log.php');


class OiVision {
	public $logger;
	private $mysqli = false;
	private $debug = false;
	protected $host = '67.199.146.103';
	protected $username = 'oivisi5_report';
	protected $password = 'Plat1num';
	protected $db_name = 'oivisi5_mm';
	
	
	//
	// Constructor called when class is created
	//
	public function __construct($docroot = NULL){
		if(is_null($docroot)){
			$docroot = $_SERVER['DOCUMENT_ROOT'];	
		}
		$this->logger = new logger($docroot);	
	}
	
	public function get_readings($mac){
		if(empty($mac)){
			$this->logger->logerr('mac for get_readings is empty',0,false,basename(__FILE__),__LINE__);
			return false;	
		}

		if (!$this->connect()) {
			$this->logger->logerr('Failed to connect',1000,false,basename(__FILE__),__LINE__);
			return false;
		}
		
		$sql = "SELECT * FROM sites WHERE mac = '$mac';";
		
		if(!$result = $this->mysqli->query($sql)){
			$this->logger->logerr($mysqli->error,1000,false,basename(__FILE__),__LINE__);
			$this->disconnect();
			return false;
		}
		
		$site_info = $result->fetch_assoc();
		
		$sql = "SELECT * FROM `".$mac."` ORDER BY `index` DESC LIMIT 1;";
		
		if(!$result = $this->mysqli->query($sql)){
			$this->logger->logerr($mysqli->error,1000,false,basename(__FILE__),__LINE__);
			$this->disconnect();
			return false;
		}
		
		$reading = $result->fetch_assoc();
		
		if($this->debug){echo "<pre>".print_r($row)."</pre>";}
		$this->disconnect();
		return array('site'=>$site_info, 'reading'=>$reading);

	}
	
	public function get_history($mac){
		if(empty($mac)){
			$this->logger->logerr('mac for get_readings is empty',0,false,basename(__FILE__),__LINE__);
			return false;	
		}

		if (!$this->connect()) {
			$this->logger->logerr('Failed to connect',1000,false,basename(__FILE__),__LINE__);
			return false;
		}
		
		$sql = "SELECT * FROM sites WHERE mac = '$mac';";
		
		if(!$result = $this->mysqli->query($sql)){
			$this->logger->logerr($mysqli->error,1000,false,basename(__FILE__),__LINE__);
			$this->disconnect();
			return false;
		}
		
		$site_info = $result->fetch_assoc();
		
		$sql = "SELECT * FROM `".$mac."` ORDER BY `index` DESC LIMIT 100;";
		
		if(!$result = $this->mysqli->query($sql)){
			$this->logger->logerr($mysqli->error,1000,false,basename(__FILE__),__LINE__);
			$this->disconnect();
			return false;
		}
		
		$data = array();
		$fields = $result->fetch_fields();
		while($row = $result->fetch_assoc()){
			if($this->debug){echo "<pre>".print_r($row)."</pre>";}
			$row_temp = $row;
			unset($row_temp['index']);
			$data[$row['index']] = $row_temp;
		}
		
		
		$this->disconnect();
		return array('site'=>$site_info, 'readings'=>$data);
	}
	
	public function reporting($mac){
		if(empty($mac)){
			$this->logger->logerr('mac for get_readings is empty',0,false,basename(__FILE__),__LINE__);
			return false;	
		}

		if (!$this->connect()) {
			$this->logger->logerr('Failed to connect',1000,false,basename(__FILE__),__LINE__);
			return false;
		}
		
		$sql = "SELECT CASE (
		SELECT `enabled`
		FROM sites
		WHERE `mac`='" . strtolower($mac) . "') WHEN 'y' THEN IF((
		SELECT COUNT(*) AS nodata
		FROM `" . strtolower($mac) . "`
		WHERE `date` > (DATE(DATE_SUB(CURDATE(), INTERVAL 5 HOUR)) - INTERVAL 12 HOUR)
		ORDER BY `index` DESC) >= 1, 'good', 'bad') ELSE 'disabled' END AS nodata;";

		if(!$result = $this->mysqli->query($sql)){
			$$this->logger->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$this->disconnect();
			return false;
		}
		$row = $result->fetch_assoc();
		
		if($row['nodata'] == 'bad'){
			return false;	
		}else{
			return true;
		}
			
	}
	
	private function connect(){
		$_mysqli = new MySQLi($this->host, $this->username, $this->password, $this->db_name);
		if ($_mysqli->connect_errno) {
			$this->logger->logerr($_mysqli->connect_error,1000,false,basename(__FILE__),__LINE__);
			return false;
		}
		$this->mysqli = $_mysqli;
		return true;
	}
	
	private function disconnect(){
			$this->mysqli->close();
	}
	
	
}//Class end



?>