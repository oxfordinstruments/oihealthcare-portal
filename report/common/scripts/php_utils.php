<?php
/**
 * This is a group of common functions used through out the portal
 *
 * @package OiHealthcarePortal
 * @file php_utils.php
 * @author Evotodi <evotodi@gmail.com>
 * @date 11/7/2016
 * @time 4:30 PM
 */

//error_reporting(E_ALL);
if(substr(php_sapi_name(), 0, 3) == 'cli' || empty($_SERVER['REMOTE_ADDR'])) {
    //shell
	$cron = false;
	if(isset($_SERVER['argv'][1]) and $_SERVER['argv'][1] == '--cron'){
		$cron = true;
	}
	$docroot = preg_replace("/(public_html).*/",'public_html',dirname(__FILE__),2);
	$no_define_eol = true;
	define('EOL',PHP_EOL);
} else {
    //webserver
	$cron = false;
	$docroot = $_SERVER['DOCUMENT_ROOT'];
	define('EOL','<br />');
}

require_once($docroot.'/log/log.php');
require_once($docroot.'/resources/moment_php/Moment.php');
require_once($docroot.'/resources/moment_php/MomentException.php');
require_once($docroot.'/resources/moment_php/MomentHelper.php');
require_once($docroot.'/resources/moment_php/MomentLocale.php');
/**
 * Class phpUtils
 */
class phpUtils{
	
	public $logger;
	protected $session_ident = NULL;
	private $debug = false;


	/**
	 * phpUtils constructor.
	 * @param null $docroot
	 * @param bool $debug
	 */
	public function __construct($docroot = NULL, $debug = false){
		if(is_null($docroot)){
			$docroot = $_SERVER['DOCUMENT_ROOT'];	
		}
		$this->logger = new logger($docroot);
		$this->session_ident = session_name();
		$this->debug = $debug;
	}

	/**
	 * @param bool $debug
	 */
	public function setDebug($debug)
	{
		$this->debug = $debug;
	}

	/**
	 * Updates the current users session vars from the database
	 * Pass a referance to the mysqli connection
	 *
	 * @param $mysqli
	 */
	public function session_update(&$mysqli){
		$this->logger->loginfo('Session update request from utils',0,false,basename(__FILE__),__LINE__);
		
		$sql="CALL users_data_get('".$_SESSION['login']."');";
		if(!$result = $mysqli->query($sql)){
			$this->logger->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$this->logger->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$this->logger->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
		$row = $result->fetch_assoc();
		
		$userdata = array();
		foreach($row as $key => $value){
			$userdata[$key] = $value;
		}
		
		//Clear the remaining results from the above mysql call statement
		if($mysqli->more_results()){
			while ($mysqli->next_result());
		}

		//Add user data to session
		unset($userdata['pwd']);
		unset($userdata['secret_1']);
		unset($userdata['secret_2']);
		$_SESSION['userdata'] = $userdata;	
		
		//add roles to session
		$current_roles = array();
		foreach($userdata as $key=>$value){
			if(preg_match("/^role_/",$key) and strtolower($value) == "y"){
				$current_roles[$key] = 'y';	
			}
		}
		$_SESSION['roles'] = $current_roles;
		
		//add permissions to session
		$current_perms = array();
		foreach($userdata as $key=>$value){
			if(preg_match("/^perm_/",$key) and strtolower($value) == "y"){
				$current_perms[$key] = 'y';	
			}
		}
		$_SESSION['perms'] = $current_perms;
		
		//add preferences to session
		$current_prefs = array();
		foreach($userdata as $key=>$value){
			if(preg_match("/^pref_/",$key) and strtolower($value) == "y"){
				$current_prefs[$key] = 'y';	
			}
		}
		$_SESSION['prefs'] = $current_prefs;
		
	}

	/**
	 * Returns the result from sql statement
	 * returns false on error
	 *
	 * @param $mysqli
	 * @param $sql
	 * @param bool $filename
	 * @param bool $line
	 * @return bool
	 */
	public function sql_result(&$mysqli, $sql, $filename = false, $line = false, $debug = false){
		if($sql == ''){
			$this->logger->logerr('SQL var is empty',1000,false,basename(__FILE__),__LINE__);
			return false;	
		}
		if(!$this->ping_sql($mysqli)){
			$this->logger->logerr('Could not ping the mysql server',1000,false,basename(__FILE__),__LINE__);
			return false;	
		}
		if(!$filename){
			$filename = basename(__FILE__);
		}

		$redirect = true;
		if($debug){
			$redirect = false;
		}

		if(!$result = $mysqli->query($sql)){
			$this->logger->logerr($sql,1000,false,$filename,$line);
			$this->logger->logerr('There was error running the query ['.$mysqli->error.']',1000,false,$filename,$line);
			$this->logger->logerr('Error occurred, contact support.',1000,$redirect,basename(__FILE__),__LINE__);
		}
		
		return $result;
	}

	/**
	 * Returns an array for seconds entered
	 * Returns Array ( [y] => 0 [m] => 0 [d] => 0 [h] => 0 [i] => 0 [s] => 0 [weekday] => 0 [weekday_behavior] => 0 [first_last_day_of] => 0 [invert] => 1 [days] => 3 [special_type] => 0 [special_amount] => 0 [have_weekday_relative] => 0 [have_special_relative] => 0 )
	 *
	 * @param $seconds
	 * @return array
	 */
	public function getInterval($seconds){
		$obj = new DateTime();
		$obj->setTimeStamp(time()+$seconds);
		return (array)$obj->diff(new DateTime());
	}

	/**
	 * Converts phone numbers to standard format
	 * 15551234567 becomes 555-123-4567
	 * (555) 123-4567 becomes 555-123-4567
	 * +1 (555) 123-4567 becomes 555-123-4567
	 *
	 * @param string $phone
	 * @return string
	 */
	public function localize_us_number($phone) {
		$numbers_only = preg_replace("/[^\d]/", "", $phone);
		return preg_replace("/^1?(\d{3})(\d{3})(\d{4})$/", "$1-$2-$3", $numbers_only);
	}

	/**
	 * Pings the given mysqli connection and
	 * returns true if good or error if not
	 *
	 * @param $mysqli
	 * @return bool
	 */
	public function ping_sql($mysqli){
		if ($mysqli->ping()){
			return true;
		}else{
			return($mysqli->error);
		}
	}

	/**
	 * Replace accented characters
	 *
	 * @param $str
	 * @return string
	 */
	public function toASCII($str){
		return strtr(utf8_decode($str), 
			utf8_decode(
			'ŠŒŽšœžŸ¥µÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýÿ'),
			'SOZsozYYuAAAAAAACEEEEIIIIDNOOOOOOUUUUYsaaaaaaaceeeeiiiionoooooouuuuyy');
	}

	/**
	 * Converts string to hex
	 *
	 * @param $x
	 * @return string
	 */
	public function strtohex($x){
		$s='';
		foreach (str_split($x) as $c) $s.=sprintf("%02X",ord($c));
		return($s);
	}

	/**
	 * Limit words
	 *
	 * @param $string
	 * @param $word_limit
	 * @return string
	 */
	public function limit_words($string, $word_limit){
		$words = explode(" ",$string);
		if(count($words) > $word_limit){
			return implode(" ",array_splice($words,0,$word_limit))."...(truncated)";
		}else{
			return implode(" ",array_splice($words,0,$word_limit));
		}
	}

	/**
	 * Used in getToken function
	 *
	 * @param $min
	 * @param $max
	 * @return mixed
	 */
	private function crypto_rand_secure($min, $max){
		$range = $max - $min;
		if ($range < 1) return $min; // not so random...
		$log = ceil(log($range, 2));
		$bytes = (int) ($log / 8) + 1; // length in bytes
		$bits = (int) $log + 1; // length in bits
		$filter = (int) (1 << $bits) - 1; // set all lower bits to 1
		do {
			$rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
			$rnd = $rnd & $filter; // discard irrelevant bits
		} while ($rnd >= $range);
		return $min + $rnd;
	}

	/**
	 * Used to get a random string of x length
	 *
	 * @param $length
	 * @return string
	 */
	public function getToken($length){
		$token = "";
		$codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
		$codeAlphabet.= "0123456789";
		$max = strlen($codeAlphabet) - 1;
		for ($i=0; $i < $length; $i++) {
			$token .= $codeAlphabet[$this->crypto_rand_secure(0, $max)];
		}
		return $token;
	}

	/**
	 * @param DateTime $from
	 * @param DateTime $to
	 * @return array
	 */
	public function getMonths($from, $to){
		//$from is date string
		//$to is date string
		$from = strtotime($from);
		$to = strtotime($to);
		$total = null;

		if($from < $to) {
			$whole_months = (date('Y', $to) - date('Y', $from)) * 12 + (date('m', $to) - date('m', $from));
			$from_month_end = date('t', $from);
			$from_partial = (date('j', $from) - 1) / $from_month_end;
			$to_month_end = date('t', $to);
			$to_partial = ($to_month_end - date('j', $to)) / $to_month_end;
			if($to_partial + $from_partial >= 1.0){
				$total = $whole_months + (($to_partial + $from_partial) - 1);
			}else{
				$total = $whole_months + $to_partial + $from_partial;
			}
			if($whole_months == 1){
				$total -= 1;
			}

		}else{
			$whole_months = (date('Y', $to) - date('Y', $from)) * 12 + (date('m', $to) - date('m', $from));
			$from_month_end = date('t', $from);
			$from_partial = (date('j', $from) - 1) / $from_month_end;
			$to_month_end = date('t', $to);
			$to_partial = ($to_month_end - date('j', $to)) / $to_month_end;
			if($from_partial + $to_partial >= 1.0){
				$total = $whole_months  - ((($from_partial + $to_partial) - 1));
			}else{
				$total = $whole_months + -$from_partial + -$to_partial;
			}
			if($whole_months == -1){
				$total += 1;
			}
		}
		return array(
			'dir'=>'from greater than to',
			'from'=>date('c',$from),
			'to'=>date('c', $to),
			'whole_months'=>$whole_months,
			'from_month_end'=>$from_month_end,
			'from_partial'=>$from_partial,
			'to_month_end'=>$to_month_end,
			'to_partial'=>$to_partial,
			'total'=>$total);
	}

	/**
	 * @param int $number
	 * @param array $array
	 * @return int
	 */
	public function findNearestNumber($number = 0, array $array){
		//First check if we have an exact number
		if(false !== ($exact = array_search($number,$array))){
			return $array[$exact];
		}
		//Sort the array
		sort($array);
		//make sure our search is greater then the smallest value
		if ($number < $array[0] ){
			return $array[0];
		}
		$closest = $array[0]; //Set the closest to the lowest number to start
		foreach($array as $value){
			if(abs($number - $closest) > abs($value - $number)){
				$closest = $value;
			}
		}
		return $closest;
	}

	/**
	 * @param array $array
	 * @param $col
	 */
	public function customUasort(array &$array, $col){
		uasort($array, function($a, $b) use ($col) {
			return strcmp($a[$col], $b[$col]);
		});
	}

	/**
	 * @param bool $mysqli
	 * @param string $system_ver_unique_id
	 * @return bool|int|string
	 */
	public function next_pm(&$mysqli = false, $system_ver_unique_id = ''){
		$sql="SELECT sb.pm_dates, sb.last_pm, sb.pm_freq
			FROM systems_base_cont AS sbc
			LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
			WHERE CAST(sb.pm_freq AS UNSIGNED) != 0 
			AND sb.pm_start != ''
			AND sbc.ver_unique_id = '".$system_ver_unique_id."';";
		if($system_ver_unique_id == ''){
			$this->logger->logerr('$system_ver_unique_id var is empty',1000,false,basename(__FILE__),__LINE__);
			return false;
		}
		if($mysqli == false){
			$this->logger->logerr('$mysqli var is empty',1000,false,basename(__FILE__),__LINE__);
			return false;
		}
		if(!$this->ping_sql($mysqli)){
			$this->logger->logerr('Could not ping the mysql server',1000,false,basename(__FILE__),__LINE__);
			return false;
		}
		if(!$result = $mysqli->query($sql)){
			$this->logger->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$this->logger->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			return false;
		}
		if($result->num_rows == 0){
			return false;
		}

		$now = new \Moment\Moment();

		$row = $result->fetch_assoc();

		$last_pm = new \Moment\Moment($row['last_pm']);
		if($last_pm <= $now){
			$pm_dates = json_decode($row['pm_dates']);

			$pmd_array = array();
			foreach($pm_dates as $key => $val){
				$pm_date_init = new \Moment\Moment($now->getYear() . '-' . sprintf('%02d', $val[0]) . '-' . sprintf('%02d', $val[1]), null, true);
				$pm_date_init_prev = $pm_date_init->subtractYears(1);
				$pm_date_init_next = $pm_date_init->addYears(1);

				$start = $pm_date_init_prev->subtractMonths(intval($row['pm_freq']))->addDays(31)->format('Y-m-d');
				$end = $pm_date_init_prev->addDays(30)->format('Y-m-d');
				$pmd_array[$pm_date_init_prev->format('Y-m-d')] = array('start' => $start, 'end' => $end);

				$start = $pm_date_init->subtractMonths(intval($row['pm_freq']))->addDays(31)->format('Y-m-d');
				$end = $pm_date_init->addDays(30)->format('Y-m-d');
				$pmd_array[$pm_date_init->format('Y-m-d')] = array('start' => $start, 'end' => $end);

				$start = $pm_date_init_next->subtractMonths(intval($row['pm_freq']))->addDays(31)->format('Y-m-d');
				$end = $pm_date_init_next->addDays(30)->format('Y-m-d');
				$pmd_array[$pm_date_init_next->format('Y-m-d')] = array('start' => $start, 'end' => $end);
			}
			ksort($pmd_array);

			$next_pm = false;
			$keys = array_keys($pmd_array);
			foreach($pmd_array as $key => $val){
				if($last_pm->isBetween(new \Moment\Moment($val['start']), new \Moment\Moment($val['end']))){
					$next_pm = $key;
					break;
				}
			}
			$key = array_search($next_pm, $keys);
			$next_pm = $keys[$key + 1];

			return $next_pm;

		}else{
			$this->logger->logerr('last pm after current date',0,false,basename(__FILE__),__LINE__);
			return false;
		}
	}

	/**
	 * @param $message
	 * @param bool $line_before
	 * @param bool $debug
	 */
	public function message($message, $line_before = false, $debug = true){
		if($debug){
			if($line_before){
				echo EOL, date('c', time()), " -- ", $message, EOL;
			}else{
				echo date('c', time()), " -- ", $message, EOL;
			}

		}
	}

	/**
	 * @param mysqli $mysqli
	 * @param string $script
	 * @param array $args
	 * @param string $uid
	 * @param string $schedule
	 * @throws Exception
	 * @internal param $array /assoc $args
	 * @internal param $mysqli /timestamp $schedule
	 */
	public function email_action_add(&$mysqli, $script, Array $args, $uid, $schedule = ''){
		$sql="INSERT INTO email_actions (`script`, `args`, `uid`, `schedule`) VALUES (?, ?, ?, ?)";

		if (!($stmt = $mysqli->prepare($sql))) {
			echo $mysqli->error, $mysqli->errno;
		}
		$arg = json_encode($args);

		if($this->debug){
			echo printf( str_replace('?', '%s', $sql), $script, $arg, $uid, $schedule);
		}

		if (!$stmt->bind_param("ssss", $script, $arg, $uid, $schedule)) {
			echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
		}

		if (!$stmt->execute()) {
			echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
		}
	}

	/**
	 * Get timesone name abbreviation. e.g. EST
	 * @param string $tz
	 * @return string
	 */
	public function tz_abbreviation($tz = 'UTC'){
		$dateTime = new DateTime();
		$dateTime->setTimeZone(new DateTimeZone($tz));
		return $dateTime->format('T');
	}

	/**
	 * Get timezone name gmt offset. e.g. -0500
	 * @param string $tz
	 * @return string
	 */
	public function tz_offset($tz = 'UTC'){
		$dateTime = new DateTime();
		$dateTime->setTimeZone(new DateTimeZone($tz));
		return $dateTime->format('P');
	}

	/**
	 * Get timezone offset in seconds. e.g. -18000
	 * @param string $tz
	 * @return int
	 */
	public function tz_offset_sec($tz = 'UTC'){
		if(is_null($tz)){
			$tz = 'UTC';
		}
		$timezone = new DateTimeZone($tz);
		return $timezone->getOffset(new DateTime);
	}

	/**
	 * Convert array to curl post array
	 * @param array $arrays
	 * @param array $new
	 * @param null $prefix
	 */
	public function http_build_query_for_curl($arrays, &$new = array(), $prefix = null ) {

		if ( is_object( $arrays ) ) {
			$arrays = get_object_vars( $arrays );
		}

		foreach ( $arrays AS $key => $value ) {
			$k = isset( $prefix ) ? $prefix . '[' . $key . ']' : $key;
			if ( is_array( $value ) OR is_object( $value )  ) {
				$this->http_build_query_for_curl( $value, $new, $k );
			} else {
				$new[$k] = $value;
			}
		}
	}

	/**
	 * @return bool|string
	 */
	public function whichServer()
	{
		if(file_exists( $_SERVER['DOCUMENT_ROOT'].'/whichserver')){
//			$str = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/whichserver');
			$f = fopen($_SERVER['DOCUMENT_ROOT'].'/whichserver', 'r');
			$str = trim(fgets($f));
			fclose($f);
			if(strlen($str) > 0){
				return $str;
			}else{
				$this->logger->logerr('/whichserver strlen = '.strlen($str),0,false,basename(__FILE__),__LINE__);
				$this->logger->logerr('/whichserver str = '.$str,0,false,basename(__FILE__),__LINE__);
				return false;
			}
		}else{
			$this->logger->logerr('/whichserver not found',0,false,basename(__FILE__),__LINE__);
			return false;
		}
	}

	/**
	 * @return string
	 */
	public function generate_uuid() {
		return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
			mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
			mt_rand( 0, 0xffff ),
			mt_rand( 0, 0x0fff ) | 0x4000,
			mt_rand( 0, 0x3fff ) | 0x8000,
			mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
		);
	}
} //end class
?>