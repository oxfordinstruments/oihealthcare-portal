<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');

class Session {

	private $mysqli;
	
	public $logger;
	
	protected $debug = true;
	
	public function __construct(){
		$this->logger = new logger();
		
		require($_SERVER['DOCUMENT_ROOT'].'/mysqlInfo.php');
		$this->mysqli = new MySQLi("$host", "$username", "$password", "$db_name");
		
		session_set_save_handler(
			array($this, "_open"),
			array($this, "_close"),
			array($this, "_read"),
			array($this, "_write"),
			array($this, "_destroy"),
			array($this, "_gc")
		);
		session_start();
	}
	
	public function _open(){
		if($this->mysqli->connect_errno) {
			$this->logger->logerr($mysqli->connect_error,1000,true,basename(__FILE__),__LINE__);
			return false;
		}
		return true;
	}
	
	public function _close(){
		if(!$this->mysqli->close()){
			$this->logger->logerr('SQL DB Close Failed',1000,false,basename(__FILE__),__LINE__);
			return false;
		}
		return true;
	}
	
	public function _read($id){
		
		if(!$result = $this->mysqli->query("SELECT data FROM sessions WHERE id = '$id';")){
			return '';			
		}else{
			$row = $result->fetch_assoc();
			return $row['data'];
		}
	}
	
	public function _write($id, $data){
		$access = time();
		if(!$this->mysqli->query("REPLACE INTO sessions VALUES ('$id', $access, '$data');")){
			return false;
		}
		return true;
	}
	
	public function _destroy($id){
		if(!$this->mysqli->query("DELETE FROM sessions WHERE id = '$id';")){
			return false;
		}
		$_SESSION = array();
		return true;
	} 
	
	public function _gc($max){
		$old = time() - $max;
		if(!$this->mysqli->query('DELETE * FROM sessions WHERE access < $old;')){
			return false;
		}
		return true;
	}
 
}

?>