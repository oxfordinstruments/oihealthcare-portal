<?php 
//Update Completed 11/25/14

$debug = false;
if(isset($_GET['debug'])){
	$debug = true;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

$customer_id = false;
if(isset($_POST['customer_id'])){
	$customer_id = $_POST['customer_id'];
}else if(isset($_GET['customer_id'])){
	$customer_id = $_GET['customer_id'];	
}

if($customer_id == false){
	echo 'Customer ID Not Found';
	return;	
}

$sql = "SELECT GROUP_CONCAT(sbc.system_id SEPARATOR ',') AS `systems`
FROM systems_base_cont AS sbc
LEFT JOIN facilities AS f ON f.unique_id = sbc.facility_unique_id
LEFT JOIN customers AS c ON c.unique_id = f.customer_unique_id
WHERE c.customer_id = '$customer_id';";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);	
}

if($result->num_rows > 0){
	$row = $result->fetch_assoc();	
}else{
	echo 'None';
	return;	
}

echo $row['systems'];
return;

?>