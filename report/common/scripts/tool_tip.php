<?php

/*todo-evo Must remove all remnants of the old skinnytip tool tip system!!! */
$debug = false;

if($debug){
	ini_set('display_errors', '1'); // Set by installer
	ini_set('display_startup_errors', '1'); // Set by installer
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$logger = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

$id = "";
$page = "";
if(isset($_POST['id']) and $_POST['id'] != ""){
	$id = $_POST['id'];
	$page = $_POST['page'];
}elseif(isset($_GET['id']) and $_GET['id'] != ""){
	$id = $_GET['id'];
	$page = $_GET['page'];
}else{
	die("FAILED");
}



//$referer = explode('?', basename($_SERVER['HTTP_REFERER']));
//$referer = $referer[0];
$referer = basename($page);
$fileparts = explode('.',$referer);
d($fileparts);
$referer = $fileparts[0];
d($referer);

$sql="SELECT * FROM misc_tool_tips WHERE element = \"".$mysqli->real_escape_string(str_replace('tooltip_','',$id))."\" AND `page` = \"".$mysqli->real_escape_string($referer)."\"  LIMIT 1;";
d($sql);

if(!$result = $mysqli->query($sql)){
	$logger->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$logger->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$logger->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$row = $result->fetch_assoc();

if(!$row){
	$rtn = array("tip"=>"NO HELP FOR YOU!");
	echo json_encode($rtn);
}else {
	echo json_encode($row);
}

?>