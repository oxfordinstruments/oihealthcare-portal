<?php
//error_reporting(E_ALL);

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/php_utils.php');

class sessionAutoLogin{
	public $debug = false;
	private $mysqli = NULL;
	const EOL = "<br>";
	private $logger;
	private $php_utils;
	
	//
	//Constructor
	//
	public function __construct(&$mysqli = NULL){
		if(is_null($mysqli)){
			$this->logger->logerr('MySQLi is NULL',0,false,basename(__FILE__),__LINE__);
			throw new Exception('MySQLi Null');	
		}
		$this->mysqli = &$mysqli;

		$this->logger = new logger($_SERVER['DOCUMENT_ROOT']);
		$this->php_utils = new phpUtils($_SERVER['DOCUMENT_ROOT']);
	}	
	
	//
	//Returns false on failed
	//
	public function auto_login_check(){
		
		if(!isset($_COOKIE['OIREPORT_AL'])){
			return false;	
		}
		//cookie_data -> users_id : token : session_id
		$cookie_data = explode(':', $_COOKIE['OIREPORT_AL'],3);
		
		if($this->debug){
			echo EOL,"cookie_data:<pre>",var_export($cookie_data),"</pre>",EOL;	
		}
		
		$current_data = $this->check_token($cookie_data[0], $cookie_data[1]);
		if($current_data == false){
			setcookie('OIREPORT_AL','',0,'/');
			return false;
		}
		
		if($_SERVER['REMOTE_ADDR'] != $current_data['ip']){
			if($this->debug){echo EOL,"ips don't match",EOL;}
			return false;	
		}
		
		if(gethostbyaddr($_SERVER['REMOTE_ADDR']) != $current_data['domain']){
			if($this->debug){echo EOL,"domains don't match",EOL;}
			return false;	
		}
		
		return $current_data;
		
	}
	
	//
	//Updates the auto login table and cookie with a new token
	//
	public function auto_login_update($session_id = NULL){
		if(is_null($session_id)){
			return false;	
		}
		
		if(!isset($_COOKIE['OIREPORT_AL'])){
			if($this->debug){echo EOL,"oireport_al cookie missing",EOL;}
			return false;	
		}
		//cookie_data -> users_id : token : session_id
		$cookie_data = explode(':', $_COOKIE['OIREPORT_AL'],3);
		
		if($this->debug){
			echo EOL,"cookie_data:<pre>",var_export($cookie_data),"</pre>",EOL;	
		}
		
		$current_data = $this->check_token($cookie_data[0], $cookie_data[1]);
		if($current_data == false){
			setcookie('OIREPORT_AL','',0,'/');
			if($this->debug){echo EOL,"check token failed",EOL;}
			return false;
		}
		if($this->debug){
			echo EOL,"current_data:<pre>",var_export($current_data),"</pre>",EOL;	
		}
		
		if($_SERVER['REMOTE_ADDR'] != $current_data['ip']){
			if($this->debug){echo EOL,"ips don't match",EOL;}
			return false;	
		}
		
		if(gethostbyaddr($_SERVER['REMOTE_ADDR']) != $current_data['domain']){
			if($this->debug){echo EOL,"domains don't match",EOL;}
			return false;	
		}
		
		$expire = 0;
		if(isset($_COOKIE['oihp_expire'])){
			$expire = $_COOKIE['oihp_expire'];
		}
		
		$new_data = array('uid'=>$current_data['uid'],
						  'token'=>$this->php_utils->getToken(26),
						  'session_id'=>$current_data['session_id'],
						  'ip'=>$current_data['ip'],
						  'domain'=>$current_data['domain'],
						  'expires'=>$expire);
		
		
		
		setcookie('OIREPORT_AL', implode(':', $new_data), $expire, '/');
		
		$sql="UPDATE users_auto_logins SET token = '".$new_data['token']."' WHERE session_id = '".$new_data['session_id']."';";
		if(!$result = $this->mysqli->query($sql)){
			$this->logger->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$this->logger->logerr('There was error running the query ['.$this->mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			return false;
		}
	
		return $new_data;		
	}
	
	public function auto_login_create(){
		if(!isset($_SESSION['login'])){
			if($this->debug){echo EOL,"session login not set",EOL;}
			return false;	
		}
		
		$expire = 0;
		if(isset($_COOKIE['oihp_expire'])){
			$expire = $_COOKIE['oihp_expire'];
		}else{
			if($this->debug){echo EOL,"oihp_expire cookie missing",EOL;}
			return false;	
		}
		
		$new_data = array('uid'=>$_SESSION['login'],
						  'token'=>$this->php_utils->getToken(26),
						  'session_id'=>session_id(),
						  'ip'=>$_SERVER['REMOTE_ADDR'],
						  'domain'=>gethostbyaddr($_SERVER['REMOTE_ADDR']),
						  'expires'=>$expire);
		
		
		
		setcookie('OIREPORT_AL', implode(":", array($new_data['uid'], $new_data['token'], $new_data['session_id'])), $expire, '/');
		
		$sql="INSERT INTO users_auto_logins (uid, token, session_id, ip, domain, expires) 
		VALUES ('".$new_data['uid']."', '".$new_data['token']."', '".$new_data['session_id']."', '".$new_data['ip']."', '".$new_data['domain']."', '".$new_data['expires']."')
		ON DUPLICATE KEY UPDATE	token=VALUES(token), session_id=VALUES(session_id), ip=VALUES(ip), domain=VALUES(domain), expires=VALUES(expires);";
		
		if(!$result = $this->mysqli->query($sql)){
			$this->logger->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$this->logger->logerr('There was error running the query ['.$this->mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			return false;
		}
		
		return $new_data;
	}
	
	//
	//Returns session_id on match else false
	//
	private function check_token($user_id, $token){
		$sql="SELECT uid, token, session_id, ip, domain, expires FROM users_auto_logins WHERE uid = '$user_id' and token = '$token';";
		if(!$result = $this->mysqli->query($sql)){
			$this->logger->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$this->logger->logerr('There was error running the query ['.$this->mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			return false;
		}
		if($result->num_rows > 1){
			if($this->debug){echo EOL,"check token returned multiple rows",EOL;}
			return false;	
		}
		$row = $result->fetch_assoc();
		if($this->debug){
			echo EOL,"sql result: <pre>",var_export($row),"</pre>",EOL;	
		}
		return $row;
	}
		
}//end class


?>