<?php 

//
// Error reporting
//
//error_reporting(E_ALL);
//error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
//ini_set('display_errors', 'On');

//
//Load settings
//


$settings = new SimpleXMLElement($_SERVER['DOCUMENT_ROOT'].'/settings.xml', null, true);
require_once($_SERVER['DOCUMENT_ROOT'].'/define_inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/Mobile_Detect.php');
$mobile_detect = new Mobile_Detect();

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/session_handler.php');


//
//Start session
//


if(!isset($remove_session)){
	session_name("OIREPORT");
	ini_set('session.cookie_lifetime', 300);
	ini_set('session.gc_maxlifetime', 300);
	$session = new Session();
}else{
	session_name("OIREPORT");
	$session = new Session();
	session_destroy();
	setcookie(session_name(),'',0,'/');
}


$now = time(); // checking the time now when home page starts
ob_start();//Start output buffering
set_include_path(implode(PATH_SEPARATOR, array(get_include_path(), $settings->php_include_path)));


//
//check if mobile device
//
$_SESSION['mobile_device'] = false;
if($mobile_detect->isMobile()){
	$_SESSION['mobile_device'] = true;
}

//
//check if site is closed
//
$closed_to_user = false;
if($settings->close_site == 1){
	if(isset($_SESSION['login'])){
		$allowed_users = explode(',',$settings->users_pass_close);
		if(!in_array((string)$_SESSION['login'],$allowed_users)){
			$closed_to_user = true;			
		}
	}else{
		$closed_to_user = true;
	}
}
if(!isset($at_login_page)){
	if($closed_to_user){
		if(!strpos($_SERVER['REQUEST_URI'],'error.php')){
			header("location:/error.php?n=1");
		}
	}
}

//
//connect to mysql
//
require($_SERVER['DOCUMENT_ROOT'].'/mysqlInfo.php');

$mysqli = new mysqli("$host", "$username", "$password", "$db_name");

if ($mysqli->connect_errno) {
	$log->logerr($mysqli->connect_error,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support',1000,true,basename(__FILE__),__LINE__);
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_auto_login.php');
$sal = new sessionAutoLogin($mysqli);

if($at_login_page){
	if(isset($_GET)){
		//Check for address GET statements		
		if(isset($_GET['email_request'])){ //check if login is from email request
			$_SESSION['email_request'] = $_GET['email_request'];
			$_SESSION['urlGET'] = true;	
		}			
		if(isset($_GET['email_face'])){ //check if login is from email face sheet
			$_SESSION['email_face'] = $_GET['email_face'];
			$_SESSION['urlGET'] = true;
		}	
	}	
}//else{
//	if(!isset($script)){
//		$auto_login_update = $sal->auto_login_update(session_id);
//		if($auto_login_update == false){
//			header("Location:/report/");	
//		}
//	}
//}

if(isset($_COOKIE['oihp_timezone'])){
	$timezone_info = explode(',',$_COOKIE['oihp_timezone']);
	$_SESSION['tz_name'] = $timezone_info[0];
	$_SESSION['tz_offset'] = $timezone_info[1];
	$timezone = new DateTimeZone($timezone_info[0]);
	$_SESSION['tz_offset_sec'] = $timezone->getOffset(new DateTime);
}

//
//logout if login time expired
//
if(!isset($remove_session)){
	if (!isset($_SESSION["login"])){
		if($now > $_SESSION['expire']){
			session_destroy();
			if(!isset($at_login_page)){
				if(!isset($no_redirect)){
					header("Location:/report/logout.php");
				}else{
					$log->loginfo('no redirect true session expired',2);	
				}
			}
		}
	
		if(!isset($at_login_page)){
			if(!isset($no_redirect)){
				header("location:/report");
			}else{
				$log->loginfo('no redirect true no session login set',2);	
			}
		}
	}
}

//
//common vars
//
if(!isset($remove_session)){
	$loc=$_SESSION['loc'];
	$myusername=$_SESSION["login"];
}

$head_include = $_SERVER['DOCUMENT_ROOT'].'/report/head_foot/default_inc_head.php';
$js_include = $_SERVER['DOCUMENT_ROOT'].'/report/head_foot/default_inc_js.php';
$css_include = $_SERVER['DOCUMENT_ROOT'].'/report/head_foot/default_inc_css.php';
$header_include = $_SERVER['DOCUMENT_ROOT'].'/report/head_foot/default_header.php';
$no_login_header = $_SERVER['DOCUMENT_ROOT'].'/report/head_foot/default_header.php';
$footer_include = $_SERVER['DOCUMENT_ROOT'].'/report/head_foot/default_footer.php';

//
//common includes
//
//include($_SERVER['DOCUMENT_ROOT'].'/report/common/send_error.php');


if(isset($_SESSION['group']) and !isset($at_login_page) and !isset($use_default_includes)){
	switch($_SESSION['group']){
		case 1:
			require_once ($_SERVER['DOCUMENT_ROOT'].'/report/head_foot/employee_inc_php.php');
			$head_include = $_SERVER['DOCUMENT_ROOT'].'/report/head_foot/employee_inc_head.php';
			$js_include = $_SERVER['DOCUMENT_ROOT'].'/report/head_foot/employee_inc_js.php';
			$css_include = $_SERVER['DOCUMENT_ROOT'].'/report/head_foot/employee_inc_css.php';
			$header_include = $_SERVER['DOCUMENT_ROOT'].'/report/head_foot/employee_header.php';
			$footer_include = $_SERVER['DOCUMENT_ROOT'].'/report/head_foot/employee_footer.php';
			break;
		case 2:
			require_once ($_SERVER['DOCUMENT_ROOT'].'/report/head_foot/contractor_inc_php.php');
			$head_include = $_SERVER['DOCUMENT_ROOT'].'/report/head_foot/contractor_inc_head.php';
			$js_include = $_SERVER['DOCUMENT_ROOT'].'/report/head_foot/contractor_inc_js.php';
			$css_include = $_SERVER['DOCUMENT_ROOT'].'/report/head_foot/contractor_inc_css.php';
			$header_include = $_SERVER['DOCUMENT_ROOT'].'/report/head_foot/contractor_header.php';
			$footer_include = $_SERVER['DOCUMENT_ROOT'].'/report/head_foot/contractor_footer.php';
			break;
		case 3:
			require_once ($_SERVER['DOCUMENT_ROOT'].'/report/head_foot/customer_inc_php.php');
			$head_include = $_SERVER['DOCUMENT_ROOT'].'/report/head_foot/customer_inc_head.php';
			$js_include = $_SERVER['DOCUMENT_ROOT'].'/report/head_foot/customer_inc_js.php';
			$css_include = $_SERVER['DOCUMENT_ROOT'].'/report/head_foot/customer_inc_css.php';
			$header_include = $_SERVER['DOCUMENT_ROOT'].'/report/head_foot/customer_header.php';
			$footer_include = $_SERVER['DOCUMENT_ROOT'].'/report/head_foot/customer_footer.php';
			break;	
	}
}

//
//Set referring uri
//
if(!isset($remove_session)){
	$refering_uri = $_SERVER['HTTP_HOST']."/".$_SESSION['loc']."/";
	$refering_uri = str_replace('//','/',$refering_uri);
	$refering_uri = "https://".$refering_uri;
}
?>