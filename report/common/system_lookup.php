<?php 
//Update Completed 11/25/14
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

$sql = "SELECT sb.system_id, sbc.nickname, st.name AS system_name, sbc.property
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
LEFT JOIN systems_types AS st ON st.id = sb.system_type
WHERE sbc.property = 'C' OR sbc.property = 'F'
ORDER BY sb.system_id ASC;";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);	
}

?>
<!doctype html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<?php require_once($js_include);?>

<script type="text/javascript">
$(document).ready(function() {
	allTable = $('#allSitesTable').dataTable({
		"bJQueryUI": true,
		"bStateSave": false,
		"sPaginationType": "full_numbers",
		"fnInitComplete": function () {
			 this.$('tr').click( function () {
				 var href = $(this).find("a").attr("href");
				 if(href) {
					window.location = href;
				 }
			 });
		},
		"fnDrawCallback" : function(oSettings) {
			var total_count = oSettings.fnRecordsTotal();
			var columns_in_row = $(this).children('thead').children('tr').children('th').length;
			var show_num = oSettings._iDisplayLength;
			var tr_count = $(this).children('tbody').children('tr').length;
			var missing = show_num - tr_count;
			if (show_num < total_count && missing > 0){
				for(var i = 0; i < missing; i++){
					$(this).append('<tr class="space"><td colspan="' + columns_in_row + '">&nbsp;</td></tr>'); 
				}
			}
			if (show_num > total_count) {
				for(var i = 0; i < (total_count - tr_count); i++) {
					$(this).append('<tr class="space"><td colspan="' + columns_in_row + '">&nbsp;</td></tr>'); 
				}
			}
		}
	});	
});
function select_id(id){
	console.log("ID: " + id);
	$('#system_id',top.document).val(id);
	parent.$.fancybox.close();
}
</script>
</head>
<body>
 <table width="100%" id="allSitesTable">
                <thead>
                    <tr>
                        <th width="15%">System ID</th>
                        <th width="40%">Nickname</th>
                        <th width="45%">System Type</th>
						<th width="">Future</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
						while($row = $result->fetch_assoc())
						{
							echo "<tr onclick=\"javascript: select_id('".$row['system_id']."');\">\n";	
							echo "<td>". $row['system_id']."</td>\n";
							echo "<td>". $row['nickname']."</td>\n";
							echo "<td>". $row['system_name']."</td>\n";
							switch (strtolower($row['property'])){
								case 'f':
									echo "<td>Yes</td>\n";
									break;
								case 'c':
									echo "<td>&nbsp;</td>\n";
									break;
							}
							echo "</tr>\n";
						}
						?>     
                </tbody>
            </table>
</body>
</html>