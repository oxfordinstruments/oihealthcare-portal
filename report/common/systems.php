<?php
// Error reporting
//
//error_reporting(E_ALL);
//error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
//ini_set('display_errors', 'On');

$debug = false;
if(isset($_GET['debug'])){
	$debug = true;
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

if(isset($_GET['e'])) {
	$edit = true;
	if(isset($_GET['ver_unique_id'])) {
		$ver_unique_id = $_GET['ver_unique_id'];
	}else{
		$log->logerr('Blank SYSVERUID',1016,true,basename(__FILE__),__LINE__);
	}
}else{
	$edit = false;	
	$unique_id = md5(uniqid());
	$version = 1;
}

$probrpt_word_limt	 = intval($settings->probrpt_word_limit);
$uploadBtnValue = "Attach Documents";

$sql="SELECT * FROM misc_states;";
if(!$resultStates1= $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

$sql="SELECT * FROM misc_states;";
if(!$resultStates2 = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

$sql="SELECT * FROM misc_months;";
if(!$resultMonths = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

$sql="SELECT * FROM (SELECT * FROM systems_types WHERE id < 3 ORDER BY id DESC) AS st1 
UNION
SELECT * FROM (SELECT * FROM systems_types WHERE id > 2 ORDER BY modality, mfg, name ASC) AS st2;";
if(!$resultEquipment = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

$sql="SELECT * FROM misc_contracts;";
if(!$resultContract = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}	

$sql="SELECT u.uid, u.name
FROM users AS u
LEFT JOIN users_roles AS ur ON ur.uid = u.uid
LEFT JOIN users_role_id AS rid ON rid.id = ur.rid
WHERE u.active = 'y' AND (rid.role = 'role_engineer' OR rid.role = 'role_contractor') ORDER BY u.name ASC;";
if(!$resultEngineers = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

$sql="SELECT * FROM users WHERE uid = '".$_SESSION['login']."';";
if(!$resultUser = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowUser = $resultUser->fetch_assoc();

$sql = "SELECT sb.*, sbc.*, rc.vpn_mac, rc.eth0_mac, ra.system_ip, ra.system_port, ra.system_uid, ra.connection_type, ra.notes AS remote_notes, f.facility_id, f.email_list, tr.trailer_id
		FROM systems_base AS sb
		LEFT JOIN systems_base_cont AS sbc ON sb.unique_id = sbc.unique_id
		LEFT JOIN facilities AS f ON f.unique_id = sbc.facility_unique_id
		LEFT JOIN remote_assignments AS ra ON ra.system_unique_id = sb.unique_id
		LEFT JOIN remote_computers AS rc ON rc.eth0_mac = ra.remote_serial
		LEFT JOIN trailers as tr ON tr.unique_id = sb.trailer_unique_id
		WHERE sbc.ver_unique_id = '".$ver_unique_id."';";
if(!$resultS = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowS = $resultS->fetch_assoc();
d($rowS);

if(empty($rowS)){
	$ver_unique_id = md5(uniqid());
	s('New ver_unique_id');
}else{
	$unique_id = $rowS['unique_id'];
	$version = $rowS['ver'];
}
d($ver_unique_id);
d($unique_id);

$open_requests = 0;
$open_reports = 0;
if($edit){
	$sql = "SELECT 
			( SELECT uid FROM systems_assigned_pri WHERE system_ver_unique_id='".$ver_unique_id."') AS engineer_pri, 
			( SELECT uid FROM systems_assigned_sec WHERE system_ver_unique_id='".$ver_unique_id."') AS engineer_sec;";
	if(!$resultPriSecEng = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$rowPriSecEng = $resultPriSecEng->fetch_assoc();
	if(strtolower($rowS['has_files']) == 'y'){
		$uploadBtnValue = "View/Modify Documents";	
	}
	
	$sql = "SELECT 
			(SELECT COUNT(req.id)
			FROM systems_requests AS req
			LEFT JOIN systems_base_cont AS sbc ON req.system_ver_unique_id = sbc.ver_unique_id
			WHERE req.`status` = 'open' AND req.system_unique_id = '".$rowS['unique_id']."' AND sbc.property = 'C' AND req.deleted = 'n') AS req,
			
			(SELECT COUNT(rep.id)
			FROM systems_reports AS rep
			LEFT JOIN systems_base_cont AS sbc ON rep.system_ver_unique_id = sbc.ver_unique_id
			WHERE rep.`status` = 'open' AND rep.system_unique_id = '".$rowS['unique_id']."' AND sbc.property = 'C' AND rep.deleted = 'n') AS rep;";
	d($sql);
	if(!$resultOpenReqRepCur = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$rowOpenReqRepCur = $resultOpenReqRepCur->fetch_assoc();
	d($rowOpenReqRepCur);
	$open_requests_cur = intval($rowOpenReqRepCur['req']);
	d($open_requests_cur);
	$open_reports_cur = intval($rowOpenReqRepCur['rep']);
	d($open_reports_cur);
	
	$sql = "SELECT 
			(SELECT COUNT(id)
			FROM systems_requests AS req
			WHERE req.`status` = 'open' AND req.system_ver_unique_id = '".$rowS['ver_unique_id']."' AND req.deleted = 'n') AS req,
			(SELECT COUNT(id)
			FROM systems_reports AS rep
			WHERE rep.`status` = 'open' AND rep.system_ver_unique_id = '".$rowS['ver_unique_id']."' AND rep.deleted = 'n') AS rep;";
	d($sql);
	if(!$resultOpenReqRepSel = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$rowOpenReqRepSel = $resultOpenReqRepSel->fetch_assoc();
	d($rowSysOpenReqRep);
	$open_requests_sel = intval($rowOpenReqRepSel['req']);
	d($open_requests_sel);
	$open_reports_sel = intval($rowOpenReqRepSel['rep']);
	d($open_reports_sel);
}

function limit_words($string, $word_limit){
	$words = explode(" ",$string);
	if(count($words) > $word_limit){
		return implode(" ",array_splice($words,0,$word_limit))."...(more)";
	}else{
		return implode(" ",array_splice($words,0,$word_limit));
	}
}
?>
<!DOCTYPE html>
<html>
<head>
<?php require_once($head_include);?>
<?php require_once($css_include);?>
<?php require_once($js_include);?>
<script src="/resources/js/jquery-ui/jquery-ui.min.js"></script>
<script src="/resources/js/moment.min.js"></script>
<script src="/resources/js/autoNumeric.js"></script>

<script type="text/javascript">
$(document).ready(function() {
 		
$( "#tabs" ).tabs({ 
	<?php if(!$edit){
		echo "disabled: [ 6, 7, 8, 9 ]";
	}elseif(!isset($_SESSION['perms']['perm_edit_financial'])){
		echo "disabled: [ 11 ]";
	} ?>
});

		<?php
		if(strtolower($_SESSION['prefs']['pref_show_help']) != 'y' or $_SESSION['mobile_device'] == true){
		?>
		$('#tiplayer').hide();
		<?php
			}
		?>
});
</script>
<script src="/resources/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	var current_future = <?php if(strtolower($rowS['property']) == 'f'){echo 'true';}else{echo 'false';} ?>;
	var allMonths = ['Jan','Feb','Mar', "Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];

	$(document).ready(function(){
		var engineers = '<?php
					while($rowEngineers = $resultEngineers->fetch_assoc())
					{
						echo "<option value=\"" . $rowEngineers['uid'] . "\">" . $rowEngineers['name'] . "</option>";
					}
				  ?> ';
		$("#engineer_pri").append(engineers);
		$("#engineer_sec").append(engineers);

		$('#engineer_pri').val("<?php echo $rowPriSecEng['engineer_pri']; ?>");
		$('#engineer_sec').val("<?php echo $rowPriSecEng['engineer_sec']; ?>");

		 $("#credit_hold").change(function(){
			var sel=  $(this).val();
			if(sel=='N'){
				$("#credit_hold_div").hide();
			}else{
				$("#credit_hold_div").show();
			}
		});

		$("#mobile").change(function(){
			var sel = $(this).val();
			if(sel == 'N'){
				$("#mobile_trailer_tr").hide('slow');
				$("#trailer_id").val('');
			}else{
				$("#mobile_trailer_tr").show('slow');
			}
		});

		$(".button_jquery_upload").button({
			icons: {
				primary: "ui-icon-folder-open"
			}
		});

		$(".button_jquery_save").button({
			icons: {
				primary: "ui-icon-disk"
			}
		});

		$(".button_jquery_lookup").button({
			icons: {
				primary: "ui-icon-search"
			}
		});

		$(".button_jquery_new").button({
			icons: {
				primary: "ui-icon-document"
			}
		});

		$("select").chosen({
			no_results_text: "Oops, nothing found!",
			disable_search_threshold: 10,
			placeholder_text_single: '  ',
			search_contains: true
		});

		$("div .chosen-container").each(function(index, element) {
			if($(this).attr('style') =='width: 0px;'){
				$(this).removeAttr('style');
				$(this).css('display','block');

			}
		});

		$("#remote_conn_type").change(function(e) {
			if($(this).val().toLowerCase() == 's'){
				$("#remote_sys_port").val('22');
			}else{
				$("#remote_sys_port").val('23');
			}
		});

		<?php if( strtolower($rowS['property']) == 'f' and ($open_reports_cur > 0 or $open_requests_cur > 0)){ ?>
		$("#future").prop('disabled', true);
		$("#future").trigger("chosen:updated");
		$("#future_msg").show();

		<?php }else if(strtolower($rowS['property']) != 'c' and strtolower($rowS['property']) != 'f' and ($open_reports_sel > 0 or $open_requests_sel > 0)){ ?>
		$("#archived_select").prop('disabled', true);
		$("#archived_select").trigger("chosen:updated");
		$("#archive_msg").show();

		<?php }else if(strtolower($rowS['property']) == 'c' and ($open_reports_sel > 0 or $open_requests_sel > 0)){ ?>
		$("#archived_select").prop('disabled', true);
		$("#archived_select").trigger("chosen:updated");
		$("#archive_msg").show();
		<?php } ?>

		$("#pm_start, #pm_freq").change(function (e){
			if($("#pm_freq").val() == 0){
				$("#pm_start_list").html("");
				$("#pm_dates").val("");
				return;
			}

			if($("#pm_start").val() == ""){
				$("#pm_start_list").html("");
				$("#pm_dates").val("");
				return;
			}

			var pms = [];
			var pms_disp = "";

			var pm_start = moment($("#pm_start").val(), "YYYY-MM-DD");
			pm_start.utc();
			console.log("PM START: "+pm_start.format("YYYY-MM-DD"));

			for(var i=1; i<=12/$("#pm_freq").val(); i++){
				if(i != 1){
					pm_start.add($("#pm_freq").val(), "months");
				}
				pms.push([pm_start.get('month')+1, pm_start.get('date')]);
			}

			pms.sort(
				function sortFunction(a, b) {
					if (a[0] === b[0]) {
						return 0;
					}
					else {
						return (a[0] < b[0]) ? -1 : 1;
					}
				});
			console.log(pms);

			$("#pm_dates").val(JSON.stringify(pms));
			console.log($("#pm_dates").val());


			for(var p=0; p <= pms.length - 1; p++){
				//pms[p][0] = allMonths[pms[p][0]];
				pms_disp += allMonths[pms[p][0]-1] + "/" + pms[p][1] + "&nbsp";
			}
			$("#pm_start_list").html("PM Due: " + pms_disp);

		})

		var notesTableDT = $('#notesTable')
			.on('xhr.dt', function ( e, settings, json, xhr ) {
				//console.log( json );
				if(json['data'].length == 0){
					$("#notesDiv").hide();
				}else{
					$("#notesDiv").show();
				}
			})
			.dataTable({
				"bJQueryUI": true,
				"searching": false,
				"lengthChange": false,
				"sPaginationType": "full_numbers",
				"aaSorting": [],
				"autoWidth": false,
				"ajax": {
					'type': 'POST',
					'url': 'systems_notes.php',
					'data': {
						method: 'get',
						system_unique_id: '<?php echo $unique_id; ?>'
					}
				}
			});

		var creditNotesTableDT = $('#creditNotesTable')
			.on('xhr.dt', function ( e, settings, json, xhr ) {
				//console.log( json );
				if(json['data'].length == 0){
					$("#creditNotesDiv").hide();
				}else{
					$("#creditNotesDiv").show();
				}
			})
			.dataTable({
				"bJQueryUI": true,
				"searching": false,
				"lengthChange": false,
				"sPaginationType": "full_numbers",
				"aaSorting": [],
				"autoWidth": false,
				"ajax": {
					'type': 'POST',
					'url': 'systems_notes.php',
					'data': {
						method: 'get',
						credit: true,
						system_ver_unique_id: '<?php echo $rowS['ver_unique_id']; ?>'
					}
				}
			});

		$('#contract_month_amt').change(function(){
			if(!$.isNumeric($(this).autoNumeric('get'))){
				console.log("Invalid value");
				return;
			}
			calc_financial();
		});

	});
	function cash(status){
	    if(status == undefined){
	        status = 'init';
        }
		var cash_opts = {aSign: '$ '};
		switch (status){
			case 'init':
				$(".cash").autoNumeric('init', cash_opts);
				break;
			case 'update':
				$(".cash").autoNumeric('update', cash_opts);
				break;
			case 'destroy':
				$(".cash").each(function(i){
					var self = $(this);
					try{
						var v = self.autoNumeric('get');
						self.autoNumeric('destroy');
						self.val(v);
					}catch (err){
						console.log("Not an autonumeric field: " + self.attr("id"));
					}
				});
				//$(".cash").autoNumeric('destroy');
				break;
		}
	}
	function calc_financial(){
		var credit_hold = <?php echo strtolower($rowS['credit_hold'])=='y'? 'true':'false'; ?>;
		var contract_end = moment.parseZone($("#contract_end_date").val().toString() + "T00:00:00-00:00", "YYYY-MM-DDTHH:mm:ssZ");
		var now = moment().utc();
		//var now = moment.parseZone("2016-04-05T00:00:00-00:00", "YYYY-MM-DDTHH:mm:ssZ");
		var contract_days_left = contract_end.diff(now.subtract(1, 'days'), 'days');
		var year_end_month = <?php echo $settings->fiscal->year_end_month; ?>;
		var year_end_day = <?php echo $settings->fiscal->year_end_day; ?>;
		var year_end = moment.parseZone(now.year().toString() + "-" + year_end_month + "-" + year_end_day + "T00:00:00-00:00", "YYYY-MM-DDTHH:mm:ssZ");
		var warranty_end = undefined;
		var current_month_amt = 0;
		var exp_back_log = 0;
		var cur_year = moment().utc().format("YY");
		var fiscal_year = "";


		if(now > year_end){
			year_end.add(1, 'year');
			fiscal_year = cur_year + "/" + year_end.format("YY");
		}else{
			fiscal_year = (cur_year - 1) + "/" + cur_year;
		}

		var fiscal_months_left = year_end.diff(now, 'months', true);
		console.log(year_end.diff(now, 'days',true));
		var contract_months_left = contract_end.diff(now, 'months', true);

		if($("#warranty_end_date").val() != ""){
			warranty_end = moment.parseZone($("#warranty_end_date").val().toString() + "T00:00:00-00:00", "YYYY-MM-DDTHH:mm:ssZ");
			if(warranty_end > now){
				console.log("Under Warranty");
			}else{
				if (!credit_hold) {
					if(intval(contract_days_left) >= 0) {
						$("#current_month_amt").val($("#contract_month_amt").autoNumeric('get'));
						current_month_amt = $("#contract_month_amt").autoNumeric('get');
						exp_back_log = intval($("#contract_month_amt").autoNumeric('get')) * contract_months_left;
					}
				}
			}
			if(intval(contract_days_left) >= 0) {
				exp_back_log = intval($("#contract_month_amt").autoNumeric('get')) * contract_months_left;
			}
		}else {
			if (!credit_hold) {
				if(intval(contract_days_left) >= 0) {
					$("#current_month_amt").val($("#contract_month_amt").autoNumeric('get'));
					current_month_amt = $("#contract_month_amt").autoNumeric('get');
					exp_back_log = intval($("#contract_month_amt").autoNumeric('get')) * contract_months_left;
				}
			}
			if(intval(contract_days_left) >= 0) {
				exp_back_log = intval($("#contract_month_amt").autoNumeric('get')) * contract_months_left;
			}
		}

		var amt_left = undefined;

		if(contract_end.isBetween(now, year_end)){
			amt_left = intval(current_month_amt) * contract_months_left;
		}else{
			amt_left = intval(current_month_amt) * fiscal_months_left;
		}

		var total_back_log = intval(current_month_amt) * contract_months_left;
		$(".fiscal_year").html(fiscal_year);

		console.log("Credit Hold: " + credit_hold);
		console.log("Now: " + now.format());
		console.log("Current Year: " + cur_year);
		console.log("Fiscal Year: " + fiscal_year);
		console.log("Contract End: " + contract_end.format());
		console.log("Contract Days Left: " + contract_days_left);
		console.log("Year End Month: " + year_end_month);
		console.log("Year End Day: " + year_end_day);
		console.log("Year End: " + year_end.format());
		console.log("Fiscal Months Left: " + fiscal_months_left);
		console.log("Contract Months Left: " + contract_months_left);
		console.log("Current Month Amount: " + current_month_amt);
		console.log("Amount Left: " + amt_left);
		console.log("Expected Backlog: " + exp_back_log);
		console.log("Total Backlog: " + total_back_log);

		$("#contract_days_left").val(contract_days_left.toFixed(2));
		$("#contract_months_left").val(contract_months_left.toFixed(2));
		$("#fiscal_months_left").val(fiscal_months_left.toFixed(2));
		$("#amount_left").val(amt_left.toFixed(2));
		$("#expected_backlog").val(exp_back_log.toFixed(2));
		$("#total_backlog").val(total_back_log.toFixed(2));
		cash('update');
	}
	$(function() {
		$("#pm_freq").change();

		$( "#contract_start_date" ).datepicker({
			numberOfMonths: 1,
			showButtonPanel: true,
			changeYear: true,
			duration: "fast",
			dateFormat: "<?php echo dpdispfd; ?>"
		});
		$( "#contract_end_date" ).datepicker({
			numberOfMonths: 1,
			showButtonPanel: true,
			changeYear: true,
			duration: "fast",
			dateFormat: "<?php echo dpdispfd; ?>"
		});
		$( "#warranty_start_date" ).datepicker({
			numberOfMonths: 1,
			showButtonPanel: true,
			changeYear: true,
			duration: "fast",
			dateFormat: "<?php echo dpdispfd; ?>"
		});
		$( "#warranty_end_date" ).datepicker({
			numberOfMonths: 1,
			showButtonPanel: true,
			changeYear: true,
			duration: "fast",
			dateFormat: "<?php echo dpdispfd; ?>"
		});
		$( "#date_installed" ).datepicker({
			numberOfMonths: 1,
			showButtonPanel: true,
			changeYear: true,
			duration: "fast",
			dateFormat: "<?php echo dpdispfd; ?>"
		});
		$( "#last_pm" ).datepicker({
			numberOfMonths: 1,
			showButtonPanel: true,
			changeYear: true,
			duration: "fast",
			dateFormat: "<?php echo dpdispfd; ?>"
		});
		$( "#acceptance_date" ).datepicker({
			numberOfMonths: 1,
			showButtonPanel: true,
			changeYear: true,
			duration: "fast",
			dateFormat: "<?php echo dpdispfd; ?>"
		});
		$( "#arrival_date" ).datepicker({
			numberOfMonths: 1,
			showButtonPanel: true,
			changeYear: true,
			duration: "fast",
			dateFormat: "<?php echo dpdispfd; ?>"
		});
		$( "#service_key_expire" ).datepicker({
			numberOfMonths: 1,
			showButtonPanel: true,
			changeYear: true,
			duration: "fast",
			dateFormat: "<?php echo dpdispfd; ?>"
		});
		$( "#pm_start" ).datepicker({
			numberOfMonths: 1,
			showButtonPanel: true,
			changeYear: false,
			duration: "fast",
			dateFormat: "<?php echo dpdispfd; ?>"
		});
		/////////////////////////////////////////////////////////////////////////////////////
		$(".iframeUpload").fancybox({
				'type'			: 'iframe',
				'height'		: 600,
				'fitToView'		: true,
				'maxWidth'		: 900,
				'maxHeight'		: 600,
				'autoSize'		: false,
				'closeBtn'		: true,
				'margin'		: [5,5,5,5]
		});
		
		$("#archived_select").change(function(){
			if(document.getElementById("archived_select").value == "Y"){
				$.prompt("<h3>Archiving a system could disable customer access and<br>archive the facility and customer account if no other systems are assigned to them.<br> Archive system?</h3>",{
					title: "Caution",
					buttons: { Yes: 1, No: -1 },
					focus: 1,
					submit:function(e,v,m,f){ 
						if(v == 1){
							$.prompt.close();
							$("#archived_div").removeProp('style');
							$("#archived_select").prop('disabled', true);
							$("#archived_select").trigger("chosen:updated");
							$("#archived").prop('checked', true);
							$("#future").val('N');
							$("#future").prop('disabled', true);
							$("#future").trigger("chosen:updated");
							$("#future_new").prop('checked', false);
							$("#future_div").hide();
						}else{
							$.prompt.close();
						}
						e.preventDefault();
					}
				});
			}else{
				$.prompt("<h3>Unarchiving this system will not enable customer access or unarchive the facility and customer accounts.<br> Unarchive system?</h3>",{
					title: "Caution",
					buttons: { Yes: 1, No: -1 },
					focus: 1,
					submit:function(e,v,m,f){ 
						if(v == 1){
							$.prompt.close();
							$("#archived_div").hide();
							$("#archived_select").prop('disabled', true);
							$("#archived_select").trigger("chosen:updated");
							$("#archived").prop('checked', false);
							$("#future").prop('disabled', true);
							$("#future").trigger("chosen:updated");
							$("#future_new").prop('checked', false);
							$("#future_div").hide();
						}else{
							$.prompt.close();
						}
						e.preventDefault();
					}
				});	
			}
		});
		<?php if($edit){?>
		$("#future").change(function(){
			if(document.getElementById("future").value == "Y" && current_future == false){
				$.prompt("<h3>Setting this system to future will duplicate the current system with the current values "
							  +"and unassign the facility and engineers, clear any credit hold or pre-paid, "
							  +"blank the system nickname, contact info, special email, arrival date, and contract info<br><br>"
							  +"Proceed?</h3>",{
					buttons: { Yes: 1, No: -1 },
					focus: 1,
					submit:function(e,v,m,f){ 
						if(v == 1){
							$.prompt.close();
							$(".future_clear").each(function(index, element) {
								//console.log(element);
								if( $(this).is("#credit_hold") ){
									$(this).val('N');	
								}else if( $(this).is("#pre_paid") ){
									$(this).val('N');	
								}else{
									$(this).val('');
								}
								$(this).trigger("chosen:updated");
							});
							$("#future_div").removeProp('style');
							$("#pre_paid_div").hide();
							$("#future").prop('disabled', true);
							$("#future").trigger("chosen:updated");
							$("#future_new").prop('checked', true);
							$("#archived_div").hide();
							$("#archived_select").prop('disabled', true);
							$("#archived_select").trigger("chosen:updated");
							$("#archived").prop('checked', false);
						}else{
							$.prompt.close();
							$("#future").val('N');
							$("#future").trigger("chosen:updated");
						}
						e.preventDefault();
					}
				});
			}
			if(document.getElementById("future").value == "N" && current_future == true){
				$.prompt("<h3>Setting this system to the current one will archive the existing current system<br><br>"
							  +"Proceed?</h3>",{
					buttons: { Yes: 1, No: -1 },
					focus: 1,
					submit:function(e,v,m,f){ 
						if(v == 1){
							$.prompt.close();
							$("#future_div").hide();
							$("#future").prop('disabled', true);
							$("#future").trigger("chosen:updated");
							$("#future_convert").prop('checked', true);
							$("#archived_div").hide();
							$("#archived_select").prop('disabled', true);
							$("#archived_select").trigger("chosen:updated");
							$("#archived").prop('checked', false);
						}else{
							$.prompt.close();
							$("#future").val('Y');
							$("#future").trigger("chosen:updated");
						}
						e.preventDefault();
					}
				});
			}
		});
		<?php }else{ ?>
		$("#future").change(function(){
			if(document.getElementById("future").value == "Y"){
				$.prompt("<h3>Set this system as a new future system?</h3>",{
					buttons: { Yes: 1, No: -1 },
					focus: 1,
					submit:function(e,v,m,f){
						if(v == 1){
							$.prompt.close();
							$("#future_div").removeProp('style');
							//$("#future").prop('disabled', true);
							$("#future").trigger("chosen:updated");
							//$("#future_new").prop('checked', true);
						}else{
							$.prompt.close();
							$("#future").val('N');
							$("#future").trigger("chosen:updated");
						}
						e.preventDefault();
					}
				});
			}
			if(document.getElementById("future").value == "N"){
				$.prompt("<h3>Setting this as a new current system?</h3>",{
					buttons: { Yes: 1, No: -1 },
					focus: 1,
					submit:function(e,v,m,f){
						if(v == 1){
							$.prompt.close();
							$("#future_div").hide();
							//$("#future").prop('disabled', true);
							$("#future").trigger("chosen:updated");
						}else{
							$.prompt.close();
							$("#future").val('Y');
							$("#future").trigger("chosen:updated");
						}
						e.preventDefault();
					}
				});
			}
		});

	<?php } ?>
	});
	function submitcheck(action = '') {
		var debug = <?php echo($debug)?"true":"false"; ?>;
		var edit = <?php echo($edit)?"true":"false"; ?>;
		ids = [];
		tabs = [];
		errors = [];

		if(action == 'del'){
			<?php if($open_reports_sel > 0 or $open_requests_sel > 0){ ?>
//				errors.push("System has open reports/requests so cannot be deleted!")
//				showErrors(ids,errors,tabs);
				$.prompt("<h2>System has open reports/requests so cannot be deleted!</h2>",{
					title: "Error",
					buttons: { OK: 1 },
					focus: 1,
					submit:function(e,v,m,f){
						$.prompt.close();
						e.preventDefault();
					}
				});
				return;
			<?php } ?>
			$.prompt("<h2>Deleting a system will disable customer access and delete the facility and customer account if no other systems are assigned to them.<br>This action cannot be undone!<br>Delete system?</h2>",{
				title: "Question",
				buttons: { Yes: 1, No: -1 },
				focus: 1,
				submit:function(e,v,m,f){
					if(v == 1){
						$.prompt.close();
						<?php if($debug){?>
						if(!confirm('DEBUG: really submit?!')){
							return;
						}
						<?php } ?>
						cash('destroy');
						$("#del_system").attr('checked', true);
						document.forms['form'].submit();
					}else{
						$.prompt.close();
					}
					e.preventDefault();
				}
			});
			return;
		}

		if($("#lblSystemId").html().indexOf("!") >= 0){ids.push("#system_id");tabs.push("#li-tab-10");}
		if($("#system_nickname").val()==""){ids.push("#system_nickname");tabs.push("#li-tab-10");}
		if(!check_tbd($("#system_nickname").val())){ids.push("#system_nickname");tabs.push("#li-tab-10");errors.push("Nickname must not be N/A");}

		if($("#system_id").val()==""){ids.push("#system_id");tabs.push("#li-tab-10");}
		if($("#system_type").val()==""){ids.push("#system_type_chosen");tabs.push("#li-tab-10");}
		if($("#system_serial").val()==""){ids.push("#system_serial");tabs.push("#li-tab-10");}
		if(!check_serial($("#system_serial").val())){ids.push("#system_serial");tabs.push("#li-tab-10");errors.push("System Serial must not be N/A");}

		if($("#date_installed").val()==""){ids.push("#date_installed");tabs.push("#li-tab-10");}
		if($("#pm_freq").val()==""){ids.push("#pm_freq");tabs.push("#li-tab-10");}
		if($("#pm_start").val()==""){ids.push("#pm_start");tabs.push("#li-tab-10");}

		if($("#mr_lhe_list").val()=="Y" && $("#mr_lhe_contact").val()==""){ids.push("#mr_lhe_contact");tabs.push("#li-tab-20");}
		if($("#mr_lhe_list").val()=="Y" && $("#mr_lhe_phone").val()==""){ids.push("#mr_lhe_phone");tabs.push("#li-tab-20");}

		if($("#facility_id").val()==""){ids.push("#facility_id");tabs.push("#li-tab-30");}

		if($("#engineer_pri").val()==""){ids.push("#engineer_pri_chosen");tabs.push("#li-tab-40");}
		if($("#engineer_sec").val()==""){ids.push("#engineer_sec_chosen");tabs.push("#li-tab-40");}

		if($("#contact_name").val()==""){ids.push("#contact_name");tabs.push("#li-tab-50");}
		if($("#contact_phone").val()==""){ids.push("#contact_phone");tabs.push("#li-tab-50");}

		if($("#arrival_date").val()==""){ids.push("#arrival_date");tabs.push("#li-tab-10");}



		if($("#contract_type").val()=="1" || $("#contract_type").val()=="3" || $("#contract_type").val()==""){
			if($("#contract_terms").val()==""){ids.push("#contract_terms");tabs.push("#li-tab-70");}
			if($("#contract_start_date").val()==""){ids.push("#contract_start_date");tabs.push("#li-tab-70");}
			if($("#contract_end_date").val()==""){ids.push("#contract_end_date");tabs.push("#li-tab-70");}
			if($("#contract_hours").val()==""){ids.push("#contract_hours");tabs.push("#li-tab-70");}
		}
		if($("#contract_type").val()==""){ids.push("#contract_type_chosen");tabs.push("#li-tab-70");}


		//if($("#email_list").val()==""){ids.push("#email_list");tabs.push("#li-tab-100");}
		if($("#status").val()==""){ids.push("#status");tabs.push("#li-tab-100");}
		if($("#last_pm").val()==""){ids.push("#last_pm");tabs.push("#li-tab-100");}



		if(Date.parse($("#contract_start_date").val()) > Date.parse($("#contract_end_date").val())){ids.push("#contract_start_date");ids.push("#contract_end_date");tabs.push("#li-tab-70"); errors.push("The Contract Start Date cannot be AFTER the Contract End Date");}
		if(Date.parse($("#warranty_end_date").val()) > Date.parse($("#contract_end_date").val())){ids.push("#warranty_end_date");ids.push("#contract_end_date");tabs.push("#li-tab-70"); errors.push("The Warranty End Date cannot be AFTER the Contract End Date");}
		if(Date.parse($("#warranty_start_date").val()) > Date.parse($("#warranty_end_date").val())){ids.push("#warranty_start_date");ids.push("#warranty_end_date");tabs.push("#li-tab-70"); errors.push("The Warranty Start Date cannot be AFTER the Warranty End Date");}

		var regex = new RegExp(/[\'@\"\$\\\#\+\?]/g);


		if(document.getElementById("system_nickname").value.match(regex)){ids.push("#system_nickname");errors.push("System Nickname contains invalid characters  ' @ $ \ # + ? ");}

		if($("#remote_eth0_mac").val() != ""){
			var regex_mac = /^([0-9A-F]{2}[:-]){5}([0-9A-F]{2})$/;
			//alert(regex.test(mystring.toUpperCase()));
			//mystring = '3d:f2:c9:a6:b3:4f';
			//alert(regex.test(mystring.toUpperCase()));
			if(regex_mac.test($("#remote_eth0_mac").val().toUpperCase())){
				if($("#remote_sys_ip").val()==""){ids.push("#remote_sys_ip");tabs.push("#li-tab-120");}
				if($("#remote_sys_uid").val()==""){ids.push("#remote_sys_uid");tabs.push("#li-tab-120");}
			}else{
				tabs.push("#li-tab-12");
				ids.push("#remote_eth0_mac");
				errors.push("Remote Computer Serial is invalid!");
			}
		}

		if($("#mobile").val() == 'Y'){
			if($("#trailer_id").val() == ''){
				tabs.push("li-tab-10");
				ids.push("#trailer_id");
				errors.push("Assigned trailer id is blank!");
			}
		}

		$("#location").val(ConvChar($("#location").val()));
		$("#contract_terms").val(ConvChar($("#contract_terms").val()));


		//if(debug){
			console.log("ids: " + ids);
			console.log("tabs: " + tabs);
			console.log("errors: " + errors);
		//}

		console.log("Archived:" + $("#archived").val());
		if(edit){
			if($("#archived_select").val().toLowerCase()=="n"){
				showErrors(ids,errors,tabs);
			}else{
				while(ids.length > 0) {
					ids.pop();
				}
			}
		}else{
			showErrors(ids,errors,tabs);
		}

		if(ids.length <= 0){
			var prompt_question = "<h3><?php if($edit){echo "Update";}else{echo "Add New";} ?> System?</h3>";
			if($("#future").val() == 'Y'){
				prompt_question = "<h3>Create Future System?</h3>";
			}
			$.prompt(prompt_question,{
				title: "Question",
				buttons: { Yes: 1, No: -1 },
				focus: 1,
				submit:function(e,v,m,f){
					if(v == 1){
						$.prompt.close();
						<?php if($debug){?>
							if(!confirm('DEBUG: really submit?!')){
								return;
							}
						<?php } ?>
						cash('destroy');
						document.forms['form'].submit();
					}else{
						$.prompt.close();
					}
					e.preventDefault();
				}
			});
		}

	}
	function ConvChar( str ) {
	  c = {'<':'&lt;', '>':'&gt;', '&':'&amp;', '"':'&quot;', "'":'&#039;',
		   '#':'&#035;', '@':'&#64;' };
	  return str.replace( /[<&>'"#@]/g, function(s) { return c[s]; } );
	}
	function showErrors(ids,errors,tabs){
		if(tabs.length > 0){
			errors.unshift("<h1><b>Correct the red highlighted fields</b></h1>");
		}
		$("input, select, .chosen-container").each(function(index, element) { //set all inputs to not highlighted
			$(this).animate({
				borderColor: "#2C3594",
				boxShadow: 'none'
			});
		});

		$("#tabs > ul > li").each(function(index, element) { //set all inputs to not highlighted
			$(this).animate({
				borderColor: "#2C3594",
				boxShadow: 'none'
			});
		});

		$.each(ids,function(index,value){//highlight inputs
			$(value).animate({
				borderColor: "#cc0000",
				boxShadow: '0 0 5px 3px rgba(255,0,0,0.4)'
			});
		});

		$.each(tabs,function(index,value){ //highlight tabs
			$(value).animate({
				borderColor: "#cc0000",
				boxShadow: '0 0 5px 3px rgba(255,0,0,0.4)'
			});
		});

		$("#errors > span").html("");
		$.each(errors,function(index, value){
			$("#errors > span").append(value + "<br>");
		});
		$("#errors").show('slow');
		//$(document).scrollTop(0);
	}
	function checkSystem(str){
	if (str==""){
	  return;
	}
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}else{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			if(str != "<?php echo $rowS['system_id']; ?>"){
				document.getElementById("lblSystemId").innerHTML=xmlhttp.responseText;
			}else{
				document.getElementById("lblSystemId").innerHTML="System ID";
			}
		 }
	  }
	xmlhttp.open("GET","scripts/check_system_id_edit.php?q="+str,true);
	xmlhttp.send();
	}
	function setLastPm(){
		if($("#last_pm").val() == ""){
			$("#last_pm").val($("#date_installed").val());
		}
	}
	function setEmailList(){
		if($("#email_list").val() == ""){
			$("#email_list").val($("#system_contact_email").val());
		}
	}
	function gotofacility(){
		<?php if(!$_SESSION['mobile_device']){ ?>
		$.prompt("<h3>Your changes will not be saved if you proceed!</h3>",{
			title: "Caution",
			buttons: { Proceed: 1, No: -1 },
			focus: 1,
			submit:function(e,v,m,f){
				if(v == 1){
					$.prompt.close();
					window.location.href = "facilities.php?e&unique_id=<?php echo $rowS['facility_unique_id'] ?>&user_id=<?php echo $_SESSION['login'] ?>";
				}else{
					$.prompt.close();
				}
				e.preventDefault();
			}
		});
		<?php }else{ ?>
		if(confirm("Your changes will not be saved if you proceed!")){
			window.location.href = "facilities.php?e&unique_id=<?php echo $rowS['facility_unique_id'] ?>&user_id=<?php echo $_SESSION['login'] ?>";
		}
		<?php } ?>
	}
	function sortNumber(a,b) {
		return a - b;
	}
	function iframeNoteClosed(){
		$('#notesTable').DataTable().ajax.reload();
		$('#creditNotesTable').DataTable().ajax.reload();
	}
</script>
</head>
<body>
<?php require_once($header_include); ?>
<div id="OIReportContent">
	<div id="styledForm">
		<form id="form" name="form" method="post" autocomplete="off" action="systems_do.php">
			<?php if($edit){ ?>
			<h1>Editing System: <?php echo $rowS['nickname'];?></h1>
			<span class="red">Required fields marked with an *</span>
			<div id="credit_hold_div" <?php if(strtolower($rowS['credit_hold'])=="n"){echo "style=\"display:none\"";} ?>> <br />
				<h1 class="credit_hold_bold">System is on credit hold</h1>
			</div>
			<div id="pre_paid_div" <?php if(strtolower($rowS['pre_paid'])=="n"){echo "style=\"display:none\"";} ?>> <br />
				<h1 class="pre_paid_bold">System is pre-paid only</h1>
			</div>
			<div id="future_div" <?php if(strtolower($rowS['property'])!="f"){echo "style=\"display:none\"";} ?>> <br />
				<h1 class="future_bold">System is future service</h1>
			</div>
			<div id="archived_div" <?php if(strtolower($rowS['property'])!="a"){echo "style=\"display:none\"";} ?>> <br />
				<h1 class="archived_bold">System is archived</h1>
			</div>
			<div id="goverment_div" <?php if(strtolower($rowS['goverment_contract'])!="y"){echo "style=\"display:none\"";} ?>> <br />
				<h1 class="goverment_bold">Goverment Contract</h1>
			</div>
			<?php }else{ ?>
			<h1>Create New System </h1>
			<span class="red">Required fields marked with an *</span>
			<?php } ?>
			<p>&nbsp;</p>
			<div id="errors" style="text-align:center;display:none"> 
				<!--<h2 style="margin:0px; padding:0px">Errors to fix</h2>--> 
				<span style="color:#F00"> </span> 
			</div>


			<table width="100%" cellpadding="5" cellspacing="15">
				<tr>
					<?php if($edit){ ?>
					<td width="15%">&nbsp;<label>Archived</label>
						<select name="archived_select" id="archived_select">
							<option value="Y"<?php if($edit){if(strtolower($rowS['property'])=="a"){echo " selected";}} ?>>Yes</option>
							<option value="N"<?php if($edit){if(strtolower($rowS['property'])!="a"){echo " selected";}} ?>>No</option>
						</select></td>
					<?php } ?>
					<td width="15%">&nbsp;<label>Future</label>
						<select name="future" id="future">
							<option value="Y"<?php if($edit){if(strtolower($rowS['property'])=="f"){echo " selected";}} ?>>Yes</option>
							<option value="N"<?php if($edit){if(strtolower($rowS['property'])!="f"){echo " selected";}}else{echo " selected";} ?>>No</option>
						</select></td>
					<td align="center" valign="middle">
						<div id="future_msg" style="display:none" class="red">Current system has open reports/requests or is archived<br>so this future system cannot be converted to current system!</div>
						<div id="archive_msg" style="display:none" class="red">Current system has open reports/requests.<br>Reports/Requests must be closed before archiving the system</div>
					</td>
				</tr>
			</table>

			<div class="line"></div>
			<div id="tabs">
				 <ul>
					 <li id="li-tab-10"><a href="#tab-10">System</a></li>
					 <li id="li-tab-20"><a href="#tab-20">MRI</a></li>
					 <li id="li-tab-30"><a href="#tab-30">Facility</a></li>
					 <li id="li-tab-40"><a href="#tab-40">Engineers</a></li>
					 <li id="li-tab-50"><a href="#tab-50">System Contact</a></li>
					 <li id="li-tab-70"><a href="#tab-70">Contract</a></li>
					 <li id="li-tab-80"><a href="#tab-80">Credit</a></li>
					 <li id="li-tab-90"><a href="#tab-90">Notes</a></li>
					 <li id="li-tab-100"><a href="#tab-100">Special</a></li>
					 <li id="li-tab-110"><a href="#tab-110">Docs</a></li>
					 <li id="li-tab-120"><a href="#tab-120">Remote</a></li>
					 <li id="li-tab-130"><a href="#tab-130">Finance</a></li>
				</ul>

				<div id="tab-10" class="tab">
							<table width="100%" cellpadding="5" cellspacing="5">
								<tr>
									<td width="20%"><label id="lblSystemId">System ID <span class="red">*</span></label>
										<input type="text" name="system_id" id="system_id" onblur="checkSystem(this.value)" <?php if($edit){ echo 'readonly'; } ?> value="<?php if($edit){echo $rowS['system_id'];} ?>"/></td>
									<td><label>Nickname <span class="red">*</span></label>
										<input type="text" name="system_nickname" id="system_nickname" class="future_clear" value="<?php if($edit){echo $rowS['nickname'];} ?>"/></td>
								</tr>
								<tr>
									<td width="50%"><label>System Type <span class="red">*</span></label>
										<select name="system_type" id="system_type">
											<option value=""></option>
											<?php
												while($rowEquipment = $resultEquipment->fetch_assoc())
												{
													echo "<option value='" . $rowEquipment['id'] . "'";
													if($edit){
														if($rowS['system_type']==$rowEquipment['id']){echo " selected";}
													}
													echo ">" . $rowEquipment['type'] ." - " . $rowEquipment['mfg'] . " - " . $rowEquipment['name']. "</option>\n";
												}
											  ?>
										</select></td>
									<td><label>System Location In building</label>
										<input type="text" name="location" id="location" value="<?php if($edit){echo $rowS['location'];} ?>" /></td>
								</tr>
								<tr>
									<td width="50%"><label>Software Version</label>
										<input type="text" name="sw_ver" id="sw_ver" value="<?php if($edit){echo $rowS['sw_ver'];} ?>" /></td>
									<td width="50%"><label>Acceptance Date</label>
										<input type="text" name="acceptance_date" id="acceptance_date" value="<?php if($edit and $rowS['acceptance_date'] != ''){echo date(phpdispfd, strtotime($rowS['acceptance_date']));} ?>" /></td>
								</tr>
								<tr>
									<td width="50%"><label>Date Installed <span class="red">*</span></label>
										<input type="text" name="date_installed" id="date_installed" value="<?php if($edit and $rowS['date_installed'] != ''){echo date(phpdispfd, strtotime($rowS['date_installed']));} ?>" onChange="setLastPm();" /></td>
									<td><label>Gantry/Magnet Serial Number <span class="red">*</span></label>
										<input type="text" name="system_serial" id="system_serial" value="<?php if($edit){echo $rowS['system_serial'];} ?>" /></td>
								</tr>
								<tr>
									<td width="50%"><label>PM Frequency <span class="red">* </span></label>
										<select name="pm_freq" id="pm_freq">
											<option value="0" <?php if($edit){if(strtolower($rowS['pm_freq'])=="0"){echo " selected";}} ?>>None (0)</option>
											<option value="2" <?php if($edit){if(strtolower($rowS['pm_freq'])=="2"){echo " selected";}} ?>>Bi-Monthly (2)</option>
											<option value="3" <?php if($edit){if(strtolower($rowS['pm_freq'])=="3"){echo " selected";}}else{echo " selected";} ?>>Quarterly (3)</option>
											<option value="4" <?php if($edit){if(strtolower($rowS['pm_freq'])=="4"){echo " selected";}} ?>>Tri-Annual (4)</option>
											<option value="6" <?php if($edit){if(strtolower($rowS['pm_freq'])=="6"){echo " selected";}} ?>>Bi-Annual (6)</option>
											<option value="12" <?php if($edit){if(strtolower($rowS['pm_freq'])=="12"){echo " selected";}} ?>>Yearly (12)</option>
										</select>
									</td>
									<td><label>System Logbook</label>
										<select id="log_book" name="log_book">
											<option value="Y"<?php if($edit){if(strtolower($rowS['log_book'])=="y"){echo " selected";}} ?>>Yes</option>
											<option value="N"<?php if($edit){if(strtolower($rowS['log_book'])=="n"){echo " selected";}}else{ echo " selected";} ?>>No</option>
										</select></td>
								</tr>
								<tr>
									<td width="50%"><label>PM Start Date <span class="red">*</span> <div id="pm_start_list" style="font-size: smaller; font-style: italic; text-align: right; float: right;"></div></label>
										<input type="text" name="pm_start" id="pm_start" value="<?php if($edit and $rowS['pm_start'] != ''){echo date(phpdispfd, strtotime($rowS['pm_start']));} ?>" /></td>
									<td width="50%"><label>Service Key</label>
										<input type="text" name="service_key" id="service_key" value="<?php if($edit){echo $rowS['service_key'];} ?>" /></td>
								</tr>

								<tr>
									<td width="50%"><label>Arrival Date <span class="red">*</span></label>
										<input type="text" name="arrival_date" id="arrival_date" class="future_clear" value="<?php if($edit and $rowS['arrival_date'] != ''){echo date(phpdispfd, strtotime($rowS['arrival_date']));} ?>" /></td>
									<td width="50%"><label>Service Key Expire</label>
										<input type="text" name="service_key_expire" id="service_key_expire" value="<?php if($edit and $rowS['service_key_expire'] != ''){echo date(phpdispfd, strtotime($rowS['service_key_expire']));} ?>" /></td>
								</tr>
								<tr>
									<td width="50%"><label>Mobile System</label>
										<select id="mobile" name="mobile">
											<option value="Y"<?php if($edit){if(strtolower($rowS['mobile'])=="y"){echo " selected";}} ?>>Yes</option>
											<option value="N"<?php if($edit){if(strtolower($rowS['mobile'])=="n"){echo " selected";}}else{ echo " selected";} ?>>No</option>
										</select></td>
									<td width="50%"><label>&nbsp;</label>
										&nbsp;</td>
								</tr>
								<tr id="mobile_trailer_tr" <?php if($edit){if(strtolower($rowS['mobile']) == 'n'){echo "style='display:none'";}}else{echo "style='display:none'";} ?>>
									<td width="50%"><label>Assigned Trailer <span class="red">*</span></label>
										<input type="text" name="trailer_id" id="trailer_id" readonly value="<?php if($edit){echo $rowS['trailer_id'];} ?>" /></td>
									<td width="50%" style="text-align:center;"><span style="font-size:larger; font-weight:bolder;">&#8678;</span> To change: Support &#8658;  Mainenance &#8658;  Manage Trailes</td>
								</tr>
							</table>
				</div>
				<div id="tab-20" class="tab">
							<table width="100%" cellpadding="5" cellspacing="5">
								<tr>
									<td width="50%"><label>Helium Monitoring</label>
										<select id="he_monitor" name="he_monitor">
											<option value="Y"<?php if($edit){if(strtolower($rowS['he_monitor'])=="y"){echo " selected";}} ?>>Yes</option>
											<option value="N"<?php if($edit){if(strtolower($rowS['he_monitor'])=="n"){echo " selected";}}else{ echo " selected";} ?>>No</option>
										</select></td>
									<td><label>He Monitor Serial</label>
										<input type="text" name="he_monitor_serial" id="he_monitor_serial" value="<?php if($edit){echo $rowS['he_monitor_serial'];} ?>" /></td>
								</tr>
							</table>
							<table width="100%" cellpadding="5" cellspacing="5">
								<tr>
									<td width="50%"><label>Helium Call List</label>
										<select id="mr_lhe_list" name="mr_lhe_list">
											<option value="Y"<?php if($edit){if(strtolower($rowS['mr_lhe_list'])=="y"){echo " selected";}} ?>>Yes</option>
											<option value="N"<?php if($edit){if(strtolower($rowS['mr_lhe_list'])=="n"){echo " selected";}}else{echo " selected";} ?>>No</option>
										</select></td>
									<td width="50%"><label>Helium Vessel Pressure</label>
										<select id="mr_lhe_press" name="mr_lhe_press">
											<option value="4"<?php if($edit){if(strtolower($rowS['mr_lhe_press'])=="4"){echo " selected";}}else{echo " selected";} ?>>4 PSIG</option>
											<option value="1"<?php if($edit){if(strtolower($rowS['mr_lhe_press'])=="1"){echo " selected";}} ?>>1 PSIG</option>
											<option value="0"<?php if($edit){if(strtolower($rowS['mr_lhe_press'])=="0"){echo " selected";}} ?>>NA</option>
										</select></td>
								</tr>
							</table>
							<table width="100%" cellpadding="5" cellspacing="5">
								<tr>
									<td width="50%"><label>Helium Call Contact</label>
										<input type="text" name="mr_lhe_contact" id="mr_lhe_contact" value="<?php if($edit){echo $rowS['mr_lhe_contact'];} ?>" /></td>
									<td width="50%">&nbsp;</td>
								</tr>
								<tr>
									<td width="50%"><label>Helium Call Email</label>
										<input type="text" name="mr_lhe_email" id="mr_lhe_email" value="<?php if($edit){echo $rowS['mr_lhe_email'];} ?>" /></td>
									<td width="50%"><label>Helium Call Phone</label>
										<input type="text" name="mr_lhe_phone" id="mr_lhe_phone" value="<?php if($edit){echo $rowS['mr_lhe_phone'];} ?>" /></td>
								</tr>
							</table>
				</div>
				<div id="tab-30" class="tab">
							<!--Facility-->
							<table width="100%" cellpadding="5" cellspacing="5">
								<tr>
									<td width="50%"><label>Assigned Facility <span class="red">*</span></label>
										<input type="text" name="facility_id" id="facility_id" class="future_clear" value="<?php if($edit){echo $rowS['facility_id'];} ?>" /></td>
									<td width="50%" style="text-align:center;"><a name="lookup_facility" class="iframeView button_jquery_lookup" href="facility_lookup.php">Lookup Facility</a></td>
								</tr>
							</table>
				</div>
				<div id="tab-40" class="tab">
							<!--Engineer Assignments-->
							<table width="100%" cellpadding="5" cellspacing="5">
								<tr>
									<td width="50%"><label>Primary Engineer <span class="red">*</span></label>
										<select name="engineer_pri" id="engineer_pri" class="future_clear">
											<option value=""></option>
										</select></td>
									<td><label>Secondary Engineer <span class="red">*</span></label>
										<select name="engineer_sec" id="engineer_sec" class="future_clear">
											<option value=""></option>
										</select></td>
								</tr>
							</table>
				</div>
				<div id="tab-50" class="tab">
							<!--System Contact-->
							<table width="100%" cellpadding="5" cellspacing="5">
								<tr>
									<td width="50%"><label>Name <span class="red">*</span></label>
										<input type="text" name="contact_name" id="contact_name" class="future_clear" value="<?php if($edit){echo $rowS['contact_name'];} ?>" /></td>
									<td width="50%"><label>Title</label>
										<input type="text" name="contact_title" id="contact_title" class="future_clear" value="<?php if($edit){echo $rowS['contact_title'];} ?>" /></td>
								</tr>
							</table>
							<table width="100%" cellpadding="5" cellspacing="5">
								<tr>
									<td width="33%"><label>Phone <span class="red">*</span></label>
										<input type="text" name="contact_phone" id="contact_phone" class="future_clear" value="<?php if($edit){echo $rowS['contact_phone'];} ?>" /></td>
									<td width="33%"><label>Cell</label>
										<input type="text" name="contact_cell" id="contact_cell" class="future_clear" value="<?php if($edit){echo $rowS['contact_cell'];} ?>" /></td>
									<td><label>Email</label>
										<input type="email" name="contact_email" id="contact_email" class="future_clear" value="<?php if($edit){echo $rowS['contact_email'];} ?>" /></td>
								</tr>
							</table>
							</td>
				</div>
				<div id="tab-70" class="tab">
							<!--Contract Info-->
							<table width="100%" cellpadding="5" cellspacing="5">
								<tr>
									<td width="33%"><label>Contract Type <span class="red">*</span></label>
											<select name="contract_type" id="contract_type" class="future_clear">
											<option value=""></option>
											<?php
												while($rowContract = $resultContract->fetch_assoc())
												{
													echo "<option value='" . $rowContract['id'] . "'";
													if($edit){
														if($rowS['contract_type']==$rowContract['id']){echo " selected";}
													}
													echo ">" . $rowContract['type'] . "</option>\n";
												}
											  ?>
										</select></td>
									<td width="34%"><label>Contract/Lease Start <span class="red">*</span></label>
										<input type="text" name="contract_start_date" id="contract_start_date" class="future_clear" value="<?php if($edit and $rowS['contract_start_date'] != ''){echo date(phpdispfd, strtotime($rowS['contract_start_date']));} ?>" /></td>
									<td width="33%"><label>Contract/Lease End <span class="red">*</span></label>
										<input type="text" name="contract_end_date" id="contract_end_date" class="future_clear" value="<?php if($edit and $rowS['contract_end_date'] != ''){echo date(phpdispfd, strtotime($rowS['contract_end_date']));} ?>" /></td>
								</tr>
								<tr>
									<td colspan="3"><label>Contract Terms <span class="red">*</span></label>
										<textarea name="contract_terms" id="contract_terms" class="future_clear"><?php if($edit){echo $rowS['contract_terms'];} ?></textarea>
									</td>
								</tr>
								<tr>
									<td><label>Contract Hours Per Year <span class="red">*</span></label>
										<input name="contract_hours" type="number" id="contract_hours" class="future_clear" value="<?php if($edit){echo $rowS['contract_hours'];} ?>" /></td>
									<td><label>Warranty Start</label>
										<input type="text" name="warranty_start_date" id="warranty_start_date" class="future_clear" value="<?php if($edit and $rowS['warranty_start_date'] != ''){echo date(phpdispfd, strtotime($rowS['warranty_start_date']));} ?>" /></td>
									<td><label>Warranty End</label>
										<input type="text" name="warranty_end_date" id="warranty_end_date" class="future_clear" value="<?php if($edit and $rowS['warranty_end_date'] != ''){echo date(phpdispfd, strtotime($rowS['warranty_end_date']));} ?>" /></td>
								</tr>
							</table>
							<table width="100%" cellpadding="5" cellspacing="5">
								<tr>
									<td width="50%"><label>Labor Rate Regular Time</label>
										<input type="number" name="labor_reg_rate" id="labor_reg_rate" value="<?php if($edit){echo $rowS['labor_reg_rate'];} ?>" /></td>
									<td width="50%"><label>Labor Rate Over-Time</label>
										<input type="number" name="labor_ot_rate" id="labor_ot_rate" value="<?php if($edit){echo $rowS['labor_ot_rate'];} ?>" /></td>
								</tr>
								<tr>
									<td><label>Travel Rate Regular Time</label>
										<input type="number" name="travel_reg_rate" id="travel_reg_rate" value="<?php if($edit){echo $rowS['travel_reg_rate'];} ?>" /></td>
									<td><label>Travel Rate Over-Time</label>
										<input type="number" name="travel_ot_rate" id="travel_ot_rate" value="<?php if($edit){echo $rowS['travel_ot_rate'];} ?>" /></td>
								</tr>
								<tr>
									<td><label>Tube Covered</label>
										<select id="tube_covered" name="tube_covered">
											<option value="Y"<?php if($edit){if(strtolower($rowS['tube_covered'])=="y"){echo " selected";}} ?>>Yes</option>
											<option value="N"<?php if($edit){if(strtolower($rowS['tube_covered'])=="n"){echo " selected";}}else{ echo " selected";} ?>>No</option>
										</select></td>
									<td><label>Pre-Pay For Service</label>
										<select id="pre_paid" name="pre_paid" class="future_clear">
											<option value="Y"<?php if($edit){if(strtolower($rowS['pre_paid'])=="y"){echo " selected";}} ?>>Yes</option>
											<option value="N"<?php if($edit){if(strtolower($rowS['pre_paid'])=="n"){echo " selected";}}else{ echo " selected";} ?>>No</option>
										</select></td>
								</tr>
								<tr>
									<td><label>Government Contract</label>
										<select id="government_contract" name="government_contract">
											<option value="Y"<?php if($edit){if(strtolower($rowS['government_contract'])=="y"){echo " selected";}} ?>>Yes</option>
											<option value="N"<?php if($edit){if(strtolower($rowS['government_contract'])=="n"){echo " selected";}}else{ echo " selected";} ?>>No</option>
										</select></td>
									<td>&nbsp;</td>
								</tr>
							</table>
				</div>
				<div id="tab-80" class="tab">
							<!--Credit-->
							<table width="100%" cellpadding="5" cellspacing="5">
								<tr>
									<td width="70%"><label>Credit Hold</label>
										<div id="creditNotesFrame" style="text-align:center">
											<?php
											$sql = "SELECT sn.note, sn.`date`, u.name, sn.credit_hold, sn.id
											FROM systems_notes AS sn
											LEFT JOIN users AS u ON u.uid = sn.uid
											WHERE sn.system_ver_unique_id = '".$rowS['ver_unique_id']."'
											AND sn.property = 'C'
											ORDER BY sn.`date` DESC;";
											s($sql);

											if(!$resultNotes = $mysqli->query($sql)){
												$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
												$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
												$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
											}
											?>
											<div id="creditNotesFrame">
												<div id="create_note" style="width:99%; text-align:center;">
													<div id="newNote" style="margin-bottom:15px; margin-top:5px;"><a class="iframeNote button_jquery_new" href="/report/common/systems_notes.php?method=new&credit&credit_status=<?php echo $rowS['credit_hold']; ?>&system_ver_unique_id=<?php echo $rowS['ver_unique_id']; ?>">New Credit Status</a></div>
												</div>
												<div id="creditNotesDiv" <?php if($resultNotes->num_rows == 0){echo "style=\"display:none\"";} ?>>
													<table width="100%" id="creditNotesTable" >
														<thead>
														<tr>
															<th width="20%">Date</th>
															<th width="55%">Entry</th>
															<th width="15%">User</th>
															<th width="10%">On Hold</th>
														</tr>
														</thead>
														<tbody>
														<?php
														if($resultNotes->num_rows != 0){
															while($rowNotes = $resultNotes->fetch_assoc())
															{
																echo "<tr>\n";
																echo "<td><a class='iframeNote' href='/report/common/systems_notes.php?method=view&id=".$rowNotes['id']."'>". date(storef,strtotime($rowNotes['date']))."</a></td>\n";
																echo "<td>". limit_words($rowNotes['note'],$probrpt_word_limt)."</td>\n";
																echo "<td>". $rowNotes['name']."</td>\n";
																echo "<td>". $rowNotes['credit_hold']."</td>\n";
																echo "</tr>\n";
															}
														}
														?>
														</tbody>
													</table>
												</div>
											</div>
										</div>

									</td>
								</tr>
							</table>
				</div>
				<div id="tab-90" class="tab">
							<!--Notes-->
							<table width="100%" cellpadding="5" cellspacing="5">
								<tr>
									<td width="70%"><label>System Notes</label>
										<div id="notesFrame" style="text-align:center">
											<?php
											$sql = "SELECT sn.note, sn.`date`, u.name, sn.id
											FROM systems_notes AS sn
											LEFT JOIN users AS u ON u.uid = sn.uid
											WHERE sn.system_unique_id = '".$unique_id."'
											AND sn.property = 'G'
											ORDER BY sn.`date` DESC;";

											if(!$resultNotes = $mysqli->query($sql)){
												$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
												$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
												$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
											}
											?>
											<div id="notesFrame">
												<div id="create_note" style="width:99%; text-align:center;">
													<div id="newNote" style="margin-bottom:15px; margin-top:5px;"><a class="iframeNote button_jquery_new" href="/report/common/systems_notes.php?method=new&system_ver_unique_id=<?php echo $rowS['ver_unique_id']; ?>">New Entry</a></div>
												</div>
												<div id="notesDiv" <?php if($resultNotes->num_rows == 0){echo "style=\"display:none\"";} ?>>
													<table width="100%" id="notesTable" >
														<thead>
														<tr>
															<th width="20%">Date</th>
															<th width="65%">Entry</th>
															<th width="15%">User</th>
														</tr>
														</thead>
														<tbody>
														<?php
														if($resultNotes->num_rows != 0){
															while($rowNotes = $resultNotes->fetch_assoc())
															{
																echo "<tr>\n";
																echo "<td><a class='iframeNote' href='/report/common/systems_notes.php?method=view&id=".$rowNotes['id']."'>".date(phpdispfdt, strtotime($rowNotes['date']))."</a></td>\n";
																echo "<td>". limit_words($rowNotes['note'],$probrpt_word_limt)."</td>\n";
																echo "<td>". $rowNotes['name']."</td>\n";
																echo "</tr>\n";
															}
														}
														?>
														</tbody>
													</table>
												</div>
											</div>
										</div>

									</td>
								</tr>
							</table>
				</div>
				<div id="tab-100" class="tab">
							<!--Special-->
							<table width="100%" cellpadding="5" cellspacing="5">
<!--								<tr>-->
<!--									<td colspan="3"><label>Email addresses for reports (To Change Edit Facility)</label>-->
<!--										<input type="text" name="email_list" id="email_list" class="future_clear" readonly value="--><?php //if($edit){echo $rowS['email_list'];} ?><!--" onChange="setEmailList();" /></td>-->
<!--								</tr>-->
								<tr>
									<td><label>System Status</label>
										<input type="number" name="status" id="status" value="<?php if($edit){echo $rowS['status'];}else{echo "1";} ?>" /></td>
									<td><label>Last PM <span class="red">*</span></label>
										<input type="text" name="last_pm" id="last_pm" value="<?php if($edit and $rowS['last_pm'] != ''){echo date(phpdispfd, strtotime($rowS['last_pm']));} ?>" /></td>
									<td id="no_validation_td"></td>
								</tr>
								<tr>
									<td><label>System UniqueID</label>
										<input type="text" readonly value="<?php echo $unique_id; ?>" /></td>
								</tr>
							</table>
				</div>
				<div id="tab-110" class="tab">
							<div id="uploadDocs">
								<a name="uploadX" class="iframeUpload button_jquery_upload"  href="/resources/kcfinder/kcfinder.php?unique_id=<?php echo $unique_id; ?>&table=systems_files"><?php echo $uploadBtnValue; ?></a>
							</div>

				</div>
				<div id="tab-120" class="tab">
							<!--Remote-->
							<table width="100%" cellpadding="5" cellspacing="5">
								<tr>
									<td width="50%"><label>Remote Computer Serial</label>
										<input type="text" name="remote_eth0_mac" id="remote_eth0_mac" value="<?php if($edit){echo $rowS['eth0_mac'];} ?>" />
									</td>
									<td width="50%" align="center">
										<a name="Lookup" class="iframeUpload button_jquery_lookup" href="scripts/lookup_remote_serial.php">Lookup Serial</a></td>
									</td>
								</tr>
							</table>
							<table width="100%" cellpadding="5" cellspacing="5">
								<tr>
									<td width="25%"><label>Console IP</label>
										<input type="text" name="remote_sys_ip" id="remote_sys_ip" value="<?php if($edit){echo $rowS['system_ip'];} ?>" />
									</td>
									<td width="25%"><label>Console Port</label>
										<input type="text" name="remote_sys_port" id="remote_sys_port" value="<?php if($edit){echo $rowS['system_port'];}else{echo '22';} ?>" />
									</td>
									<td width="25%"><label>Console User Login</label>
										<?php
											if($edit){
												if($rowS['eth0_mac'] == ""){
													$remote_sys_uid = 'service';
												}else{
													$remote_sys_uid = $rowS['system_uid'];
												}
											}else{
												$remote_sys_uid = 'service';
											}
										?>
										<input type="text" name="remote_sys_uid" id="remote_sys_uid" value="<?php echo $remote_sys_uid; ?>" />
									</td>
									<td width="25%"><label>Console Connection Type</label>
										<select name="remote_conn_type" id="remote_conn_type">
											<option value="S"<?php if($edit){if(strtolower($rowS['connection_type'])=="s"){echo " selected";}}else{echo " selected";} ?>>SSH</option>
											<option value="T"<?php if($edit){if(strtolower($rowS['connection_type'])=="t"){echo " selected";}} ?>>Telnet</option>
										</select>
									</td>
								</tr>
								<table width="100%" cellpadding="5" cellspacing="5">
								<tr>
									<td width="70%"><label>Remote Computer Notes</label>
										<textarea name="remote_notes" id="remote_notes"><?php if($edit){echo $rowS['remote_notes'];} ?></textarea>
									</td>
								</tr>
							</table>
							</table>
				</div>
				<div id="tab-130" class="tab">
					<!--Finance-->
					<table width="100%" cellpadding="5" cellspacing="5">
						<tr>
							<td width="33%"><label>Contract Monthly Amount</label>
								<input class="cash" type="text" name="contract_month_amt" id="contract_month_amt" value="<?php if($edit){echo $rowS['contract_month_amt'];} ?>" /></td>
							<td width="33%">&nbsp;</td>
							<td width="33%">&nbsp;</td>
						</tr>
					</table>
					<div class="line"></div>
					<table width="100%" cellpadding="5" cellspacing="5">
						<tr>
							<td><label>Current Monthly Amount</label>
								<input class="input_no_box cash" type="text" id="current_month_amt" value="0"  /></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td><label>Contract Days Left</label>
								<input class="input_no_box" type="text" id="contract_days_left" value="0"  /></td>
							<td><label>Contract Months Left</label>
								<input class="input_no_box" type="text" id="contract_months_left" value="0"  /></td>
							<td><label>Fiscal Year Months Left <span class="fiscal_year"></span></label>
								<input class="input_no_box" type="text" id="fiscal_months_left" value="0"  /></td>
						</tr>
						<tr>
							<td><label>Amount Left <span class="fiscal_year"></span></label>
								<input class="input_no_box cash" type="text" id="amount_left" value="0"  /></td>
							<td><label>Expected Total Backlog <span class="fiscal_year"></span></label>
								<input class="input_no_box cash" type="text" id="expected_backlog" value="0"  /></td>
							<td><label>Total Backlog <span class="fiscal_year"></span></label>
								<input class="input_no_box cash" type="text" id="total_backlog" value="0"  /></td>
						</tr>
					</table>

				</div>
			</div>
			<div class="line"></div>
			<table width="100%" cellpadding="5" cellspacing="5">
				<tr>
					<td><a name="Submit" class="button_jquery_save" onclick="submitcheck()"><?php if(!$edit){echo "Create New System";}else{ echo "Save System Changes";} ?></a></td>
					<?php if(isset($_SESSION['perms']['perm_del_system'])){ ?>
						<td><a name="Submit" class="button_jquery_save" onclick="submitcheck('del')">Delete System</a></td>
					<?php } ?>
					<td><a name="Goto" class="button_jquery_lookup" onClick="gotofacility()">Goto Facility</a></td>
				</tr>
			</table>
			<div style="display:none">
				<input name="engineer_pri_orig" id="engineer_pri_orig" type="text" value="<?php echo $rowPriSecEng['engineer_pri']; ?>" />
				<input name="engineer_sec_orig" id="engineer_sec_orig" type="text" value="<?php echo $rowPriSecEng['engineer_sec']; ?>" />
				<input name="edit_system" id="edit_system" type="hidden" value="<?php if($edit){echo "Y";}else{echo "N";} ?>" />
				<input name="unique_id" id="unique_id" type="text" value="<?php echo $unique_id; ?>" />
				<input name="edit_version" id="edit_version" type="text" value="<?php echo $version; ?>" />
				<input name="ver_unique_id" id="ver_unique_id" type="text" value="<?php echo $ver_unique_id; ?>" />
				<input name="future_new" id="future_new" type="checkbox" value="Y" />
				<input name="archived" id="archived" type="checkbox" value="Y" <?php if(strtolower($rowS['property']) == 'a'){ echo "checked";} ?>/>
				<input name="future_convert" id="future_convert" type="checkbox" value="Y" />
				<input name="pm_dates" id="pm_dates" type="text" value="<?php echo $rowS['pm_dates']; ?>" />
				<input name="del_system" id="del_system" type="checkbox" value="Y" />
				<?php if($debug){ ?>
				<input name="debug" id="debug" type="hidden" value="Y" />
				<?php } ?>
			</div>

			<br />
			<br />
		</form>
		
	</div>
</div>
<script type="text/javascript">
	cash();
	calc_financial();
</script>
<?php require_once($footer_include); ?>
