<?php
$debug = false;
if(isset($_POST['debug'])){
	$debug = true;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

d($_POST);

$send = true;
if($settings->disable_email == '1'){
	$send = false;	
}

if(!isset($_POST['unique_id'])){
	$log->logerr('Blank Unique Id',1016,true,basename(__FILE__),__LINE__);	
}else{
	$unique_id=$_POST['unique_id'];	
}

if(!isset($_POST['ver_unique_id'])){
	$log->logerr('Blank Ver Unique Id',1016,true,basename(__FILE__),__LINE__);	
}else{
	$ver_unique_id=$_POST['ver_unique_id'];	
}

$edit = false;
if(strtolower($_POST['edit_system']) == "y"){
	$edit = true;	
}
d($edit);

$version = 0;
if(isset($_POST['edit_version'])){
	$version = intval($_POST['edit_version']);
}
d($version);
if($version == 0){
	$log->logerr('Invalid System Version, contact support.',1074,true,basename(__FILE__),__LINE__);
}

$del_system = false;
if(isset($_POST['del_system'])){
	$_POST['archived'] = 'y';
	$del_system = true;
}

$future_new = false;
if(isset($_POST['future_new'])){
	$future_new = true;
	$ver_unique_id = md5(uniqid());
}
d($future_new);
d($ver_unique_id);

$future_convert = false;
if(isset($_POST['future_convert'])){
	$future_convert = true;
}
d($future_convert);

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/smarty/libs/Smarty.class.php');
$smarty = new Smarty();
$smarty->setTemplateDir($_SERVER['DOCUMENT_ROOT'].$settings->email_templates);
$smarty->setCompileDir($_SERVER['DOCUMENT_ROOT'].$settings->email_template_c);
$smarty->setCacheDir($_SERVER['DOCUMENT_ROOT'].$settings->email_cache);
$smarty->setConfigDir($_SERVER['DOCUMENT_ROOT'].$settings->email_configs);
$smarty->use_sub_dirs = true;
$smarty->force_compile = true;
//$smarty->debugging = true;
$smarty->caching = false;

$sql="SELECT sbc.ver_unique_id, sbc.created_by, sbc.archived_by
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
WHERE sbc.ver_unique_id = '".$ver_unique_id."';";
d($sql);
if(!$resultExistSystem = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowExistSystem = false;
$new_system = false;
if($resultExistSystem->num_rows > 0){
	$rowExistSystem = $resultExistSystem->fetch_assoc();
}else{
	$new_system = true;
}
d($rowExistSystem);
d($new_system);

if(strtolower($_POST['archived'])=="y"){
	$property = 'A';
	if($del_system){
		$property = 'D';
	}

	$sql="SELECT sac.uid, sac.system_ver_unique_id
	FROM systems_assigned_customer AS sac
	LEFT JOIN systems_base_cont AS sbc ON sbc.ver_unique_id = sac.system_ver_unique_id
	WHERE sbc.ver_unique_id = '".$ver_unique_id."';";
	d($sql);
	if(!$resultSystemUsers = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$system_users = array();
	while($rowSystemUsers = $resultSystemUsers->fetch_assoc()){
		array_push($system_users, $rowSystemUsers['uid']);	
	}
	d($system_users);
	
	$sql="SELECT sac.uid, sac.system_ver_unique_id 
	FROM systems_assigned_customer AS sac
	WHERE sac.uid IN ('".implode("', '",$system_users)."')
	GROUP BY sac.uid
	HAVING COUNT(sac.uid) = 1;";
	d($sql);
	if(!$resultSystemUsersUnique = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$system_users_unique = array();
	while($rowSystemUsersUnique = $resultSystemUsersUnique->fetch_assoc()){
		array_push($system_users_unique, $rowSystemUsersUnique['uid']);	
	}
	d($system_users_unique);
	
	$system_users_disable = array_intersect($system_users_unique, $system_users);
	d($system_users_disable);
	if($del_system){
		$sql = "UPDATE users SET active = 'N' WHERE uid IN ('" . implode("', '", $system_users_disable) . "');";
		d($sql);
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql, 1000, false, basename(__FILE__), __LINE__);
			$log->logerr('There was error running the query [' . $mysqli->error . ']', 1000, false, basename(__FILE__), __LINE__);
			$log->logerr('Error occurred, contact support.', 1000, true, basename(__FILE__), __LINE__);
		}

		$sql = "UPDATE customers_email_list AS cel
		SET cel.active = 'N'
		WHERE cel.system_ver_unique_id = '" . $_POST['ver_unique_id'] . "';";
		d($sql);
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql, 1000, false, basename(__FILE__), __LINE__);
			$log->logerr('There was error running the query [' . $mysqli->error . ']', 1000, false, basename(__FILE__), __LINE__);
			$log->logerr('Error occurred, contact support.', 1000, true, basename(__FILE__), __LINE__);
		}

		$sql = "DELETE FROM systems_assigned_customer WHERE system_ver_unique_id = '" . $ver_unique_id . "';";
		d($sql);
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql, 1000, false, basename(__FILE__), __LINE__);
			$log->logerr('There was error running the query [' . $mysqli->error . ']', 1000, false, basename(__FILE__), __LINE__);
			$log->logerr('Error occurred, contact support.', 1000, true, basename(__FILE__), __LINE__);
		}
	}

	system_data();

	
	$sql="SELECT sbc.facility_unique_id, sbc.property
	FROM systems_base_cont as sbc
	WHERE sbc.ver_unique_id = '".$ver_unique_id."';";
	d($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$row = $result->fetch_assoc();
	d($row);
	$facility_unique_id = $row['facility_unique_id'];
	d($facility_unique_id);
	
	$sql="SELECT f.unique_id, f.customer_unique_id, f.property
	FROM systems_base_cont AS sbc
	LEFT JOIN facilities AS f ON f.unique_id = sbc.facility_unique_id
	WHERE f.unique_id = '".$facility_unique_id."'
	AND f.facility_id NOT IN ('1','2','3')
	HAVING COUNT(f.facility_id) = 1;";
	d($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	d($result->num_rows);
	
	if($result->num_rows > 0){
		$row = $result->fetch_assoc();
		d($row);
		$customer_unique_id = $row['customer_unique_id'];
		d($customer_unique_id);
		
		$sql="SELECT c.customer_id, c.unique_id, c.property
		FROM facilities AS f
		LEFT JOIN customers AS c ON c.unique_id = f.customer_unique_id
		WHERE f.customer_unique_id = '".$customer_unique_id."' 
		AND c.customer_id NOT IN ('1', '2', '3')
		HAVING COUNT(f.unique_id) = 1;";
		d($sql);
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
		d($result->num_rows);
		
		if($result->num_rows > 0){
			$sql="UPDATE customers SET property = '".$property."' WHERE unique_id = '".$customer_unique_id."';";
			d($sql);
			if(!$result = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
				$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
			}	
		}
		
		$sql="UPDATE facilities SET property = '".$property."' WHERE unique_id = '".$facility_unique_id."';";
		d($sql);
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
	}
	if($del_system){
		$log->loginfo('Deleted: '.$_POST['system_id'].' - '.$unique_id.' - v'.$version,101,false,basename(__FILE__),__LINE__);
	}else{
		$log->loginfo('Archived: '.$_POST['system_id'].' - '.$unique_id.' - v'.$version,101,false,basename(__FILE__),__LINE__);
	}
}else{

	$property = 'C';
	if(strtolower($_POST['archived']) == 'y'){
		$property = 'A';	
	}elseif(strtolower($_POST['future']) == 'y'){
		$property = 'F';	
	}
	
	if($future_new){
		$sql="SELECT IFNULL(MAX(ver),0) AS max_ver
			  FROM systems_base_cont AS sbc
			  WHERE sbc.unique_id = '".$unique_id."';";	
		d($sql);
		if(!$resultMax = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
		$rowMax = $resultMax->fetch_assoc();
		d($rowMax);
		if(intval($rowMax['max_ver']) == 0){
			$log->logerr('System version returned 0, contact support.',1075,true,basename(__FILE__),__LINE__);
		}else{
			$version = intval($rowMax['max_ver']) + 1;
			$property = 'F';
		}
		$log->loginfo("Future New: ".$_POST['system_id'].' -  VerUID: '.$ver_unique_id,113,false,basename(__FILE__),__LINE__);
	}

	system_data();

	$sql="DELETE FROM systems_assigned_pri WHERE system_ver_unique_id='$ver_unique_id';";
	d($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}		
	$sql="INSERT INTO systems_assigned_pri (`system_ver_unique_id`,`uid`)VALUES('$ver_unique_id','".$_POST['engineer_pri']."');";
	d($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	
	
	
	$sql="DELETE FROM systems_assigned_sec WHERE system_ver_unique_id='$ver_unique_id';";
	d($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$sql="INSERT INTO systems_assigned_sec (`system_ver_unique_id`,`uid`)VALUES('$ver_unique_id','".$_POST['engineer_sec']."');";
	d($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	
	if($future_convert){
		$sql="SELECT sbc.ver_unique_id
		FROM systems_base_cont AS sbc
		WHERE sbc.property = 'c' AND sbc.unique_id = '".$unique_id."';";	
		d($sql);
		if(!$resultCurSys = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
		$rowCurSys = $resultCurSys->fetch_assoc();
		$current_active_sys = $rowCurSys['ver_unique_id'];
		
		$sql="UPDATE systems_base_cont 
		SET property = IF(ver_unique_id = '".$ver_unique_id."', 'C', IF(ver_unique_id = '".$current_active_sys."', 'A', property)) 
		WHERE unique_id = '".$unique_id."';";
		d($sql);
		if(!$resultCurSys = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
		$log->loginfo("Future Convert: ".$_POST['system_id'].' -  VerUID: '.$ver_unique_id,114,false,basename(__FILE__),__LINE__);	
	}

	$sql="SELECT ms.id, ms.abv, ms.serviced FROM misc_states AS ms WHERE ms.abv = '".$rowFacility['state']."';";
	d($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	if($result->num_rows > 0){
		$row = $result->fetch_assoc();
		if(strtolower($row['serviced']) == 'n'){
			$sql="INSERT INTO email_actions (script, args, uid) VALUES ('CHANGEME.php SENT-FROM:systems_do.php','".$rowFacility['state']."', '".$_SESSION['login']."') ;";
			if(!$result = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
				$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
			}
		}
	}
	
	if($edit){
		$log->loginfo("Edited: ".$_POST['system_id'].' -  VerUID: '.$ver_unique_id,102,false,basename(__FILE__),__LINE__);	
	}else{
		$log->loginfo("New: ".$_POST['system_id'].' - VerUID: '.$ver_unique_id,103,false,basename(__FILE__),__LINE__);
	}
	
}//else archived


function system_data(){
	global $mysqli, $log, $unique_id, $ver_unique_id, $version, $property, $rowExistSystem, $new_system;

	$sql="SELECT f.unique_id, state FROM facilities AS f WHERE f.facility_id = '".$_POST['facility_id']."';";
	d($sql);
	if(!$resultFacility = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$rowFacility = $resultFacility->fetch_assoc();

	if(strtolower($_POST['mobile']) == 'y'){
		$sql="SELECT t.unique_id FROM trailers AS t WHERE t.trailer_id = '".$_POST['trailer_id']."';";
		d($sql);
		if(!$resultTrailer = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
		$rowTrailer = $resultTrailer->fetch_assoc();
		$trailer_unique_id = $rowTrailer['unique_id'];
	}else{
		$trailer_unique_id = '';
	}

	$remote_diags = 'N';
	if(isset($_POST['remote_eth0_mac'])){
		$remote_diags = 'Y';
	}

//	$credit_hold_date = '';
//	if(strtolower($_POST['credit_hold']) == 'y'){
//		$credit_hold_date = date(storef, time());
//	}

	$contract_start_date = '';
	if($_POST['contract_start_date'] != ''){
		$contract_start_date = date(storef,strtotime($_POST['contract_start_date']));
	}

	$contract_end_date = '';
	if($_POST['contract_end_date'] != ''){
		$contract_end_date = date(storef,strtotime($_POST['contract_end_date']));
	}

	$acceptance_date = '';
	if($_POST['acceptance_date'] != ''){
		$acceptance_date = date(storef,strtotime($_POST['acceptance_date']));
	}

	$service_key_expire = '';
	if($_POST['service_key_expire'] != ''){
		$service_key_expire = date(storef,strtotime($_POST['service_key_expire']));
	}

	$last_pm = '';
	if($_POST['last_pm'] != ''){
		$last_pm = date(storef,strtotime($_POST['last_pm']));
	}

	$pm_start = '';
	if($_POST['pm_start'] != ''){
		$pm_start = date(storef,strtotime($_POST['pm_start']));
	}

	$warranty_start_date = '';
	if($_POST['warranty_start_date'] != ''){
		$warranty_start_date = date(storef,strtotime($_POST['warranty_start_date']));
	}

	$warranty_end_date = '';
	if($_POST['warranty_end_date'] != ''){
		$warranty_end_date = date(storef,strtotime($_POST['warranty_end_date']));
	}

	$date_installed = '';
	if($_POST['date_installed'] != ''){
		$date_installed = date(storef,strtotime($_POST['date_installed']));
	}

	$arrival_date = '';
	if($_POST['arrival_date'] != ''){
		$arrival_date = date(storef,strtotime($_POST['arrival_date']));
	}

	$values_base = array(
		'system_id' => $_POST['system_id'],
		'system_type' => $_POST['system_type'],
		'sw_ver' => $_POST['sw_ver'],
		'mobile' => $_POST['mobile'],
		'trailer_unique_id' => $trailer_unique_id,
		'system_serial' => $_POST['system_serial'],
		'service_key' => $_POST['service_key'],
		'service_key_expire' => $service_key_expire,
		'he_monitor' => $_POST['he_monitor'],
		'he_monitor_serial' => $_POST['he_monitor_serial'],
		'remote_access' => $remote_diags,
		'pm_freq' => $_POST['pm_freq'],
		'pm_start' => $pm_start,
		'pm_dates' => $_POST['pm_dates'],
		'last_pm' => $last_pm,
		'status' => $_POST['status'],
		'unique_id' => $unique_id
	);
	d($values_base);

	$values_base_cont = array(
		'ver' => $version,
		'property' => $property,
		'system_id' => $_POST['system_id'],
		'facility_unique_id' => $rowFacility['unique_id'],
		'nickname' => addslashes($_POST['system_nickname']),
		'contact_name' => addslashes($_POST['contact_name']),
		'contact_title' => addslashes($_POST['contact_title']),
		'contact_phone' => $_POST['contact_phone'],
		'contact_cell' => $_POST['contact_cell'],
		'contact_email' => $_POST['contact_email'],
		'location' => $_POST['location'],
		'government_contract' => $_POST['government_contract'],
		'tube_covered' => $_POST['tube_covered'],
		'log_book' => $_POST['log_book'],
		'contract_type' => $_POST['contract_type'],
		'contract_terms' => addslashes($_POST['contract_terms']),
		'contract_start_date' => $contract_start_date,
		'contract_end_date' => $contract_end_date,
		'contract_hours' => $_POST['contract_hours'],
		'acceptance_date' => $acceptance_date,
		'date_installed' => $date_installed,
		'pre_paid' => $_POST['pre_paid'],
		'labor_reg_rate' => $_POST['labor_reg_rate'],
		'labor_ot_rate' => $_POST['labor_ot_rate'],
		'travel_reg_rate' => $_POST['travel_reg_rate'],
		'travel_ot_rate' => $_POST['travel_ot_rate'],
		'warranty_start_date' => $warranty_start_date,
		'warranty_end_date' => $warranty_end_date,
		'contract_month_amt' => $_POST['contract_month_amt'],
		'arrival_date' => $arrival_date,
		'mr_lhe_list' => $_POST['mr_lhe_list'],
		'mr_lhe_press' => $_POST['mr_lhe_press'],
		'mr_lhe_contact' => $_POST['mr_lhe_contact'],
		'mr_lhe_email' => $_POST['mr_lhe_email'],
		'mr_lhe_phone' => $_POST['mr_lhe_phone'],
		'ver_unique_id' => $ver_unique_id,
		'unique_id' => $unique_id
	);

	if($rowExistSystem == false and strtolower($property) != 'a' and strtolower($property) != 'd'){
		$values_base_cont['created_by'] = $_SESSION['login'];
		$values_base_cont['created_date'] = date(storef,time());
		$values_base['created_by'] = $_SESSION['login'];
		$values_base['created_date'] = date(storef,time());
	}

	if($rowExistSystem != false and strtolower($property) != 'a' and strtolower($property) != 'd'){
		$values_base_cont['archived_by'] = '';
		$values_base_cont['archived_date'] = '';
	}

	if(strtolower($property) == 'a'){
		$values_base_cont['archived_by'] = $_SESSION['login'];
		$values_base_cont['archived_date'] = date(storef,time());
	}

	if(strtolower($property) == 'd'){
		$values_base_cont['deleted_by'] = $_SESSION['login'];
		$values_base_cont['deleted_date'] = date(storef,time());
	}

	d($values_base_cont);

	$sql="INSERT INTO systems_base (";
	foreach($values_base as $col=>$data){
		$sql.="`".$col."`, ";
	}
	$sql = rtrim($sql,', ').") VALUES ('". implode("', '", $values_base) . "') ON DUPLICATE KEY UPDATE ";
	foreach($values_base as $col=>$data){
		$sql.="`".$col."` = VALUES(`".$col."`), ";
	}
	$sql.= "edited_by = '".$_SESSION['login']."', edited_date = '".date(storef,time())."';";

	d($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}

	$sql="INSERT INTO systems_base_cont (";
	foreach($values_base_cont as $col=>$data){
		$sql.="`".$col."`, ";
	}
	$sql = rtrim($sql,', ').") VALUES ('". implode("', '", $values_base_cont) . "') ON DUPLICATE KEY UPDATE ";
	foreach($values_base_cont as $col=>$data){
		$sql.="`".$col."` = VALUES(`".$col."`), ";
	}
	$sql.= "edited_by = '".$_SESSION['login']."', edited_date = '".date(storef,time())."';";

	d($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}

	if(isset($_POST['remote_eth0_mac'])){
		if($_POST['remote_eth0_mac'] != ''){
			$sql ="INSERT INTO remote_assignments (system_unique_id, remote_serial, system_ip, system_port, system_uid, connection_type, edited_by, edited_date, notes) 
			VALUES ('".$unique_id."', '".$_POST['remote_eth0_mac']."', '".$_POST['remote_sys_ip']."', '".$_POST['remote_sys_port']."', '".$_POST['remote_sys_uid']."', 
			'".$_POST['remote_conn_type']."', '".$_SESSION['login']."', '".date(storef,time())."', '".addslashes($_POST['remote_notes'])."') 
			ON DUPLICATE KEY
			UPDATE remote_serial = VALUES(remote_serial), system_ip = VALUES(system_ip), system_port = VALUES(system_port), system_uid = VALUES(system_uid),
			connection_type = VALUES(connection_type), edited_by = VALUES(edited_by), edited_date = VALUES(edited_date), notes = VALUES(notes);";

			d($sql);
			if(!$result = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
				$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
			}
		}
	}

	if($trailer_unique_id != ''){
		$sql="UPDATE trailers SET assigned = 'Y', system_unique_id = '$unique_id' WHERE unique_id = '$trailer_unique_id';";
		d($sql);
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
	}
}

?>
<!DOCTYPE html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<?php require_once($js_include);?>

<script type="text/javascript">
function delayer(){
    <?php if(!$debug){ ?>window.location = "<?php echo $refering_uri; ?>"<?php } ?>
}
</script>
</head>
<body onLoad="setTimeout('delayer()', 3000)">
<?php require_once($header_include); ?>
<div id="OIReportContent">
	<h1>System <?php if($edit){
		if(strtolower($_POST['archived']) == 'y'){
			if($del_system){
				echo "Deleted";
			}else{
				echo "Archived";
			}
		}else{
			echo "Updated";
		}
	}else{
		echo "Created";
	} ?> Successfully</h1><br>
	System ID: <?php echo $_POST['system_id']; ?><br>
	System Nickname: <?php echo $_POST['system_nickname']; ?><br><br>
    <h1>Page will return to main page in 3 seconds</h1>
</div>
<?php require_once($footer_include); ?>

