<?php

$debug = false;
if(isset($_GET['debug'])){
	$debug = true;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

$word_limit	 = intval($settings->probrpt_word_limit);

$params = array('method'=>'');
if(isset($_POST['method'])){
	$params = $_POST;
}elseif(isset($_GET['method'])){
	$params = $_GET;
}
d($params);

$credit = false;
if(isset($params['credit'])){
	$credit = true;
}

$new = false;
$save = false;
$view = false;
$get = false;
$id = false;

switch($params['method']){
	case "save":
		if(!isset($params['system_ver_unique_id'])){
			$log->logerr('Save Error occurred, contact support.',1016,true,basename(__FILE__),__LINE__);
		}
		$save = true;
		save_note($mysqli, $log, $params);
		break;
	case "get":
		if(!isset($params['system_ver_unique_id']) and !isset($params['system_unique_id'])){
			$log->logerr('Get Error occurred, contact support.',1016,true,basename(__FILE__),__LINE__);
			//die("ERROR GET");
		}
		$get = true;
		if($credit){
			get_notes($mysqli, $log, $params['system_ver_unique_id'], $credit, $word_limit);
		}else{
			get_notes($mysqli, $log, $params['system_unique_id'], $credit, $word_limit);
		}

		break;
	case "view":
		$view = true;
		$id = $params['id'];
		$rowView = view_note($mysqli, $log, $params['id']);
		break;
	case "new":
		if(!isset($params['system_ver_unique_id'])){
			$log->logerr('NEW Error occurred, contact support.',1016,true,basename(__FILE__),__LINE__);
		}

		$new = true;
		break;
}

function save_note(&$mysqli, &$log, $params){
	$date = date(storef,time());
	$uid = $_SESSION['login'];
	$property = "G";
	if(isset($params['credit'])){
		$property = 'C';
	}
	$credit = false;
	if(isset($params['credit']) and $params['credit'] == true){
		$credit = true;
	}

	if(isset($_POST['note'])){
		$sql="SELECT unique_id FROM systems_base_cont AS sbc WHERE sbc.ver_unique_id = '".$params['system_ver_unique_id']."';";
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
		if($result->num_rows == 0){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Query returned 0 rows',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
		$row = $result->fetch_assoc();

		$sql="INSERT INTO systems_notes (note, uid, `date`, system_unique_id, system_ver_unique_id, property, credit_hold) 
			  VALUES ('".$mysqli->real_escape_string($params['note'])."', '$uid', '$date', '".$row['unique_id']."', '".$params['system_ver_unique_id']."', '$property', '".$params['credit_hold']."');";
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}

		if($credit){
			if($params['credit_hold'] == 'Y'){
				$sql="UPDATE systems_base_cont SET credit_hold = 'Y', credit_hold_date = '$date' WHERE ver_unique_id = '".$params['system_ver_unique_id']."';";
			}else{
				$sql="UPDATE systems_base_cont SET credit_hold = 'N', credit_hold_date = '$date' WHERE ver_unique_id = '".$params['system_ver_unique_id']."';";
			}
			$log->loginfo($sql,1000,false,basename(__FILE__),__LINE__);
			if(!$result = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
				$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
			}
		}
		exit('1');
	}else{
		exit('0');
	}

}

function get_notes(&$mysqli, &$log, $unique_id, $credit, $word_limit){
	if($credit){
		$credit = "C";
		$where = "WHERE sn.system_ver_unique_id = '".$unique_id."'";
	}else{
		$credit = "G";
		$where = "WHERE sn.system_unique_id = '".$unique_id."'";
	}
	$sql = "SELECT sn.note, sn.`date`, u.name, sn.credit_hold, sn.id
	FROM systems_notes AS sn
	LEFT JOIN users AS u ON u.uid = sn.uid
	$where
	AND sn.property = '".$credit."'
	ORDER BY sn.`date` DESC;";
	if(!$resultNotes = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	if($resultNotes->num_rows > 0){
		$json = array('data'=>array());
		while($rowNotes = $resultNotes->fetch_assoc()){
			array_push($json['data'],array("<a class='iframeNoteView' href='/report/common/systems_notes.php?method=view&id=".$rowNotes['id']."'>".date(storef, strtotime($rowNotes['date']))."</a>",
				limit_words($rowNotes['note'],$word_limit),
				$rowNotes['name'],
				strtolower($rowNotes['credit_hold']) == "y"? "Yes": "No"));
		}
		d($json);
		$json = json_encode($json);
		d($json);
		die($json);
	}else{
		die("{\"data\":[]}");
	}
}

function view_note(&$mysqli, &$log, $id){
	$sql="SELECT  sn.`date`, sn.note, u.name, sn.credit_hold, sn.property
	FROM systems_notes AS sn
	LEFT JOIN users AS u ON u.uid = sn.uid
	WHERE sn.id=".$mysqli->real_escape_string($id).";";
	if(!$resultView = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	return $resultView->fetch_assoc();

}

function limit_words($string, $word_limit){
	$words = explode(" ",$string);
	if(count($words) > $word_limit){
		return implode(" ",array_splice($words,0,$word_limit))."...(more)";
	}else{
		return implode(" ",array_splice($words,0,$word_limit));
	}
}

?>

<!doctype html>
<html>
<head>
<?php require_once($head_include);?>
<?php require_once($css_include);?>
<link rel="stylesheet" type="text/css" href="/resources/css/portal.css">
<link rel="stylesheet" type="text/css" href="/resources/css/form_request.css">
<style type="text/css">
#newTable {
	width: 100%;
}
.rowLabel {
	padding-top: 5px;
	text-align: center;
	width: 20%
}
.rowData {
	text-indent: 5px;
	padding-top: 5px;
	padding-right: 20px;
	width: 30%
}
textarea {
	width: 100%;
	height: 200px;
	margin-bottom: 15px;
}
.bottomBtn {
	margin-left: 8px;
	margin-top: 30px;
}
</style>
<?php require_once($js_include);?>
<script type="text/javascript">
	$(document).ready(function(){
		$(".button_jquery_save").button({
			icons: {
				primary: "ui-icon-disk"
			}
		});

		$("select").chosen({
			no_results_text: "Oops, nothing found!",
			disable_search_threshold: 10,
			placeholder_text_single: '  ',
			search_contains: true
		});
	});
	
	function submitcheck(){
		if($("#note").val() == ""){
			return;
		}
		var ajax_data = {'method': 'save',
					'note': $("#note").val(),
					'system_ver_unique_id': '<?php echo $params['system_ver_unique_id']; ?>',
					'credit_hold': $("#credit_hold").val()};
		<?php if($credit){?>
			ajax_data['credit'] = true;
		<?php } ?>
		console.log(ajax_data);
		$.ajax({
			type: 'POST',
			url: 'systems_notes.php',
			data: ajax_data,
			success:function(data){
				// successful request; do something with the data
				if(data.trim() != '1'){
					alert('Failed to save the note. Contact support. Code 1');
				}
			},
			error: function(){
				alert('Failed to save the note. Contact support. Code 2');
			}
		}).always(function() {
			parent.jQuery.fancybox.close();
		});
	}
</script>
</head>
<body>
	<?php if($view){ ?>
	<table width="100%">
		<tr>
			<td>Date: <?php echo $rowView['date']; ?></td>
			<?php if(strtolower($rowView['property'])=='c'){ ?><td>On Hold: <?php echo strtolower($rowView['credit_hold']) == "y"? "Yes":"No"; ?></td><?php } ?>
			<td>User: <?php echo $rowView['name']; ?></td>
		</tr>
	</table>
	<div>
		<pre style="color:#000500"><?php echo $rowView['note']; ?></pre>
	</div>


	<?php }elseif($new){ ?>


	<table id="newTable">
		<tr>
		  <td class="rowLabel">Entry</td>
		</tr>
		<tr>
		  <td class="rowData"><textarea id="note" name="note" maxlength="6000"></textarea></td>
		</tr>
			<tr <?php if(!$credit){echo "hidden";} ?>>
				<td align="center">
					<label for="credit_hold">Place System On Credit Hold&emsp;</label>
					<select id="credit_hold" name="credit_hold" style="width: 50%;">
						<option value="Y" <?php if(isset($params['credit_status']) and $params['credit_status'] == 'Y'){echo "selected";} ?>>Yes</option>
						<option value="N" <?php if(isset($params['credit_status']) and $params['credit_status'] == 'N'){echo "selected";} ?>>No</option>
					</select>
				</td>
			</tr>
	</table>
	<div class="bottomBtn"><a class="button_jquery_save" id="save_btn" onClick="submitcheck()">Save</a></div>

	<?php }elseif($get){ ?>
	
	<?php } ?>
</body>
</html>