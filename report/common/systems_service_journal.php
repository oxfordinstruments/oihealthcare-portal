<?php
$debug = false;
if(isset($_GET['debug'])){
	$debug = true;
}

require_once($_SERVER['DOCUMENT_ROOT'] . '/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'] . '/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'] . '/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

$probrpt_word_limt = intval($settings->probrpt_word_limit);

if(isset($_POST['save'])){
	$date = date(storef, time());
	$uid = $_SESSION['login'];
	if(isset($_POST['cur_system_status'])){
		$status = $_POST['cur_system_status'];
	}else{
		$status = 0;
	}
	if(isset($_POST['note']) and $_POST['note'] != ''){
		$sql = "INSERT INTO systems_service_journal (note, `date`, uid, system_status, request_report_unique_id) VALUES ('" . $mysqli->real_escape_string($_POST['note']) . "', '$date', '$uid', '$status', '" . $_POST['unique_id'] . "');";
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql, 1000, false, basename(__FILE__), __LINE__);
			$log->logerr('There was error running the query [' . $mysqli->error . ']', 1000, false, basename(__FILE__), __LINE__);
			$log->logerr('Error occurred, contact support.', 1000, true, basename(__FILE__), __LINE__);
		}
		if($status != 0){
			$sql = "UPDATE systems_base SET `status`='" . $_POST['cur_system_status'] . "' WHERE `unique_id`='" . $_POST['system_base_unique_id'] . "' LIMIT 1;";
			if(!$result = $mysqli->query($sql)){
				$log->logerr($sql, 1000, false, basename(__FILE__), __LINE__);
				$log->logerr('There was error running the query [' . $mysqli->error . ']', 1000, false, basename(__FILE__), __LINE__);
				$log->logerr('Error occurred, contact support.', 1000, true, basename(__FILE__), __LINE__);
			}
		}

		die('1');
	}else{
		die('0');
	}
}

$unique_id = false;
if(isset($_GET['unique_id'])){
	$unique_id = $_GET['unique_id'];
}elseif(isset($_POST['unique_id'])){
	$unique_id = $_POST['unique_id'];
}

$id = false;
$view = false;
if(isset($_GET['id'])){
	$view = true;
	$id = $_GET['id'];
}

$new = false;
if(isset($_GET['new'])){
	$new = true;
}

$get = false;
if(isset($_POST['get']) or isset($_GET['get'])){
	$get = true;
}

$sql="SELECT * FROM systems_status;";
if(!$resultStatus = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

if($view){
	$sql = "SELECT  ssj.`date`, ssj.note, u.name, IF(ssj.system_status IS NULL OR ssj.system_status = '', 'N/A', ss.status) AS system_status
	FROM systems_service_journal AS ssj
	LEFT JOIN systems_status AS ss ON ss.id = ssj.system_status
	LEFT JOIN users AS u ON u.uid = ssj.uid
	WHERE ssj.id=" . $mysqli->real_escape_string($id) . ";";
	if(!$resultView = $mysqli->query($sql)){
		$log->logerr($sql, 1000, false, basename(__FILE__), __LINE__);
		$log->logerr('There was error running the query [' . $mysqli->error . ']', 1000, false, basename(__FILE__), __LINE__);
		$log->logerr('Error occurred, contact support.', 1000, true, basename(__FILE__), __LINE__);
	}
	$rowView = $resultView->fetch_assoc();
}

if($new){
	$sql = "SELECT sb.status AS system_status, sb.unique_id AS system_base_unique_id
	FROM systems_requests AS sr
	LEFT JOIN systems_base AS sb ON sb.unique_id = sr.system_unique_id
	WHERE sr.unique_id='" . $unique_id . "';";
	if(!$resultView = $mysqli->query($sql)){
		$log->logerr($sql, 1000, false, basename(__FILE__), __LINE__);
		$log->logerr('There was error running the query [' . $mysqli->error . ']', 1000, false, basename(__FILE__), __LINE__);
		$log->logerr('Error occurred, contact support.', 1000, true, basename(__FILE__), __LINE__);
	}
	$rowNew = $resultView->fetch_assoc();
}

if($get){
	$sql = "SELECT ssj.note, ssj.`date`, u.name, ssj.id, IF(ssj.system_status IS NULL OR ssj.system_status = '', 'N/A', ss.abv) AS status_abv
	FROM systems_service_journal AS ssj
	LEFT JOIN systems_reports AS sr ON sr.unique_id = ssj.request_report_unique_id
	LEFT JOIN users AS u ON u.uid = ssj.uid
	LEFT JOIN systems_status AS ss ON ss.id = ssj.system_status
	WHERE ssj.request_report_unique_id = '" . $unique_id . "'
	ORDER BY ssj.`date` DESC;";
	if(!$resultNotes = $mysqli->query($sql)){
		$log->logerr($sql, 1000, false, basename(__FILE__), __LINE__);
		$log->logerr('There was error running the query [' . $mysqli->error . ']', 1000, false, basename(__FILE__), __LINE__);
		$log->logerr('Error occurred, contact support.', 1000, true, basename(__FILE__), __LINE__);
	}
	if($resultNotes->num_rows > 0){
		$json = array('data' => array());
		while($rowNotes = $resultNotes->fetch_assoc()){
			d($rowNotes);
			array_push($json['data'], array("<a class='iframeNoteView' href='/report/common/systems_service_journal.php?id=" . $rowNotes['id'] . "'>" . date(phpdispfdt, strtotime($rowNotes['date'])) . "</a>",
				limit_words($rowNotes['note'], $probrpt_word_limt),
				$rowNotes['status_abv'],
				$rowNotes['name']));
		}
		d($json);
		$json = json_encode($json);
		d($json);
		die($json);
	}else{
		die("{\"data\":[]}");
	}
}

function limit_words($string, $word_limit)
{
	$words = explode(" ", $string);
	if(count($words) > $word_limit){
		return implode(" ", array_splice($words, 0, $word_limit)) . "...(more)";
	}else{
		return implode(" ", array_splice($words, 0, $word_limit));
	}
}

?>

<!doctype html>
<html>
<head>
	<?php require_once($head_include); ?>
	<script type="text/javascript" language="javascript" src="/resources/js/jquery/jquery.min.js"></script>
	<script type="text/javascript" language="javascript" src="/resources/js/jquery-ui/jquery-ui.min.js"></script>
	<script type="text/javascript" language="javascript" src="/resources/js/chosen/chosen.jquery.js"></script>

	<?php require_once($css_include); ?>
	<link rel="stylesheet" type="text/css" href="/resources/css/portal.css">
	<link rel="stylesheet" type="text/css" href="/resources/css/form_request.css">
	<link href="/resources/js/chosen/chosen.css" rel="stylesheet" type="text/css" >

	<style type="text/css">
		#newTable {
			width: 100%;
		}

		.rowLabel {
			padding-top: 5px;
			text-align: center;
		}

		.rowData {
			text-indent: 5px;
			padding-top: 5px;
			padding-right: 20px;
		}

		textarea {
			width: 100%;
			height: 200px
		}

		.bottomBtn {
			margin-left: 8px;
			margin-top: 10px;
		}
	</style>
	<?php require_once($js_include); ?>
	<script type="text/javascript">
		$(document).ready(function () {
			$(".button_jquery_save").button({
				icons: {
					primary: "ui-icon-disk"
				}
			});

			$(".chooser").chosen({
				no_results_text: "Oops, nothing found!",
				disable_search_threshold: 10,
				placeholder_text_single: '  '
			});
		});

		function submitcheck() {
			$.ajax({
				type: 'POST',
				url: 'systems_service_journal.php',
				data: {
					save: 'true',
					note: $("#note").val(),
					unique_id: '<?php echo $unique_id; ?>',
					cur_system_status: $("#cur_system_status").val(),
					system_base_unique_id: '<?php echo $rowNew['system_base_unique_id']; ?>'
				},
				success: function (data) {
					// successful request; do something with the data
					if (data.trim() != '1') {
						alert('Failed to save the note. Contact support. Code 1');
					}
				},
				error: function () {
					alert('Failed to save the note. Contact support. Code 2');
				}
			}).always(function (data) {
				parent.jQuery.fancybox.close();
			});
		}
	</script>
</head>
<body>
<div style="width: 99%; margin-left: auto; margin-right: auto;">
	<?php if($view){ ?>
		<table width="100%">
			<tr>
				<td>Date: <?php echo date(phpdispfdt, strtotime($rowView['date'])) ?></td>
				<td>User: <?php echo $rowView['name']; ?></td>
				<td>System Status: <?php echo $rowView['system_status']; ?></td>
			</tr>
		</table>
		<div>
			<pre style="color:#000500"><?php echo $rowView['note']; ?></pre>
		</div>
	<?php }elseif($new){ ?>
		<table id="newTable">
			<tr>
				<td class="rowLabel" width="60%">Entry</td>
				<td class="rowLabel">&nbsp;</td>
			</tr>
			<tr>
				<td class="rowData"><textarea id="note" name="note" maxlength="6000"></textarea></td>
				<td class="rowData" style="vertical-align: top; text-align: center;">
					<label for="cur_system_status">Current System Status:</label>
					<div style="text-align: left; margin-bottom: 50px;">
						<select name="cur_system_status" id="cur_system_status" class="chooser">
							<option value=""></option>
							<?php
							while($rowStatus = $resultStatus->fetch_assoc())
							{
								echo "<option value='" . $rowStatus['id'] . "' ";
								if($rowNew['system_status'] == $rowStatus['id']){echo "selected ";}
								echo ">" . $rowStatus['status'] . "</option>\n";
							}
							?>
						</select>
					</div>
					<a class="button_jquery_save" id="save_btn" onClick="submitcheck()">Save</a>
				</td>
			</tr>
		</table>
		<div style="padding-bottom: 25px;"></div>
	<?php }elseif($get){ ?>

	<?php } ?>
</div>
</body>
</html>