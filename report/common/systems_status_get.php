<?php

$debug = true;
if(isset($_GET['debug'])){
	$debug = true;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

$unique_id = false;
if(isset($_POST['unique_id'])){
	$unique_id = $_POST['unique_id'];
}elseif(isset($_GET['unique_id'])){
	$unique_id = $_GET['unique_id'];
}else{
	die('0');
}

$sql="SELECT `status` FROM systems_base WHERE unique_id = '$unique_id';";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql, 1000, false, basename(__FILE__), __LINE__);
	$log->logerr('There was error running the query [' . $mysqli->error . ']', 1000, false, basename(__FILE__), __LINE__);
	$log->logerr('Error occurred, contact support.', 1000, true, basename(__FILE__), __LINE__);
}
if($result->num_rows == 0){
	die('0');
}
$row = $result->fetch_assoc();
die($row['status']);