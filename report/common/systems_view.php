<?php
//Update Completed 11/26/14
$debug = false;
if(isset($_GET['debug'])){
	$debug = true;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/moment_php/Moment.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/resources/moment_php/MomentException.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/resources/moment_php/MomentHelper.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/resources/moment_php/MomentLocale.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/resources/moment_php/MomentFromVo.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/php_utils.php');
$php_utils =  new phpUtils();

if(isset($_GET['ver_unique_id'])) {
	$ver_unique_id=$_GET['ver_unique_id'];
}else{
	$log->logerr('viewSystem.php',1044);	
	header("location:/error.php?n=1044&p=systems_view.php");
}

$myusername = $_SESSION["login"];

if(strtolower($_SESSION['userdata']['grp_employee']) == "y"){
	$employee = true;
}else{
	$employee = false;
}

$probrpt_word_limt	 = intval($settings->probrpt_word_limit);

$sql="SELECT sb.*, sbc.*, f.name AS facility_name, f.facility_id, f.address, f.city, f.state, f.zip, ss.flag,
st.name AS equipment_type, mc.`type` AS contract_type, GROUP_CONCAT(u.uid SEPARATOR '  |  ') AS logins, c.customer_id, c.name AS customer_name, f.customer_unique_id,
tr.trailer_id, tr.mfg AS trailer_mfg, tr.vin AS trailer_vin
FROM systems_base AS sb
LEFT JOIN systems_base_cont AS sbc ON sb.unique_id = sbc.unique_id
LEFT JOIN misc_contracts AS mc ON mc.id = sbc.contract_type
LEFT JOIN systems_types AS st ON st.id = sb.system_type
LEFT JOIN facilities AS f ON f.unique_id = sbc.facility_unique_id
LEFT JOIN users AS u ON u.customer_unique_id = f.customer_unique_id
LEFT JOIN customers AS c ON c.unique_id = f.customer_unique_id
LEFT JOIN trailers AS tr ON tr.unique_id = sb.trailer_unique_id
LEFT JOIN systems_status AS ss ON ss.id = sb.`status`
WHERE sbc.ver_unique_id = '$ver_unique_id'
LIMIT 1;";
if(!$resultSystems=$mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowSystems = $resultSystems->fetch_assoc();

$customer_logins = false;
if($rowSystems['logins'] != ''){
	$customer_logins = $rowSystems['logins'];	
}

$unique_id = $rowSystems['unique_id'];

$sql="SELECT a.uid,u.name,u.email,u.cell,u.carrier, MAX(IF(pid.perm = 'perm_rcv_sms', 'Y', 'N')) AS perm_rcv_sms
FROM systems_assigned_pri AS a
LEFT JOIN users AS u ON a.uid = u.uid
LEFT JOIN users_perms AS up ON up.uid = u.uid
LEFT JOIN users_perm_id AS pid ON pid.id = up.pid
WHERE a.system_ver_unique_id='".$rowSystems['ver_unique_id']."';";
if(!$resultEngPri = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowEngPri = $resultEngPri->fetch_assoc();

$sql="SELECT a.uid,u.name,u.email,u.cell,u.carrier, MAX(IF(pid.perm = 'perm_rcv_sms', 'Y', 'N')) AS perm_rcv_sms
FROM systems_assigned_sec AS a
LEFT JOIN users AS u ON a.uid = u.uid
LEFT JOIN users_perms AS up ON up.uid = u.uid
LEFT JOIN users_perm_id AS pid ON pid.id = up.pid
WHERE a.system_ver_unique_id='".$rowSystems['ver_unique_id']."';";
if(!$resultEngSec = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowEngSec = $resultEngSec->fetch_assoc();

$sql="SELECT u.uid, u.name
FROM users AS u
LEFT JOIN users_roles AS ur ON ur.uid = u.uid
LEFT JOIN users_role_id AS rid ON rid.id = ur.rid
WHERE u.active = 'y' AND (rid.role = 'role_engineer' OR rid.role = 'role_contractor' OR u.uid = 'unassigned') ORDER BY u.name ASC;";
if(!$resultEngineers = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$arr_engineers = array();
while($rowEngineer = $resultEngineers->fetch_assoc()){
	$arr_engineers[$rowEngineer['uid']] = $rowEngineer['name'];	
}

$sql="SELECT id,`status` FROM systems_status;";
if(!$resultSysStatus = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$arr_status = array();
while($rowSysStatus = $resultSysStatus->fetch_assoc()){
	$arr_status[$rowSysStatus['id']] = $rowSysStatus['status'];	
}

$warranty_warn = false;
$contract_warn = false;
$now = new Moment\Moment('now', null, true);
try{
	if($rowSystems['warranty_end_date'] != ''){
		$warranty_end_date = new \Moment\Moment($rowSystems['warranty_end_date'], null, true);
		if($warranty_end_date->isBefore($now)){
			$warranty_warn = true;
		}
	}
}catch(\Moment\MomentException $momentException){
	d($momentException);
}
try{
	if($rowSystems['contract_end_date'] != ''){
		$contract_end_date = new Moment\Moment($rowSystems['contract_end_date'], null, true);
		if($contract_end_date->isBefore($now)){
			$contract_warn = true;
		}
	}
}catch(\Moment\MomentException $momentException){
	d($momentException);
}

function limit_words($string, $word_limit){
	$words = explode(" ",$string);
	if(count($words) > $word_limit){
		return implode(" ",array_splice($words,0,$word_limit))."...(more)";
	}else{
		return implode(" ",array_splice($words,0,$word_limit));
	}
}
?>
<!DOCTYPE html>
<html>
<head>
<?php require_once($head_include);?>
<?php require_once($css_include);?>
<link href="/resources/css/viewSiteNav.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" href="/resources/css/tables.css">
<link rel="stylesheet" type="text/css" href="/resources/css/meter.css">

<style>
#system_health {
    width: 80%;
    text-align: center;
    overflow: hidden;
    position: relative;
    vertical-align: middle;
    line-height: 35px;
}

#system_health_label {
    float: left;
    width: 100%;
    height: 100%;
    position: absolute;
    vertical-align: middle;
}

.status_bar {
	width: 80%;
	text-align: center;
	overflow: hidden;
	position: relative;
	vertical-align: middle;
	line-height: 35px;
}
</style>
<?php require_once($js_include);?>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDvmB-Iiut3rCFwWrvTKooa6H88GAw4rWs&sensor=false"></script>
<script type="text/javascript">
	$(function() {
		$(".meter > span").each(function() {
			$(this)
				.data("origWidth", $(this).width())
				.width(0)
				.animate({
					width: $(this).data("origWidth")
				}, 1200);
		});
	});
	$(function() {
		$('#system_health').progressbar();
		<?php 
		switch($rowSystems['flag']){
			case 'green':
				echo "$('#system_health').css({color: '#ffffff'});";
				echo "$('#system_health').css({background: '#00C832'});";
				break;
			case 'yellow':
				echo "$('#system_health').css({color: '#000000'});";
				echo "$('#system_health').css({background: '#ECEC00'});";
				break;
			case 'red':
				echo "$('#system_health').css({color: '#ffffff'});";
				echo "$('#system_health').css({background: '#FF0000'});";
				break;
		}
		?>
		$('#system_health_text').html('<?php echo $arr_status[$rowSystems['status']]; ?>');
	 });

	$(function() {
		$('.status_bar').progressbar();
		$('.status_bar').css({color: '#ffffff'});
		$('.status_bar').css({background: '#FF0000'});
	});

	
</script>
<script type="text/javascript">
$(document).ready(function() {

/////////////////////////////////////////////////////////////////////////////////////		
		<?php
		if(strtolower($_SESSION['prefs']['pref_show_help']) != 'y' or $_SESSION['mobile_device'] == true){
		?>
		$('#tiplayer').hide()
		<?php
			}
		?> 		
});
</script>
<script type="text/javascript">
  var geocoder;
  var map;
  var infowindow = new google.maps.InfoWindow();
  var daddr;

  function initialize() {
    geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(-34.397, 150.644);
    var myOptions = {
      zoom: 14,
      center: latlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
	codeAddress();
	
    document.getElementById("getdir").setAttribute("href","http://maps.google.com/maps?saddr=&daddr="+document.getElementById("address").value.replace(/,/g, "+").replace(/ /g, "+")+"");
  }

  function codeAddress() {
    var address = document.getElementById("address").value;
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        map.setCenter(results[0].geometry.location);
        var marker = new google.maps.Marker({
            map: map, 
            position: results[0].geometry.location
        });
		daddr = document.getElementById("address").value.replace(/,/g, "+").replace(/ /g, "+");
		var systeminfo = "<?php echo $rowSystems['nickname'] . "<br />" . $rowSystems['address'] . "<br />" . $rowSystems['city'] . "<br />" . $rowSystems['state'] . "<br /><br />Contact: ". $rowSystems['contact'] . "<br />Phone: " . $rowSystems['phone'] . "<br />Cell: " .$rowSystems['cell']; ?> <br> <a href=\"http://maps.google.com/maps?saddr=&daddr="+daddr+"\" target ='_blank'>Directions<\/a>";
		 // Adding a click event to the marker
        google.maps.event.addListener(marker, 'click', function() {
            infowindow.setContent(systeminfo);
            infowindow.open(map, this);
        }); 
      } else {
        alert("Google Maps cannot find the address for the facility\nVerify the facility address it is possibly incorrect.\nGeocode status: " + status);
      }
    });
  }
  
</script>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		var openTable = $('#openReportsTable').dataTable({
			"bJQueryUI": true,
			"bStateSave": true,
			"aLengthMenu": [[5, 10, 25, 50], [5, 10, 25, 50]],
			"iDisplayLength": 5,
			"iCookieDuration": 60*60*24*365, // 1 year
			"sPaginationType": "full_numbers",
			"aaSorting": [ [0,'desc'] ],
			 "fnInitComplete": function () {
           		 this.$('tr').click( function () {
               		 var href = $(this).find("a").attr("href");
					 if(href) {
						window.location = href;
					 }
           		 });
       		 }
		});
		var pastTable = $('#pastReportsTable').dataTable({
			"bJQueryUI": true,
			"bStateSave": true,
			"aLengthMenu": [[5, 10, 25, 50], [5, 10, 25, 50]],
			"iDisplayLength": 5,
			"iCookieDuration": 60*60*24*365, // 1 year
			"sPaginationType": "full_numbers",
			"aaSorting": [ [0,'desc'] ],
			"fnInitComplete": function () {
           		 this.$('tr').click( function () {
               		 var href = $(this).find("a").attr("href");
					 if(href) {
						window.location = href;
					 }
           		 });
       		 }
		});
		var reqestTable = $('#requestsTable').dataTable({
			"bJQueryUI": true,
			"bStateSave": true,
			"aLengthMenu": [[5, 10, 25, 50], [5, 10, 25, 50]],
			"iDisplayLength": 5,
			"iCookieDuration": 60*60*24*365, // 1 year
			"sPaginationType": "full_numbers",
			"aaSorting": [ [0,'desc'] ],
			"fnInitComplete": function () {
           		 this.$('tr').click( function () {
               		 var href = $(this).find("a").attr("href");
					 if(href) {
						window.location = href;
					 }
           		 });
       		 }
		});
		
		var eveUpdatesTable = $('#eveUpdatsTable').dataTable({
			"bJQueryUI": true,
			"bStateSave": true,
			"aLengthMenu": [[5, 10, 25, 50], [5, 10, 25, 50]],
			"iDisplayLength": 5,
			"iCookieDuration": 60*60*24*365, // 1 year
			"sPaginationType": "full_numbers",
			"aaSorting": [ [0,'desc'] ]//,
//			"fnInitComplete": function () {
//           		 this.$('tr').click( function () {
//               		 var href = $(this).find("a").attr("href");
//					 if(href) {
//						window.location = href;
//					 }
//           		 });
//       		 }
		});

		var notesTableDT = $('#notesTable')
			.on('xhr.dt', function ( e, settings, json, xhr ) {
				//console.log( json );
				if(json['data'].length == 0){
					$("#notesDiv").hide();
				}else{
					$("#notesDiv").show();
				}
			})
			.dataTable({
				"bJQueryUI": true,
				"searching": false,
				"lengthChange": false,
				"sPaginationType": "full_numbers",
				"aaSorting": [],
				"autoWidth": false,
				"ajax": {
					'type': 'POST',
					'url': 'systems_notes.php',
					'data': {
						method: 'get',
						system_unique_id: '<?php echo $unique_id; ?>'
					}
				}
			});
		
//		$(function() {
//		  $(".sendUpdate").click(function() {
//			var status = $("textarea#status").val();
//			if (status == "") {
//			  alert("Cannot send an empty system status update");
//			  $("textarea#status").focus();
//			  return false;
//			}
//			var dataString = 'status='+ status + '&uid=' + $('input#update_uid').val() + '&system_unique_id=' + $('input#system_unique_id').val();
//			$('textarea#status').prop('disabled', true);
//			$('input#submit').prop('disabled', true);
//			$('input#submit').css('cursor', 'default');
//			$.ajax({
//			  type: "POST",
//			  url: "report_update_send.php",
//			  data: dataString,
//			  success: function(data) {
//				<?php //if(!$_SESSION['mobile_device']){ ?>
//					$.prompt("<h3>System Status Update has been sent</h3>",{
//						title: "System Status Update"
//					});
//				<?php //}else{ ?>
//					alert("System Status Update has been sent.");
//				<?php //} ?>
//				$('textarea#status').prop('disabled', false);
//				$('textarea#status').val('');
//				$('input#submit').prop('disabled', false);
//				$('input#submit').css('cursor', 'default');
//			  }
//			});
//			return false;
//		  });
//		});
		
		
		$(".button_jquery_print").button({
			icons: {
				primary: "ui-icon-print"
			}
		});
		
		$(".button_jquery_new").button({
			icons: {
				primary: "ui-icon-document"
			}
		});
		
		$(".button_jquery_edit").button({
			icons: {
				primary: "ui-icon-pencil"
			}
		});
		
		$(".button_jquery_send").button({
			icons: {
				primary: "ui-icon-signal-diag"
			}
		});
		
		$(".button_jquery_history").button({
			icons: {
				primary: "ui-icon-wrench"
			}
		});
		
		/////////////////////////////////////////////////////////////////////////////////////		
			
});

	function iframeNoteClosed(){

	}
</script>
</head>
<body onload="initialize()">
<?php require_once($header_include); ?>
<div id="OIReportContent">
	<div id="viewSystemTopBtns1">
		<?php if($employee){ ?>
		<div class="viewSystemBtn"><a class="button_jquery_print" href="/report/common/reports/face_sheets.php?ver_unique_id=<?php echo $ver_unique_id; ?>">Print Face Sheet</a></div>
		<div class="viewSystemBtn"><a class="button_jquery_history" href="/report/common/reports/system_service_history.php?unique_id=<?php echo $unique_id; ?>">Service History Report</a></div>
		<?php if(strtolower($_SESSION['perms']['perm_service_request']) == "y"){ ?>
<!--		<div class="viewSystemBtn"><a class="button_jquery_new" href="--><?php //echo "/report/common/request_new_edit.php?user_id=".$myusername."&unique_id=".$unique_id."&ver_unique_id=".$rowSystems['ver_unique_id']."&version=".$rowSystems['ver']; ?><!--">New Service Request</a></div>-->
		<?php } ?>
		<?php } ?>
	</div>
	<div id="viewSystemTopBtns2">
		<?php if($employee){ ?>
		<?php if(strtolower($_SESSION['perms']['perm_edit_systems']) == "y"){ ?>
<!--		<div class='viewSystemBtn'><a class="button_jquery_edit" href='/report/common/systems.php?e&ver_unique_id=--><?php //echo $ver_unique_id; ?><!--'>Edit System</a></div>-->
		<?php } ?>
		<?php if(strtolower($_SESSION['perms']['perm_edit_facilities']) == "y"){ ?>
<!--		<div class='viewSystemBtn'><a class="button_jquery_edit" href='/report/common/facilities.php?unique_id=--><?php //echo $rowSystems['facility_unique_id']; ?><!--&e'>Edit Facility</a></div>-->
		<?php } ?>
		<?php if(strtolower($_SESSION['perms']['perm_edit_customers']) == "y"){ ?>
<!--		<div class='viewSystemBtn'><a class="button_jquery_edit" href='/report/common/customers.php?unique_id=--><?php //echo $rowSystems['customer_unique_id']; ?><!--&e'>Edit Customer</a></div>-->
		<?php } ?>
		<?php } ?>
	</div>
	<?php if(strtolower($rowSystems['credit_hold']) == "y"){ ?>
	<div align="center" style="margin-bottom:15px;">
		<h1 class="credit_hold">System is on Credit Hold!</h1>
	</div>
	<?php } ?>
	<?php if(strtolower($rowSystems['pre_paid']) == "y"){ ?>
	<div align="center" style="margin-bottom:15px;">
		<h1 class="pre_paid">System is pre-paid service only!</h1>
	</div>
	<?php } ?>
	<?php if(strtolower($rowSystems['property']) == "f"){ ?>
	<div align="center" style="margin-bottom:15px;">
		<h1 class="future">System is future service!</h1>
	</div>
	<?php } ?>
	<?php if(strtolower($rowSystems['government_contract']) == "y"){ ?>
	<div align="center" style="margin-bottom:15px;">
		<h1 class="government">Government Contract</h1>
	</div>
	<?php } ?>
	<div style="clear:both;"></div>
	<div id="system_health_container" style="width:100%;  text-align:center; margin-bottom:15px;">
		<!--<div id="system_health" style="margin-left:auto; margin-right:auto;"></div>-->
		<div id="system_health" style="width:50%; margin-left:auto; margin-right:auto;"><div id='system_health_label'>System Health: <span id="system_health_text"></span></div></div>
	</div>
	<?php if($warranty_warn){ ?>
	<div id="warranty_warn_container" style="width:100%;  text-align:center; margin-bottom:15px;">
		<div id="warranty_warn" class="status_bar" style="width:50%; margin-left:auto; margin-right:auto;"><div id='warranty_warn'>Warranty Expired</div></div>
	</div>
	<?php } ?>
	<?php if($contract_warn){ ?>
	<div id="contract_warn_container" style="width:100%;  text-align:center; margin-bottom:15px;">
		<div id="contract_warn" class="status_bar" style="width:50%; margin-left:auto; margin-right:auto;"><div id='contract_warn'>Contract Expired</div></div>
	</div>
	<?php } ?>

	<div id="systemInfoLeft">
		<table id="systemData">
			<tr>
				<td class="rowLabel">System ID: </td>
				<td class="rowData"><?php echo $rowSystems['system_id'];?></td>
			</tr>
			<tr>
				<td class="rowLabel">System Nickname: </td>
				<td class="rowData"><?php echo $rowSystems['nickname'];?></td>
			</tr>
			<tr>
				<td class="rowLabel">Customer ID: </td>
				<td class="rowData"><?php echo $rowSystems['customer_id'];?></td>
			</tr>
			<tr>
				<td class="rowLabel">Customer Name: </td>
				<td class="rowData"><?php echo $rowSystems['customer_name'];?></td>
			</tr>
			<tr>
				<td class="rowLabel">Facility ID: </td>
				<td class="rowData"><?php echo $rowSystems['facility_id'];?></td>
			</tr>
			<tr>
				<td class="rowLabel">Facility Name: </td>
				<td class="rowData"><?php echo $rowSystems['facility_name'];?></td>
			</tr>
			<tr>
				<td class="rowLabel">Facility Address: </td>
				<td class="rowData"><?php echo $rowSystems['address'];?></td>
			</tr>
			<tr>
				<td class="rowLabel">Facility City: </td>
				<td class="rowData"><?php echo $rowSystems['city'];?></td>
			</tr>
			<tr>
				<td class="rowLabel">Facility State: </td>
				<td class="rowData"><?php echo $rowSystems['state'];?></td>
			</tr>
			<tr>
				<td class="rowLabel">Facility Zip: </td>
				<td class="rowData"><?php echo $rowSystems['zip'];?></td>
			</tr>
			<tr>
				<td class="rowLabel">Contact Name: </td>
				<td class="rowData"><?php echo $rowSystems['contact_name'];?></td>
			</tr>
			<tr>
				<td class="rowLabel">Contact Phone: </td>
				<td class="rowData"><?php echo $rowSystems['contact_phone'];?></td>
			</tr>
			<tr>
				<td class="rowLabel">Contact cell: </td>
				<td class="rowData"><?php echo $rowSystems['contact_cell'];?></td>
			</tr>
			<tr>
				<td class="rowLabel">Contact eMail: </td>
				<td class="rowData"><a href="mailto:<?php echo $rowSystems['contact_email'];?>" ><?php echo $rowSystems['contact_email'];?></a></td>
			</tr>
		</table>
	</div>
	<div id="systemInfoRight">
		<table id="systemData">
			<tr>
				<td class="rowLabel">System&nbsp;Type: </td>
				<td class="rowData"><?php echo $rowSystems['equipment_type'];?></td>
			</tr>
			<tr>
				<td class="rowLabel">System Location: </td>
				<td class="rowData"><?php echo $rowSystems['location'];?></td>
			</tr>
			<tr>
				<td class="rowLabel">Gantry/Magnet&nbsp;Serial: </td>
				<td class="rowData"><?php echo $rowSystems['system_serial'];?></td>
			</tr>
			<tr>
				<td class="rowLabel">Mobile: </td>
				<td class="rowData"><?php if(strtolower($rowSystems['mobile']) == 'y'){echo "Yes";}else{echo "No";}?></td>
			</tr>
			<?php if(strtolower($rowSystems['mobile']) == 'y'){ ?>
			<tr>
				<td class="rowLabel">Trailer ID: </td>
				<td class="rowData"><?php echo $rowSystems['trailer_id']; ?></td>
			</tr>
			<tr>
				<td class="rowLabel">Trailer MFG/VIN: </td>
				<td class="rowData"><?php echo"Mfg: ".$rowSystems['trailer_mfg']." VIN: ".$rowSystems['trailer_vin']; ?></td>
			</tr>
			<?php } ?>
			<tr>
				<td class="rowLabel">Date&nbsp;Installed: </td>
				<td class="rowData"><?php echo(empty($rowSystems['date_installed']))?'':date(phpdispfd, strtotime($rowSystems['date_installed']));?></td>
			</tr>
			<tr>
				<td class="rowLabel">Primary&nbsp;Engineer: </td>
				<td class="rowData"><?php echo $rowEngPri['name'];?></td>
			</tr>
			<tr>
				<td class="rowLabel">Secondary&nbsp;Engineer: </td>
				<td class="rowData"><?php echo $rowEngSec['name'];?></td>
			</tr>
			<tr>
				<td class="rowLabel">Contract&nbsp;Type: </td>
				<td class="rowData"><?php if($employee){echo $rowSystems['contract_type'];}?></td>
			</tr>
			<tr>
				<td class="rowLabel">Contract&nbsp;Terms: </td>
				<td class="rowData"><pre style="display:inline"><?php if($employee){echo $rowSystems['contract_terms'];}?>
</pre></td>
			</tr>
			<tr>
				<td class="rowLabel">Contract&nbsp;Start: </td>
				<td class="rowData"><?php if($employee){echo(empty($rowSystems['contract_start_date']))?'':date(phpdispfd, strtotime($rowSystems['contract_start_date']));}?></td>
			</tr>
			<tr>
				<td class="rowLabel">Contract&nbsp;End: </td>
				<td class="rowData"><?php if($employee){echo(empty($rowSystems['contract_end_date']))?'':date(phpdispfd, strtotime($rowSystems['contract_end_date']));}?></td>
			</tr>
			<tr>
				<td class="rowLabel">Warrenty&nbsp;Start: </td>
				<td class="rowData"><?php if($employee){echo(empty($rowSystems['warranty_start_date']))?'':date(phpdispfd, strtotime($rowSystems['warranty_start_date']));}?></td>
			</tr>
			<tr>
				<td class="rowLabel">Warranty&nbsp;End: </td>
				<td class="rowData"><?php if($employee){echo(empty($rowSystems['warranty_end_date']))?'':date(phpdispfd, strtotime($rowSystems['warranty_end_date']));}?></td>
			</tr>
			<tr>
				<td class="rowLabel">Arrival&nbsp;Date: </td>
				<td class="rowData"><?php echo(intval(date('Y',strtotime($rowSystems['arrival_date']))) < 1980)?'':date(phpdispfd, strtotime($rowSystems['arrival_date']));?></td>
			</tr>
			<tr>
				<td class="rowLabel">Last PM: </td>
				<td class="rowData"><?php echo(intval(date('Y',strtotime($rowSystems['last_pm']))) < 1980)?'':date(phpdispfd, strtotime($rowSystems['last_pm']));?></td>
			</tr>
			<tr>
				<td class="rowLabel">Next PM: </td>
				<td class="rowData"><?php $next_pm = $php_utils->next_pm($mysqli, $rowSystems['ver_unique_id']); if($next_pm){echo date(phpdispfd, strtotime($next_pm));}else{echo "";};?></td>
			</tr>
			<tr>
				<td class="rowLabel">Service&nbsp;Key: </td>
				<td class="rowData"><?php if($employee){echo $rowSystems['service_key'];}?></td>
			</tr>
			<tr>
				<td class="rowLabel">Service&nbsp;Key&nbsp;Expire: </td>
				<td class="rowData"><?php if($employee){echo(intval(date('Y',strtotime($rowSystems['service_key_expire']))) < 1980)?'':date(phpdispfd, strtotime($rowSystems['service_key_expire']));}?></td>
			</tr>
			<tr>
				<td class="rowLabel">System&nbsp;Unique&nbsp;ID: </td>
				<td class="rowData"><?php if($employee){echo $rowSystems['unique_id'];}?></td>
			</tr>
			<tr>
				<td class="rowLabel">System&nbsp;Version&nbsp;Unique&nbsp;ID: </td>
				<td class="rowData"><?php if($employee){echo $rowSystems['ver_unique_id'];}?></td>
			</tr>
		</table>
	</div>
	<?php if($customer_logins){ ?>
	<div id="systemNotes">
		<table id="customerLogins" width="100%">
			<tr>
				<td class="rowLabel">Customer Logins: </td>
				<td class="rowData"><?php 
					$customer_logins = explode(',',$customer_logins);
					foreach($customer_logins as $login){
						if(strtolower($_SESSION['perms']['perm_edit_users']) == "y"){
							echo "<a href=\"users.php?uid=".$login."\">".$login."</a>&emsp;&emsp;";
						}else{
							echo $login."&emsp;&emsp;";
						}
					}?></td>
			</tr>
		</table>
	</div>
	<?php } ?>
	<div class="line"></div>
	<div id="map"> <br />
		<a id="getdir" target="_new" >Get Directions</a>
		<div style="display:none">
			<input id="address" type="textbox" value="<?php echo $rowSystems['address'] . "," . $rowSystems['city'] . "," . $rowSystems['state'] ;?>">
			<input type="button" value="Geocode" onclick="codeAddress()">
		</div>
		<div id="map_canvas" style="height:300px;top:30px; clear:both"> </div>
	</div>
	<br>
	<br>
<!--	<div class="line"></div>-->
<!--	<div id="afterReportContainer">-->
<!--		<div id="afterReportUpdate">-->
<!--			<div id="ReportFrameTitle"> Send System Status Update </div>-->
<!--			<br>-->
<!--			<div id="afterReportUpdateForm">-->
<!--				<form id="form" name="form" >-->
<!--					<textarea name="status" id="status" maxlength="2000"></textarea>-->
<!--					<input type="hidden" name="system_unique_id" id="system_unique_id" value="--><?php //echo $rowSystems['unique_id']; ?><!--" />-->
<!--					<input type="hidden" name="update_uid" id="update_uid" value="--><?php //echo $_SESSION['login'] ?><!--" />-->
<!--					<input type="submit" name="submit" id="submit" value="Send"  class="sendUpdate button_jquery_send">-->
<!--				</form>-->
<!--			</div>-->
<!--		</div>-->
<!--	</div>-->
	<?php if($employee == true){ ?>
	<div class="line"></div>
	<div id="ReportOuterFrame">
		<div id="ReportFrame">
			<div id="ReportFrameTitle"> System Notes </div>
			<div id="notesFrame" style="text-align:center">
				<?php
				$sql = "SELECT sn.note, sn.`date`, u.name, sn.id
						FROM systems_notes AS sn
						LEFT JOIN users AS u ON u.uid = sn.uid
						WHERE sn.system_unique_id = '".$unique_id."'
						AND sn.property = 'G'
						ORDER BY sn.`date` DESC;";

				if(!$resultNotes = $mysqli->query($sql)){
					$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
					$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
					$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
				}
				?>
				<div id="notesFrame">
					<div id="notesDiv" <?php if($resultNotes->num_rows == 0){echo "style=\"display:none\"";} ?>>
						<table width="100%" id="notesTable" >
							<thead>
							<tr>
								<th width="20%">Date</th>
								<th width="65%">Entry</th>
								<th width="15%">User</th>
							</tr>
							</thead>
							<tbody>
							<?php
							if($resultNotes->num_rows != 0){
								while($rowNotes = $resultNotes->fetch_assoc())
								{
									echo "<tr>\n";
									echo "<td><a class='iframeNote' href='/report/common/systems_notes.php?method=view&id=".$rowNotes['id']."'>".date(phpdispfdt, strtotime($rowNotes['date']))."</a></td>\n";
									echo "<td>". limit_words($rowNotes['note'],$probrpt_word_limt)."</td>\n";
									echo "<td>". $rowNotes['name']."</td>\n";
									echo "</tr>\n";
								}
							}
							?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="line"></div>
	<div id="ReportOuterFrame">
		<?php
					$sql = "SELECT sr.*, ss.`status` AS system_status_name
						FROM systems_requests AS sr
						LEFT JOIN systems_base AS sb ON sb.unique_id = sr.system_unique_id
						LEFT JOIN systems_status AS ss ON ss.id = sb.status
						WHERE sr.`status`='open' AND sr.`system_ver_unique_id` = '".$rowSystems['ver_unique_id']."' AND sr.`deleted` = 'n';";
					if(!$resultRequests = $mysqli->query($sql)){
						$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
						$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
						$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);	
					}
					$resultRequestsNum = $resultRequests->num_rows;
				?>
		<div id="ReportFrame">
			<div id="ReportFrameTitle"> Open Service Requests </div>
			<div id="requestsDiv" <?php if($resultRequestsNum <= 0){echo "style=\"display:none\"";} ?>>
				<table width="100%" id="requestsTable" >
					<thead>
						<tr>
							<th width="50px">Request Number</th>
							<th width="100px">Date</th>
							<!--<th width="50px">System ID</th>
                                    <th>System Name</th>-->
							<th width="300">Problem</th>
							<?php if(isset($_COOKIE["showall"]) or isset($_SESSION['adn']) or isset($_SESSION['mgt'])){echo "<th>Engineer</th>";}else{echo "<th>Cust Act</th>";} ?>
							<th width="125px">Status</th>
							<th width="50px">Report Started</th>
						</tr>
					</thead>
					<tbody>
						<?php
								if($resultRequestsNum != 0){
									while($rowRequest = $resultRequests->fetch_assoc())
									{
										echo "<tr class=\"requestsHelp\">\n";
										if(strtolower($rowRequest['report_started'])=="y"){
											echo "<td><a href=\"/report/common/report_new_edit.php?id=".$rowRequest['unique_id'].'&uid='.$myusername."\">". $rowRequest['request_num']."</a></td>\n";
										}else{
											echo "<td><a href=\"/report/common/report_new_edit.php?id=".$rowRequest['unique_id'].'&uid='.$myusername."\">". $rowRequest['request_num']."</a></td>\n";
										}
										echo "<td>". date(phpdispfd, strtotime($rowRequest['request_date']))."</td>\n";
										//echo "<td>". $rowRequest['system_id']."</td>\n";
										//echo "<td>". $rowRequest['system_nickname']."</td>\n";
										echo "<td>". $rowRequest['problem_reported']."</td>\n";
										if(isset($_COOKIE["showall"]) or isset($_SESSION['adm']) or isset($_SESSION['mgt'])){
											echo "<td>". $arr_engineers[$rowRequest['engineer']]."</td>\n";
										}else{
											echo "<td>". $rowRequest['customer_actions']."</td>\n";
										}
										echo "<td>". $rowRequest['system_status_name']."</td>\n";
										echo "<td>"; if(strtolower($rowRequest['report_started'])=="y"){echo "Yes";} echo "</td>\n";
										echo "</tr>\n";
									}
								}
								?>
					</tbody>
				</table>
			</div>
			<div id="requestDiv" <?php if($resultRequestsNum != 0){echo "style=\"display:none\"";}else{echo "style=\"text-align:center\"";} ?>> <br />
				<h2>No Service Requests for <?php echo $rowSystems['system_name'] ?></h2>
			</div>
		</div>
	</div>
	<div class="line"></div>
	<div id="ReportOuterFrame">
		<?php  
					$sql = "SELECT * FROM systems_reports WHERE `status`='open' AND `system_unique_id` = '".$rowSystems['unique_id']."'";
					if(!$resultOpen = $mysqli->query($sql)){
						$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
						$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
						$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);	
					}
				?>
		<div id="ReportFrame">
			<div id="ReportFrameTitle"> Unfinished Reports </div>
			<div id="openReprotsDiv" <?php if($resultOpen->num_rows <= 0){echo "style=\"display:none\"";} ?>>
				<table width="100%" id="openReportsTable" >
					<thead>
						<tr>
							<th width="75px">Date Started</th>
							<th width="50px">Report ID</th>
							<!--<th width="50px">System ID</th>
                                    <th>System Name</th>-->
							<th>Engineer</th>
							<th>Complaint</th>
						</tr>
					</thead>
					<tbody>
						<?php
								if($resultOpen){
									while($rowOpen = $resultOpen->fetch_assoc())
									{
										echo "<tr class=\"openReportsHelp\">\n";
										if(isset($_SESSION['roles']['role_engineer']) or isset($_SESSION['roles']['contractor']) or isset($_SESSION['roles']['management']) or isset($_SESSION['perms']['perm_edit_reports'])){
											echo "<td><a href=\"/report/common/report_new_edit.php?id=".$rowOpen['unique_id'].'&uid='.$myusername."\">". date(phpdispfd, strtotime($rowOpen['date']))."</a></td>\n";
										}else{
											echo "<td>". date(phpdispfd, strtotime($rowOpen['date']))."</td>\n";
										}
										echo "<td>". $rowOpen['report_id']."</td>\n";
										//echo "<td>". $rowOpen['system_id']."</td>\n";
										//echo "<td>". $rowOpen['system_nickname']."</td>\n";
										echo "<td>". $arr_engineers[$rowOpen['engineer']]."</td>\n";
										echo "<td>". $rowOpen['complaint']."</td>\n";
										echo "</tr>\n";
									}
								}
								?>
					</tbody>
				</table>
			</div>
			<div id="openReportsDiv" <?php if($resultOpen->num_rows != 0){echo "style=\"display:none\"";}else{echo "style=\"text-align:center\"";} ?>> <br />
				<h2>No Unfinished Reports for <?php echo $rowSystems['system_name'];?></h2>
			</div>
		</div>
	</div>
	<?php } ?>
	<div class="line"></div>
	<div id="ReportOuterFrame">
		<?php  
					$sql = "SELECT * FROM systems_reports WHERE `status`='closed' AND `system_unique_id` = '".$rowSystems['unique_id']."';";
					if(!$resultClosed = $mysqli->query($sql)){
						$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
						$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
						$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
					}
				?>
		<div id="ReportFrame">
			<div id="ReportFrameTitle">Completed Reports</div>
			<div id="pastReprotsDiv" <?php if($resultClosed->num_rows <= 0){echo "style=\"display:none\"";} ?>>
				<table width="100%" id="pastReportsTable" >
					<thead>
						<tr>
							<th>Date</th>
							<th width="50px">Report ID</th>
							<!--<th width="50px">System ID</th>
                                    <th>System Name</th>-->
							<th width="100px">Engineer</th>
							<th width="200px">Complaint</th>
							<th>Service</th>
						</tr>
					</thead>
					<tbody>
						<?php
								while($rowClosed = $resultClosed->fetch_assoc())
								{
									echo "<tr class=\"pastReportsHelp\">\n";
									echo "<td><a href=\"/report/common/report_view.php?id=".$rowClosed['unique_id'].'&uid='.$myusername."\">". date(phpdispfd, strtotime($rowClosed['date']))."</a></td>\n";
									echo "<td>". $rowClosed['report_id']."</td>\n";
									//echo "<td>". $rowClosed['system_id']."</td>\n";
									//echo "<td>". $rowClosed['system_nickname']."</td>\n";
									echo "<td>". $arr_engineers[$rowClosed['engineer']]."</td>\n";
									echo "<td>". $rowClosed['complaint']."</td>\n";
									echo "<td>". $rowClosed['service']."</td>\n";
									echo "</tr>\n";
								}
								?>
					</tbody>
				</table>
			</div>
			<div id="openReportsDiv" <?php if($resultClosed->num_rows != 0){echo "style=\"display:none\"";}else{echo "style=\"text-align:center\"";} ?>> <br />
				<h2>No Completed Reports for <?php echo $rowSystems['nickname'];?></h2>
			</div>
		</div>
	</div>
	<?php if($employee == true){ ?>
	<div class="line"></div>
	<div id="ReportOuterFrame">
		<?php  
					$sql = "SELECT ssj.id, ssj.date, ssj.note, u.name, IF(ssj.system_status IS NULL OR ssj.system_status = '', 'N/A', ss.abv) AS status_abv, sr.request_num, sr.`status`
					FROM systems_service_journal AS ssj
					LEFT JOIN systems_requests AS sr ON sr.unique_id = ssj.request_report_unique_id
					LEFT JOIN users AS u ON u.uid = ssj.uid
					LEFT JOIN systems_status AS ss ON ss.id = ssj.system_status
					WHERE sr.system_unique_id = '".$unique_id."';";
					if(!$resultEve = $mysqli->query($sql)){
						$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
						$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
						$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
					}
				?>
		<div id="ReportFrame">
			<div id="ReportFrameTitle">Service Journal</div>
			<div id="eveUpdatsDiv" <?php if($resultEve->num_rows <= 0){echo "style=\"display:none\"";} ?>>
				<table width="100%" id="eveUpdatsTable" >
					<thead>
						<tr>
							<th width="15%">Date</th>
							<th width="15%">Rep/Req ID</th>
							<th>Entry</th>
							<th width="10%">Status</th>
							<th width="15%">User</th>
						</tr>
					</thead>
					<tbody>
						<?php
								while($rowEve = $resultEve->fetch_assoc()){
									echo "<tr>\n";
									echo "<td><a class='iframeNote' href='/report/common/systems_service_journal.php?id=".$rowEve['id']."'>". date(phpdispfdt,strtotime($rowEve['date']))."</a></td>\n";
									echo "<td>". $rowEve['request_num']."</td>\n";
									echo "<td>". limit_words($rowEve['note'],$probrpt_word_limt)."</td>\n";
									echo "<td>". $rowEve['status_abv']."</td>\n";
									echo "<td>". $rowEve['name']."</td>\n";
									echo "</tr>\n";
								}

//								while($rowEve = $resultEve->fetch_assoc())
//								{
//									echo "<tr>\n";
//									if($rowEve['report_unique_id'] == ''){
//										echo "<td>".$rowEve['id']."</td>\n";
//									}else{
//										echo "<td><a href=\"/report/common/report_view.php?id=".$rowEve['report_unique_id'].'&uid='.$myusername."\">". $rowEve['id']."</a></td>\n";
//									}
//									echo "<td>". $rowEve['name']."</td>\n";
//									echo "<td>". $rowEve['status']."</td>\n";
//									echo "<td>". date(storef, strtotime($rowEve['date']))."</td>\n";
//									echo "</tr>\n";
//								}
								?>
					</tbody>
				</table>
			</div>
			<div id="eveUpdatsDiv" <?php if($resultEve->num_rows != 0){echo "style=\"display:none\"";}else{echo "style=\"text-align:center\"";} ?>> <br />
				<h2>No System Status Updates for <?php echo $rowSystems['nickname'];?></h2>
			</div>
		</div>
	</div>
	<?php if(strtolower($rowSystems['he_monitor']) == "y" and $rowSystems['he_monitor_serial'] != ''){ 
				if (!@require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/oivision_data.php')) {
					require_once '/report/common/scripts/oivision_data.php';	
				}
				$oivision = new OiVision();
				
				$oivision_data = $oivision->get_readings($rowSystems['he_monitor_serial']);
				d($oivision_data);
								
				if(strtolower($oivision_data['site']['use_global_alarm']) == "y"){
					$oivision_settings = new SimpleXMLElement($settings->oivision_settings_xml, null, true);
				}else{
					$oivision_settings = new SimpleXMLElement("<settings></settings>");	
					$oivision_settings->HeLevelThreshhold = $rowSites['alarm_he'];
					$oivision_settings->HeLevelCriticalThreshhold = $rowSites['alarm_hec'];
					$oivision_settings->HeBurnThreshhold = $rowSites['alarm_heb'];
					$oivision_settings->HePressHighCriticalThreshhold = $rowSites['alarm_vphc'];
					$oivision_settings->HePressHighThreshhold = $rowSites['alarm_vph'];
					$oivision_settings->HePressLowThreshhold = $rowSites['alarm_vpl'];
					$oivision_settings->HePressLowCriticalThreshhold = $rowSites['alarm_vplc'];
					$oivision_settings->WaterFlowHighCriticalThreshhold = $rowSites['alarm_wfhc'];
					$oivision_settings->WaterFlowHighThreshhold = $rowSites['alarm_wfh'];
					$oivision_settings->WaterFlowLowThreshhold = $rowSites['alarm_wfl'];
					$oivision_settings->WaterFlowLowCriticalThreshhold = $rowSites['alarm_wflc'];
					$oivision_settings->WaterTempHighCriticalThreshhold = $rowSites['alarm_wthc'];
					$oivision_settings->WaterTempHighThreshhold = $rowSites['alarm_wth'];
					$oivision_settings->WaterTempLowThreshhold = $rowSites['alarm_wtl'];
					$oivision_settings->WaterTempLowCriticalThreshhold = $rowSites['alarm_wtlc'];
					$oivision_settings->RoomTempHighCriticalThreshhold = $rowSites['alarm_rthc'];
					$oivision_settings->RoomTempHighThreshhold = $rowSites['alarm_rth'];
					$oivision_settings->RoomTempLowThreshhold = $rowSites['alarm_rtl'];
					$oivision_settings->RoomTempLowCriticalThreshhold = $rowSites['alarm_rtlc'];
					$oivision_settings->RoomHumHighCriticalThreshhold = $rowSites['alarm_rhhc'];
					$oivision_settings->RoomHumHighThreshhold = $rowSites['alarm_rhh'];
					$oivision_settings->RoomHumLowThreshhold = $rowSites['alarm_rhl'];
					$oivision_settings->RoomHumLowCriticalThreshhold = $rowSites['alarm_rhlc'];
					$oivision_settings->StoreHeLevelThreshhold = $rowSites['alarm_she'];
					$oivision_settings->StoreHeLevelCriticalThreshhold = $rowSites['alarm_shec'];
					$oivision_settings->StoreHePressHighCriticalThreshhold = $rowSites['alarm_svphc'];
					$oivision_settings->StoreHePressHighThreshhold = $rowSites['alarm_svph'];
					$oivision_settings->StoreHePressLowThreshhold = $rowSites['alarm_svpl'];
					$oivision_settings->StoreHePressLowCriticalThreshhold = $rowSites['alarm_svplc'];	
				}
				d($oivision_settings);
				$reporting = false;
				if($oivision->reporting($rowSystems['he_monitor_serial'])){
					$reporting = true;
				}
			?>
	<div class="line"></div>
	<div id="ReportOuterFrame">
		<div id="ReportFrame">
			<div id="ReportFrameTitle">OiVision Data</div>
			<div id="oivision" <?php if(!$reporting){echo "style=\"display:none\"";}else{echo "style=\"text-align:center\"";} ?>>
				
				<div style="width: 100%; text-align:center; margin-bottom:5px;">Last Update: <?php echo $oivision_data['reading']['date']; ?></div>
				<div style="width: 100%; text-align:center; margin-bottom:15px;">OiVision MAC: <?php echo $oivision_data['site']['mac']; ?></div>
				
				<div class="overview_div" style="width:100%; margin-left:auto; margin-right:auto;">
					<div style="clear:both">
						<div class="meter_name" <?php if($oivision_data['site']['He'] != 'Y'){echo "style=\"text-decoration:line-through;\"";} ?> title="HeCont">Helium Level</div>
						<div class="meter_data" title="HeReading"><?php echo $oivision_data['reading']['He']; ?>%</div>
						<div class="meter_bar" title="Helium">
							<div class="meter <?php
								if ($oivision_data['site']['Storage'] != "Y") {
									$hightresh = $oivision_settings->HeLevelThreshhold;
								} else {
									$hightresh = $oivision_settings->StoreHeLevelThreshhold;
								}
								if ($oivision_data['reading']['He'] <= $hethresh) {			
								   if(strtolower($oivision_data['site']['He']) == "y"){ 
										echo "red";
									}
								}
							?>"><span style="width: <?php
								if ($oivision_data['reading']['He'] > 100.00) {
									echo "100.00";
								} else {
									echo round($oivision_data['reading']['He']);
								}
							 ?>%;"></span></div>
						</div>
					</div>
					<div style="clear:both">
						<div class="meter_name" <?php if($oivision_data['site']['HeP'] != 'Y'){echo "style=\"text-decoration:line-through;\"";} ?> title="HePCont">Helium Pressure</div>
						<div class="meter_data" title="HePReading"><?php echo $oivision_data['reading']['HeP']; ?> PSIG</div>
						<div class="meter_bar" title="Helium Pressure">
							<div class="meter <?php
								if ($oivision_data['site']['Storage'] != "Y") {
									$hightresh = $oivision_settings->HePressHighThreshhold;
									$lowthresh = $oivision_settings->HePressLowThreshhold;
								} else {
									$hightresh = $oivision_settings->StoreHePressHighThreshhold;
									$lowthresh = $oivision_settings->StoreHePressLowThreshhold;
								}
								if ($oivision_data['reading']['HeP'] <= $lowthresh or $oivision_data['reading']['HeP'] >= $hightresh) {
									if(strtolower($oivision_data['site']['HeP']) == "y"){
										echo "red";
									}
								}
							?>"><span style="width: <?php echo $oivision_data['reading']['HeP'] * 10 ?>%;"></span></div>
						</div>
					</div>
					<div style="clear:both">
						<div class="meter_name" <?php if($oivision_data['site']['Wf'] != 'Y'){echo "style=\"text-decoration:line-through;\"";} ?> title="wfCont">Water Flow</div>
						<div class="meter_data" title="wfReading"><?php echo $oivision_data['reading']['Wf']; ?> GPM</div>
						<div class="meter_bar" title="Water Flow">
							<div class="meter <?php
								if ($oivision_data['site']['Storage'] != "Y") {
									if ($oivision_data['reading']['Wf'] <= $oivision_settings->WaterFlowLowThreshhold or $oivision_data['reading']['Wf'] >= $oivision_settings->WaterFlowHighThreshhold) {
										if(strtolower($oivision_data['site']['Wf']) == "y"){ 
											echo "red";
										}
									}
								}
							?>"><span style="width: <?php echo round($oivision_data['reading']['Wf']) * 10; ?>%;"></span></div>
						</div>
					</div>
					<div style="clear:both">
						<div class="meter_name" <?php if($oivision_data['site']['Wtf'] != 'Y'){echo "style=\"text-decoration:line-through;\"";} ?> title="WtCont">Water Temperature</div>
						<div class="meter_data" title="WtReading"><?php echo $oivision_data['reading']['Wtf']; ?> Deg F</div>
						<div class="meter_bar" title="Water Temperature">
							<div class="meter <?php 
								if ($oivision_data['site']['Storage'] != "Y") {
									if ($oivision_data['reading']['Wtf'] <= $oivision_settings->WaterTempLowThreshhold or $oivision_data['reading']['Wtf'] >= $oivision_settings->WaterTempHighThreshhold) {
										if(strtolower($oivision_data['site']['Wtf']) == "y"){
											echo "red";
										}
									}
								}
							
							?>"><span style="width: <?php echo round($oivision_data['reading']['Wtf']); ?>%;"></span></div>
						</div>
					</div>
					<div style="clear:both">
						<div class="meter_name" <?php if($oivision_data['site']['Tf'] != 'Y'){echo "style=\"text-decoration:line-through;\"";} ?> title="tfCont">Room Temperature</div>
						<div class="meter_data" title="tfReading"><?php echo $oivision_data['reading']['Tf']; ?> Deg F</div>
						<div class="meter_bar" title="Equipment Room Temperature">
							<div class="meter <?php
								if ($oivision_data['site']['Storage'] != "Y") {
									if ($oivision_data['reading']['Tf'] <= $oivision_settings->RoomTempLowThreshhold or $oivision_data['reading']['Tf'] >= $oivision_settings->RoomTempHighThreshhold) {
										if(strtolower($oivision_data['site']['Tf']) == "y"){
											echo " red";
										}
									}
								}
							?>"><span style="width: <?php echo round($oivision_data['reading']['Tf']); ?>%;"></span></div>
						</div>
					</div>
					<div style="clear:both">
						<div class="meter_name" <?php if($oivision_data['site']['Rh'] != 'Y'){echo "style=\"text-decoration:line-through;\"";} ?> title="rhCont">Room Humidity</div>
						<div class="meter_data" title="rhReading"><?php echo $oivision_data['reading']['Rh']; ?> %RH</div>
						<div class="meter_bar" title="Equipment Room Humidity">
							<div class="meter <?php 
								if ($oivision_data['site']['Storage'] != "Y") {
									if ($oivision_data['reading']['Rh'] <= $oivision_settings->RoomHumLowThreshhold or $oivision_data['reading']['Rh'] >= $oivision_settings->RoomHumHighThreshhold) {
										if(strtolower($oivision_data['site']['Rh']) == "y"){
											echo " red";
										}
									}
								}							
							?>"><span style="width: <?php echo round($oivision_data['reading']['Rh']) * 0.9; ?>%;"></span></div>
						</div>
					</div>
					<div style="clear:both">
						<div class="meter_name" <?php if($oivision_data['site']['FieldOn'] != 'Y'){echo "style=\"text-decoration:line-through;\"";} ?> title="FieldOnCont">Magnet Field</div>
						<div class="meter_data" title="FieldOnReading"><?php echo $oivision_data['reading']['FieldOn']; ?></div>
						<div class="meter_bar" title="Field On">
							<div class="meter <?php 
								if ($oivision_data['site']['Storage'] != "Y") {
									if ($oivision_data['reading']['FieldOn'] == 0) {
										if(strtolower($oivision_data['site']['FieldOn']) == "y"){
											echo " red";
										}
									}
								}
							?>"><span style="width: 100%;"></span></div>
						</div>
					</div>
					<div style="clear:both">
						<div class="meter_name" <?php if($oivision_data['site']['CompOn'] != 'Y'){echo "style=\"text-decoration:line-through;\"";} ?> title="CCont">Compressor On</div>
						<div class="meter_data" title="CReading"><?php echo $oivision_data['reading']['CompOn']; ?></div>
						<div class="meter_bar" title="Compressor On">
							<div class="meter <?php
								if ($oivision_data['site']['Storage'] != "Y") {
									if ($oivision_data['reading']['CompOn'] == 0) {
									   if(strtolower($oivision_data['site']['CompOn']) == "y"){ 
											echo " red";
										}
									}
								}
							?>"><span style="width: 100%;"></span></div>
						</div>
					</div>
				</div>
				<br>
				<br>
			</div>
			<div id="oivision" <?php if($reporting){echo "style=\"display:none\"";}else{echo "style=\"text-align:center\"";} ?>><br>
				<h2>System not reporting. Last report <?php echo $oivision_data['reading']['date']; ?></h2>
			</div>
		</div>
	</div>
	<?php } ?>
	<?php } ?>
</div>
<?php require_once($footer_include); ?>
