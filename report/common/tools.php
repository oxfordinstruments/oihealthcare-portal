<?php
//Update Completed 6/18/2015

$debug = false;
if(isset($_GET['debug'])){
	$debug = true;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

if(!isset($_SESSION['perms']['perm_tool_cal_edit']) and !isset($_SESSION['perms']['perm_tool_cal_view'])){
	$log->logerr("You do not have permission to manage tools",1071,true,basename(__FILE__),__LINE__);
}

$edit = false;
if(isset($_GET['edit'])){
	if(!isset($_SESSION['perms']['perm_tool_cal_edit'])){
		$log->logerr("You do not have permission to edit tools",1071,true,basename(__FILE__),__LINE__);
	}
	$edit = true;
}

$view = false;
if(isset($_GET['view'])){
	if(!isset($_SESSION['perms']['perm_tool_cal_view'])){
		$log->logerr("You do not have permission to view tools",1071,true,basename(__FILE__),__LINE__);
	}
	$view = true;
}

if($edit and $view){
	$log->logerr("Cannot edit and view at the same time",4,true,basename(__FILE__),__LINE__);	
}


$users['none'] = array('name'=>'');
$sql="SELECT u.uid, u.name
FROM users AS u
INNER JOIN users_groups AS ug ON ug.uid = u.uid
INNER JOIN users_group_id AS grid ON grid.id = ug.gid AND grid.`group` = 'grp_employee'
WHERE u.id >= 10
ORDER BY u.uid ASC;";
s($sql);
if(!$resultUsers = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
while($rowUsers = $resultUsers->fetch_assoc()){
	$users[$rowUsers['uid']]=array('name'=>$rowUsers['name'], 'timezone'=>$rowUsers['timezone']);
}
d($users);

$showdata = false;
if($edit or $view){
	$showdata = true;
	
	if(!isset($_GET['unique_id'])){
		$log->logerr("Invalid tool Unique ID",1048,true,basename(__FILE__),__LINE__);
	}
	
	$sql="SELECT t.*, u.name, tc.id, tc.date, tc.expire, tc.calibrated_by, tc.notes AS cal_notes
	FROM tools AS t
	LEFT JOIN misc_offices AS mo ON mo.id = t.office
	LEFT JOIN users AS u ON u.uid = t.uid
	LEFT JOIN tools_calibrations AS tc ON tc.unique_id = t.unique_id
	WHERE t.unique_id = '".$_GET['unique_id']."'
	ORDER BY tc.id DESC;";
	s($sql);
	if(!$resultTool = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	
	$tool = array();
	while($rowTool = $resultTool->fetch_assoc()){
		array_push($tool,$rowTool);
	}
	d($tool);
	$unique_id = $tool[0]['unique_id'];	
}else{
	$unique_id = md5(uniqid());
}
s($unique_id);

d($tool[0]);

$sql="SELECT tc.calibrated_by
FROM tools_calibrations AS tc
WHERE tc.calibrated_by IS NOT NULL
GROUP BY tc.calibrated_by;";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$calibrators = "[";
while($row = $result->fetch_assoc()){
	$calibrators .= "'".trim($row['calibrated_by'])."',";
}
$calibrators = rtrim($calibrators,',');
$calibrators .= "]";

?>
<!DOCTYPE html>
<html>
<head>
<?php require_once($head_include);?>
<?php require_once($css_include);?>

<link href="/resources/css/form_report.css" rel="stylesheet" type="text/css" media="screen">

<?php require_once($js_include);?>

<script src="/resources/js/jquery.ui.datepicker.js"></script>
<script src="/resources/js/jquery-ui-timepicker-addon.js"></script>
<script src="/resources/js/jquery-dynamic-form_MODIFIED.js"></script>
<script type="text/javascript">
var users_json = <?php echo json_encode($users); ?>;
var calibrators = <?php echo $calibrators; ?>;

$(document).ready(function() {

//	$(".chooser").chosen({
//		no_results_text: "Oops, nothing found!",
//		disable_search_threshold: 10,
//		placeholder_text_single: '  ',
//		width: '100%'
//	});

	$(".button_jquery_save").button({
		icons: {
			primary: "ui-icon-disk"
		}
	});
	
	$(".button_jquery_edit").button({
		icons: {
			primary: "ui-icon-pencil"
		}
	});
		

	$(function() {
		$( ".date" ).datepicker({
			numberOfMonths: 1,
			showButtonPanel: true,
			changeYear: true,
			constrainInput: true,
			duration: "fast",
			dateFormat: "<?php echo dpdispfd; ?>"
		});	
	});
	
	$(".text_toggle").focus(function() {
		$(this).animate({height: 104}, "normal");
	}).blur(function() {
		$(this).animate({height: 15}, "normal");
	});

	allTable = $('#allHistTable').dataTable({
		"bJQueryUI": true,
		"bStateSave": false,
		"sPaginationType": "full_numbers",
		"aaSorting": [ [1,'desc'] ],
	});	
	
	$( ".autocomp" ).autocomplete({
    	source: calibrators
	});
/////////////////////////////////////////////////////////////////////////////////////
});
/////////////////////////////////////////////////////////////////////////////////////

function calibrate(){
	$("#cal_btn").hide();
	$("#cal_input").show();	
	
	$('option:selected', 'select[name="calibrating"]').removeAttr('selected');
	$('select[name="calibrating"]').find('option[value="N"]').attr("selected",true);
	//$("#calibrating").val(2);
	$("#calibrating").trigger("chosen:updated");
	document.getElementById("new_cal").checked = true;
}

function submitcheck(data){
//	alert('Testing Only!');
//	document.forms["form"].submit();
	
	ids = [];
	errors = [];
	var find = ["\""];
	var replace = ["'"];
	var datere = /^20(1|2)\d-((1[0-2])|(0[1-9]))-(([0-2][0-9])|3(0|1))$/; //date yyyy-mm-dd
	var dtre = /^\d{1,10}(\.\d{1,10})?$/; //downtime 00000.00000  .00000 optional
		
	if($('#tool').val()==''){ids.push('#tool'); errors.push('Tool name blank');}
	if($('#model').val()==''){ids.push('#model'); errors.push('Model number blank');}
	if($('#mfg').val()==''){ids.push('#mfg'); errors.push('Manufacturer blank');}
	if($('#serial').val()==''){ids.push('#serial'); errors.push('Serial number blank');}
	
	if(document.getElementById("new_cal").checked == true){
		if(!datere.test($('#date').val())){ids.push("#date"); errors.push("Calibration date invalid");}	
		if(!datere.test($('#expire').val())){ids.push("#expire"); errors.push("Calibration expiration date invalid");}	
		if($('#calibrated_by').val()==''){ids.push('#calibrated_by'); errors.push('Calibrated by blank');}
	}
	
	var textarea = $('#notes').val();
	textarea = textarea.replaceArray(find, replace);
	$('#notes').val(textarea);
	
	var location = $('#location').val();
	location = location.replaceArray(find, replace);
	$('#location').val(location);
	
	
	console.log("ids: " + ids);
	console.log("errors: " + errors);
	showErrors(ids,errors);
	
	if(ids.length <= 0){
		if(data == 'save'){
			<?php if(!$_SESSION['mobile_device']){ ?>
				$.prompt("<h3>Save changes to this tool?</h3>",{
					title: "Save Changes",
					buttons: { Yes: 1, No: -1 },
					focus: 1,
					submit:function(e,v,m,f){ 
						e.preventDefault();
						showErrors(ids,errors);
						$("#errors").hide();
						if(v == 1){
							$.prompt.close();
							document.forms["form"].submit();
						}else{
							$.prompt.close();
						}
					}
				});
			<?php }else{ ?>
				if(confirm("Save changes to this tool?")){
					document.forms["form"].submit();
				}
			<?php } ?>
		}else{
			<?php if(!$_SESSION['mobile_device']){ ?>
				$.prompt("<h3>Create new tool?</h3>",{
					title: "Create Tool",
					buttons: { Yes: 1, No: -1 },
					focus: 1,
					submit:function(e,v,m,f){ 
						e.preventDefault();
						showErrors(ids,errors);
						$("#errors").hide();
						if(v == 1){
							$.prompt.close();
							document.getElementById("new_tool").checked = true;
							document.forms["form"].submit();
						}else{
							$.prompt.close();
						}
					}
				});
			<?php }else{ ?>
				if(confirm("Create new tool?")){
					document.getElementById("new_tool").checked = true;
					document.forms["form"].submit();
				}
			<?php } ?>			
		}		
	}
}
/////////////////////////////////////////////////////////////////////////////////////
function showErrors(ids,errors){
	//thin solid #2C3594
	$("input, select, .chosen-container").each(function(index, element) {
		$(this).animate({
			borderColor: "#2C3594",
			boxShadow: 'none'
		});
	});
	
	$.each(ids,function(index,value){
		$(value).animate({
			borderColor: "#cc0000",
			boxShadow: '0 0 5px 3px rgba(255,0,0,0.4)'
		});
	});
	$("#errors > span").html("");
	$.each(errors,function(index, value){
		$("#errors > span").append(value + "<br>");
	});
	$("#errors").show('slow');
	$(document).scrollTop(0);
}

String.prototype.replaceArray = function(find, replace) {
	var replaceString = this;
	var regex; 
	for (var i = 0; i < find.length; i++) {
		regex = new RegExp(find[i], "g");
		replaceString = replaceString.replace(regex, replace[i]);
	}
	return replaceString;
};

/////////////////////////////////////////////////////////////////////////////////////
</script>

</head>
<body>
<?php require_once($header_include); ?>
<div id="OIReportContent"> 
<div id="stylized" class="myform">
<form id="form" name="form" method="post" enctype='multipart/form-data' action="tools_save.php">
<div id="srHeaderDiv">
	<h1>Tool Calibration</h1>
	<br />
</div>
<div id="main"><!-- do not remove -->
<div id="errors" style="text-align:center;display:none; margin-bottom:25px; font-size:18px;">
	<h2 style="margin:0px; padding:0px; font-size:24px;">Errors to fix</h2>
	<span style="color:#F00">
	</span>
</div>

<div id="srDataDiv">
    <table id="srDataTable" class="srTable">
		<tr>
			<td class="rowLabel">Tool:</td>
         	<td class="rowData"><input type="text" id="tool" name="tool" class="tooltip" <?php if($view){ echo "readonly";} ?> value="<?php if($showdata){ echo $tool[0]['tool'];} ?>" /></td>
          	<td class="rowLabel">Manufacturer:</td>
          	<td class="rowData"><input type="text" id="mfg" name="mfg" class="tooltip" <?php if($view){ echo "readonly";} ?> value="<?php if($showdata){ echo $tool[0]['mfg'];} ?>" /></td>
        </tr>
        <tr>
         	<td class="rowLabel">Model Number:</td>
         	<td class="rowData"><input type="text" name="model" id="model" class="tooltip" <?php if($view){ echo "readonly";} ?> value="<?php if($showdata){ echo $tool[0]['model'];} ?>" /></td>
         	<td class="rowLabel">Serial Number:</td>
         	<td class="rowData"><input type="text" name="serial" id="serial" class="tooltip" <?php if($view){ echo "readonly";} ?> value="<?php if($showdata){ echo $tool[0]['serial'];} ?>" /></td>
        </tr>
		<tr>
         	<td class="rowLabel">Office:</td>
			<?php
				$sql="SELECT * FROM misc_offices;";
				$resultOffices = $mysqli->query($sql);
			?>
         	<td class="rowData"><select id="office" name="office" class="chooser tooltip" <?php if($view){ echo "disabled";} ?>>
				<option value="0"></option>
				<?php while($rowOffice = $resultOffices->fetch_assoc()){
					echo "<option value='".$rowOffice['id']."'";
					if($tool[0]['office'] == $rowOffice['id']){
						echo " selected ";	
					}
					echo ">".$rowOffice['office']."</option>";
				}?>
			</select></td>
         	<td class="rowLabel">Location Info:</td>
         	<td class="rowData"><input type="text" name="location" id="location" class="tooltip" <?php if($view){ echo "readonly";} ?> value="<?php if($showdata){ echo $tool[0]['location'];} ?>" /></td>
        </tr>
		<tr>
         	<td class="rowLabel">Tool User:</td>
         	<td class="rowData"><select id="user" name="user" class="chooser tooltip" <?php if($view){ echo "disabled";} ?>>
				<option value="unassigned"></option>
				<?php foreach($users as $uid=>$data){
					echo "<option value='".$uid."'";
					if($tool[0]['uid'] == $uid){
						echo " selected ";	
					}
					echo ">".$data['name']."</option>";
				}?>
			</select></td>
         	<td>&nbsp;</td>
			<td>&nbsp;</td>
        </tr>
		<tr>
          	<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
        </tr>
		<tr>
			<td class="rowLabel">Active:</td>
         	<td class="rowData"><select id="active" name="active" class="chooser tooltip" <?php if($view){ echo "disabled";} ?>>
				<option value="Y" <?php if($showdata){if(strtolower($tool[0]['active']) == 'y'){echo 'selected';}} ?> >Yes</option>
				<option value="N" <?php if($showdata){if(strtolower($tool[0]['active']) == 'n'){echo 'selected';}} ?> >No</option>
			</select></td>
          	<td class="rowLabel">Out for Calibration:</td>
         	<td class="rowData"><select id="calibrating" name="calibrating" class="chooser tooltip" <?php if($view){ echo "disabled";} ?>>
				<option value="Y" <?php if($showdata){if(strtolower($tool[0]['calibrating']) == 'y'){echo 'selected';}} ?> >Yes</option>
				<option value="N" <?php if($showdata){if(strtolower($tool[0]['calibrating']) == 'n'){echo 'selected';}}else{echo "selected";} ?> >No</option>
			</select></td>
        </tr>
	</table>
</div>
<?php if(count($tool) > 0){ ?>
	<div id="srDataDiv">
		<table id="srDataTable" class="srTable">
			<tr>
				<td class="rowLabel">Calibrated:</td>
				<td class="rowData"><?php if($showdata){ echo date(phpdispfd,strtotime($tool[0]['date']));} ?></td>
				<td class="rowLabel">Calibration Expire:</td>
				<td class="rowData"><?php if($showdata){ echo date(phpdispfd,strtotime($tool[0]['expire']));} ?></td>
			</tr>
			<tr>
				<td class="rowLabel">Calibrated By:</td>
				<td class="rowData"><?php if($showdata){ echo $tool[0]['calibrated_by'];} ?></td>
				<td class="rowLabel">Calibration Notes:</td>
				<td class="rowData"><?php if($showdata){ echo $tool[0]['cal_notes'];} ?></td>
			</tr>
		</table>
	</div>
<?php } ?>
<?php if(isset($_SESSION['perms']['perm_tool_cal_edit']) and $edit){ ?>
	<div id="srDataDiv">
		<div id="cal_btn">
			<table id="srDataTable" class="srTable">
				<tr>
					<td class="rowData"><div class="srBottomBtn"><a class="button_jquery_edit" onClick="calibrate();">Input Calibration</a></div></td>
				</tr>
			</table>
		</div>
		<div id="cal_input" style="display:none">
			<table id="srDataTable" class="srTable">
				<tr>
					<td class="rowLabel">Calibrated:</td>
					<td class="rowData"><input class="date" type="text" name="date" id="date" value="" /></td>
					<td class="rowLabel">Calibration Expire:</td>
					<td class="rowData"><input class="date" type="text" name="expire" id="expire" value="" /></td>
				</tr>
				<tr>
					<td class="rowLabel">Calibrated By:</td>
					<td class="rowData"><input class="autocomp" type="text" name="calibrated_by" id="calibrated_by" value="" /></td>
					<td class="rowLabel">Calibration Notes:</td>
					<td class="rowData"><input type="text" name="cal_notes" id="cal_notes" maxlength="1000" value="" /></td>
				</tr>
			</table>
		</div>
	</div>
<?php } ?>
<div id="srServiceDiv">
    <table id="srServiceTable" class="srTable"  >
        <tr>
          <td class="rowLabel">Notes<span id="notes_lbl" class="tooltip"></span></td>
        </tr>
        <tr>
          <td class="rowData"><textarea class="text_toggle" style="height:15px;" id="notes" name="notes" <?php if($view){ echo "readonly";} ?> maxlength="4000" ><?php if($edit){echo addslashes(htmlspecialchars_decode($tool[0]['notes']));}?></textarea></td>
        </tr>
    </table>
</div>

<div style="display:none">
	<?php if($debug){ ?><input name="debug" id="debug" type="hidden" value="Y" /><?php } ?>
	<?php if($edit){ ?><input type="text" id="edit" name="edit" value="Y" /><?php } ?>
    <input type="text" id="user_id" name="user_id" value="<?php echo $_SESSION['login']; ?>" /> 
	<input type="text" id="unique_id" name="unique_id" value="<?php echo $unique_id; ?>" />	
	<input type="checkbox" id="new_cal" name="new_cal" value="Y" />	
	<input type="checkbox" id="new_tool" name="new_tool" value="Y" />	
</div>
</div>
</form>
<?php if(isset($_SESSION['perms']['perm_tool_cal_edit'])){ ?>
	<div id="srFooterDiv">
		<?php if($edit){ ?>
			<div class="srBottomBtn"><a class="button_jquery_save" onClick="submitcheck('save')">Save Changes</a></div>
		<?php }elseif(!$showdata){ ?>
			<div class="srBottomBtn"><a class="button_jquery_save" onClick="submitcheck()">Create New Tool</a></div>
		<?php } ?>
	</div>
	<br>
	<br>
<?php } ?>
<?php if(count($tool) > 0){ ?>
	<div id="srHeaderDiv">
		<h1>History</h1>
		<br />
	</div>
	<div id="srDataDiv">
		<table width="100%" id="allHistTable">
			<thead>
				<tr>
					<th>Calibrated</th>
					<th>Expire</th>
					<th>Cal'ed By</th>
					<th>Cal Notes</th>
				</tr>
			</thead>
			<tbody>
				<?php
					foreach($tool as $hist){
						echo "<tr>\n";	
						echo "<td>". date(phpdispfd,strtotime($hist['date']))."</td>\n";
						echo "<td>". date(phpdispfd,strtotime($hist['expire']))."</td>\n";
						echo "<td>". $hist['calibrated_by']."</td>\n";
						echo "<td>". $hist['cal_notes']."</td>\n";
						echo "</tr>\n";
					}
					?>     
			</tbody>
		</table>
	</div>
<?php } ?>
</div> 
</div>
<?php require_once($footer_include); ?>