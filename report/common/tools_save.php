<?php 
//Update Completed 3/11/205

$debug = false;
if(isset($_POST['debug'])){
	$debug = true;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

d($_POST);


$unique_id = $_POST['unique_id'];
$action_string = "";


if(isset($_POST['new_tool'])){
	$action_string = "created";	
	new_tool();
}

if(isset($_POST['new_cal'])){
	$action_string = "updated";
	new_cal();
}

if(isset($_POST['edit'])){
	$action_string = "updated";	
	update();
}

function new_tool(){
	global $mysqli, $log, $_POST, $unique_id;
	
	$sql="INSERT INTO tools (active, calibrating, tool, model, serial, mfg, office, location, uid, notes, created_by, created_date, unique_id) 
	VALUES('".$_POST['active']."','".$_POST['calibrating']."','".addslashes($_POST['tool'])."','".addslashes($_POST['model'])."','".addslashes($_POST['serial'])."','".addslashes($_POST['mfg'])."',
	'".$_POST['office']."','".addslashes($_POST['location'])."','".$_POST['user']."','".addslashes($_POST['notes'])."','".$_POST['user_id']."','".date(storef,time())."','".$unique_id."');";
	s($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
}

function new_cal(){
	global $mysqli, $log, $_POST, $unique_id;
	
	$sql="INSERT INTO tools_calibrations (`date`, expire, calibrated_by, notes, input_by, input_date, unique_id) 
	VALUES('".date(storef,strtotime($_POST['date']))."','".date(storef,strtotime($_POST['expire']))."','".addslashes($_POST['calibrated_by'])."','".addslashes($_POST['cal_notes'])."',
	'".$_POST['user_id']."','".date(storef,time())."','".$unique_id."');";
	s($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
}

function update(){
	global $mysqli, $log, $_POST, $unique_id;
	
	$sql="UPDATE tools SET
	tool = '".addslashes($_POST['tool'])."',
	model = '".addslashes($_POST['model'])."',
	serial = '".addslashes($_POST['serial'])."',
	mfg = '".addslashes($_POST['mfg'])."',
	office = '".$_POST['office']."',
	location = '".addslashes($_POST['location'])."',
	notes = '".addslashes($_POST['notes'])."',
	uid = '".$_POST['user']."',
	active = '".$_POST['active']."',
	calibrating = '".$_POST['calibrating']."',
	edited_by = '".$_POST['user_id']."',
	edited_date = '".date(storef,time())."'
	WHERE unique_id = '$unique_id';";
	s($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);	
	}
}

?>
<!DOCTYPE html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<?php require_once($js_include);?>

<script type="text/javascript">
function delayer(){
    <?php if(!$debug){ ?>
	window.location = "<?php echo $refering_uri; ?>"
	<?php } ?>
}
</script>
</head>
<body onLoad="setTimeout('delayer()',3000 )">
<?php require_once($header_include); ?>
<div id="OIReportContent"> 
	<h1>Tool has been <?php echo $action_string; ?></h1>
	<br />
	<h1><span class="red">Page will return to home page in 3 seconds</span></h1>
</div>
<?php require_once($footer_include); ?>