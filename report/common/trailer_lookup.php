<?php 

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

$sql = "SELECT t.trailer_id, t.nickname, t.mfg, t.vin
	FROM trailers AS t
	WHERE t.property = 'C' AND t.assigned = 'n'
	ORDER BY t.trailer_id ASC;";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

?>
<!doctype html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<style>
.dataTable th, .dataTable td {
	max-width: 200px;
	min-width: 70px;
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
}
</style>
<?php require_once($js_include);?>

<script type="text/javascript">
$(document).ready(function() {
	allTable = $('#allTable').dataTable({
		"bJQueryUI": true,
		"bStateSave": false,
		"sPaginationType": "full_numbers",
		"fnInitComplete": function () {
			 this.$('tr').click( function () {
				 var href = $(this).find("a").attr("href");
				 if(href) {
					window.location = href;
				 }
			 });
		},
		"fnDrawCallback" : function(oSettings) {
			var total_count = oSettings.fnRecordsTotal();
			var columns_in_row = $(this).children('thead').children('tr').children('th').length;
			var show_num = oSettings._iDisplayLength;
			var tr_count = $(this).children('tbody').children('tr').length;
			var missing = show_num - tr_count;
			if (show_num < total_count && missing > 0){
				for(var i = 0; i < missing; i++){
					$(this).append('<tr class="space"><td colspan="' + columns_in_row + '">&nbsp;</td></tr>'); 
				}
			}
			if (show_num > total_count) {
				for(var i = 0; i < (total_count - tr_count); i++) {
					$(this).append('<tr class="space"><td colspan="' + columns_in_row + '">&nbsp;</td></tr>'); 
				}
			}
		}
	});	
});
function select_id(id){
	console.log("ID: " + id);
	$('#trailer_id',top.document).val(id);
	parent.$.fancybox.close();
}
</script>
</head>
<body>
 <table width="100%" id="allTable">
                <thead>
                    <tr>
                        <th width="15%">Trailer ID</th>
						<th>Nickname</th>
                        <th>Manufacturer</th>
                        <th>VIN</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
						while($row = $result->fetch_assoc())
						{
							echo "<tr onclick=\"javascript: select_id(".$row['trailer_id'].");\">\n";	
							echo "<td>". $row['trailer_id']."</td>\n";
							echo "<td>". $row['nickname']."</td>\n";
							echo "<td>". $row['mfg']."</td>\n";
							echo "<td>". $row['vin']."</td>\n";
							echo "</tr>\n";
						}
						?>     
                </tbody>
            </table>
</body>
</html>