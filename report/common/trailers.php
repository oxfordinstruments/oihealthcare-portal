<?php

$debug = false;
if(isset($_GET['debug'])){
	$debug = true;
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

if(isset($_GET['e'])) {
	$edit = true;
	if(isset($_GET['unique_id'])) {
		$unique_id=$_GET['unique_id'];
	}else if(isset($_GET['customer_id'])){
		$sql="SELECT unique_id FROM trailers WHERE trailer_id = '".$_GET['trailer_id']."';";
		if(!$resultId = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
		$rowId = $resultId->fetch_assoc();
		$unique_id = $rowId['unique_id'];
	}else{
		$log->logerr('Blank Customer UID',1055,true,basename(__FILE__),__LINE__);
	}
}else{
	$edit = false;	
	$unique_id = md5(uniqid());
	
	$sql="SELECT MAX(CAST(TRIM(LEADING 'tr' FROM lower(trailer_id)) AS UNSIGNED)) + 1 AS next_id FROM trailers;;";
	if(!$resultNextTrailerID = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$rowNextTrailerID = $resultNextTrailerID->fetch_assoc();
}

$uploadBtnValue = "Attach Documents";

$sql="SELECT * FROM misc_states;";
if(!$resultStates= $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$states = array();
while($rowStates = $resultStates->fetch_assoc()){
	$states[$rowStates['abv']] = $rowStates['name'];	
}

$sql = "SELECT t.*, sb.system_id
FROM trailers AS t
LEFT JOIN systems_base AS sb ON sb.unique_id = t.system_unique_id
WHERE t.unique_id = '".$unique_id."';";
if(!$resultT = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowT = $resultT->fetch_assoc();
d($rowT);

if($edit){
	if(strtolower($rowT['has_files']) == 'y'){
		$uploadBtnValue = "View/Modify Documents";	
	}
}

?>
<!DOCTYPE html>
<html>
<head>
<?php require_once($head_include);?>
<?php require_once($css_include);?>
<?php require_once($js_include);?>
<script src="/resources/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
$(document).ready(function() {
 		
$( "#tabs" ).tabs({ 
<?php if(!$edit){ ?>	disabled: [ 3 ] <?php } ?>
});

/////////////////////////////////////////////////////////////////////////////////////		
		<?php
		if(strtolower($_SESSION['prefs']['pref_show_help']) != 'y' or $_SESSION['mobile_device'] == true){
		?>
		$('#tiplayer').hide()
		<?php
			}
		?>

	
	$(".button_jquery_upload").button({
		icons: {
			primary: "ui-icon-folder-open"
		}
	});
	
	$(".button_jquery_save").button({
		icons: {
			primary: "ui-icon-disk"
		}
	});
	
	$(".button_jquery_lookup").button({
		icons: {
			primary: "ui-icon-search"
		}
	});
	
	$("select").chosen({
		no_results_text: "Oops, nothing found!",
		disable_search_threshold: 10,
		placeholder_text_single: '  ',
		search_contains: true
	}); 
	$("div .chosen-container").each(function(index, element) {
		if($(this).attr('style') =='width: 0px;'){
			$(this).removeAttr('style');
			$(this).css('display','block');
			
		}
	});

});

	$(function() {
		$( "#license_expire" ).datepicker({
			numberOfMonths: 1,
			showButtonPanel: true,
			changeYear: true,
			duration: "fast",
			dateFormat: "<?php echo dpdispfd; ?>"
		});
		/////////////////////////////////////////////////////////////////////////////////////
		$(".iframeUpload").fancybox({
				'type'			: 'iframe',
				'height'		: 600,
				'fitToView'		: true,
				'maxWidth'		: 900,
				'maxHeight'		: 600,
				'autoSize'		: false,
				'closeBtn'		: true,
				'margin'		: [5,5,5,5]
		});
		
		$("#archived").change(function(){
			if(document.getElementById("archived").value == "Y"){
				$.prompt("<h3>Archiving a trailer will prevent it from being assigned to a system.<br>"+
				"The assigned system will be unassigned from this trailer.<br>"+
				"The assigned system's mobile status will be set to NO.</h3>",{
					title: "Caution"
				});
				//alert("Archiving a facility will un-assign all users assigned to this system, and disable customer access to this system!");	
			}
		});	
	});

function submitcheck()
{
	var debug = <?php echo($debug)?"true":"false"; ?>;
	var edit = <?php echo($edit)?"true":"false"; ?>;
	ids = [];
	tabs = [];
	errors = [];
	
	if($("#lblTrailerId").html().indexOf("!") >= 0){ids.push("#trailer_id");tabs.push("#li-tab-10");}	
	if($("#trailer_id").val()==""){ids.push("#trailer_id");tabs.push("#li-tab-10");}
	if($("#nickname").val()==""){ids.push("#nickname");tabs.push("#li-tab-10");}
	if($("#mfg").val()==""){ids.push("#mfg");tabs.push("#li-tab-10");}
	if($("#mfg_year").val()==""){ids.push("#mfg_year");tabs.push("#li-tab-10");}
	if($("#vin").val()==""){ids.push("#vin");tabs.push("#li-tab-10");}

	var regex = new RegExp(/[\'@\"\$\\\#\+\?]/g);
	if(document.getElementById("nickname").value.match(regex)){ids.push("#nickname");errors.push("Nickname contains invalid characters  ' @ $ \ # + ? ");tabs.push("#li-tab-10");}

	$("#notes").val(ConvChar($("#notes").val()));
			
	//if(debug){
		console.log("ids: " + ids);
		console.log("tabs: " + tabs);
		console.log("errors: " + errors);
	//}
	
	console.log("Archived:" + $("#archived").val());
	if(edit){
		if($("#archived").val().toLowerCase()=="n"){
			showErrors(ids,errors,tabs);
		}else{
			while(ids.length > 0) {
				ids.pop();
			}	
		}
	}else{
		showErrors(ids,errors,tabs);
	}
	
	if(ids.length <= 0){
		$.prompt("<h3><?php if($edit){echo "Update";}else{echo "Add New";} ?> Trailer?</h3>",{
			title: "Question",
			buttons: { Yes: 1, No: -1 },
			focus: 1,
			submit:function(e,v,m,f){ 
				if(v == 1){
					$.prompt.close();
					<?php if($debug){?>
						if(!confirm('DEBUG: really submit?!')){
							return;	
						}
					<?php } ?>
					document.forms['form'].submit();
				}else{
					$.prompt.close();	
				}
				e.preventDefault();
			}
		});
	}

}

/////////////////////////////////////////////////////////////////////////////////////
function ConvChar( str ) {
  c = {'<':'&lt;', '>':'&gt;', '&':'&amp;', '"':'&quot;', "'":'&#039;',
       '#':'&#035;', '@':'&#64;' };
  return str.replace( /[<&>'"#@]/g, function(s) { return c[s]; } );
}

/////////////////////////////////////////////////////////////////////////////////////
function showErrors(ids,errors,tabs){
	if(tabs.length > 0){
		errors.unshift("<h1><b>Correct the red highlighted fields</b></h1>");
	}
	$("input, select, .chosen-container").each(function(index, element) { //set all inputs to not highlighted
		$(this).animate({
			borderColor: "#2C3594",
			boxShadow: 'none'
		});
	});
	
	$("#tabs > ul > li").each(function(index, element) { //set all inputs to not highlighted
		$(this).animate({
			borderColor: "#2C3594",
			boxShadow: 'none'
		});
	});
	
	$.each(ids,function(index,value){//highlight inputs
		$(value).animate({
			borderColor: "#cc0000",
			boxShadow: '0 0 5px 3px rgba(255,0,0,0.4)'
		});
	});
	
	$.each(tabs,function(index,value){ //highlight tabs
		$(value).animate({
			borderColor: "#cc0000",
			boxShadow: '0 0 5px 3px rgba(255,0,0,0.4)'
		});
	});
	
	$("#errors > span").html("");
	$.each(errors,function(index, value){
		$("#errors > span").append(value + "<br>");
	});
	$("#errors").show('slow');
	//$(document).scrollTop(0);
}
/////////////////////////////////////////////////////////////////////////////////////////////////
function checkTrailer(str){
	if (str==""){
	  return;
	}
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}else{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			if(str != "<?php echo $rowT['trailer_id']; ?>"){
				document.getElementById("lblTrailerId").innerHTML=xmlhttp.responseText;
			}else{
				document.getElementById("lblTrailerId").innerHTML="Trailer ID";	
			}
		}
	}
	xmlhttp.open("GET","scripts/check_trailer_id_edit.php?q="+str,true);
	xmlhttp.send();
}
/////////////////////////////////////////////////////////////////////////////////////


</script>
</head>
<body>
<?php require_once($header_include); ?>
<div id="OIReportContent">
	<div id="styledForm">
		<form id="form" name="form" method="post" autocomplete="off" action="trailers_do.php">
			<?php if($edit){ ?>
			<h1>Editing Trailer: <?php echo $rowT['nickname'];?></h1>
			<span class="red">Required fields marked with an *</span>
			<?php }else{ ?>
			<h1>Create New Trailer </h1>
			<span class="red">Required fields marked with an *</span>
			<?php } ?>
			<p>&nbsp;</p>
			<div id="errors" style="text-align:center;display:none"> 
				<span style="color:#F00"> </span> 
			</div>

			<?php if($edit){ ?>
			<table width="100%" cellpadding="5" cellspacing="5">
				<tr>
					<td width="30%"><label>Archived</label>
						<select name="archived" id="archived">
							<option value="Y"<?php if($edit){if(strtolower($rowT['property'])=="a"){echo " selected";}} ?>>Yes</option>
							<option value="N"<?php if($edit){if(strtolower($rowT['property'])=="c"){echo " selected";}}else{echo " selected";} ?>>No</option>
						</select></td>
					<td>&nbsp;</td>
				</tr>
			</table>
			<?php } ?>
			<div class="line"></div>
<div id="tabs">
	 <ul>
		<li id="li-tab-10"><a href="#tab-10">Trailer</a></li>
		<li id="li-tab-40"><a href="#tab-40">Assigned System</a></li>
		<li id="li-tab-70"><a href="#tab-70">Notes</a></li>
		<li id="li-tab-80"><a href="#tab-80">Docs</a></li>
	</ul>

<div id="tab-10" class="tab">
			<!--Facility-->
			<table width="100%" cellpadding="5" cellspacing="5">
				<tr>
					<td width="20%"><label id="lblTrailerId">Trailer ID <span class="red">*</span></label>
						<input type="text" name="trailer_id" id="trailer_id" onblur="checkTrailer(this.value)" value="<?php if($edit){echo $rowT['trailer_id'];}else{echo "TR",$rowNextTrailerID['next_id'];} ?>"/></td>
					<td><label>Nickname <span class="red">*</span></label>
						<input type="text" name="nickname" id="nickname" value="<?php if($edit){echo $rowT['nickname'];} ?>"/></td>
				</tr>
			</table>
			<table width="100%" cellpadding="5" cellspacing="5">
				<tr>
					<td><label>Manufacturer <span class="red">*</span></label>
						<input type="text" name="mfg" id="mfg" value="<?php if($edit){echo $rowT['mfg'];} ?>" /></td>
					<td width="20%"><label>Year Manufactured <span class="red">*</span></label>
						<input type="text" name="mfg_year" id="mfg_year" value="<?php if($edit){echo $rowT['mfg_year'];} ?>" /></td>
					<td><label>VIN <span class="red">*</span></label>
						<input type="text" name="vin" id="vin" value="<?php if($edit){echo $rowT['vin'];} ?>" /></td>
				</tr>
			</table>
			<table width="100%" cellpadding="5" cellspacing="5">
				<tr>
					<td><label>License Number</label>
						<input type="text" name="license_number" id="license_number" value="<?php if($edit){echo $rowT['license_number'];} ?>" /></td>
					<td width="16%"><label>Licensed State</label>
						<select name="license_state" id="license_state">
							<option value=""></option>
							<?php
								foreach($states as $state_key=>$state_name){
									echo "<option value='" . $state_key . "'";
									if($edit){
										if($rowT['license_state']==$state_key){echo " selected";}
									}
									echo">" . $state_name . "</option>\n";
								}
							  ?>
						</select></td>
					<td width="20%"><label>License Expire</label>
						<input type="date" name="license_expire" id="license_expire" value="<?php if($edit){echo date(phpdispfd,strtotime($rowT['license_expire']));} ?>" /></td>
				</tr>
			</table>
			<table width="100%" cellpadding="5" cellspacing="5">
				<tr>
					<td width="50%"><label>Trailer Pop-Outs</label>
						<select name="pop_outs" id="pop_outs">
							<option value="Y"<?php if($edit){if(strtolower($rowT['pop_outs'])=="y"){echo " selected";}} ?>>Yes</option>
							<option value="N"<?php if($edit){if(strtolower($rowT['pop_outs'])=="n"){echo " selected";}}else{echo " selected";} ?>>No</option>
						</select></td>
					<td><label>Trailer Size</label>
						<input type="text" name="size" id="size" value="<?php if($edit){echo $rowT['size'];} ?>" /></td>
				</tr>
			</table>

</div>

<div id="tab-40" class="tab">
			<!--System-->
			<table width="100%" cellpadding="5" cellspacing="5">
				<tr>
					<td width="50%"><label>Assigned System</label>
						<input type="text" name="system_id" id="system_id" value="<?php if($edit){echo $rowT['system_id'];} ?>" /></td>
					<td width="50%" style="text-align:center;"><a name="lookup_system" class="iframeView button_jquery_lookup" href="system_lookup.php">Lookup System</a></td>
				</tr>
			</table>
</div>

<div id="tab-70" class="tab">
			<!--Notes-->
			<table width="100%" cellpadding="5" cellspacing="5">
				<tr>
					<td width="70%"><label>Important Trailer Notes</label>
						<textarea name="notes" id="notes"><?php if($edit){echo $rowT['notes'];} ?></textarea>
					</td>
				</tr>
			</table>
</div>


<div id="tab-80" class="tab">
			<!--Docs-->
			<div id="uploadDocs">
				<a name="uploadX" class="iframeUpload button_jquery_upload"  href="/resources/kcfinder/kcfinder.php?unique_id=<?php echo $unique_id; ?>&table=trailers_files"><?php echo $uploadBtnValue; ?></a>
			</div>

</div>

</div>
			<div class="line"></div>
			<table width="100%" cellpadding="5" cellspacing="5">
				<tr>
					<td width="50%"><a name="Submit" class="button_jquery_save" onclick="submitcheck()"><?php if(!$edit){echo "Create New Trailer";}else{ echo "Save Trailer Changes";} ?></a></td>
				</tr>
			</table>
			<div style="display:none">
				<input name="edit_trailer" id="edit_trailer" type="hidden" value="<?php if($edit){echo "Y";}else{echo "N";} ?>" />
				<input name="unique_id" id="unique_id" type="text" value="<?php echo $unique_id; ?>" />
				<?php if($debug){ ?><input name="debug" id="debug" type="hidden" value="Y" /><?php } ?>
			</div>

			<br />
			<br />
		</form>
		
	</div>
</div>
<?php require_once($footer_include); ?>
