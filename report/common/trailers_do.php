<?php

$debug = false;
if(isset($_POST['debug'])){
	$debug = true;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

d($_POST);

if(!isset($_POST['unique_id'])){
	$log->logerr('Blank Unique Id',1016,true,basename(__FILE__),__LINE__);	
}else{
	$unique_id=$_POST['unique_id'];	
}

if(strtolower($_POST['edit_trailer']) == "y"){
	$edit = true;	
}else{
	$edit = false;
}



if(strtolower($_POST['archived'])=="y"){
	$sql="SELECT system_unique_id FROM trailers WHERE unique_id = '$unique_id';";
	d($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$rowSystemUID = $result->fetch_assoc();
	
	$sql="UPDATE trailers AS t
	SET t.property = 'A', t.assigned = 'N', t.system_unique_id = NULL
	WHERE t.unique_id = '$unique_id';";
	d($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	
	$sql="UPDATE systems_base AS sb
	SET sb.mobile = 'N', sb.trailer_unique_id = NULL
	WHERE sb.unique_id = '".$rowSystemUID['system_unique_id']."';";
	d($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	
	$log->loginfo($_POST['trailer_id'].' - '.$unique_id,104,false,basename(__FILE__),__LINE__);
}else{
	
	$sql="SELECT system_unique_id FROM trailers WHERE unique_id = '$unique_id';";
	d($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$rowCurrentSystemUID = $result->fetch_assoc();
	d($rowCurrentSystemUID);
		
	$sql="SELECT unique_id FROM systems_base WHERE system_id = '".$_POST['system_id']."';";
	d($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$rowNewSystemUID = $result->fetch_assoc();
	d($rowNewSystemUID);
	
	$license_expire = '';
	if($_POST['license_expire'] != ''){
		$license_expire = date(storef, strtotime($_POST['license_expire']));
	}
	
	$archived = 'C';
	if(strtolower($_POST['archived']) == 'y'){
		$archived = 'A';	
	}
	
	$assigned = 'N';
	if($_POST['system_id'] != ''){
		$assigned = 'Y';	
	}
	
	$pop_outs = 'N';
	if(strtolower($_POST['pop_outs']) == 'y'){
		$pop_outs = 'Y';	
	}
	
	$values = array(
		'trailer_id' => $_POST['trailer_id'], 
		'nickname' => addslashes($_POST['nickname']), 
		'mfg' => addslashes($_POST['mfg']),
		'mfg_year' => addslashes($_POST['mfg_year']),
		'vin' => addslashes($_POST['vin']),
		'license_number' => addslashes($_POST['license_number']),
		'license_state' => $_POST['license_state'],
		'license_expire' => date(storef,strtotime($_POST['license_expire'])),
		'pop_outs' => $pop_outs,
		'size' => addslashes($_POST['size']),
		'system_unique_id' => $rowNewSystemUID['unique_id'],
		'notes' => addslashes($_POST['notes']), 
		'archived' => $archived,
		'assigned' => $assigned,
		'unique_id' => $unique_id,
		'created_by' => $myusername,
		'created_date' => date(storef,time())
	);
	d($values);
	
	$sql="INSERT INTO trailers (trailer_id, nickname, mfg, mfg_year, vin, license_number, license_state, license_expire, pop_outs, size, system_unique_id, notes,
		property, assigned,	unique_id, created_by, created_date)
		VALUES ('". implode("', '", $values) . "') ON DUPLICATE KEY
		UPDATE trailer_id = VALUES(trailer_id), nickname = VALUES(nickname), mfg = VALUES(mfg), mfg_year = VALUES(mfg_year), 
		vin = VALUES(vin), license_number = VALUES(license_number), license_state = VALUES(license_state), license_expire = VALUES(license_expire),
		pop_outs = VALUES(pop_outs), size = VALUES(size), system_unique_id = VALUES(system_unique_id), notes = VALUES(notes), property = VALUES(property), 
		assigned = VALUES(assigned), unique_id = VALUES(unique_id), edited_by = '".$myusername."',  edited_date = '".date(storef,time())."';";
	
	d($sql);
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	
	if(strtolower($assigned) == 'y'){
		$sql="UPDATE systems_base SET mobile = 'N', trailer_unique_id = NULL WHERE unique_id = '".$rowCurrentSystemUID['system_unique_id']."';";	
		d($sql);
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
		$sql="UPDATE systems_base SET mobile = 'Y', trailer_unique_id = '$unique_id' WHERE unique_id = '".$rowNewSystemUID['unique_id']."';";	
		d($sql);
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
	}else{
		if($edit){
			$sql="UPDATE systems_base SET mobile = 'N', trailer_unique_id = NULL WHERE unique_id = '".$rowCurrentSystemUID['system_unique_id']."';";	
			d($sql);
			if(!$result = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
				$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
			}	
		}
	}
			
	if($edit){
		$log->loginfo($_POST['trailer_id'].' - '.$unique_id,106,false,basename(__FILE__),__LINE__);	
	}else{
		$log->loginfo($_POST['trailer_id'].' - '.$unique_id,105,false,basename(__FILE__),__LINE__);
	}
	
}//else archived

?>
<!DOCTYPE html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<?php require_once($js_include);?>

<script type="text/javascript">
function delayer(){
    <?php if(!$debug){ ?>window.location = "<?php echo $refering_uri; ?>"<?php } ?>
}
</script>
</head>
<body onLoad="setTimeout('delayer()', 3000)">
<?php require_once($header_include); ?>
<div id="OIReportContent">
	<h1>Trailer <?php if($edit){if(strtolower($_POST['archived']) == 'y'){echo "Archived";}else{echo "Updated";}}else{echo "Created";} ?> Successfully</h1><br>
	Trailer ID: <?php echo $_POST['trailer_id']; ?><br>
	Trailer Nickname: <?php echo $_POST['nickname']; ?><br><br>
    <h1>Page will return to main page in 3 seconds</h1>
</div>
<?php require_once($footer_include); ?>

