<?php
//Update Completed 11/26/14
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

$debug = false;

if(!isset($_GET['q'])){
	if($debug){
		echo "New role not set!",EOL;
		die("ERROR");
	}else{
		$log->logerr('user_switch_role.php',1032,true,basename(__FILE__),__LINE__);
	}
}

$newRole = $_GET['q'];
if($debug){echo "New Role: ".$newRole,EOL;}

$sql="SELECT * FROM users WHERE uid = '".$_SESSION['login']."' LIMIT 1;";
if($debug){echo $sql,EOL;}
if(!$resultNewRole = $mysqli->query($sql)){
	if($debug){
		echo "Could not lookup user!",EOL;
		die("ERROR");
	}else{
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		header("location:/error.php?n=1033&t=Could not lookup user&p=user_switch_role.php");
	}
}

if($resultNewRole->num_rows < 1){
	if($debug){
		echo "User does not exist!",EOL;
		die("ERROR");
	}else{
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('User does not exist '.$_SESSION['login'],1033,false,basename(__FILE__),__LINE__);
		header("location:/error.php?n=1033&t=User does not exist&p=user_switch_role.php");
	}	
}

$rowNewRole = $resultNewRole->fetch_assoc();

$sql="SELECT rid.*
FROM users_roles AS ur
LEFT JOIN users_role_id AS rid ON rid.id = ur.rid
WHERE ur.uid = '".$_SESSION['login']."';";
if($debug){echo $sql,EOL;}
if(!$resultRolesLst = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

if($resultRolesLst->num_rows < 1){
	if($debug){
		echo "Roles list is empty!",EOL;
		die("ERROR");
	}else{
		$log->logerr('Roles list is empty: '.$_SESSION['login'],1034,false,basename(__FILE__),__LINE__);
		header("location:/error.php?n=1034&t=Roles list is empty&p=user_switch_role.php");
	}	
}

$rolesLstArr = array();
while($rowRolesList = $resultRolesLst->fetch_assoc()){
	$rolesLstArr[$rowRolesList['id']] = array('role'=>$rowRolesList['role'],'location'=>$rowRolesList['location']);
}

if($debug){echo "Roles:<pre>",print_r($rolesLstArr),"</pre>",EOL;}

if(!array_key_exists($newRole,$rolesLstArr)){
	if($debug){
		echo "Invalid new role id!",EOL;
		die("ERROR");
	}else{
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Invalid new role id: '.$_SESSION['login'],1034,false,basename(__FILE__),__LINE__);
		header("location:/error.php?n=1034&t=Invalid new role id&p=user_switch_role.php");
	}		
}



if(!array_key_exists($rolesLstArr[$newRole]['role'],$_SESSION['roles'])){
	if($debug){
		echo "Role not allowed!",EOL;
		die("ERROR");
	}else{
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Role not allowed: '.$_SESSION['login'],1034,false,basename(__FILE__),__LINE__);
		header("location:/error.php?n=1034&t=Role not allowed&p=user_switch_role.php");
	}		
}

$_SESSION['role'] = $newRole;
$_SESSION['loc'] = $rolesLstArr[$newRole]['location'];
$_SESSION['current_role'] = $rolesLstArr[$newRole]['role'];


if($debug){
	echo "Redirect to: ",$_SESSION['loc'],EOL;
	die("DONE");
}else{
	header("location:".$_SESSION['loc']."/");
}	

?>