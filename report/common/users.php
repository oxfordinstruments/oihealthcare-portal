<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

unset($_SESSION['user_refresh_prevent']);

$debug=false;
if(isset($_GET['debug'])){
	$debug=true;
	if(isset($_GET['send'])){$send = true;}else{$send = false;}
}

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

$sql="SELECT * FROM misc_states;";
if(!$resultStates= $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$states = array();
while($rowStates = $resultStates->fetch_assoc()){
	$states[$rowStates['abv']] = $rowStates['name'];	
}

$edit = false;
$reg = false;

if(isset($_GET['regid'])){
	if(is_numeric($_GET['regid'])){
		$sql="SELECT * FROM registration_requests WHERE id = '".$_GET['regid']."' AND registered = 'n';";
		if(!$resultReg = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
		
		if($resultReg->num_rows < 1){
			$log->logerr("Registration lookup returned 0",1072,true,basename(__FILE__),__LINE__);
		}
		
		$rowReg = $resultReg->fetch_assoc();
		d($rowReg);
		$reg = true;
	
	}else{
		$log->logerr("Invalid reg id: ".$_GET['regid'],1018,true,basename(__FILE__),__LINE__);	
	}
}elseif(isset($_GET['uid'])){
	$edit = true;
	$userid = $_GET['uid'];
	$sql="CALL users_data_get('$userid');";
	if(!$resultUser = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}	
	if($resultUser->num_rows != 1){
		if($debug){
			$log->logerr('users.php',1017,false,basename(__FILE__),__LINE__);
			die("invalid uid");
		}else{
			$log->logerr('users.php',1017,true,basename(__FILE__),__LINE__);
		}
	}
	$rowUser = $resultUser->fetch_assoc();
	d($rowUser);
	
	$user_data = $rowUser;
	d($user_data);
	
	//Clear the remaining results from the above mysql call statement
	if($mysqli->more_results()){
		while ($mysqli->next_result());
	}
	
	$groups = array();
	$group = '';
	foreach($user_data as $key=>$value){
		if(preg_match("/^grp_/",$key)){
			array_push($groups,$key);
			if(strtolower($value) == "y"){
				$group = $key;	
			}
		}
	}
	d($groups);
	d($group);
	if($group == ''){	
		if($debug){
			$log->logerr('users.php',1042,false,basename(__FILE__),__LINE__);
			die("invalid group");
		}else{
			$log->logerr('users.php',1042,true,basename(__FILE__),__LINE__);
		}
	}
	
	$current_roles = array();
	foreach($user_data as $key=>$value){
		if(preg_match("/^role_/",$key) and strtolower($value) == "y"){
			array_push($current_roles,$key);	
		}
	}
	d($current_roles);
}

if($edit){
	$has_systems_assigned = false;
	if(strtolower($user_data['perm_no_assigned_systems']) != 'y' and ( strtolower($user_data['role_engineer']) == 'y' or strtolower($user_data['role_contractor']) == 'y' or strtolower($user_data['role_customer']) == 'y')){
		$has_systems_assigned = true;
		if(strtolower($user_data['grp_customer']) == 'y'){
			$sql="SELECT sbc.ver_unique_id, sbc.system_id, sbc.nickname, u1.name AS `primary`, u2.name AS `secondary`, sbc.ver, IF(sbc.property = 'F','Y','N') AS future, sbc.pre_paid, sbc.credit_hold
			FROM systems_base_cont AS sbc
			LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
			LEFT JOIN systems_assigned_pri AS pri ON pri.system_ver_unique_id = sbc.ver_unique_id
			LEFT JOIN systems_assigned_sec AS sec ON sec.system_ver_unique_id = sbc.ver_unique_id
			LEFT JOIN systems_assigned_customer AS asn ON asn.system_ver_unique_id = sbc.ver_unique_id
			LEFT JOIN users AS u1 ON pri.uid = u1.uid
			LEFT JOIN users AS u2 ON sec.uid = u2.uid
			WHERE sbc.property != 'D' AND sb.system_id != '9999' AND asn.uid = '$userid'
			ORDER BY sb.system_id ASC;";
		}else{
			$sql="SELECT sbc.ver_unique_id, sbc.system_id, sbc.nickname, u1.name AS `primary`, u2.name AS `secondary`, sbc.ver, IF(sbc.property = 'F','Y','N') AS future, sbc.pre_paid, sbc.credit_hold
			FROM systems_base_cont AS sbc
			LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
			LEFT JOIN systems_assigned_pri AS pri ON pri.system_ver_unique_id = sbc.ver_unique_id
			LEFT JOIN systems_assigned_sec AS sec ON sec.system_ver_unique_id = sbc.ver_unique_id
			LEFT JOIN users AS u1 ON pri.uid = u1.uid
			LEFT JOIN users AS u2 ON sec.uid = u2.uid
			WHERE sbc.property != 'A' AND sb.system_id != '9999' AND (pri.uid = '$userid' OR sec.uid = '$userid')
			ORDER BY sb.system_id ASC;";
		}
		d($sql);
		if(!$resultAsgnSystems = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}	
		if($resultAsgnSystems->num_rows < 1){
			if($debug){
				$log->logerr('users.php',1043,false,basename(__FILE__),__LINE__);
				s("no systems assigned");
			}else{
				$log->logerr(addslashes($userid),1043,false,basename(__FILE__),__LINE__);
			}
		}
		$assignedSystemsArr = array();
		while($rowAssignedSystems = $resultAsgnSystems->fetch_assoc()){

			$assignedSystemsArr[$rowAssignedSystems['ver_unique_id']] = array("system_id"=>$rowAssignedSystems['system_id'],"nickname"=>$rowAssignedSystems['nickname'],"primary"=>$rowAssignedSystems['primary'],"secondary"=>$rowAssignedSystems['secondary'],"version"=>$rowAssignedSystems['ver'],"future"=>$rowAssignedSystems['future'],"pre_paid"=>$rowAssignedSystems['pre_paid'],"credit_hold"=>$rowAssignedSystems['credit_hold']);
		}	
		d($assignedSystemsArr);
	}
	
	if(strtolower($user_data['grp_customer']) != 'y'){
		$sql="SELECT sbc.ver_unique_id, sbc.system_id, sbc.nickname, u1.name AS `primary`, u2.name AS `secondary`, IF(sbc.property = 'F','Y','N') AS future, sbc.pre_paid, sbc.credit_hold
		FROM systems_base_cont AS sbc
		LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
		LEFT JOIN systems_assigned_pri AS pri ON pri.system_ver_unique_id = sbc.ver_unique_id
		LEFT JOIN users AS u1 ON u1.uid = pri.uid
		LEFT JOIN systems_assigned_sec AS sec ON sec.system_ver_unique_id = sbc.ver_unique_id
		LEFT JOIN users AS u2 ON u2.uid = sec.uid
		WHERE sbc.property != 'A' AND sb.system_id != '9999' 
		AND  ( pri.uid != '$userid' OR pri.uid IS NULL )
		AND  ( sec.uid != '$userid' OR sec.uid IS NULL )
		ORDER BY sb.system_id ASC;";
	}else{
		$sql="SELECT sbc.ver_unique_id, sbc.system_id, sbc.nickname, u1.name AS `primary`, u2.name AS `secondary`, IF(sbc.property = 'F','Y','N') AS future, sbc.pre_paid, sbc.credit_hold
		FROM systems_base_cont AS sbc
		LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
		LEFT JOIN systems_assigned_pri AS pri ON pri.system_ver_unique_id = sbc.ver_unique_id
		LEFT JOIN users AS u1 ON u1.uid = pri.uid
		LEFT JOIN systems_assigned_sec AS sec ON sec.system_ver_unique_id = sbc.ver_unique_id
		LEFT JOIN users AS u2 ON u2.uid = sec.uid
		WHERE sbc.property != 'D' AND sb.system_id != '9999'
		ORDER BY sb.system_id ASC;";		
	}
}else{
	$sql="SELECT sbc.ver_unique_id, sbc.system_id, sbc.nickname, u1.name AS `primary`, u2.name AS `secondary`, IF(sbc.property = 'F','Y','N') AS future, sbc.pre_paid, sbc.credit_hold
	FROM systems_base_cont AS sbc
	LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
	LEFT JOIN systems_assigned_pri AS pri ON pri.system_ver_unique_id = sbc.ver_unique_id
	LEFT JOIN users AS u1 ON u1.uid = pri.uid
	LEFT JOIN systems_assigned_sec AS sec ON sec.system_ver_unique_id = sbc.ver_unique_id
	LEFT JOIN users AS u2 ON u2.uid = sec.uid
	WHERE sbc.property != 'A' AND sb.system_id != '9999'
	ORDER BY sb.system_id ASC;";
}
d($sql);
if(!$resultSystems = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}	
$systems_num_rows = $resultSystems->num_rows;
s($systems_num_rows);
$systemsArr = array();
while($rowSystems = $resultSystems->fetch_assoc()){
	if($rowSystems['primary'] == ''){
		$rowSystems['primary'] = 'unassinged';	
	}
	if($rowSystems['secondary'] == ''){
		$rowSystems['secondary'] = 'unassinged';	
	}
    $systemsArr[$rowSystems['ver_unique_id']] = array("system_id"=>$rowSystems['system_id'],"nickname"=>$rowSystems['nickname'],"primary"=>$rowSystems['primary'],"secondary"=>$rowSystems['secondary'],"version"=>$rowSystems['ver'],"future"=>$rowSystems['future'],"pre_paid"=>$rowSystems['pre_paid'],"credit_hold"=>$rowSystems['credit_hold']);
}
d($systemsArr);

$systems_json = "{\"rows\":[";
$systems_json_index = "\"index\":[{";
$x = 0;
foreach($systemsArr as $key_system=>$value_system){
	$systems_json.="{ \"id\":\"".$key_system."\", ";
	$systems_json_index.="\"".$key_system."\":".$x.",";
	$systems_json.="\"userdata\":{\"future\":\"".$value_system['future']."\", \"pre_paid\":\"".$value_system['pre_paid']."\", \"credit_hold\":\"".$value_system['credit_hold']."\"},";
	$systems_json.="\"data\":[\"".$value_system['system_id']."\",\"".$value_system['nickname']."\",\"".$value_system['primary']."\",\"".$value_system['secondary']."\",";
	$systems_json.="\"".$value_system['future']."\",\"".$value_system['pre_paid']."\",\"".$value_system['credit_hold']."\"]},";
	$x++;
}
$systems_json = preg_replace("/,$/","",$systems_json) . "], ".preg_replace("/,$/","",$systems_json_index)."}]}";
d($systems_json);

if($has_systems_assigned){
	$pri_systems_json = "{\"rows\":["; //also used for assigned systems for customer
	$pri_systems_json_index = "\"index\":[{"; //also used for assigned systems for customer
	$sec_systems_json = "{\"rows\":[";
	$sec_systems_json_index = "\"index\":[{";
	$x = 0;
	foreach($assignedSystemsArr as $key_system=>$value_system){		
		if(strtolower($user_data['grp_customer']) == 'y'){
			//$pri_systems_json.="{ \"id\":".$key_system.", \"data\":[\"".$value_system['system_id']."\",\"".$value_system['system_nickname']."\"]}";
			//$pri_systems_json.="{ \"id\":".$key_system.", \"data\":[\"".$value_system['system_id']."\",\"".$value_system['nickname']."\",\"".$value_system['primary']."\",\"".$value_system['secondary']."\",\"".$value_system['future']."\",\"".$value_system['version']."\"]},";
			$pri_systems_json.="{ \"id\":\"".$key_system."\", ";
			$pri_systems_json_index.="\"".$key_system."\":".$x.",";
			$pri_systems_json.="\"userdata\":{\"future\":\"".$value_system['future']."\", \"pre_paid\":\"".$value_system['pre_paid']."\", \"credit_hold\":\"".$value_system['credit_hold']."\"}, ";
			$pri_systems_json.="\"data\":[\"".$value_system['system_id']."\",\"".$value_system['nickname']."\",\"".$value_system['primary']."\",\"".$value_system['secondary']."\",";
			$pri_systems_json.="\"".$value_system['future']."\",\"".$value_system['pre_paid']."\",\"".$value_system['credit_hold']."\"]},";
		}else{
			if($value_system['primary'] == $user_data['name']){
				//$pri_systems_json.="{ \"id\":".$key_system.", \"data\":[\"".$value_system['system_id']."\",\"".$value_system['system_nickname']."\"]}";
				//$pri_systems_json.="{ \"id\":".$key_system.", \"data\":[\"".$value_system['system_id']."\",\"".$value_system['nickname']."\",\"".$value_system['primary']."\",\"".$value_system['secondary']."\",\"".$value_system['future']."\",\"".$value_system['version']."\"]},";
				$pri_systems_json.="{ \"id\":\"".$key_system."\", ";
				$pri_systems_json_index.="\"".$key_system."\":".$x.",";
				$pri_systems_json.="\"userdata\":{\"future\":\"".$value_system['future']."\", \"pre_paid\":\"".$value_system['pre_paid']."\", \"credit_hold\":\"".$value_system['credit_hold']."\"}, ";
				$pri_systems_json.="\"data\":[\"".$value_system['system_id']."\",\"".$value_system['nickname']."\",\"".$value_system['primary']."\",\"".$value_system['secondary']."\",";
				$pri_systems_json.="\"".$value_system['future']."\",\"".$value_system['pre_paid']."\",\"".$value_system['credit_hold']."\"]},";
			}
			
			if($value_system['secondary'] == $user_data['name']){
				//$sec_systems_json.="{ \"id\":".$key_system.", \"data\":[\"".$value_system['system_id']."\",\"".$value_system['system_nickname']."\"]}";
				//$sec_systems_json.="{ \"id\":".$key_system.", \"data\":[\"".$value_system['system_id']."\",\"".$value_system['nickname']."\",\"".$value_system['primary']."\",\"".$value_system['secondary']."\",\"".$value_system['future']."\",\"".$value_system['version']."\"]},";
				$sec_systems_json.="{ \"id\":\"".$key_system."\", ";
				$sec_systems_json_index.="\"".$key_system."\":".$x.",";
				$sec_systems_json.="\"userdata\":{\"future\":\"".$value_system['future']."\", \"pre_paid\":\"".$value_system['pre_paid']."\", \"credit_hold\":\"".$value_system['credit_hold']."\"}, ";
				$sec_systems_json.="\"data\":[\"".$value_system['system_id']."\",\"".$value_system['nickname']."\",\"".$value_system['primary']."\",\"".$value_system['secondary']."\",";
				$sec_systems_json.="\"".$value_system['future']."\",\"".$value_system['pre_paid']."\",\"".$value_system['credit_hold']."\"]},";
			}	
		}
		$x++;
	}

	$x++;
	$pri_systems_json = preg_replace("/,$/","",$pri_systems_json) . "], ".preg_replace("/,$/","",$pri_systems_json_index)."}]}";
	$sec_systems_json = preg_replace("/,$/","",$sec_systems_json) . "], ".preg_replace("/,$/","",$sec_systems_json_index)."}]}";
}
d($pri_systems_json);
d($sec_systems_json);

$sql="SELECT * FROM misc_carrier ORDER BY company ASC;";
if(!$resultCarrier = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

$myusername = $_SESSION["login"];

$sql="SELECT * FROM users_role_id;";
if(!$resultRoleId = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rolesArray= array();
$rolesNamesArr = array();
$default_role = '';
while($rowRoleId = $resultRoleId->fetch_assoc()){
	$rolesArray[$rowRoleId['id']]=$rowRoleId['role'];
	if(intval($rowRoleId['id']) < 10){
		$rolesNamesArr[$rowRoleId['id']]=$rowRoleId['name'];
	}
	if($rowRoleId['id'] == $user_data['default_role']){
		$default_role = $rowRoleId['role'];
	}
}
d($rolesArray);
d($rolesNamesArr);

$sql="SELECT upi.*, GROUP_CONCAT(uri.id) AS roles
FROM users_perm_id AS upi
LEFT JOIN users_role_perms AS urp ON urp.pid = upi.id
LEFT JOIN users_role_id AS uri ON uri.id = urp.rid
GROUP BY upi.id
ORDER BY sequence ASC;";
if(!$resultPermId = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$permArray = array();
while($rowPermId = $resultPermId->fetch_assoc()){
	$expoded_roles = explode(',', $rowPermId['roles']);
	$perms_roles = array();
	foreach($expoded_roles as $role){
		$perms_roles[$role] = $rolesArray[$role];
	}
	$permArray[$rowPermId['id']] = array('name'=>$rowPermId['name'],'perm'=>$rowPermId['perm'],'notes'=>$rowPermId['notes'],'roles'=>$perms_roles); 
}

d($permArray);

$sql="SELECT id, qual, modality, mfg, CONCAT(modality,' - ',mfg) AS name
FROM users_qual_id AS qlid
ORDER BY sequence ASC;";
if(!$resultQualId = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$qualArray = array();
while($rowQualId = $resultQualId->fetch_assoc()){
	$qualArray[$rowQualId['id']] = array('name'=>$rowQualId['name'],'qual'=>$rowQualId['qual'],'modality'=>$rowQualId['modality'],'mfg'=>$rowQualId['mfg']); 
}
d($qualArray);

?>
<!DOCTYPE html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>

<link rel="stylesheet" type="text/css" href="/resources/dhtmlx/dhtmlxGrid/dhtmlxgrid.css">
<link rel="stylesheet" type="text/css" href="/resources/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">

<?php require_once($js_include);?>
<script  src="/resources/dhtmlx/dhtmlxGrid/dhtmlxcommon.js"></script>
<script  src="/resources/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>        
<script  src="/resources/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>    
<script  src="/resources/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_drag.js"></script>
<script  src="/resources/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>

<script type="text/javascript">
var globalUserCheck = true;
var globalEmailCheck = true;
var globalAddUser = <?php echo(!$edit)?"true":"false"; ?>;
var primary_grid;
var systems_grid;
var secondary_grid;
var current_roles = [<?php if($edit){echo "'".implode("','", $current_roles)."'";} ?>];
var tab;

$(document).ready(function() {

	$("#pwd_reset").change(function(e) {
		if($(this).is(":checked")){
			if(!confirm("Reset " + $("#firstname").val() + "'s password?")){
				$(this).prop("checked",false);
			}
		}
	});
	
	$("#secret_reset").change(function(e) {
		if($(this).is(":checked")){
			if(!confirm("Reset " + $("#firstname").val() + "'s secret questions?")){
				$(this).prop("checked",false);
			}
		}
	});
	
	$("#perm_admin_menus").change(function(e) {
		if($(this).is(":checked")){
			if(!confirm("Give " + $("#firstname").val() + " admin rights?")){
				$(this).prop("checked",false);
			}
		}
	});
	
	$(".button_jquery_save").button({
		icons: {
			primary: "ui-icon-disk"
		}
	});
	
	$(".button_jquery_lookup").button({
		icons: {
			primary: "ui-icon-search"
		}
	});
	$(".chooser").chosen({
		no_results_text: "Oops, nothing found!",
		disable_search_threshold: 10,
		placeholder_text_single: '  ',
		search_contains: true
	}); 
});
/////////////////////////////////////////////////////////////////////////////////////
function submitcheck(){
	var debug = <?php echo($debug)?"true":"false"; ?>;
	
	var systems_cnt = getRowCounts();
	if(systems_cnt[0] != 0){
		getPriSecSystems(primary_grid,'pri_systems');
		console.log('Got pri systems');
	}
	if(systems_cnt[1] != 0){
		getPriSecSystems(secondary_grid,'sec_systems');
		console.log('Got sec systems');
	}
	<?php if(strtolower($user_data['grp_customer']) == 'y'){echo "systems_cnt[1] = 1;";} ?>
	ids = [];
	tabs = [];
	errors = [];
	
//	if($("#lblSystemId").html().indexOf("!") >= 0){ids.push("#system_id");tabs.push("#li-tab-10");}	
	
	if(globalEmailCheck == false){ids.push("#email");errors.push("Email address already taken or invalid format.");tabs.push("#li-tab-UserInfo");}
	if(globalUserCheck == false){ids.push("#userid");errors.push("User ID already taken");tabs.push("#li-tab-UserInfo");}

	if($("#firstname").val()==""){ids.push("#firstname");errors.push("First Name is required");tabs.push("#li-tab-UserInfo");}
	if($("#lastname").val()==""){ids.push("#lastname");errors.push("Last Name is required");tabs.push("#li-tab-UserInfo");}
	if($("#userid").val()==""){ids.push("#userid");errors.push("User ID is required");tabs.push("#li-tab-UserInfo");}
	if($("#email").val()==""){ids.push("#email");errors.push("Email Address is required");tabs.push("#li-tab-UserInfo");}
	if($("#initials").val()==""){ids.push("#initials");errors.push("Initials required");tabs.push("#li-tab-UserInfo");}
	
	if($("#address").val() == ""){ids.push("#address");errors.push("Address required");tabs.push("#li-tab-UserInfo");}
	if($("#city").val() == ""){ids.push("#city");errors.push("City required");tabs.push("#li-tab-UserInfo");}
	if($("#state").val() == ""){ids.push("#state_chosen");errors.push("State required");tabs.push("#li-tab-UserInfo");}
	if($("#zip").val() == ""){ids.push("#zip");errors.push("Zipcode required");tabs.push("#li-tab-UserInfo");}
	if($("#cell").val() == ""){ids.push("#cell");errors.push("Phone required");tabs.push("#li-tab-UserInfo");}
	
	if($("#group_edit").val() == ''){
		ids.push("#none");
		errors.push("Select a group");
		tabs.push("#li-tab-Groups");
	}
	
	var no_roles = true;
	$(".role_input").each(function(index, element) {
		if($(this).is(":checked")){
			no_roles = false;	
		}
	});
	if(no_roles){
		ids.push("#none");
		errors.push("Select roles for user");
		tabs.push("#li-tab-Roles");	
	}
	
	if(($("#role_engineer").is(":checked") || $("#role_contractor").is(":checked") || $("#role_customer").is(":checked")) && !$("#perm_no_assigned_systems").is(":checked") && systems_cnt[0] == 0 && systems_cnt[1] == 0){
		ids.push("#none");
		errors.push("System assignments required");
		tabs.push("#li-tab-Assignments");
	}
	
	if($(".qualification_input:checked").length == 0 && $("#role_engineer").is(":checked")){
		ids.push("#none");
		errors.push("Engineer Qualifications Required");
		tabs.push("#li-tab-Qualifications");
	}
	
	if(!$("#role_engineer").is(":checked") && $("#grp_employee").is(":checked")){
		$("#pri_systems").val("");
		$("#sec_systems").val("");	
		console.log("no engineer role");
	}
	if(!$("#role_contractor").is(":checked") && $("#grp_contractor").is(":checked")){
		$("#pri_systems").val("");
		$("#sec_systems").val("");	
		console.log("no contractor role");
	}
	if(!$("#role_customer").is(":checked") && $("#grp_customer").is(":checked")){
		$("#pri_systems").val("");
		$("#sec_systems").val("");	
		console.log("no customer role");
	}
	
	if($("#role_customer").is(":checked")){
		if($("#customer_id").val() == ''){
			ids.push("#customer_id");
			errors.push("Customer ID is required for cutomer login");
			tabs.push("#li-tab-CustomerLink");
		}
	}
	
	if($("#carrier").val() == "" && $("#perm_rcv_sms").is(":checked")){ids.push("#carrier");errors.push("Cell Carrier required to receive sms alerts");tabs.push("#li-tab-UserInfo");}
	
	var uiserid_check = $("#userid").val();
	var userid_regex = new RegExp("[@\.a-zA-Z0-9_-]", 'g');
	uiserid_check = uiserid_check.replace(userid_regex, '');
	console.log("user id remainder:" + uiserid_check);
	if(uiserid_check.length > 0){
		ids.push("#userid");
		errors.push("User ID may only contain letters numbers &emsp;<b> - &emsp; . &emsp; _ &emsp; @</b>");
		tabs.push("#li-tab-UserInfo");
	}
	
	var regex = new RegExp(/[\'@\"\$\\\#\+\?\&]/g);

//	if($("#userid").val().match(regex)){
//		ids.push("#userid");
//		errors.push("User ID contains invalid characters  ' @ & $ \ # + ?");
//	}
	
	if($("#firstname").val().match(regex)){
		ids.push("#firstname");
		errors.push("First Name contains invalid characters  ' @ & $ \ # + ?");
		tabs.push("#li-tab-UserInfo");
	}
	
	if($("#lastname").val().match(regex)){
		ids.push("#lastname");
		errors.push("Last Name contains invalid characters  ' @ & $ \ # + ?");
		tabs.push("#li-tab-UserInfo");
	}
	
	if(debug){
		console.log("ids: " + ids);
		console.log("errors: " + errors);
	}
	showErrors(ids,errors,tabs);
	if(ids.length <= 0){
		checkquals();
		if (confirm("<?php if($edit){echo "Update";}else{echo "Add New";} ?> User?")){
			<?php if($debug){?>
				if(!confirm('DEBUG: really submit?!')){
					return;	
				}
			<?php } ?>
			document.forms['form'].submit();
		}	
	}
}
/////////////////////////////////////////////////////////////////////////////////////
function showErrors(ids,errors,tabs){
	if(tabs.length > 0){
		errors.unshift("<h1><b>Correct the red highlighted fields</b></h1>");
	}
	$("input, select, .chosen-container").each(function(index, element) { //set all inputs to not highlighted
		$(this).animate({
			borderColor: "#2C3594",
			boxShadow: 'none'
		});
	});
	
	$("#tabs > ul > li").each(function(index, element) { //set all inputs to not highlighted
		$(this).animate({
			borderColor: "#2C3594",
			boxShadow: 'none'
		});
	});
	
	$.each(ids,function(index,value){//highlight inputs
		$(value).animate({
			borderColor: "#cc0000",
			boxShadow: '0 0 5px 3px rgba(255,0,0,0.4)'
		});
	});
	
	$.each(tabs,function(index,value){ //highlight tabs
		$(value).animate({
			borderColor: "#cc0000",
			boxShadow: '0 0 5px 3px rgba(255,0,0,0.4)'
		});
	});
	
	$("#errors > span").html("");
	$.each(errors,function(index, value){
		$("#errors > span").append(value + "<br>");
	});
	$("#errors").show('slow');
	//$(document).scrollTop(0);
}
/////////////////////////////////////////////////////////////////////////////////////
function showGroups(str){
	if(str != ""){
		tab.tabs( "enable", "#tab-Groups" );
		tab.tabs( "enable", "#tab-UserPic" );	
	}else{
		tab.tabs( "disable", "#tab-Groups" );
		tab.tabs( "disable", "#tab-UserPic" );
	}
}
/////////////////////////////////////////////////////////////////////////////////////
function checkUser(str){
	if (str==""){
		document.getElementById("txtUser").innerHTML=" ";
		return;
	}
	str = str.toLowerCase();
	$("#userid").val($("#userid").val().toLowerCase());
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}
	else{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("txtUser").innerHTML=xmlhttp.responseText;
			if(xmlhttp.responseText.replace(/ /g,'') != ""){
				globalUserCheck = false;
			}else{		
				globalUserCheck = true;
			}
		}
	}
	xmlhttp.open("GET","scripts/check_user_id_add.php?q="+str,true);
	xmlhttp.send();
}
/////////////////////////////////////////////////////////////////////////////////////
function checkEmail(str){
	if (str=="" || str == "<?php echo ($edit)?$user_data['email']:""; ?>"){
		//document.getElementById("txtUser").innerHTML=" ";
		return;
	}
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}else{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			//var emailAdd = $(xmlhttp.responseText).text().replace(/ /g,'');
			if(xmlhttp.responseText.replace(/ /g,'') == "2"){
				globalEmailCheck = false;
				alert("The email address is an invalid format");
			}else{
				globalEmailCheck = true;
				if(xmlhttp.responseText.replace(/ /g,'') != "0"){
					//globalEmailCheck = false;
					alert("The email address already exists for user(s) " + xmlhttp.responseText);
				}
			}
		}
	}
	xmlhttp.open("GET","scripts/check_email_add.php?q="+str,true);
	xmlhttp.send();     	
}
/////////////////////////////////////////////////////////////////////////////////////
function validateEmail(elementValue){  
       var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;  
       return emailPattern.test(elementValue);  
} 
/////////////////////////////////////////////////////////////////////////////////////
function validatePhone(phoneNumber){
	//var regexObj = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
	var regexObj = /^(\+)?[0-9\-\ ]{9,15}$/;
	var re = /\s/g;
	if (regexObj.test(phoneNumber)) {
		//var formattedPhoneNumber =	phoneNumber.replace(regexObj, "$1-$2-$3");
		var formattedPhoneNumber =	phoneNumber.replace(re, "-");
		document.getElementById('cell').value = formattedPhoneNumber;
		//return formattedPhoneNumber;
	} else {
		// Invalid phone number
		if(phoneNumber != ""){
			alert("Please type a valid 123-123-1234 or +012 1234 123123 phone/cell number.");
		}
	}	
}
/////////////////////////////////////////////////////////////////////////////////////
function checkgroup(id){
	console.log("checkgroup: " + id);
	if(globalAddUser){
		$(".grp_employee, .grp_contractor, .grp_customer").each(function(index, element) {
			$(this).prop("checked",false);
		});	
		if($("input[name=group]:checked").val()){
			tab.tabs("enable", "#tab-Roles");
		}
		$("#grp_employee_row, #grp_contractor_row, #grp_customer_row").hide();
		$("#"+id+"_row").show();
		$("."+id).each(function(index, element) {
			$(this).prop("checked",false);
		});	
		$(".permission_input").prop("checked",false);
		$(".permission_row").hide();
		$("#grp_employee, #grp_contractor, #grp_customer").prop("disabled",true);
		$("#group_edit").val(id);
	}else{
		$("#grp_employee_row, #grp_contractor_row, #grp_customer_row").hide();
		$("#"+id+"_row").show();
		if(id != 'grp_customer'){
			$("#li-tab-CustomerLink").remove().attr( "aria-controls" );
			$("#tab-CustomerLink").hide();
			tab.tabs("refresh");	
		}else{
			$("#li-tab-Qualifications").remove().attr( "aria-controls" );
			$("#tab-Qualifications").hide();
			tab.tabs("refresh");
		}
		$("#group_edit").val(id);
		checkrole('none',this);	
	}
}
/////////////////////////////////////////////////////////////////////////////////////
function checkrole(id,sender){
	//sender is not used
	console.log('checkrole id:' + id + ' sender:' + sender + ' globalAddUser:' + globalAddUser);
	if(globalAddUser){
		if($(".role_input:checked").length > 0){
			tab.tabs("enable", "#tab-DefaultRole");
			tab.tabs("enable", "#tab-Permissions");
		}else{
			tab.tabs("disable", "#tab-DefaultRole");
			tab.tabs("disable", "#tab-Permissions");
			tab.tabs("disable", "#tab-CustomerLink");			
		}
		//show all perms for checked roles
		$(".permission_row").hide();
		$("#default_role option").remove();
		$(".role_input:checked").each(function(index, element) {
			var id = $(this).prop("id");
			console.log(id);
			$("."+id).show();	
			$("#default_role").append($("<option></option>").attr("value",id).text($("."+id+"_label").text()));	
			tab.tabs("enable", "#tab-DefaultRole");
			if(id == 'role_customer'){
				tab.tabs("enable", "#tab-CustomerLink");	
			}
			$(".chooser").trigger("chosen:updated");
		});	
		
		if(!$("#perm_no_assigned_systems").is(":checked") && ($("#role_engineer").is(":checked") || $("#role_contractor").is(":checked") || $("#role_customer").is(":checked"))){
			tab.tabs("enable", "#tab-Assignments");
			if(!$("#role_customer").is(":checked")){
				tab.tabs("enable", "#tab-Qualifications");
			}			
			loadsystems();
		}else{
			tab.tabs("disable", "#tab-Assignments");
			tab.tabs("disable", "#tab-Qualifications");
			$("#qual-none").prop("checked",true);
		}
	}else{
		$(".permission_row").hide();
		$("#default_role option").remove();
		$(".role_input:checked").each(function(index, element) {
			var id = $(this).prop("id");
			var edit_id = '<?php if($edit){echo $default_role;} ?>';
			
			$("."+id).show();	
			if(id == edit_id){
				$("#default_role").append($("<option></option>").attr("value",id).text($("."+id+"_label").text()).attr('selected',''));				
			}else{
				$("#default_role").append($("<option></option>").attr("value",id).text($("."+id+"_label").text()));
			}
			tab.tabs("enable", "#tab-DefaultRole");
		});	
		checkperm('none',this);
		if(!$("#perm_no_assigned_systems").is(":checked") && ($("#role_engineer").is(":checked") || $("#role_contractor").is(":checked") || $("#role_customer").is(":checked"))  ){
			tab.tabs("enable", "#tab-Assignments");
			if(!$("#role_customer").is(":checked")){
				tab.tabs("enable", "#tab-Qualifications");
			}
			console.log('loading systems');
			loadsystems();
			console.log('loaded systems');
		}else{
			tab.tabs("disable", "#tab-Assignments");
			tab.tabs("disable", "#tab-Qualifications");
			$("#qual-none").prop("checked",true);
		}
	}
	
	//uncheck perms if role is unchecked
	if(!$("#"+id).is(":checked")){		
		$(".permission_input:checked").each(function(index, element) {
			var stay_checked = false;
			var id = $(this).prop("id");
			var classes = element.className.split(/\s+/);					
			$(".role_input:checked").each(function(index, element) {							
				var index = classes.indexOf($(this).prop("id")+"_input");
				if(index >= 0){
					stay_checked = true;
				}
			});
			if(!stay_checked){
				$("#"+id).prop("checked",false);
			}					
		});
	}
}
/////////////////////////////////////////////////////////////////////////////////////
function checkperm(id,sender){
	console.log('checkperm id:' + id + ' sender:' + sender);
	//perm_no_assigned_systems
	if($("#perm_no_assigned_systems").is(":checked") && ($("#role_engineer").is(":checked") || $("#role_contractor").is(":checked"))){
		tab.tabs("disable", "#tab-Assignments");
	}else if(!$("#perm_no_assigned_systems").is(":checked") && ($("#role_engineer").is(":checked") || $("#role_contractor").is(":checked"))){
		tab.tabs("enable", "#tab-Assignments");
		loadsystems();
	}
	
}
/////////////////////////////////////////////////////////////////////////////////////
function checkquals(){
	var none = true;
	$(".qualification_input").each(function(index, element) {
		if($(this).is(":checked")){
			console.log($(this).prop("id"));
			none = false;	
		}
	});
	if(none){
		$("#qual_none").prop("checked", true);
		console.log('No quals so selecting none');
	}else{
		$("#qual_none").prop("checked", false);	
	}
}
/////////////////////////////////////////////////////////////////////////////////////
function loadsystems(){
	var systems = <?php echo $systems_json; ?>;
	<?php echo($has_systems_assigned)?"var primary_systems = ".$pri_systems_json:""; ?>;
	<?php echo($has_systems_assigned)?"var secondary_systems =".$sec_systems_json:""; ?>;
	
	
	primary_grid = new dhtmlXGridObject('primary_systems_div');
	primary_grid.setImagePath("/resources/dhtmlx/dhtmlxGrid/imgs/");
	primary_grid.setHeader("System ID,System Nickname,Primary,Secondary,Future,Pre Paid, Credit Hold");
	primary_grid.setColumnIds("system_id","system_nickname","primary","secondary","future","pre_paid","credit_hold");
	primary_grid.setInitWidths("50");
	primary_grid.setColAlign("left,left,left,left,left,left,left");
	primary_grid.setColTypes("ro,ro,ro,ro,ro,ro,ro");
	primary_grid.setColSorting("str,str,str,str,str,str,str");
	primary_grid.setMultiLine(false);
	primary_grid.enableDragAndDrop(true);
	primary_grid.enableAutoWidth(true);
	primary_grid.attachEvent("onDrop", function(sId,tId,dId,sObj,tObj,sCol,tCol){
		if(primary_grid.cells(sId,4).getValue().toLowerCase() == 'y'){ //Future
			tObj.setRowTextStyle(sId,'color:#7F00FF;');	
		}
		if(primary_grid.cells(sId,5).getValue().toLowerCase() == 'y'){ //Pre Paid
			tObj.setRowTextStyle(sId,'color:#FF8040;');	
		}
		if(primary_grid.cells(sId,6).getValue().toLowerCase() == 'y'){ //Credit Hold
			tObj.setRowTextStyle(sId,'color:#FF0000;');	
		}
		tObj.sortRows(0,"str","asc");
		getRowCounts();
	});
	primary_grid.init();
	primary_grid.setColWidth(2,"0");
	primary_grid.setColWidth(3,"0");
	primary_grid.setColWidth(4,"0");
	primary_grid.setColWidth(5,"0");
	primary_grid.setColWidth(6,"0");
	primary_grid.setSkin("dhx_skyblue");
	<?php if($has_systems_assigned){ ?>
		primary_grid.parse(primary_systems,"json");
		$.each(primary_systems.rows, function(key, value){
			
			if(value.userdata.future.toLowerCase() == 'y'){ //Future
				primary_grid.setRowTextStyle(value.id,'color:#7F00FF;');
			}
			if(value.userdata.pre_paid.toLowerCase() == 'y'){ //Pre Paid
				primary_grid.setRowTextStyle(value.id,'color:#FF8040;');	
			}
			if(value.userdata.credit_hold.toLowerCase() == 'y'){ //Credit Hold
				primary_grid.setRowTextStyle(value.id,'color:#FF0000;');	
			}
		})
		
	<?php } ?>
		 
	systems_grid = new dhtmlXGridObject('systems_div');
	systems_grid.setImagePath("/resources/dhtmlx/dhtmlxGrid/imgs/");
	systems_grid.setHeader("System ID,System Nickname,Primary,Secondary,Future,Pre Paid, Credit Hold");
	systems_grid.setColumnIds("system_id","system_nickname","primary","secondary","future","pre_paid","credit_hold");
	systems_grid.setInitWidths("50");
	systems_grid.setColAlign("left,left,left,left,left,left,left");
	systems_grid.setColTypes("ro,ro,ro,ro,ro,ro,ro");
	systems_grid.setColSorting("str,str,str,str,str,str,str");
	systems_grid.setMultiLine(false);
	systems_grid.enableDragAndDrop(true);
	systems_grid.attachHeader("#text_filter,#text_filter,#text_filter,#text_filter");
	systems_grid.enableAutoWidth(true);
	systems_grid.attachEvent("onDrop", function(sId,tId,dId,sObj,tObj,sCol,tCol){
		if(systems_grid.cells(sId,4).getValue().toLowerCase() == 'y'){ //Future
			tObj.setRowTextStyle(sId,'color:#7F00FF;');	
		}
		if(systems_grid.cells(sId,5).getValue().toLowerCase() == 'y'){ //Pre Paid
			tObj.setRowTextStyle(sId,'color:#FF8040;');	
		}
		if(systems_grid.cells(sId,6).getValue().toLowerCase() == 'y'){ //Credit Hold
			tObj.setRowTextStyle(sId,'color:#FF0000;');	
		}
		tObj.sortRows(0,"str","asc");
		getRowCounts();
	});
	systems_grid.init();
	systems_grid.setColWidth(4,"0");
	systems_grid.setColWidth(5,"0");
	systems_grid.setColWidth(6,"0");
	systems_grid.setSkin("dhx_skyblue");
	systems_grid.parse(systems,"json");
	$.each(systems.rows, function(key, value){
		if(value.userdata.future.toLowerCase() == 'y'){ //Future
			systems_grid.setRowTextStyle(value.id,'color:#7F00FF;');	
		}
		if(value.userdata.pre_paid.toLowerCase() == 'y'){ //Pre Paid
			systems_grid.setRowTextStyle(value.id,'color:#FF8040;');	
		}
		if(value.userdata.credit_hold.toLowerCase() == 'y'){ //Credit Hold
			systems_grid.setRowTextStyle(value.id,'color:#FF0000;');	
		}
	})
	
	secondary_grid = new dhtmlXGridObject('secondary_systems_div');
	secondary_grid.setImagePath("/resources/dhtmlx/dhtmlxGrid/imgs/");
	secondary_grid.setHeader("System ID,System Nickname,Primary,Secondary,Future,Pre Paid, Credit Hold");
	secondary_grid.setColumnIds("system_id","system_nickname","primary","secondary","future","pre_paid","credit_hold");
	secondary_grid.setInitWidths("50");
	secondary_grid.setColAlign("left,left,left,left,left,left,left");
	secondary_grid.setColTypes("ro,ro,ro,ro,ro,ro,ro");
	secondary_grid.setColSorting("str,str,str,str,str,str,str");
	secondary_grid.setMultiLine(false);
	secondary_grid.enableDragAndDrop(true);
	secondary_grid.enableAutoWidth(true);
	secondary_grid.attachEvent("onDrop", function(sId,tId,dId,sObj,tObj,sCol,tCol){
		if(secondary_grid.cells(sId,4).getValue().toLowerCase() == 'y'){ //Future
			tObj.setRowTextStyle(sId,'color:#7F00FF;');	
		}
		if(secondary_grid.cells(sId,5).getValue().toLowerCase() == 'y'){ //Pre Paid
			tObj.setRowTextStyle(sId,'color:#FF8040;');	
		}
		if(secondary_grid.cells(sId,6).getValue().toLowerCase() == 'y'){ //Credit Hold
			tObj.setRowTextStyle(sId,'color:#FF0000;');	
		}
		tObj.sortRows(0,"str","asc");
		getRowCounts();
	});
	secondary_grid.init();
	secondary_grid.setColWidth(2,"0");
	secondary_grid.setColWidth(3,"0");
	secondary_grid.setColWidth(4,"0");
	secondary_grid.setColWidth(5,"0");
	secondary_grid.setColWidth(6,"0");
	secondary_grid.setSkin("dhx_skyblue");
	<?php if($has_systems_assigned and strtolower($user_data['grp_customer']) != 'y'){ ?>
		secondary_grid.parse(secondary_systems,"json");
		$.each(secondary_systems.rows, function(key, value){
			if(value.userdata.future.toLowerCase() == 'y'){ //Future
				secondary_grid.setRowTextStyle(value.id,'color:#7F00FF;');	
			}
			if(value.userdata.pre_paid.toLowerCase() == 'y'){ //Pre Paid
				secondary_grid.setRowTextStyle(value.id,'color:#FF8040;');	
			}
			if(value.userdata.credit_hold.toLowerCase() == 'y'){ //Credit Hold
				secondary_grid.setRowTextStyle(value.id,'color:#FF0000;');	
			}
		})
	<?php } ?>

	//$("#eng_cont_systems_div").show(); //debug only
	
	$("#primary_systems_div, #systems_div, #secondary_systems_div").width("100%");
	var customer = <?php if(strtolower($user_data['grp_customer']) != 'y'){echo "false";}else{echo "true";}?>;
	if(customer || $("#role_customer").is(":checked")){
		secondary_grid.destructor();
		$("#secondary_systems_div").remove();
		$("#secondary_systems_count_td, #secondary_systems_label_td").html("");
			
	}
	
	getRowCounts();
}
/////////////////////////////////////////////////////////////////////////////////////
function getPriSecSystems(grid,input){
	var systems = [];
	grid.forEachRow(function(id){ // function that gets id of the row as an incoming argument
		systems.push(id)
		//console.log(id);
	})
	document.getElementById(input).value = systems.join(',');
}

/////////////////////////////////////////////////////////////////////////////////////
function getRowCounts(){
	try {
		var customer = <?php if($edit){if(strtolower($user_data['grp_customer']) == 'y'){echo "true";}else{echo "false";}}else{echo "true";}?>;
		var secondary_grid_count;
		if(!customer || !$("#role_customer").is(":checked")){
			secondary_grid_count = secondary_grid.getRowsNum();
		}else{
			secondary_grid_count = 0;
		}
		var result = [primary_grid.getRowsNum(),secondary_grid_count,systems_grid.getRowsNum()]
		$("#pri_assigned_systems_count").html(result[0]);
		$("#sec_assigned_systems_count").html(result[1]);
		return result;
	}catch(e){
		console.log(e);
		return [0,0,0];
	}
	
	
}
/////////////////////////////////////////////////////////////////////////////////////
function getSystems(){
	var id = $("#customer_id").val();
	$("#systems_by_customer").html('Click the Customer ID field to update this text');
	$.ajax({
		type: 'POST',
		url: 'scripts/systems_by_customer.php',
		data: { customer_id: id },
		success:function(data){
			// successful request; do something with the data
			$("#systems_by_customer").html("Associated Systems: " + data);
		}
	});	
}

//Keep this last for the registration auto fill to work
<?php if($reg){
	$reg_name = explode(' ',$rowReg['name'],2);
?>
	$(document).ready(function() {
		$("#firstname").val("<?php echo $reg_name[0]; ?>");
		$("#lastname").val("<?php echo $reg_name[1]; ?>");
		$("#initials").val("<?php echo $reg_name[0][0] . $reg_name[1][0]; ?>");
		$("#userid").val("<?php echo $rowReg['uid']; ?>");
		showGroups($("#userid").val());
		checkUser($("#userid").val());
		$("#email").val("<?php echo $rowReg['email']; ?>");
		$("#cell").val("<?php echo $rowReg['phone']; ?>");
		$("#address").val("<?php echo $rowReg['address']; ?>");
		$("#city").val("<?php echo $rowReg['city']; ?>");
		$("#state").val("<?php echo $rowReg['state']; ?>");
		$("#zip").val("<?php echo $rowReg['zip']; ?>");
		$("#reg_id").val("<?php echo $rowReg['id']; ?>");
		$("select").trigger("chosen:updated");
	});
<?php } ?>
<?php if($resultAsgnSystems->num_rows < 1 and $has_systems_assigned == true){ ?>
	$(document).ready(function() {
		$.prompt("<h3>The user has no systems assigned and should have. Please correct this issue.</h3>",
		{
			title: "Warning",
			submit: function(e,v,m,f){
					$.prompt.close();
					$("#systems_table")[0].scrollIntoView( true );
					
			}	
		});		
	});
<?php } ?>


</script>
</head>
<body>
<?php require_once($header_include); ?>
<div id="OIReportContent">
    <div id="styledForm">
        <form id="form" name="form" method="post" autocomplete="off" action="users_do.php">
            <h1>Create New User Form</h1>
			<span class="red">Required fields marked with an *</span>
			<div id="errors" style="text-align:center;display:none">
				<!--<h2 style="margin:0px; padding:0px">Errors to fix</h2>-->
				<span style="color:#F00">
				</span>
			</div>
			<div class="line"></div>
			
			<div id="tabs">
				<ul>
					<li id="li-tab-UserInfo"><a href="#tab-UserInfo">User Info</a></li>
					<!--<li id="li-tab-UserPic"><a href="#tab-UserPic">User Picture</a></li>-->
					<li id="li-tab-Groups"><a href="#tab-Groups">Groups</a></li>
					<li id="li-tab-Roles"><a href="#tab-Roles">Roles</a></li>
					<li id="li-tab-DefaultRole"><a href="#tab-DefaultRole">Default Role</a></li>
					<li id="li-tab-CustomerLink"><a href="#tab-CustomerLink">Customer Link</a></li>
					<li id="li-tab-Permissions"><a href="#tab-Permissions">Permissions</a></li>
					<li id="li-tab-Qualifications"><a href="#tab-Qualifications">Qualifications</a></li>
					<li id="li-tab-Assignments"><a href="#tab-Assignments">Assignments</a></li>
					<li id="li-tab-Maintenance"><a href="#tab-Maintenance">Maintenance</a></li>
				</ul>
				
<!-- User Info Tab -->
				
				<div id="tab-UserInfo" class="tab">
					<table width="100%" cellpadding="5" cellspacing="5">
						<tr>
							<td width="50%" colspan="2"><table><tr>
							<td width="4%"><input name="active" type="checkbox" id="active" value="Y" <?php if(strtolower($user_data['active']) == 'y' or !$edit){echo "checked=\"checked\"";} ?> class="checkbox"  /></td>
							<td><label>User Enabled (disabled users will not be allowed to login)</label></td>
							</tr></table>
						</tr>
						<tr>
							<td width="50%"><label>User ID (cannot be changed once set) <span class="red">*</span>&emsp;<span class="required" style="color:red" id="txtUser"></span></label>
									<input type="text" name="userid" id="userid"  value="<?php echo ($edit)? $user_data['uid']:""; ?>" <?php echo (!$edit)? 'onchange="showGroups(this.value);" onBlur="checkUser(this.value);" onkeyup="this.onchange();" onpaste="this.onblur(); this.onchange();" oninput="this.onchange();"':"disabled"; ?>  /></td>
									<?php echo($edit)?"<input type=\"hidden\" name=\"userid_edit\" id=\"userid_edit\" value=\"$userid\" />":""; ?>
							<td>&nbsp;</td>
						</tr>
						
						<tr>
							<td><label>First Name <span class="red">*</span></label>
								<input type="text" name="firstname" id="firstname" value="<?php echo ($edit)? $user_data['firstname']:""; ?>"  /></td>
							<td><label>Last Name <span class="red">*</span></label>
								<input type="text" name="lastname" id="lastname" value="<?php echo ($edit)? $user_data['lastname']:""; ?>"  /></td>
						</tr>
						<tr>
							<td><label>Initials <span class="red">*</span></label>
								<input type="text" name="initials" id="initials" value="<?php echo ($edit)? $user_data['initials']:""; ?>"  /></td>
							<td width="50%"><label>Email Address <span class="red">*</span></label>
								<input name="email" type="email" id="email"  value="<?php echo ($edit)? $user_data['email']:""; ?>" onchange="" onBlur="checkEmail(this.value);" onkeyup="this.onchange();" onpaste="this.onblur();" oninput="this.onchange();" /></td>
						</tr>
						<tr>
							<td><label>Cell/Phone Number <span class="red">*</span></label>
							<input name="cell" type="tel" id="cell" value="<?php echo ($edit)? $user_data['cell']:""; ?>" onblur="validatePhone(this.value)" onpaste="this.onblur();"  /></td>
							<td>
							<label>Cell Carrier</label>
							<select id="carrier" name="carrier" class="chooser">
							<option value="" title=""></option>
								<?php
								while($rowC = $resultCarrier->fetch_assoc()){
									echo "<option ";
									if($edit and $rowC['carrierid'] == $user_data['carrier']){  
										echo "selected ";	
									}
									echo "value='".$rowC['carrierid']."' title='".$rowC['carrierid']."'>".$rowC['company']." - ".$rowC['address']."</option>\n";
								}
								?>
							</select>
							</td>
						</tr>
					</table>
					<table width="100%" cellpadding="5" cellspacing="5">
						<tr>
							<td colspan="3"><label>Address <span class="red">*</span></label>
							<input name="address" type="text" id="address" value="<?php echo ($edit)? $user_data['address']:""; ?>"/></td>
						</tr>
						<tr>
							<td><label>City <span class="red">*</span></label>
							<input name="city" type="text" id="city" value="<?php echo ($edit)? $user_data['city']:""; ?>" /></td>
							<td><label>State <span class="red">*</span></label>
		
							<select name="state" id="state" class="chooser">
									<option value=""></option>
									<?php
										foreach($states as $state_key=>$state_name){
											echo "<option value='" . $state_key . "'";
											if($edit){
												if($user_data['state']==$state_key){echo " selected";}
											}
											echo">" . $state_name . "</option>\n";
										}
									  ?>
								</select></td>
							<td><label>Zip <span class="red">*</span></label>
							<input name="zip" type="text" id="zip" value="<?php echo ($edit)? $user_data['zip']:""; ?>" /></td>
						</tr>
					</table>
					<?php if(!$edit){ ?>
					<table width="100%" cellpadding="5" cellspacing="5">
						<tr>
							<td width="50%"><label>Registration ID</label>
							<input name="reg_id" type="text" id="reg_id" value="" /></td>
							<td>&nbsp;</td>
						</tr>
					</table>
					<?php } ?>
				</div>

<!-- Picture Tab -->

<!--				<div id="tab-UserPic" class="tab">
					<div id="profile_pic"></div>
				</div>
-->
<!-- Group Tab -->

				<div id="tab-Groups" class="tab">
					<table width="100%" cellpadding="5" cellspacing="5">
						<tr align="center">
							<td colspan="3"><h4>Group cannot be changed once the user has been added</h4></td>
						</tr>
						<tr align="center">
							<td><label>Employees</label>
								<input type="radio" name="group" id="grp_employee" <?php echo ($edit and strtolower($user_data['grp_employee'])=='y')? "checked":""; echo($edit)?" disabled":"";?> onClick="checkgroup('grp_employee');" value="grp_employee" />
							</td>
								
							<td><label>Contractors</label>
								<input type="radio" name="group" id="grp_contractor" <?php echo ($edit and strtolower($user_data['grp_contractor'])=='y')? "checked":""; echo($edit)?" disabled":""; ?> onClick="checkgroup('grp_contractor');" value="grp_contractor" />
							</td>
								
							<td align="center"><label>Customers</label>
								<input type="radio" name="group" id="grp_customer" <?php echo ($edit and strtolower($user_data['grp_customer'])=='y')? "checked":""; echo($edit)?" disabled":""; ?> onClick="checkgroup('grp_customer');" value="grp_customer" />
							</td>
							<?php echo($edit)?"<input type=\"hidden\" name=\"group\" id=\"grp_temp\" value=\"$group\" />":""; ?>	
						</tr>
					</table>
				</div>

<!-- Roles Tab -->
				
				<div id="tab-Roles" class="tab">
					<table width="100%" cellpadding="5" cellspacing="5">	
						<tr align="center" id="grp_employee_row">
							<td>
								<table width="100%" cellpadding="5" cellspacing="5">
									<tr>
										<td width="25%" align="right"><label class="role_engineer_label">Engineer</label></td><td><input class="grp_employee role_input" type="checkbox" id="role_engineer" name="roles[role_engineer]" value="Y" <?php echo ($edit and strtolower($user_data['role_engineer'])=='y')? "checked":""; ?> onClick="checkrole('role_engineer',this);" style="width:auto;" /></td>
										<td width="25%" align="right"><label class="role_dispatch_label">Dispatcher</label></td><td><input class="grp_employee role_input" type="checkbox" id="role_dispatch" name="roles[role_dispatch]" value="Y" <?php echo ($edit and strtolower($user_data['role_dispatch'])=='y')? "checked":""; ?> onClick="checkrole('role_dispatch',this);" style="width:auto;" /></td>
										<td width="25%" align="right"><label class="role_finance_label">Finance</label></td><td><input class="grp_employee role_input" type="checkbox" id="role_finance" name="roles[role_finance]" value="Y" <?php echo ($edit and strtolower($user_data['role_finance'])=='y')? "checked":""; ?> onClick="checkrole('role_finance',this);" style="width:auto;" /></td>
									</tr>
									<tr>
										<td align="right"><label class="role_basic_label">Basic</label></td><td><input class="grp_employee role_input" type="checkbox" id="role_basic" name="roles[role_basic]" value="Y" <?php echo ($edit and strtolower($user_data['role_basic'])=='y')? "checked":""; ?> onClick="checkrole('role_basic',this);" style="width:auto;" /></td>
										<td align="right"><label class="role_management_label">Management</label></td><td><input class="grp_employee role_input" type="checkbox" id="role_management" name="roles[role_management]" value="Y" <?php echo ($edit and strtolower($user_data['role_management'])=='y')? "checked":""; ?> onClick="checkrole('role_management',this);" style="width:auto;" /></td>
										<td align="right"><label class="role_admin_label">Administrator</label></td><td><input class="grp_employee role_input" type="checkbox" id="role_admin" name="roles[role_admin]" value="Y" <?php echo ($edit and strtolower($user_data['role_admin'])=='y')? "checked":""; ?> onClick="checkrole('role_admin',this);" style="width:auto;" /></td>
									</tr>
									<tr>
										<td align="right"><label class="role_quality_label">Quality Management</label></td><td><input class="grp_employee role_input" type="checkbox" id="role_quality" name="roles[role_quality]" value="Y" <?php echo ($edit and strtolower($user_data['role_quality'])=='y')? "checked":""; ?> onClick="checkrole('role_quality',this);" style="width:auto;" /></td>
										<td align="right"><label class="role_sales_label">Sales</label></td><td><input class="grp_employee role_input" type="checkbox" id="role_sales" name="roles[role_sales]" value="Y" <?php echo ($edit and strtolower($user_data['role_sales'])=='y')? "checked":""; ?> onClick="checkrole('role_sales',this);" style="width:auto;" /></td>
										<td align="right"><label class="role_sales_manager_label">Sales Manager</label></td><td><input class="grp_employee role_input" type="checkbox" id="role_sales_manager" name="roles[role_sales_manager]" value="Y" <?php echo ($edit and strtolower($user_data['role_sales_manager'])=='y')? "checked":""; ?> onClick="checkrole('role_sales_manager',this);" style="width:auto;" /></td>
									</tr>
								</table>
							</td>
						</tr>						
						<tr align="center" id="grp_contractor_row">
							<td>
								<table width="100%" cellpadding="5" cellspacing="5">
									<tr>
										<td width="25%" align="right">&nbsp;</td><td>&nbsp;</td>
										<td width="25%" align="right"><label class="role_contractor_label">Contractor</label></td><td><input class="grp_contractor role_input" type="checkbox" id="role_contractor" name="roles[role_contractor]" value="Y" <?php echo ($edit and strtolower($user_data['role_contractor'])=='y')? "checked":""; ?> onClick="checkrole('role_contractor',this);" style="width:auto;" /></td>
										<td width="25%" align="right">&nbsp;</td><td>&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>						
						<tr align="center" id="grp_customer_row">
							<td>
								<table width="100%" cellpadding="5" cellspacing="5">
									<tr>
										<td width="25%" align="right">&nbsp;</td><td>&nbsp;</td>
										<td width="25%" align="right"><label class="role_customer_label">Customer</label></td><td><input class="grp_customer role_input" type="checkbox" id="role_customer" name="roles[role_customer]" value="Y" <?php echo ($edit and strtolower($user_data['role_customer'])=='y')? "checked":""; ?> onClick="checkrole('role_customer',this);" style="width:auto;" /></td>
										<td width="25%" align="right">&nbsp;</td><td>&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
				
<!-- Default Role Tab -->

				<div id="tab-DefaultRole" class="tab">
					<table width="100%" cellpadding="5" cellspacing="5" id="default_role_table">
						<tr align="center">
							<td><select id="default_role" name="default_role" style="width:auto"></select></td>
						</tr>
					</table>
				</div>
				
<!-- Customer Link Tab -->
				
				<div id="tab-CustomerLink" class="tab">				
					<table width="100%" cellpadding="5" cellspacing="5" id="customer_table">
						<tr align="center">
							<td><label class="customer_id_label">Customer ID </label>
							<input class="" type="text" id="customer_id" name="customer_id" onMouseDown="getSystems()" onBlur="getSystems()" value="<?php echo ($edit)? $user_data['customer_id']:""; ?>" style="width:auto;" />&emsp;
							<a name="lookup_customer" class="iframeView button_jquery_lookup" href="customer_lookup.php">Lookup Customer ID</a></td>
						</tr>
						<tr align="center">
							<td><span id="systems_by_customer">Click the Customer ID field to update this text</span>
						</tr>
					</table>
				</div>
							
<!-- Permissions Link Tab -->
				
				<div id="tab-Permissions" class="tab">
					<table width="100%" cellpadding="5" cellspacing="5" id="permissions_table">
						<?php
							foreach($permArray as $key=>$value){
								echo "<tr class=\"permission_row ";
								foreach($value['roles'] as $keyRole=>$valueRole){
									echo $valueRole." ";
								}
								echo "\">\n";
								echo "<td width=\"50%\" align=\"right\"><label>".$value['name']."</label></td>
								<td><input class=\"permission_input ";
								foreach($value['roles'] as $keyRole=>$valueRole){
									echo $valueRole."_input ";
								}
								echo "\" type=\"checkbox\" id=\"".$value['perm']."\" name=\"perms[".$value['perm']."]\" value=\"Y\"";
								if($edit and strtolower($user_data[$value['perm']]) == 'y'){
									echo " checked";
								}else{
									echo "";
								}
								echo " onClick=\"checkperm('".$value['perm']."',this);\" style=\"width:auto;\" />&emsp;".$value['notes']."</td>";
								echo "</tr>\n";
							}
						?>						
					</table>
				</div>
				
<!-- Qualifications Tab -->
				
				<div id="tab-Qualifications" class="tab">
					<table width="100%" cellpadding="5" cellspacing="5" id="qualifications_table">
						<?php
							foreach($qualArray as $key=>$value){
								echo "<tr class=\"qualification_row\">\n";
								echo "<td width=\"50%\" align=\"right\"><label>".$value['name']."</label></td>
								<td><input class=\"qualification_input\" type=\"checkbox\" id=\"".$value['qual']."\" name=\"quals[".$value['qual']."]\" value=\"Y\" onClick=\"checkquals('".$value['qual']."',this);\"";
								if($edit and strtolower($user_data[$value['qual']]) == 'y'){
									echo " checked";
								}else{
									echo "";
								}
								echo " style=\"width:auto;\" />&emsp;</td>";
								echo "</tr>\n";
							}
						?>						
					</table>
				</div>
				
<!-- Assignments Tab -->
				
				<div id="tab-Assignments" class="tab">
					
					<table width="100%" cellpadding="5" cellspacing="5" id="systems_table">
						<tr align="center">
							<td colspan="3"><h2 style="margin:0; padding:0">Drag and drop systems to assign them</h2></td>
						</tr>
						<tr align="center">
							<td id="primary_systems_label_td"><h2 style="margin:0; padding:0">Primary Systems</h2></td>
							<td><h2 style="margin:0; padding:0">Available Systems</h2></td>
							<td id="secondary_systems_label_td"><h2 style="margin:0; padding:0">Secondary Systems</h2></td>
						</tr>
						<tr>
							<td width="25%">
								<div id="primary_systems_div" style="width:100%; height:270px;background-color:white;"></div>
							</td>
							<td width="50%">
								<div id="systems_div" style="width:100%; height:270px;background-color:white;"></div>
							</td>
							<td width="25%">
								<div id="secondary_systems_div" style="width:100%; height:270px;background-color:white;"></div>
							</td>
						</tr>
						<tr align="center">
							<td id="primary_systems_count_td"><h2>Assigned: <span id="pri_assigned_systems_count">0</span></h2></td>
							<td>&nbsp;</td>
							<td id="secondary_systems_count_td"><h2>Assigned: <span id="sec_assigned_systems_count">0</span></h2></td>
						</tr>
					</table>
				</div>			
					
<!-- Maintenance Tab -->
				
				<div id="tab-Maintenance" class="tab">
					<table width="100%" cellpadding="5" cellspacing="5">
						<tr>
							<td><label>Reset Password</label>
							<input name="pwd_reset" type="checkbox" id="pwd_reset" value="Y" style="width:auto; margin-left:10px;"/></td>
							<td><label>Reset Secret Questions</label>
							<input name="secret_reset" type="checkbox" id="secret_reset" value="Y" style="width:auto; margin-left:10px;"/></td>
						</tr>
					</table>
				</div>

<!-- END TABS -->
				
			</div> <!-- Tabs Dig -->
            <div class="line"></div>
            <br />
            <!--<input name="Submit" type="button" value="<?php echo($edit)?"Update User's Profile":"Add New User"; ?>" class="button" onclick="submitcheck()" />-->
			<a name="Submit" class="button_jquery_save" onclick="submitcheck()"><?php echo($edit)?"Update User's Profile":"Add New User"; ?></a>
            <br />
            <br />
			<div style="display:none">
				<input type="hidden" name="systems[pri]" id="pri_systems" value="" />
				<input type="hidden" name="systems[sec]" id="sec_systems" value="" />
				<input type="hidden" name="debug" id="debug" value="<?php echo($debug)?"Y":"N"; ?>" />
				<input type="hidden" name="send" id="send" value="<?php echo($send)?"Y":"N"; ?>" />
				<input type="hidden" name="group_edit" id="group_edit" value="" />
			</div>
        </form>
    </div>
</div>
<script type="text/javascript">
	tab = $( "#tabs" ).tabs({ 
		<?php if(!$edit){ ?>	disabled: [ 2,3,4,5,6,7,8,9 ] <?php } ?>		
	});
	<?php if($edit){echo "checkgroup('$group');";} ?>
</script>
<?php require_once($footer_include); ?>