<?php
//error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
//ini_set('display_errors', 'On');

$send = true;
$insert_update = true;
$ldap_enable = true;
$debug=false;

if(isset($_GET['debug']) or $_POST['debug'] == "Y"){ 
	$debug=true; 
	if(isset($_GET['send']) or $_POST['send'] == "Y"){$send = true;}else{$send = false;}
}

if($settings->disable_email == '1'){
	$send = false;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/ldap.php');
$ldap = new ldapUtil();

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/php_utils.php');
$php_utils = new phpUtils();

if(isset($_SESSION['user_refresh_prevent'])){
	$insert_update = false;
	$log->loginfo('user refresh prevent',1035);
	if($debug){
		s("refresh prevented. insert_update disabled!");
	}else{
		header("Location:/".$loc."/");
	}
}
 
if($debug){
	d($_POST);
}

ob_start();

if(isset($_POST['userid_edit'])){
	$userid = $_POST['userid_edit'];
	$edit = true;	
	$sql="SELECT * FROM users WHERE uid = '$userid' LIMIT 1;";
	if(!$resultUser = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$rowUser = $resultUser->fetch_assoc();
}else{
	$userid = $_POST['userid'];
	$edit = false;
}

s($userid);

//Get default role id
$default_role_id = 0;
$sql="SELECT id FROM users_role_id WHERE role = '".$_POST['default_role']."';";
if(!$resultRoleId = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowRoleId = $resultRoleId->fetch_assoc();
$default_role_id = $rowRoleId['id'];	
d($rowRoleId);

//Get selected role's names
$sql="SELECT GROUP_CONCAT(name SEPARATOR '<br>') AS name FROM users_role_id WHERE role IN("."'".implode("','",array_keys($_POST['roles']))."'".");";
s($sql);
if(!$resultRoleNames = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowRoleNames = $resultRoleNames->fetch_assoc();
d($rowRoleNames);

//Get selected permission's names
if(count($_POST['perms']) != 0){
	$sql="SELECT GROUP_CONCAT(name SEPARATOR '<br>') AS name FROM users_perm_id WHERE perm IN("."'".implode("','",array_keys($_POST['perms']))."'".");";
	s($sql);
	if(!$resultPermNames = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$rowPermNames = $resultPermNames->fetch_assoc();
	d($rowPermNames);
}

//Get customer's unique id
$sql="SELECT unique_id FROM customers WHERE customer_id = '".$_POST['customer_id']."';";
s($sql);
if(!$resultCustomer = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
if($resultCustomer->num_rows > 0){
	$rowCustomer = $resultCustomer->fetch_assoc();
	$customer_unique_id = $rowCustomer['unique_id'];
}else{
	$customer_unique_id = '';
}
d($customer_unique_id);

//Get registration request unique id
if(!$edit){
	if($_POST['reg_id'] != ''){
		$sql="SELECT rr.unique_id FROM registration_requests AS rr WHERE rr.id = ".$_POST['reg_id'].";";
		s($sql);
		if(!$resultReg = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
		if($resultReg->num_rows > 0){
			$rowReg = $resultReg->fetch_assoc();
			$registration_unique_id = $rowReg['unique_id'];
		}else{
			$registration_unique_id = '';
		}
		s($registration_unique_id);
	}
}

$firstname=$_POST['firstname'];
$lastname=$_POST['lastname'];
$initials=strtoupper($_POST['initials']);
$email=$_POST['email'];
$cell=$_POST['cell'];
$carrier=$_POST['carrier'];
$address=$_POST['address'];
$city=$_POST['city'];
$state=$_POST['state'];
$zip=$_POST['zip'];
$full_addy = $address."&emsp;".$city.",".$state." ".$zip;

//Get cell carrier info
$carrier_company = "None";
if($carrier != ""){
	$sql="SELECT company FROM misc_carrier WHERE carrierid='$carrier';";
	s($sql);
	if(!$resultCarrier = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$rowCarrier = $resultCarrier->fetch_assoc();
	$carrier_company = $rowCarrier['company'];
}

if (isset($_POST['active'])){$active=strtoupper($_POST['active']);}else{$active="N";}

//Get lat/long for user's addy

if($address == "" or $city == "" or $state == "" or $zip == ""){
	if(isset($_POST['roles']['role_engineer']) or isset($_POST['roles']['role_contractor'])){
		$log->logerr('users_do.php',1036);
		if($debug){
			die("incomplete address");
		}else{
			$log->logerr('',1036,true,basename(__FILE__),__LINE__);
		}
	}
}

require($_SERVER['DOCUMENT_ROOT'].'/resources/php_geocode/GoogleMapsGeocoder.php');
$Geocoder = new GoogleMapsGeocoder();	
$geo_address = $address.", ".$city.", ".$state.", ".$zip;
$Geocoder->setAddress($geo_address);
$geo_response = $Geocoder->geocode();
if($debug){
	d($geo_response);	
}
if($geo_response['status'] == "OK"){
	$lat = $geo_response['results'][0]['geometry']['location']['lat'];
	$lng = $geo_response['results'][0]['geometry']['location']['lng'];	
	
	$jsonObject = file_get_contents("https://maps.googleapis.com/maps/api/timezone/json?location=".$lat.",".$lng."&timestamp=1331161200&key=AIzaSyDvmB-Iiut3rCFwWrvTKooa6H88GAw4rWs");
	$jsonObject = json_decode($jsonObject);
	$timezone = $jsonObject->timeZoneId;	
}else{
	$lat = 0;
	$lng = 0;	
	$timezone = 'America/New_York';
}


//Create temp password if needed
if(isset($_POST['pwd_reset']) or !$edit){
	$pwd_reset = true;
	$pwd=createRandomPassword();
	$salt = "STyf1anSTydajgnmd";
	$encrypted_password = "{MD5}" . base64_encode( pack( "H*", md5( $pwd . $salt ) ) );
	if($debug){s("password: ".$pwd);}
	$log->logmsg($userid,1037);
}else{
	$pwd_reset = false;	
}

//Clear secret if needed
if(isset($_POST['secret_reset'])){
	$log->logmsg($userid,1038);
	$secret_reset = true;	
}else{
	$secret_reset = false;
}


//User's Data
$sql="INSERT INTO users (uid, pwd, name, firstname, lastname, initials, active, address, city, state, zip, lat, lng, email, cell, carrier, timezone, default_role, 
customer_unique_id, registration_unique_id, created_by, created_date) 
VALUES ('$userid', '$encrypted_password', '".addslashes($firstname)." ".addslashes($lastname)."', '".addslashes($firstname)."', '".addslashes($lastname)."', 
'".addslashes($initials)."', '$active', '".addslashes($address)."', '".addslashes($city)."', '".addslashes($state)."', '$zip', '$lat', '$lng', '$email', '$cell', '$carrier', 
'$timezone', '$default_role_id', '$customer_unique_id', '$registration_unique_id', '".$_SESSION['login']."', '".date(storef, time())."') 
ON DUPLICATE KEY
UPDATE ";
if($pwd_reset){$sql.="pwd = '$encrypted_password', pwd_chg_date = NULL, logged_in = '0', pwd_chg_remind = 0, ";}
if($secret_reset){$sql.="secret_1 = NULL, secret_1_id = NULL, secret_2 = NULL, secret_2_id = NULL, ";}
$sql.="name = VALUES(name), firstname = VALUES(firstname), lastname = VALUES(lastname), initials = VALUES(initials), active = VALUES(active), address = VALUES(address),
city = VALUES(city), state = VALUES(state), zip = VALUES(zip), lat = VALUES(lat), lng = VALUES(lng), email = VALUES(email), cell = VALUES(cell), carrier = VALUES(carrier),
timezone = VALUES(timezone), default_role = VALUES(default_role), customer_unique_id = VALUES(customer_unique_id), registration_unique_id = VALUES(registration_unique_id), 
edited_by = '".$_SESSION['login']."', edited_date = '".date(storef, time())."';";
s($sql);
if($insert_update){
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
}else{
	echo "insert_update is false",EOL;	
}

//Users's groups
$sql="INSERT INTO users_groups (uid, gid)
	  VALUES('$userid', (SELECT id FROM users_group_id WHERE `group` = '".$_POST['group_edit']."')) ON DUPLICATE KEY 
	  UPDATE gid = (SELECT id FROM users_group_id WHERE `group` = '".$_POST['group_edit']."');";
s($sql);
if($insert_update){
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
}else{
	echo "insert_update is false",EOL;	
}

//User's roles
$sql="DELETE FROM users_roles WHERE uid = '$userid';";
s($sql);
if($insert_update){
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
}else{
	echo "insert_update is false",EOL;	
}
if(count($_POST['roles']) >= 1){
	$sql="INSERT INTO users_roles (uid, rid) VALUES ";
	foreach($_POST['roles'] as $key=>$value){
		$sql.="('$userid', (SELECT id FROM users_role_id WHERE role = '$key')), ";
	}
	$sql = rtrim($sql, ', ') . ';';
	s($sql);
	if($insert_update){
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
	}else{
		echo "insert_update is false",EOL;	
	}
}else{
	$log->logerr('User has no roles assigned',0,false,basename(__FILE__),__LINE__);
}

//User's Permissions

//Get old perms
$sql="SELECT upi.perm
FROM users_perms AS up
LEFT JOIN users_perm_id AS upi ON upi.id = up.pid
WHERE up.uid = '".$userid."';";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
if($result->num_rows > 0){
	while($row = $result->fetch_assoc()){
		$_POST['perms'][$row['perm']] = 'OLD';
	}
}
d($_POST['perms']);

$sql="DELETE FROM users_perms WHERE uid = '$userid';";
s($sql);
if($insert_update){
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
}else{
	echo "insert_update is false",EOL;	
}

if(count($_POST['perms']) >= 1){
	$sql="INSERT INTO users_perms (uid, pid) VALUES ";
	foreach($_POST['perms'] as $key=>$value){
		$sql.="('$userid', (SELECT id FROM users_perm_id WHERE perm = '$key')), ";
	}
	$sql = rtrim($sql, ', ') . ';';
	s($sql);
	if($insert_update){
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
	}else{
		echo "insert_update is false",EOL;	
	}
}else{
	$log->logerr('User has no permission assigned',0,false,basename(__FILE__),__LINE__);
}

//User's Preferences
if(!$edit){
	$sql = "INSERT INTO users_prefs (uid, pid) SELECT '".$userid."', upi.id	FROM users_pref_id AS upi;";
	if($insert_update){
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql, 1000, false, basename(__FILE__), __LINE__);
			$log->logerr('There was error running the query [' . $mysqli->error . ']', 1000, false, basename(__FILE__), __LINE__);
			$log->logerr('Error occurred, contact support.', 1000, true, basename(__FILE__), __LINE__);
		}
	}else{
		echo "insert_update is false", EOL;
	}
}else{
	if(count($_POST['perms']) > 0){
		foreach($_POST['perms'] as $key=>$val){
			if(strtolower($val) != 'old'){
				$sql="INSERT INTO users_prefs (uid, pid)
				SELECT '".$userid."', upi.id
				FROM users_pref_id AS upi
				WHERE upi.linked_perm = '".$key."';";
				s($sql);
				if($insert_update){
					if(!$result = $mysqli->query($sql)){
						$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
						$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
						$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
					}
				}else{
					echo "insert_update is false",EOL;
				}
			}
		}
	}
}

//User's Qualifications
$sql="DELETE FROM users_quals WHERE uid = '$userid';";
s($sql);
if($insert_update){
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
}else{
	echo "insert_update is false",EOL;	
}

if(count($_POST['quals']) >= 1){
	$sql="INSERT INTO users_quals (uid, qid) VALUES ";
	foreach($_POST['quals'] as $key=>$value){
		$sql.="('$userid', (SELECT id FROM users_qual_id WHERE qual = '$key')), ";
	}
	$sql = rtrim($sql, ', ') . ';';
	s($sql);
	if($insert_update){
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
	}else{
		echo "insert_update is false",EOL;	
	}
}else{
	$log->logerr('User has no qualifications assigned',0,false,basename(__FILE__),__LINE__);
}

//Update registration requests
if(!$edit){
	if($insert_update){
		$sql="UPDATE registration_requests SET registered = 'Y', used='Y' WHERE unique_id = '$registration_unique_id';";
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
	}else{
		echo "insert_update is false",EOL;		
	}
}

//Clean up auto logins
if($edit and $pwd_reset){
	$sql="DELETE FROM users_auto_logins WHERE uid='$userid';";	
	s($sql);
	if($insert_update){
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
	}else{
		echo "insert_update is false",EOL;	
	}
}

//delete users primary/secondary assigned systems if there are any
$sql="DELETE FROM systems_assigned_pri WHERE uid='$userid';";
s($sql);
if($insert_update){
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
}else{
	echo "insert_update is false",EOL;	
}
$sql="DELETE FROM systems_assigned_sec WHERE uid='$userid';";
s($sql);
if($insert_update){
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
}else{
	echo "insert_update is false",EOL;	
}

//delete customers assigned systems if there are any
$sql="DELETE FROM systems_assigned_customer WHERE uid='$userid';";
s($sql);
if($insert_update){
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
}else{
	echo "insert_update is false",EOL;	
}

//assign new systems
$rowPriSystemIds = array('system_ids'=>'');
$rowSecSystemIds = array('system_ids'=>'');
if(isset($_POST['roles']['role_engineer']) or isset($_POST['roles']['role_contractor']) or isset($_POST['roles']['role_customer'])){
	//Assign customer's systems
	if(isset($_POST['roles']['role_customer'])){
		if($_POST['systems']['pri'] == ""){
			if($debug){
				die("customer must have systems");
			}else{
				$log->logerr('',1039,true,basename(__FILE__),__LINE__);
			}
		}
		$sql="SELECT GROUP_CONCAT(system_id) AS system_ids FROM systems_base_cont WHERE ver_unique_id IN('".str_replace(",","','",$_POST['systems']['pri'])."');";
		s($sql);
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
		$rowPriSystemIds = $result->fetch_assoc();
		d($rowPriSystemIds);

		$customer_systems = explode(",",$_POST['systems']['pri']);
		d($pri_systems);
		$sql="INSERT INTO systems_assigned_customer (system_ver_unique_id, uid) VALUES ";
		foreach($customer_systems as $key=>$value){
			$sql.="('$value','$userid'),";	
		}
		$sql = rtrim($sql,',');
		$sql.=";";	
		s($sql);
		if($insert_update){
			if(!$result = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
				$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
			}	
		}else{
			echo "insert_update is false",EOL;	
		}
	//Assigne engineer's systems
	}else{
		if($_POST['systems']['pri'] == '' and $_POST['systems']['sec'] == '' and !isset($_POST['perms']['perm_no_assigned_systems'])){
			$log->logerr('',1040);
			if($debug){
				die("engineer/contractor does not have permission to have no assigned systems");
			}else{
				$log->logerr('',1040,true,basename(__FILE__),__LINE__);
			}
		}
		if(!isset($_POST['perms']['perm_no_assigned_systems'])){
			//Assigne primary systems
			if($_POST['systems']['pri'] != ''){
				$sql="SELECT GROUP_CONCAT(system_id) AS system_ids FROM systems_base_cont WHERE ver_unique_id IN('".str_replace(",","','",$_POST['systems']['pri'])."');";
				if(!$result = $mysqli->query($sql)){
					$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
					$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
					$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
				}
				s($sql);
				$rowPriSystemIds = $result->fetch_assoc();
				d($rowPriSystemIds);

				$sql="DELETE FROM systems_assigned_pri WHERE system_ver_unique_id IN('".str_replace(",","','",$_POST['systems']['pri'])."');";
				s($sql);
				if($insert_update){
					if(!$result = $mysqli->query($sql)){
						$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
						$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
						$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
					}
				}else{
					echo "insert_update is false",EOL;
				}

				$pri_systems = explode(",",$_POST['systems']['pri']);
				d($pri_systems);
				$sql="INSERT INTO systems_assigned_pri (system_ver_unique_id, uid) VALUES ";
				foreach($pri_systems as $key=>$value){
					$sql.="('$value','$userid'),";	
				}
				$sql = rtrim($sql,',');
				$sql.=";";	
				s($sql);		
				if($insert_update){
					if(!$result = $mysqli->query($sql)){
						$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
						$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
						$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
					}
				}else{
					echo "insert_update is false",EOL;	
				}
			}
			//Assigne secondary systmes
			if($_POST['systems']['sec'] != ''){
				$sql="SELECT GROUP_CONCAT(system_id) AS system_ids FROM systems_base_cont WHERE ver_unique_id IN('".str_replace(",","','",$_POST['systems']['sec'])."');";
				s($sql);
				if(!$result = $mysqli->query($sql)){
					$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
					$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
					$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
				}
				$rowSecSystemIds = $result->fetch_assoc();
				d($rowSecSystemIds);

				$sql="DELETE FROM systems_assigned_sec WHERE system_ver_unique_id IN('".str_replace(",","','",$_POST['systems']['sec'])."');";
				s($sql);
				if($insert_update){
					if(!$result = $mysqli->query($sql)){
						$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
						$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
						$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
					}
				}else{
					echo "insert_update is false",EOL;
				}

				$sec_systems = explode(",",$_POST['systems']['sec']);
				d($sec_systems);
				$sql="INSERT INTO systems_assigned_sec (system_ver_unique_id, uid) VALUES ";
				foreach($sec_systems as $key=>$value){
					$sql.="('$value','$userid'),";	
				}
				$sql = rtrim($sql,',');
				$sql.=";";	
				s($sql);
				if($insert_update){
					if(!$result = $mysqli->query($sql)){
						$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
						$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
						$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
					}	
				}else{
					echo "insert_update is false",EOL;	
				}
			}
		}
	}
}

//Update ldap server
if($ldap_enable){
	if($debug){echo "Running ldap operations...",EOL;}
	if(strtolower($active) == 'y'){
		if($_POST['group_edit'] == 'grp_employee'){
			if(isset($_POST['perms']['perm_dms_access'])){
				$dms = true;
			}else{
				$dms = false;
			}
			if(isset($_POST['perms']['perm_kb_access'])){
				$kb = true;
			}else{
				$kb = false;
			}
			
			$log->logmsg('users_do.php',1041);
			$ldap = new ldapUtil();
			
			if($pwd_reset){
				$ldap_password = $encrypted_password;
			}else{
				$ldap_password = $rowUser['pwd'];
			}
			
			$ldap->user($userid,$ldap_password,$firstname." ".$lastname,$lastname,$email,$kb,$dms);
		}else{
			if($debug){echo "User is not of employee group",EOL;}	
		}
	}else{
		if($debug){echo "Deleting user: ".$userid,EOL;}
		$ldap->deluser($userid);
	}
}


$portal = (isset($_POST['roles']['role_customer']))?"Customer":"Service";
$customer = (isset($_POST['roles']['role_customer']))?true:false;
$no_systems = (isset($_POST['perms']['perm_no_assigned_systems']))?true:false;


$args = array('uid'=>$userid);
if($edit){$args['edit'] = true;}
if($customer){$args['customer'] = true;}
if($pwd_reset){
	$args['pwd_reset'] = true;
	$args['pwd'] = $pwd;
}
d($args);
$php_utils->email_action_add($mysqli, "/report/common/users_send.php", $args, $_SESSION['login']);

if($customer){
	
$systems_html = <<<HTML_SystemS_C
<tr>
	<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
</tr>
<tr>
	<td width="50%" align="right">Systems:</td>
	<td>{$rowPriSystemIds['system_ids']}</td>
</tr>
HTML_SystemS_C;
	
}else{
if($no_systems){
	$systems_html = "";
	$systems_text = "";
}else{
	$rowPriSystemIds_pretty = wordwrap($rowPriSystemIds['system_ids'],50,'<br>',true);
	$rowSecSystemIds_pretty = wordwrap($rowSecSystemIds['system_ids'],50,'<br>',true);

$systems_html = <<<HTML_SystemS_EC
<tr>
	<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
</tr>
<tr>
	<td width="50%" align="right">Primary Assigned Systems:</td>
	<td>{$rowPriSystemIds_pretty}</td>
</tr>
<tr>
	<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
</tr>
<tr>
	<td width="50%" align="right">Secondary Assigned Systems:</td>
	<td>{$rowSecSystemIds_pretty}</td>
</tr>
HTML_SystemS_EC;

}
}

$html_new_page = <<<HTML_NEW_PAGE
<table cellpadding="10" cellspacing="0" border="0" width="100%">
	<tr>
		<td colspan="2" align="center" style="color:#F79447"><h1>{$firstname} {$lastname} add to the OiHealthcare {$portal} Portal</h1></td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="50%" align="right">Username:</td>
		<td>{$userid}</td>
	</tr>
	<tr>
		<td width="50%" align="right">Temporary Password:</td>
		<td style="color:#F00">{$pwd}</td>
	</tr>
	<tr>
		<td width="50%" align="right">First Name:</td>
		<td>{$firstname}</td>
	</tr>
	<tr>
		<td width="50%" align="right">Last Name:</td>
		<td>{$lastname}</td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td colspan="2" align="center" style="color:#1B2673"><h3>This page will return to the dashboard in 10 seconds</h3></td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
</table>
HTML_NEW_PAGE;

$html_update_page = <<<HTML_UPDATE_PAGE
<table cellpadding="10" cellspacing="0" border="0" width="100%">
	<tr>
		<td colspan="2" align="center" style="color:#F79447"><h1>{$firstname}&rsquo;s OiHealthcare {$portal} Portal user profile updated</h1></td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="50%" align="right">Active User:</td>
		<td>{$active}</td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="50%" align="right">Username:</td>
		<td>{$userid}</td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="50%" align="right">First Name:</td>
		<td>{$firstname}</td>
	</tr>
	<tr>
		<td width="50%" align="right">Last Name:</td>
		<td>{$lastname}</td>
	</tr>
	<tr>
		<td width="50%" align="right">Address:</td>
		<td>{$full_addy}</td>
	</tr>
	<tr>
		<td width="50%" align="right">Cell / Phone:</td>
		<td>{$cell}</td>
	</tr>
	<tr>
		<td width="50%" align="right">Cell Carrier:</td>
		<td>{$carrier_company}</td>
	</tr>
	<tr>
		<td width="50%" align="right">Email Address:</td>
		<td>{$email}</td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="50%" align="right">Roles Assigned:</td>
		<td>{$rowRoleNames['name']}</td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="50%" align="right">Permissions Granted:</td>
		<td>{$rowPermNames['name']}</td>
	</tr>
	{$systems_html}
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td colspan="2" align="center" style="color:#1B2673"><h3>This page will return to the dashboard in 10 seconds</h3></td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
</table>
HTML_UPDATE_PAGE;

if($edit){
	$html_page = $html_update_page;
}else{
	$html_page = $html_new_page;
}

function createRandomPassword() {
    $chars = "abcdefghjkmnpqrstuvwxyz23456789";
    srand((double)microtime()*1000000);
    $i = 0;
    $pass = '' ;
    while ($i <= 7) {
        $num = rand() % 33;
        $tmp = substr($chars, $num, 1);
        $pass = $pass . $tmp;
        $i++;
    }
    return $pass;
}

?> 
<!DOCTYPE html>
<html>
<head>
<?php require_once($head_include);?>
<?php require_once($css_include);?>
<?php require_once($js_include);?>

<script type="text/javascript">
$(document).ready(function() {
 		
});
</script>
<script type="text/javascript">
function delayer(){
	<?php if(!$debug){?>
		window.location = "<?php echo $refering_uri; ?>"
	<?php } ?>    
}
</script>
</head>
<body onLoad="setTimeout('delayer()',10000);">
<?php require_once($header_include); ?>
<div id="OIReportContent"> 

<?php echo $html_page; ?>

</div>
<?php require_once($footer_include); ?>
