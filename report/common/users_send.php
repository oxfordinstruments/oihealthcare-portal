<?php

//error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
//ini_set('display_errors', 'On');
ignore_user_abort(true);
set_time_limit(30);

$debug = false;

if(substr(php_sapi_name(), 0, 3) == 'cli' || empty($_SERVER['REMOTE_ADDR'])) {
	//shell
	$cron = false;
	if(isset($_SERVER['argv'][1]) and $_SERVER['argv'][1] == '--cron'){
		$cron = true;
	}
	$docroot = preg_replace("/(public_html).*/",'public_html',dirname(__FILE__),2);
	$no_define_eol = true;
	define('EOL',PHP_EOL);
} else {
	//webserver
	$cron = false;
	$docroot = $_SERVER['DOCUMENT_ROOT'];
	define('EOL','<br />');
}

$settings = new SimpleXMLElement($docroot.'/settings.xml', null, true);
require_once($docroot.'/define_inc.php');
require_once($docroot.'/mysqlInfo.php');
require_once($docroot.'/log/log.php');
$log = new logger($docroot);
require_once($docroot.'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}
require_once($docroot.'/report/common/scripts/php_utils.php');
$php_utils = new phpUtils($docroot, $debug);

$send = true;
if(intval($settings->disable_email) == 1){
	$send = false;
}

if(!isset($_POST['uid'])){
	exit('UID not set');
}
$uid = $_POST['uid'];

require_once($docroot.'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

$edit = false;
if(isset($_POST['edit'])){
	$edit = true;
}

$pwd_reset = false;
$pwd = '';
if(isset($_POST['pwd_reset']) or !$edit){
	if(!isset($_POST['pwd'])){
		exit('Password not set');
	}
	$pwd_reset = true;
	$pwd = $_POST['pwd'];

}

require_once($docroot.'/resources/smarty/libs/Smarty.class.php');
$smarty = new Smarty();
$smarty->setTemplateDir($_SERVER['DOCUMENT_ROOT'].$settings->email_templates);
$smarty->setCompileDir($_SERVER['DOCUMENT_ROOT'].$settings->email_template_c);
$smarty->setCacheDir($_SERVER['DOCUMENT_ROOT'].$settings->email_cache);
$smarty->setConfigDir($_SERVER['DOCUMENT_ROOT'].$settings->email_configs);
$smarty->use_sub_dirs = true;
$smarty->force_compile = true;
//$smarty->debugging = true;
$smarty->caching = false;


if($debug){
	d($_POST);
}

ob_start();


$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno) {die('There was an error running the query [' . $mysqli->connect_error . ']');}

$sql="SELECT * FROM users WHERE uid = '".$_POST['uid']."' LIMIT 1;";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	exit("SQL ERROR");
}
if($result->num_rows == 0){
	exit('User '.$_POST['uid'].' not found');
}
$rowUser = $result->fetch_assoc();
d($rowUser);

//Get default role id
$default_role_id = 0;
$sql="SELECT id FROM users_role_id WHERE role = '".$rowUser['default_role']."';";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$row = $result->fetch_assoc();
$default_role_id = $row['id'];
d($row);

//Get selected role's names
$rowRoleNames = array();
$sql="SELECT urid.role, urid.name
FROM users_roles AS ur
LEFT JOIN users_role_id AS urid ON urid.id = ur.rid
WHERE ur.uid = '".$_POST['uid']."';";
s($sql);
if(!$result= $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

$roles = array();
while($row = $result->fetch_assoc()){
	$roles[$row['role']] = $row['name'];
}
d($roles);

//Get selected permission's names
$rowPermNames = array();
$sql="SELECT upid.perm, upid.name
FROM users_perms AS up
LEFT JOIN users_perm_id AS upid ON upid.id = up.pid
WHERE up.uid = '".$_POST['uid']."';";
s($sql);
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$perms = array();
while($row = $result->fetch_assoc()){
	$perms[$row['perm']] = $row['name'];
}
d($perms);


$firstname=$rowUser['firstname'];
$lastname=$rowUser['lastname'];
$initials=strtoupper($rowUser['initials']);
$email=$rowUser['email'];
$cell=$rowUser['cell'];
$carrier=$rowUser['carrier'];
$address=$rowUser['address'];
$city=$rowUser['city'];
$state=$rowUser['state'];
$zip=$rowUser['zip'];
$full_addy = $address."&emsp;".$city.",".$state." ".$zip;

$rowPriSystemIds = array();
$rowSecSystemIds = array();

if(( isset($roles['role_engineer']) or isset($roles['role_contractor']) or isset($roles['role_customer']) ) and !isset($perms['perm_no_assigned_systems'])){
	if(isset($roles['role_customer'])){
		$customer = true;
		$sql = "SELECT GROUP_CONCAT(sbc.system_id) AS system_ids
		FROM systems_assigned_customer AS sac
		RIGHT JOIN systems_base_cont AS sbc ON sbc.ver_unique_id = sac.system_ver_unique_id
		WHERE sac.uid = '".$_POST['uid']."';";
		s($sql);
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql, 1000, false, basename(__FILE__), __LINE__);
			$log->logerr('There was error running the query [' . $mysqli->error . ']', 1000, false, basename(__FILE__), __LINE__);
			$log->logerr('Error occurred, contact support.', 1000, true, basename(__FILE__), __LINE__);
		}
		$rowPriSystemIds = $result->fetch_assoc();
	}else{
		$sql = "SELECT GROUP_CONCAT(sbc.system_id) AS system_ids
		FROM systems_assigned_pri AS sap
		RIGHT JOIN systems_base_cont AS sbc ON sbc.ver_unique_id = sap.system_ver_unique_id
		WHERE sap.uid = '" . $_POST['uid'] . "';";
		s($sql);
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql, 1000, false, basename(__FILE__), __LINE__);
			$log->logerr('There was error running the query [' . $mysqli->error . ']', 1000, false, basename(__FILE__), __LINE__);
			$log->logerr('Error occurred, contact support.', 1000, true, basename(__FILE__), __LINE__);
		}
		$rowPriSystemIds = $result->fetch_assoc();


		$sql = "SELECT GROUP_CONCAT(sbc.system_id) AS system_ids
		FROM systems_assigned_sec AS ses
		RIGHT JOIN systems_base_cont AS sbc ON sbc.ver_unique_id = ses.system_ver_unique_id
		WHERE ses.uid = '" . $_POST['uid'] . "';";
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql, 1000, false, basename(__FILE__), __LINE__);
			$log->logerr('There was error running the query [' . $mysqli->error . ']', 1000, false, basename(__FILE__), __LINE__);
			$log->logerr('Error occurred, contact support.', 1000, true, basename(__FILE__), __LINE__);
		}
		if($result->num_rows > 0){
			$rowSecSystemIds = $result->fetch_assoc();
		}
	}
}

d($rowPriSystemIds);
d($rowSecSystemIds);


$active = (isset($rowUser['active']))?strtoupper($rowUser['active']):"N";
$portal = $customer?"Customer":"Service";
$no_systems = (isset($perms['perm_no_assigned_systems']))?true:false;

$smarty->assign('company_name',$settings->company_name);
$smarty->assign('copyright_date',$settings->copyright_date);
$smarty->assign('email_pics',$settings->email_pics);

if($customer){
	$smarty->assign('portal','Customer');
	$smarty->assign('customer',true);
	$smarty->assign('systems',wordwrap($rowPriSystemIds['system_ids'],50,'<br>',true));
}else{
	if($no_systems){
		$smarty->assign('no_systems',$no_systems);
	}else{
		$smarty->assign('pri_systems', wordwrap($rowPriSystemIds['system_ids'],50,'<br>',true));
		$smarty->assign('sec_systems', wordwrap($rowSecSystemIds['system_ids'],50,'<br>',true));
	}
}



if($pwd_reset){
	$smarty->assign('pwd',$pwd);
}

if($edit){
	$smarty->assign('active',$active);
	$smarty->assign('uid',$uid);
	$smarty->assign('firstname',$firstname);
	$smarty->assign('lastname',$lastname);
	$smarty->assign('addy',$full_addy);
	$smarty->assign('cell',$cell);
	$smarty->assign('carrier',$carrier_company);
	$smarty->assign('email',$email);
	$smarty->assign('roles',implode('<br>',$roles));
	$smarty->assign('perms',implode('<br>',$perms));
	$smarty->assign('pri_systems',wordwrap($rowPriSystemIds['system_ids'],49,'<br>',true));
	$smarty->assign('sec_systems',wordwrap($rowSecSystemIds['system_ids'],49,'<br>',true));
//	$smarty->assign('errors',$errors);
	$tpl = 'users_update.tpl';	
	$subject = (string)$settings->company_name.' Portal profile update for '.$uid;
}else{
	$smarty->assign('uid',$uid);
	$smarty->assign('pwd',$pwd);
	$smarty->assign('firstname',$firstname);
	$smarty->assign('lastname',$lastname);
	//$smarty->assign('errors',$errors);
	$tpl = 'users_new.tpl';
	$subject = $uid.' added to '.(string)$settings->company_name.' Portal';
}


$errors = "";

require_once($docroot.'/resources/PHPMailer/PHPMailerAutoload.php');

$mail = new PHPMailer;

$mail->IsSMTP();
$mail->Host = (string)$settings->email_host;
$mail->SMTPAuth = true;
$mail->Username = (string)$settings->email_users;
$mail->Password = (string)$settings->email_password;
$mail->SMTPSecure = 'tls';
$mail->From = (string)$settings->email_users;
$mail->FromName = 'Users '.$settings->short_name.' Portal';
if($debug){
	$mail->AddAddress((string)$settings->email_support, $firstname." ".$lastname);	
}else{
	$mail->AddAddress($email, $firstname." ".$lastname);
}
$mail->AddReplyTo((string)$settings->email_support, 'Support '.$settings->short_name.' Portal');
$mail->AddCC((string)$settings->email_support);
$mail->IsHTML(true);
$mail->Subject = $subject;
$mail->Body = $smarty->fetch($tpl);
if($debug){
	echo EOL,"Email:",EOL,$mail->Body,EOL;	
}
if($send and strtolower($active) == 'y'){
	if(!$mail->Send()){
	    $errors .= "Email could not be sent<br>";
	    $errors .= 'Mailer Error: ' . $mail->ErrorInfo."<br>";
		$log->logerr($errors.'  Mailer Error: ' . $mail->ErrorInfo,1053,false,basename(__FILE__),__LINE__);
		exit($errors);
	}else{
		$errors .= 'User has been notified via email.<br>';	
		$log->loginfo($errors ."  ". implode('; ',array_keys($mail->getAllRecipientAddresses())),200,false,basename(__FILE__),__LINE__);
		exit('OK');
	}
	
}else{
	exit('OK');
}