<?php
//Update Completed 12/12/14
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

$comp_word_limt 	 = intval($settings->complaint_word_limit);
$serv_word_limt		 = intval($settings->service_word_limit);
$custact_word_limt	 = intval($settings->custact_word_limit);
$probrpt_word_limt	 = intval($settings->probrpt_word_limit);

$sql = "SELECT * FROM users WHERE uid='$myusername';";
if(!$resultUser = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowUser = $resultUser->fetch_assoc();
$emailReqStarted = false;

if(isset($_SESSION['email_request'])){
	$emailReqNum = $_SESSION['email_request'];
	unset($_SESSION['email_request']);
	$sql="SELECT COUNT(*) AS `count`,unique_id,system_nickname,system_id FROM system_requests WHERE request_num = '".$emailReqNum."' AND `status` = 'open';";
	if(!$resultEmailReq = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$rowEmailReq = $resultEmailReq->fetch_assoc();
	if($rowEmailReq['count'] == '1'){
		header("location:/report/common/report_new_edit.php?id=".$rowEmailReq['unique_id']."&uid=".$myusername);	
	}else{
		$sql="SELECT unique_id,system_nickname,system_id FROM system_requests WHERE request_num = '".$emailReqNum."';";
		if(!$resultEmailReq = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
		$rowEmailReq = $resultEmailReq->fetch_assoc();
		$emailReqStarted = true;
	}
}

$sql="SELECT * FROM `systems_status`";
if(!$resultStatus = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);	
}
$arr_status = array();
while($rowStatus = $resultStatus->fetch_assoc())
{
	array_push_assoc($arr_status, $rowStatus['id'], $rowStatus['status']);
}

$sql="SELECT u.uid, u.name
FROM users AS u
LEFT JOIN users_roles AS ur ON ur.uid = u.uid
LEFT JOIN users_role_id AS rid ON rid.id = ur.rid
WHERE rid.role = 'role_engineer' OR rid.role = 'role_contractor';";
if(!$resultEngineers = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$arr_engineers = array();
while($rowEngineers = $resultEngineers->fetch_assoc())
{
	array_push_assoc($arr_engineers, $rowEngineers['uid'], $rowEngineers['name']);
}

function array_push_assoc(&$array, $key, $value){
	$array[$key] = $value;
	return $array;
}
function limit_words($string, $word_limit)
{
	$words = explode(" ",$string);
	if(count($words) > $word_limit){
		return implode(" ",array_splice($words,0,$word_limit))."...(more)";
	}else{
		return implode(" ",array_splice($words,0,$word_limit));
	}
}

?>
<!DOCTYPE html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<?php require_once($js_include);?>

<script type="text/javascript">
/////////////////////////////////////////////////////////////////////////////////////	
	function getCookies(){
		var showall=getCookie("showall");
		if (showall!=null && showall!="")
		{
			document.getElementById("showallcb").checked = true;
		}else{
			document.getElementById("showallcb").checked = false;
		}
	}

/////////////////////////////////////////////////////////////////////////////////////
	$(document).ready(function() {
		
		openTable = $('#openReportsTable').dataTable({
			"bJQueryUI": true,
			//"bStateSave": true,
			"iCookieDuration": 60*60*24*365, // 1 year
			"sPaginationType": "full_numbers",
			"aaSorting": [],
			 "fnInitComplete": function () {
           		 this.$('tr').click( function () {
               		 var href = $(this).find("a").attr("href");
					 if(href) {
						window.location = href;
					 }
           		 });
       		 }
		});
/////////////////////////////////////////////////////////////////////////////////////		
		pastTable = $('#pastReportsTable').dataTable({
			"bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "/report/common/connectors/completed_reports_connector.php",
			"bJQueryUI": true,
			"aLengthMenu": [[5, 10, 25, 50], [5, 10, 25, 50]],
			"iCookieDuration": 60*60*24*365, // 1 year
			"iDisplayLength": 5,
			"sPaginationType": "full_numbers",
			"aaSorting": [ [2,'desc'] ], 			
			"aoColumnDefs": [
                        { "bSearchable": false, "bVisible": false, "aTargets": [ 0 ] }
            ],
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
					//$('tr:eq(0)', nRow).addClass("pastReportsHelp");
					$('td:eq(0)', nRow).html( function() {	
						var moment_dt = moment($(this).text()).tz(aData[9]);
						return "<a href='/report/common/report_view.php?id=" + aData[8] + "&uid=<?php echo $myusername ?>'>" + moment_dt.format('<?php echo jsdispfd; ?>') + "</a>";
					});
					this.$('tr').click( function () {
               		 var href = $(this).find("a").attr("href");
					 if(href) {
						window.location = href;
					 }
           		 });
			},
			"fnInitComplete": function () {
           		 this.$('tr').addClass("pastReportsHelp");
       		},
			"fnServerParams": function ( aoData ) {
			  aoData.push( { "name": "oiuid", "value": "<?php echo $myusername ?>" },
			  			   { "name": "oiall", "value": "0"} ); //change to 0 for just engineer
			}
		});
/////////////////////////////////////////////////////////////////////////////////////		
		reqestTable = $('#requestsTable').dataTable({
			"bJQueryUI": true,
			//"bStateSave": true,
			"iCookieDuration": 60*60*24*365, // 1 year
			"sPaginationType": "full_numbers",
			"aaSorting": [],
			"fnInitComplete": function () {
           		 this.$('tr').click( function () {
               		 var href = $(this).find("a").attr("href");
					 if(href) {
						window.location = href;
					 }
           		 });
       		 }
		});
/////////////////////////////////////////////////////////////////////////////////////		
		pmTable = $('#pmTable').dataTable({
			"bJQueryUI": true,
			//"bStateSave": true,
			"iCookieDuration": 60*60*24*365, // 1 year
			"sPaginationType": "full_numbers",
			"aaSorting": [],
			"aoColumnDefs": [
			  {"aTargets": [ 0 ], "bSortable": true, "sType": "date"}
			],
			 "fnInitComplete": function () {
           		 this.$('tr').click( function () {
               		 var href = $(this).find("a").attr("href");
					 if(href) {
						window.location = href;
					 }
           		 });
       		 }
		});
		jQuery.fn.dataTableExt.oSort['us_date-asc']  = function(a,b) {
			var x = new Date(a),
			y = new Date(b);
			return ((x < y) ? -1 : ((x > y) ?  1 : 0));
		};
		
		jQuery.fn.dataTableExt.oSort['us_date-desc'] = function(a,b) {
			var x = new Date(a),
			y = new Date(b);
			return ((x < y) ? 1 : ((x > y) ?  -1 : 0));
		};

		<?php
		if($emailReqStarted){
		?>
		alert("The service report for <?php echo $rowEmailReq['system_nickname']." (".$rowEmailReq['system_id'].")"; ?> has already been finalized.");
		<?php
			}
		?>
/////////////////////////////////////////////////////////////////////////////////////
		$("div#requestsTable_filter").children("label").children("input").focus();

		$(document).keyup(function(e) {
			$("#requestsTable").each(function(index, element) {
				if($(this).children("tbody").children("tr").length == 1 && e.which == 13){
					$(this).find("td > a").click();
				}
			});
			
			$("#openReportsTable").each(function(index, element) {
				if($(this).children("tbody").children("tr").length == 1 && e.which == 13){
					$(this).find("td > a").click();
				}
			});			
		});
});
/////////////////////////////////////////////////////////////////////////////////////
</script>
</head>
<body">
<?php require_once($header_include); ?>   
<div id="OIReportContent"> 
        <div id="ReportOuterFrame">
	        <?php
				$sql="SELECT r.*, sbc.credit_hold, sbc.pre_paid, sbc.property, sb.status AS cur_system_status
							FROM systems_requests AS r
							LEFT JOIN systems_base_cont AS sbc ON sbc.ver_unique_id = r.system_ver_unique_id
							LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
							WHERE r.`status`='open' AND `engineer`='$myusername' AND r.`deleted` = 'n'
							ORDER BY r.id ASC;";
				if(!$resultRequests = $mysqli->query($sql)){
					$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
					$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
					$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
				}
			?>  
            <div id="ReportFrame">     
            <div id="ReportFrameTitle"> Open Service Requests </div>
            <div id="requestsDiv" <?php if($resultRequests->num_rows == 0){echo "style=\"display:none\"";} ?>>
                <table width="100%" id="requestsTable" >
                    <thead>
                        <tr>
                        	<th width="75px">Date</th> 
                        	<th width="50px">Request Number</th>                          
                            <th width="50px">System ID</th>
                            <th>System Name</th>
                            <th width="300">Problem</th>
                            <?php if(isset($_COOKIE["showall"])){echo "<th>Engineer</th>";}else{echo "<th>Cust Act</th>";} ?>
                            <th width="125px">Status</th>     
                            <th width="50px">Report Started</th>                   
                        </tr>
                    </thead>
                    <tbody>
                    	<?php
						if($resultRequests->num_rows != 0){
							while($rowRequest = $resultRequests->fetch_assoc())
							{
								//echo "<tr class=\"requestsHelp\">\n";
								echo "<tr>\n";
								if(strtolower($rowRequest['report_started'])=="y"){
									echo "<td><a href=\"/report/common/report_new_edit.php?id=".$rowRequest['unique_id'].'&uid='.$myusername."\">". date(phpdispfd,strtotime($rowRequest['request_date']))."</a></td>\n";
								}else{
									echo "<td><a href=\"/report/common/report_new_edit.php?id=".$rowRequest['unique_id'].'&uid='.$myusername."\">". date(phpdispfd,strtotime($rowRequest['request_date']))."</a></td>\n";
								}
								echo "<td>". $rowRequest['request_num']."</td>\n";
								echo "<td>". $rowRequest['system_id']."</td>\n";
								echo "<td>". $rowRequest['system_nickname']."</td>\n";
								echo "<td>". limit_words($rowRequest['problem_reported'],$probrpt_word_limt)."</td>\n";
								if(isset($_COOKIE["showall"]) or isset($_SESSION['adm']) or isset($_SESSION['mgt'])){
									echo "<td>". $arr_engineers[$rowRequest['engineer']]."</td>\n";
								}else{
									echo "<td>". limit_words($rowRequest['customer_actions'],$custact_word_limt)."</td>\n";
								}
								echo "<td>". $arr_status[$rowRequest['cur_system_status']]."</td>\n";
								echo "<td>"; if(strtolower($rowRequest['report_started'])=="y"){echo "Yes";} echo "</td>\n";
								echo "</tr>\n";
							}
						}
						?>                        
                    </tbody>
                </table>
            </div>
            <div id="requestDiv" <?php if($resultRequests->num_rows != 0){echo "style=\"display:none\"";}else{echo "style=\"text-align:center\"";} ?>>
            	<br />
            	<h2>No Service Requests for <?php echo $rowUser['name'] ?></h2>
            </div>
          </div>
        </div>
        <div class="line"></div>
        <div id="ReportOuterFrame">
	        <?php  
				$sql = "SELECT * FROM systems_reports WHERE `status`='open' and user_id='$myusername' ORDER BY `id` ASC";
				if(!$resultOpen = $mysqli->query($sql)){
					$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
					$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
					$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
				}
			?>  
            <div id="ReportFrame">     
            <div id="ReportFrameTitle"> Unfinished Reports </div>
            <div id="openReprotsDiv" <?php if($resultOpen->num_rows == 0){echo "style=\"display:none\"";} ?>>
                <table width="100%" id="openReportsTable" >
                    <thead>
                        <tr>
                            <th width="75px">Date Started</th>
                            <th width="50px">Report ID</th>
                            <th width="50px">System ID</th>
                            <th>System Name</th>
                            <th>Engineer</th>
                            <th>Complaint</th>                   
                        </tr>
                    </thead>
                    <tbody>
                    	<?php
						if($resultOpen){
							while($rowOpen = $resultOpen->fetch_assoc())
							{
								//echo "<tr class=\"openReportsHelp\">\n";
								echo "<tr>\n";
								echo "<td><a href=\"/report/common/report_new_edit.php?id=".$rowOpen['unique_id'].'&uid='.$myusername."\">". date(phpdispfd,strtotime($rowOpen['date']))."</a></td>\n";
								echo "<td>". $rowOpen['report_id']."</td>\n";
								echo "<td>". $rowOpen['system_id']."</td>\n";
								echo "<td>". $rowOpen['system_nickname']."</td>\n";
								echo "<td>". $arr_engineers[$rowOpen['assigned_engineer']]."</td>\n";
								echo "<td>". limit_words($rowOpen['complaint'],$comp_word_limt)."</td>\n";
								echo "</tr>\n";
							}
						}
						?>                        
                    </tbody>
                </table>
            </div>
            <div id="openReportsDiv" <?php if($resultOpen->num_rows != 0){echo "style=\"display:none\"";}else{echo "style=\"text-align:center\"";} ?>>
            	<br />
                <h2>No Unfinished Reports for <?php echo $rowUser['name'];?></h2>
            </div>
          </div>
        </div>
         <div class="line"></div>
     <?php 
				$sql = "SELECT sb.system_id, sbc.nickname, DATE_FORMAT(sb.last_pm,'".$settings->mysql_disp_date."') as last_pm, u.name AS engineer, DATE_ADD(DATE(sb.last_pm), INTERVAL sb.pm_freq MONTH) AS due
						FROM systems_base_cont AS sbc
						LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
						LEFT JOIN systems_assigned_pri AS a ON sbc.ver_unique_id = a.system_ver_unique_id
						LEFT JOIN users AS u ON a.uid = u.uid
						WHERE DATEDIFF(DATE_ADD(DATE(sb.last_pm), INTERVAL sb.pm_freq MONTH), CURDATE()) < ".$settings->pm_table_lookahead." AND sb.pm_freq > 0  AND a.uid = '".$myusername."'
						ORDER BY due ASC;";
				if(!$resultPM = $mysqli->query($sql)){
					$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
					$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
					$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
				}
		?>
    <div id="ReportFrame">
        <div id="ReportFrameTitle">PMs Due</div>
        <div id="pmDiv" <?php if($resultPM->num_rows == 0){echo "style=\"display:none\"";} ?>>
            <table width="100%" id="pmTable" >
                <thead>                    
                <th width="75px">Due Date</th>
                        <th>Last PM</th>
                        <th>System ID</th>
                        <th width="40%">System Nickname</th>
                        <th>Engineer</th>
                                            
                </thead>
                <tbody>
                    <?php
						while($rowPM = $resultPM->fetch_assoc())
						{
							//echo "<tr class=\"pmDueHelp\">\n";
							echo "<tr>\n";
							if(strtolower($rowUser['make_request']) == "y"){
								echo "<td><a href=\"/report/common/request_new_edit.php?system_id=".$rowPM['system_id']."&user_id=".$myusername."&pm\">". date(phpdispfd,strtotime($rowPM['due']))."</a></td>\n";
							}else{
								echo "<td><a onClick=\"alert('Not Finished Yet');\">". $rowPM['due']."</a></td>\n";	
							}
							echo "<td>". $rowPM['last_pm']."</td>\n";
							echo "<td>". $rowPM['system_id']."</td>\n";
							echo "<td>". $rowPM['nickname']."</td>\n";
							echo "<td>". $rowPM['engineer']."</td>\n";							
							echo "</tr>\n";
						}
						?>     					
                </tbody>
            </table>
        </div>
        <div id="pmDiv" <?php if($resultPM->num_rows != 0){echo "style=\"display:none\"";}else{echo "style=\"text-align:center\"";} ?>>
            	<br />
                <h2>No PMs Due for <?php echo $rowUser['name'];?></h2>
        </div>
    </div> 
		<div class="line"></div>
			<div id="ReportFrame">
			<div id="ReportFrameTitle">Completed Reports</div>
			<div id="pastReprotsDiv">
				<table width="100%" id="pastReportsTable" >
					<thead>
						<tr>
							<th width="1px">idx</th>
							<th width="75px">Date</th>
							<th width="50px">Report ID</th>
							<th width="50px">System ID</th>
							<th width="100px">System Nickname</th>
							<th width="75px">Engineer</th>
							<th width="150px">Complaint</th>
							<th>Service</th>
						</tr>
					</thead>
					<tbody>    
					</tbody>
				</table>
			</div>
		</div>
</div>
    </div>
    <br> 
</div>
<?php require_once($footer_include); ?>
