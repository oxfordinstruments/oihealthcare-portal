<?php
$debug = false;
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

$custrpt_word_limt = intval($settings->custrpt_word_limit);

$credit_hold = array();

$sql = "SELECT sb.*, sbc.*, ss.flag, u.name AS engineer, f.name AS facility_name, st.name AS system_type_name
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sbc.unique_id = sb.unique_id
LEFT JOIN systems_assigned_customer AS a ON a.system_ver_unique_id = sbc.ver_unique_id
LEFT JOIN systems_status AS ss ON ss.id = sb.`status`
LEFT JOIN systems_assigned_pri AS ps ON ps.system_ver_unique_id = sbc.ver_unique_id
LEFT JOIN users AS u ON ps.uid = u.uid
LEFT JOIN facilities AS f ON f.unique_id = sbc.facility_unique_id
LEFT JOIN systems_types AS st ON sb.system_type = st.id
WHERE a.uid = '".$_SESSION['login']."'
ORDER BY sb.system_id ASC;";
if(!$resultSites = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$systemsArr = array();
while($rowSites = $resultSites->fetch_assoc()){
	array_push($systemsArr,$rowSites);
}

function limit_words($string, $word_limit)
{
	$words = explode(" ",$string);
	if(count($words) > $word_limit){
		return implode(" ",array_splice($words,0,$word_limit))."...(more)";
	}else{
		return implode(" ",array_splice($words,0,$word_limit));
	}
}

?>
<!DOCTYPE html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<?php require_once($js_include);?>

<script type="text/javascript">
$(document).ready(function() {
/////////////////////////////////////////////////////////////////////////////////////	

	$( "#dbContent" ).tabs({ 
	});
	
	<?php
	if(strtolower($_SESSION['prefs']['pref_show_help']) != 'y' or $_SESSION['mobile_device'] == true){
	?>
	$('#tiplayer').hide()
	<?php
		}
	?>


});
</script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
  	google.load("visualization", "1", {packages:["corechart"]});
  	//google.setOnLoadCallback(drawChart);
	<?php 
		$x = 1;
		foreach($systemsArr as $key=>$system){
			$system_unique_id = $system['unique_id'];
			$sql="SELECT
			(SELECT SUM(downtime) FROM systems_reports WHERE system_unique_id = '$system_unique_id') AS tdt,
			(SELECT IF(FLOOR(DATEDIFF(DATE(NOW()),(SELECT DATE(MAX(`date`)) FROM systems_reports WHERE system_unique_id = '$system_unique_id')) / 365.25) = 0, 1,FLOOR(DATEDIFF(DATE(NOW()),(SELECT DATE(MAX(`date`)) FROM systems_reports WHERE system_unique_id = '$system_unique_id')) / 365.25))) AS yrs,
			(SELECT SUM(downtime) FROM systems_reports WHERE DATE(`date`) BETWEEN CURDATE() - INTERVAL 12 MONTH AND SYSDATE() AND system_unique_id = '$system_unique_id') AS dt12,
			(SELECT SUM(downtime) FROM systems_reports WHERE DATE(`date`) BETWEEN CURDATE() - INTERVAL 6 MONTH AND SYSDATE() AND system_unique_id = '$system_unique_id') AS dt6,
			(SELECT SUM(downtime) FROM systems_reports WHERE DATE(`date`) BETWEEN CURDATE() - INTERVAL 3 MONTH AND SYSDATE() AND system_unique_id = '$system_unique_id') AS dt3,
			(SELECT SUM(downtime) FROM systems_reports WHERE DATE(`date`) BETWEEN CURDATE() - INTERVAL 1 MONTH AND SYSDATE() AND system_unique_id = '$system_unique_id') AS dt1;";
			if(!$resultData = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
				$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);	
			}
			$rowData = $resultData->fetch_assoc();
			echo "function chart".$x."() {
				var data = google.visualization.arrayToDataTable([
				['Months', 'Index'],";
//				if(strtolower($system['new_customer']) == "y"){
//					echo "['Lifetime', "; echo round((((intval($system['contract_hours'])*intval($rowData['yrs']))-$rowData['tdt']) / (intval($system['contract_hours'])*intval($rowData['yrs'])))*100,2); echo "],";
//				}
				echo "['12',     "; echo round(((intval($system['contract_hours']) - $rowData['dt12'])/intval($system['contract_hours']))*100,2); echo "],
				['6',      "; echo round((((intval($system['contract_hours'])/2) - $rowData['dt6'])/(intval($system['contract_hours'])/2))*100,2); echo "],
				['3',      "; echo round((((intval($system['contract_hours'])/4) - $rowData['dt3'])/(intval($system['contract_hours'])/4))*100,2); echo "],
				['1',      "; echo round((((intval($system['contract_hours'])/12) - $rowData['dt1'])/(intval($system['contract_hours'])/12))*100,2); echo "]
				]);

				var options = {
					vAxis: {title: \"Months\"},
					hAxis: {title: \"% of Uptime\",
					viewWindow: {max: 100}},
					legend: {position: 'none'},
					width: 385
				};
				
				var chart = new google.visualization.BarChart(document.getElementById('chart_div".$x."'));
				chart.draw(data, options);
			}";
			$x = $x + 1;
		}
		
		$y = 1;
		echo "function drawVisualization() {\n";
		while($y < $x)
		{
			echo "chart".$y."();\n";
			$y = $y + 1;
		}
		echo "}\n";
	?>
  	google.setOnLoadCallback(drawVisualization);
</script>
</head>
<body>
<?php require_once($header_include); ?>
<div id="OIReportContentLeft">
	<div id="advert">
		<?php if($_SESSION['mobile_device'] != true){?>
		<img src="<?php echo $settings->customer_left_pic; ?>">
		<?php } ?>
	</div>
</div>
<div id="OIReportContentIndex"> 
	<div id="custdb">
		<div id="dbHeader"><span style="font-size:1.2em; color:#f79447;">Customer Dashboard</span></div>
        	<div id="dbContent">
				<ul>
					<?php 
					$x = 1;
					foreach($systemsArr as $key=>$system){ ?>
						<li id="li-tab-<?php echo $x; ?>"><a href="#tab-<?php echo $x; ?>"><?php echo "System ".$system['system_id']; ?></a></li>
						
					<?php 
						$x += 1;
					} ?>
				</ul>
			
            	<?php 
					$x = 1;
					foreach($systemsArr as $key=>$system){
						$system_unique_id = $system['unique_id'];
						if(strtolower($system['credit_hold'])=="y"){array_push($credit_hold,$system['system_id']);}
						$sql = "SELECT * FROM systems_reports WHERE system_unique_id='$system_unique_id' AND property = 'C' AND deleted = 'n' ORDER BY `date` DESC LIMIT 5;";
						if(!$resultReports = $mysqli->query($sql)){
							$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
							$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
							$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
						}
						$sql = "SELECT COUNT(*) AS `counts` FROM systems_files WHERE unique_id = '$system_unique_id'";
						if(!$resultFiles = $mysqli->query($sql)){
							$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
							$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
							$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
						}
						$count = 0;
						$count = $resultFiles->fetch_object()->counts;					
						$sql="SELECT 
						(SELECT YEAR(CURDATE())) AS cur_year,
						(select e.`type` from systems_base AS sb LEFT JOIN systems_types AS e ON sb.system_type = e.id WHERE unique_id = '$system_unique_id') AS equip,
						
						(SELECT COUNT(*) FROM systems_reports WHERE system_unique_id='$system_unique_id' AND YEAR(DATE(`date`)) = YEAR(CURDATE())) AS t_rpt_yr_1,
						(SELECT COUNT(*) FROM systems_reports WHERE system_unique_id='$system_unique_id' AND YEAR(DATE(`date`)) = (YEAR(CURDATE())-1)) AS t_rpt_yr_2,
						(SELECT COUNT(*) FROM systems_reports WHERE system_unique_id='$system_unique_id' AND YEAR(DATE(`date`)) = (YEAR(CURDATE())-2)) AS t_rpt_yr_3,
						
						(SELECT COUNT(*) FROM systems_requests WHERE system_unique_id='$system_unique_id' AND deleted = 'n' AND YEAR(DATE(request_date)) = YEAR(CURDATE())) AS t_rq_yr_1,
						(SELECT COUNT(*) FROM systems_requests WHERE system_unique_id='$system_unique_id' AND deleted = 'n' AND YEAR(DATE(request_date)) = (YEAR(CURDATE())-1)) AS t_rq_yr_2,
						(SELECT COUNT(*) FROM systems_requests WHERE system_unique_id='$system_unique_id' AND deleted = 'n' AND YEAR(DATE(request_date)) = (YEAR(CURDATE())-2)) AS t_rq_yr_3,
						
						(select count(*) FROM systems_reports WHERE year(curdate()) = YEAR(DATE(`date`)) AND invoice_required = 'Y' AND valuation_do_not_invoice = 'N' AND system_unique_id = '$system_unique_id') AS service_invoiced_yr_1,
						(select count(*) FROM systems_reports WHERE (year(curdate())-1) = YEAR(DATE(`date`)) AND invoice_required = 'Y' AND valuation_do_not_invoice = 'N' AND system_unique_id = '$system_unique_id') AS service_invoiced_yr_2,
						(select count(*) FROM systems_reports WHERE (year(curdate())-2) = YEAR(DATE(`date`)) AND invoice_required = 'Y' AND valuation_do_not_invoice = 'N' AND system_unique_id = '$system_unique_id') AS service_invoiced_yr_3,
						
						(select count(*) FROM systems_parts  WHERE year(curdate()) = YEAR(DATE(`date`)) AND system_unique_id = '$system_unique_id') AS total_parts_yr_1,
						(select count(*) FROM systems_parts  WHERE (year(curdate())-1) = YEAR(DATE(`date`)) AND system_unique_id = '$system_unique_id') AS total_parts_yr_2,
						(select count(*) FROM systems_parts  WHERE (year(curdate())-2) = YEAR(DATE(`date`)) AND system_unique_id = '$system_unique_id') AS total_parts_yr_3,
						
						(select COALESCE(sum(reg_labor),0) FROM systems_hours WHERE year(curdate()) = YEAR(DATE(`date`)) AND system_unique_id = '$system_unique_id') AS labor_reg_yr_1,
						(select COALESCE(sum(ot_labor),0) FROM systems_hours WHERE year(curdate()) = YEAR(DATE(`date`)) AND system_unique_id = '$system_unique_id') AS labor_ot_yr_1,
						(select COALESCE(sum(reg_travel),0) FROM systems_hours WHERE year(curdate()) = YEAR(DATE(`date`)) AND system_unique_id = '$system_unique_id') AS travel_reg_yr_1,
						(select COALESCE(sum(ot_travel),0) FROM systems_hours WHERE year(curdate()) = YEAR(DATE(`date`)) AND system_unique_id = '$system_unique_id') AS travel_ot_yr_1,
						
						(select COALESCE(sum(reg_labor),0) FROM systems_hours WHERE (year(curdate())-1) = YEAR(DATE(`date`)) AND system_unique_id = '$system_unique_id') AS labor_reg_yr_2,
						(select COALESCE(sum(ot_labor),0) FROM systems_hours WHERE (year(curdate())-1) = YEAR(DATE(`date`)) AND system_unique_id = '$system_unique_id') AS labor_ot_yr_2,
						(select COALESCE(sum(reg_travel),0) FROM systems_hours WHERE (year(curdate())-1) = YEAR(DATE(`date`)) AND system_unique_id = '$system_unique_id') AS travel_reg_yr_2,
						(select COALESCE(sum(ot_travel),0) FROM systems_hours WHERE (year(curdate())-1) = YEAR(DATE(`date`)) AND system_unique_id = '$system_unique_id') AS travel_ot_yr_2,
						
						(select COALESCE(sum(reg_labor),0) FROM systems_hours WHERE (year(curdate())-2) = YEAR(DATE(`date`)) AND system_unique_id = '$system_unique_id') AS labor_reg_yr_3,
						(select COALESCE(sum(ot_labor),0) FROM systems_hours WHERE (year(curdate())-2) = YEAR(DATE(`date`)) AND system_unique_id = '$system_unique_id') AS labor_ot_yr_3,
						(select COALESCE(sum(reg_travel),0) FROM systems_hours WHERE (year(curdate())-2) = YEAR(DATE(`date`)) AND system_unique_id = '$system_unique_id') AS travel_reg_yr_3,
						(select COALESCE(sum(ot_travel),0) FROM systems_hours WHERE (year(curdate())-2) = YEAR(DATE(`date`)) AND system_unique_id = '$system_unique_id') AS travel_ot_yr_3,
						
						(SELECT `date` FROM systems_mri_readings WHERE system_unique_id = '$system_unique_id' ORDER BY id DESC LIMIT 1) AS reading_date,
						(SELECT he FROM systems_mri_readings WHERE system_unique_id = '$system_unique_id' ORDER BY id DESC LIMIT 1) AS he,
						(SELECT vp FROM systems_mri_readings WHERE system_unique_id = '$system_unique_id' ORDER BY id DESC LIMIT 1) AS vp;";
						
						if(!$resultStats = $mysqli->query($sql)){
							$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
							$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
							$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);	
						}
						$rowStats = $resultStats->fetch_assoc();
					?>
					<div id="tab-<?php echo $x; ?>" class="tab">
						<div class='site'>
							<div class='sys'>
								<div class='sysContent'>
									<div class='sysItem'>
										<!--<div class='sysItemId'><span class="blue_txt">System ID:</span> <?php echo $system['system_id']; ?></div>-->
										<div class='sysItemName'><span class="blue_txt">System Nickname: <?php echo $system['nickname']; ?></span></div>
										<div class='sysItemName'><span class="blue_txt">Facility: <?php echo $system['facility_name']; ?></span></div>
										<div class='sysLeft'>
											<div class='sysItemHealth'> <span class="blue_txt"><b>System Health</b></span>
												<div class='sysItemHealthFlag'><a <?php if($system['status'] != 1){ echo "href=\"javascript:alert('Not Implemented');\"";}?> ><img src="/resources/images/<?php echo "flag".$system['flag']; ?>.png" width="64" height="64"></a></div>
											</div>
											
											<div class="sysItemEng"<?php if($system['engineer']==""){echo " style=\"display:none\"";} ?>> <span class="blue_txt">Primary Engineer</span>
												<div class='sysItemEngName' ><?php echo $system['engineer']; ?></div>
											</div>
											<div class="sysItemEng"<?php if($system['system_type_name']==""){echo " style=\"display:none\"";} ?>> <span class="blue_txt">System Type</span>
												<div class='sysItemEngName' ><?php echo $system['system_type_name']; ?></div>
											</div>
											<div class="sysItemWarr"<?php if($system['warranty_end_date']=="" or strtolower($_SESSION['perms']['perm_facility_manager']) != "y"){echo " style=\"display:none\"";} ?>> <span class="blue_txt">Warranty Expire</span>
												<div class='sysItemWarrDate' <?php $date = strtotime($system['warranty_end_date']); if ($date <= strtotime('+30 day',strtotime('+0 days'))){echo " style=\"color:#F00 !important;\"";}?> ><?php echo date(phpdispfd, strtotime($system['warranty_end_date'])); ?></div>
											</div>
											<div class="sysItemCon"<?php if($system['contract_end_date']=="" or strtolower($_SESSION['perms']['perm_facility_manager']) != "y"){echo " style=\"display:none\"";} ?>> <span class="blue_txt">Contract Expire</span>
												<div class='sysItemConDate ' <?php $date = strtotime($system['contract_end_date']); if ($date <= strtotime('+30 day',strtotime('+0 days'))){echo " style=\"color:#F00 !important;\"";}?> ><?php echo date(phpdispfd, strtotime($system['contract_end_date'])); ?></div>
											</div>
											<div class="sysItemDocs"<?php if(strtolower($_SESSION['perms']['perm_facility_manager']) != "y" or intval($count) == 0){echo " style=\"display:none\"";} ?> > <span class="blue_txt">Documents</span>
												<div class='sysItemDocument'><a class="iframeFiles" href="/customer/customer_files.php?unique_id=<?php echo $system['unique_id'];?>">View <?php echo $count; ?> Documents</a></div>
											</div>
											<div class="sysItemCryo"<?php if(strtolower($rowStats['equip']) != "mr"){echo " style=\"display:none\"";} ?>><span class="blue_txt">Last Cryo Reading</span>
													<table cellpadding="2" cellspacing="2" border="0" width="100%">
														<tr>
															<td><?php echo strstr(date(phpdispfd, strtotime($rowStats['reading_date'])),' ',true); ?></td>
														</tr>
													</table>
													<table cellpadding="2" cellspacing="2" border="0" width="100%">
														<tr>
															<td>Helium</td><td><?php echo $rowStats['he']; ?></td>
														</tr>
														<tr>
															<td>Pressure</td><td><?php echo $rowStats['vp']; ?></td>
														</tr>
													</table>
													</div>
										</div>
										<div class='sysRight'>
											<div class='sysItemIndex'<?php if(strtolower($_SESSION['perms']['perm_facility_manager']) == "n"){echo " style=\"display:none\"";} ?>>
												<div class='sysItemIndexTitle'><span class="blue_txt"><b>Service Index</b></span></div>
												<div class='sysItemIndexChart'>
													<div id='chart_div<?php echo $x; ?>' ></div>
												</div>
											</div>
											<br>
											<div class="sysItemStat"<?php if(strtolower($_SESSION['perms']['perm_facility_manager']) == "n"){echo " style=\"display:none\"";} ?>>
												<div class="sysItemStatTitle"><span class="blue_txt"><b>Service Stats</b></span></div>
												<div class="sysItemStatContent">
												<?php $cur_year =  intval($rowStats['cur_year']); ?>
													<table cellpadding="2" cellspacing="2" border="0" width="100%">
														<tr>
															<td width="150px">&nbsp;</td><td class="blue_txt"><?php echo $cur_year; ?></td><td class="blue_txt"><?php echo $cur_year - 1; ?></td><td class="blue_txt"><?php echo $cur_year - 2; ?></td><td class="blue_txt">Total</td>
														</tr>
														<tr>
															<td class="blue_txt">Service Requests</td><td><?php echo $rowStats['t_rq_yr_1'];?></td><td><?php echo $rowStats['t_rq_yr_2'];?></td><td><?php echo $rowStats['t_rq_yr_3'];?></td><td><?php echo $rowStats['t_rq_yr_1'] + $rowStats['t_rq_yr_2'] + $rowStats['t_rq_yr_3'];?></td>
														</tr>
														<tr>
															<td class="blue_txt">Service Reports</td><td><?php echo $rowStats['t_rpt_yr_1'];?></td><td><?php echo $rowStats['t_rpt_yr_2'];?></td><td><?php echo $rowStats['t_rpt_yr_3'];?></td><td><?php echo $rowStats['t_rpt_yr_1'] + $rowStats['t_rpt_yr_2'] + $rowStats['t_rpt_yr_3'];?></td>
														</tr>
													</table>
													<br>
													<table cellpadding="2" cellspacing="2" border="0" width="100%">
														<tr> 
															<td width="150px">&nbsp;</td><td class="blue_txt"><?php echo $cur_year; ?></td><td class="blue_txt"><?php echo $cur_year - 1; ?></td><td class="blue_txt"><?php echo $cur_year - 2; ?></td><td class="blue_txt">Total</td>
														</tr>
														<tr>
															<td class="blue_txt">Service Invoiced</td><td><?php echo $rowStats['service_invoiced_yr_1'];?></td><td><?php echo $rowStats['service_invoiced_yr_2'];?></td><td><?php echo $rowStats['service_invoiced_yr_3'];?></td><td><?php echo $rowStats['service_invoiced_yr_1'] + $rowStats['service_invoiced_yr_2'] + $rowStats['service_invoiced_yr_3'];?></td>
														</tr>
													</table>
													<br>
													<table cellpadding="2" cellspacing="2" border="0" width="100%">
														<tr> 
															<td width="150px">&nbsp;</td><td class="blue_txt"><?php echo $cur_year; ?></td><td class="blue_txt"><?php echo $cur_year - 1; ?></td><td class="blue_txt"><?php echo $cur_year - 2; ?></td><td class="blue_txt">Total</td>
														</tr>
														<tr>
															<td class="blue_txt">Parts Used</td><td><?php echo $rowStats['total_parts_yr_1'];?></td><td><?php echo $rowStats['total_parts_yr_2'];?></td><td><?php echo $rowStats['total_parts_yr_3'];?></td><td><?php echo $rowStats['total_parts_yr_1'] + $rowStats['total_parts_yr_2'] + $rowStats['total_parts_yr_3'];?></td>
														</tr>
													</table>
													<br>
													<table cellpadding="2" cellspacing="2" border="0" width="100%">
														<tr> 
															<td width="150px">&nbsp;</td><td class="blue_txt"><?php echo $cur_year; ?></td><td class="blue_txt"><?php echo $cur_year - 1; ?></td><td class="blue_txt"><?php echo $cur_year - 2; ?></td><td class="blue_txt">Total</td>
														</tr>
														<tr>
															<td class="blue_txt">Onsite Reg</td><td><?php echo $rowStats['labor_reg_yr_1'];?></td><td><?php echo $rowStats['labor_reg_yr_2'];?></td><td><?php echo $rowStats['labor_reg_yr_3'];?></td><td><?php echo $rowStats['labor_reg_yr_1'] + $rowStats['labor_reg_yr_2'] + $rowStats['labor_reg_yr_3'];?></td>
														</tr>
														<tr>
															<td class="blue_txt">Onsite OT</td><td><?php echo $rowStats['labor_ot_yr_1'];?></td><td><?php echo $rowStats['labor_ot_yr_2'];?></td><td><?php echo $rowStats['labor_ot_yr_3'];?></td><td><?php echo $rowStats['labor_ot_yr_1'] + $rowStats['labor_ot_yr_2'] + $rowStats['labor_ot_yr_3'];?></td>
														</tr>
														<tr>
															<td class="blue_txt">Travel Reg</td><td><?php echo $rowStats['travel_reg_yr_1'];?></td><td><?php echo $rowStats['travel_reg_yr_2'];?></td><td><?php echo $rowStats['travel_reg_yr_3'];?></td><td><?php echo $rowStats['travel_reg_yr_1'] + $rowStats['travel_reg_yr_2'] + $rowStats['travel_reg_yr_3'];?></td>
														</tr>
														<tr>
															<td class="blue_txt">Travel OT</td><td><?php echo $rowStats['travel_ot_yr_1'];?></td><td><?php echo $rowStats['travel_ot_yr_2'];?></td><td><?php echo $rowStats['travel_ot_yr_3'];?></td><td><?php echo $rowStats['travel_ot_yr_1'] + $rowStats['travel_ot_yr_2'] + $rowStats['travel_ot_yr_3'];?></td>
														</tr>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class='ag'>
								<div class='agTitle'><span class="blue_txt">Service Reports</span></div>
								<div class='agContent'>
								<?php if($resultReports->num_rows == 0){ ?>								
									<div class='agItem' id="noHistory">
										<div class='agItemDate'>&nbsp;</div>
										<div class='agItemId'>&nbsp;</div>
										<div class='agItemSys'>No Recent Report History</div>
										<div class='agItemData'>&nbsp;</div>
									</div>
								  <?php 
								}else{
									while($rowReports = $resultReports->fetch_assoc())
									{?>
									<div class='agItem' onclick="location.href='/report/common/report_view.php?uid=<?php echo $_SESSION['login']."&id=".$rowReports['unique_id']?>';" style="cursor:pointer;" >
											<div class='agItemDate'><?php echo date(phpdispfd, strtotime($rowReports['date'])); ?></div>
											<div class='agItemId'><?php echo $system['system_id'] ." - ".$system['nickname'];?></div>
											<div class='agItemSys'><?php echo limit_words($rowReports['service'],$custrpt_word_limt);?></div>
											<div class='agItemData'>Status: <?php echo $rowReports['status'];?></div>
										</div>
										<div class='line'></div>
							  <?php }
								}
								?>
								</div>
								<div class='agFooter'><a class='iframeAllReports' href="/report/common/all_reports.php?unique_id=<?php echo $system['unique_id'];?>" >View All Service Reports</a></div>
								<!-- agcontent --> 
							</div><!-- ag -->
							<div class='sitefooter'></div>
						</div><!--site--> 
					</div><!-- tab -->
					<?php 
					$x = $x + 1;
				}
					?>
                    
                    
                    
    	</div><!-- dbcontent -->
	</div> <!-- custdb -->
</div><!-- OIRemote Content -->

<script type="text/javascript">
$(document).ready(function() {
/////////////////////////////////////////////////////////////////////////////////////		
		<?php
		if(count($credit_hold)>0){
			$credit_hold_alert = "Please contact service!\\nError Code: C H \\nSites effected:\\n";
			foreach ($credit_hold as &$credit_hold_val){
				$credit_hold_alert .= "System ID: ".$credit_hold_val."\\n";
			}
			echo "alert('".$credit_hold_alert."');";
		}
		?>
});
</script>

<?php require_once($footer_include); ?>