<?php
//
// Error reporting
//
//error_reporting(E_ALL);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
ini_set('display_errors', 'On');

$debug = false;
if(isset($_GET['debug'])){
	$debug = true;
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/php_utils.php');
$php_utils =  new phpUtils();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/moment_php/Moment.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/resources/moment_php/MomentException.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/resources/moment_php/MomentHelper.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/resources/moment_php/MomentLocale.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/resources/moment_php/MomentFromVo.php');

$now = new \Moment\Moment();
$now->setImmutableMode(true);

$comp_word_limt 	 = intval($settings->complaint_word_limit);
$serv_word_limt		 = intval($settings->service_word_limit);
$custact_word_limt	 = intval($settings->custact_word_limit);
$probrpt_word_limt	 = intval($settings->probrpt_word_limit);

$myusername = $_SESSION["login"];

$emailReqStarted = false;
$emailReqNum = false;

if(isset($_SESSION['email_request'])){
	$emailReqNum = $_SESSION['email_request'];
	unset($_SESSION['email_request']);
	$sql = "SELECT COUNT(srq.id) AS `count`, srq.unique_id, srq.system_nickname, srq.system_id
	FROM systems_requests AS srq
	LEFT JOIN systems_base_cont AS sbc ON sbc.ver_unique_id = srq.system_ver_unique_id
	WHERE srq.request_num = '".$emailReqNum."' AND srq.`status` = 'open' AND sbc.property = 'C';";
	d($sql);
	if(!$resultEmailReq = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$rowEmailReq = $resultEmailReq->fetch_assoc();
	d($rowEmailReq);
	if($rowEmailReq['count'] == '1'){
		header("location:/report/common/report_new_edit.php?id=".$rowEmailReq['unique_id']."&uid=".$myusername);	
	}else{
		$sql = "SELECT unique_id,system_nickname,system_id FROM systems_requests WHERE request_num = '".$emailReqNum."';";
		d($sql);
		if(!$resultEmailReq = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
		$rowEmailReq = $resultEmailReq->fetch_assoc();
		d($rowEmailReq);
		$emailReqStarted = true;
	}
}
$sql="SELECT * FROM `systems_status`";
d($sql);
if(!$resultStatus = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$arr_status = array();
while($rowStatus = $resultStatus->fetch_assoc())
{
	array_push_assoc($arr_status, $rowStatus['id'], $rowStatus['status']);
}
d($arr_status);

$sql="SELECT u.uid, u.name
FROM users AS u
LEFT JOIN users_roles AS ur ON ur.uid = u.uid
LEFT JOIN users_role_id AS rid ON rid.id = ur.rid
WHERE rid.role = 'role_engineer' OR rid.role = 'role_contractor';";
d($sql);
if(!$resultEngineers = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$arr_engineers = array();
while($rowEngineers = $resultEngineers->fetch_assoc())
{
	array_push_assoc($arr_engineers, $rowEngineers['uid'], $rowEngineers['name']);
}
d($arr_engineers);

function array_push_assoc(&$array, $key, $value){
	$array[$key] = $value;
	return $array;
}
function limit_words($string, $word_limit)
{
	$words = explode(" ",$string);
	if(count($words) > $word_limit){
		return implode(" ",array_splice($words,0,$word_limit))."...(more)";
	}else{
		return implode(" ",array_splice($words,0,$word_limit));
	}
}

?>
<!DOCTYPE html>
<html>
<head>
<?php require_once($head_include);?>

<?php require_once($css_include);?>
<link rel="stylesheet" type="text/css" href="/resources/js/iButton/css/jquery.ibutton.min.css">
<link rel="stylesheet" type="text/css" href="/resources/css/portal.css">
<?php require_once($js_include);?>
<script src="/resources/js/iButton/lib/jquery.ibutton.min.js"></script>
<script src="/resources/js/iButton/lib/jquery.easing.1.3.js"></script>

<script type="text/javascript">
/////////////////////////////////////////////////////////////////////////////////////	
	function showall(){
		var c_name = "showall"; //cookie name
		var value = "true"; //cookie value
		var exdays = 365; //cookie expire in days	
		if(document.getElementById("showallcb").checked == false){
			var exdate=new Date();
			exdate.setDate(exdate.getDate() + exdays);
			var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
			document.cookie=c_name + "=" + c_value;	
			location.reload(); 
		}else{
			document.cookie = c_name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';	
			location.reload(); 
		}
	}
/////////////////////////////////////////////////////////////////////////////////////	
	function getCookie(c_name)
	{
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++)
		{
			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
			x=x.replace(/^\s+|\s+$/g,"");
			if (x==c_name)
			{
			return unescape(y);
			}
		}
	}

/////////////////////////////////////////////////////////////////////////////////////
	$(document).ready(function() {
		var showall=getCookie("showall");
		if (showall!=null && showall!=""){
			$("#showallcb").attr('checked',false);
		}else{
			$("#showallcb").attr('checked',true);
		}
				
		openTable = $('#openReportsTable').dataTable({
			"bJQueryUI": true,
			//"bStateSave": true,
			"iCookieDuration": 60*60*24*365, // 1 year
			"sPaginationType": "full_numbers",
			"aaSorting": [],
			 "fnInitComplete": function () {
           		 this.$('tr').click( function () {
               		 var href = $(this).find("a").attr("href");
					 if(href) {
						window.location = href;
					 }
           		 });
       		 }
		});
/////////////////////////////////////////////////////////////////////////////////////	
		pastTable = $('#pastReportsTable').dataTable({
			"bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "../common/connectors/completed_reports_connector.php",
			"bJQueryUI": true,
			"aLengthMenu": [[5, 10, 25, 50], [5, 10, 25, 50]],
			"iCookieDuration": 60*60*24*365, // 1 year
			"iDisplayLength": 5,
			"sPaginationType": "full_numbers",
			"aaSorting": [ [2,'desc'] ], 			
			"aoColumnDefs": [
                        { "bSearchable": false, "bVisible": false, "aTargets": [ 0 ] }
            ],
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
					//$('tr:eq(0)', nRow).addClass("pastReportsHelp");
					$('td:eq(0)', nRow).html( function() {	
						var moment_dt = moment($(this).text()).tz(aData[9]);
						return "<a href='/report/common/report_view.php?id=" + aData[8] + "&uid=<?php echo $myusername ?>'>" + moment_dt.format('<?php echo jsdispfd; ?>') + "</a>";
					});
					this.$('tr').click( function () {
               		 var href = $(this).find("a").attr("href");
					 if(href) {
						window.location = href;
					 }
           		 });
			},
			"fnInitComplete": function () {
           		 this.$('tr').addClass("pastReportsHelp");
       		},
			"fnServerParams": function ( aoData ) {
			  aoData.push( { "name": "oiuid", "value": "<?php echo $myusername ?>" },
			  			   { "name": "oiall", "value": "<?php if(isset($_COOKIE["showall"])){echo "1";}else{echo "0";}?>"} ); //change to 0 for just engineer
			}
		});
/////////////////////////////////////////////////////////////////////////////////////		
		reqestTable = $('#requestsTable').dataTable({
			"bJQueryUI": true,
			//"bStateSave": true,
			"iCookieDuration": 60*60*24*365, // 1 year
			"sPaginationType": "full_numbers",
			"aaSorting": [],
			"fnInitComplete": function () {
           		 this.$('tr').click( function () {
               		 var href = $(this).find("a").attr("href");
					 if(href) {
						window.location = href;
					 }
           		 });
       		 }
		});
/////////////////////////////////////////////////////////////////////////////////////		
		pmTable = $('#pmTable').dataTable({
			"bJQueryUI": true,
			//"bStateSave": true,
			"iCookieDuration": 60*60*24*365, // 1 year
			"sPaginationType": "full_numbers",
			"aaSorting": [],
			"aoColumnDefs": [
			  {"aTargets": [ 0 ], "bSortable": true, "sType": "date"}
			],
			 "fnInitComplete": function () {
           		 this.$('tr').click( function () {
               		 var href = $(this).find("a").attr("href");
					 if(href) {
						window.location = href;
					 }
           		 });
       		 }
		});
/////////////////////////////////////////////////////////////////////////////////////		
		
		
		jQuery.fn.dataTableExt.oSort['us_date-asc']  = function(a,b) {
			var x = new Date(a),
			y = new Date(b);
			return ((x < y) ? -1 : ((x > y) ?  1 : 0));
		};
		
		jQuery.fn.dataTableExt.oSort['us_date-desc'] = function(a,b) {
			var x = new Date(a),
			y = new Date(b);
			return ((x < y) ? 1 : ((x > y) ?  -1 : 0));
		};

		<?php
		if($emailReqStarted and !$emailReqNum){
		?>
		alert("The service report for <?php echo $rowEmailReq['system_nickname']." (".$rowEmailReq['system_id'].")"; ?> has already been finalized.");
		<?php
			unset($_SESSION['urlGET']);
			unset($_SESSION['email_request']);
		}
		?>
/////////////////////////////////////////////////////////////////////////////////////
		$(document).keyup(function(e) {
			$("#requestsTable").each(function(index, element) {
				if($(this).children("tbody").children("tr").length == 1 && e.which == 13){
					$(this).find("td > a").click();
				}
			});
			
			$("#openReportsTable").each(function(index, element) {
				if($(this).children("tbody").children("tr").length == 1 && e.which == 13){
					$(this).find("td > a").click();
				}
			});
			
			$("#pastReportsTable").each(function(index, element) {
				if($(this).children("tbody").children("tr").length == 1 && e.which == 13){
					$(this).find("td > a").click();
				}
			});
			
		});
		
/////////////////////////////////////////////////////////////////////////////////////	
		
		 $("#showallcb").iButton({
			labelOn: "Show My Data&emsp;&emsp;",
		 	labelOff: "Show All Data",
			resizeHandle: true,
			resizeContainer: true,
			duration: 250,
		 	easing: "swing"   
		 });
		
		
/////////////////////////////////////////////////////////////////////////////////////
});
/////////////////////////////////////////////////////////////////////////////////////
</script>
</head>
<body>
<?php require_once($header_include); ?>
    
<div id="OIReportContent"> 
	<div id="showalldiv"><input name="showallcb" data-role="flipswitch" type="checkbox" id="showallcb" onChange="showall()" value="true" ></div>
        <div id="ReportOuterFrame">
		
	        <?php  

				if(isset($_COOKIE["showall"])){
					$sql = "SELECT r.*, sbc.credit_hold, sbc.pre_paid, sbc.property, sb.status AS cur_system_status
							FROM systems_requests AS r
							LEFT JOIN systems_base_cont AS sbc ON sbc.ver_unique_id = r.system_ver_unique_id
							LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
							WHERE r.`status`='open'  AND r.`deleted` = 'n'
							ORDER BY r.id ASC;";
				}else{
					$sql = "SELECT r.*, sbc.credit_hold, sbc.pre_paid, sbc.property, sb.status AS cur_system_status
							FROM systems_requests AS r
							LEFT JOIN systems_base_cont AS sbc ON sbc.ver_unique_id = r.system_ver_unique_id
							LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
							WHERE r.`status`='open' AND `engineer`='$myusername' AND r.`deleted` = 'n'
							ORDER BY r.id ASC;";
				}
				d($sql);
				if(!$resultRequests = $mysqli->query($sql)){
					$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
					$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
					$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
				}
			?>  
            <div id="ReportFrame">
            <div id="ReportFrameTitle"> Open Service Requests </div>
            <div id="requestsDiv" <?php if($resultRequests->num_rows == 0){echo "style=\"display:none\"";} ?>>
	            <div class="legend">Color Legend:&nbsp;<span class="credit_hold">Credit Hold</span>&emsp;<span class="pre_paid">Pre-Paid</span>&emsp;<span class="future">Future</span></div>
                <table width="100%" id="requestsTable" >
                    <thead>
                        <tr>
                            <th width="75px">Date</th>
                            <th width="50px">Request Number</th>
                            <th width="50px">System ID</th>
                            <th>System Nickname</th>
                            <th>Problem</th>
                            <?php if(isset($_COOKIE["showall"]) or isset($_SESSION['adm']) or isset($_SESSION['mgt'])){echo "<th>Engineer</th>";}else{echo "<th>Cust Act</th>";} ?>
                            <th>System Status</th>
                            <th>Report Due Days</th>
	                        <th>PM</th>
	                        <th>PM Sched</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
						if($resultRequests->num_rows != 0){
							$new_now = $now->cloning();
							$new_now->setImmutableMode(false);
							$new_now->subtractDays(5);
							$new_now->subtractHours(12);
							$new_now->setImmutableMode(true);
							while($rowRequest = $resultRequests->fetch_assoc())
							{
								$onsite_date = new Moment\Moment($rowRequest['onsite_date']);
								$days_left = round($new_now->from($onsite_date)->getDays(), 1);

								if(strtolower($rowRequest['property']) == 'f'){
									echo "<tr class=\"future\">\n";
								}elseif(strtolower($rowRequest['credit_hold']) == "y"){
									echo "<tr class=\"credit_hold\">\n";
								}elseif(strtolower($rowRequest['pre_paid']) == "y"){
									echo "<tr class=\"pre_paid\">\n";
								}else{
									echo "<tr>\n";
								}
								if(strtolower($rowRequest['property']) == 'f'){
									//echo "<td><span onClick=\"alert('Cannot create report on future system.');\">". date(phpdispfd,strtotime($rowRequest['request_date'])) ."</span></td>\n";
									echo "<td><a style=\"color:#960\" href='/report/common/request_new_edit.php?j&ver_unique_id=".$rowRequest['system_ver_unique_id']."&version=".$rowRequest['system_ver']."&unique_id=".$rowRequest['system_unique_id']."&user_id=$myusername&req_id=".$rowRequest['unique_id']."'>". date(phpdispfd,strtotime($rowRequest['request_date']))."</a></td>\n";
								}else{
									echo "<td><a href=\"/report/common/report_new_edit.php?id=".$rowRequest['unique_id'].'&uid='.$myusername."\">". date(phpdispfd,strtotime($rowRequest['request_date']))."</a></td>\n";
								}
								echo "<td>". $rowRequest['request_num']."</td>\n";
								echo "<td>". $rowRequest['system_id']."</td>\n";
								echo "<td>". $rowRequest['system_nickname']."</td>\n";
								echo "<td>". limit_words($rowRequest['problem_reported'],$probrpt_word_limt)."</td>\n";
								if(isset($_COOKIE["showall"]) or isset($_SESSION['adm']) or isset($_SESSION['mgt'])){
									echo "<td>". $arr_engineers[$rowRequest['engineer']]."</td>\n";
								}else{
									echo "<td>". limit_words($rowRequest['customer_actions'],$custact_word_limt)."</td>\n";
								}
								echo "<td>". $arr_status[$rowRequest['cur_system_status']]."</td>\n";
								echo "<td"; if($days_left < 0 ){echo " class='credit_hold'";}  echo ">". $days_left . "</td>\n";
								echo "<td>"; if(strtolower($rowRequest['pm'])=="y"){echo "Yes";} echo "</td>\n";
								if(strtolower($rowRequest['pm'])=="y"){
									if($rowRequest['pm_date'] == ""){
										echo "<td>Contact Dispatch</td>\n";
									}else{
										echo "<td>" . implode('<br>', explode('T', $rowRequest['pm_date'])) . "</td>\n";
									}
								}else{
									echo "<td>&nbsp;</td>\n";
								}
								echo "</tr>\n";
								unset($onsite_date);
								unset($days_left);
							}
						}
						?>
                    </tbody>
                </table>
            </div>
            <div id="requestDiv" <?php if($resultRequests->num_rows != 0){echo "style=\"display:none\"";}else{echo "style=\"text-align:center\"";} ?>>
                <br />
                <h2>No Service Requests for <?php echo $$_SESSION['userdata']['name'] ?></h2>
            </div>
          </div>
        </div>
        <div class="line"></div>
        <div id="ReportOuterFrame">
	        <?php  
				if(isset($_COOKIE["showall"])){
					$sql = "SELECT r.*, sbc.credit_hold, sbc.pre_paid, sbc.property
							FROM systems_reports AS r
							LEFT JOIN systems_base_cont AS sbc ON sbc.ver_unique_id = r.system_ver_unique_id
							WHERE r.`status`='open'
							ORDER BY r.id ASC;";
				}else{
					$sql = "SELECT r.*, sbc.credit_hold, sbc.pre_paid, sbc.property
							FROM systems_reports AS r
							LEFT JOIN systems_base_cont AS sbc ON sbc.ver_unique_id = r.system_ver_unique_id
							WHERE r.`status`='open' AND user_id='$myusername'
							ORDER BY r.id ASC;";
				}
				d($sql);
				if(!$resultOpen = $mysqli->query($sql)){
					$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
					$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
					$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
				}
			?>  
            <div id="ReportFrame">     
            <div id="ReportFrameTitle"> Unfinished Reports </div>
            <div id="openReprotsDiv" <?php if($resultOpen->num_rows == 0){echo "style=\"display:none\"";} ?>>
	            <div class="legend">Color Legend:&nbsp;<span class="credit_hold">Credit Hold</span>&emsp;<span class="pre_paid">Pre-Paid</span>&emsp;<span class="future">Future</span></div>
                <table width="100%" id="openReportsTable" >
                    <thead>
                        <tr>
                            <th width="75px">Date Started</th>
                            <th width="50px">Report ID</th>
                            <th width="50px">System ID</th>
                            <th>System Nickname</th>
                            <th>Engineer</th>
                            <th>Complaint</th>                   
                        </tr>
                    </thead>
                    <tbody>
                    	<?php
						if($resultOpen){
							while($rowOpen = $resultOpen->fetch_assoc())
							{								
								if(strtolower($rowOpen['credit_hold']) == "y"){
									echo "<tr class=\"credit_hold\">\n";
								}elseif(strtolower($rowOpen['pre_paid']) == "y"){
									echo "<tr class=\"pre_paid\">\n";
								}else{
									echo "<tr>\n";
								}
								echo "<td><a href=\"/report/common/report_new_edit.php?id=".$rowOpen['unique_id'].'&uid='.$myusername."\">". date(phpdispfd,strtotime($rowOpen['date']))."</a></td>\n";
								echo "<td>". $rowOpen['report_id']."</td>\n";
								echo "<td>". $rowOpen['system_id']."</td>\n";
								echo "<td>". $rowOpen['system_nickname']."</td>\n";
								echo "<td>". $arr_engineers[$rowOpen['assigned_engineer']]."</td>\n";
								echo "<td>". limit_words($rowOpen['complaint'],$comp_word_limt)."</td>\n";
								echo "</tr>\n";
							}
						}
						?>                        
                    </tbody>
                </table>
            </div>
            <div id="openReportsDiv" <?php if($resultOpen->num_rows != 0){echo "style=\"display:none\"";}else{echo "style=\"text-align:center\"";} ?>>
            	<br />
                <h2>No Unfinished Reports for <?php echo $_SESSION['userdata']['name'];?></h2>
            </div>
          </div>
        </div>
         <div class="line"></div>
     <?php  

				if(isset($_COOKIE["showall"])){
					$sql = "SELECT sbc.ver_unique_id, sb.system_id, sbc.nickname, sb.pm_dates, sb.last_pm, sb.pm_freq, u.name AS engineer, sbc.ver, sbc.credit_hold, sbc.pre_paid, sbc.property, sr.request_num, sr.pm_date
							FROM systems_base_cont AS sbc
							LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
							LEFT JOIN systems_assigned_pri AS sap ON sap.system_ver_unique_id = sbc.ver_unique_id
							LEFT JOIN users AS u ON sap.uid = u.uid
							LEFT JOIN systems_requests AS sr ON sr.system_ver_unique_id = sbc.ver_unique_id AND sr.pm = 'y' AND sr.`status` = 'open'
							WHERE sbc.property = 'c' 
							AND CAST(sb.pm_freq AS UNSIGNED) != 0 
							AND sb.pm_start != ''
							ORDER BY sb.last_pm ASC;";
				}else{
						$sql = "SELECT sbc.ver_unique_id, sb.system_id, sbc.nickname, sb.pm_dates, sb.last_pm, sb.pm_freq, u.name AS engineer, sbc.ver, sbc.credit_hold, sbc.pre_paid, sbc.property, sr.request_num, sr.pm_date
								FROM systems_base_cont AS sbc
								LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
								LEFT JOIN systems_assigned_pri AS sap ON sap.system_ver_unique_id = sbc.ver_unique_id
								LEFT JOIN users AS u ON sap.uid = u.uid
								LEFT JOIN systems_requests AS sr ON sr.system_ver_unique_id = sbc.ver_unique_id AND sr.pm = 'y' AND sr.`status` = 'open'
								WHERE sbc.property = 'c' 
								AND CAST(sb.pm_freq AS UNSIGNED) != 0 
								AND sb.pm_start != ''
								AND sap.uid = '".$myusername."'
								ORDER BY sb.last_pm ASC;";
				}
				d($sql);
				if(!$resultPM = $mysqli->query($sql)){
					$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
					$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
					$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
				}
				
		?>
    <div id="ReportFrame">
        <div id="ReportFrameTitle">PMs Due</div>

        <div id="pmDiv" <?php if($resultPM->num_rows == 0){echo "style=\"display:none\"";} ?>>
	        <div class="legend">Showing next <?php echo $settings->pm->look_ahead; ?> days</div>
	        <div class="legend">Color Legend:&nbsp;<span class="credit_hold">Credit Hold</span>&emsp;<span class="pre_paid">Pre-Paid</span>&emsp;<span class="future">Future</span>&emsp;<span class="late">PM Late <span class="smaller">( > <?php echo $settings->pm->late; ?> days )</span></span>&emsp;<span class="overdue">PM Overdue <span class="smaller">( > <?php echo $settings->pm->overdue; ?> days )</span></span></div>
	        <table width="100%" id="pmTable" >
                <thead>                    
                <th width="75px">Due Date</th>
                        <th>Last PM</th>
                        <th>System ID</th>
                        <th width="40%">System Nickname</th>
                        <th>Engineer</th>
                        <th>Scheduled</th>
                        <th>Request ID</th>
                </thead>
                <tbody>
                    <?php
                        $pm_systems = array();
                        $look_ahead = $now->addDays($settings->pm->look_ahead);
						while($rowPM = $resultPM->fetch_assoc()){
							$last_pm = new \Moment\Moment($rowPM['last_pm']);
							if($last_pm <= $now){
								$pm_dates = json_decode($rowPM['pm_dates']);

								$pmd_array = array();
								foreach($pm_dates as $key=>$val){
									$pm_date_init = new \Moment\Moment($now->getYear() . '-' . sprintf('%02d', $val[0]) . '-' . sprintf('%02d', $val[1]), null, true);
									$pm_date_init_prev = $pm_date_init->subtractYears(1);
									$pm_date_init_next = $pm_date_init->addYears(1);

									$start = $pm_date_init_prev->subtractMonths(intval($rowPM['pm_freq']))->addDays(31)->format('Y-m-d');
									$end = $pm_date_init_prev->addDays(30)->format('Y-m-d');
									$pmd_array[$pm_date_init_prev->format('Y-m-d')] = array('start' => $start, 'end' => $end);

									$start = $pm_date_init->subtractMonths(intval($rowPM['pm_freq']))->addDays(31)->format('Y-m-d');
									$end = $pm_date_init->addDays(30)->format('Y-m-d');
									$pmd_array[$pm_date_init->format('Y-m-d')] = array('start' => $start, 'end' => $end);

									$start = $pm_date_init_next->subtractMonths(intval($rowPM['pm_freq']))->addDays(31)->format('Y-m-d');
									$end = $pm_date_init_next->addDays(30)->format('Y-m-d');
									$pmd_array[$pm_date_init_next->format('Y-m-d')] = array('start' => $start, 'end' => $end);
								}
								ksort($pmd_array);

								$next_pm = false;
								$keys = array_keys($pmd_array);
								foreach($pmd_array as $key=>$val){
									if($last_pm->isBetween(new \Moment\Moment($val['start']), new \Moment\Moment($val['end']))){
										$next_pm = $key;
										break;
									}
								}
								$key = array_search($next_pm, $keys);
								$next_pm = $keys[$key + 1];
								$next_pm_moment = new Moment\Moment($next_pm);

								if($rowPM['request_num'] != "" and $rowPM['pm_date'] == ""){
									$scheduled = "Contact Dispatch";
								}else{
									$scheduled = $rowPM['pm_date'];
								}


								//Limit to next X days look ahead
								if($next_pm_moment <= $look_ahead){
									$pm_systems[$rowPM['system_id']] = array(
										'ver_unique_id'=>$rowPM['ver_unique_id'],
										'system_id'=>$rowPM['system_id'],
										'nickname'=>$rowPM['nickname'],
										'last_pm' => $last_pm->format('Y-m-d'),
										'engineer'=>$rowPM['engineer'],
										'ver'=>$rowPM['ver'],
										'credit_hold'=>$rowPM['credit_hold'],
										'pre_paid'=>$rowPM['pre_paid'],
										'property'=>$rowPM['property'],
										'next_pm' => $next_pm,
										'request' => $rowPM['request_num'],
										'sched' => $scheduled
									);
								}

							}
						}
                        $php_utils->customUasort($pm_systems, 'next_pm');
                        //d($pm_systems);
						foreach($pm_systems as $key=>$val){

							if(strtolower($val['property']) == 'f'){
								echo "<tr class=\"future\">\n";
							}elseif(strtotime($val['next_pm'].' + '.$settings->pm->overdue.' days') < time()){
								echo "<tr class=\"overdue\">\n";
							}elseif(strtotime($val['next_pm'].' + '.$settings->pm->late.' days') < time()){
								echo "<tr class=\"late\">\n";
							}elseif(strtolower($val['credit_hold']) == "y"){
								echo "<tr class=\"credit_hold\">\n";
							}elseif(strtolower($val['pre_paid']) == "y"){
								echo "<tr class=\"pre_paid\">\n";
							}else{
								echo "<tr>\n";
							}
							if(strtolower($_SESSION['perms']['service_request']) == "y"){
								echo "<td><a href=\"/report/common/request_new_edit.php?system_id=" . $val['system_id'] . "&user_id=" . $myusername . "&pm\">" . date(phpdispfd, strtotime($val['next_pm'])) . "</a></td>\n";
							}else{
								echo "<td><a href=\"/report/common/systems_view.php?ver_unique_id=" . $val['ver_unique_id'] . "&pm\">" . $val['next_pm'] . "</a></td>\n";
								//echo "<td><a onClick=\"alert('Contact Dispatch to open a pm request.');\">". $rowPM['due']."</a></td>\n";	
							}
							echo "<td>" . date(phpdispfd, strtotime($val['last_pm'])) . "</td>\n";
							echo "<td>" . $val['system_id'] . "</td>\n";
							echo "<td>" . $val['nickname'] . "</td>\n";
							echo "<td>" . $val['engineer'] . "</td>\n";
							echo "<td>" . $val['sched'] . "</td>\n";
							echo "<td>" . $val['request'] . "</td>\n";
							echo "</tr>\n";
						}

						?>     					
                </tbody>
            </table>
        </div>
        <div id="pmDiv" <?php if($resultPM->num_rows != 0){echo "style=\"display:none\"";}else{echo "style=\"text-align:center\"";} ?>>
            	<br />
                <h2>No PMs Due for <?php echo $_SESSION['userdata']['name'];?></h2>
        </div>
    </div>
        <div class="line"></div>
        <div id="ReportFrame">
        <div id="ReportFrameTitle">Completed Reports</div>
        <div id="pastReprotsDiv">
            <table width="100%" id="pastReportsTable" >
                <thead>
                    <tr>
                    	<th width="1px">idx</th>
                        <th width="75px">Date</th>
                        <th width="50px">Report ID</th>
                        <th width="50px">System ID</th>
                        <th width="100px">System Nickname</th>
                        <th width="75px">Engineer</th>
                        <th width="150px">Complaint</th>
                        <th>Service</th>
                    </tr>
                </thead>
                <tbody>    
                </tbody>
            </table>
        </div>
    </div>
</div>
    </div>
    <br> 
</div>
<?php require_once($footer_include); ?>