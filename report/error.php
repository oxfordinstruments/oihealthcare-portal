<?php
//Update Completed 12/12/14
$remove_session = true;
$no_redirect = true;
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

if(isset($_GET['e'])) {$error=$_GET['e'];}else{$error="unknown";}
if(isset($_GET['n'])) {$number=$_GET['n'];}else{$number="NA";}
if(isset($_GET['p'])) {$page=$_GET['p'];}else{$page="NA";}

error($error,$number,$page);

?>
<!DOCTYPE html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<?php require_once($js_include);?>

</head>
<body>
<?php require_once($no_login_header); ?>
<div id="OIReportContent"> 
    <h1>Error has occured.</h1><br>
	
	<h2>Error Description:&nbsp;
	<?php echo $error; ?><br>
	Page in error:&nbsp;
	<?php echo $page; ?><br>
	Error Number:&nbsp;
	<?php echo $number; ?> </h2>
	<br>
	<h2>Please contact support at <a href="mailto:support@oihealthcareportal.com">support@oihealthcareportal.com</a> if this error persists.</h2>
	<h2>If you were logged into the portal then you have now been logged out for security.</h2>
</div>
<?php require_once($footer_include); ?>