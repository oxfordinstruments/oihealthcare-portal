<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

$comp_word_limt 	 = intval($settings->complaint_word_limit);
$serv_word_limt		 = intval($settings->service_word_limit);
$custact_word_limt	 = intval($settings->custact_word_limit);
$probrpt_word_limt	 = intval($settings->probrpt_word_limit);


$myusername = $_SESSION["login"];

$sql="SELECT * FROM `systems_status`";
if(!$resultStatus = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$arr_status = array();
while($rowStatus = $resultStatus->fetch_assoc())
{
	array_push_assoc($arr_status, $rowStatus['id'], $rowStatus['status']);
}

$sql="SELECT u.uid, u.name
FROM users AS u
LEFT JOIN users_roles AS ur ON ur.uid = u.uid
LEFT JOIN users_role_id AS rid ON rid.id = ur.rid
WHERE rid.role = 'role_engineer' OR rid.role = 'role_contractor';";
if(!$resultEngineers = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);	
}
$arr_engineers = array();
while($rowEngineers = $resultEngineers->fetch_assoc())
{
	array_push_assoc($arr_engineers, $rowEngineers['uid'], $rowEngineers['name']);
}
$sql="SELECT sb.system_id, sbc.nickname, sbc.contract_end_date, sbc.warranty_end_date, sb.unique_id
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
LEFT JOIN systems_types AS e ON e.id = sb.system_type
WHERE DATEDIFF(DATE(sbc.contract_end_date), NOW()) < 30 
AND DATE(sbc.contract_end_date) >= DATE_SUB(NOW(), INTERVAL 1 DAY) 
AND e.`type` = 'CT' AND sbc.property = 'C';";
if(!$resultCTContractExpire30 = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);	
}
$numResultCTContractExpire30 = $resultCTContractExpire30->num_rows;

$sql="SELECT sb.system_id, sbc.nickname, sbc.contract_end_date, sbc.warranty_end_date, sb.unique_id
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
LEFT JOIN systems_types AS e ON e.id = sb.system_type
WHERE DATEDIFF(DATE(sbc.contract_end_date), NOW()) < 30 
AND DATE(sbc.contract_end_date) >= DATE_SUB(NOW(), INTERVAL 1 DAY) 
AND e.`type` = 'MR' AND sbc.property = 'C';";
if(!$resultMRContractExpire30 = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$numResultMRContractExpire30 = $resultMRContractExpire30->num_rows;

$sql="SELECT sb.system_id, sbc.nickname, sbc.contract_end_date, sbc.warranty_end_date, sb.unique_id
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
LEFT JOIN systems_types AS e ON e.id = sb.system_type
WHERE DATEDIFF(DATE(sbc.warranty_end_date), NOW()) < 30 
AND DATE(sbc.warranty_end_date) >= DATE_SUB(NOW(), INTERVAL 1 DAY) 
AND e.`type` = 'CT' AND sbc.property = 'C';";
if(!$resultCTWarrentyExpire30 = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$numResultCTWarrentyExpire30 = $resultCTWarrentyExpire30->num_rows;

$sql="SELECT sb.system_id, sbc.nickname, sbc.contract_end_date, sbc.warranty_end_date, sb.unique_id
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
LEFT JOIN systems_types AS e ON e.id = sb.system_type
WHERE DATEDIFF(DATE(sbc.warranty_end_date), NOW()) < 30 
AND DATE(sbc.warranty_end_date) >= DATE_SUB(NOW(), INTERVAL 1 DAY) 
AND e.`type` = 'MR' AND sbc.property = 'C';";
if(!$resultMRWarrentyExpire30 = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$numResultMRWarrentyExpire30 = $resultMRWarrentyExpire30->num_rows;

$sql="SELECT sb.system_id, sbc.nickname, sbc.contract_end_date, sbc.warranty_end_date, sb.unique_id
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
LEFT JOIN systems_types AS e ON e.id = sb.system_type
WHERE DATEDIFF(DATE(sbc.contract_end_date), NOW()) < 120 
AND DATE(sbc.contract_end_date) >= DATE_SUB(NOW(), INTERVAL 1 DAY) 
AND e.`type` = 'CT' AND sbc.property = 'C';";
if(!$resultCTContractExpire120 = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$numResultCTContractExpire120 = $resultCTContractExpire120->num_rows;

$sql="SELECT sb.system_id, sbc.nickname, sbc.contract_end_date, sbc.warranty_end_date, sb.unique_id
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
LEFT JOIN systems_types AS e ON e.id = sb.system_type
WHERE DATEDIFF(DATE(sbc.contract_end_date), NOW()) < 120 
AND DATE(sbc.contract_end_date) >= DATE_SUB(NOW(), INTERVAL 1 DAY) 
AND e.`type` = 'MR' AND sbc.property = 'C';";
if(!$resultMRContractExpire120 = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$numResultMRContractExpire120 = $resultMRContractExpire120->num_rows;

$sql="SELECT sb.system_id, sbc.nickname, sbc.contract_end_date, sbc.warranty_end_date, sb.unique_id
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
LEFT JOIN systems_types AS e ON e.id = sb.system_type
WHERE DATEDIFF(DATE(sbc.warranty_end_date), NOW()) < 120 
AND DATE(sbc.warranty_end_date) >= DATE_SUB(NOW(), INTERVAL 1 DAY) 
AND e.`type` = 'CT' AND sbc.property = 'C';";
if(!$resultCTWarrentyExpire120 = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$numResultCTWarrentyExpire120 = $resultCTWarrentyExpire120->num_rows;

$sql="SELECT sb.system_id, sbc.nickname, sbc.contract_end_date, sbc.warranty_end_date, sb.unique_id
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
LEFT JOIN systems_types AS e ON e.id = sb.system_type
WHERE DATEDIFF(DATE(sbc.warranty_end_date), NOW()) < 120 
AND DATE(sbc.warranty_end_date) >= DATE_SUB(NOW(), INTERVAL 1 DAY) 
AND e.`type` = 'MR' AND sbc.property = 'C';";
if(!$resultMRWarrentyExpire120 = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$numResultMRWarrentyExpire120 = $resultMRWarrentyExpire120->num_rows;

$ct_contracts_expire30 = $numResultCTContractExpire30;
$ct_warranties_expire30 = $numResultCTWarrentyExpire30;
$mr_contracts_expire30 = $numResultMRContractExpire30;
$mr_warranties_expire30 = $numResultMRWarrentyExpire30;

$ct_contracts_expire120 = $numResultCTContractExpire120;
$ct_warranties_expire120 = $numResultCTWarrentyExpire120;
$mr_contracts_expire120 = $numResultMRContractExpire120;
$mr_warranties_expire120 = $numResultMRWarrentyExpire120;

function array_push_assoc(&$array, $key, $value){
	$array[$key] = $value;
	return $array;
}
function limit_words($string, $word_limit)
{
	$words = explode(" ",$string);
	if(count($words) > $word_limit){
		return implode(" ",array_splice($words,0,$word_limit))."...(more)";
	}else{
		return implode(" ",array_splice($words,0,$word_limit));
	}
}

?>
<!DOCTYPE html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<?php require_once($js_include);?>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">

$(document).ready(function() {	
		invoicedTable = $('#invoicedTable').dataTable({
			"bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "../common/connectors/invoiced_reports_connector.php",
			"bJQueryUI": true,
			"aLengthMenu": [[5, 10, 25, 50], [5, 10, 25, 50]],
			"iCookieDuration": 60*60*24*365, // 1 year
			"iDisplayLength": 5,
			"sPaginationType": "full_numbers",
			"aaSorting": [ [0,'desc'] ], 			
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
					$('td:eq(0)', nRow).html( function() {	
						return "<a href='/report/common/report_invoice.php?ed&id=" + aData[8] + "&uid=<?php echo $myusername ?>'>" + $(this).text() + "</a>";
					});
					this.$('tr').click( function () {
               		 var href = $(this).find("a").attr("href");
					 if(href) {
						window.location = href;
					 }
           		 });
			},
		});
			
/////////////////////////////////////////////////////////////////////////////////////		
		holdTable = $('#holdTable').dataTable({
			"bJQueryUI": true,
			"iCookieDuration": 60*60*24*365, // 1 year
			"sPaginationType": "full_numbers",
			"aaSorting": [ [1,'asc'] ],
			"fnInitComplete": function () {
           		 this.$('tr').click( function () {
               		 var href = $(this).find("a").attr("href");
					 if(href) {
						window.location = href;
					 }
           		 });
       		 }
		});
/////////////////////////////////////////////////////////////////////////////////////		
		prepaidTable = $('#prepaidTable').dataTable({
			"bJQueryUI": true,
			"iCookieDuration": 60*60*24*365, // 1 year
			"sPaginationType": "full_numbers",
			"aaSorting": [ [1,'asc'] ],
			"fnInitComplete": function () {
           		 this.$('tr').click( function () {
               		 var href = $(this).find("a").attr("href");
					 if(href) {
						window.location = href;
					 }
           		 });
       		 }
		});
				
/////////////////////////////////////////////////////////////////////////////////////
		needInvTable = $('#needInvTable').dataTable({
			"bJQueryUI": true,
			"aLengthMenu": [[5, 10, 25, 50], [5, 10, 25, 50]],
			"iDisplayLength": 10,
			"iCookieDuration": 60*60*24*365, // 1 year
			"sPaginationType": "full_numbers",
			"aaSorting": [ [0,'desc'] ],
			 "fnInitComplete": function () {
           		 this.$('tr').click( function () {
               		 var href = $(this).find("a").attr("href");
					 if(href) {
						window.location = href;
					 }
           		 });
       		 }
		});
/////////////////////////////////////////////////////////////////////////////////////		
		pastTable = $('#pastReportsTable').dataTable({
			"bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "../common/connectors/completed_reports_connector.php",
			"bJQueryUI": true,
			"aLengthMenu": [[5, 10, 25, 50], [5, 10, 25, 50]],
			"iCookieDuration": 60*60*24*365, // 1 year
			"iDisplayLength": 5,
			"sPaginationType": "full_numbers",
			"aaSorting": [ [2,'desc'] ], 			
			"aoColumnDefs": [
                        { "bSearchable": false, "bVisible": false, "aTargets": [ 0 ] }
            ],
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
					$('td:eq(0)', nRow).html( function() {	
						var moment_dt = moment($(this).text()).tz(aData[9]);
						return "<a href='/report/common/report_view.php?id=" + aData[8] + "&uid=<?php echo $myusername ?>'>" + moment_dt.format('<?php echo jsdispfd; ?>') + "</a>";
					});
					this.$('tr').click( function () {
               		 var href = $(this).find("a").attr("href");
					 if(href) {
						window.location = href;
					 }
           		 });
			},
			"fnInitComplete": function () {
           		 this.$('tr').addClass("pastReportsHelp");
       		},
			"fnServerParams": function ( aoData ) {
			  aoData.push( { "name": "oiuid", "value": "<?php echo $myusername ?>" },
			  			   { "name": "oiall", "value": "1"} ); //change to 0 for just engineer
			}
		});
/////////////////////////////////////////////////////////////////////////////////////			
		tableContWarr = $('#tableContWarr').dataTable({
			"bJQueryUI": true,
			"iCookieDuration": 60*60*24*365, // 1 year
			"sPaginationType": "full_numbers",
			"aaSorting": []			
		});	

		$("div#needInvTable_filter").children("label").children("input").focus();

		$(document).keyup(function(e) {
			$("#needInvTable").each(function(index, element) {
				if($(this).children("tbody").children("tr").length == 1 && e.which == 13){
					$(this).find("td > a").click();
				}
			});
			
			$("#invoicedTable").each(function(index, element) {
				if($(this).children("tbody").children("tr").length == 1 && e.which == 13){
					$(this).find("td > a").click();
				}
			});
			
			$("#holdTable").each(function(index, element) {
				if($(this).children("tbody").children("tr").length == 1 && e.which == 13){
					$(this).find("td > a").click();
				}
			});

			$("#pastReportsTable").each(function(index, element) {
				if($(this).children("tbody").children("tr").length == 1 && e.which == 13){
					$(this).find("td > a").click();
				}
			});
			
		});
		
		$('.manageInfo').fancybox({
			//'autoScale'			: true,
			'width'				: '100%',
			'height'			: '100%',
			//'padding'			: '0',
			'transitionIn'		: 'none',
			'transitionOut'		: 'none',
			'type'				: 'inline',
			//'autoDimensions'	: true,
			'centerOnScroll'	: true
		});
});
function drawChart() {
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	// Warrenty and Contract expire Chart 30 Days
	var data30 = google.visualization.arrayToDataTable([
	  ['','CT Contracts', 'CT Warranties', 'MR Contracts', 'MR Warranties'],
	  ['',<?php echo $ct_contracts_expire30.",".$ct_warranties_expire30.",".$mr_contracts_expire30.",".$mr_warranties_expire30; ?>]
	]);
	var options = {
		hAxis: {title: ""},
		chartArea:{width:'50%'},
		//bar:{groupWidth:'40%'},
		title: 'Contracts and Warranties Expiring Within 30 Days Snapshot',
		width:490,
		height:300
	};
		
	var chart30 = new google.visualization.ColumnChart(document.getElementById('chart_contracts_expire30'));	
	
	google.visualization.events.addListener(chart30, 'select', clickHandlerChart30);
	
	chart30.draw(data30, options);
	
	function clickHandlerChart30(){
		selectedData = chart30.getSelection();
		if (selectedData.length >= 1){
			console.log(selectedData);
			col = selectedData[0].column;
			item = data30.getValue(0,col);
			console.log(col +" "+ item);
			tableContWarr.fnClearTable();
			switch(col){
				case 1:
					chart30rows1();
					break;
				case 2:
					chart30rows2();
					break;
				case 3:
					chart30rows3();
					break;
				case 4:	
					chart30rows4();
					break;
			}
			
			$('#aContWarr').click();
		}
	}
	
	function chart30rows1() {
			<?php
			while($rowData = $resultCTContractExpire30->fetch_assoc()){
				echo "tableContWarr.fnAddData(['".$rowData['system_id']."','".$rowData['nickname']."','".date(phpdispfd,strtotime($rowData['contract_end_date']))."']);\n";
			}
			?>
	}
	function chart30rows2() {
			<?php
			while($rowData = $resultCTWarrentyExpire30->fetch_assoc()){
				echo "tableContWarr.fnAddData(['".$rowData['system_id']."','".$rowData['nickname']."','".date(phpdispfd,strtotime($rowData['contract_end_date']))."']);\n";
			}
			?>
	}
	function chart30rows3() {
			<?php
			while($rowData = $resultMRContractExpire30->fetch_assoc()){
				echo "tableContWarr.fnAddData(['".$rowData['system_id']."','".$rowData['nickname']."','".date(phpdispfd,strtotime($rowData['contract_end_date']))."']);\n";
			}
			?>
	}
	function chart30rows4() {
			<?php
			while($rowData = $resultMRWarrentyExpire30->fetch_assoc()){
				echo "tableContWarr.fnAddData(['".$rowData['system_id']."','".$rowData['nickname']."','".date(phpdispfd,strtotime($rowData['contract_end_date']))."']);\n";
			}
			?>
	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	// Warrenty and Contract expire Chart 120 Days
	var data120 = google.visualization.arrayToDataTable([
	  ['','CT Contracts', 'CT Warranties', 'MR Contracts', 'MR Warranties'],
	  ['',<?php echo $ct_contracts_expire120.",".$ct_warranties_expire120.",".$mr_contracts_expire120.",".$mr_warranties_expire120; ?>]
	]);

	var options = {
		hAxis: {title: ""},
		chartArea:{width:'50%'},
		//bar:{groupWidth:'40%'},
		title: 'Contracts and Warranties Expiring Within 120 Days Snapshot',
		width:490,
		height:300
	};
	var chart120 = new google.visualization.ColumnChart(document.getElementById('chart_contracts_expire120'));
	
	google.visualization.events.addListener(chart120, 'select', clickHandlerChart120);
	
	chart120.draw(data120, options);
	
	function clickHandlerChart120(){
		selectedData = chart120.getSelection();
		if (selectedData.length >= 1){
			col = selectedData[0].column;
			item = data120.getValue(0,col);
			console.log(col +" "+ item);
			tableContWarr.fnClearTable();
			switch(col){
				case 1:
					chart120rows1();
					break;
				case 2:
					chart120rows2();
					break;
				case 3:
					chart120rows3();
					break;
				case 4:	
					chart120rows4();
					break;
			}
			
			$('#aContWarr').click();
		}
	}
	
	function chart120rows1() {
			<?php
			while($rowData = $resultCTContractExpire120->fetch_assoc()){
				echo "tableContWarr.fnAddData(['".$rowData['system_id']."','".$rowData['nickname']."','".date(phpdispfd,strtotime($rowData['contract_end_date']))."']);\n";
			}
			?>
	}
	function chart120rows2() {
			<?php
			while($rowData = $resultCTWarrentyExpire120->fetch_assoc()){
				echo "tableContWarr.fnAddData(['".$rowData['system_id']."','".$rowData['nickname']."','".date(phpdispfd,strtotime($rowData['contract_end_date']))."']);\n";
			}
			?>
	}
	function chart120rows3() {
			<?php
			while($rowData = $resultMRContractExpire120->fetch_assoc()){
				echo "tableContWarr.fnAddData(['".$rowData['system_id']."','".$rowData['nickname']."','".date(phpdispfd,strtotime($rowData['contract_end_date']))."']);\n";
			}
			?>
	}
	function chart120rows4() {
			<?php
			while($rowData = $resultMRWarrentyExpire120->fetch_assoc()){
				echo "tableContWarr.fnAddData(['".$rowData['system_id']."','".$rowData['nickname']."','".date(phpdispfd,strtotime($rowData['contract_end_date']))."']);\n";
			}
			?>
	}
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Load the Visualization API and the piechart package.
  google.load('visualization', '1.0', {'packages':['corechart']});
// Set a callback to run when the Google Visualization API is loaded.
  google.setOnLoadCallback(drawChart);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
</script>
</head>
<body>
<?php require_once($header_include); ?>
    
<div id="OIReportContent"> 
	 <div id="ReportOuterFrame">
	        <?php  
				$sql = "SELECT r.report_id,r.system_id,r.system_nickname,u.name,r.valuation_date,r.unique_id
						FROM systems_reports AS r
						LEFT JOIN users AS u ON r.valuation_uid = u.uid
						WHERE r.invoice_required='y' AND r.valuation_complete='y' AND r.invoiced != 'y' AND r.valuation_do_not_invoice = 'N' AND r.deleted = 'n'
						ORDER BY r.report_id ASC;";
				if(!$resultNeedInv = $mysqli->query($sql)){
					$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
					$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
					$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);	
				}
			?>  
            <div id="ReportFrame">     
            <div id="ReportFrameTitle">Service Reports To Be Invoiced</div>
            <div id="openReprotsDiv" <?php if($resultNeedInv->num_rows == 0){echo "style=\"display:none\"";} ?>>
                <table width="100%" id="needInvTable" >
                    <thead>
                        <tr>
                            <th>Report ID</th>
							<th>System ID</th>
                            <th>System Name</th>
                            <th>Valued By</th>
                            <th>Valued Date</th>                   
                        </tr>
                    </thead>
                    <tbody>
                    	<?php
						if($resultNeedInv){
							while($rowNeedInv = $resultNeedInv->fetch_assoc())
							{
								echo "<tr>\n";
								echo "<td><a href=\"/report/common/report_invoice.php?id=".$rowNeedInv['unique_id']."&uid=".$_SESSION['login']."\">". $rowNeedInv['report_id']."</a></td>\n";
								echo "<td>". $rowNeedInv['system_id']."</td>\n";
								echo "<td>". $rowNeedInv['system_nickname']."</td>\n";
								echo "<td>". $rowNeedInv['name']."</td>\n";
								echo "<td>". date(phpdispfd,strtotime($rowNeedInv['valuation_date']))."</td>\n";
								echo "</tr>\n";
							}
						}
						?>                        
                    </tbody>
                </table>
            </div>
			<div id="openReprotsDiv" <?php if($resultNeedInv->num_rows != 0){echo "style=\"display:none\"";}else{echo "style=\"text-align:center\"";} ?>>
            	<br />
            	<h2>No Service Reports Need Invoicing</h2>
            </div>
          </div>
        </div>
		<div class="line"></div>	
        <div id="ReportOuterFrame">
            <div id="ReportFrame">     
            <div id="ReportFrameTitle">Invoiced Service Reports</div>
            <div id="requestsDiv">
                <table width="100%" id="invoicedTable">
                    <thead>
                        <tr>
                        	<th>Report ID</th>
							<th>System ID</th>
                            <th>System Name</th>
                            <th>Valued By</th>
                            <th>Valued Date</th> 
							<th>Invoiced By</th>
							<th>Invoice</th>
                            <th>Invoice Date</th>                   
                        </tr>
                    </thead>
                    <tbody>                    
                    </tbody>
                </table>
            </div>
          </div>
        </div>
		<div class="line"></div>
		<div id="ReportOuterFrame">
	        <?php  
				$sql = "SELECT sb.system_id, sbc.nickname, sbc.credit_hold_date, c.`type` as contract_type, sb.unique_id, sbc.ver
						FROM systems_base AS sb
						LEFT JOIN systems_base_cont AS sbc ON sb.unique_id = sbc.unique_id
						LEFT JOIN misc_contracts AS c ON sbc.contract_type = c.id
						WHERE sbc.credit_hold = 'y' AND sbc.property = 'C';";
				if(!$resultHold = $mysqli->query($sql)){
					$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
					$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
					$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
				}
			?>  
            <div id="ReportFrame">     
            <div id="ReportFrameTitle">Systems on Credit Hold</div>
            <div id="holdDiv" <?php if($resultHold->num_rows == 0){echo "style=\"display:none\"";} ?>>
                <table width="100%" id="holdTable">
                    <thead>
                        <tr>
							<th>On Hold Date</th>
							<th>System ID</th>
                            <th>System Name</th>
                            <th>Contract Type</th>
                        </tr>
                    </thead>
                    <tbody>
                    	<?php
						if($resultHold->num_rows != 0){
							while($rowHold = $resultHold->fetch_assoc())
							{
								//echo "<tr class=\"requestsHelp\">\n";
								echo "<tr>\n";
								echo "<td><a href=\"/report/common/systems.php?e&unique_id=".$rowHold['unique_id']."&version=".$rowHold['ver']."\">". date(phpdispfd,strtotime($rowHold['credit_hold_date']))."</a></td>\n";
								//echo "<td>". $rowHold['credit_hold_date']."</td>\n";
								echo "<td>". $rowHold['system_id']."</td>\n";
								echo "<td>". $rowHold['nickname']."</td>\n";
								echo "<td>". $rowHold['contract_type']."</td>\n";
								echo "</tr>\n";
							}
						}
						?>                        
                    </tbody>
                </table>
            </div>
            <div id="holdDiv" <?php if($resultHold->num_rows != 0){echo "style=\"display:none\"";}else{echo "style=\"text-align:center\"";} ?>>
            	<br />
            	<h2>No systems on credit hold</h2>
            </div>
          </div>
        </div>
		<div class="line"></div>
		<div id="ReportOuterFrame">
	        <?php  
				$sql = "SELECT sbc.system_id, sbc.nickname, c.`type` as contract_type, sbc.unique_id, sbc.ver
						FROM systems_base_cont AS sbc
						LEFT JOIN systems_base AS sb ON sbc.unique_id = sb.unique_id
						LEFT JOIN misc_contracts AS c ON sbc.contract_type = c.id
						WHERE sbc.pre_paid = 'y' AND sbc.property = 'C';";
				if(!$resultPrepaid = $mysqli->query($sql)){
					$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
					$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
					$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
				}
			?>  
            <div id="ReportFrame">     
            <div id="ReportFrameTitle">Systems Pre-Paid Service</div>
            <div id="prepaidDiv" <?php if($resultPrepaid->num_rows == 0){echo "style=\"display:none\"";} ?>>
                <table width="100%" id="prepaidTable">
                    <thead>
                        <tr>
							<th>System ID</th>
                            <th>System Name</th>
                            <th>Contract Type</th>
                        </tr>
                    </thead>
                    <tbody>
                    	<?php
						if($resultPrepaid->num_rows != 0){
							while($rowPrepaid = $resultPrepaid->fetch_assoc())
							{
								echo "<tr>\n";
								echo "<td><a href=\"/report/common/systems.php?e&unique_id=".$rowPrepaid['unique_id']."&version=".$rowPrepaid['ver']."\">". $rowPrepaid['system_id']."</a></td>\n";
								echo "<td>". $rowPrepaid['nickname']."</td>\n";
								echo "<td>". $rowPrepaid['contract_type']."</td>\n";
								echo "</tr>\n";
							}
						}
						?>                        
                    </tbody>
                </table>
            </div>
            <div id="holdDiv" <?php if($resultPrepaid->num_rows != 0){echo "style=\"display:none\"";}else{echo "style=\"text-align:center\"";} ?>>
            	<br />
            	<h2>No systems pre-paid service</h2>
            </div>
          </div>
        </div>
		<div class="line"></div>
			<table width="100%">
				<tr>
					<td align="center"><div id="chart_contracts_expire30"></div></td>
					<td align="center"><div id="chart_contracts_expire120"></div></td>
				</tr>
				<tr>
					<td align="center"><div id="chart_contracts"></div></td>
					<td align="center"><div id="chart_response"></div></td>
				</tr>
			</table>		
		<div class="line"></div>
		<div id="ReportFrame">
        <div id="ReportFrameTitle">Completed Reports</div>
			<div id="pastReprotsDiv">
				<table width="100%" id="pastReportsTable" >
					<thead>
						<tr>
							<th width="1px">idx</th>
							<th width="75px">Date</th>
							<th width="50px">Report ID</th>
							<th width="50px">System ID</th>
							<th width="100px">System Name</th>
							<th width="75px">Engineer</th>
							<th width="150px">Complaint</th>
							<th>Service</th>
						</tr>
					</thead>
					<tbody>    
					</tbody>
				</table>
			</div>
		</div>       
	</div>
	<div style="display:none">
		<a id="aContWarr" class="manageInfo" href="#divContWarr">CTContWarr</a>
		<div id="divContWarr">
			<table width="100%" id="tableContWarr">
				<thead>                    
					<th>System ID</th>
					<th>System Name</th>
					<th>Expire</th>
											
				</thead>
				<tbody>						
				</tbody>
			</table>
		</div>
	</div>	
</div>
<br> 
</div>
<?php require_once($footer_include); ?>