<div class="outerContainer" id="navBar">
    <div id="navBarTop" class="centerContainer container_12 withBottom">
        <div class="grid_12">
            <ul class="hztlNavList">
                <!--<li class="hztlNavItem hztlNavLarge withIcon"><img src="/resources/images/icons/nav-icon-global.png"  /> </li>-->
            </ul>
        </div>
    </div>
    <div id="navBarBottom" class="centerContainer container_12">
        <div class="grid_12" style="line-height:4px;">&nbsp;</div>
    </div>
</div>
<div class="outerContainer" id="social">
	<img src="/resources/images/comodo_secure_76x26_transp.png" width="76" height="26">
	<div style="padding-left:15px; font-weight:bold">
		<?php
		$ver = file_get_contents($settings->php_base_dir.'/'.$settings->ver_file);
		if($ver != false and trim($ver) != ""){
			echo "Portal Version ",$ver;
		}
		?>
	</div>
</div>
</body>
</html>