<div id="CMSHeaderDiv"> 
    <!-- leave blank --> 
</div>
<div id="tiplayer" style="position:absolute; visibility:hidden; z-index:10000;"></div>
<header class="outerContainer" id="header">
    <div class="innerContainer" id="headerBottom">
        <div class="centerContainer container_12">
		<div class="section">
                <div class="grid_9">
				&nbsp;
                </div>
            </div>
        
            <div class="section withPadding">
                <div class="grid_3"> <a class="image" target="new" id="headerLogo" href="http://www.oxford-instruments.com/businesses/service/ct-mr"><img src="/resources/images/common/oxford-instruments-logo.png" /></a> </div>
                <div class="grid_9">
					<nav id="mainNav" class="cf">
					<ul class="hztlNavList">
<!-- Home menu item -->
					<li class="hztlNavItem hztlNavParent"> <a class="hztlNavElem level2" href="/report/contractor/"> <span class="hztlNavText"><span>Dashboard</span></span> </a>
						<div class="megaNav noGridSpacing">
							<div class="megaNavInner cf">
								<div class="concertinaNav">
									<div class="grid_4">
										<div class="concertinaNavItems concertinaNavTopLevel concertinaNavItemsActive" style="background-color:inherit"> 
										</div>
									</div>
									<div class="grid_8">
										<div class="sliderViewport">
											<div class="sliderPanel">
												<div class="sliderItem sliderItemDefault"> <a href="/report/contractor/"><img src="/resources/images/global-home.jpg" width="455" height="200" class="imageMedium"  /></a>
													<h2 class="level3"> Portal Dashboard</h2>
													<p class="intro"> Click here to go the main portal dashboard</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</li>
<!-- Systems menu item -->
					<li class="hztlNavItem hztlNavParent"> <a class="hztlNavElem level2 iframeAllSites" href="/report/common/all_systems.php?q=1" target="_parent" ><span class="hztlNavText"><span>Systems</span></span> </a>
							<div class="megaNav noGridSpacing">
								<div class="megaNavInner cf">
									<div class="concertinaNav">
										<div class="grid_4">
											<div class="concertinaNavItems concertinaNavTopLevel concertinaNavItemsActive" style="background-color:inherit"> 
											</div>
										</div>
										<div class="grid_8">
											<div class="sliderViewport">
												<div class="sliderPanel">
													<div class="sliderItem sliderItemDefault"> <a class="iframeAllSites" href="/report/common/all_systems.php?q=1" target="new"><img class="imageMedium" id="CTMRSiteImage" src="/resources/images/mr.jpg"  /></a>
														<h2 class="level3"> Oxford Instruments Healthcare System List</h2>
														<p class="intro"> Click to open the system select list.</p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</li>
<!-- Support menu item -->					
					<li class="hztlNavItem hztlNavParent"> <a class="hztlNavElem level2"><span class="hztlNavText"><span>Support</span></span></a>
						<div class="megaNav noGridSpacing">
							<div class="megaNavInner cf">
								<div class="grid_4">
									<div class="vertNavOuter">
										<ul class="vertNavList">
											<li class="vertNavItem vertNavParent"> <a class="vertNavElem"><span>Change Password</span></a>
												<div class="grid_8 flyoutNav">                            	
													<h2 class="level3"> Change Your Password</h2>
													<br />
													<div>
														<div style="width:200px; float:left">
															<form id="pwdchange" name="pwdchange" method="post" autocomplete="off" action="/report/common/scripts/change_pwd.php">                                    
															<label>New Password<span class="small" id="txtSite" style='color:#FF0000'> (Required) </span></label>
															<input type="password" name="password1" id="password1" />
															<br />
															<label>New Password Again<span class="small" id="txtSite" style='color:#FF0000'> (Required) </span></label>
															<input type="password" name="password2" id="password2" />
															<br />
															<br />
															<!--<input id="pwdchangeSubmit" name="Submit" type="button" value="Change Password" class="button" onclick="document.forms['pwdchange'].submit(); return false;" />-->
															<a id="pwdchangeSubmit" name="Submit" class="menu_button_jquery_save" onclick="document.forms['pwdchange'].submit(); return false;" >Change Password</a>
															</form>
														</div>
														<div style="width:200px; float:left">
															Rules:
															<br />
															Atleast 6 characters
															<br />
															CaSe SenSitIve
															<br />
															Must contain letters AND numbers
															<br />
															<br />
														</div>
													</div>
													</div>
											 </li>
											<li class="vertNavItem vertNavParent"> <a class="vertNavElem"><span>Change Email</span></a>
												<div class="grid_8 flyoutNav">
													<h2 class="level3"> Change Your Email</h2>
													<br />
													<div style="width:200px">
														<form id="emailchange" name="emailchange" method="post" autocomplete="off" action="/report/common/scripts/change_email.php">                                    
														<label>Old Email<span class="small" id="txtSite" style='color:#FF0000'> (Required) </span></label>
														<input type="text" name="email0" id="email0" value="<?php echo $_SESSION['userdata']['email']; ?>" readonly />
														<br />
														<br />
														<label>New Email<span class="small" id="txtSite" style='color:#FF0000'> (Required) </span></label>
														<input type="text" name="email1" id="email1" />
														<br />
														<label>New Email Again<span class="small" id="txtSite" style='color:#FF0000'> (Required) </span></label>
														<input type="text" name="email2" id="email2" />
														<br />
														<br />
														<!--<input id="emailchangeSubmit" name="Submit" type="button" value="Change Email" class="button" onclick="emailSubmit(); return false;" />-->
														<a id="emailchangeSubmit" name="Submit" class="menu_button_jquery_save" onclick="emailSubmit(); return false;">Change Email</a>                                   
														<input type="hidden" name="emailcheck" id="emailcheck" value="" />                                    
					
														</form>
													</div>
												</div>
											</li>
											<li class="vertNavItem vertNavParent"> <a class="vertNavElem"><span>Cell Phone & SMS Alerts</span></a>
											   <div class="grid_8 flyoutNav">
													<h2 class="level3"> Change Your Cell Number, Carrier, and SMS Alerts</h2>
													<br />
													<div style="width:200px">
														<form id="cellinfochange" name="cellinfochange" method="post" autocomplete="off" action="/report/common/scripts/change_cell_info.php">
														<label>Receive SMS Alerts</label>
														<input name="menu_get_sms" type="hidden" value="N">
														<input name="menu_get_sms" type="checkbox" class="menuiButton" value="Y" <?php if(strtolower($_SESSION['prefs']['pref_rcv_sms']) == "y"){echo "checked";}   ?>>                                    
														
														<br />
														<label>Cell Number  123-123-1234</label>
														<input name="menu_cell" id="menu_cell" type="text" value="<?php echo $_SESSION['userdata']['cell']; ?>" <?php if(strtolower($_SESSION['perms']['perm_change_phone'])== "n"){echo " disabled";} ?> />
														<br />
														<label class="red">Cell Carrier</label>
														<select id="menu_carrier" name="menu_carrier" <?php  if(strtolower($_SESSION['perms']['perm_change_phone'])== "n"){echo " disabled";} ?>>
														<option value="" title=""></option>
															<?php
															while($menuRowC = $menuResultCarrier->fetch_assoc())
															  {
																echo "<option value='".$menuRowC['carrierid']."' title='".$menuRowC['carrierid']."' ";
																if($_SESSION['userdata']['carrier']==$menuRowC['carrierid']){echo "selected='selected'";}
																echo ">".$menuRowC['company']." - ".$menuRowC['address']."</option>\n";
															  }
															?>
														</select>
														<br />
														<br />
														<!--<input id="cellinfochangeSubmit" name="Submit" type="button" value="Change Cell Info" class="button" onclick="cellInfoSubmit(); return false;" /> -->
														<a id="cellinfochangeSubmit" name="Submit" class="menu_button_jquery_save" onclick="cellInfoSubmit(); return false;" >Change Cell Info</a>                                
														</form>
													</div>
												</div>
											</li>
											 <li class="vertNavItem vertNavParent"> <a class="vertNavElem"><span>Email Notifications</span></a>
											   <div class="grid_8 flyoutNav">
													<h2 class="level3"> Email Notifications</h2>
													<br />
													<div style="width:250px">
														<form id="emailnotify" name="emailnotify" method="post" autocomplete="off" action="/report/common/scripts/change_email_notify.php">
														
														<?php if(isset($_SESSION['perms']['perm_rcv_service_requests'])){ ?> 
														<label>Service Request Notifications</label>
														<input name="pref_rcv_service_requests" id="pref_rcv_service_requests" type="checkbox" class="menuiButton" value="Y" <?php if(strtolower($_SESSION['prefs']['pref_rcv_service_requests']) == "y"){echo "checked";}   ?>>                                    
														<br />
														<?php } ?>
														
														<?php if(isset($_SESSION['perms']['perm_rcv_service_reports'])){ ?> 
														<label>Copy of Service Report</label>   
														<input name="pref_rcv_service_reports" id="pref_rcv_service_reports" type="checkbox" class="menuiButton" value="Y" <?php if(strtolower($_SESSION['prefs']['pref_rcv_service_reports']) == "y"){echo "checked";}   ?>>  
														<br />
														<?php } ?>
														
														<!--<input id="emailnotifySubmit" name="Submit" type="button" value="Save Email Settings" class="button" onclick="emailNotifySubmit(); return false;" />-->
														<a id="emailnotifySubmit" name="Submit" class="menu_button_jquery_save" onclick="emailNotifySubmit(); return false;" >Save Email Settings</a>                                 
														</form>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="grid_8"> <img class="imageMedium" src="/resources/images/management.jpg"  />
									<h2 class="level3">Support</h2>
									<p class="intro"> Choose a support item from the menu on the left.</p>
								</div>
							</div>
						</div>
					</li>
<!-- Help menu item -->
					<li class="hztlNavItem hztlNavParent"> <a class="hztlNavElem level2"><span class="hztlNavText"><span>Help</span></span></a>
						<div class="megaNav noGridSpacing">
							<div class="megaNavInner cf">
								<div class="grid_4">
									<div class="vertNavOuter">
										<ul class="vertNavList">
<!--											<li class="vertNavItem vertNavParent"> <a class="vertNavElem"><span>How-To Videos</span></a>
												<div class="grid_8 flyoutNav">
													<h2 class="level3"> How-To Videos</h2>
													<br />
													<div class="twoColumnLists cf">
														<ul class="unstyledList">
														
															<li> <a class="ctaLink iframeYouTube" href="https://www.youtube.com/embed/ZNZdT5RzRVw?autoplay=1"> <span>First Time User</span> </a> </li>
															<li> <a class="ctaLink iframeYouTube" href="https://www.youtube.com/embed/xbBhR86b_pI?autoplay=1"> <span>Complete a service report</span> </a> </li>
														</ul>
													</div>
												</div>
											</li> 
-->											<li class="vertNavItem vertNavParent"> <a class="vertNavElem"><span>Show/Hide Help Pop-Ups</span></a>
											   <div class="grid_8 flyoutNav">
													<h2 class="level3"> Help Popups</h2>
													<br />
													<div style="width:250px">
														<form id="helpchange" name="helpchange" method="post" autocomplete="off" action="/report/common/scripts/change_help_pref.php">
														
														<label>Show Help Pop-Ups</label>
														<input name="help_pop" id="help_pop" class="menuiButton" type="checkbox" value="Y" <?php if(strtolower($_SESSION['prefs']['pref_show_help']) == "y"){echo "checked";}   ?>>                                    
														<br />
														<!--<input id="helpchangeSubmit" name="Submit" type="button" value="Change Help Settings" class="button" onclick="helpPrefSubmit(); return false;" />-->
														<a id="helpchangeSubmit" name="Submit" class="menu_button_jquery_save" onclick="helpPrefSubmit(); return false;" >Change Help Settings</a>                                 
														</form>
													</div>
												</div>
											</li>
											<li class="vertNavItem vertNavParent"> <a class="vertNavElem" target="_blank" href="<?php echo $settings->issue_url; ?>"><span>OiHealthcare Portal Ticketing System</span></a>
												<div class="grid_8 flyoutNav">
													<h2 class="level3"> OiHealthcare Portal Ticketing System</h2>
													<br />
													<div class="twoColumnLists cf">
														<ul class="unstyledList">
															<li> <a class="ctaLink" target="_blank" href="<?php echo $settings->issue_url; ?>"> <span>OiHealthcare Portal Ticketing System</span> </a> </li>
														</ul>
													</div>
												</div>
											</li> 
											<?php if(strtolower($_SESSION['perms']['perm_kb_access']) == "y"){ ?>
											<li class="vertNavItem vertNavParent"> <a class="vertNavElem" target="new" href="<?php echo $settings->kb_url; ?>"><span>Knowledge Base</span></a>
												<div class="grid_8 flyoutNav">
													<h2 class="level3"> Knowledge Base Access</h2>
													<br />
													<div class="twoColumnLists cf">
														<ul class="unstyledList">
															<li> <a class="ctaLink" target="new" href="<?php echo $settings->kb_url; ?>"> <span>Knowledge Base</span> </a> </li>
														</ul>
													</div>
												</div>
											</li> 
											<?php } ?>                                                
										</ul>
									</div>
								</div>
								<div class="grid_8"> <img class="imageMedium" src="/resources/images/help_menu.jpg"  />
									<h2 class="level3">Help</h2>
									<p class="intro"> Choose a help item from the menu on the left.</p>
								</div>
							</div>
						</div>
					</li>
<!-- Logout menu item -->					
					<li class="hztlNavItem hztlNavParent"> <a class="hztlNavElem level2" href="/report/logout.php"> <span class="hztlNavText"><span>Logout</span></span> </a>
						<div class="megaNav noGridSpacing">
							<div class="megaNavInner cf">
								<div class="concertinaNav">
									<div class="grid_4">
										<div class="concertinaNavItems concertinaNavTopLevel concertinaNavItemsActive" style="background-color:inherit"> 
										</div>
									</div>
									<div class="grid_8">
										<div class="sliderViewport">
											<div class="sliderPanel">
												<div class="sliderItem sliderItemDefault"> <a href="/report/logout.php"><img class="imageMedium" src="/resources/images/logout.jpg"  /></a>
													<h2 class="level3"> Portal Logout</h2>
													<p class="intro"> <?php echo $settings->company_name; ?> would like to thank you for using our web portal.</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</li>
<!-- End menu items -->					
					</nav>
                </div> 
            </div>
            <div class="section">
                <div class="grid_9">
                   <h1><a href="<?php echo $settings->full_url; ?>/report" style="text-decoration:none; color:#FFF"><?php echo $settings->company_name; ?> Portal</a></h1>
                    <p id="breadcrumb" class="breadcrumb">User: <?php echo $_SESSION['user_name']; ?></p>
                </div>
            </div>
        </div>
    </div>
</header>