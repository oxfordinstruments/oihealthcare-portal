<script type="text/javascript" language="javascript" src="/resources/js/jquery/jquery.min.js"></script>
<script type="text/javascript" language="JavaScript" src="/resources/js/jquery.qtip.min.js"></script>
<script type="text/javascript" language="javascript" src="/resources/fancybox/jquery.fancybox.pack.js"></script>
<script type="text/javascript" language="javascript" src="/resources/datatables/js/jquery.dataTables.js"></script>
<script type='text/javascript' language="javascript" src="/resources/js/jsmd5.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/jquery.animate-shadow.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/jquery.ui.widget.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/jquery.countdown.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/php.default.min.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/jquery.ddslick.min.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/iButton/lib/jquery.easing.1.3.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/iButton/lib/jquery.ibutton.min.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/iButton/lib/jquery.metadata.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/detect_timezone.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/jquery.detect_timezone.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/jquery.cookie.pack.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/moment.min.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/moment-timezone-with-data-2010-2020.min.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/impromptu/jquery-impromptu.min.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/chosen/chosen.jquery.js"></script>

<script type="text/javascript">
$(document).ready(function() { 
	var stayalive = 0;
	
	var tz_name = $(this).get_timezone();
	var tz_offset = $(this).get_offset();
	$.cookie('oihp_timezone', [tz_name, tz_offset], { path: '/', secure: false });
	
	$(".menuiButton:radio").iButton({ 
		resizeHandle: "auto",
		resizeContainer: "auto",
		duration: 250,
		easing: "swing"   
	});
	
	$(".menuiButton:checkbox").iButton({ 
		resizeHandle: "auto",
		resizeContainer: "auto",
		duration: 250,
		easing: "swing"   
	});
	 
	<?php 
	
		if($_SESSION['user_settings']=='pwdchg') {
			echo "alert(\"The password has been changed !\\n You will now be logged out so the\\n password change can take effect\");\n";
			$_SESSION['user_settings']='';
			echo "location.href='/report/logout.php'; \n";	
		}
		if($_SESSION['user_settings']=='match') {
			$_SESSION['user_settings']='';
			echo "alert(\"The password do not match !\");\n";	
		}
		if($_SESSION['user_settings']=='old') {
			$_SESSION['user_settings']='';
			echo "alert(\"The password old password is incorrect !\");\n";	
		}
		if($_SESSION['user_settings']=='blank') {
			$_SESSION['user_settings']='';
			echo "alert(\"The password cannot be blank !\");\n";	
		}
		if($_SESSION['user_settings']=='requirement') {
			$_SESSION['user_settings']='';
			echo "alert(\"The new password does not meet the minimum requirements !\");\n";	
		}
		if($_SESSION['user_settings']=='name') {
			$_SESSION['user_settings']='';
			echo "alert(\"The users displayed name has been changed\");\n";	
		}
		if($_SESSION['user_settings']=='emailchg') {
			$_SESSION['user_settings']='';
			echo "alert(\"The users email has been changed\");\n";	
		}
		if($_SESSION['user_settings']=='error') {
			$_SESSION['user_settings']='';
			echo "alert(\"There was an sql error submitting the form\");\n";	
		}
	 ?>

		$(".TypeMR, .TypeCT, .TypeSMR").hover(
			function() {
				if($(this).hasClass("TypeMR")){
					$("#CTMRSiteImage").attr("src","/resources/images/mr.jpg");
					$(".CTMRSiteInfo").html($("#SystemID" + $(this).attr("rel")).html());
				}else if($(this).hasClass("TypeCT")){
					$("#CTMRSiteImage").attr("src","/resources/images/ct.jpg");
					$(".CTMRSiteInfo").html($("#SystemID" + $(this).attr("rel")).html());
				}else if($(this).hasClass("TypeSMR")){
					$("#CTMRSiteImage").attr("src","/resources/images/smr.jpg");
					$(".CTMRSiteInfo").html($("#SystemID" + $(this).attr("rel")).html());
				}else if($(this).hasClass("TypeCTMR")){
					$("#CTMRSiteImage").attr("src","/resources/images/ct.jpg");
					$(".CTMRSiteInfo").html($("#SystemID" + $(this).attr("rel")).html());
				}
		});
/////////////////////////////////////////////////////////////////////////////////////
	var brwide = $(window).width();
	var closeBtn = true;
	var fitToView = true;
	var autoSize = true;
	var showFancyBox = true;
	
	if(brwide < 850){
		closeBtn = false;	
		fitToView = false;
		autoSize = false;
		showFancyBox = false;
	}

	if(showFancyBox){
		$(".iframeAllSites").fancybox({
				'type'			: 'iframe',
				'fitToView'		: true,
				'maxWidth'		: 900,
				'maxHeight'		: 600,
				'autoSize'		: true,
				'closeBtn'		: true,
				'margin'		: [5,5,5,5],
				'iframe'		: {'scrolling' : 'no'}
		});	
		
		$(".iframeAllUsers").fancybox({
				'type'			: 'iframe',
				'fitToView'		: true,
				'maxWidth'		: 900,
				'maxHeight'		: 600,
				'autoSize'		: true,
				'closeBtn'		: true,
				'margin'		: [5,5,5,5],
				'iframe'		: {'scrolling' : 'no'}
		});	
	
		$(".iframeNewRequest").fancybox({
				'type'			: 'iframe',
				'fitToView'		: true,
				'maxWidth'		: 900,
				'maxHeight'		: 600,
				'autoSize'		: true,
				'closeBtn'		: true,
				'margin'		: [5,5,5,5],
				'iframe'		: {'scrolling' : 'no'}
		});	
		
		$(".iframeYouTube").fancybox({
				'type'			: 'iframe',
				'fitToView'		: true,
				'autoSize'		: true,
				'closeBtn'		: true
		});			
		
		$(".iframeNote").fancybox({
				'type'			: 'iframe',
				'fitToView'		: true,
				'maxWidth'		: 900,
				'maxHeight'		: 600,
				'autoSize'		: true,
				'closeBtn'		: true,
				'margin'		: [5,5,5,5],
				'iframe'		: {'scrolling' : 'no'},
				'afterClose'	: function(){
					iframeNoteClosed();	
				}
		});	
		
		$(".iframeNoteView").fancybox({
				'type'			: 'iframe',
				'fitToView'		: true,
				'maxWidth'		: 900,
				'maxHeight'		: 600,
				'autoSize'		: true,
				'closeBtn'		: true,
				'margin'		: [5,5,5,5],
				'iframe'		: {'scrolling' : 'yes'}
		});		
	}
/////////////////////////////////////////////////////////////////////////////////////
	//$('#headerTop').prepend('<div id="defaultCountdown" style="width:"200px";></div>');
	$('.hztlNavList','#navBarTop').prepend('<ul class="iconList shareIcons"><li class="iconListItem"><div id="defaultCountdown"></div></li></ul>'); 
	$('#defaultCountdown').countdown({
		until: '<?php echo intval(($_SESSION['expire'] - $now)) ?>s', 
		format: 'HMS',
		layout: 'Session Timout <b>{hnn}{sep}{mnn}{sep}{snn}</b>',
		onTick: highlightLast5,
		onExpiry: expiredSession
	}); 
/////////////////////////////////////////////////////////////////////////////////////
	function highlightLast5(periods) { 
		if ($.countdown.periodsToSeconds(periods) == 300) { 
			$(this).addClass('highlight'); 
			alert("Your session will expire in 5 minutes!\nPlease save any work now!.");
		} 
		if(stayalive == 60){
			stayalive = 0;
			$.ajax({
				type: 'POST',
				url: '/report/common/stay_alive.php',
				data: { none: 'none' },
				success:function(data){
					// successful request; do something with the data
					//$("#emailSent").text("Emails Sent");
				}
			});
			console.log('staying alive');
		}
		stayalive++
	}

	function expiredSession(){
		alert("Your session has expired!\nAny unsaved work will be lost and you will be logged out.");
	}
	
	$(".menu_button_jquery_save").button({
		icons: {
			primary: "ui-icon-disk"
		}
	});

	<?php
	if(strtolower($_SESSION['prefs']['pref_show_help']) == 'y' and $_SESSION['mobile_device'] == false){
	?>
	console.log("Help On");
	$(".tooltip").each(function() {
		var id = $(this).attr('id');
		var re = new RegExp(/_lbl?/);
		var lbl_match = re.test(id);
		if (lbl_match) {
			$(this).after("<img style='display: inline-block; vertical-align: text-bottom; padding-left: 5px;' src=\"/resources/images/help/help16.png\" class='tooltip_img' id='tooltip_" + $(this).attr('id') + "'>");
		} else {
			if($(this).hasClass('chooser')) {
				$(this).after("<img style='display: inline-block; vertical-align: text-top; padding-left: 2px;' src=\"/resources/images/help/help16.png\" class='tooltip_img' id='tooltip_" + $(this).attr('id') + "'>");
			}else{
				$(this).css("width", "-=32px");
				$(this).wrap("<div> </div>");
				$(this).after("<img style='display: inline-block; vertical-align: text-top; padding-left: 2px;' src=\"/resources/images/help/help16.png\" class='tooltip_img' id='tooltip_" + $(this).attr('id') + "'>");
			}
		}
	});

	$(".tooltip_fix").each(function(){
		$(this).css("width","-=38px");
		$(this).wrap("<div> </div>");

	});

	$(".tooltip_img").qtip({
		show: {
			event: 'click',
			solo: true,
			effect: function(offset) {
				$(this).slideDown(100); // "this" refers to the tooltip
			}
		},
		position: {
			my: 'left center',  // Position my top left...
			at: 'right center'//, // at the bottom right of...
			//target: $(this) // my target
		},
		style: {
			classes: 'qtip-shadow qtip-rounded'
		},
		content: {
			text: function(event, api) {
				$.ajax({
					url: '/report/common/scripts/tool_tip.php',
					type: 'POST',
					dataType: 'json',
					data: {
						id: $(this).attr('id'),
						page: window.location.href.split('?')[0]
					}
				})
					.then(function(data) {

						// Now we set the content manually (required!)
						//console.log(data);
						api.set('content.text', data.tip);
					}, function(xhr, status, error) {
						// Upon failure... set the tooltip content to the status and error value
						api.set('content.text', status + ': ' + error);
					});

				return 'Loading...'; // Set some initial loading text
			}
		}
	});
	$(".chooser").chosen({
		no_results_text: "Oops, nothing found!",
		disable_search_threshold: 10,
		placeholder_text_single: '  ',
		width: '89%'
	});
	<?php }else{?>
	$(".chooser").chosen({
		no_results_text: "Oops, nothing found!",
		disable_search_threshold: 10,
		placeholder_text_single: '  ',
		width: '100%'
	});
	<?php } ?>

/////////////////////////////////////////////////////////////////////////////////////
}); <!-- end document ready -->
/////////////////////////////////////////////////////////////////////////////////////
function emailSubmit(){
	if (document.getElementById("email1").value != document.getElementById("email2").value){
		alert("New Email addresses do not match");
		document.getElementById("email1").value = "";
		document.getElementById("email2").value = "";
	}else{
		if(document.getElementById("email1").value == ""){
			 alert("Email Cannot be blank");
			 document.getElementById("email1").value = "";
	         document.getElementById("email2").value = "";
		}else{
			checkEmail(document.getElementById("email1").value);
			if(document.getElementById("emailcheck").value != ""){
				alert("Email not changed\n" + document.getElementById("emailcheck").value);
				document.getElementById("email1").value = "";
	    		document.getElementById("email2").value = "";
			}else{
				document.getElementById("emailchange").submit()
			}
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////
function checkEmail(str){
if (str=="")
  {
  //document.getElementById("txtUser").innerHTML=" ";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
		//var emailAdd = $(xmlhttp.responseText).text().replace(/ /g,'');
	  if(xmlhttp.responseText.replace(/ /g,'') == "2"){
		  //alert("The email address is an invalid format");
		  document.getElementById("emailcheck").value = "The email address is an invalid format";
	  }else{
		if(xmlhttp.responseText.replace(/ /g,'') != "0"){
			//alert("The email address already exsists for user(s) " + xmlhttp.responseText);
			document.getElementById("emailcheck").value = "The email address already exsists for user(s) " + xmlhttp.responseText;
		}else{
			document.getElementById("emailcheck").value = "";
		}
	  }
    }
  }
xmlhttp.open("GET","/report/common/scripts/check_email_edit.php?q="+str+"&x="+ document.getElementById("uid").value ,false);
xmlhttp.send();     	
	
}
/////////////////////////////////////////////////////////////////////////////////////
function helpPrefSubmit(str){
	// abort any pending request
    if (request) {
        request.abort();
    }
	var help_checked = 0;
	if(document.getElementById('help_pop').checked){
		help_checked = 1;
	}
    $("#helpchangeSubmit").prop("disabled", true);

	var request = $.ajax({
		url: "/report/common/scripts/change_help_pref.php",
		type: "POST",
		data: {help_pop:help_checked},
		dataType: "html",
		timeout: 3000
	});
	
	request.done(function( msg ) {
		if(msg.replace(/ /g,'') == "1"){
			alert("Profile Updated");
			document.location.reload();
		}else{
			alert("Profile Update Failed!!");
		}
		$("#helpchangeSubmit").prop("disabled", false);
	});
	
	request.fail(function( jqXHR, textStatus ) {
		alert( "Profile Update failed: " + textStatus );
		$("#helpchangeSubmit").prop("disabled", false);
	});	
}
/////////////////////////////////////////////////////////////////////////////////////
function emailNotifySubmit(){
	// abort any pending request
    if (request) {
        request.abort();
    }
    // setup some local variables
    var $form = $("#emailnotify");
    // let's select and cache all the fields
    var $inputs = $form.find("input");
    // serialize the data in the form
    var serializedData = $form.serialize();
    // let's disable the inputs for the duration of the ajax request
    $("#emailnotifySubmit").prop("disabled", true);

    // fire off the request	
	var request = $.ajax({
		url: "/report/common/scripts/change_email_notify.php",
		type: "POST",
		data: serializedData,
		dataType: "html",
		timeout: 3000
	});
	
	request.done(function( msg ) {
		if(msg.replace(/ /g,'') == "1"){
			alert("Email Notifications Changed");
			document.location.reload();
		}else{
			alert( "Email settings changed failed");
		}
    $("#emailnotifySubmit").prop("disabled", false);
	});
	
	request.fail(function( jqXHR, textStatus ) {
		alert( "Email settings changed failed: " + textStatus );
    $("#emailnotifySubmit").prop("disabled", false);
	});
}
/////////////////////////////////////////////////////////////////////////////////////
function isValidPhone(phonenumber){
	if (phonenumber != "" && phonenumber.length == 12) {
		var goodChars = "-1234567890"
		for (i = 0; i < phonenumber.length; i++){   
		    var c = phonenumber.charAt(i);
		    if (goodChars.indexOf(c) < 0) return false;
		}
		return true;
	} else {
		return false;
	}
}
/////////////////////////////////////////////////////////////////////////////////////
function cellInfoSubmit(str){
	if(isValidPhone(document.getElementById("menu_cell").value)){
		cellInfoSubmit(val);
	}else{
		alert("Please enter a valid cellphone number e.g. 123-123-1234");	
		return;
	}
	
	// abort any pending request
    if (request) {
        request.abort();
    }
    // setup some local variables
    var $form = $("#cellinfochange");
    // let's select and cache all the fields
    var $inputs = $form.find("input, select");
    // serialize the data in the form
    var serializedData = $form.serialize();
    // let's disable the inputs for the duration of the ajax request
    $("#cellinfochangeSubmit").prop("disabled", true);

    // fire off the request	
	var request = $.ajax({
		url: "/report/common/scripts/change_cell_info.php",
		type: "POST",
		data: serializedData,
		dataType: "html",
		timeout: 3000
	});
	
	request.done(function( msg ) {
		if(msg.replace(/ /g,'') == "1"){
			alert("Profile Updated");
			document.location.reload();
		}else{
			alert("Profile Update Failed!!");
		}
   		$("#cellinfochangeSubmit").prop("disabled", false);
	});
	
	request.fail(function( jqXHR, textStatus ) {
		alert( "Profile Update failed: " + textStatus );
   		$("#cellinfochangeSubmit").prop("disabled", false);
	});	
}
/////////////////////////////////////////////////////////////////////////////////////
function get_radio_value(name) {
	var inputs = document.getElementsByName(name);
	for (var i = 0; i < inputs.length; i++) {
	  if (inputs[i].checked) {
		return inputs[i].value;
	  }
	}
}
/////////////////////////////////////////////////////////////////////////////////////

</script>
