<div class="outerContainer" id="navBar">
    <div id="navBarTop" class="centerContainer container_12 withBottom">
		<div class="centerContainer grid_9" style="">
			<div id="links" style="color:#FFF; display:table;">
				
				<div style="display:table-cell; float:left; vertical-align:middle; text-align:center; padding-top:3px; padding-left:20px;">
					<span style="color:#d2d3d5; font-size:1.3em;"><a href="<?php echo $settings->contact_url;?>" style="color:#d2d3d5;" target="new">Sales &amp; Support<br>Toll Free 888-673-5151</a></span>
				</div>
				
				<div style="display:table-cell; float:left; vertical-align:middle; text-align:center; padding-top:12px; padding-left:80px;">
					<span style="font-size:1.3em;"><a href="<?php echo $settings->enquiry_url;?>" style="color:#d2d3d5;" target="new">Make an Enquiry</a></span>
				</div>
			
				<div style="display:table-cell; float:left; vertical-align:middle; text-align:center; padding-top:8px; padding-left:80px;">
					<span style=""><a href="<?php echo $settings->facebook_url;?>" target="new"><img src="/resources/images/icons/icon-facebook.gif" width="30" height="30"></a></span>
				</div>
				<div style="display:table-cell; float:left; vertical-align:middle; text-align:center; padding-top:8px; padding-left:20px;">
					<span style=""><a href="<?php echo $settings->twitter_url;?>" target="new"><img src="/resources/images/icons/icon-twitter.gif" width="30" height="30"></a></span>
				</div>
				<div style="display:table-cell; float:left; vertical-align:middle; text-align:center; padding-top:8px; padding-left:20px;">
					<span style=""><a href="<?php echo $settings->linkedin_url;?>" target="new"><img src="/resources/images/icons/icon-linkedin.gif" width="30" height="30"></a></span>
				</div>
				<div style="display:table-cell; float:left; vertical-align:middle; text-align:center; padding-top:8px; padding-left:20px;">
					<span style=""><a href="<?php echo $settings->youtube_url;?>" target="new"><img src="/resources/images/icons/icon-youtube.gif" width="30" height="30"></a></span>
				</div>
								
				<div id="clear" style="clear:both;"></div>
			</div>
		</div>
        <div class="grid_12">
            <ul class="hztlNavList">
                <ul class="iconList shareIcons">
					<li class="iconListItem">
						<div id="defaultCountdown"></div>
					</li>
				</ul>
            </ul>
        </div>
    </div>
    <div id="navBarBottom" class="centerContainer container_12">
        <div class="grid_12" style="line-height:4px;">&nbsp;</div>
    </div>
</div>

<div class="outerContainer" id="social">
	<div style="width:100%;">
		<div id="copyright" style="float:left; padding-top:0.5em; padding-left:20px;">
			<?php echo $settings->company_name; ?> Copyright <?php echo $settings->copyright_date; ?>, All Rights Reserved
		</div>
		<div id="comodo" style="float:right; padding-right:20px;">
			<img src="/resources/images/comodo_secure_76x26_transp.png" width="76" height="26">
		</div>
		<div id="clear" style="clear:both;"></div>
	</div>
</div>

</body>
</html>