<div id="CMSHeaderDiv"> 
    <!-- leave blank --> 
</div>
<div id="tiplayer" style="position:absolute; visibility:hidden; z-index:10000;"></div>
<header class="outerContainer" id="header">
    <div class="innerContainer" id="headerBottom">
        <div class="centerContainer container_12">
		<div class="section">
                <div class="grid_9">
				&nbsp;
                </div>
            </div>       
            <div class="section withPadding">
                <div class="grid_3"> <a class="image" target="new" id="headerLogo" href="http://www.oxford-instruments.com/businesses/service/ct-mr"><img src="/resources/images/common/oxford-instruments-logo.png" /></a> </div>
                <div class="grid_9">
					<nav id="mainNav" class="cf">
					<ul class="hztlNavList">
					<li class="hztlNavItem hztlNavParent"> <a class="hztlNavElem level2" href="/report/customer/"> <span class="hztlNavText"><span>Dashboard</span></span> </a>
						<div class="megaNav noGridSpacing">
							<div class="megaNavInner cf">
								<div class="concertinaNav">
									<div class="grid_4">
										<div class="concertinaNavItems concertinaNavTopLevel concertinaNavItemsActive" style="background-color:inherit"> 
											<!--<ul class="vertNavList" data-nav-type="{ &#39;rootNode&#39;: 257, &#39;src&#39;: &#39;mn&#39; }">
												<li class="vertNavItem"><a class="vertNavElem" href=""><span class="TypeMR">Change Your Password</span></a></li>
												<li class="vertNavItem"><a class="vertNavElem" href=""><span class="TypeCT">Change Your Email Address</span></a></li>
												<li class="vertNavItem"><a class="vertNavElem" href=""><span class="TypeCT">Edit Your Signature</span></a></li>                            
											  </ul>--> 
										</div>
									</div>
									<div class="grid_8">
										<div class="sliderViewport">
											<div class="sliderPanel">
												<div class="sliderItem sliderItemDefault"> <a href="/report/customer/"><img src="/resources/images/global-home.jpg" width="451" height="200" class="imageMedium"  /></a>
													<h2 class="level3"> Customer Portal Dashboard</h2>
													<p class="intro"> Click here to go the main customer portal dashboard</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li class="hztlNavItem hztlNavParent"> <a class="hztlNavElem level2"> <span class="hztlNavText"><span>User Settings</span></span> </a>
						<div class="megaNav noGridSpacing">
							<div class="megaNavInner cf">
								<div class="grid_4">
									<div class="vertNavOuter">
										<ul class="vertNavList">
											<li class="vertNavItem vertNavParent"> <a class="vertNavElem"><span>Change Password</span></a>
												<div class="grid_8 flyoutNav">                            	
													<h2 class="level3"> Change Your Password</h2>
													<br />
													<div>
														<div style="width:200px; float:left">
															<form id="pwdchange" name="pwdchange" method="post" autocomplete="off" action="/report/common/scripts/change_pwd.php">                                    
															<label>New Password<span class="small" id="txtSite" style='color:#FF0000'> (Required) </span></label>
															<input type="password" name="password1" id="password1" />
															<br />
															<label>New Password Again<span class="small" id="txtSite" style='color:#FF0000'> (Required) </span></label>
															<input type="password" name="password2" id="password2" />
															<br />
															<br />
															<!--<input name="Submit" type="button" value="Change Password" class="button" onclick="document.forms['pwdchange'].submit(); return false;" />-->
															<a id="pwdchangeSubmit" name="Submit" class="menu_button_jquery_save" onclick="document.forms['pwdchange'].submit(); return false;" >Change Password</a>
															</form>
														</div>
														<div style="width:200px; float:left">
															Rules:
															
															<ul style="list-style:outside; list-style-type:disc; padding-left:10px;">
																<li>Atleast 6 characters</li>
																<li>CaSe SenSitIve</li>
																<li>Must contain letters AND numbers</li>
																<li>Different than previous 3 passwords</li>
															</ul>
															<br>
															<br>
														</div>
													</div>
													</div>
											 </li>
											<li class="vertNavItem vertNavParent"> <a class="vertNavElem"><span>Change Email</span></a>
												<div class="grid_8 flyoutNav">
													<h2 class="level3"> Change Your Email</h2>
													<br />
													<div style="width:200px">
														<form id="emailchange" name="emailchange" method="post" autocomplete="off" action="/report/common/scripts/change_email.php">                                    
														<label>Old Email<span class="small" id="txtSite" style='color:#FF0000'> (Required) </span></label>
														<input type="text" name="email0" id="email0" value="<?php echo $_SESSION['userdata']['email']; ?>" readonly />
														<br />
														<br />
														<label>New Email<span class="small" id="txtSite" style='color:#FF0000'> (Required) </span></label>
														<input type="text" name="email1" id="email1" />
														<br />
														<label>New Email Again<span class="small" id="txtSite" style='color:#FF0000'> (Required) </span></label>
														<input type="text" name="email2" id="email2" />
														<br />
														<br />
														<!--<input name="Submit" type="button" value="Change Email" class="button" onclick="emailSubmit(); return false;" />-->
														<a id="emailchangeSubmit" name="Submit" class="menu_button_jquery_save" onclick="emailSubmit(); return false;">Change Email</a>                                   
														
														<input type="hidden" name="uid" id="uid" value="<?php echo $_SESSION['login'];  ?>" />
														<input type="hidden" name="emailcheck" id="emailcheck" value="" />                                    
					
														</form>
													</div>
												</div>
											</li>
										   <li class="vertNavItem vertNavParent"> <a class="vertNavElem"><span>Show/Hide Help Pop-Ups</span></a>
											   <div class="grid_8 flyoutNav">
													<h2 class="level3"> Help Popups</h2>
													<br />
													<div style="width:250px">
														<form id="helpchange" name="helpchange" method="post" autocomplete="off" action="/report/common/scripts/change_help_pref.php">
														
														<label>Show Help Pop-Ups</label>
														<input name="help_pop" class="menuiButton" type="hidden" value="N">
														<input name="help_pop" class="menuiButton" type="checkbox" value="Y" <?php if(strtolower($_SESSION['pref']['pref_show_help']) == "y"){echo "checked";}   ?>>                                    
														
											
																						  
														<br />
														<br />
														<!--<input id="helpchangeSubmit" name="Submit" type="button" value="Change Help Settings" class="button" onclick="helpPrefSubmit(); return false;" />-->
														<a id="helpchangeSubmit" name="Submit" class="menu_button_jquery_save" onclick="helpPrefSubmit(); return false;" >Change Help Settings</a>                                 
														</form>
													</div>
												</div>
											</li>
											<li class="vertNavItem vertNavParent"> <a class="vertNavElem"><span>Email Notifications</span></a>
											   <div class="grid_8 flyoutNav">
													<h2 class="level3"> Email Notifications</h2>
													<br />
													<div style="width:100%">
														<form id="emailnotify" name="emailnotify" method="post" autocomplete="off" action="/report/common/scripts/change_email_notify.php">
														
														<?php if(isset($_SESSION['perms']['perm_rcv_service_requests'])){ ?> 
														<label> Service Request Notifications</label>
														<input name="rcv_request" id="rcv_request" class="menuiButton" type="checkbox" value="Y" <?php if(strtolower($_SESSION['pref']['pref_rcv_service_requests']) == "y"){echo "checked";}   ?>>                                    
														<br>
														<?php } ?>

														<?php if(isset($_SESSION['perms']['perm_rcv_service_reports'])){ ?> 
														<label>Copy of Service Report</label>
														<input name="rcv_report" id="rcv_report" class="menuiButton" type="checkbox" value="Y" <?php if(strtolower($_SESSION['pref']['pref_rcv_service_reports']) == "y"){echo "checked";}   ?>>  
														<br>
														<?php } ?> 
														
														<!--<input id="emailnotifySubmit" name="Submit" type="button" value="Save Email Settings" class="button" onclick="emailNotifySubmit(); return false;" />-->
														<a id="emailnotifySubmit" name="Submit" class="menu_button_jquery_save" onclick="emailNotifySubmit(); return false;" >Save Email Settings</a>                                  
														</form>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="grid_8"> <img class="imageMedium" src="/resources/images/UserSettings.jpg"  />
									<h2 class="level3">Management</h2>
									<p class="intro"> Choose a user profile item from the menu on the left.</p>
								</div>
							</div>
						</div>
					</li>
					<!--<li class="hztlNavItem hztlNavParent"> <a class="hztlNavElem level2" href="#"> <span class="hztlNavText"><span>Management</span></span> </a>
						<div class="megaNav noGridSpacing">
							<div class="megaNavInner cf">
								<div class="grid_4">
									<div class="vertNavOuter">
										<ul class="vertNavList">
											<li class="vertNavItem vertNavParent"> <a class="vertNavElem" href="#"><span>Management Dashboard</span></a>
												<div class="grid_8 flyoutNav">
													<h2 class="level3"> Management Dashboard</h2>
													<p style="text-align: justify;"> Access the management dashboard</p>
												</div>
											</li>
											<li class="vertNavItem vertNavParent"> <a class="vertNavElem"><span>Reports</span></a>
												<div class="grid_8 flyoutNav">
													<h2 class="level3"> Reports</h2>
													<br />
													<div class="twoColumnLists cf">
														<ul class="unstyledList">
															<li> <a class="ctaLink" href=""> <span>Report 1</span> </a> </li>
															<li> <a class="ctaLink" href=""> <span>Report 2</span> </a> </li>
															<li> <a class="ctaLink" href=""> <span>Report 3</span> </a> </li>
														</ul>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="grid_8"> <img class="imageMedium" src="/resources/images/management.jpg"  />
									<h2 class="level3">Management</h2>
									<p class="intro"> Choose a management item from the menu on the left.</p>
								</div>
							</div>
						</div>
					</li>-->
					<li class="hztlNavItem hztlNavParent"> <a class="hztlNavElem level2" href="/report/logout.php"> <span class="hztlNavText"><span>Logout</span></span> </a>
						<div class="megaNav noGridSpacing">
							<div class="megaNavInner cf">
								<div class="concertinaNav">
									<div class="grid_4">
										<div class="concertinaNavItems concertinaNavTopLevel concertinaNavItemsActive" style="background-color:inherit"> 
											<!--<ul class="vertNavList" data-nav-type="{ &#39;rootNode&#39;: 257, &#39;src&#39;: &#39;mn&#39; }">
												<li class="vertNavItem"><a class="vertNavElem" href=""><span class="TypeMR">Change Your Password</span></a></li>
												<li class="vertNavItem"><a class="vertNavElem" href=""><span class="TypeCT">Change Your Email Address</span></a></li>
												<li class="vertNavItem"><a class="vertNavElem" href=""><span class="TypeCT">Edit Your Signature</span></a></li>                            
											  </ul>--> 
										</div>
									</div>
									<div class="grid_8">
										<div class="sliderViewport">
											<div class="sliderPanel">
												<div class="sliderItem sliderItemDefault"> <a href="/report/logout.php"><img class="imageMedium" src="/resources/images/logout.jpg"  /></a>
													<h2 class="level3"> Portal Logout</h2>
													<p class="intro"> <?php echo $settings->company_name; ?> would like to thank you for using our web portal.</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</li>
					</nav>
                </div> 
            </div>
             <div class="section">
                <div class="grid_9">
                    <h1><?php echo $settings->company_name; ?> Portal</h1>
                    <p id="breadcrumb" class="breadcrumb"></p>
                </div>
            </div>
        </div>
    </div>
</header>