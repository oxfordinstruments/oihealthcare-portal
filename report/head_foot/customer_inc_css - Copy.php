<link href="/resources/css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" media="screen" />

<link href="/resources/images/favicon.ico" type="image/x-icon" rel="shortcut icon"/>
<link href="/resources/images/favicon.ico" type="image/x-icon" rel="icon"/>

<link href="/resources/css/main_customer_960.css" rel="stylesheet" type="text/css" media="screen" />
<link href="/resources/css/main_customer_720.css" rel="stylesheet" type="text/css" media="screen" />
<link href="/resources/css/main_customer_320.css" rel="stylesheet" type="text/css" media="screen" />

<link href="/resources/css/screen.min.css" rel="stylesheet" type="text/css" media="screen" />
<link href="/resources/css/320.min.css" rel="stylesheet" type="text/css" />
<link href="/resources/css/720.min.css" rel="stylesheet" type="text/css" />
<link href="/resources/css/960.min.css" rel="stylesheet" type="text/css" />
<link href="/resources/css/print.min.css" rel="stylesheet" type="text/css" media="print" />
<!--[if lt IE 9 ]>
        <link href="/resources/css/screen-ie.min.css" rel="stylesheet" type="text/css" />
    <![endif]-->
<!--[if IE 9 ]>
        <link href="/resources/css/screen-ie9.min.css" rel="stylesheet" media="screen" />
    <![endif]-->
	
<link rel="stylesheet" type="text/css" href="/resources/css/jquery.countdown.css">
<link href="/resources/datatables/css/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
<link href="/resources/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css" media="screen" />
<link href="/resources/js/iButton/css/jquery.ibutton.min.css" rel="stylesheet" type="text/css">