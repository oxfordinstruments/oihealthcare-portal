<script type="text/javascript" language="javascript" src="/resources/js/jquery/jquery.min.js"></script>
<script type="text/javascript" language="JavaScript" src="/resources/js/jquery.qtip.min.js"></script>
<script type="text/javascript" language="javascript" src="/resources/fancybox/jquery.fancybox.pack.js"></script>
<script type="text/javascript" language="javascript" src="/resources/datatables/js/jquery.dataTables.js"></script>
<script type='text/javascript' language="javascript" src="/resources/js/jsmd5.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/jquery.animate-shadow.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/jquery.ui.widget.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/php.default.min.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/jquery.ddslick.min.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/jquery.countdown.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/iButton/lib/jquery.easing.1.3.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/iButton/lib/jquery.ibutton.min.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/iButton/lib/jquery.metadata.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/detect_timezone.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/jquery.detect_timezone.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/jquery.cookie.pack.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/moment.min.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/moment-timezone-with-data-2010-2020.min.js"></script>

<script type="text/javascript">
	
	<?php 
		if($_SESSION['user_settings']=='pwdchg') {
			echo "alert(\"The password has been changed !\\n You will now be logged out so the\\n password change can take effect\");\n";
			$_SESSION['user_settings']='';
			echo "location.href='/report/logout.php'; \n";	
		}
		if($_SESSION['user_settings']=='match') {
			$_SESSION['user_settings']='';
			echo "alert(\"The passwords do not match !\");\n";	
		}
		if($_SESSION['user_settings']=='old') {
			$_SESSION['user_settings']='';
			echo "alert(\"The old password is incorrect !\");\n";	
		}
		if($_SESSION['user_settings']=='blank') {
			$_SESSION['user_settings']='';
			echo "alert(\"The password cannot be blank !\");\n";	
		}
		if($_SESSION['user_settings']=='requirement') {
			$_SESSION['user_settings']='';
			echo "alert(\"The new password does not meet the minimum requirements !\\n \\n -Atleast 6 characters \\n -CaSe SenSitIve \\n -Must contain letters AND numbers \\n -Different than previous 3 passwords \");\n";	
		}
		if($_SESSION['user_settings']=='name') {
			$_SESSION['user_settings']='';
			echo "alert(\"The users displayed name has been changed\");\n";	
		}
		if($_SESSION['user_settings']=='emailchg') {
			$_SESSION['user_settings']='';
			echo "alert(\"The users email has been changed\");\n";	
		}
		if($_SESSION['user_settings']=='error') {
			$_SESSION['user_settings']='';
			echo "alert(\"There was an sql error submitting the form\");\n";	
		}
	 ?>

$(document).ready(function() { 
	var stayalive = 0;
	
	var tz_name = $(this).get_timezone();
	var tz_offset = $(this).get_offset();
	$.cookie('oihp_timezone', [tz_name, tz_offset], { path: '/', secure: false });
	
/////////////////////////////////////////////////////////////////////////////////////
	$(".menuiButton:radio").iButton({ 
		resizeHandle: "auto",
		resizeContainer: "auto",
		duration: 250,
		easing: "swing"   
	});
	
	$(".menuiButton:checkbox").iButton({ 
		resizeHandle: "auto",
		resizeContainer: "auto",
		duration: 250,
		easing: "swing"   
	});

		//$('#headerTop').prepend('<div id="defaultCountdown" style="width:"200px";></div>');
		//$('.hztlNavList','#navBarTop').prepend('<ul class="iconList shareIcons"><li class="iconListItem"><div id="defaultCountdown"></div></li></ul>'); 
		$('#defaultCountdown').countdown({
			until: '<?php echo intval(($_SESSION['expire'] - $now)) ?>s', 
			format: 'HMS',
			layout: 'Session Timout <b>{hnn}{sep}{mnn}{sep}{snn}</b>',
			onTick: highlightLast5,
			onExpiry: expiredSession
		}); 
		function highlightLast5(periods) { 
			if ($.countdown.periodsToSeconds(periods) == 300) { 
				$(this).addClass('highlight'); 
				alert("Your session will expire in 5 minutes!\nPlease logout and log back in.");
			} 
			if(stayalive == 60){
				stayalive = 0;
				$.ajax({
					type: 'POST',
					url: '/report/common/stay_alive.php',
					data: { none: 'none' },
					success:function(data){
						// successful request; do something with the data
						//$("#emailSent").text("Emails Sent");
					}
				});
				console.log('staying alive');
			}
			stayalive++
		}
		function expiredSession(){
			alert("Your session has expired!\nAny unsaved work will be lost and you will be logged out.");
		}
		
/////////////////////////////////////////////////////////////////////////////////////
	var brwide = $(window).width();
	var closeBtn = true;
	var fitToView = true;
	var autoSize = true;
	var showFancyBox = true;
	
	if(brwide < 850){
		closeBtn = false;	
		fitToView = false;
		autoSize = false;
		showFancyBox = false;
	}

	if(showFancyBox){
		$(".iframeAllReports").fancybox({
				'type'			: 'iframe',
				'fitToView'		: true,
				'maxWidth'		: 900,
				'maxHeight'		: 600,
				'autoSize'		: true,
				'closeBtn'		: true,
				'margin'		: [5,5,5,5],
				'iframe'		: {'scrolling' : 'no'}
		});	
		
		$(".iframeFiles").fancybox({
				'type'			: 'iframe',
				'fitToView'		: true,
				'maxWidth'		: 900,
				'maxHeight'		: 600,
				'autoSize'		: true,
				'closeBtn'		: true,
				'margin'		: [5,5,5,5],
				'iframe'		: {'scrolling' : 'no'}
		});	
	}
	
	$(".menu_button_jquery_save").button({
		icons: {
			primary: "ui-icon-disk"
		}
	});

	<?php
	if(strtolower($_SESSION['prefs']['pref_show_help']) == 'y' and $_SESSION['mobile_device'] == false){
	?>
	$(".tooltip").each(function() {
		var id = $(this).attr('id');
		var re = new RegExp(/_lbl?/);
		var lbl_match = re.test(id);
		if (lbl_match) {
			$(this).after("<img style='display: inline-block; vertical-align: text-bottom; padding-left: 5px;' src=\"/resources/images/help/help16.png\" class='tooltip_img' id='tooltip_" + $(this).attr('id') + "'>");
		} else {
			if($(this).hasClass('chooser')) {
				$(this).after("<img style='display: inline-block; vertical-align: text-top; padding-left: 2px;' src=\"/resources/images/help/help16.png\" class='tooltip_img' id='tooltip_" + $(this).attr('id') + "'>");
			}else{
				$(this).css("width", "-=32px");
				$(this).wrap("<div> </div>");
				$(this).after("<img style='display: inline-block; vertical-align: text-top; padding-left: 2px;' src=\"/resources/images/help/help16.png\" class='tooltip_img' id='tooltip_" + $(this).attr('id') + "'>");
			}
		}
	});

	$(".tooltip_fix").each(function(){
		$(this).css("width","-=38px");
		$(this).wrap("<div> </div>");

	});

	$(".tooltip_img").qtip({
		show: {
			event: 'click',
			solo: true,
			effect: function(offset) {
				$(this).slideDown(100); // "this" refers to the tooltip
			}
		},
		position: {
			my: 'left center',  // Position my top left...
			at: 'right center'//, // at the bottom right of...
			//target: $(this) // my target
		},
		style: {
			classes: 'qtip-shadow qtip-rounded'
		},
		content: {
			text: function(event, api) {
				$.ajax({
					url: '/report/common/scripts/tool_tip.php',
					type: 'POST',
					dataType: 'json',
					data: {
						id: $(this).attr('id'),
						page: window.location.href.split('?')[0]
					}
				})
					.then(function(data) {

						// Now we set the content manually (required!)
						//console.log(data);
						api.set('content.text', data.tip);
					}, function(xhr, status, error) {
						// Upon failure... set the tooltip content to the status and error value
						api.set('content.text', status + ': ' + error);
					});

				return 'Loading...'; // Set some initial loading text
			}
		}
	});
	/*todo-evo Chosen Not Needed for customer yet */
//	$(".chooser").chosen({
//		no_results_text: "Oops, nothing found!",
//		disable_search_threshold: 10,
//		placeholder_text_single: '  ',
//		width: '89%'
//	});
	<?php }else{?>
//	$(".chooser").chosen({
//		no_results_text: "Oops, nothing found!",
//		disable_search_threshold: 10,
//		placeholder_text_single: '  ',
//		width: '100%'
//	});
	<?php } ?>


/////////////////////////////////////////////////////////////////////////////////////
}); <!-- end document ready -->
/////////////////////////////////////////////////////////////////////////////////////
function emailSubmit(){
	if (document.getElementById("email1").value != document.getElementById("email2").value){
		alert("New Email addresses do not match");
		document.getElementById("email1").value = "";
		document.getElementById("email2").value = "";
	}else{
		if(document.getElementById("email1").value == ""){
			 alert("Email Cannot be blank");
			 document.getElementById("email1").value = "";
	         document.getElementById("email2").value = "";
		}else{
			checkEmail(document.getElementById("email1").value);
			if(document.getElementById("emailcheck").value != ""){
				alert("Email not changed\n" + document.getElementById("emailcheck").value);
				document.getElementById("email1").value = "";
	    		document.getElementById("email2").value = "";
			}else{
				document.getElementById("emailchange").submit()
			}
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////
function checkEmail(str){
	if (str==""){
		//document.getElementById("txtUser").innerHTML=" ";
		return;
	}
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}else{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			//var emailAdd = $(xmlhttp.responseText).text().replace(/ /g,'');
			if(xmlhttp.responseText.replace(/ /g,'') == "2"){
				//alert("The email address is an invalid format");
				document.getElementById("emailcheck").value = "The email address is an invalid format";
			}else{
				if(xmlhttp.responseText.replace(/ /g,'') != "0"){
					//alert("The email address already exsists for user(s) " + xmlhttp.responseText);
					document.getElementById("emailcheck").value = "The email address already exsists for user(s) " + xmlhttp.responseText;
				}else{
					document.getElementById("emailcheck").value = "";
				}
			}
		}
	}
	xmlhttp.open("GET","/report/common/scripts/check_email_edit.php?q="+str+"&x="+ document.getElementById("uid").value ,false);
	xmlhttp.send();
}
/////////////////////////////////////////////////////////////////////////////////////
function emailNotifySubmit(){
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}else{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			if(xmlhttp.responseText.replace(/ /g,'') == "1"){
				alert("User Profile Updated");
			}else{
				alert("ERROR");
			}
		}
	}
	var rcv_report;
	var rcv_request;
	if(document.getElementById("rcv_report").checked){rcv_report = "Y";}else{rcv_report = "N";}
	if(document.getElementById("rcv_request").checked){rcv_request = "Y";}else{rcv_request = "N";}
	xmlhttp.open("GET","/report/common/scripts/change_email_notify.php?rp="+ rcv_report +"&rq="+ rcv_request ,false);
	xmlhttp.send(); 
	document.location.reload();
}
/////////////////////////////////////////////////////////////////////////////////////
function helpPrefPreSubmit(){
	val = get_radio_value("help_pop");
	helpPrefSubmit(val);
}
/////////////////////////////////////////////////////////////////////////////////////
function helpPrefSubmit(str){
	if (str==""){
		//document.getElementById("txtUser").innerHTML=" ";
		return;
	}
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}else{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			if(xmlhttp.responseText.replace(/ /g,'') == "1"){
				alert("User Profile Updated");
			}else{
				location.href="/error.php?n=1047&p=menu_customer_include.php";
			}
		}
	}
	xmlhttp.open("GET","/report/common/scripts/change_help_pref.php?q="+str+"&x="+ document.getElementById("uid").value ,false);
	xmlhttp.send(); 
	document.location.reload();
}
/////////////////////////////////////////////////////////////////////////////////////
function get_radio_value(name) {
	var inputs = document.getElementsByName(name);
	for (var i = 0; i < inputs.length; i++) {
	  if (inputs[i].checked) {
		return inputs[i].value;
	  }
	}
}
/////////////////////////////////////////////////////////////////////////////////////

</script>