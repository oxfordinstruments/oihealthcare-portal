
<div class="outerContainer" id="social">
	<img src="/resources/images/comodo_secure_76x26_transp.png" width="76" height="26">
	<div style="padding-left:15px; font-weight:bold">
		<?php
		$ver = file_get_contents($settings->php_base_dir.'/'.$settings->ver_file);
		if($ver != false and trim($ver) != ""){
			echo "Portal Version ",$ver;
		}
		?>
	</div>
</div>
<footer id="footer" class="outerContainer">
	<div class="centerContainer container_12">
		<h2 class="nonvisual">Footer</h2>
		<div class="section">
			<div class="grid_12">
				<p id="copyright">&copy; Copyright <?php echo $settings->copyright_date; ?></p>
				<nav id="footerNav">
					<ul class="separatedList">
						<li class="separatedListItem">
							<a href="https://oxford-instruments.com/copyright-statement?src=fn" id="ctl00_ctl00_ucFooter_ucFooterNavigation_rptList_ctl01_lnkElement">Copyright Statement</a>
						</li>
						<li class="separatedListItem">
							<a href="https://oxford-instruments.com/corporate-and-social-responsibility?src=fn" id="ctl00_ctl00_ucFooter_ucFooterNavigation_rptList_ctl02_lnkElement">
								CSR</a>
						</li>
						<li class="separatedListItem">
							<a href="https://oxford-instruments.com/modern-slavery?src=fn" id="ctl00_ctl00_ucFooter_ucFooterNavigation_rptList_ctl03_lnkElement">
								Modern Slavery</a>
						</li>
						<li class="separatedListItem">
							<a href="https://oxford-instruments.com/regulatory-information?src=fn" id="ctl00_ctl00_ucFooter_ucFooterNavigation_rptList_ctl04_lnkElement">Regulatory Information</a>
						</li>
						<li class="separatedListItem">
							<a href="https://oxford-instruments.com/terms-and-conditions?src=fn" id="ctl00_ctl00_ucFooter_ucFooterNavigation_rptList_ctl05_lnkElement">
								Terms &amp; Conditions</a>
						</li>
						<li class="separatedListItem">
							<a href="https://oxford-instruments.com/privacy-statement?src=fn" id="ctl00_ctl00_ucFooter_ucFooterNavigation_rptList_ctl06_lnkElement">
								Privacy</a>
						</li>
						<li class="separatedListItem">
							<a href="https://oxford-instruments.com/disclaimer?src=fn" id="ctl00_ctl00_ucFooter_ucFooterNavigation_rptList_ctl07_lnkElement">Disclaimer</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</footer>
</body>
</html>