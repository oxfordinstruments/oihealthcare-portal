<?php if($menuResultUpdates->num_rows > 0){ ?>
<div style="width: 100%; text-align: center; color: black; font-size: 1.5em; padding-top: 15px; padding-bottom: 15px; background: #ff0000; margin-bottom: 15px;">STOP! There are scripts that need run.</div>
<?php } ?>
<div id="CMSHeaderDiv">
    <!-- leave blank --> 
</div>
<div id="tiplayer" style="position:absolute; visibility:hidden; z-index:10000;"></div>
<header class="outerContainer" id="header">
    <div class="innerContainer" id="headerBottom">
        <div class="centerContainer container_12">
            <div class="section withPadding">
                <div class="grid_3"> <a class="image" target="new" id="headerLogo" href="http://www.oxford-instruments.com/businesses/service/ct-mr"><img src="/resources/images/common/oxford-instruments-logo.png" /></a> </div>
                <div class="grid_9">
                    <nav id="mainNav" class="cf">
						<ul class="hztlNavList">
<!-- Home menu item -->
						<li class="hztlNavItem hztlNavParent"> <a class="hztlNavElem level2" href="<?php echo $_SESSION['loc']; ?>/"> <span class="hztlNavText"><span>Dashboard</span></span> </a>
							<div class="megaNav noGridSpacing">
								<div class="megaNavInner cf">
									<div class="concertinaNav">
										<div class="grid_4">
											<div class="concertinaNavItems concertinaNavTopLevel concertinaNavItemsActive" style="background-color:inherit"> 
												<!--<ul class="vertNavList" data-nav-type="{ &#39;rootNode&#39;: 257, &#39;src&#39;: &#39;mn&#39; }">
													<li class="vertNavItem"><a class="vertNavElem" href=""><span class="TypeMR">Change Your Password</span></a></li>
													<li class="vertNavItem"><a class="vertNavElem" href=""><span class="TypeCT">Change Your Email Address</span></a></li>
													<li class="vertNavItem"><a class="vertNavElem" href=""><span class="TypeCT">Edit Your Signature</span></a></li>                            
												  </ul>--> 
											</div>
										</div>
										<div class="grid_8">
											<div class="sliderViewport">
												<div class="sliderPanel">
													<div class="sliderItem sliderItemDefault"> <a href="<?php echo $_SESSION['loc']; ?>/"><img src="/resources/images/global-home.jpg" width="455" height="200" class="imageMedium"  /></a>
														<h2 class="level3"> Portal Dashboard</h2>
														<p class="intro"> Click here to go the main service portal dashboard</p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</li>
<!-- Systems menu item -->
						<li class="hztlNavItem hztlNavParent"> <a class="hztlNavElem level2 iframeAllSites" href="/report/common/all_systems.php?q=1" target="_parent" ><span class="hztlNavText"><span>Systems</span></span> </a>
							<div class="megaNav noGridSpacing">
								<div class="megaNavInner cf">
									<div class="concertinaNav">
										<div class="grid_4">
											<div class="concertinaNavItems concertinaNavTopLevel concertinaNavItemsActive" style="background-color:inherit"> 
											</div>
										</div>
										<div class="grid_8">
											<div class="sliderViewport">
												<div class="sliderPanel">
													<div class="sliderItem sliderItemDefault"> <a class="iframeAllSites" href="/report/common/all_systems.php?q=1" target="new"><img class="imageMedium" id="CTMRSiteImage" src="/resources/images/mr.jpg"  /></a>
														<h2 class="level3"> Oxford Instruments Healthcare System List</h2>
														<p class="intro"> Click to open the system select list.</p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</li>						
<!-- Calendar menu item -->
						<li class="hztlNavItem hztlNavParent"> <a class="hztlNavElem level2" href="/report/common/calendar/calendar.php" target="new" ><span class="hztlNavText"><span>Calendar</span></span> </a>
							<div class="megaNav noGridSpacing">
								<div class="megaNavInner cf">
									<div class="concertinaNav">
										<div class="grid_4">
											<div class="concertinaNavItems concertinaNavTopLevel concertinaNavItemsActive" style="background-color:inherit"> 
											</div>
										</div>
										<div class="grid_8">
											<div class="sliderViewport">
												<div class="sliderPanel">
													<div class="sliderItem sliderItemDefault"> <a href="/report/common/calendar/calendar.php" target="new"><img class="imageMedium" id="CTMRSiteImage" src="/resources/images/calendar.jpg"  /></a>
														<h2 class="level3"> Oxford Instruments Healthcare Calendar</h2>
														<p class="intro"> Opens the calendar in a new window.</p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</li>
<!-- Support menu item -->
						<li class="hztlNavItem hztlNavParent"> <a class="hztlNavElem level2"><span class="hztlNavText"><span>Support</span></span></a>
							<div class="megaNav noGridSpacing">
								<div class="megaNavInner cf">
									<div class="grid_4">
										<div class="vertNavOuter">
											<ul class="vertNavList">
												<li class="vertNavItem vertNavParent"> <a class="vertNavElem"><span>Change Password</span></a>
													<div class="grid_8 flyoutNav">                            	
														<h2 class="level3"> Change Your Password</h2>
														<br />
														<div>
															<div style="width:200px; float:left">
																<form id="pwdchange" name="pwdchange" method="post" autocomplete="off" action="/report/common/scripts/change_pwd.php">                                    
																<label>New Password<span class="small" id="txtSite" style='color:#FF0000'> (Required) </span></label>
																<input type="password" name="password1" id="password1" />
																<br />
																<label>New Password Again<span class="small" id="txtSite" style='color:#FF0000'> (Required) </span></label>
																<input type="password" name="password2" id="password2" />
																<br />
																<br />
																<!--<input id="pwdchangeSubmit" name="Submit" type="button" value="Change Password" class="button" onclick="document.forms['pwdchange'].submit(); return false;" />-->
															<a id="pwdchangeSubmit" name="Submit" class="menu_button_jquery_save" onclick="document.forms['pwdchange'].submit(); return false;" >Change Password</a>
																</form>
															</div>
															<div style="width:200px; float:left">
																Rules:
																<br />
																Atleast 6 characters
																<br />
																CaSe SenSitIve
																<br />
																Must contain letters AND numbers
																<br />
																<br />
															</div>
														</div>
														</div>
												 </li>
												<li class="vertNavItem vertNavParent"> <a class="vertNavElem"><span>Change Email</span></a>
													<div class="grid_8 flyoutNav">
														<h2 class="level3"> Change Your Email</h2>
														<br />
														<div style="width:200px">
															<form id="emailchange" name="emailchange" method="post" autocomplete="off" action="/report/common/scripts/change_email.php">                                    
															<label>Old Email<span class="small" id="txtSite" style='color:#FF0000'> (Required) </span></label>
															<input type="text" name="email0" id="email0" value="<?php echo $_SESSION['userdata']['email']; ?>" readonly />
															<br />
															<br />
															<label>New Email<span class="small" id="txtSite" style='color:#FF0000'> (Required) </span></label>
															<input type="text" name="email1" id="email1" />
															<br />
															<label>New Email Again<span class="small" id="txtSite" style='color:#FF0000'> (Required) </span></label>
															<input type="text" name="email2" id="email2" />
															<br />
															<br />
															<!--<input id="emailchangeSubmit" name="Submit" type="button" value="Change Email" class="button" onclick="emailSubmit(); return false;" />-->
														<a id="emailchangeSubmit" name="Submit" class="menu_button_jquery_save" onclick="emailSubmit(); return false;">Change Email</a>                                   
															<input type="hidden" name="emailcheck" id="emailcheck" value="" />                                    
						
															</form>
														</div>
													</div>
												</li>
												<li class="vertNavItem vertNavParent"> <a class="vertNavElem"><span>Cell Phone & SMS Alerts</span></a>
												   <div class="grid_8 flyoutNav">
														<h2 class="level3"> Change Your Cell Number, Carrier, and SMS Alerts</h2>
														<br />
														<div style="width:200px">
                                                            <form id="cellinfochange" name="cellinfochange" method="post" autocomplete="off" action="/report/common/scripts/change_cell_info.php">
                                                            <label>Receive SMS Alerts</label>
                                                            <input name="menu_get_sms" type="hidden" value="N">
                                                            <input name="menu_get_sms" type="checkbox" class="menuiButton" value="Y" <?php if(strtolower($_SESSION['prefs']['pref_rcv_sms']) == "y"){echo "checked";}   ?>>                                    
                                                            
                                                            <br />
                                                            <label>Cell Number  123-123-1234</label>
                                                            <input name="menu_cell" id="menu_cell" type="text" value="<?php echo $_SESSION['userdata']['cell']; ?>" <?php if(strtolower($_SESSION['perms']['perm_change_phone'])== "n"){echo " disabled";} ?> />
                                                            <br />
                                                            <label class="red">Cell Carrier</label>
                                                            <select id="menu_carrier" name="menu_carrier" <?php  if(strtolower($_SESSION['perms']['perm_change_phone'])== "n"){echo " disabled";} ?>>
                                                            <option value="" title=""></option>
                                                                <?php
                                                                while($menuRowC = $menuResultCarrier->fetch_assoc())
                                                                  {
                                                                    echo "<option value='".$menuRowC['carrierid']."' title='".$menuRowC['carrierid']."' ";
                                                                    if($_SESSION['userdata']['carrier']==$menuRowC['carrierid']){echo "selected='selected'";}
                                                                    echo ">".$menuRowC['company']." - ".$menuRowC['address']."</option>\n";
                                                                  }
                                                                ?>
                                                            </select>
                                                            <br />
                                                            <br />
                                                            <!--<input id="cellinfochangeSubmit" name="Submit" type="button" value="Change Cell Info" class="button" onclick="cellInfoSubmit(); return false;" /> -->
                                                            <a id="cellinfochangeSubmit" name="Submit" class="menu_button_jquery_save" onclick="cellInfoSubmit(); return false;" >Change Cell Info</a>                                
                                                            </form>
                                                        </div>
													</div>
												</li>
												 <li class="vertNavItem vertNavParent"> <a class="vertNavElem" href="/report/common/email_notifications.php"><span>Email Notifications</span></a>
												   <div class="grid_8 flyoutNav">
														<h2 class="level3"> Email Notifications</h2>
														<br />
													   <div class="twoColumnLists cf">
														   <ul class="unstyledList">
															   <li> <a class="ctaLink" id="all_email_prefs" href="/report/common/email_notifications.php"> <span> Change Email Notifications</span> </a> </li>
														   </ul>
													   </div>
													</div>
												</li>
												<li class="vertNavItem vertNavParent"> <a class="vertNavElem" href="/report/common/reports.php"><span>Reports</span></a>
													<div class="grid_8 flyoutNav">
														<h2 class="level3"> Reports</h2>
														<br />
														<div class="twoColumnLists cf">
															<ul class="unstyledList">
																<li> <a class="ctaLink" id="eng_assignments_rpt" href="/report/common/reports/eng_assignments.php"> <span>Engineer Assignments (Excel)</span> </a> </li>
																<li> <a class="ctaLink" id="engineer_systems_rpt" href="/report/common/reports/engineer_systems.php"> <span>Engineer's System IDs (Excel)</span> </a> </li>
																<li> <a class="ctaLink" id="systems_map_rpt" href="/report/common/reports/systems_map.php" target="new"> <span>Systems Map</span> </a> </li><br>
																<br>
																<li> <a class="ctaLink" id="all_reports" href="/report/common/reports.php"> <span>All Reports</span> </a> </li>
															</ul>
														</div>
													</div>
												</li>
												<?php if(isset($_SESSION['perms']['perm_mri_readings_menus'])){ ?>
												<li class="vertNavItem vertNavParent"> <a class="vertNavElem"><span>MRI Readings</span></a>
													<div class="grid_8 flyoutNav">
														<h2 class="level3"> MRI Readings</h2>
														<br />
														<div class="twoColumnLists cf">
															<ul class="unstyledList">
																<li> <a class="ctaLink iframeAllSites" href="/report/common/all_systems.php?mri&q=3"> <span>Input Reading (Single System)</span> </a> </li>
																<li> <a class="ctaLink" href="/report/common/mri_readings_input.php"> <span>Input Readings For All Systems</span> </a> </li>
																<br />
																Reports
																<li> <a class="ctaLink" href="/report/common/reports/mri_readings_week.php"> <span>Current Week Report</span> </a> </li>
																<li> <a class="ctaLink iframeAllSites" href="/report/common/all_systems.php?mri&q=4"> <span>System History Report</span> </a> </li>
															</ul>
														</div>
													</div>
												</li> 
												<?php } ?>  
												<?php if(isset($_SESSION['perms']['perm_nps_menus'])){ ?>   
												<li class="vertNavItem vertNavParent"> <a class="vertNavElem"><span>Net Promoter Score</span></a>
													<div class="grid_8 flyoutNav">
														<h2 class="level3"> Net Promoter Score</h2>
														<br />
														<div class="twoColumnLists cf">
															<ul class="unstyledList">
																<li> <a class="ctaLink" href="/report/common/nps_setup.php"> <span>NPS Setup</span> </a> </li>
															</ul>
														</div>
													</div>
												</li> 
												<?php } ?>
												<?php if(isset($_SESSION['perms']['perm_submit_parcarfbc']) or isset($_SESSION['perms']['perm_close_parcarfbc']) or isset($_SESSION['perms']['perm_view_parcarfbc'])){ ?>
												<li class="vertNavItem vertNavParent"> <a class="vertNavElem"><span>Quality Control</span></a>
													<div class="grid_8 flyoutNav">
														<h2 class="level3"> Quality Control</h2>
														<br />
														<div class="twoColumnLists cf">
															<ul class="unstyledList">
																<?php if(isset($_SESSION['perms']['perm_view_parcarfbc'])){ ?>
																<li> <a class="ctaLink iframeAllSites" href="/report/common/all_iso_car.php"> <span>Corrective Action Request View</span> </a> </li>
																<li> <a class="ctaLink iframeAllSites" href="/report/common/all_iso_par.php"> <span>Prevenative Action Request View</span> </a> </li>
																<li> <a class="ctaLink iframeAllSites" href="/report/common/all_iso_fbc.php"> <span>Feedback Complaint View</span> </a> </li>
																<br>
																<?php } ?>
																<?php if(isset($_SESSION['perms']['perm_submit_parcarfbc'])){ ?>
																<li> <a class="ctaLink" href="/report/common/iso_car.php"> <span>Corrective Action Request Submit</span> </a> </li>
																<li> <a class="ctaLink" href="/report/common/iso_par.php"> <span>Prevenative Action Request Submit</span> </a> </li>
																<li> <a class="ctaLink" href="/report/common/iso_fbc.php"> <span>Feedback Complaint Submit</span> </a> </li>
																<?php } ?>
															</ul>
														</div>
													</div>
												</li>	
												<?php } ?>
												<?php if(isset($_SESSION['perms']['perm_maintenance_menus'])){ ?>    
												<li class="vertNavItem vertNavParent"> <a class="vertNavElem"><span>Maintenance</span></a>
													<div class="grid_8 flyoutNav">
														<h2 class="level3">Maintenance</h2>
														<br />
														<div class="twoColumnLists cf">
															<ul class="unstyledList" style="float:left;">
																<?php if(isset($_SESSION['perms']['perm_edit_users'])){ ?>
																<li> <a class="ctaLink iframeAllUsers" href="/report/common/all_users.php?q=0"> <span>Manage Users</span> </a> </li>
																<li> <a class="ctaLink iframeAllUsers" href="/report/common/all_registrations.php"> <span>Registration Requests</span> </a> </li>
																<?php } ?>
																<br>
																<?php if(isset($_SESSION['perms']['perm_tool_cal_edit']) or isset($_SESSION['perms']['perm_tool_cal_view'])){ ?>
																<li> <a class="ctaLink iframeAllUsers" href="/report/common/all_tools.php"> <span>Manage Tools</span> </a> </li>
																<?php } ?>
															</ul>
															
															<ul class="unstyledList" style="float:left;">
																<?php if(isset($_SESSION['perms']['perm_edit_systems'])){ ?>
																<li> <a class="ctaLink iframeAllSites" href="/report/common/all_systems.php?q=2&all"> <span>Manage All Systems</span> </a> </li>
																<br>
																<?php } ?>
																<?php if(isset($_SESSION['perms']['perm_edit_facilities'])){ ?>
																<li> <a class="ctaLink iframeAllSites" href="/report/common/all_facilities.php"> <span>Manage Facilities</span> </a> </li>																
																<li> <a class="ctaLink iframeAllSites" href="/report/common/all_facilities.php?archived"> <span>Manage Archived Facilities</span> </a> </li>
																<br>
																<?php } ?>
																<?php if(isset($_SESSION['perms']['perm_edit_customers'])){ ?>
																<li> <a class="ctaLink iframeAllSites" href="/report/common/all_customers.php"> <span>Manage Customers</span> </a> </li>																
																<li> <a class="ctaLink iframeAllSites" href="/report/common/all_customers.php?archived"> <span>Manage Archived Customers</span> </a> </li>
																<br>
																<?php } ?>
																<?php if(isset($_SESSION['perms']['perm_edit_trailers'])){ ?>
																<li> <a class="ctaLink iframeAllSites" href="/report/common/all_trailers.php"> <span>Manage Trailers</span> </a> </li>																
																<br>
																<?php } ?>
															</ul>
														</div>
													</div>
												</li> 
												<?php } ?>	
											</ul>
										</div>
									</div>
									<div class="grid_8"> <img class="imageMedium" src="/resources/images/management.jpg"  />
										<h2 class="level3">Support</h2>
										<p class="intro"> Choose a support item from the menu on the left.</p>
									</div>
								</div>
							</div>
						</li>
<!-- Help menu item -->
						<li class="hztlNavItem hztlNavParent"> <a class="hztlNavElem level2"><span class="hztlNavText"><span>Help</span></span></a>
							<div class="megaNav noGridSpacing">
								<div class="megaNavInner cf">
									<div class="grid_4">
										<div class="vertNavOuter">
											<ul class="vertNavList">
												<li class="vertNavItem vertNavParent"> <a class="vertNavElem" href="/report/common/docs/" target="_blank"><span>Documentation</span></a>
													<div class="grid_8 flyoutNav">
														<h2 class="level3"> Documentation</h2>
														<br />
														<div class="twoColumnLists cf">
															<ul class="unstyledList">
																<li> <a class="ctaLink" target="_blank" href="/report/common/docs/"> <span>OIHP Docs</span> </a> </li>
																<li> <a class="ctaLink" target="_blank" href="/resources/rpi-docs/RPI Documentation 6-2-2015.pdf"> <span>Remote Diagnostics Computer</span> </a> </li>
															</ul>
														</div>
													</div>
												</li>
<!--												<li class="vertNavItem vertNavParent"> <a class="vertNavElem"><span>How-To Videos</span></a>
													<div class="grid_8 flyoutNav">
														<h2 class="level3"> How-To Videos</h2>
														<br />
														<div class="twoColumnLists cf">
															<ul class="unstyledList">
															
																<li> <a class="ctaLink iframeYouTube" href="https://www.youtube.com/embed/ZNZdT5RzRVw?rel=0&autoplay=1"> <span>First Time User</span> </a> </li>
																<li> <a class="ctaLink iframeYouTube" href="https://www.youtube.com/embed/xbBhR86b_pI?autoplay=1"> <span>Complete a service report</span> </a> </li>
															</ul>
														</div>
													</div>
												</li>
-->												<li class="vertNavItem vertNavParent"> <a class="vertNavElem"><span>Show/Hide Help Pop-Ups</span></a>
												   <div class="grid_8 flyoutNav">
														<h2 class="level3"> Help Popups</h2>
														<br />
														<div style="width:250px">
                                                            <form id="helpchange" name="helpchange" method="post" autocomplete="off" action="/report/common/scripts/change_help_pref.php">
                                                            
                                                            <label>Show Help Pop-Ups</label>
                                                            <input name="help_pop" id="help_pop" class="menuiButton" type="checkbox" value="Y" <?php if(strtolower($_SESSION['prefs']['pref_show_help']) == "y"){echo "checked";}   ?>>                                    
                                                            <br />
                                                            <!--<input id="helpchangeSubmit" name="Submit" type="button" value="Change Help Settings" class="button" onclick="helpPrefSubmit(); return false;" />-->
                                                            <a id="helpchangeSubmit" name="Submit" class="menu_button_jquery_save" onclick="helpPrefSubmit(); return false;" >Change Help Settings</a>                                 
                                                            </form>
                                                        </div>
													</div>
												</li>
												<li class="vertNavItem vertNavParent"> <a class="vertNavElem" target="_blank" href="<?php echo $settings->issue_url; ?>"><span>OiHealthcare Portal Ticketing System</span></a>
													<div class="grid_8 flyoutNav">
														<h2 class="level3"> OiHealthcare Portal Ticketing System</h2>
														<br />
														<div class="twoColumnLists cf">
															<ul class="unstyledList">
																<li> <a class="ctaLink" target="_blank" href="<?php echo $settings->issue_url; ?>"> <span>OiHealthcare Portal Ticketing System</span> </a> </li>
															</ul>
														</div>
													</div>
												</li> 
												<?php if(strtolower($_SESSION['perms']['perm_kb_access']) == "y"){ ?>
												<li class="vertNavItem vertNavParent"> <a class="vertNavElem" target="_blank" href="<?php echo $settings->kb_url; ?>"><span>OiHealthcare Knowledge Base</span></a>
													<div class="grid_8 flyoutNav">
														<h2 class="level3"> OiHealthcare Knowledge Base Access</h2>
														<br />
														<div class="twoColumnLists cf">
															<ul class="unstyledList">
																<li> <a class="ctaLink" target="_blank" href="<?php echo $settings->kb_url; ?>"> <span>OiHealthcare Knowledge Base</span> </a> </li>
															</ul>
														</div>
													</div>
												</li> 
												<?php } ?>    
												<?php if(strtolower($_SESSION['perms']['perm_dms_access']) == "y"){ ?>
												<li class="vertNavItem vertNavParent"> <a class="vertNavElem" target="_blank" href="<?php echo $settings->dms_url; ?>"><span>OiHealthcare DMS</span></a>
													<div class="grid_8 flyoutNav">
														<h2 class="level3"> OiHealthcare DMS Access</h2>
														<br />
														<div class="twoColumnLists cf">
															<ul class="unstyledList">
																<li> <a class="ctaLink" target="_blank" href="<?php echo $settings->dms_url; ?>"> <span>OiHealthcare DMS</span> </a> </li>
															</ul>
														</div>
													</div>
												</li> 
												<?php } ?>                                        
												<li class="vertNavItem vertNavParent"> <a class="vertNavElem" target="new" href="/report/admin/changelog_view.php"><span>OiHealthcare Portal Change Log</span></a>
													<div class="grid_8 flyoutNav">
														<h2 class="level3"> OiHealthcare Portal Change Log</h2>
														<br />
														<div class="twoColumnLists cf">
															<ul class="unstyledList">
																<li> <a class="ctaLink" target="new" href="/report/admin/changelog_view.php"> <span>OiHealthcare Portal Change Log</span> </a> </li>
															</ul>
														</div>
													</div>
												</li>
												<li class="vertNavItem vertNavParent"> <a class="vertNavElem" target="new" href="/report/common/scripts/org_chart/org_chart.php"><span>OiHealthcare Organization Chart</span></a>
													<div class="grid_8 flyoutNav">
														<h2 class="level3"> OiHealthcare Organization Chart  </h2>
														<br />
														<div class="twoColumnLists cf">
															<ul class="unstyledList">
																<li> <a class="ctaLink" target="new" href="/report/common/scripts/org_chart/org_chart.php"> <span>OiHealthcare Organization Chart</span> </a> </li>
															</ul>
														</div>
													</div>
												</li> 
											</ul>
										</div>
									</div>
									<div class="grid_8"> <img class="imageMedium" src="/resources/images/help_menu.jpg"  />
										<h2 class="level3">Help</h2>
										<p class="intro"> Choose a help item from the menu on the left.</p>
									</div>
								</div>
							</div>
						</li>
<!-- Logout menu item -->
						<li class="hztlNavItem hztlNavParent"> <a class="hztlNavElem level2" href="/report/logout.php"> <span class="hztlNavText"><span>Logout</span></span> </a>
							<div class="megaNav noGridSpacing">
								<div class="megaNavInner cf">
									<div class="concertinaNav">
										<div class="grid_4">
											<div class="concertinaNavItems concertinaNavTopLevel concertinaNavItemsActive" style="background-color:inherit"> 
												<!--<ul class="vertNavList" data-nav-type="{ &#39;rootNode&#39;: 257, &#39;src&#39;: &#39;mn&#39; }">
													<li class="vertNavItem"><a class="vertNavElem" href=""><span class="TypeMR">Change Your Password</span></a></li>
													<li class="vertNavItem"><a class="vertNavElem" href=""><span class="TypeCT">Change Your Email Address</span></a></li>
													<li class="vertNavItem"><a class="vertNavElem" href=""><span class="TypeCT">Edit Your Signature</span></a></li>                            
												  </ul>--> 
											</div>
										</div>
										<div class="grid_8">
											<div class="sliderViewport">
												<div class="sliderPanel">
													<div class="sliderItem sliderItemDefault"> <a href="/report/logout.php"><img class="imageMedium" src="/resources/images/logout.jpg"  /></a>
														<h2 class="level3"> Portal Logout</h2>
														<p class="intro"> Oxford Instruments Healthcare would like to thank you for using our web portal.</p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</li>
<!-- End menu items -->
						</nav>
                </div> 
            </div>
            <div class="section">
                <div class="grid_8">
                    <h1><a href="<?php echo $settings->full_url; ?>" style="text-decoration:none; color:#FFF"><?php echo $settings->company_name; ?> Portal</a></h1>
                    <p id="breadcrumb" class="breadcrumb">User: <?php echo $_SESSION['user_name']; ?></p>
                </div>
				<div class=".container_12 .grid_3 role_change_div" style="float:right;">
					<div id="rolesDropdown"></div>
				</div>
            </div>
			
        </div>
    </div>
</header>