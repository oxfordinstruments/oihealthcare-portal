<?php
//Update Completed 12/12/14
$no_redirect = true;
$use_default_includes = true;
$remove_session = true;
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');
?>
<!DOCTYPE html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<?php require_once($js_include);?>

</head>
<body>
<?php require_once($no_login_header); ?>
<div id="OIReportContent">
    <table width="100%" border="0" cellspacing="3" cellpadding="3">
        <tr>
            <td><h1>
                    <p align="center">Your username is inactive</p>
                    <p align="center">Please contact your administrator</p>
                </h1></td>
        </tr>
    </table>
</div>
<?php require_once($footer_include); ?>