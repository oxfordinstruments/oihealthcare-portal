<?php
$at_login_page = true;
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_auto_login.php');
$sal = new sessionAutoLogin($mysqli);

if($sal->auto_login_check() != false){
	header("location:".$_SESSION['loc']);	
}

?>

<!DOCTYPE html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<?php require_once($js_include);?>

<script src="/resources/js/jquery.badBrowser.js"></script>

<script type="text/javascript">
$(document).ready(function() {  	
	$("#myusername").focus();
		
	$("form").bind("keypress", function(e) {
		if (e.keyCode == 13) {
			if ($("#myusername").is(":focus")){
				$("#mypassword").focus();
			}else{
				submitcheck();
	            return false; // ignore default event
			}
	   }
	});
	
	

 });

function highlight(){
	document.getElementById("submitbtn").focus();
}
function submitcheck(){
	//$key = "platinum";
	document.getElementById("uname").value = base64_encode(document.getElementById("username").value);
	document.getElementById("pword").value = base64_encode(document.getElementById("password").value);	
	var parentuname=document.getElementById("username").parentNode;
	var childuname=document.getElementById("username");
	parentuname.removeChild(childuname);
	var parentpwd=document.getElementById("password").parentNode;
	var childpwd=document.getElementById("password");
	parentpwd.removeChild(childpwd);
	document.forms["form"].submit();
}


</script>

</head>
<body onLoad="highlight()">
<?php require_once($header_include); ?>
<div class="outerContainer" id="content">
    <div style="width:400px; margin-left:auto; margin-right:auto; margin-top:20px; margin-bottom:60px; background-color:#D1D3D4; padding-left:10px; padding-right:10px">
        <form  id="form" method="post" action="check_login.php">
            <div style="text-align:center; margin-bottom:10px; padding-top:10px">
                <h1 style="color:#F79447">Portal Login</h1>
            </div>
            <div style="width:25%; float:left; padding-left:5px">Username</div>
            <div style="margin-bottom:10px;">
                <input name="username" autocorrect="off" autocapitalize="none" type="text" class="textField"  id="username" style="width:70%;">
            </div>
            <div style="width:25%; float:left; padding-left:5px">Password</div>
            <div style="margin-bottom:10px">
                <input name="password" type="password" class="textField"  id="password" style="width:70%;">
            </div>
            <div style="text-align:center; padding-bottom:10px">
                <input name="Submit" id="submitbtn" type="button" value="Login" class="button" onclick="submitcheck()" />
            </div>
			<div style="display:none">
            	<input type="hidden" id="uname" name="uname" value="none" />
            	<input type="hidden" id="pword" name="pword" value="none" />
				<?php if(isset($_GET['email_request'])){
						echo "<input type=\"checkbox\" id=\"email_request\" name=\"email_request\" checked value=\"".$_GET['email_request']."\" />";
				}?>
				<?php if(isset($_GET['email_face'])){
						echo "<input type=\"checkbox\" id=\"email_face\" name=\"email_face\" checked value=\"".$_GET['email_face']."\" />";
				}?>
			</div>
        </form>
    </div>
    <div style="width:100%; margin-top:20px; text-align:center">
    	<h3 style="margin-bottom:0px; padding-bottom:15px;">Have you forgotten or lost your login information?</h3>
    	<h2><a href="recover_login.php" target="_self">Recover login information</a></h2>
    </div>
    <div style="width:100%; margin-top:20px; text-align:center">
    	<h3 style="padding-bottom:15px;">Are you a member of the Oi Service Team?</h3>
        <h2 style="padding-bottom:15px; margin-bottom:0px"><a href="http://oihealthcareportal.com/kb/" target="new">Access the OiHealthcare Knowledge Base</a></h2>
		<h2><a href="http://dms.oihealthcareportal.com" target="new">Access the OiHealthcare DMS</a></h2>
    </div>
	<div style="width:100%; margin-top:20px; margin-bottom:60px">
		<div style="margin-left:auto; margin-right:auto; width:50%; text-align:left; display:none">
		<?php echo $settings->company_name; ?> Service Portal is a property of <?php echo $settings->company_name; ?>.  This system should be accessed by authorized <?php echo $settings->company_name; ?> personnel and customers only.  All information on this computer system may be intercepted, recorded, read, copied, and disclosed by and to authorized personnel for official purposes, including criminal investigations.  Unauthorized access or use of this computer system may subject violators to criminal, civil, and/or administrative action.
		</div>
		<div style="margin-left:auto; margin-right:auto; width:50%; text-align:left;">
		This system should be accessed by authorized <?php echo $settings->company_name; ?> personnel and customers only. Unauthorized access or use of this computer system may subject violators to criminal, civil, and/or administrative action.
		</div>
	</div>
</div>
<?php require_once($footer_include); ?>