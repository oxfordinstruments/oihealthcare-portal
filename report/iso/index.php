<?php


require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

$comp_word_limt 	 = intval($settings->complaint_word_limit);
$serv_word_limt		 = intval($settings->service_word_limit);
$custact_word_limt	 = intval($settings->custact_word_limit);
$probrpt_word_limt	 = intval($settings->probrpt_word_limit);

$myusername = $_SESSION["login"];

function array_push_assoc(&$array, $key, $value){
	$array[$key] = $value;
	return $array;
}

function limit_words($string, $word_limit)
{
	$words = explode(" ",$string);
	if(count($words) > $word_limit){
		return implode(" ",array_splice($words,0,$word_limit))."...(more)";
	}else{
		return implode(" ",array_splice($words,0,$word_limit));
	}
}

?>
<!DOCTYPE html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<?php require_once($js_include);?>
<script src="/resources/js/jquery-ui/jquery-ui.min.js"></script>
<script src="/resources/js/jquery.ui.widget.js"></script>

<script type="text/javascript">
	function start(){
		//getCookies();	
	}
/////////////////////////////////////////////////////////////////////////////////////	
	function getCookie(c_name)
	{
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++)
		{
			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
			x=x.replace(/^\s+|\s+$/g,"");
			if (x==c_name)
			{
			return unescape(y);
			}
		}
	}
/////////////////////////////////////////////////////////////////////////////////////	
$(document).ready(function() {	

	$(".button_jquery_new").button({
		icons: {
			primary: "ui-icon-document"
		}
	});
	
	openCar = $('#openCARTable').dataTable({
		"bJQueryUI": true,
		//"bStateSave": true,
		"iCookieDuration": 60*60*24*365, // 1 year
		"sPaginationType": "full_numbers",
		"aaSorting": [ [0,'asc'] ],
		"fnInitComplete": function () {
			 this.$('tr').click( function () {
				 var href = $(this).find("a").attr("href");
				 if(href) {
					window.location = href;
				 }
			 });
		 }
	});
	
	closedCar = $('#closedCARTable').dataTable({
		"bJQueryUI": true,
		//"bStateSave": true,
		"iCookieDuration": 60*60*24*365, // 1 year
		"sPaginationType": "full_numbers",
		"aaSorting": [ [0,'asc'] ],
		"fnInitComplete": function () {
			 this.$('tr').click( function () {
				 var href = $(this).find("a").attr("href");
				 if(href) {
					window.location = href;
				 }
			 });
		 }
	});	
	
	openPar = $('#openPARTable').dataTable({
		"bJQueryUI": true,
		//"bStateSave": true,
		"iCookieDuration": 60*60*24*365, // 1 year
		"sPaginationType": "full_numbers",
		"aaSorting": [ [0,'asc'] ],
		"fnInitComplete": function () {
			 this.$('tr').click( function () {
				 var href = $(this).find("a").attr("href");
				 if(href) {
					window.location = href;
				 }
			 });
		 }
	});
	
	closedPar = $('#closedPARTable').dataTable({
		"bJQueryUI": true,
		//"bStateSave": true,
		"iCookieDuration": 60*60*24*365, // 1 year
		"sPaginationType": "full_numbers",
		"aaSorting": [ [0,'asc'] ],
		"fnInitComplete": function () {
			 this.$('tr').click( function () {
				 var href = $(this).find("a").attr("href");
				 if(href) {
					window.location = href;
				 }
			 });
		 }
	});	
	
	openFbc = $('#openFBCTable').dataTable({
		"bJQueryUI": true,
		//"bStateSave": true,
		"iCookieDuration": 60*60*24*365, // 1 year
		"sPaginationType": "full_numbers",
		"aaSorting": [ [0,'asc'] ],
		"fnInitComplete": function () {
			 this.$('tr').click( function () {
				 var href = $(this).find("a").attr("href");
				 if(href) {
					window.location = href;
				 }
			 });
		 }
	});
	
	closedFbc = $('#closedFBCTable').dataTable({
		"bJQueryUI": true,
		//"bStateSave": true,
		"iCookieDuration": 60*60*24*365, // 1 year
		"sPaginationType": "full_numbers",
		"aaSorting": [ [0,'asc'] ],
		"fnInitComplete": function () {
			 this.$('tr').click( function () {
				 var href = $(this).find("a").attr("href");
				 if(href) {
					window.location = href;
				 }
			 });
		 }
	});
	
	reportBadFbc = $('#reportBadFBCTable').dataTable({
		"bJQueryUI": true,
		//"bStateSave": true,
		"iCookieDuration": 60*60*24*365, // 1 year
		"sPaginationType": "full_numbers",
		"aaSorting": [ [0,'asc'] ],
		"fnInitComplete": function () {
			 this.$('tr').click( function () {
				 var href = $(this).find("a").attr("href");
				 if(href) {
					window.location = href;
				 }
			 });
		 }
	});
	
	requestBadFbc = $('#requestBadFBCTable').dataTable({
		"bJQueryUI": true,
		//"bStateSave": true,
		"iCookieDuration": 60*60*24*365, // 1 year
		"sPaginationType": "full_numbers",
		"aaSorting": [ [0,'asc'] ],
		"fnInitComplete": function () {
			 this.$('tr').click( function () {
				 var href = $(this).find("a").attr("href");
				 if(href) {
					window.location = href;
				 }
			 });
		 }
	});	

/////////////////////////////////////////////////////////////////////////////////////	
		<?php
		if(strtolower($_SESSION['prefs']['pref_show_help']) != 'y' or $_SESSION['mobile_device'] == true){
		?>
		$('#tiplayer').hide()
		<?php
		}
		?>

	$('#tabs_div').tabs({
	});
});
/////////////////////////////////////////////////////////////////////////////////////
</script>
</head>
<body onLoad="start()">
<?php require_once($header_include); ?>

<div id="OIReportContent">
	<div id="tabs_div">
		<ul>
			<li id="li-tab-1"><a href="#tab-1">Corrective Action</a></li>
			<li id="li-tab-2"><a href="#tab-2">Preventive Action</a></li>
			<li id="li-tab-3"><a href="#tab-3">Feedback Complaint</a></li>
		</ul>
		
		<div id="tab-1" class="tab">
			<div id="ReportOuterFrame">
				<?php  
					$sql="SELECT c.id, c.title, c.department, c.location, c.created_date, u.name AS created_name, c.unique_id
					FROM iso_car AS c
					LEFT JOIN users AS u ON u.uid = c.created_uid
					WHERE c.closed = 'n' AND c.deleted = 'n'
					ORDER BY c.created_date ASC;";
					if(!$resultOpenCAR = $mysqli->query($sql)){
						$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
						$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
						$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
					}
				?>  
				<div id="ReportFrame">     
				<div id="ReportFrameTitle">Open CARs</div>
				<div id="openReprotsDiv" <?php if($resultOpenCAR->num_rows == 0){echo "style=\"display:none\"";} ?>>
					<table width="100%" id="openCARTable" >
						<thead>
							<tr>
								<th>ID</th>
								<th>Title</th>
								<th>Department</th>
								<th>Location</th>
								<th>Created Date</th>
								<th>Creating User</th>                   
							</tr>
						</thead>
						<tbody>
							<?php
							if($resultOpenCAR){
								while($rowOpenCAR = $resultOpenCAR->fetch_assoc())
								{
									echo "<tr>\n";
									echo "<td><a href=\"/report/common/iso_car.php?edit&unique_id=".$rowOpenCAR['unique_id']."\">". $rowOpenCAR['id']."</a></td>\n";
									echo "<td>". $rowOpenCAR['title']."</td>\n";
									echo "<td>". $rowOpenCAR['department']."</td>\n";
									echo "<td>". $rowOpenCAR['location']."</td>\n";
									echo "<td>". date(phpdispfd,strtotime($rowOpenCAR['created_date']))."</td>\n";
									echo "<td>". $rowOpenCAR['created_name']."</td>\n";
									echo "</tr>\n";
								}
							}
							?>                        
						</tbody>
					</table>
				</div>
				<div id="openReprotsDiv" <?php if($resultOpenCAR->num_rows != 0){echo "style=\"display:none\"";}else{echo "style=\"text-align:center\"";} ?>>
					<br />
					<h2>No Open CARs</h2>
				</div>
			  </div>
			</div>
			
			<div class="line"></div>
			
			<div id="ReportOuterFrame">
				<?php  
					$sql="SELECT c.id, c.title, c.department, c.location, c.closed_date, u.name AS closed_name, c.unique_id
					FROM iso_car AS c
					LEFT JOIN users AS u ON u.uid = c.closed_uid
					WHERE c.closed = 'y' AND c.deleted = 'n'
					ORDER BY c.closed_date ASC;";
					if(!$resultClosedCAR = $mysqli->query($sql)){
						$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
						$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
						$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);	
					}
				?>  
				<div id="ReportFrame">     
				<div id="ReportFrameTitle">Closed CARs</div>
				<div id="openReprotsDiv" <?php if($resultClosedCAR->num_rows == 0){echo "style=\"display:none\"";} ?>>
					<table width="100%" id="closedCARTable" >
						<thead>
							<tr>
								<th>ID</th>
								<th>Title</th>
								<th>Department</th>
								<th>Location</th>
								<th>Closed Date</th>
								<th>Closing User</th>                   
							</tr>
						</thead>
						<tbody>
							<?php
							if($resultClosedCAR){
								while($rowClosedCAR = $resultClosedCAR->fetch_assoc())
								{
									echo "<tr>\n";
									echo "<td><a href=\"/report/common/iso_car.php?view&unique_id=".$rowClosedCAR['unique_id']."\">". $rowClosedCAR['id']."</a></td>\n";
									echo "<td>". $rowClosedCAR['title']."</td>\n";
									echo "<td>". $rowClosedCAR['department']."</td>\n";
									echo "<td>". $rowClosedCAR['location']."</td>\n";
									echo "<td>". date(phpdispfd,strtotime($rowClosedCAR['closed_date']))."</td>\n";
									echo "<td>". $rowClosedCAR['closed_name']."</td>\n";
									echo "</tr>\n";
								}
							}
							?>                        
						</tbody>
					</table>
				</div>
				<div id="openReprotsDiv" <?php if($resultClosedCAR->num_rows != 0){echo "style=\"display:none\"";}else{echo "style=\"text-align:center\"";} ?>>
					<br />
					<h2>No Closed CARs</h2>
				</div>
			  </div>
			</div>
		</div><!-- tab 1 -->
		
		<div id="tab-2" class="tab">
			<div id="ReportOuterFrame">
				<?php  
					$sql="SELECT p.id, p.title, p.department, p.location, p.created_date, u.name AS created_name, p.unique_id
					FROM iso_par AS p
					LEFT JOIN users AS u ON u.uid = p.created_uid
					WHERE p.closed = 'n' AND p.deleted = 'n'
					ORDER BY p.created_date ASC;";
					if(!$resultOpenPAR = $mysqli->query($sql)){
						$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
						$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
						$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);	
					}
				?>  
				<div id="ReportFrame">     
				<div id="ReportFrameTitle">Open PARs</div>
				<div id="openReprotsDiv" <?php if($resultOpenPAR->num_rows == 0){echo "style=\"display:none\"";} ?>>
					<table width="100%" id="openPARTable" >
						<thead>
							<tr>
								<th>ID</th>
								<th>Title</th>
								<th>Department</th>
								<th>Location</th>
								<th>Created Date</th>
								<th>Creating User</th>                   
							</tr>
						</thead>
						<tbody>
							<?php
							if($resultOpenPAR){
								while($rowOpenPAR = $resultOpenPAR->fetch_assoc())
								{
									echo "<tr>\n";
									echo "<td><a href=\"/report/common/iso_par.php?edit&unique_id=".$rowOpenPAR['unique_id']."\">". $rowOpenPAR['id']."</a></td>\n";
									echo "<td>". $rowOpenPAR['title']."</td>\n";
									echo "<td>". $rowOpenPAR['department']."</td>\n";
									echo "<td>". $rowOpenPAR['location']."</td>\n";
									echo "<td>". date(phpdispfd,strtotime($rowOpenPAR['created_date']))."</td>\n";
									echo "<td>". $rowOpenPAR['created_name']."</td>\n";
									echo "</tr>\n";
								}
							}
							?>                        
						</tbody>
					</table>
				</div>
				<div id="openReprotsDiv" <?php if($resultOpenPAR->num_rows != 0){echo "style=\"display:none\"";}else{echo "style=\"text-align:center\"";} ?>>
					<br />
					<h2>No Open PARs</h2>
				</div>
			  </div>
			</div>
			
			<div class="line"></div>
			
			<div id="ReportOuterFrame">
				<?php  
					$sql="SELECT p.id, p.title, p.department, p.location, p.closed_date, u.name AS closed_name, p.unique_id
					FROM iso_par AS p
					LEFT JOIN users AS u ON u.uid = p.closed_uid
					WHERE p.closed = 'y' AND p.deleted = 'n'
					ORDER BY p.closed_date ASC;";
					if(!$resultClosedPAR = $mysqli->query($sql)){
						$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
						$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
						$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
					}
				?>  
				<div id="ReportFrame">     
				<div id="ReportFrameTitle">Closed PARs</div>
				<div id="openReprotsDiv" <?php if($resultClosedPAR->num_rows == 0){echo "style=\"display:none\"";} ?>>
					<table width="100%" id="closedPARTable" >
						<thead>
							<tr>
								<th>ID</th>
								<th>Title</th>
								<th>Department</th>
								<th>Location</th>
								<th>Closed Date</th>
								<th>Closing User</th>                   
							</tr>
						</thead>
						<tbody>
							<?php
							if($resultClosedPAR){
								while($rowClosedPAR = $resultClosedPAR->fetch_assoc())
								{
									echo "<tr>\n";
									echo "<td><a href=\"/report/common/iso_par.php?view&unique_id=".$rowClosedPAR['unique_id']."\">". $rowClosedPAR['id']."</a></td>\n";
									echo "<td>". $rowClosedPAR['title']."</td>\n";
									echo "<td>". $rowClosedPAR['department']."</td>\n";
									echo "<td>". $rowClosedPAR['location']."</td>\n";
									echo "<td>". date(phpdispfd,strtotime($rowClosedPAR['closed_date']))."</td>\n";
									echo "<td>". $rowClosedPAR['closed_name']."</td>\n";
									echo "</tr>\n";
								}
							}
							?>                        
						</tbody>
					</table>
				</div>
				<div id="openReprotsDiv" <?php if($resultClosedPAR->num_rows != 0){echo "style=\"display:none\"";}else{echo "style=\"text-align:center\"";} ?>>
					<br />
					<h2>No Closed PARs</h2>
				</div>
			  </div>
			</div>
		</div><!-- tab 2 -->
		
		<div id="tab-3" class="tab">
			<div id="ReportOuterFrame">
				<?php  
					$sql="SELECT f.id, f.customer, f.description, f.created_date, u.name AS created_name, f.unique_id
					FROM iso_fbc AS f
					LEFT JOIN users AS u ON u.uid = f.created_uid
					WHERE f.closed = 'n' AND f.deleted = 'n'
					ORDER BY f.created_date ASC;";
					if(!$resultOpenFBC = $mysqli->query($sql)){
						$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
						$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
						$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
					}
				?>  
				<div id="ReportFrame">     
					<div id="ReportFrameTitle">Open FBCs</div>
					<div id="openReprotsDiv" <?php if($resultOpenFBC->num_rows == 0){echo "style=\"display:none\"";} ?>>
						<table width="100%" id="openFBCTable" >
							<thead>
								<tr>
									<th>ID</th>
									<th>Customer</th>
									<th>Description</th>
									<th>Created Date</th>
									<th>Creating User</th>                   
								</tr>
							</thead>
							<tbody>
								<?php
								if($resultOpenFBC){
									while($rowOpenFBC = $resultOpenFBC->fetch_assoc())
									{
										echo "<tr>\n";
										echo "<td><a href=\"/report/common/iso_fbc.php?edit&unique_id=".$rowOpenFBC['unique_id']."\">". $rowOpenFBC['id']."</a></td>\n";
										echo "<td>". $rowOpenFBC['customer']."</td>\n";
										echo "<td>". limit_words($rowOpenFBC['description'],$comp_word_limt)."</td>\n";
										echo "<td>". date(phpdispfd,strtotime($rowOpenFBC['created_date']))."</td>\n";
										echo "<td>". $rowOpenFBC['created_name']."</td>\n";
										echo "</tr>\n";
									}
								}
								?>                        
							</tbody>
						</table>
					</div>
					<div id="openReprotsDiv" <?php if($resultOpenFBC->num_rows != 0){echo "style=\"display:none\"";}else{echo "style=\"text-align:center\"";} ?>>
						<br />
						<h2>No Open FBCs</h2>
					</div>
			  	</div>
			</div>
			
			<div class="line"></div>
		
			<div id="ReportOuterFrame">
				<?php  
					$sql="SELECT f.id, f.customer, f.description, f.closed_date, u.name AS closed_name, f.unique_id
					FROM iso_fbc AS f
					LEFT JOIN users AS u ON u.uid = f.created_uid
					WHERE f.closed = 'y' AND f.deleted = 'n'
					ORDER BY f.closed_date ASC;";
					if(!$resultClosedFBC = $mysqli->query($sql)){
						$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
						$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
						$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);	
					}
				?>  
				<div id="ReportFrame">     
					<div id="ReportFrameTitle">Closed FBCs</div>
					<div id="openReprotsDiv" <?php if($resultClosedFBC->num_rows == 0){echo "style=\"display:none\"";} ?>>
						<table width="100%" id="closedFBCTable" >
							<thead>
								<tr>
									<th>ID</th>
									<th>Customer</th>
									<th>Description</th>
									<th>Closed Date</th>
									<th>Closing User</th>                   
								</tr>
							</thead>
							<tbody>
								<?php
								if($resultClosedFBC){
									while($rowClosedFBC = $resultClosedFBC->fetch_assoc())
									{
										echo "<tr>\n";
										echo "<td><a href=\"/report/common/iso_fbc.php?view&unique_id=".$rowClosedFBC['unique_id']."\">". $rowClosedFBC['id']."</a></td>\n";
										echo "<td>". $rowClosedFBC['customer']."</td>\n";
										echo "<td>". limit_words($rowClosedFBC['description'],$comp_word_limt)."</td>\n";
										echo "<td>". date(phpdispfd,strtotime($rowClosedFBC['closed_date']))."</td>\n";
										echo "<td>". $rowClosedFBC['closed_name']."</td>\n";
										echo "</tr>\n";
									}
								}
								?>                        
							</tbody>
						</table>
					</div>
					<div id="openReprotsDiv" <?php if($resultClosedFBC->num_rows != 0){echo "style=\"display:none\"";}else{echo "style=\"text-align:center\"";} ?>>
						<br />
						<h2>No Closed FBCs</h2>
					</div>
				</div>
			</div>
			
			<div class="line"></div>
		
			<div id="ReportOuterFrame">
				<?php  
					$sql="SELECT r.report_id, r.system_id, r.system_nickname, u.name, r.unique_id
						FROM systems_reports AS r
						LEFT JOIN iso_fbc AS f ON f.report_unique_id = r.unique_id
						LEFT JOIN users AS u ON u.uid = r.user_id
						WHERE r.fbc = 'Y' AND r.deleted = 'N' AND r.finalize_date IS NOT NULL AND f.unique_id IS NULL;";
					if(!$resultFBCReportBad = $mysqli->query($sql)){
						$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
						$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
						$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
					}
				?>  
				<div id="ReportFrame">     
					<div id="ReportFrameTitle">Closed Reports Without FBCs</div>
					<div id="openReprotsDiv" <?php if($resultFBCReportBad->num_rows == 0){echo "style=\"display:none\"";} ?>>
						<table width="100%" id="reportBadFBCTable">
							<thead>
								<tr>
									<th>Report ID</th>
									<th>System ID</th>
									<th>System Nickname</th>
									<th>Engineer</th>                   
								</tr>
							</thead>
							<tbody>
								<?php
								if($resultFBCReportBad){
									while($rowFBCReportBad = $resultFBCReportBad->fetch_assoc())
									{
										echo "<tr>\n";
										echo "<td><a href=\"/report/common/report_view.php?uid=".$myusername."&id=".$rowFBCReportBad['unique_id']."\">". $rowFBCReportBad['report_id']."</a></td>\n";
										echo "<td>". $rowFBCReportBad['system_id']."</td>\n";
										echo "<td>". $rowFBCReportBad['system_nickname']."</td>\n";
										echo "<td>". $rowFBCReportBad['name']."</td>\n";
										echo "</tr>\n";
									}
								}
								?>                        
							</tbody>
						</table>
					</div>
					<div id="openReprotsDiv" <?php if($resultFBCReportBad->num_rows != 0){echo "style=\"display:none\"";}else{echo "style=\"text-align:center\"";} ?>>
						<br />
						<h2>No Reports without FBCs</h2>
					</div>
				</div>
			</div>
			
			<div class="line"></div>
		
			<div id="ReportOuterFrame">
				<?php  
					$sql="SELECT r.request_num, r.system_id, r.system_nickname, u.name, r.unique_id, r.system_unique_id
						FROM systems_requests AS r
						LEFT JOIN iso_fbc AS f ON f.request_unique_id = r.unique_id
						LEFT JOIN users AS u ON u.uid = r.sender
						WHERE r.fbc = 'Y' AND r.deleted = 'N' AND f.unique_id IS NULL;";
					if(!$resultFBCRequestBad = $mysqli->query($sql)){
						$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
						$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
						$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
					}
				?>  
				<div id="ReportFrame">     
					<div id="ReportFrameTitle">Requests Without FBCs</div>
					<div id="openReprotsDiv" <?php if($resultFBCRequestBad->num_rows == 0){echo "style=\"display:none\"";} ?>>
						<table width="100%" id="requestBadFBCTable">
							<thead>
								<tr>
									<th>Request ID</th>
									<th>System ID</th>
									<th>System Nickname</th>
									<th>Engineer</th>                   
								</tr>
							</thead>
							<tbody>
								<?php
								if($resultFBCRequestBad){
									while($rowFBCRequestBad = $resultFBCRequestBad->fetch_assoc())
									{
										echo "<tr>\n";
										echo "<td><a href=\"/report/common/request_new_edit.php?v&e&user_id=".$myusername."&req_id=".$rowFBCRequestBad['unique_id']."&unique_id=".$rowFBCRequestBad['system_unique_id']."\">". $rowFBCRequestBad['request_num']."</a></td>\n";
										echo "<td>". $rowFBCRequestBad['system_id']."</td>\n";
										echo "<td>". $rowFBCRequestBad['system_nickname']."</td>\n";
										echo "<td>". $rowFBCRequestBad['name']."</td>\n";
										echo "</tr>\n";
									}
								}
								?>                        
							</tbody>
						</table>
					</div>
					<div id="openReprotsDiv" <?php if($resultFBCRequestBad->num_rows != 0){echo "style=\"display:none\"";}else{echo "style=\"text-align:center\"";} ?>>
						<br />
						<h2>No Requests without FBCs</h2>
					</div>
				</div>
			</div>
			
		</div><!--tab 3 -->
</div>
</div>
<?php require_once($footer_include); ?>