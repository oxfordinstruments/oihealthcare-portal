<?php
//Update Completed 12/12/14
$ip = $_SERVER['REMOTE_ADDR'];
$domain = gethostbyaddr($ip);
$settings = new SimpleXMLElement($_SERVER['DOCUMENT_ROOT'].'/settings.xml', null, true);
?>
<!DOCTYPE html>
<html>
<head>
<?php require_once($_SERVER['DOCUMENT_ROOT'].'/report/head_foot/default_inc_head.php'); ?>
<?php require_once($_SERVER['DOCUMENT_ROOT'].'/report/head_foot/default_inc_css.php'); ?>
<?php require_once($_SERVER['DOCUMENT_ROOT'].'/report/head_foot/default_inc_js.php'); ?>

</head>
<body>
<?php require_once($_SERVER['DOCUMENT_ROOT'].'/report/head_foot/default_header.php'); ?>
<div id="OIReportContent">
    <table width="100%" border="0" cellspacing="3" cellpadding="3">
        <tr>
            <td><h1>
                    <p align="center">You have been banned from this website!</p>
                    <br />
                    <p align="center">You IP Address and Domain Name have been logged for future referance.</p>
                    <br />
                    <p align="center"><?php echo "IP: ".$ip." Domain: ".$domain; ?></p>
                    <br />
                    <p align="center">Please contact <?php echo $settings->company_name; ?> support<br />if you feel you have reached this page in error.</p>
                    <br />
					<p align="center">888-673-5151 (US Only)</p> 
                </h1></td>
        </tr>
    </table>
</div>
<?php require_once($_SERVER['DOCUMENT_ROOT'].'/report/head_foot/default_footer.php'); ?>