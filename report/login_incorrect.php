<?php
//Update Completed 12/12/14
$no_redirect = true;
$remove_session = true;

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');
?>
<!DOCTYPE html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<?php require_once($js_include);?>

</head>
<body>
<?php require_once($header_include); ?>
<div id="OIReportContent">
    <table width="100%" border="0" cellspacing="3" cellpadding="3">
        <tr>
            <td><h1><p align="center">Wrong Username/Password</p>
                <p align="center"><?php echo "Attempt " . $_COOKIE["oihp_tries"] . " of 4!<br />";  ?></p>
                <p align="center"><?php if($_COOKIE['oihp_tries'] == "3"){ echo "One more failed attempt and you will be locked out of the site!<br />";}?></p>
                <p align="center"><a href="index.php">&lt;Try Again&gt;</a></p></h1></td>
        </tr>
    </table>
</div>
<?php require_once($footer_include); ?>