<?php
$debug = false;

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/session_handler.php');
session_name("OIREPORT");
$session = new Session();

$settings = new SimpleXMLElement($_SERVER['DOCUMENT_ROOT'].'/settings.xml', null, true);//Open the settings.xml file
require_once($_SERVER['DOCUMENT_ROOT'].'/define_inc.php');

ob_start();//Start output buffering

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

require($_SERVER['DOCUMENT_ROOT'].'/mysqlInfo.php');//require the mysql connection info
$mysqli = new mysqli("$host", "$username", "$password", "$db_name");

/* check connection */
if ($mysqli->connect_errno) {
	$log->logerr($mysqli->connect_error,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support',1000,true,basename(__FILE__),__LINE__);
}

if (isset($_SESSION["login"])){
	$_SESSION['start'] = time(); // taking now logged in time
	if(isset($_SESSION['login_extend'])){
		$_SESSION['expire'] = $_SESSION['start'] + 36000; // ending a session in 10hrs from the starting time
		$expire_cookie = time()+60*60*10;
	}else{
		$_SESSION['expire'] = $_SESSION['start'] + 10800; // ending a session in 3hrs from the starting time
		$expire_cookie = time()+60*60*3;
	}

	//set expire cookie
	setcookie("oihp_expire",$expire_cookie,$expire_cookie,'/');
	
	//unset tries cookie
	if (isset($_COOKIE['oihp_tries'])) {
		unset($_COOKIE['oihp_tries']);
		setcookie('oihp_tries', '', time() - 3600, '/'); // empty value and old timestamp
	}
	
	//setcookie("oihp_tries", '', time()-3600, '/');

	//check for out of date or missing login info
	$sql="SELECT * FROM users AS u 
			WHERE ((u.secret_1 IS NULL OR u.secret_1_id IS NULL OR u.secret_2 IS NULL OR u.secret_2_id IS NULL)
			OR (u.secret_1_id = 0 OR u.secret_2_id = 0)
			OR (u.pwd_chg_date IS NULL OR u.pwd_chg_date = '' OR datediff(date(u.pwd_chg_date),curdate()) < 0))
			AND u.uid = '".$_SESSION['login']."' LIMIT 1;";
	if($debug){
		echo $sql,EOL;	
	}
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
	$num_rows = $result->num_rows;
		
	if($num_rows > 0){
		if(!$debug){
			header("location:login_update.php");
		}else{
			die("location:login_update.php");
		}
	}else{	
		$sql="SELECT * FROM users_role_id WHERE id='".$_SESSION['role']."';";
		if($debug){
			echo $sql,EOL;	
		}
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		}
		$rowRole = $result->fetch_assoc();
		
		//Send to default role location
		if($_SESSION['role'] == '0'){
			if(!$debug){
				$log->logerr('',1003,true,basename(__FILE__),__LINE__);
			}else{
				echo "location:/error.php?n=1003&p=login_success",EOL;
			}
		}else{
			if(!$debug){
				if(isset($_SESSION['urlGET'])){
					urlGET();
				}
				header("location:".$rowRole['location']."/");
			}else{
				if(isset($_SESSION['urlGET'])){
					echo "URL has GET included",EOL;
				}
				echo "Header Location "	.$rowRole['location']."/",EOL;
			}
		}
	}
}else{
	if(!$debug){
		header("location:/");
	}else{
		d($_SESSION);
		die("location:/");
	}
}

function urlGET(){
	//unset($_SESSION['urlGET']);
	if(isset($_SESSION['email_face'])){
		//unset($_SESSION['email_face']);	
		header("location:/report/common/face_signoff.php?system_id=".$_SESSION['email_face']);
		die("oops something bad happened!!");
	}
}
?> 
