<?php
//Update Completed 12/12/14
$no_redirect = true;
$use_default_includes = true;

$debug = false;
if(isset($_GET['debug'])){
	$debug=true;
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

$questions = array();
$sql="SELECT id, question FROM users_secret_questions;";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);	
}
while($row = $result->fetch_assoc()){
	array_push($questions,array($row['id'],$row['question']));
}

$sql="SELECT u.secret_1,u.secret_1_id,u.secret_2,u.secret_2_id,u.pwd_chg_date, datediff(date(u.pwd_chg_date),curdate()) AS diff
FROM users AS u 
WHERE ((u.secret_1 IS NULL OR u.secret_1_id IS NULL OR u.secret_2 IS NULL OR u.secret_2_id IS NULL)
OR (u.secret_1_id = 0 OR u.secret_2_id = 0)
OR (u.pwd_chg_date IS NULL OR u.pwd_chg_date = '' OR datediff(date(u.pwd_chg_date),curdate()) <= 0))
AND u.uid = '".$_SESSION['login']."' LIMIT 1;";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);	
}
$row = $result->fetch_assoc();
d($row);

$badsecret = false;
$badpassword = false;


if($row['secret_1'] == '' || $row['secret_2'] == '' || $row['secret_1_id'] == '' || $row['secret_2_id'] == '' || $row['secret_1_id'] == '0' || $row['secret_2_id'] == '0'){
	$badsecret = true;
}

if($row['pwd_chg_date'] == '' or intval($row['diff']) < 0 ){
	$badpassword = true;	
}

d($row['pwd_chg_date']);
d($badpassword);
d($badsecret);

?> 

<!DOCTYPE html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<?php require_once($js_include);?>

<script src="/resources/js/jquery.badBrowser.js"></script>
<script type="text/javascript">
var Global = {};

$(document).ready(function() {  
	/*
		assigning keyup event to password field
		so everytime user type code will execute
	*/
	Global.pwdstr = 0;
	Global.mch = false;
	
	<?php if($badpassword){?>
	$("#new1pwd").focus();
	
	$("#formpwd").bind("keypress", function(e) {
		if (e.keyCode == 13) {
			if($("#new1pwd").is(":focus")){
				$("#new2pwd").focus();
		    }else{
				submitcheck();
	            return false; // ignore default event
			}
	   }
	});
	<?php }else{ ?>
	$("#secret1").focus();
	<?php } ?>
	<?php if(isset($_GET['old'])){ ?>
	$('#result1').addClass('passwordNoMatch');
	$('#result1').html("Old and New passwords cannot be the same !");
	<?php } ?>
	<?php if(isset($_GET['prev'])){ ?>
	$('#result1').addClass('passwordNoMatch');
	$('#result1').html("New password must differ from previous 3 !");
	<?php } ?>
	
	$('#new1pwd').keyup(function()
	{
		$('#result1').html(checkStrength($('#new1pwd').val()));
		$('#result2').html(checkMatch($('#new2pwd').val()));
	})	
	
	$('#new2pwd').keyup(function()
	{
		$('#result2').html(checkMatch($('#new2pwd').val()));
	})
	
//	$('#oldpwd').keyup(function()
//	{
//		$('#oldresult').removeClass();
//		$('#oldresult').html('');
//	})
	
	/*
		checkStrength is function which will do the 
		main password strength checking for us
	*/
	
	function checkStrength(password)
	{
		//console.log("Start");
		//initial strength
		var strength = 0
		
		//if the password length is less than 6, return message.
		if (password.length < 5) { 
			$('#result1').removeClass()
			$('#result1').addClass('passwordShort')
			return 'Password Too short' 
		}
		
		//length is ok, lets continue.
		
		//if length is 8 characters or more, increase strength value
		if (password.length > 6) strength += 1
		
		//if password contains both lower and uppercase characters, increase strength value
		if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))  strength += 1
		
		//if it has numbers and characters, increase strength value
		if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))  strength += 1 
		
		//if it has one special character, increase strength value
		if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))  strength += 1
		
		//if it has two special characters, increase strength value
		if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1
		
		//now we have calculated strength value, we can return messages
		
		Global.pwdstr = strength;
		
		//console.log(Global.pwdstr);
		
		//if value is less than 2
		if (strength < 2 )
		{
			$('#result1').removeClass()
			$('#result1').addClass('passwordWeak')
			return 'Password Weak'			
		}
		else if (strength == 2 )
		{
			$('#result1').removeClass()
			$('#result1').addClass('passwordGood')
			return 'Password Good'		
		}
		else
		{
			$('#result1').removeClass()
			$('#result1').addClass('passwordStrong')
			return 'Password Strong'
		}	
	}
	
	function checkMatch(password)
	{
		if (password != $("#new1pwd").val()) { 
			$('#result2').removeClass();
			$('#result2').addClass('passwordNoMatch');
			Global.mch = false;
			return 'Passwords Don\'t Match' ;
		}else{
			$('#result2').removeClass();
			$('#result2').addClass('passwordMatch');
			Global.mch = true;
			return 'Passwords Match';
		}	
	}
});

function cancel_login(){
	window.location = 'logout.php';	
}

function submitcheck(){
	var errors = false;
	<?php if($badpassword){?>
	if(Global.pwdstr < 2){
		$.prompt("<h3>Password strength must be at least \"good\"!</h3>",{
			title: 'Error'	
		});
		//alert("Password strength must be at least \"good\"!");	
		return;
	}
	if(!Global.mch){
		$.prompt("<h3>Passwords must match!</h3>",{
			title: "Error"	
		});
		//alert("Passwords must match!");	
		return;
	}
	document.getElementById("formpwd").submit();
	<?php } 
	if($badsecret){
	?>
	if(document.getElementById("secret1").selectedIndex == document.getElementById("secret2").selectedIndex){
		$.prompt("<h3>Must choose 2 differnet secret questions!</h3>",{
			title: "Error"	
		});
		//alert("Must choose 2 different secret questions!");
		return;
	}
	if(document.getElementById("answer1").value == ""){
		$.prompt("<h3>Answer 1 cannot be blank!</h3>",{
			title: "Error"	
		});
		//alert("Answer 1 cannot be blank!");	
		return;
	}
	if(document.getElementById("answer2").value == ""){
		$.prompt("<h3>Answer 2 cannot be blank!</h3>",{
			title: "Error"	
		});
		//alert("Answer 2 cannot be blank!");	
		return;
	}
	
	$.prompt("<h3>Please remember your secret question answers, you will need them to reset your password.</h3>",{
		title: "Question",
		buttons: { Yes: 1, No: -1 },
		focus: 1,
		submit:function(e,v,m,f){ 
			e.preventDefault();	
			if(v == 1){
				$.prompt.close();
				document.getElementById("formpwd").submit();
			}else{
				$.prompt.close();	
			}
		}
	});
//	if(confirm("Please remember your secret question answers, you will need them to reset your password.")){
//		document.getElementById("formpwd").submit();
//	}
	<?php } ?>
}


</script>
<style>
.passwordShort {
	font-weight: bold;
	color: #F00;
}
.passwordWeak {
	font-weight: bold;
	color: #F00;
}
.passwordGood {
	font-weight: bold;
	color: #F90;
}
.passwordStrong {
	font-weight: bold;
	color: #090;
}
.passwordMatch {
	font-weight: bold;
	color: #090;
}
.passwordNoMatch {
	font-weight: bold;
	color: #F00;
}


</style>
</head>
<body>
<?php require_once($header_include); ?>
<div class="outerContainer" id="content">
	<div style="width:100%; margin-top:20px; text-align:center;  margin-bottom:20px;">
		<h1 style="color:#F79447">Login Information Update Required</h1>
	</div>
	<div style="width:500px; margin-left:auto; margin-right:auto; color:#1B2673; padding-top:15px; padding-bottom:10px">
		<div style="text-align:center">
		
			<form id="formpwd" name="formpwd" method="post" action="login_update_do.php">
				<div id="chgpwd" <?php echo ($badpassword ? '' : 'style="display:none"'); ?>>
					<input type="hidden" name="password" value="<?php if($badpassword){echo 'Y';} ?>" />
					<h2>Change Password</h2>
					<div style="text-align:left; width:75%; margin-left:auto; margin-right:auto"><strong>New Password:</strong></div>
					<div style=" width:75%; margin-left:auto; margin-right:auto; margin-bottom:2px;">
						<input style="width:100%" type="password" id="new1pwd" name="new1pwd" />
						<span id="result1"></span>
					</div>
					<br>
					<div style="text-align:left; width:75%; margin-left:auto; margin-right:auto"><strong>Retype New Password:</strong></div>
					<div style=" width:75%; margin-left:auto; margin-right:auto; margin-bottom:2px;">
						<input style="width:100%" type="password" id="new2pwd" name="new2pwd" />
						<span id="result2"></span>
					</div>
					<br>
					<div style="width:70%; margin-left:auto; margin-right:auto; text-align:left">
						<span style="font-weight:bold">New Password Requirements:</span>
						<ul style="list-style:inside;">
							<li>More than 6 characters</li>
							<li>Not the same as old password</li>
							<li>Not the same as previous 3 passwords</li>
							<li>Strength of "good" or "strong"</li>
							<li>Updated every 6 months</li>
						</ul>
					</div>
					<br>
					<br>
				</div>
				<div id="secrets"<?php echo ($badsecret ? '' : 'style="display:none"'); ?>>
					<input type="hidden" name="secret" value="<?php if($badsecret){echo 'Y';} ?>" />
					<h2>Must choose 2 different questions</h2>
					<div style="text-align:left; width:75%; margin-left:auto; margin-right:auto"><strong>Secret 1*:</strong></div>
					<div style=" width:75%; margin-left:auto; margin-right:auto; margin-bottom:2px;">
						<select style="width:100%" id="secret1" name="secret1">
						<?php 	
							foreach($questions as $value){
								echo "<option value=\"".$value[0]."\">".$value[1]."</option>",EOL;
							}
						?>	
						</select>
					</div>
					
					<div style="text-align:left; width:75%; margin-left:auto; margin-right:auto"><strong>Answer 1*:</strong></div>
					<div style=" width:75%; margin-left:auto; margin-right:auto; margin-bottom:2px;">
						<input style="width:100%" type="text" id="answer1" name="answer1" />
					</div>
					<br>
					<br>
					
					<div style="text-align:left; width:75%; margin-left:auto; margin-right:auto"><strong>Secret 2*:</strong></div>
					<div style=" width:75%; margin-left:auto; margin-right:auto; margin-bottom:2px;">
						<select style="width:100%" id="secret2" name="secret2">
						<?php 	
							foreach($questions as $value){
								echo "<option value=\"".$value[0]."\">".$value[1]."</option>",EOL;
							}
						?>	
						</select>
					</div>				
					<div style="text-align:left; width:75%; margin-left:auto; margin-right:auto"><strong>Answer 2*:</strong></div>
					<div style=" width:75%; margin-left:auto; margin-right:auto; margin-bottom:2px;">
						<input style="width:100%" type="text" id="answer2" name="answer2" />
					</div>
					<br>
					<br>
				</div>
				<p>
					<input name="Submit" id="submitbtn" type="button" value="Submit" class="button" onclick="submitcheck();" />
				</p>
				<p>
					<input name="Submit" id="cancel" type="button" value="Cancel Login" class="button" onclick="cancel_login();" />
				</p>
			</form>   
		</div>
    </div>
</div>
<?php require_once($footer_include); ?>