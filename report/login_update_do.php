<?php
$debug = false;
$no_redirect = true;
$use_default_includes = true;
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/ldap.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$logger = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

$errors = array();
if(isset($_POST['password'])){
	if(strtolower($_POST['password']) == 'y'){
		//if(!isset($_POST['oldpwd'])){header("Location:login_update.php?old");}
		$pwd_chg_date = strtotime('+6 months', time());
		$pwd_chg_date = date(storef,$pwd_chg_date);
		$salt = "STyf1anSTydajgnmd";
		//$oldpwd = "{MD5}" . base64_encode( pack( "H*", md5( $_POST['oldpwd'] . $salt ) ) );
		$newpwd = "{MD5}" . base64_encode( pack( "H*", md5( $_POST['new1pwd'] . $salt ) ) );
		
		if($oldpwd == $newpwd){		
			$logger->logmsg('password same');
			header("Location:login_update.php?same");
			exit();
		}		
		
		$sql="SELECT u.uid, u.name, u.lastname, u.email, u.pwd, 
		CONCAT_WS('', MAX(IF(pid.perm = 'perm_dms_access', 'Y', 'N'))) AS perm_dms_access, 
		CONCAT_WS('', MAX(IF(pid.perm = 'perm_kb_access', 'Y', 'N'))) AS perm_kb_access
		FROM users AS u
		LEFT JOIN users_perms AS up ON up.uid = u.uid
		LEFT JOIN users_perm_id AS pid ON pid.id = up.pid
		LEFT JOIN users_groups AS ug ON ug.uid = u.uid
		LEFT JOIN users_group_id AS gid ON gid.id = ug.gid
		WHERE u.uid = '".$_SESSION['login']."'
		GROUP BY u.uid;";
		$result = $mysqli->query($sql);
		$row = $result->fetch_assoc();
		$count = $result->num_rows;
		if($row['pwd'] == $newpwd){	
			$logger->logmsg('password matches old pwd');	
			header("Location:login_update.php?old");
			exit();
		}			
		
		$sql="UPDATE users SET `pwd`='$newpwd', `pwd_chg_date`='$pwd_chg_date' WHERE uid='".$_SESSION['login']."' LIMIT 1;";
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);	
		}
		
		$ldap = new ldapUtil();
			
		if($debug){echo "running ldap operations...",EOL;}
		$logger->logmsg('Running ldap ops in login_update_do');
		
		if(strtolower($row['perm_kb_access']) == 'y'){
			$kb = true;	
		}else{
			$kb = false;
		}
		
		if(strtolower($row['perm_dms_access']) == 'y'){
			$dms = true;	
		}else{
			$dms = false;
		}
		
		$ldap->user($_SESSION['login'],$newpwd,$row['name'],$row['lastname'],$row['email'],$kb,$dms);				
	}
}

if(isset($_POST['secret'])){
	if(strtolower($_POST['secret']) == 'y'){
		
		
		if(!isset($_POST['secret1'])){
			$logger->logerr('Secret Update Error S1',1004,true,basename(__FILE__),__LINE__);
		}
		if(!isset($_POST['answer1'])){
			$logger->logerr('Secret Update Error A1',1004,true,basename(__FILE__),__LINE__);
		}
		if(!isset($_POST['secret2'])){
			$logger->logerr('Secret Update Error S2',1004,true,basename(__FILE__),__LINE__);
		}
		if(!isset($_POST['answer2'])){
			$logger->logerr('Secret Update Error A2',1004,true,basename(__FILE__),__LINE__);
		}
		
		$sql="UPDATE users SET `secret_1`='".base64_encode(strtolower($_POST['answer1']))."', `secret_1_id`='".$_POST['secret1']."', `secret_2`='".base64_encode(strtolower($_POST['answer2']))."', `secret_2_id`='".$_POST['secret2']."' WHERE uid='".$_SESSION['login']."';";
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);	
		}
	}
}

if (isset($_SESSION["login"])){
	//Send to default role location
		if($_SESSION['role'] == '0'){
			if(!$debug){
				$logger->logerr('Default Role Error in login_success',1003,true,basename(__FILE__),__LINE__);
			}else{
				$logger->logerr('Default Role Error in login_success',1003,true,basename(__FILE__),__LINE__);
			}
		}else{
			if(!$debug){
				header("location:".$_SESSION['loc']."/");
			}else{
				echo "Header Location "	.$_SESSION['loc']."/",EOL;
			}
		}
}else{
  header("location:/");
}