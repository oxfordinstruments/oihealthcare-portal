<?php
//Update Completed 12/12/14
$no_redirect = true;

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

$sql="UPDATE `users` SET `logged_in`='0' WHERE `uid`='".$_SESSION['login']."' LIMIT 1;";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support. Letter A',1000,true,basename(__FILE__),__LINE__);
}
$sql="DELETE FROM users_auto_logins WHERE uid = '".$_SESSION['login']."';";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support. Letter B',1000,true,basename(__FILE__),__LINE__);
}

$log->loginfo("User ".$_SESSION['login']." logged out",3,false,basename(__FILE__),__LINE__);

setcookie('OIREPORT','',time()-60*60*100,'/');
setcookie('OIREPORT_AL','',time()-60*60*100,'/');

session_destroy();
header("location:/report/index.php");
?>
