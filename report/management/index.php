<?php
/*todo-evo The PM sql and charts have to be reworked */


require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/resources/moment_php/Moment.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/resources/moment_php/MomentException.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/resources/moment_php/MomentHelper.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/resources/moment_php/MomentLocale.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/resources/moment_php/MomentFromVo.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/php_utils.php');
$php_utils =  new phpUtils();

$comp_word_limt 	 = intval($settings->complaint_word_limit);
$serv_word_limt		 = intval($settings->service_word_limit);
$custact_word_limt	 = intval($settings->custact_word_limit);
$probrpt_word_limt	 = intval($settings->probrpt_word_limit);
$myusername = $_SESSION["login"];

$now = new \Moment\Moment();
$now->setImmutableMode(true);

$sql="SELECT(
SELECT COUNT(id) AS over
FROM systems_requests
WHERE `deleted` = 'n' AND YEAR(DATE(response_date)) = YEAR(CURDATE()) AND MONTH(DATE(response_date)) = MONTH(CURDATE()) AND TIME_TO_SEC(TIMEDIFF(DATE(response_date), DATE(initial_call_date))) >= 1860) AS over, (
SELECT COUNT(id) AS under
FROM systems_requests
WHERE `deleted` = 'n' AND YEAR(DATE(response_date)) = YEAR(CURDATE()) AND MONTH(DATE(response_date)) = MONTH(CURDATE())) AS total;";
if(!$resultInitRespPerc = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$rowInitRespPerc = $resultInitRespPerc->fetch_assoc();

//contract Types
$sql="SELECT COUNT(sbc.contract_type) AS `count`, CONCAT('CT ',c.`type`) AS `type`
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
LEFT JOIN misc_contracts AS c ON sbc.contract_type = c.id
LEFT JOIN systems_types as t ON sb.system_type = t.id
WHERE t.`type` = 'CT' AND sbc.property = 'C'
GROUP BY sbc.contract_type;";
if(!$resultCTContractTypesCount = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$numResultsCTContractTypesCount = $resultCTContractTypesCount->num_rows;

$sql="SELECT COUNT(sbc.contract_type) AS `count`, CONCAT('MR ',c.`type`) AS `type`
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
LEFT JOIN misc_contracts AS c ON sbc.contract_type = c.id
LEFT JOIN systems_types AS t ON sb.system_type = t.id
WHERE t.`type` = 'MR' AND sbc.property = 'C'
GROUP BY sbc.contract_type;";
if(!$resultMRContractTypesCount = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$numResultsMRContractTypesCount = $resultMRContractTypesCount->num_rows;

$counter = 0;
$contractsType = "['',";
$contractsCount = "['',";
while($rowCTContractTypesCount = $resultCTContractTypesCount->fetch_assoc()){
		$contractsType .= " '".$rowCTContractTypesCount['type']."',";
		$contractsCount .= " ".$rowCTContractTypesCount['count'].",";
}
$counter = 0;
while($rowMRContractTypesCount = $resultMRContractTypesCount->fetch_assoc()){
    if (++$counter == $numResultsMRContractTypesCount) {
        // last row
		$contractsType .= " '".$rowMRContractTypesCount['type']."']";
		$contractsCount .= " ".$rowMRContractTypesCount['count']."]";
    } else {
        // not last row
		$contractsType .= " '".$rowMRContractTypesCount['type']."',";
		$contractsCount .= " ".$rowMRContractTypesCount['count'].",";
    }
}

$sql="SELECT sb.system_id, sbc.nickname, sbc.contract_end_date, sbc.warranty_end_date, sb.unique_id
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sbc.unique_id = sb.unique_id AND sbc.property = 'C' 
LEFT JOIN systems_types AS e ON e.id = sb.system_type
WHERE DATEDIFF(DATE(sbc.contract_end_date), NOW()) < 30 
AND DATE(sbc.contract_end_date) >= DATE_SUB(NOW(), INTERVAL 1 DAY) 
AND e.`type` = 'CT';";
if(!$resultCTContractExpire30 = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$numResultCTContractExpire30 = $resultCTContractExpire30->num_rows;

$sql="SELECT sb.system_id, sbc.nickname, sbc.contract_end_date, sbc.warranty_end_date, sb.unique_id
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sbc.unique_id = sb.unique_id AND sbc.property = 'C' 
LEFT JOIN systems_types AS e ON e.id = sb.system_type
WHERE DATEDIFF(DATE(sbc.contract_end_date), NOW()) < 30 
AND DATE(sbc.contract_end_date) >= DATE_SUB(NOW(), INTERVAL 1 DAY) 
AND e.`type` = 'MR';";
if(!$resultMRContractExpire30 = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$numResultMRContractExpire30 = $resultMRContractExpire30->num_rows;

$sql="SELECT sb.system_id, sbc.nickname, sbc.contract_end_date, sbc.warranty_end_date, sb.unique_id
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sbc.unique_id = sb.unique_id AND sbc.property = 'C' 
LEFT JOIN systems_types AS e ON e.id = sb.system_type
WHERE DATEDIFF(DATE(sbc.warranty_end_date), NOW()) < 30 
AND DATE(sbc.warranty_end_date) >= DATE_SUB(NOW(), INTERVAL 1 DAY) 
AND e.`type` = 'CT';";
if(!$resultCTWarrentyExpire30 = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$numResultCTWarrentyExpire30 = $resultCTWarrentyExpire30->num_rows;

$sql="SELECT sb.system_id, sbc.nickname, sbc.contract_end_date, sbc.warranty_end_date, sb.unique_id
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sbc.unique_id = sb.unique_id AND sbc.property = 'C' 
LEFT JOIN systems_types AS e ON e.id = sb.system_type
WHERE DATEDIFF(DATE(sbc.warranty_end_date), NOW()) < 30 
AND DATE(sbc.warranty_end_date) >= DATE_SUB(NOW(), INTERVAL 1 DAY) 
AND e.`type` = 'MR';";
if(!$resultMRWarrentyExpire30 = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$numResultMRWarrentyExpire30 = $resultMRWarrentyExpire30->num_rows;

$sql="SELECT sb.system_id, sbc.nickname, sbc.contract_end_date, sbc.warranty_end_date, sb.unique_id
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sbc.unique_id = sb.unique_id AND sbc.property = 'C' 
LEFT JOIN systems_types AS e ON e.id = sb.system_type
WHERE DATEDIFF(DATE(sbc.contract_end_date), NOW()) < 120 
AND DATE(sbc.contract_end_date) >= DATE_SUB(NOW(), INTERVAL 1 DAY) 
AND e.`type` = 'CT';";
if(!$resultCTContractExpire120 = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$numResultCTContractExpire120 = $resultCTContractExpire120->num_rows;

$sql="SELECT sb.system_id, sbc.nickname, sbc.contract_end_date, sbc.warranty_end_date, sb.unique_id
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sbc.unique_id = sb.unique_id AND sbc.property = 'C' 
LEFT JOIN systems_types AS e ON e.id = sb.system_type
WHERE DATEDIFF(DATE(sbc.contract_end_date), NOW()) < 120 
AND DATE(sbc.contract_end_date) >= DATE_SUB(NOW(), INTERVAL 1 DAY) 
AND e.`type` = 'MR';";
if(!$resultMRContractExpire120 = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$numResultMRContractExpire120 = $resultMRContractExpire120->num_rows;

$sql="SELECT sb.system_id, sbc.nickname, sbc.contract_end_date, sbc.warranty_end_date, sb.unique_id
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sbc.unique_id = sb.unique_id AND sbc.property = 'C' 
LEFT JOIN systems_types AS e ON e.id = sb.system_type
WHERE DATEDIFF(DATE(sbc.warranty_end_date), NOW()) < 120 
AND DATE(sbc.warranty_end_date) >= DATE_SUB(NOW(), INTERVAL 1 DAY) 
AND e.`type` = 'CT';";
if(!$resultCTWarrentyExpire120 = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$numResultCTWarrentyExpire120 = $resultCTWarrentyExpire120->num_rows;

$sql="SELECT sb.system_id, sbc.nickname, sbc.contract_end_date, sbc.warranty_end_date, sb.unique_id
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sbc.unique_id = sb.unique_id AND sbc.property = 'C' 
LEFT JOIN systems_types AS e ON e.id = sb.system_type
WHERE DATEDIFF(DATE(sbc.warranty_end_date), NOW()) < 120 
AND DATE(sbc.warranty_end_date) >= DATE_SUB(NOW(), INTERVAL 1 DAY) 
AND e.`type` = 'MR';";
if(!$resultMRWarrentyExpire120 = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$numResultMRWarrentyExpire120 = $resultMRWarrentyExpire120->num_rows;

$ct_contracts_expire30 = $numResultCTContractExpire30;
$ct_warranties_expire30 = $numResultCTWarrentyExpire30;
$mr_contracts_expire30 = $numResultMRContractExpire30;
$mr_warranties_expire30 = $numResultMRWarrentyExpire30;

$ct_contracts_expire120 = $numResultCTContractExpire120;
$ct_warranties_expire120 = $numResultCTWarrentyExpire120;
$mr_contracts_expire120 = $numResultMRContractExpire120;
$mr_warranties_expire120 = $numResultMRWarrentyExpire120;

//$sql="SELECT (
//SELECT COUNT(sbc.id) AS total
//FROM systems_base_cont AS sbc
//LEFT JOIN systems_base AS sb ON sbc.unique_id = sb.unique_id AND sbc.property = 'C'
//LEFT JOIN systems_types AS t ON sb.system_type = t.id
//WHERE t.`type` = 'CT'
//AND CAST(sb.pm_freq AS UNSIGNED) != 0) AS totalsites,
//(
//SELECT COUNT(sbc.id)
//FROM systems_base_cont AS sbc
//LEFT JOIN systems_base AS sb ON sbc.unique_id = sb.unique_id AND sbc.property = 'C'
//LEFT JOIN systems_types AS t ON sb.system_type = t.id
//WHERE YEAR(DATE(sb.last_pm)) = YEAR(CURDATE())
//AND DATEDIFF(DATE_ADD(DATE(sb.last_pm), INTERVAL sb.pm_freq MONTH), CURDATE()) > ".$settings->pm_overdue_days."
//AND DATEDIFF(DATE_ADD(DATE(sb.last_pm), INTERVAL sb.pm_freq MONTH), CURDATE()) < ".$settings->pm_due_days."
//AND t.`type` = 'CT'
//AND CAST(sb.pm_freq AS UNSIGNED) != 0) AS due,
//(
//SELECT COUNT(sbc.id)
//FROM systems_base_cont AS sbc
//LEFT JOIN systems_base AS sb ON sbc.unique_id = sb.unique_id AND sbc.property = 'C'
//LEFT JOIN systems_types AS t ON sb.system_type = t.id
//WHERE DATEDIFF(DATE_ADD(DATE(sb.last_pm), INTERVAL sb.pm_freq MONTH), CURDATE()) < ".$settings->pm_overdue_days."
//AND t.`type` = 'CT'
//AND CAST(sb.pm_freq AS UNSIGNED) != 0) AS overdue,
//(
//SELECT totalsites - (due + overdue)) AS current;";
//if(!$resultCTPM = $mysqli->query($sql)){
//	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
//	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
//	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
//}
//$rowCTPM =$resultCTPM->fetch_assoc();
//
//$sql="SELECT sbc.system_id, sbc.nickname, sb.last_pm, u.name, DATE_ADD(DATE(sb.last_pm), INTERVAL sb.pm_freq MONTH) AS due, sbc.credit_hold
//FROM systems_base_cont AS sbc
//LEFT JOIN systems_base AS sb ON sbc.unique_id = sb.unique_id AND sbc.property = 'C'
//LEFT JOIN systems_assigned_pri AS a ON sbc.ver_unique_id = a.system_ver_unique_id
//LEFT JOIN users AS u ON a.uid = u.uid
//LEFT JOIN systems_types AS e ON sb.system_type = e.id
//WHERE YEAR(DATE(sb.last_pm)) = YEAR(CURDATE())
//AND DATEDIFF(DATE_ADD(DATE(sb.last_pm), INTERVAL sb.pm_freq MONTH), CURDATE()) > ".$settings->pm_overdue_days."
//AND DATEDIFF(DATE_ADD(DATE(sb.last_pm), INTERVAL sb.pm_freq MONTH), CURDATE()) < ".$settings->pm_due_days."
//AND e.`type` = 'CT'
//AND CAST(sb.pm_freq AS UNSIGNED) != 0;";
//if(!$resultCTPMdue = $mysqli->query($sql)){
//	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
//	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
//	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
//}
//
//$sql="SELECT sbc.system_id, sbc.nickname, sb.last_pm, u.name, DATE_ADD(DATE(sb.last_pm), INTERVAL sb.pm_freq MONTH) AS due, sbc.credit_hold
//FROM systems_base_cont AS sbc
//LEFT JOIN systems_base AS sb ON sbc.unique_id = sb.unique_id AND sbc.property = 'C'
//LEFT JOIN systems_assigned_pri AS a ON sbc.ver_unique_id = a.system_ver_unique_id
//LEFT JOIN users AS u ON a.uid = u.uid
//LEFT JOIN systems_types AS e ON sb.system_type = e.id
//WHERE DATEDIFF(DATE_ADD(DATE(sb.last_pm), INTERVAL sb.pm_freq MONTH), CURDATE()) < ".$settings->pm_overdue_days."
//AND e.`type` = 'CT'
//AND CAST(sb.pm_freq AS UNSIGNED) != 0;";
//if(!$resultCTPMoverdue = $mysqli->query($sql)){
//	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
//	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
//	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
//}
//
//$sql="SELECT (
//SELECT COUNT(sbc.id) AS total
//FROM systems_base_cont AS sbc
//LEFT JOIN systems_base AS sb ON sbc.unique_id = sb.unique_id AND sbc.property = 'C'
//LEFT JOIN systems_types AS t ON sb.system_type = t.id
//WHERE t.`type` = 'MR'
//AND CAST(sb.pm_freq AS UNSIGNED) != 0) AS totalsites,
//(
//SELECT COUNT(sbc.id)
//FROM systems_base_cont AS sbc
//LEFT JOIN systems_base AS sb ON sbc.unique_id = sb.unique_id AND sbc.property = 'C'
//LEFT JOIN systems_types AS t ON sb.system_type = t.id
//WHERE YEAR(DATE(sb.last_pm)) = YEAR(CURDATE())
//AND DATEDIFF(DATE_ADD(DATE(sb.last_pm), INTERVAL sb.pm_freq MONTH), CURDATE()) > ".$settings->pm_overdue_days."
//AND DATEDIFF(DATE_ADD(DATE(sb.last_pm), INTERVAL sb.pm_freq MONTH), CURDATE()) < ".$settings->pm_due_days."
//AND t.`type` = 'MR'
//AND CAST(sb.pm_freq AS UNSIGNED) != 0) AS due,
//(
//SELECT COUNT(sbc.id)
//FROM systems_base_cont AS sbc
//LEFT JOIN systems_base AS sb ON sbc.unique_id = sb.unique_id AND sbc.property = 'C'
//LEFT JOIN systems_types AS t ON sb.system_type = t.id
//WHERE DATEDIFF(DATE_ADD(DATE(sb.last_pm), INTERVAL sb.pm_freq MONTH), CURDATE()) < ".$settings->pm_overdue_days."
//AND t.`type` = 'MR'
//AND CAST(sb.pm_freq AS UNSIGNED) != 0) AS overdue,
//(
//SELECT totalsites - (due + overdue)) AS current;";
//if(!$resultMRPM = $mysqli->query($sql)){
//	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
//	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
//	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
//}
//$rowMRPM = $resultMRPM->fetch_assoc();
//
//$sql="SELECT sbc.system_id, sbc.nickname, sb.last_pm, u.name, DATE_ADD(DATE(sb.last_pm), INTERVAL sb.pm_freq MONTH) AS due, sbc.credit_hold
//FROM systems_base_cont AS sbc
//LEFT JOIN systems_base AS sb ON sbc.unique_id = sb.unique_id AND sbc.property = 'C'
//LEFT JOIN systems_assigned_pri AS a ON sbc.ver_unique_id = a.system_ver_unique_id
//LEFT JOIN users AS u ON a.uid = u.uid
//LEFT JOIN systems_types AS e ON sb.system_type = e.id
//WHERE YEAR(DATE(sb.last_pm)) = YEAR(CURDATE())
//AND DATEDIFF(DATE_ADD(DATE(sb.last_pm), INTERVAL sb.pm_freq MONTH), CURDATE()) > ".$settings->pm_overdue_days."
//AND DATEDIFF(DATE_ADD(DATE(sb.last_pm), INTERVAL sb.pm_freq MONTH), CURDATE()) < ".$settings->pm_due_days."
//AND e.`type` = 'MR'
//AND CAST(sb.pm_freq AS UNSIGNED) != 0;";
//if(!$resultMRPMdue = $mysqli->query($sql)){
//	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
//	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
//	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
//}
//
//$sql="SELECT sbc.system_id, sbc.nickname, sb.last_pm, u.name, DATE_ADD(DATE(sb.last_pm), INTERVAL sb.pm_freq MONTH) AS due, sbc.credit_hold
//FROM systems_base_cont AS sbc
//LEFT JOIN systems_base AS sb ON sbc.unique_id = sb.unique_id AND sbc.property = 'C'
//LEFT JOIN systems_assigned_pri AS a ON sbc.ver_unique_id = a.system_ver_unique_id
//LEFT JOIN users AS u ON a.uid = u.uid
//LEFT JOIN systems_types AS e ON sb.system_type = e.id
//WHERE DATEDIFF(DATE_ADD(DATE(sb.last_pm), INTERVAL sb.pm_freq MONTH), CURDATE()) < ".$settings->pm_overdue_days."
//AND e.`type` = 'MR'
//AND CAST(sb.pm_freq AS UNSIGNED) != 0;";
//if(!$resultMRPMoverdue = $mysqli->query($sql)){
//	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
//	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
//	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
//}
//
//$ct_pm_current = $rowCTPM['current'];
//$ct_pm_due = $rowCTPM['due'];
//$ct_pm_overdue = $rowCTPM['overdue'];
//$mr_pm_current = $rowMRPM['current'];
//$mr_pm_due = $rowMRPM['due'];
//$mr_pm_overdue = $rowMRPM['overdue'];


$response_total = intval($rowInitRespPerc['total']);
if($response_total != 0){
	$response_time = round((($rowInitRespPerc['total']- $rowInitRespPerc['over']) / $rowInitRespPerc['total'])*100,2);
}else{
	$response_time = 100;
}

$sql="SELECT u.uid, u.name
FROM users AS u
LEFT JOIN users_roles AS ur ON ur.uid = u.uid
LEFT JOIN users_role_id AS rid ON rid.id = ur.rid
WHERE rid.role = 'role_engineer' OR rid.role = 'role_contractor';";
if(!$resultEngineers = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$arr_engineers = array();
while($rowEngineers = $resultEngineers->fetch_assoc())
{
	array_push_assoc($arr_engineers, $rowEngineers['uid'], $rowEngineers['name']);
}

$sql="SELECT u.uid, u.name
FROM users AS u
LEFT JOIN users_groups AS ug ON ug.uid = u.uid
LEFT JOIN users_group_id AS gid ON gid.id = ug.gid
WHERE gid.`group` = 'grp_employee';";
if(!$resultUsers = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$arr_users = array();
while($rowUsers = $resultUsers->fetch_assoc())
{
	array_push_assoc($arr_users, $rowUsers['uid'], $rowUsers['name']);
}

$sql="SELECT * FROM `systems_status`";
if(!$resultStatus = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}
$arr_status = array();
while($rowStatus = $resultStatus->fetch_assoc())
{
	array_push_assoc($arr_status, $rowStatus['id'], $rowStatus['status']);
}

$sql = "SELECT sbc.unique_id, sb.system_id, sbc.nickname, sb.pm_dates, sb.last_pm, sb.pm_freq, u.name AS engineer, sbc.ver, sbc.credit_hold, sbc.pre_paid, sbc.property, sr.request_num, sr.pm_date
					FROM systems_base_cont AS sbc
					LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
					LEFT JOIN systems_assigned_pri AS sap ON sap.system_ver_unique_id = sbc.ver_unique_id
					LEFT JOIN users AS u ON sap.uid = u.uid
					LEFT JOIN systems_requests AS sr ON sr.system_ver_unique_id = sbc.ver_unique_id AND sr.pm = 'y' AND sr.`status` = 'open'
					WHERE sbc.property = 'c' 
					AND CAST(sb.pm_freq AS UNSIGNED) != 0 
					AND sb.pm_start != ''
					ORDER BY sb.last_pm ASC;";

if(!$resultPM = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

function array_push_assoc(&$array, $key, $value){
	$array[$key] = $value;
	return $array;
}

function limit_words($string, $word_limit)
{
	$words = explode(" ",$string);
	if(count($words) > $word_limit){
		return implode(" ",array_splice($words,0,$word_limit))."...(more)";
	}else{
		return implode(" ",array_splice($words,0,$word_limit));
	}
}
?>
<!DOCTYPE html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<link rel="stylesheet" type="text/css" href="/resources/css/portal.css">
<?php require_once($js_include);?>

<script type="text/javascript">
$(document).ready(function() {
/////////////////////////////////////////////////////////////////////////////////////	
	priceTable = $('#priceTable').dataTable({
			"bJQueryUI": true,
			//"bStateSave": true,
			"iCookieDuration": 60*60*24*365, // 1 year
			"sPaginationType": "full_numbers",
			"aaSorting": [],
			"fnInitComplete": function () {
           		 this.$('tr').click( function () {
               		 var href = $(this).find("a").attr("href");
					 if(href) {
						window.location = href;
					 }
           		 });
       		 }
		});
/////////////////////////////////////////////////////////////////////////////////////	
	pricedTable = $('#pricedTable').dataTable({
			"bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "../common/connectors/valuated_reports_connector.php",
			"bJQueryUI": true,
			"aLengthMenu": [[5, 10, 25, 50], [5, 10, 25, 50]],
			"iCookieDuration": 60*60*24*365, // 1 year
			"iDisplayLength": 5,
			"sPaginationType": "full_numbers",
			"aaSorting": [[0,'desc']], 			
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
					//$('tr:eq(0)', nRow).addClass("pastReportsHelp");
					//console.log(aData);
					$('td:eq(0)', nRow).html( function() {	
						var moment_dt = moment($(this).text()).tz(aData[10]);
						if(aData[7] != null){
							return "<a href='/report/common/report_valuation.php?v&id=" + aData[9] + "&uid=<?php echo $myusername ?>'>" + moment_dt.format('<?php echo jsdispfd; ?>') + "</a>";
						}else{
							return "<a href='/report/common/report_valuation.php?id=" + aData[9] + "&uid=<?php echo $myusername ?>'>" + moment_dt.format('<?php echo jsdispfd; ?>') + "</a>";	
						}
					});
					$('td:eq(6)', nRow).html( function(){
						if(aData[6] != null){
							var moment_dt = moment($(this).text()).tz(aData[10]);
							return moment_dt.format('<?php echo jsdispfd; ?>');
						}
					});
					$('td:eq(8)', nRow).html( function() {	
						if(aData[8] != null){
							var moment_dt = moment($(this).text()).tz(aData[10]);
							return moment_dt.format('<?php echo jsdispfd; ?>');
						}
					});
					this.$('tr').click( function () {
               		 var href = $(this).find("a").attr("href");
					 if(href) {
						window.location = href;
					 }
           		 });
			}
		});
/////////////////////////////////////////////////////////////////////////////////////			
		pastTable = $('#pastReportsTable').dataTable({
			"bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "../common/connectors/completed_reports_connector.php",
			"bJQueryUI": true,
			"aLengthMenu": [[5, 10, 25, 50], [5, 10, 25, 50]],
			"iCookieDuration": 60*60*24*365, // 1 year
			"iDisplayLength": 5,
			"sPaginationType": "full_numbers",
			"aaSorting": [ [2,'desc'] ], 			
			"aoColumnDefs": [
                        { "bSearchable": false, "bVisible": false, "aTargets": [ 0 ] }
            ],
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
					//$('tr:eq(0)', nRow).addClass("pastReportsHelp");
					$('td:eq(0)', nRow).html( function() {	
						var moment_dt = moment($(this).text()).tz(aData[9]);
						return "<a href='/report/common/report_view.php?id=" + aData[8] + "&uid=<?php echo $myusername ?>'>" + moment_dt.format('<?php echo jsdispfd; ?>') + "</a>";
					});
					this.$('tr').click( function () {
               		 var href = $(this).find("a").attr("href");
					 if(href) {
						window.location = href;
					 }
           		 });
			},
			"fnInitComplete": function () {
           		 this.$('tr').addClass("pastReportsHelp");
       		},
			"fnServerParams": function ( aoData ) {
			  aoData.push( { "name": "oiuid", "value": "<?php echo $myusername ?>" },
			  			   { "name": "oiall", "value": "1"} ); //change to 0 for just engineer
			}
		});
/////////////////////////////////////////////////////////////////////////////////////			
	reqestsTable = $('#requestsTable').dataTable({
			"bJQueryUI": true,
			//"bStateSave": true,
			"iCookieDuration": 60*60*24*365, // 1 year
			"sPaginationType": "full_numbers",
			"aaSorting": [],
			"aLengthMenu": [[5, 10, 25, 50], [5, 10, 25, 50]],
			"iDisplayLength": 5,
			"fnInitComplete": function () {
           		 this.$('tr').click( function () {
               		 var href = $(this).find("a").attr("href");
					 if(href) {
						window.location = href;
					 }
           		 });
       		 }
		});
/////////////////////////////////////////////////////////////////////////////////////			
		openTable = $('#openReportsTable').dataTable({
			"bJQueryUI": true,
			//"bStateSave": true,
			"iCookieDuration": 60*60*24*365, // 1 year
			"sPaginationType": "full_numbers",
			"aaSorting": [],
			"aLengthMenu": [[5, 10, 25, 50], [5, 10, 25, 50]],
			"iDisplayLength": 5,
			 "fnInitComplete": function () {
           		 this.$('tr').click( function () {
               		 var href = $(this).find("a").attr("href");
					 if(href) {
						window.location = href;
					 }
           		 });
       		 }
		});

	pmTable = $('#pmTable').dataTable({
		"bJQueryUI": true,
		//"bStateSave": true,
		"iCookieDuration": 60*60*24*365, // 1 year
		"sPaginationType": "full_numbers",
		"aaSorting": [],
		"aoColumnDefs": [
			{"aTargets": [ 0 ], "bSortable": true, "sType": "date"}
		],
		"fnInitComplete": function () {
			this.$('tr').click( function () {
				var href = $(this).find("a").attr("href");
				if(href) {
					window.location = href;
				}
			});
		}
	});
/////////////////////////////////////////////////////////////////////////////////////			
//		tableCTPMdue = $('#tableCTPMdue').dataTable({
//			"bJQueryUI": true,
//			//"bStateSave": true,
//			"iCookieDuration": 60*60*24*365, // 1 year
//			"sPaginationType": "full_numbers",
//			"aaSorting": []
//		});
///////////////////////////////////////////////////////////////////////////////////////
//		tableCTPMoverdue = $('#tableCTPMoverdue').dataTable({
//			"bJQueryUI": true,
//			//"bStateSave": true,
//			"iCookieDuration": 60*60*24*365, // 1 year
//			"sPaginationType": "full_numbers",
//			"aaSorting": []
//		});
///////////////////////////////////////////////////////////////////////////////////////
//		tableMRPMdue = $('#tableMRPMdue').dataTable({
//			"bJQueryUI": true,
//			//"bStateSave": true,
//			"iCookieDuration": 60*60*24*365, // 1 year
//			"sPaginationType": "full_numbers",
//			"aaSorting": []
//		});
///////////////////////////////////////////////////////////////////////////////////////
//		tableMRPMoverdue = $('#tableMRPMoverdue').dataTable({
//			"bJQueryUI": true,
//			//"bStateSave": true,
//			"iCookieDuration": 60*60*24*365, // 1 year
//			"sPaginationType": "full_numbers",
//			"aaSorting": []
//		});
/////////////////////////////////////////////////////////////////////////////////////			
		tableContWarr = $('#tableContWarr').dataTable({
			"bJQueryUI": true,
			//"bStateSave": true,
			"iCookieDuration": 60*60*24*365, // 1 year
			"sPaginationType": "full_numbers",
			"aaSorting": []			
		});						

	$('.manageInfo').fancybox({
        //'autoScale'			: true,
		'width'				: '100%',
		'height'			: '100%',
		//'padding'			: '0',
		'transitionIn'		: 'none',
		'transitionOut'		: 'none',
		'type'				: 'inline',
        //'autoDimensions'	: true,
        'centerOnScroll'	: true
    });
	
	$("div#priceTable_filter").children("label").children("input").focus();

	$(document).keyup(function(e) {
		$("#priceTable").each(function(index, element) {
			if($(this).children("tbody").children("tr").length == 1 && e.which == 13){
				$(this).find("td > a").click();
			}
		});
		
		$("#pricedTable").each(function(index, element) {
			if($(this).children("tbody").children("tr").length == 1 && e.which == 13){
				$(this).find("td > a").click();
			}
		});
		
		$("#pastReportsTable").each(function(index, element) {
			if($(this).children("tbody").children("tr").length == 1 && e.which == 13){
				$(this).find("td > a").click();
			}
		});

		$("#requestsTable").each(function(index, element) {
			if($(this).children("tbody").children("tr").length == 1 && e.which == 13){
				$(this).find("td > a").click();
			}
		});
		
	});
});
</script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
  
  function drawChart() {
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	// CT PMs Chart
//	var dataCTPM = new google.visualization.DataTable();
//	dataCTPM.addColumn('string', 'Desc');
//	dataCTPM.addColumn('number', 'Total');
//	dataCTPM.addRows([
//	  ['Up-to-date', <?php //echo $ct_pm_current; ?>//],
//	  ['Due', <?php //echo $ct_pm_due; ?>//],
//	  ['Overdue', <?php //echo $ct_pm_overdue; ?>//]
//	]);
//	var options = {
//		title:'CT Preventive Maintenance Snapshot',
//		width:490,
//		height:300
//	};
//	var chartCTPM = new google.visualization.PieChart(document.getElementById('chart_ct_pm'));
//
//	google.visualization.events.addListener(chartCTPM, 'select', clickHandlerChartCTPM);
//
//	chartCTPM.draw(dataCTPM, options);
//
//	function clickHandlerChartCTPM(){
//		selectedData = chartCTPM.getSelection();
//		if (selectedData.length >= 1){
//			row = selectedData[0].row;
//			item = dataCTPM.getValue(row,1);
//			console.log(row +" "+ item);
//			switch(row){
//				case 0:
//					//$('#aCTPMuptodate').click();
//					break;
//				case 1:
//					$('#aCTPMdue').click();
//					break;
//				case 2:
//					$('#aCTPMoverdue').click();
//					break;
//			}
//		}
//	}
//
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	// MR PMs Chart
//	var dataMRPM = new google.visualization.DataTable();
//	dataMRPM.addColumn('string', 'Desc');
//	dataMRPM.addColumn('number', 'Total');
//	dataMRPM.addRows([
//	  ['Up-to-date', <?php //echo $mr_pm_current; ?>//],
//	  ['Due', <?php //echo $mr_pm_due; ?>//],
//	  ['Overdue', <?php //echo $mr_pm_overdue; ?>//]
//	]);
//	var options = {
//		title:'MR Preventive Maintenance Snapshot',
//		width:490,
//		height:300
//	};
//	var chartMRPM = new google.visualization.PieChart(document.getElementById('chart_mr_pm'));
//
//	google.visualization.events.addListener(chartMRPM, 'select', clickHandlerChartMRPM);
//
//	chartMRPM.draw(dataMRPM, options);
//
//	function clickHandlerChartMRPM(){
//		selectedData = chartMRPM.getSelection();
//		if (selectedData.length >= 1){
//			row = selectedData[0].row;
//			item = dataMRPM.getValue(row,1);
//			console.log(row +" "+ item);
//			switch(row){
//				case 0:
//					//$('#aCTPMuptodate').click();
//					break;
//				case 1:
//					$('#aMRPMdue').click();
//					break;
//				case 2:
//					$('#aMRPMoverdue').click();
//					break;
//			}
//		}
//	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	// Contracts Totals Chart
	var dataCont = google.visualization.arrayToDataTable([
	  <?php echo $contractsType.",".$contractsCount; ?>
	]);
	var options = {
		hAxis: {title: ""},
		chartArea:{width:'50%'},
		//bar:{groupWidth:'40%'},
		title: 'Contracts Snapshot',
		width:490,
		height:300
	};
	var chartCont = new google.visualization.ColumnChart(document.getElementById('chart_contracts'));

	google.visualization.events.addListener(chartCont, 'select', clickHandlerChartCont);

	chartCont.draw(dataCont, options);
			
	function clickHandlerChartCont(){
		selectedData = chartCont.getSelection();
		if (selectedData.length >= 1){
			col = selectedData[0].column;
			item = dataCont.getValue(0,col);
			console.log(col +" "+ item);
		}
	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	// Warrenty and Contract expire Chart 30 Days
	var data30 = google.visualization.arrayToDataTable([
	  ['','CT Contracts', 'CT Warranties', 'MR Contracts', 'MR Warranties'],
	  ['',<?php echo $ct_contracts_expire30.",".$ct_warranties_expire30.",".$mr_contracts_expire30.",".$mr_warranties_expire30; ?>]
	]);
	var options = {
		hAxis: {title: ""},
		chartArea:{width:'50%'},
		//bar:{groupWidth:'40%'},
		title: 'Contracts and Warranties Expiring Within 30 Days Snapshot',
		width:490,
		height:300
	};
		
	var chart30 = new google.visualization.ColumnChart(document.getElementById('chart_contracts_expire30'));	
	
	google.visualization.events.addListener(chart30, 'select', clickHandlerChart30);
	
	chart30.draw(data30, options);
	
	function clickHandlerChart30(){
		selectedData = chart30.getSelection();
		if (selectedData.length >= 1){
			console.log(selectedData);
			col = selectedData[0].column;
			item = data30.getValue(0,col);
			console.log(col +" "+ item);
			tableContWarr.fnClearTable();
			switch(col){
				case 1:
					chart30rows1();
					break;
				case 2:
					chart30rows2();
					break;
				case 3:
					chart30rows3();
					break;
				case 4:	
					chart30rows4();
					break;
			}
			
			$('#aContWarr').click();
		}
	}
	
	function chart30rows1() {
			<?php
			while($rowData = $resultCTContractExpire30->fetch_assoc()){
				echo "tableContWarr.fnAddData(['".$rowData['system_id']."','".$rowData['nickname']."','".date(phpdispfd,strtotime($rowData['contract_end_date']))."']);\n";
			}
			?>
	}
	function chart30rows2() {
			<?php
			while($rowData = $resultCTWarrentyExpire30->fetch_assoc()){
				echo "tableContWarr.fnAddData(['".$rowData['system_id']."','".$rowData['nickname']."','".date(phpdispfd,strtotime($rowData['contract_end_date']))."']);\n";
			}
			?>
	}
	function chart30rows3() {
			<?php
			while($rowData = $resultMRContractExpire30->fetch_assoc()){
				echo "tableContWarr.fnAddData(['".$rowData['system_id']."','".$rowData['nickname']."','".date(phpdispfd,strtotime($rowData['contract_end_date']))."']);\n";
			}
			?>
	}
	function chart30rows4() {
			<?php
			while($rowData = $resultMRWarrentyExpire30->fetch_assoc()){
				echo "tableContWarr.fnAddData(['".$rowData['system_id']."','".$rowData['nickname']."','".date(phpdispfd,strtotime($rowData['contract_end_date']))."']);\n";
			}
			?>
	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	// Warrenty and Contract expire Chart 120 Days
	var data120 = google.visualization.arrayToDataTable([
	  ['','CT Contracts', 'CT Warranties', 'MR Contracts', 'MR Warranties'],
	  ['',<?php echo $ct_contracts_expire120.",".$ct_warranties_expire120.",".$mr_contracts_expire120.",".$mr_warranties_expire120; ?>]
	]);

	var options = {
		hAxis: {title: ""},
		chartArea:{width:'50%'},
		//bar:{groupWidth:'40%'},
		title: 'Contracts and Warranties Expiring Within 120 Days Snapshot',
		width:490,
		height:300
	};
	var chart120 = new google.visualization.ColumnChart(document.getElementById('chart_contracts_expire120'));
	
	google.visualization.events.addListener(chart120, 'select', clickHandlerChart120);
	
	chart120.draw(data120, options);
	
	function clickHandlerChart120(){
		selectedData = chart120.getSelection();
		if (selectedData.length >= 1){
			col = selectedData[0].column;
			item = data120.getValue(0,col);
			console.log(col +" "+ item);
			tableContWarr.fnClearTable();
			switch(col){
				case 1:
					chart120rows1();
					break;
				case 2:
					chart120rows2();
					break;
				case 3:
					chart120rows3();
					break;
				case 4:	
					chart120rows4();
					break;
			}
			
			$('#aContWarr').click();
		}
	}
	
	function chart120rows1() {
			<?php
			while($rowData = $resultCTContractExpire120->fetch_assoc()){
				echo "tableContWarr.fnAddData(['".$rowData['system_id']."','".$rowData['nickname']."','".date(phpdispfd,strtotime($rowData['contract_end_date']))."']);\n";
			}
			?>
	}
	function chart120rows2() {
			<?php
			while($rowData = $resultCTWarrentyExpire120->fetch_assoc()){
				echo "tableContWarr.fnAddData(['".$rowData['system_id']."','".$rowData['nickname']."','".date(phpdispfd,strtotime($rowData['contract_end_date']))."']);\n";
			}
			?>
	}
	function chart120rows3() {
			<?php
			while($rowData = $resultMRContractExpire120->fetch_assoc()){
				echo "tableContWarr.fnAddData(['".$rowData['system_id']."','".$rowData['nickname']."','".date(phpdispfd,strtotime($rowData['contract_end_date']))."']);\n";
			}
			?>
	}
	function chart120rows4() {
			<?php
			while($rowData = $resultMRWarrentyExpire120->fetch_assoc()){
				echo "tableContWarr.fnAddData(['".$rowData['system_id']."','".$rowData['nickname']."','".date(phpdispfd,strtotime($rowData['contract_end_date']))."']);\n";
			}
			?>
	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	// Initial Response Time Chart
	var dataResp = google.visualization.arrayToDataTable([
	  ['','Response Time'],
	  ['',<?php echo $response_time; ?>]
	]);
	var options = {
		hAxis: {title: ""},
		vAxis: {maxValue: 100, minValue: 0},
		chartArea:{width:'50%'},
		bar:{groupWidth:'15%'},
		title: 'Initial Response Time Percentage (Under 30 Minutes) Snapshot',
		width:490,
		height:300
	};
	var chartResp = new google.visualization.ColumnChart(document.getElementById('chart_response'));
	
	google.visualization.events.addListener(chartResp, 'select', clickHandlerChartResp);
	
	chartResp.draw(dataResp, options);
	
	function clickHandlerChartResp(){
		selectedData = chartResp.getSelection();
		if (selectedData.length >= 1){
			col = selectedData[0].column;
			item = dataResp.getValue(0,col);
			console.log(col +" "+ item);
		}
	}
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Load the Visualization API and the piechart package.
google.load('visualization', '1.0', {'packages':['corechart']});
// Set a callback to run when the Google Visualization API is loaded.
google.setOnLoadCallback(drawChart);
</script>
</head>
<body>
<?php require_once($header_include); ?>
<div id="OIReportContent">
        <div id="ReportFrame">
		<div id="ReportOuterFrame">
	        <?php  
				$sql = "SELECT * FROM systems_reports WHERE `status`='closed' AND invoice_required='y' AND valuation_complete='n' AND deleted = 'n' ORDER BY `id` ASC";
				if(!$resultPrice = $mysqli->query($sql)){
					$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
					$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
					$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
				}
			?>  
            <div id="ReportFrame">     
            <div id="ReportFrameTitle"> Reports awaiting valuation </div>
            <div id="priceDiv" <?php if($resultPrice->num_rows == 0){echo "style=\"display:none\"";} ?>>
                <table width="100%" id="priceTable" >
                    <thead>
                        <tr>
                            <th width="75px">Date</th>
                            <th width="50px">Report ID</th>
                            <th width="50px">System ID</th>
                            <th>System Name</th>
                            <th>Engineer</th>
                            <th>Complaint</th>                   
                        </tr>
                    </thead>
                    <tbody>
                    	<?php
						if($resultPrice){
							while($rowPrice = $resultPrice->fetch_assoc())
							{
								echo "<tr>\n";
								echo "<td><a href=\"/report/common/report_valuation.php?id=".$rowPrice['unique_id'].'&uid='.$myusername."\">". date(phpdispfd,strtotime($rowPrice['date']))."</a></td>\n";
								echo "<td>". $rowPrice['report_id']."</td>\n";
								echo "<td>". $rowPrice['system_id']."</td>\n";
								echo "<td>". $rowPrice['system_nickname']."</td>\n";
								echo "<td>". $arr_engineers[$rowPrice['assigned_engineer']]."</td>\n";
								echo "<td>". limit_words($rowPrice['complaint'],$comp_word_limt)."</td>\n";
								echo "</tr>\n";
							}
						}
						?>                        
                    </tbody>
                </table>
            </div>
            <div id="priceDiv" <?php if($resultPrice->num_rows != 0){echo "style=\"display:none\"";}else{echo "style=\"text-align:center\"";} ?>>
            	<br />
                <h2>No reports waiting for valuation</h2>
            </div>
          </div>
        </div>
<div class="line"></div>
		 <div id="ReportFrame">
		<div id="ReportOuterFrame">
            <div id="ReportFrame">     
            <div id="ReportFrameTitle"> Valuated Reports</div>
            <div id="pricedDiv">
                <table width="100%" id="pricedTable" >
                    <thead>
                        <tr>
                            <th width="75px">Date</th>
                            <th width="50px">Report ID</th>
                            <th width="50px">System ID</th>
                            <th>System Name</th>
                            <th>Engineer</th>  
							<th>Valuated By</th>      
							<th width="75px">Valuation Date</th>           
							<th>Invoiced By</th>
							<th width="75px">Invoiced Date</th>
                        </tr>
                    </thead>
                    <tbody>                    
                    </tbody>
                </table>
            </div>
          </div>
        </div>
		<div class="line"></div>
			<table width="100%">
				<tr>
					<td align="center"><div id="chart_ct_pm"></div></td>
					
					<td align="center"><div id="chart_mr_pm"></div></td>
				</tr>
				<tr>
					<td align="center"><div id="chart_contracts_expire30"></div></td>
					<td align="center"><div id="chart_contracts_expire120"></div></td>
				</tr>
				<tr>
					<td align="center"><div id="chart_contracts"></div></td>
					<td align="center"><div id="chart_response"></div></td>
				</tr>
			</table>
		<div class="line"></div>
        <div id="ReportFrameTitle">Completed Reports</div>
        <div id="pastReprotsDiv">
            <table width="100%" id="pastReportsTable" >
                <thead>
                    <tr>
                    	<th width="1px">idx</th>
                        <th width="75px">Date</th>
                        <th width="50px">Report ID</th>
                        <th width="50px">System ID</th>
                        <th width="100px">System Name</th>
                        <th width="75px">Engineer</th>
                        <th width="150px">Complaint</th>
                        <th>Service</th>
                    </tr>
                </thead>
                <tbody>    
                </tbody>
            </table>
        </div>
    </div>
<div class="line"></div>
	<div id="ReportOuterFrame">
	        <?php  
				$sql = "SELECT r.*, sbc.credit_hold, sbc.pre_paid, sbc.property, sb.status AS cur_system_status
						FROM systems_requests AS r
						LEFT JOIN systems_base_cont AS sbc ON sbc.ver_unique_id = r.system_ver_unique_id
						LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
						WHERE r.`status`='open' AND r.deleted = 'n'
						ORDER BY r.id ASC;";
				if(!$resultRequests = $mysqli->query($sql)){
					$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
					$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
					$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
				}
			?>  
            <div id="ReportFrame">     
            <div id="ReportFrameTitle"> Open Service Requests </div>
            <div id="requestsDiv" <?php if($resultRequests->num_rows == 0){echo "style=\"display:none\"";} ?>>
	            <div class="legend">Color Legend:&nbsp;<span class="credit_hold">Credit Hold</span>&emsp;<span class="pre_paid">Pre-Paid</span>&emsp;<span class="future">Future</span></div>
                <table width="100%" id="requestsTable" >
                    <thead>
                        <tr>
                        	<th width="75px">Date</th> 
                        	<th width="50px">Request Number</th>                          
                            <th width="50px">System ID</th>
                            <th>System Name</th>
                            <th>Problem</th>
                            <th>Engineer</th>
                            <th>System Status</th>
                            <th>Report Due Days</th>
	                        <th>PM</th>
	                        <th>PM Sched</th>
                        </tr>
                    </thead>
                    <tbody>
                    	<?php
						if($resultRequests->num_rows!=0){
							$new_now = $now->cloning();
							$new_now->setImmutableMode(false);
							$new_now->subtractDays(5);
							$new_now->subtractHours(12);
							$new_now->setImmutableMode(true);
							while($rowRequest = $resultRequests->fetch_assoc())
							{
								$onsite_date = new Moment\Moment($rowRequest['onsite_date']);
								$days_left = round($new_now->from($onsite_date)->getDays(), 1);
								//$days_left =

								if(strtolower($rowRequest['property']) == 'f'){
									echo "<tr class=\"future\">\n";
								}elseif(strtolower($rowRequest['credit_hold']) == "y"){
									echo "<tr class=\"credit_hold\">\n";
								}elseif(strtolower($rowRequest['pre_paid']) == "y"){
									echo "<tr class=\"pre_paid\">\n";
								}else{
									echo "<tr>\n";
								}
								if(strtolower($rowRequest['report_started']) != 'y'){
									echo "<td><a href='/report/common/request_new_edit.php?e&ver_unique_id=".$rowRequest['system_ver_unique_id']."&version=".$rowRequest['system_ver']."&unique_id=".$rowRequest['system_unique_id']."&user_id=$myusername&req_id=".$rowRequest['unique_id']."'>". date(phpdispfd,strtotime($rowRequest['request_date']))."</a></td>\n";
								}else{
									echo "<td><a style=\"color:#960\" onClick=\"alert('Cannot edit this request because a service report has been started for this request.')\">". date(phpdispfd,strtotime($rowRequest['request_date']))."</a></td>\n";
								}
								echo "<td>". $rowRequest['request_num']."</td>\n";
								echo "<td>". $rowRequest['system_id']."</td>\n";
								echo "<td>". $rowRequest['system_nickname']."</td>\n";
								echo "<td>". limit_words($rowRequest['problem_reported'],$probrpt_word_limt)."</td>\n";
								echo "<td>". $arr_engineers[$rowRequest['engineer']]."</td>\n";
								echo "<td>". $arr_status[$rowRequest['cur_system_status']]."</td>\n";
								echo "<td"; if($days_left < 0 ){echo " class='credit_hold'";}  echo ">". $days_left . "</td>\n";
								echo "<td>"; if(strtolower($rowRequest['pm'])=="y"){echo "Yes";} echo "</td>\n";
								if(strtolower($rowRequest['pm'])=="y"){
									if($rowRequest['pm_date'] == ""){
										echo "<td>Contact Dispatch</td>\n";
									}else{
										echo "<td>" . implode('<br>', explode('T', $rowRequest['pm_date'])) . "</td>\n";
									}
								}else{
									echo "<td>&nbsp;</td>\n";
								}
								echo "</tr>\n";
								unset($onsite_date);
								unset($days_left);
							}
						}
						?>                        
                    </tbody>
                </table>
            </div>
            <div id="requestDiv" <?php if($resultRequests->num_rows != 0){echo "style=\"display:none\"";}else{echo "style=\"text-align:center\"";} ?>>
            	<br />
            	<h2>No Service Requests</h2>
            </div>
          </div>
        </div>
		<div class="line"></div>
        <div id="ReportOuterFrame">
		        <?php
		        $sql = "SELECT r.*, sbc.credit_hold, sbc.pre_paid, sbc.property
						FROM systems_reports AS r
						LEFT JOIN systems_base_cont AS sbc ON sbc.ver_unique_id = r.system_ver_unique_id
						WHERE r.`status`='open' AND r.deleted = 'n'
						ORDER BY r.`id` ASC;";
		        if(!$resultOpen = $mysqli->query($sql)){
			        $log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			        $log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			        $log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
		        }
		        ?>
		        <div id="ReportFrame">
			        <div id="ReportFrameTitle"> Unfinished Reports </div>
			        <div id="openReprotsDiv" <?php if($resultOpen->num_rows == 0){echo "style=\"display:none\"";} ?>>
				        <table width="100%" id="openReportsTable" >
					        <thead>
					        <tr>
						        <th width="75px">Date Started</th>
						        <th width="50px">Report ID</th>
						        <th width="50px">System ID</th>
						        <th>System Name</th>
						        <th>Engineer</th>
						        <th>Complaint</th>
					        </tr>
					        </thead>
					        <tbody>
					        <?php
					        if($resultOpen){
						        while($rowOpen = $resultOpen->fetch_assoc())
						        {

							        if(strtolower($rowOpen['property']) == 'f'){
								        echo "<tr class=\"future\">\n";
							        }elseif(strtolower($rowOpen['credit_hold']) == "y"){
								        echo "<tr class=\"credit_hold\">\n";
							        }elseif(strtolower($rowOpen['pre_paid']) == "y"){
								        echo "<tr class=\"pre_paid\">\n";
							        }else{
								        echo "<tr>\n";
							        }
							        echo "<td><a href=\"/report/common/report_view.php?view&id=".$rowOpen['unique_id'].'&uid='.$myusername."\">". date(phpdispfd,strtotime($rowOpen['date']))."</a></td>\n";
							        echo "<td>". $rowOpen['report_id']."</td>\n";
							        echo "<td>". $rowOpen['system_id']."</td>\n";
							        echo "<td>". $rowOpen['system_nickname']."</td>\n";
							        echo "<td>". $arr_engineers[$rowOpen['assigned_engineer']]."</td>\n";
							        echo "<td>". limit_words($rowOpen['complaint'],$comp_word_limt)."</td>\n";
							        echo "</tr>\n";
						        }
					        }
					        ?>
					        </tbody>
				        </table>
			        </div>
			        <div id="openReportsDiv" <?php if($resultOpen->num_rows != 0){echo "style=\"display:none\"";}else{echo "style=\"text-align:center\"";} ?>>
				        <br />
				        <h2>No Unfinished Reports for <?php echo $rowUser['name'];?></h2>
			        </div>
		        </div>
	        </div>

	        <div id="ReportFrame">
		        <div id="ReportFrameTitle">PMs Due</div>

		        <div id="pmDiv" <?php if($resultPM->num_rows == 0){echo "style=\"display:none\"";} ?>>
			        <div class="legend">Showing next <?php echo $settings->pm->look_ahead; ?> days</div>
			        <div class="legend">Color Legend:&nbsp;<span class="credit_hold">Credit Hold</span>&emsp;<span class="pre_paid">Pre-Paid</span>&emsp;<span class="future">Future</span>&emsp;<span class="late">PM Late <span class="smaller">( > <?php echo $settings->pm->late; ?> days )</span></span>&emsp;<span class="overdue">PM Overdue <span class="smaller">( > <?php echo $settings->pm->overdue; ?> days )</span></span></div>
			        <table width="100%" id="pmTable" >
				        <thead>
				        <tr>
				        <th width="75px">Due Date</th>
				        <th>Last PM</th>
				        <th>System ID</th>
				        <th width="40%">System Nickname</th>
				        <th>Engineer</th>
				        <th>Scheduled</th>
				        <th>Request ID</th>
				        </tr>
				        </thead>
				        <tbody>
				        <?php
				        $pm_systems = array();
				        $look_ahead = $now->addDays($settings->pm->look_ahead);
				        while($rowPM = $resultPM->fetch_assoc()){
					        $last_pm = new \Moment\Moment($rowPM['last_pm']);
					        if($last_pm <= $now){
						        $pm_dates = json_decode($rowPM['pm_dates']);

						        $pmd_array = array();
						        foreach($pm_dates as $key=>$val){
							        $pm_date_init = new \Moment\Moment($now->getYear() . '-' . sprintf('%02d', $val[0]) . '-' . sprintf('%02d', $val[1]), null, true);
							        $pm_date_init_prev = $pm_date_init->subtractYears(1);
							        $pm_date_init_next = $pm_date_init->addYears(1);

							        $start = $pm_date_init_prev->subtractMonths(intval($rowPM['pm_freq']))->addDays(31)->format('Y-m-d');
							        $end = $pm_date_init_prev->addDays(30)->format('Y-m-d');
							        $pmd_array[$pm_date_init_prev->format('Y-m-d')] = array('start' => $start, 'end' => $end);

							        $start = $pm_date_init->subtractMonths(intval($rowPM['pm_freq']))->addDays(31)->format('Y-m-d');
							        $end = $pm_date_init->addDays(30)->format('Y-m-d');
							        $pmd_array[$pm_date_init->format('Y-m-d')] = array('start' => $start, 'end' => $end);

							        $start = $pm_date_init_next->subtractMonths(intval($rowPM['pm_freq']))->addDays(31)->format('Y-m-d');
							        $end = $pm_date_init_next->addDays(30)->format('Y-m-d');
							        $pmd_array[$pm_date_init_next->format('Y-m-d')] = array('start' => $start, 'end' => $end);
						        }
						        ksort($pmd_array);

						        $next_pm = false;
						        $keys = array_keys($pmd_array);
						        foreach($pmd_array as $key=>$val){
							        if($last_pm->isBetween(new \Moment\Moment($val['start']), new \Moment\Moment($val['end']))){
								        $next_pm = $key;
								        break;
							        }
						        }
						        $key = array_search($next_pm, $keys);
						        $next_pm = $keys[$key + 1];
						        $next_pm_moment = new Moment\Moment($next_pm);

						        if($rowPM['request_num'] != "" and $rowPM['pm_date'] == ""){
							        $scheduled = "Contact Dispatch";
						        }else{
							        $scheduled = $rowPM['pm_date'];
						        }

						        //Limit to next X days look ahead
						        if($next_pm_moment <= $look_ahead){
							        $pm_systems[$rowPM['system_id']] = array(
								        'unique_id'=>$rowPM['unique_id'],
								        'system_id'=>$rowPM['system_id'],
								        'nickname'=>$rowPM['nickname'],
								        'last_pm' => $last_pm->format('Y-m-d'),
								        'engineer'=>$rowPM['engineer'],
								        'ver'=>$rowPM['ver'],
								        'credit_hold'=>$rowPM['credit_hold'],
								        'pre_paid'=>$rowPM['pre_paid'],
								        'property'=>$rowPM['property'],
								        'next_pm' => $next_pm,
								        'request' => $rowPM['request_num'],
								        'sched' => $scheduled
							        );
						        }

					        }
				        }
				        $php_utils->customUasort($pm_systems, 'next_pm');
				        //d($pm_systems);
				        foreach($pm_systems as $key=>$val){

					        if(strtolower($val['property']) == 'f'){
						        echo "<tr class=\"future\">\n";
					        }elseif(strtotime($val['next_pm'].' + '.$settings->pm->overdue.' days') < time()){
						        echo "<tr class=\"overdue\">\n";
					        }elseif(strtotime($val['next_pm'].' + '.$settings->pm->late.' days') < time()){
						        echo "<tr class=\"late\">\n";
					        }elseif(strtolower($val['credit_hold']) == "y"){
						        echo "<tr class=\"credit_hold\">\n";
					        }elseif(strtolower($val['pre_paid']) == "y"){
						        echo "<tr class=\"pre_paid\">\n";
					        }else{
						        echo "<tr>\n";
					        }
					        echo "<td><a href=\"/report/common/systems_view.php?unique_id=" . $val['unique_id'] . "&user_id=" . $myusername . "&version=" . $val['ver'] . "&pm\">" . $val['next_pm'] . "</a></td>\n";
					        echo "<td>" . date(phpdispfd, strtotime($val['last_pm'])) . "</td>\n";
					        echo "<td>" . $val['system_id'] . "</td>\n";
					        echo "<td>" . $val['nickname'] . "</td>\n";
					        echo "<td>" . $val['engineer'] . "</td>\n";
					        echo "<td>" . $val['sched'] . "</td>\n";
					        echo "<td>" . $val['request'] . "</td>\n";
					        echo "</tr>\n";
				        }

				        ?>
				        </tbody>
			        </table>
		        </div>
		        <div id="pmDiv" <?php if($resultPM->num_rows != 0){echo "style=\"display:none\"";}else{echo "style=\"text-align:center\"";} ?>>
			        <br />
			        <h2>No PMs Due</h2>
		        </div>
	        </div>
</div>
<div style="display:none">
<!--	<a id="aCTPMoverdue" class="manageInfo" href="#divCTPMoverdue">CTPM</a>-->
<!--	<a id="aCTPMdue" class="manageInfo" href="#divCTPMdue">CTPM</a>-->
<!--	<a id="aMRPMoverdue" class="manageInfo" href="#divMRPMoverdue">CTPM</a>-->
<!--	<a id="aMRPMdue" class="manageInfo" href="#divMRPMdue">CTPM</a>-->
	
	<a id="aContWarr" class="manageInfo" href="#divContWarr">CTPM</a>
	
<!--	<div id="divCTPMdue">-->
<!--		<table width="100%" id="tableCTPMdue">-->
<!--			<thead>                    -->
<!--			<th width="75px">Due Date</th>-->
<!--					<th>Last PM</th>-->
<!--					<th>System ID</th>-->
<!--					<th width="40%">System Name</th>-->
<!--					<th>Engineer</th>-->
<!--										-->
<!--			</thead>-->
<!--			<tbody>-->
<!--				--><?php
//					while($rowCTPMdue = $resultCTPMdue->fetch_assoc())
//					{
//						if(strtolower($rowCTPMdue['credit_hold']) == "y"){
//								echo "<tr style=\"color:red\">\n";
//							}else{
//								echo "<tr>\n";
//							}
//						if(strtolower($rowUser['make_request']) == "y"){
//							echo "<td><a href=\"/report/common/request_new_edit.php?system_id=".$rowCTPMdue['system_id']."&user_id=".$myusername."&pm\">". date(phpdispfd,strtotime($rowCTPMdue['due']))."</a></td>\n";
//						}else{
//							echo "<td><a onClick=\"alert('Not Finished Yet');\">". date(phpdispfd,strtotime($rowCTPMdue['due']))."</a></td>\n";
//						}
//						echo "<td>". date(phpdispfd,strtotime($rowCTPMdue['last_pm']))."</td>\n";
//						echo "<td>". $rowCTPMdue['system_id']."</td>\n";
//						echo "<td>". $rowCTPMdue['nickname']."</td>\n";
//						echo "<td>". $rowCTPMdue['name']."</td>\n";
//						echo "</tr>\n";
//					}
//					?><!--     					-->
<!--			</tbody>-->
<!--		</table>-->
<!--	</div>-->
<!--	<div id="divCTPMoverdue">-->
<!--		<table width="100%" id="tableCTPMoverdue">-->
<!--			<thead>                    -->
<!--			<th width="75px">Due Date</th>-->
<!--					<th>Last PM</th>-->
<!--					<th>System ID</th>-->
<!--					<th width="40%">System Name</th>-->
<!--					<th>Engineer</th>-->
<!--										-->
<!--			</thead>-->
<!--			<tbody>-->
<!--				--><?php
//					while($rowCTPMoverdue = $resultCTPMoverdue->fetch_assoc())
//					{
//						if(strtolower($rowCTPMoverdue['credit_hold']) == "y"){
//								echo "<tr style=\"color:red\">\n";
//							}else{
//								echo "<tr>\n";
//							}
//						if(strtolower($rowUser['make_request']) == "y"){
//							echo "<td><a href=\"/report/common/request_new_edit.php?system_id=".$rowCTPMoverdue['system_id']."&user_id=".$myusername."&pm\">". date(phpdispfd,strtotime($rowCTPMoverdue['due']))."</a></td>\n";
//						}else{
//							echo "<td><a onClick=\"alert('Not Finished Yet');\">". date(phpdispfd,strtotime($rowCTPMoverdue['due']))."</a></td>\n";
//						}
//						echo "<td>". date(phpdispfd,strtotime($rowCTPMoverdue['last_pm']))."</td>\n";
//						echo "<td>". $rowCTPMoverdue['system_id']."</td>\n";
//						echo "<td>". $rowCTPMoverdue['nickname']."</td>\n";
//						echo "<td>". $rowCTPMoverdue['name']."</td>\n";
//						echo "</tr>\n";
//					}
//					?><!--     					-->
<!--			</tbody>-->
<!--		</table>-->
<!--	</div>-->
<!--	-->
<!--	<div id="divMRPMdue">-->
<!--		<table width="100%" id="tableMRPMdue">-->
<!--			<thead>                    -->
<!--			<th width="75px">Due Date</th>-->
<!--					<th>Last PM</th>-->
<!--					<th>System ID</th>-->
<!--					<th width="40%">System Name</th>-->
<!--					<th>Engineer</th>-->
<!--										-->
<!--			</thead>-->
<!--			<tbody>-->
<!--				--><?php
//					while($rowMRPMdue = $resultMRPMdue->fetch_assoc())
//					{
//						if(strtolower($rowMRPMdue['credit_hold']) == "y"){
//								echo "<tr style=\"color:red\">\n";
//							}else{
//								echo "<tr>\n";
//							}
//						if(strtolower($rowUser['make_request']) == "y"){
//							echo "<td><a href=\"/report/common/request_new_edit.php?system_id=".$rowMRPMdue['system_id']."&user_id=".$myusername."&pm\">". date(phpdispfd,strtotime($rowMRPMdue['due']))."</a></td>\n";
//						}else{
//							echo "<td><a onClick=\"alert('Not Finished Yet');\">". date(phpdispfd,strtotime($rowMRPMdue['due']))."</a></td>\n";
//						}
//						echo "<td>". date(phpdispfd,strtotime($rowMRPMdue['last_pm']))."</td>\n";
//						echo "<td>". $rowMRPMdue['system_id']."</td>\n";
//						echo "<td>". $rowMRPMdue['nickname']."</td>\n";
//						echo "<td>". $rowMRPMdue['name']."</td>\n";
//						echo "</tr>\n";
//					}
//					?><!--     					-->
<!--			</tbody>-->
<!--		</table>-->
<!--	</div>-->
<!--	<div id="divMRPMoverdue">-->
<!--		<table width="100%" id="tableMRPMoverdue">-->
<!--			<thead>                    -->
<!--			<th width="75px">Due Date</th>-->
<!--					<th>Last PM</th>-->
<!--					<th>System ID</th>-->
<!--					<th width="40%">System Name</th>-->
<!--					<th>Engineer</th>-->
<!--										-->
<!--			</thead>-->
<!--			<tbody>-->
<!--				--><?php
//					while($rowMRPMoverdue = $resultMRPMoverdue->fetch_assoc())
//					{
//						if(strtolower($rowMRPMoverdue['credit_hold']) == "y"){
//								echo "<tr style=\"color:red\">\n";
//							}else{
//								echo "<tr>\n";
//							}
//						if(strtolower($rowUser['make_request']) == "y"){
//							echo "<td><a href=\"/report/common/request_new_edit.php?system_id=".$rowMRPMoverdue['system_id']."&user_id=".$myusername."&pm\">". date(phpdispfd,strtotime($rowMRPMoverdue['due']))."</a></td>\n";
//						}else{
//							echo "<td><a onClick=\"alert('Not Finished Yet');\">". date(phpdispfd,strtotime($rowMRPMoverdue['due']))."</a></td>\n";
//						}
//						echo "<td>". date(phpdispfd,strtotime($rowMRPMoverdue['last_pm']))."</td>\n";
//						echo "<td>". $rowMRPMoverdue['system_id']."</td>\n";
//						echo "<td>". $rowMRPMoverdue['nickname']."</td>\n";
//						echo "<td>". $rowMRPMoverdue['name']."</td>\n";
//						echo "</tr>\n";
//					}
//					?><!--     					-->
<!--			</tbody>-->
<!--		</table>-->
<!--	</div>-->
	
	<div id="divContWarr">
		<table width="100%" id="tableContWarr">
			<thead>                    
				<th>System ID</th>
				<th>System Name</th>
				<th>Expire</th>
										
			</thead>
			<tbody>						
			</tbody>
		</table>
	</div>

</div>

<?php require_once($footer_include); ?>