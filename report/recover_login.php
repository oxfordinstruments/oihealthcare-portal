<?php
//Update Completed 12/12/14
$send = true;

$remove_session = true;
$no_redirect = true;
$use_default_includes = true;

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

if($settings->disable_email == '1'){
	$send = false;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/smarty/libs/Smarty.class.php');
$smarty = new Smarty();
$smarty->setTemplateDir($_SERVER['DOCUMENT_ROOT'].$settings->email_templates);
$smarty->setCompileDir($_SERVER['DOCUMENT_ROOT'].$settings->email_template_c);
$smarty->setCacheDir($_SERVER['DOCUMENT_ROOT'].$settings->email_cache);
$smarty->setConfigDir($_SERVER['DOCUMENT_ROOT'].$settings->email_configs);
$smarty->use_sub_dirs = true;
$smarty->force_compile = true;
//$smarty->debugging = true;
$smarty->caching = false;

process_si_contact_form();
?>

<!DOCTYPE html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>

<style type="text/css">
<!--
.error {
	color: #f00;
	font-weight: bold;
	font-size: 1.2em;
}
.success {
	color: #00f;
	font-weight: bold;
	font-size: 1.2em;
}
fieldset {
	width: 90%;
}
legend {
	font-size: 24px;
}
.note {
	font-size: 18px;
}
-->
</style>

<?php require_once($js_include);?>

<script src="/resources/js/jquery.badBrowser.js"></script>
<script type="text/javascript">
function highlight(){
	document.getElementById("ct_name").focus();
	<?php
	if(isset($_SESSION['pwdresetform']['success']) && $_SESSION['pwdresetform']['success'] == true){
	?>
	document.getElementById("ct_email").value = '';
	document.getElementById("ct_name").value = '';
	$("#pwdresetdiv").hide();
	<?php
	}
	?>
}
function submitcheck(){
	document.forms["form"].submit();
}
</script>
</head>
<body onLoad="highlight()">
<?php require_once($header_include); ?>
<div class="outerContainer" id="content">
	<div style="width:100%; margin-top:20px; text-align:center;  margin-bottom:20px;">
		<h1 style="color:#F79447">Reset your password</h1>
	</div>
	<div style="width:500px; margin-left:auto; margin-right:auto; color:#1B2673; padding-top:15px; padding-bottom:10px">
		
			<div style="text-align:center">
				<?php
			//process_si_contact_form(); // Process the form, if it was submitted		
			if (isset($_SESSION['pwdresetform']['error']) &&  $_SESSION['pwdresetform']['error'] == true): /* The last form submission had 1 or more errors */ ?>
				<span class="error">There was a problem with your submission.  Errors are displayed below in red.</span><br />
				<br />
				<?php elseif (isset($_SESSION['pwdresetform']['success']) && $_SESSION['pwdresetform']['success'] == true): /* form was processed successfully */ ?>
				<span class="success">Thank you. A password reset email has been sent!<br>
				Please check your email assoiciated with the OiHeathcare Portal<br>
				<a href="/" target="_self">Click here to return to the main page.</a></span><br />
				<br />
				<?php endif; ?>
			</div>
		<div id="pwdresetdiv">
			<form id="form" name="form" method="post" action="<?php echo htmlspecialchars($_SERVER['REQUEST_URI'] . $_SERVER['QUERY_STRING']) ?>">
				<input type="hidden" name="do" value="contact" />
				<div style="text-align:center">
					<div style="text-align:left; width:75%; margin-left:auto; margin-right:auto"><strong>Name*:</strong>&nbsp; &nbsp;<?php echo @$_SESSION['pwdresetform']['name_error'] ?></div>
					<div style=" width:75%; margin-left:auto; margin-right:auto; margin-bottom:20px;">
						<input style="width:100%" type="text" id="ct_name" name="ct_name" size="35" value="<?php echo htmlspecialchars(@$_SESSION['pwdresetform']['ct_name']) ?>" />
					</div>
					<div style="text-align:left; width:75%; margin-left:auto; margin-right:auto"><strong>Registered Email*:</strong>&nbsp; &nbsp;<?php echo @$_SESSION['pwdresetform']['email_error'] ?></div>
					<div style=" width:75%; margin-left:auto; margin-right:auto; margin-bottom:20px;">
						<input style="width:100%" type="text" id="ct_email" name="ct_email" size="35" value="<?php echo htmlspecialchars(@$_SESSION['pwdresetform']['ct_email']) ?>" />
					</div>
					<img id="siimage" style="border: 1px solid #000;" src="/resources/securimage/securimage_show.php?sid=<?php echo md5(uniqid()) ?>" alt="CAPTCHA Image" />
					<p>
						<object type="application/x-shockwave-flash" data="/resources/securimage/securimage_play.swf?bgcol=#ffffff&amp;icon_file=/resources/securimage/images/audio_icon.png&amp;audio_file=/resources/securimage/securimage_play.php" height="32" width="32">
							<param name="movie" value="/resources/securimage/securimage_play.swf?bgcol=#ffffff&amp;icon_file=/resources/securimage/images/audio_icon.png&amp;audio_file=./securimage_play.php" />
						</object>
						&nbsp; <a tabindex="-1" style="border-style: none;" href="#" title="Refresh Image" onclick="document.getElementById('siimage').src = '/resources/securimage/securimage_show.php?sid=' + Math.random(); this.blur(); return false"><img src="/resources/securimage/images/refresh.png" alt="Reload Image" height="32" width="32" onclick="this.blur()" align="bottom" border="0" /></a><br />
						<strong>Enter Code*:</strong><br />
						<?php echo @$_SESSION['pwdresetform']['captcha_error'] ?>
						<input type="text" name="ct_captcha" size="12" maxlength="16" />
					</p>
					<p> <br />
						<input name="Submit" id="submitbtn" type="button" value="Submit" class="button" onclick="submitcheck();" />
					</p>
				</div>
			</form>
			</fieldset>
		</div>
	</div>
	<div style="width:100%; text-align:center; margin-top:50px; margin-bottom:100px;">
		<h1>Forgot your login</h1>
		<br />
		<h3>Contact support at <a href="mailto:<?php echo $settings->email_support; ?>"><?php echo $settings->email_support; ?></a></h3>
	</div>
	<div style="width:100%; margin-top:20px; margin-bottom:60px">
		<div style="margin-left:auto; margin-right:auto; width:50%; text-align:left; display:none"> <?php echo $settings->company_name; ?> Service Portal is a property of <?php echo $settings->company_name; ?>.  This system should be accessed by authorized <?php echo $settings->company_name; ?> personnel and customers only.  All information on this computer system may be intercepted, recorded, read, copied, and disclosed by and to authorized personnel for official purposes, including criminal investigations.  Unauthorized access or use of this computer system may subject violators to criminal, civil, and/or administrative action. </div>
		<div style="margin-left:auto; margin-right:auto; width:50%; text-align:left;"> This system should be accessed by authorized <?php echo $settings->company_name; ?> personnel and customers only. Unauthorized access or use of this computer system may subject violators to criminal, civil, and/or administrative action. </div>
	</div>
</div>
<?php require_once($footer_include); ?>
<?php
// The form processor PHP code
function process_si_contact_form()
{
	global $mysqli, $log, $settings;
	
	$_SESSION['pwdresetform'] = array(); // re-initialize the form session data
	if ($_SERVER['REQUEST_METHOD'] == 'POST' && @$_POST['do'] == 'contact') {
		// if the form has been submitted
		
		$users_email = "";
		$users_name = "";
		$users_uid = "";
		$user_exist = false;
		
		foreach($_POST as $key => $value) {
			if (!is_array($key)) {
				// sanitize the input data
				if ($key != 'ct_message') $value = strip_tags($value);
				$_POST[$key] = htmlspecialchars(stripslashes(trim($value)));
			}
		}
	
		$name    = @$_POST['ct_name'];    // name from the form
		$email   = @$_POST['ct_email'];   // email from the form
		$captcha = @$_POST['ct_captcha']; // the user's entry for the captcha code
		$name    = substr($name, 0, 64);  // limit name to 64 characters
		
		$_SESSION['pwdresetform']['ct_name'] = $name;       // save name from the form submission
		$_SESSION['pwdresetform']['ct_email'] = $email;     // save email
	
		$errors = array();  // initialize empty error array
		  
		if (strlen($name) < 3) {
			// name too short, add error
			$errors['name_error'] = 'Your name is required';
		}
		if (strlen($email) == 0) {
			// no email address given
			$errors['email_error'] = 'Email address is required';
		}else if ( !preg_match('/^(?:[\w\d]+\.?)+@(?:(?:[\w\d]\-?)+\.)+\w{2,4}$/i', $email)) {
			// invalid email format
			$errors['email_error'] = 'Email address entered is invalid';
		}
		
		// Only try to validate the captcha if the form has no errors
		// This is especially important for ajax calls
		if (sizeof($errors) == 0) {
			require_once $_SERVER['DOCUMENT_ROOT'].'/resources/securimage/securimage.php';
			$securimage = new Securimage();
			
			if ($securimage->check($captcha) == false) {
				$errors['captcha_error'] = 'Incorrect security code entered<br />';
			}
		}
	
		if (sizeof($errors) == 0) {
			// no errors, send the form
			$_SESSION['pwdresetform']['error'] = false;  // no error with form
			$_SESSION['pwdresetform']['success'] = true; // message sent
			
			//Check if the user exists in the db
			$sql="SELECT * FROM users WHERE email = '$email';";
			if(!$result = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
				$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
			}	
			$numrows = $result->num_rows;
			if($numrows > 0){
				$row = $result->fetch_assoc();
				$users_email = $row['email'];
				$users_name = $row['name'];
				$users_uid = $row['uid'];
				$user_exist = true;	
			}else{
				$user_exist = false;	
			}
			//echo "usrexist:",$user_exist,EOL;
			sendemail($name, $email, $user_exist, $users_uid, $users_email, $users_name);
		} else {
			// save the entries, this is to re-populate the form
			$_SESSION['pwdresetform']['ct_name'] = $name;       // save name from the form submission
			$_SESSION['pwdresetform']['ct_email'] = $email;     // save email
			foreach($errors as $key => $error) {
				// set up error messages to display with each field
				$_SESSION['pwdresetform'][$key] = "<span style=\"font-weight: bold; color: #f00\">$error</span>";
			}		
			$_SESSION['pwdresetform']['error'] = true; // set error floag
			//return false;
		}
	} // POST
}

function sendemail($_name,$_email,$_user_exist,$_users_uid, $_users_email, $_users_name){
	global $mysqli, $send, $log, $smarty, $settings;
	
	$token = randomString(32);
	$ip = $_SERVER['REMOTE_ADDR'];
	$domain = gethostbyaddr($ip);
	$date = date(storef,time()); 
	$browser = $_SERVER['HTTP_USER_AGENT'];
	
	if($_user_exist){
		$sql="INSERT INTO `users_pwd_reset_reqs` (`user_exist`, `token`, `uid`, `email`, `name`, `date`, `ip`, `domain`, `browser`) VALUES ('Y', '$token', '$_users_uid', '$_users_email', '$_users_name', '$date', '$ip', '$domain', '$browser');";	
		$recip_email = $_users_email;
		$recip_name = $_users_name;
	}else{
		$sql="INSERT INTO `users_pwd_reset_reqs` (`user_exist`, `token`, `uid`, `email`, `name`, `date`, `ip`, `domain`, `browser`) VALUES ('N', '', '', '$_email', '$_name', '$date', '$ip', '$domain', '$browser');";
		$recip_email = $_email;
		$recip_name = $_name;
	}	
	if(!$result = $mysqli->query($sql)){
		$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
		$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
		$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
	}
			
	if($_user_exist){
		$html = $html_good;
		$tpl = 'pwd_reset_request.tpl';
		$smarty->assign('token',$token);
		$smarty->assign('name',$_name);
		$smarty->assign('pwd_reset_url',$settings->pwd_reset_url);
		$subject = (string)$settings->short_name.' Portal Password Reset Requested';
	}else{
		$html = $html_bad;
		$tpl = 'pwd_reset_bad.tpl';
		$smarty->assign('name',$_name);
		$smarty->assign('email',$_email);
		$subject = (string)$settings->short_name.' Portal Account Access Attempted';
	}	
	
	$smarty->assign('company_name',$settings->company_name);
	$smarty->assign('copyright_date',$settings->copyright_date);
	$smarty->assign('email_pics',$settings->email_pics);
	
	require $_SERVER['DOCUMENT_ROOT'].'/resources/PHPMailer/PHPMailerAutoload.php';
	
	$mail = new PHPMailer;
	
	$mail->IsSMTP();                                      // Set mailer to use SMTP
	$mail->Host = (string)$settings->email_host;  // Specify main and backup server
	$mail->SMTPAuth = true;                               // Enable SMTP authentication
	$mail->Username = (string)$settings->email_users;    // SMTP username
	$mail->Password = (string)$settings->email_password;                           // SMTP password
	$mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted
	
	$mail->From = (string)$settings->email_users;
	$mail->FromName = 'Users '.$settings->short_name.' Portal';
	$mail->AddAddress($recip_email, $recip_name);  // Add a recipient
	$mail->AddReplyTo((string)$settings->email_support, 'Support '.$settings->short_name.' Portal');
	$mail->AddCC((string)$settings->email_support);
	$mail->IsHTML(true);
	$mail->Subject = $subject;
	$mail->Body = $smarty->fetch($tpl);
	if($send){	
		if(!$mail->Send()){
			$log->logerr($mail->ErrorInfo,1053,false,basename(__FILE__),__LINE__);
			echo 'Email could not be sent.';
			echo 'Mailer Error: ' . $mail->ErrorInfo;
			die();
			exit;
		}
	}

}

function randomString($length) {
	$keys = array_merge(range(0,9), range('a', 'z'), range('A', 'Z'));
	for($i=0; $i < $length; $i++) {
		$key .= $keys[array_rand($keys)];
	}
	return $key;
}

$_SESSION['pwdresetform']['success'] = false; // clear success value after running




