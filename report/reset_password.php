<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

ob_start();
if(isset($_GET['tkn'])){
	$token = $_GET['tkn'];		
}else{
	$log->logerr('Token missing',1012);
	header("Location:/error.php?n=1012&&p=reset_password.php");
}

$debug=false;
if(isset($_GET['debug']) or $_POST['debug'] == "Y"){ 
	$debug=true; 
}

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/ldap.php');

$settings = new SimpleXMLElement($_SERVER['DOCUMENT_ROOT'].'/settings.xml', null, true);

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/smarty/libs/Smarty.class.php');
$smarty = new Smarty();
$smarty->setTemplateDir($_SERVER['DOCUMENT_ROOT'].$settings->email_templates);
$smarty->setCompileDir($_SERVER['DOCUMENT_ROOT'].$settings->email_template_c);
$smarty->setCacheDir($_SERVER['DOCUMENT_ROOT'].$settings->email_cache);
$smarty->setConfigDir($_SERVER['DOCUMENT_ROOT'].$settings->email_configs);
$smarty->use_sub_dirs = true;
$smarty->force_compile = true;
//$smarty->debugging = true;
$smarty->caching = false;

$no_redirect = true;
$use_default_includes = true;
$remove_session = true;

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

$secret_rand = mt_rand(1,2);

$sql="SELECT pwd.*, DATE_ADD(TIMESTAMP(`date`), INTERVAL 2 HOUR) AS expire, u.secret_". $secret_rand .", s.question, s.id
FROM users_pwd_reset_reqs AS pwd
LEFT JOIN users AS u ON u.uid = pwd.uid
LEFT JOIN users_secret_questions AS s ON s.id = u.secret_".$secret_rand."_id
WHERE pwd.token = '$token';";
d($sql);

if(!$resultToken = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support. Number 1',1000,true,basename(__FILE__),__LINE__);
}
$num_token_results = $resultToken->num_rows;
if($num_token_results > 0){
	$rowToken = $resultToken->fetch_assoc();
	if(strtolower($rowToken['used']) == "y"){
		$log->loginfo('Used Token',1013);
		header("Location:/error.php?n=1013&t=UT&p=reset_password.php");	
	}
	$expire = strtotime($rowToken['expire']) - time();
	if($expire <= 0){
		$log->loginfo('Expired Token',1013);
		header("Location:/error.php?n=1013&t=ET&p=reset_password.php");	
	}
	if($rowToken['question'] == ''){
		$log->loginfo('Secret Question is 0',1073);
		header("Location:/error.php?n=1073&t=SQ&p=reset_password.php");	
	}
	
}else{
	$log->logerr('Invalid Token',1013);
	header("Location:/error.php?n=1013&t=IT&p=reset_password.php");	
}


process_si_contact_form();
?>

<!DOCTYPE html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<?php require_once($js_include);?>

<script type="text/javascript">
$(document).ready(function() {
	$(function(){
    $(".showpassword").each(function(index,input) {
        var $input = $(input);
		var $newpwd1 = $("#newpwd1");
		
        $('<label class="showpasswordlabel"/>').append(
            $("<input type='checkbox' class='showpasswordcheckbox' />").click(function() {
                var change = $(this).is(":checked") ? "text" : "password";
                var rep = $("<input style='width:100%' type='" + change + "' />")
                    .attr("id", $input.attr("id"))
                    .attr("name", $input.attr("name"))
                    .attr('class', $input.attr('class'))
                    .val($input.val())
                    .insertBefore($input);
				var rep1 = $("<input style='width:100%' type='" + change + "' />")
                    .attr("id", $newpwd1.attr("id"))
                    .attr("name", $newpwd1.attr("name"))
                    .attr('class', $newpwd1.attr('class'))
                    .val($newpwd1.val())
                    .insertBefore($newpwd1);
					
                $input.remove();
                $input = rep;
				$newpwd1.remove();
				$newpwd1 = rep1
				
             })
        ).append($("<span/>").text("Show password")).insertAfter($input);
    });
});	
});
function highlight(){
	document.getElementById("uid").focus();
	<?php
	if(isset($_SESSION['pwdresetform']['success']) && $_SESSION['pwdresetform']['success'] == true){
	?>
	document.getElementById("email").value = '';
	document.getElementById("uid").value = '';
	$("#pwdresetdiv").hide();
	<?php
	}
	?>
}
function submitcheck(){
	document.forms["formpwd"].submit();
}
</script>
</head>
<body onLoad="highlight()">
<?php require_once($header_include); ?>
<div class="outerContainer" id="content">
	<div style="width:100%; margin-top:20px; text-align:center;  margin-bottom:20px;">
		<h1 style="color:#F79447">Reset your password</h1>
	</div>
	<div style="width:500px; margin-left:auto; margin-right:auto; color:#1B2673; padding-top:15px; padding-bottom:10px">
		<div style="text-align:center">
			<?php
			//process_si_contact_form(); // Process the form, if it was submitted		
			if (isset($_SESSION['pwdresetform']['error']) &&  $_SESSION['pwdresetform']['error'] == true): /* The last form submission had 1 or more errors */ ?>
			<span class="error">There was a problem with your submission.  Errors are displayed below in red.</span><br />
			<br />
			<?php elseif (isset($_SESSION['pwdresetform']['success']) && $_SESSION['pwdresetform']['success'] == true): /* form was processed successfully */ ?>
			<span class="success">Thank you. Your password has been changed!<br>
			<a href="/" target="_self">Click here to return to the main page.</a></span><br />
			<br />
			<?php endif; ?>
		</div>
		<div id="pwdresetdiv">
		<div style="text-align:center">
			<form id="formpwd" name="formpwd" method="post" action="">
				<input type="hidden" name="do" value="contact" />
				<input type="hidden" name="id" id="id" value="<?php echo $secret_rand; ?>" />
				<div style="text-align:left; width:75%; margin-left:auto; margin-right:auto"><strong>Username*:</strong>&nbsp; &nbsp;<?php echo "<br>".@$_SESSION['pwdresetform']['uid_error'] ?></div>
				<div style=" width:75%; margin-left:auto; margin-right:auto; margin-bottom:20px;">
					<input style="width:100%" type="text" id="uid" name="uid" size="35" value="<?php echo htmlspecialchars(@$_SESSION['pwdresetform']['pwd_uid']) ?>" />
				</div>
				<div style="text-align:left; width:75%; margin-left:auto; margin-right:auto"><strong>Registered Email*:</strong>&nbsp; &nbsp;<?php echo "<br>".@$_SESSION['pwdresetform']['email_error'] ?></div>
				<div style=" width:75%; margin-left:auto; margin-right:auto; margin-bottom:20px;">
					<input style="width:100%" type="text" id="email" name="email" size="35" value="<?php echo htmlspecialchars(@$_SESSION['pwdresetform']['pwd_email']) ?>" />
				</div>
				<div style="text-align:left; width:75%; margin-left:auto; margin-right:auto"><strong><?php echo $rowToken['question']; ?></strong>&nbsp; &nbsp;<?php echo "<br>".@$_SESSION['pwdresetform']['secret_error'] ?></div>
				<div style=" width:75%; margin-left:auto; margin-right:auto; margin-bottom:20px;">
					<input style="width:100%" type="text" id="secret" name="secret" size="35" />
				</div>
				<br>
				<div style="width:75%; margin-left:auto; margin-right:auto; text-align:left">
				Password Requirements:
				<ul style="list-style:inside">
					<li>CaSe SeNsItIvE</li>
					<li>Minimun of 6 characters</li>
					<li>Must Contain letters AND numbers</li>
				</ul>
				</div>
				<br>
				
				<div style="text-align:left; width:75%; margin-left:auto; margin-right:auto"><strong>New Password*:</strong>&nbsp; &nbsp;<?php echo "<br>".@$_SESSION['pwdresetform']['password_error'] ?></div>
				<div style=" width:75%; margin-left:auto; margin-right:auto; margin-bottom:20px;">
					<input style="width:100%" type="password" id="newpwd1" name="newpwd1" size="35" />
				</div>
				
				<div style="text-align:left; width:75%; margin-left:auto; margin-right:auto"><strong>Re-Type New Password*:</strong></div>
				<div style=" width:75%; margin-left:auto; margin-right:auto;">
					<input style="width:100%" type="password" id="newpwd2" name="newpwd2" class="showpassword" size="35" /><br>
				</div>
				<!--<div style="width:75%; margin-left:auto; margin-right:auto">Show Password<input style="width:100%" type="checkbox" id="showpwd" name="showpwd" value="true" /></div>-->
				<br><br>
				
				<img id="siimage" style="border: 1px solid #000;" src="/resources/securimage/securimage_show.php?sid=<?php echo md5(uniqid()) ?>" alt="CAPTCHA Image" />
				<p>
					<object type="application/x-shockwave-flash" data="/resources/securimage/securimage_play.swf?bgcol=#ffffff&amp;icon_file=/resources/securimage/images/audio_icon.png&amp;audio_file=/resources/securimage/securimage_play.php" height="32" width="32">
						<param name="movie" value="/resources/securimage/securimage_play.swf?bgcol=#ffffff&amp;icon_file=/resources/securimage/images/audio_icon.png&amp;audio_file=./securimage_play.php" />
					</object>
					&nbsp; <a tabindex="-1" style="border-style: none;" href="#" title="Refresh Image" onclick="document.getElementById('siimage').src = '/resources/securimage/securimage_show.php?sid=' + Math.random(); this.blur(); return false"><img src="/resources/securimage/images/refresh.png" alt="Reload Image" height="32" width="32" onclick="this.blur()" align="bottom" border="0" /></a><br />
					<strong>Enter Code*:</strong><br />
					<?php echo "<br>".@$_SESSION['pwdresetform']['captcha_error'] ?>
					<input type="text" name="captcha" size="12" maxlength="16" />
				</p>
				<p> <br />
					<input name="Submit" id="submitbtn" type="button" value="Submit" class="button" onclick="submitcheck();" />
				</p>
			</form>   
		</div> 
		</div>
    </div>
	<div style="width:100%; margin-top:20px; margin-bottom:60px">
		<div style="margin-left:auto; margin-right:auto; width:50%; text-align:left;">
		This system should be accessed by authorized <?php echo $settings->company_name; ?> personnel and customers only. Unauthorized access or use of this computer system may subject violators to criminal, civil, and/or administrative action.
		</div>
	</div>
</div>
<?php require_once($footer_include); ?>

<?php
// The form processor PHP code
function process_si_contact_form()
{
	global $mysqli, $https_cookie, $log, $settings, $debug;
	
	$_SESSION['pwdresetform'] = array(); // re-initialize the form session data
	if ($_SERVER['REQUEST_METHOD'] == 'POST' && @$_POST['do'] == 'contact') {
		// if the form has been submitted
		
		$users_email = "";
		$users_uid = "";
		
		foreach($_POST as $key => $value) {
			if (!is_array($key)) {
				// sanitize the input data
				if ($key != 'ct_message') $value = strip_tags($value);
				$_POST[$key] = htmlspecialchars(stripslashes(trim($value)));
			}
		}
		$token   = @$_GET['tkn'];
		$uid     = @$_POST['uid'];    // name from the form
		$email   = @$_POST['email'];   // email from the form
		$secret  = @$_POST['secret'];   // secret from the form
		$secret_id = @$_POST['id']; //secret id from form
		$newpwd1 = @$_POST['newpwd1'];   // pwd1 from the form
		$newpwd2 = @$_POST['newpwd2'];   // pwd2 from the form
		$captcha = @$_POST['captcha']; // the user's entry for the captcha code
		$name    = substr($name, 0, 64);  // limit name to 64 characters
		
		$_SESSION['pwdresetform']['pwd_uid'] = $uid;       // save name from the form submission
		$_SESSION['pwdresetform']['pwd_email'] = $email;     // save email
		//$_SESSION['pwdresetform']['pwd_secret'] = $secret;     // save email
	
		$errors = array();  // initialize empty error array
		
		if (strlen($secret) < 2) {
			// name too short, add error
			$errors['secret_error'] = 'Secret answer is required.';
		}		  
		if (strlen($uid) < 3) {
			// name too short, add error
			$errors['uid_error'] = 'Your name is required';
		}
		if (strlen($email) == 0) {
			// no email address given
			$errors['email_error'] = 'Email address is required';
		}else{
			if(!preg_match('/^(?:[\w\d]+\.?)+@(?:(?:[\w\d]\-?)+\.)+\w{2,4}$/i', $email)){
				// invalid email format
				$errors['email_error'] = 'Email address entered is invalid';
			}
		}
		if($newpwd1 != $newpwd2){
			// pqsswords do not match
			$errors['password_error'] = 'Passwords do not match';	
		}else{
			if(!preg_match('/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{6,12}$/', $newpwd1)) {
				// pqsswords do not match
				$errors['password_error'] = 'Password does not meet requirements';
			}
		}
		
		if (sizeof($errors) == 0) {
			$sql="SELECT * FROM users WHERE uid = '$uid' AND email = '$email' AND secret_".$secret_id." = '".base64_encode(strtolower($secret))."' LIMIT 10;";
			d($sql);
			if(!$resultSecret = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
				$log->logerr('Error occurred, contact support. Number 2',1000,true,basename(__FILE__),__LINE__);
			}
			$num_rows = $resultSecret->num_rows;
			if($num_rows < 1){
				$errors['secret_error'] = 'Username, Email, or Secret do not match our database';
				$errors['uid_error'] = 'Username, Email, or Secret do not match our database';
				$errors['email_error'] = 'Username, Email, or Secret do not match our database';
			}else{
				$rowSecret = $resultSecret->fetch_assoc();
				d($rowSecret);
			}
		}
		
		// Only try to validate the captcha if the form has no errors
		// This is especially important for ajax calls
		if (sizeof($errors) == 0) {
			require_once $_SERVER['DOCUMENT_ROOT'].'/resources/securimage/securimage.php';
			$securimage = new Securimage();
			
			if ($securimage->check($captcha) == false) {
				$errors['captcha_error'] = 'Incorrect security code entered<br />';
			}
		}
		
		if (sizeof($errors) == 0) {
			// no errors, send the form
			$_SESSION['pwdresetform']['error'] = false;  // no error with form
			$_SESSION['pwdresetform']['success'] = true; // message sent
			
			$salt = "STyf1anSTydajgnmd";
			$encrypted_password1 = "{MD5}" . base64_encode( pack( "H*", md5( $newpwd1 . $salt ) ) );
			$pwd_chg_date = strtotime('+6 months', time());
			$pwd_chg_date = date(storef,$pwd_chg_date);
			$sql="UPDATE `users` SET pwd = '$encrypted_password1', pwd_chg_date = '$pwd_chg_date', pwd_chg_remind = 0 WHERE uid = '$uid' AND id=".$rowSecret['id'].";";
			d($sql);
			if(!$result = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
				$log->logerr('Error occurred, contact support. Number 3',1000,true,basename(__FILE__),__LINE__);
			}
			
			$sql="UPDATE users_pwd_reset_reqs SET used = 'Y' WHERE token = '$token';";
			d($sql);
			if(!$result = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
				$log->logerr('Error occurred, contact support. Number 4',1000,true,basename(__FILE__),__LINE__);
			}
			
			$sql="CALL `users_data_get`('$uid');";
			d($sql);
			if(!$resultEditUser = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
				$log->logerr('Error occurred, contact support. Number 5',1000,true,basename(__FILE__),__LINE__);
			}
			$rowEditUser = $resultEditUser->fetch_assoc();
			d($rowEditUser);
			
			//Clear the remaining results from the above mysql call statement
			while ($mysqli->next_result());
			
			d($rowEditUser['grp_employee']);
			if(strtolower($rowEditUser['grp_employee']) == 'y'){
				if(strtolower($rowEditUser['perm_dms_access']) == 'y'){
					$dms = true;
				}else{
					$dms = false;
				}
				s('DMS update: '.$dms);
				
				if(strtolower($rowEditUser['perm_kb_access']) == 'y'){
					$kb = true;
				}else{
					$kb = false;
				}
				s('KB update: '.$kb);
				
				s("running ldap operations...");
				$ldap = new ldapUtil();
				if($debug){$ldap->options['debug'] = true;}
				
				$ldap_password = $encrypted_password1;
				
				$ldap->user($uid,$ldap_password,$rowEditUser['firstname']." ".$rowEditUser['lastname'],$rowEditUser['lastname'],$rowEditUser['email'],$kb,$dms);				
			}			
			sendemail($rowSecret['name'], $email);
		} else {
			// save the entries, this is to re-populate the form
			$_SESSION['pwdresetform']['pwd_uid'] = $uid;       // save name from the form submission
			$_SESSION['pwdresetform']['pwd_email'] = $email;     // save email
			//$_SESSION['pwdresetform']['pwd_secret'] = $secret;     // save secret
			foreach($errors as $key => $error) {
				// set up error messages to display with each field
				$_SESSION['pwdresetform'][$key] = "<span style=\"font-weight: bold; color: #f00\">$error</span>";
			}		
			$_SESSION['pwdresetform']['error'] = true; // set error floag
			//return false;
		}
	} // POST
}

function sendemail($_name,$_email){			
	global $log, $smarty, $settings;
	
	$subject = 'OiHealthcare Portal Password Reset';
	
	require_once($_SERVER['DOCUMENT_ROOT'].'/resources/PHPMailer/PHPMailerAutoload.php');
	
	$mail = new PHPMailer;
	$mail->IsSMTP();
	$mail->Host = (string)$settings->email_host;
	$mail->SMTPAuth = true;
	$mail->Username = (string)$settings->email_users;
	$mail->Password = (string)$settings->email_password;
	$mail->SMTPSecure = 'tls';
	
	$mail->From = (string)$settings->email_users;
	$mail->FromName = 'Users '.$settings->short_name.' Portal';
	$mail->AddAddress($_email, $_name);
	$mail->AddReplyTo((string)$settings->email_support, 'Support '.$settings->short_name.' Portal');
	$mail->AddCC((string)$settings->email_support);
	$mail->IsHTML(true);
	$mail->Subject = $subject;
	
	$smarty->assign('name', $_name);
	$smarty->assign('company_name',$settings->company_name);
	$smarty->assign('copyright_date',$settings->copyright_date);
	$smarty->assign('email_pics',$settings->email_pics);
	
	$mail->Body = $smarty->fetch('pwd_reset.tpl');
	
	if(!$mail->Send()) {
		$log->logerr($mail->ErrorInfo,1014);
		header("Location:/error.php?n=1014&t=".$mail->ErrorInfo."&p=reset_password.php");
	    echo 'Email could not be sent.';
	    echo 'Mailer Error: ' . $mail->ErrorInfo;
	    exit;
	}



}

$_SESSION['pwdresetform']['success'] = false; // clear success value after running