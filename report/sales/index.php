<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

$comp_word_limt 	 = intval($settings->complaint_word_limit);
$serv_word_limt		 = intval($settings->service_word_limit);
$custact_word_limt	 = intval($settings->custact_word_limit);
$probrpt_word_limt	 = intval($settings->probrpt_word_limit);


$myusername = $_SESSION["login"];

function array_push_assoc(&$array, $key, $value){
	$array[$key] = $value;
	return $array;
}

function limit_words($string, $word_limit)
{
	$words = explode(" ",$string);
	if(count($words) > $word_limit){
		return implode(" ",array_splice($words,0,$word_limit))."...(more)";
	}else{
		return implode(" ",array_splice($words,0,$word_limit));
	}
}

?>
<!DOCTYPE html>
<html>
<head>

<?php require_once($head_include);?>
<?php require_once($css_include);?>
<?php require_once($js_include);?>

<script type="text/javascript">
	function start(){
		//getCookies();	
	}
/////////////////////////////////////////////////////////////////////////////////////	
	function getCookie(c_name)
	{
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++)
		{
			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
			x=x.replace(/^\s+|\s+$/g,"");
			if (x==c_name)
			{
			return unescape(y);
			}
		}
	}
/////////////////////////////////////////////////////////////////////////////////////	
$(document).ready(function() {	
		openQuotesTable = $('#openQuotesTable').dataTable({
			"bJQueryUI": true,
			//"bStateSave": true,
			"iCookieDuration": 60*60*24*365, // 1 year
			"sPaginationType": "full_numbers",
			"aaSorting": [],
			"fnInitComplete": function () {
           		 this.$('tr').click( function () {
               		 var href = $(this).find("a").attr("href");
					 if(href) {
						window.location = href;
					 }
           		 });
       		 }
		});
		

});
/////////////////////////////////////////////////////////////////////////////////////
</script>
</head>
<body onLoad="start()">
<?php require_once($header_include); ?>
    
<div id="OIReportContent"> 
        <div id="ReportOuterFrame">
	        <?php  
				$sql = "SELECT sq.id, sq.revision, sq.title, sq.customer_name, sq.created_date, sq.quote_id, sq.unique_id
				FROM sales_quotes AS sq
				WHERE sq.contracted = 'N' 
				AND sq.revision = (SELECT MAX(revision) FROM sales_quotes AS sqt WHERE sqt.unique_id = sq.unique_id)
				ORDER BY sq.created_date ASC;";
				if(!$resultOpenQuotes = $mysqli->query($sql)){
					$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
					$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
					$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);	
				}
			?>  
            <div id="ReportFrame">     
            <div id="ReportFrameTitle"> Open Sales Quotes </div>
            <div id="openQuotesDiv" <?php if($resultOpenQuotes->num_rows == 0){echo "style=\"display:none\"";} ?>>
                <table width="100%" id="openQuotesTable" >
                    <thead>
                        <tr>
                        	<th width="75px">Date</th> 
                        	<th width="100px">Quote ID</th>                          
                            <th width="50px">Revision</th>
                            <th>Title</th>
                            <th width="300px">Customer</th>
                        </tr>
                    </thead>
                    <tbody>
                    	<?php
						if($resultOpenQuotes->num_rows != 0){
							while($rowOpenQuote = $resultOpenQuotes->fetch_assoc())
							{
								echo "<tr>\n";
								echo "<td><a href=\"/report/common/sales_quote.php?edit&unique_id=".$rowOpenQuote['unique_id']."\">". date(phpdispfd,strtotime($rowOpenQuote['created_date']))."</a></td>\n";
								echo "<td>". $rowOpenQuote['quote_id']."</td>\n";
								echo "<td>". $rowOpenQuote['revision']."</td>\n";
								echo "<td>". $rowOpenQuote['title']."</td>\n";
								echo "<td>". $rowOpenQuote['customer_name']."</td>\n";
								echo "</tr>\n";
							}
						}
						?>                        
                    </tbody>
                </table>
            </div>
            <div id="openQuotesDiv" <?php if($resultOpenQuotes->num_rows != 0){echo "style=\"display:none\"";}else{echo "style=\"text-align:center\"";} ?>>
            	<br />
            	<h2>No Open Sales Quotes</h2>
            </div>
          </div>
        </div>         
	</div>
</div>
    <br> 
</div>
<?php require_once($footer_include); ?>