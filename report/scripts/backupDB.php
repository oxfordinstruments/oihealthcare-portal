<?php 
$no_define_eol = true;
if(substr(php_sapi_name(), 0, 3) == 'cli' || empty($_SERVER['REMOTE_ADDR'])) {
    //shell
	$cron = false;
	if(isset($_SERVER['argv'][1]) and $_SERVER['argv'][1] == '--cron'){
		$cron = true;
	}
	$docroot = preg_replace("/(public_html).*/",'public_html',dirname(__FILE__),2);
	$no_define_eol = true;
	define('EOL',PHP_EOL);
} else {
    //webserver
	$cron = false;
	$docroot = $_SERVER['DOCUMENT_ROOT'];
	define('EOL','<br />');
}

echo "Begin full db backup ".date('m/d/Y H:i',time()),EOL;
$settings = new SimpleXMLElement($docroot.'/settings.xml', null, true);
require($docroot.'/mysqlInfo.php');
$link = mysql_connect("$host", "$username", "$password")or die("cannot connect");
mysql_select_db("$db_name")or die("cannot select DB");

//get all of the tables

$tables = array();
$result = mysql_query('SHOW TABLES');
while($row = mysql_fetch_row($result))
{
	$tables[] = $row[0];
}

//cycle through
foreach($tables as $table)
{
	$result = mysql_query('SELECT * FROM '.$table);
	$num_fields = mysql_num_fields($result);
	
	$return.= 'DROP TABLE '.$table.';';
	$row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE '.$table));
	$return.= "\n\n".$row2[1].";\n\n";
	
	for ($i = 0; $i < $num_fields; $i++) 
	{
		while($row = mysql_fetch_row($result))
		{
			$return.= 'INSERT INTO '.$table.' VALUES(';
			for($j=0; $j<$num_fields; $j++) 
			{
				$row[$j] = addslashes($row[$j]);
				$row[$j] = ereg_replace("\n","\\n",$row[$j]);
				if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
				if ($j<($num_fields-1)) { $return.= ','; }
			}
			$return.= ");\n";
		}
	}
	$return.="\n\n\n";
}

//save file
$fname = '/full-db-backup-'.date('m-d-Y_H-i',time()).'.sql';
$handle = fopen($settings->backup_dir.'/temp'.$fname,'w+');
fwrite($handle,$return);
fclose($handle);
echo "backup complete ".date('m/d/Y H:i',time()),EOL;
//zip up file
$zip = new ZipArchive();
$DelFilePath=basename($settings->backup_dir.'/temp'.$fname,'.sql').'.zip';
if(file_exists($DelFilePath)) {
	unlink ($DelFilePath); 
}
if ($zip->open($settings->backup_dir.'/'.basename($fname,'.sql').'.zip', ZIPARCHIVE::CREATE) != TRUE) {
	die ("Could not open archive");
}
$zip->addFile($settings->backup_dir.'/temp'.$fname,$fname);
$zip->close(); 

//remove temp file
if(file_exists($settings->backup_dir.'/temp'.$fname)) {
	unlink ($settings->backup_dir.'/temp'.$fname); 
}
echo "cleaning up old files ".date('m/d/Y H:i',time()),EOL;
//remove old files
$fdate = '';
$files = scandir($settings->backup_dir.'/');
foreach($files as $file){
	if(strpos($file,'ull-db-backup') == 1){
		echo $file,EOL;
		$fdate = str_replace('full-db-backup-','',$file);
		$fdate = str_replace('.zip','',$fdate);
		$fdatetime = explode('_',$fdate);	
		$fdate = date('m/d/Y H:i',strtotime(str_replace('-','/',$fdatetime[0]).' '.str_replace('-',':',$fdatetime[1]).' + 10 days'));
		if(strtotime('now') > strtotime($fdate)){
			echo 'unlinking '.$file,EOL;	
			unlink($settings->backup_dir.'/'.$file);
		}
	}
}
echo "DONE ".date('m/d/Y H:i',time()),EOL;
exit();
?>