<?php

//

ob_start();//Start output buffering

$debug = true;
if(isset($_GET['debug'])){
	$debug = true;	
}

$from_system_id = NULL;
if(!isset($_GET['from_system_id'])){
	die('GET from_system_id not set');	
}else{
	$from_system_id = strtoupper($_GET['from_system_id']);
}

$to_system_id = NULL;
if(!isset($_GET['to_system_id'])){
	die('GET to_system_id not set');	
}else{
	$to_system_id = strtoupper($_GET['to_system_id']);
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/define_inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

require($_SERVER['DOCUMENT_ROOT'].'/mysqlInfo.php');
$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno) {die('There was an error running the query [' . $mysqli->connect_error . ']');}

echo "<h2>Checking System IDs</h2>";
$sql="SELECT system_id FROM systems_base WHERE system_id = '$from_system_id';";
if(!$reslut = $mysqli->query($sql)){
	die($mysqli->error);	
}
if($reslut->num_rows != 1){
	die("From System ID not valid");	
}else{
	echo "From System ID valid",EOL;	
}

$sql="SELECT system_id FROM systems_base WHERE system_id = '$to_system_id';";
if(!$reslut = $mysqli->query($sql)){
	die($mysqli->error);	
}
if($reslut->num_rows != 0){
	die("To System ID exists already");
}else{
	echo "To System ID valid",EOL;	
}


echo "<h2>Getting Unique IDs</h2>";
echo "From System ID: ",$from_system_id,EOL;
$sql="SELECT unique_id, facility_unique_id, ver_unique_id FROM systems_base_cont WHERE system_id = '$from_system_id';";
if(!$reslut = $mysqli->query($sql)){
	die($mysqli->error);	
}
$row = $reslut->fetch_assoc();
$from_system_unique_id = $row['unique_id'];
echo "From System Unique ID: ",$from_system_unique_id,EOL;
$from_system_ver_unique_id = $row['ver_unique_id'];
echo "From System Version Unique ID: ",$from_system_ver_unique_id,EOL;
$from_facility_unique_id = $row['facility_unique_id'];
echo "From Facility Unique ID: ",$from_facility_unique_id,EOL;

$sql="SELECT customer_unique_id FROM facilities WHERE unique_id = '$from_facility_unique_id';";
if(!$reslut = $mysqli->query($sql)){
	die($mysqli->error);	
}
$row = $reslut->fetch_assoc();
$from_customer_unique_id = $row['customer_unique_id'];
echo "From Customer Unique ID: ",$from_customer_unique_id,EOL;

echo EOL;
echo "<h2>Changing System ID</h2>";

$sql="UPDATE customers_email_list SET system_id = '$to_system_id' WHERE system_ver_unique_id = '$from_system_ver_unique_id';";
s($sql);
if(!$reslut = $mysqli->query($sql)){
	die($mysqli->error);
}

$sql="UPDATE systems_base SET system_id = '$to_system_id' WHERE unique_id = '$from_system_unique_id';";
s($sql);
if(!$reslut = $mysqli->query($sql)){
	die($mysqli->error);
}

$sql="UPDATE systems_base_cont SET system_id = '$to_system_id' WHERE ver_unique_id = '$from_system_ver_unique_id';";
s($sql);
if(!$reslut = $mysqli->query($sql)){
	die($mysqli->error);
}

$sql="UPDATE systems_reports SET system_id = '$to_system_id' WHERE system_unique_id = '$from_system_unique_id';";
s($sql);
if(!$reslut = $mysqli->query($sql)){
	die($mysqli->error);
}

$sql="UPDATE systems_requests SET system_id = '$to_system_id' WHERE system_unique_id = '$from_system_unique_id';";
s($sql);
if(!$reslut = $mysqli->query($sql)){
	die($mysqli->error);
}
echo "<h2>Done</h2>";
echo "<br><br>";
echo "<h1 style='color: red'>Do not refresh the page!</h1>";
?>

	
	
	
	
	
	
	
	
	
