<?php
//Run every 1 hour m-f 8am EST to 8pm EST

$no_define_eol = true;
if(substr(php_sapi_name(), 0, 3) == 'cli' || empty($_SERVER['REMOTE_ADDR'])) {
    //shell
	$cron = false;
	if(isset($_SERVER['argv'][1]) and $_SERVER['argv'][1] == '--cron'){
		$cron = true;
	}
	$docroot = preg_replace("/(public_html).*/",'public_html',dirname(__FILE__),2);
	$no_define_eol = true;
	define('EOL',PHP_EOL);
} else {
    //webserver
	$cron = false;
	$docroot = $_SERVER['DOCUMENT_ROOT'];
	define('EOL','<br />');
}

$debug = false;
if(isset($_GET['debug'])){
	$debug = true;
	if(isset($_GET['send'])){$send = true;}else{$send = false;}
}else{
	$send = true;
}


$settings = new SimpleXMLElement($docroot.'/settings.xml', null, true);
$no_define_eol = true;
require_once($docroot.'/define_inc.php');

require_once($docroot.'/resources/smarty/libs/Smarty.class.php');
$smarty = new Smarty();
$smarty->setTemplateDir($docroot.$settings->email_templates);
$smarty->setCompileDir($docroot.$settings->email_template_c);
$smarty->setCacheDir($docroot.$settings->email_cache);
$smarty->setConfigDir($docroot.$settings->email_configs);
$smarty->use_sub_dirs = true;
$smarty->force_compile = true;
//$smarty->debugging = true;
$smarty->caching = false;

echo "Begin assignments check ".date(phpdispfdt,time()),EOL,EOL;

if($settings->disable_email == '1'){
	$send = false;	
}

require($docroot.'/mysqlInfo.php');
$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno) {die('There was an error running the query [' . $mysqli->connect_error . ']');}

//xml of current tables
$newxml = new SimpleXMLElement('<root></root>');
$path = $settings->assignments_dir;


$now = date(savefdt,time());
$nowPretty = date(storef,time());


//load the previous backup
$sql="SELECT * FROM systems_assigned_history ORDER BY id DESC LIMIT 1;";
$resultPrev = $mysqli->query($sql);
if($resultPrev->num_rows < 1){
	echo EOL,"No previous backups",EOL;
	$first_run = true;
	$update = true;
}else{
	echo EOL,"Found previous backups",EOL;
	$first_run = false;
}

if($first_run == false){
	$rowPrev = $resultPrev->fetch_assoc();
	$prevxml = new SimpleXMLElement($path.'/'.$rowPrev['filename'],null,true);
	echo "Loading backup file ".$rowPrev['filename'],EOL,EOL;
	
	//compare pri previous xml to current pri database
	$modified_systems_pri = array();
	
	$priArr = array();
	$sql="SELECT * FROM systems_assigned_pri ORDER BY id ASC;";
	$resultPri = $mysqli->query($sql);
	while($rowPri = $resultPri->fetch_assoc()){
		$priArr[$rowPri['system_ver_unique_id']] = $rowPri['uid'];	
	}
	
	//compare database to xml
	if($debug){echo "Checking Primary Systems DB -> XML",EOL;}
	foreach($priArr as $system_ver_unique_id=>$uid){
		if($debug){echo "db->xml system ver_unique_id: ".$system_ver_unique_id,EOL;}
		$xmlresult = $prevxml->xpath("/root/table[@table='primary']/system[system_ver_unique_id='".$system_ver_unique_id."']/uid");
		
		//check for missing system in xml
		if($xmlresult[0] == ""){
			array_push($modified_systems_pri,array('system_ver_unique_id'=>$system_ver_unique_id,'status'=>'new'));	
		}else{
			//check if user ids match
			if($uid != (string)$xmlresult[0]){
				array_push($modified_systems_pri,array('system_ver_unique_id'=>$system_ver_unique_id,'status'=>'db','db'=>$uid,'xml'=>(string)$xmlresult[0]));	
			}
		}	
	}
	//compare xml to database
	if($debug){echo "Checking Primary Systems XML -> DB",EOL;}
	foreach($prevxml->xpath("/root/table[@table='primary']/system") as $system){
		if($debug){echo "Checking xml->db system ver_unique_id: ".$system[0]->system_ver_unique_id,EOL;}
		$system_ver_unique_id = (string)$system[0]->system_ver_unique_id;
	
		//check for missing system in db
		if(!array_key_exists($system_ver_unique_id,$priArr)){
			array_push($modified_systems_pri,array('system_ver_unique_id'=>$system_ver_unique_id,'status'=>'removed'));
		}else{
			//check if user ids match
			if((string)$system[0]->uid != $priArr[$system_ver_unique_id]){
				array_push($modified_systems_pri,array('system_ver_unique_id'=>$system_ver_unique_id,'status'=>'xml','db'=>$priArr[$system_ver_unique_id],'xml'=>(string)$system[0]->uid));	
			}
		}
	}
	
	echo "Modified Primary Systems:",EOL,print_r($modified_systems_pri),EOL;
	
	//compare sec previous xml to current sec database
	$modified_systems_sec = array();
	
	$secArr = array();
	$sql="SELECT * FROM systems_assigned_sec ORDER BY id ASC;";
	$resultSec = $mysqli->query($sql);
	while($rowSec = $resultSec->fetch_assoc()){
		$secArr[$rowSec['system_ver_unique_id']] = $rowSec['uid'];	
	}
	
	//compare database to xml
	if($debug){echo "Checking Secondary Systems DB -> XML",EOL;}
	foreach($secArr as $system_ver_unique_id=>$uid){
		if($debug){echo "Checking db->xml system ver_unique_id: ".$system_ver_unique_id,EOL;}
		$xmlresult = $prevxml->xpath("/root/table[@table='secondary']/system[system_ver_unique_id='".$system_ver_unique_id."']/uid");
		
		//check for missing system in xml
		if($xmlresult[0] == ""){
			array_push($modified_systems_sec,array('system_ver_unique_id'=>$system_ver_unique_id,'status'=>'new'));	
		}else{
			//check if user ids match
			if($uid != $xmlresult[0]){
				array_push($modified_systems_sec,array('system_ver_unique_id'=>$system_ver_unique_id,'status'=>'db','db'=>$uid,'xml'=>(string)$xmlresult[0]));	
			}
		}	
	}
	//compare xml to database
	if($debug){echo "Checking Secondary Systems XML -> DB",EOL;}
	foreach($prevxml->xpath("/root/table[@table='secondary']/system") as $system){
		if($debug){echo "Checking xml->db system ver_unique_id: ".$system[0]->system_ver_unique_id,EOL;}
		$system_ver_unique_id = (string)$system[0]->system_ver_unique_id;
	
		//check for missing system in db
		if(!array_key_exists($system_ver_unique_id,$secArr)){
			array_push($modified_systems_sec,array('system_ver_unique_id'=>$system_ver_unique_id,'status'=>'removed'));
		}else{
			//check if user ids match
			if((string)$system[0]->uid != $secArr[$system_ver_unique_id]){
				array_push($modified_systems_sec,array('system_ver_unique_id'=>$system_ver_unique_id,'status'=>'xml','db'=>$secArr[$system_ver_unique_id],'xml'=>(string)$system[0]->uid));
			}
		}
	}
	
	echo "Modified Secondary Systems:",EOL,print_r($modified_systems_sec),EOL;
	
	if(count($modified_systems_pri) != 0 or count($modified_systems_sec) != 0){
		echo "Found modified systems",EOL;
		sendEmails($modified_systems_pri,$modified_systems_sec);
		$sql="UPDATE systems_assigned_history SET email_sent = 'Y' WHERE id ='".$rowPrev['id']."';";
		$mysqli->query($sql);
		$update = true;
	}else{
		echo "No system modifications found",EOL;
		$update = false;
	}
}

//create an xml backup from the assigned_sits_* tables
if($update == true){
	echo "Creating new backup",EOL;
	$sql="INSERT INTO systems_assigned_history (`date`) VALUES ('$nowPretty');";
	$result = $mysqli->query($sql);
	$insert_id = $mysqli->insert_id;
	$newfile = "asgn_".$now."_".$insert_id.".xml";
	makexml();
	$sql="UPDATE systems_assigned_history SET filename = '$newfile' WHERE id='$insert_id';";
	$mysqli->query($sql);
	$newxml->asXML($path.'/'.$newfile);
	echo "New backup file: ". $newfile,EOL;
}else{
	echo "No new backup file needed",EOL;
}

//clean up backups
echo "Cleaning up backups",EOL;
$sql="SELECT * FROM systems_assigned_history ORDER BY id DESC LIMIT 10;";
$resultFiles = $mysqli->query($sql);

$files = scandir($path.'/');
unset($files[0]); //remove .
unset($files[1]); //remove ..

while($rowFiles = $resultFiles->fetch_assoc()){
	if($key = array_search($rowFiles['filename'],$files)){
		unset($files[$key]);
	}
}
foreach($files as $file){
	echo "Unlinking: ".$file,EOL;
	unlink($path.'/'.$file);	
}

$sql="DELETE p FROM systems_assigned_history p
LEFT JOIN (SELECT id FROM systems_assigned_history ORDER BY id DESC LIMIT 10) as p2 USING(id) 
WHERE p2.id IS NULL;";
$mysqli->query($sql);


echo "DONE ".date(phpdispfdt,time()),EOL;
exit();


function sendEmails($mods_pri = array(), $mods_sec = array()){
	global $resultEmails, $mysqli, $nowPretty, $rowPrev, $send, $smarty, $debug, $docroot, $settings;
	
	if($debug){echo "Modified Systems Primary: <pre>",print_r($mods_pri),"</pre>",EOL;}
	if($debug){echo "Modified Systems Secondary: <pre>",print_r($mods_sec),"</pre>",EOL;}
	
	$sql="SELECT u.uid, u.name
	FROM users_groups AS ug
	LEFT JOIN users AS u ON u.uid = ug.uid
	WHERE ug.gid = 1 OR ug.gid = 2;";
	$resultEngineers = $mysqli->query($sql);
	$engineers = array();
	while($rowEngineers = $resultEngineers->fetch_assoc()){
		$engineers[$rowEngineers['uid']] = $rowEngineers['name'];	
	}
		
	//merge both arrays
	$mods_pri_sec = array_merge($mods_pri,$mods_sec);
	if($debug){echo "Merged Primary/Secondary: <pre>",print_r($mods_pri_sec),"</pre>",EOL;}
	
	//make a csv str to lookup system names
	$system_ids_arr = array();
	foreach($mods_pri_sec as $system){
		array_push($system_ids_arr,$system['system_ver_unique_id']);	
	}
	$systems_csv = "'". implode("', '",array_unique($system_ids_arr)) . "'";
	if($debug){echo "System IDs CSV: ",$systems_csv,EOL;}
	
	//lookup system names from db
	$sql="SELECT system_id, nickname, ver_unique_id FROM systems_base_cont WHERE ver_unique_id IN($systems_csv);";
	$resultSystemInfo = $mysqli->query($sql);
	$system_info = array();
	while($rowSystemInfo = $resultSystemInfo->fetch_assoc()){
		$system_info[$rowSystemInfo['ver_unique_id']] = array('system_id'=>$rowSystemInfo['system_id'], 'nickname' => $rowSystemInfo['nickname']);	
	}	
	if($debug){echo "Systems Info: <pre>",print_r($system_info),"</pre>",EOL;}
	
	//make array for the template
	$tpl_arr = array();
	foreach($mods_pri as $mods) {
		$tpl_arr_data = array();
		if($mods['status'] == 'xml'){
			continue;	
		}
		
		$tpl_arr_data['system_id'] = $system_info[$mods['system_ver_unique_id']]['system_id']; //add system id to temp arr
		$tpl_arr_data['nickname'] = $system_info[$mods['system_ver_unique_id']]['nickname']; //add system nickname to temp arr
		
		switch($mods['status']){
			case 'new':
				$tpl_arr_data['status'] = "System Primary Assigned";
				$tpl_arr_data['change'] = "Primary Engineer ".eng_assignment($mods['system_ver_unique_id'],'pri');
				break;
			case 'removed':
				$tpl_arr_data['status'] = "System Primary Unassigned";
				$tpl_arr_data['change'] = "";
				break;
			case 'db':
				$tpl_arr_data['status'] = "Primary Assignment changed";
				$tpl_arr_data['change'] = $engineers[$mods['xml']] ." to ". $engineers[$mods['db']];
				break;
			case 'xml':
				$tpl_arr_data['status'] = "Primary Assignment changed";
				$tpl_arr_data['change'] = $engineers[$mods['xml']] ." to ". $engineers[$mods['db']];
				break;			
		}
		array_push($tpl_arr,$tpl_arr_data);
	}
	
	foreach($mods_sec as $mods) {
		$tpl_arr_data = array();
		if($mods['status'] == 'xml'){
			continue;	
		}
		
		$tpl_arr_data['system_id'] = $system_info[$mods['system_ver_unique_id']]['system_id']; //add system id to temp arr
		$tpl_arr_data['nickname'] = $system_info[$mods['system_ver_unique_id']]['nickname']; //add system nickname to temp arr
		
		switch($mods['status']){
			case 'new':
				$tpl_arr_data['status'] = "System Secondary Assigned";
				$tpl_arr_data['change'] = "Secondary Engineer ".eng_assignment($mods['system_ver_unique_id'],'sec');
				break;
			case 'removed':
				$tpl_arr_data['status'] = "System Secondary Unassigned";
				$tpl_arr_data['change'] = "";
				break;
			case 'db':
				$tpl_arr_data['status'] = "Secondary Assignment changed";
				$tpl_arr_data['change'] = $engineers[$mods['xml']] ." to ". $engineers[$mods['db']];
				break;
			case 'xml':
				$tpl_arr_data['status'] = "Secondary Assignment changed";
				$tpl_arr_data['change'] = $engineers[$mods['xml']] ." to ". $engineers[$mods['db']];
				break;			
		}
		array_push($tpl_arr,$tpl_arr_data);
	}
	
	if($debug){echo "Template Array: <pre>",print_r($tpl_arr),"</pre>",EOL;}
	
	$smarty->assign('date_from',$nowPretty);
	$smarty->assign('date_to',$rowPrev['date']);
	$smarty->assign('rows',$tpl_arr);	
	$smarty->assign('company_name',$settings->company_name);
	$smarty->assign('copyright_date',$settings->copyright_date);
	$smarty->assign('email_pics',$settings->email_pics);
	
	echo "Sending Engineer Assignment Changes Email...",EOL;
	
	//load phpmailer
	require_once($docroot.'/resources/PHPMailer/PHPMailerAutoload.php');
	
	$sql="SELECT u.name, u.email
	FROM users AS u
	LEFT JOIN users_perms AS perm ON perm.uid = u.uid
	LEFT JOIN users_perm_id AS permid ON permid.id = perm.pid
	LEFT JOIN users_prefs AS pref ON pref.uid = u.uid
	LEFT JOIN users_pref_id AS prefid ON prefid.id = pref.pid
	WHERE permid.perm = 'perm_rcv_assignment_change' AND prefid.pref = 'pref_rcv_assignment_change' AND u.active = 'y';";
	$resultEmails = $mysqli->query($sql);
	
	$email_result = array();
	
	//Engineer Request Email
	$mail = new PHPMailer;
	$mail->IsSMTP();                                      // Set mailer to use SMTP
	$mail->Host = (string)$settings->email_host;  // Specify main and backup server
	$mail->SMTPAuth = true;                               // Enable SMTP authentication
	$mail->Username = (string)$settings->email_support;    // SMTP username
	$mail->Password = (string)$settings->email_password;                           // SMTP password
	$mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted
	$mail->From = (string)$settings->email_support;
	$mail->FromName = 'Support '.$settings->short_name.' Portal';
	while($rowEmails = $resultEmails->fetch_assoc()) {
		$mail->AddAddress(trim($rowEmails['email']),$rowEmails['name']);  // Add a recipient
		array_push($email_result,$rowEmails['email']);
	}
	$mail->AddReplyTo((string)$settings->email_support, 'Support '.$settings->short_name.' Portal');
	$mail->AddCC((string)$settings->email_support);
	$mail->IsHTML(true);                                  // Set email format to HTML
	$mail->Subject = 'Engineer Assignment Changes '.$nowPretty;
	$mail->Body = $smarty->fetch('system_assignments.tpl');

	if($debug){
		echo "Email:",EOL,$mail->Body,EOL;	
	}
	
	if($send){
		if(!$mail->Send()) {
		   echo 'Email could not be sent.',EOL;
		   echo 'Mailer Error: ' . $mail->ErrorInfo,EOL;
		   //exit;
		}
	}else{
		echo "Debug: Not sending email",EOL;	
	}
	
	
	echo "Email addresses sent to:",EOL;
	foreach($email_result as $value){
		echo $value,EOL;
	}
	
	return true;	
}

function eng_assignment($system_ver_unique_id,$pos){
	global $mysqli;
	switch($pos){
		case 'pri':
			$sql="SELECT u.name
			FROM systems_assigned_pri AS a
			LEFT JOIN users AS u ON u.uid = a.uid
			WHERE a.system_ver_unique_id = '$system_ver_unique_id';";	
			break;
		case 'sec':
			$sql="SELECT u.name
			FROM systems_assigned_sec AS a
			LEFT JOIN users AS u ON u.uid = a.uid
			WHERE a.system_ver_unique_id = '$system_ver_unique_id';";
			break;
	}
	if(!$result = $mysqli->query($sql)){
		return 'NA';	
	}
	$row = $result->fetch_assoc();
	return $row['name'];
}

function makexml(){
	global $mysqli;
	global $newxml;
	global $newfile;
	
	$newxml->addAttribute('filename',$newfile);
	
	$table = $newxml->addChild('table');
	$table->addAttribute('table','primary');
	
	$sql="SELECT * FROM systems_assigned_pri ORDER BY id ASC;";
	$resultSystem = $mysqli->query($sql);
	while($rowSystem = $resultSystem->fetch_assoc()){
		$system = $table->addChild('system');
		$system->addAttribute('id',$rowSystem['id']);
		$system->addChild('system_ver_unique_id',$rowSystem['system_ver_unique_id']);
		$system->addChild('uid',$rowSystem['uid']);

	}
	
	$table = $newxml->addChild('table');
	$table->addAttribute('table','secondary');
	
	$sql="SELECT * FROM systems_assigned_sec ORDER BY id ASC;";
	$resultSystem = $mysqli->query($sql);
	while($rowSystem = $resultSystem->fetch_assoc()){
		$system = $table->addChild('system');
		$system->addAttribute('id',$rowSystem['id']);
		$system->addChild('system_ver_unique_id',$rowSystem['system_ver_unique_id']);
		$system->addChild('uid',$rowSystem['uid']);
	}
	
	$table = $newxml->addChild('table');
	$table->addAttribute('table','customer');
	
	$sql="SELECT * FROM systems_assigned_customer ORDER BY id ASC;";
	$resultSystem = $mysqli->query($sql);
	while($rowSystem = $resultSystem->fetch_assoc()){
		$system = $table->addChild('system');
		$system->addAttribute('id',$rowSystem['id']);
		$system->addChild('system_ver_unique_id',$rowSystem['system_ver_unique_id']);
		$system->addChild('uid',$rowSystem['uid']);
	}
	
	return true;
}


?>