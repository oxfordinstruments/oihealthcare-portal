<?php

$debug = false;
$send = true;
$remove_msg = true;


if($debug){
	ini_set('display_errors', '1'); // Set by installer
	ini_set('display_startup_errors', '1'); // Set by installer
	//error_reporting(E_ALL);
	error_reporting(E_ERROR | E_WARNING | E_PARSE);
}else{
	error_reporting(E_ERROR | E_WARNING | E_PARSE);
}

$no_define_eol = true;
if(substr(php_sapi_name(), 0, 3) == 'cli' || empty($_SERVER['REMOTE_ADDR'])) {
    //shell
	$cron = false;
	if(isset($_SERVER['argv'][1]) and $_SERVER['argv'][1] == '--cron'){
		$cron = true;
	}
	$docroot = preg_replace("/(public_html).*/",'public_html',dirname(__FILE__),2);
	$no_define_eol = true;
	define('EOL',PHP_EOL);
} else {
    //webserver
	$cron = false;
	$docroot = $_SERVER['DOCUMENT_ROOT'];
	define('EOL','<br />');
}

if($debug){echo "BEGIN",EOL;}

define("BAD_UPDATE", "Could not understand status update. Try again like: ct1234 67543 System is up");
define("BAD_UPDATE_SYSID", "System ID missing from update. Try again like: ct1234 67543 System is up");
define("BAD_UPDATE_REQID", "Request/Report ID missing from update. Try again like: ct1234 67543 System is up");
define("SQL_ERROR", "Error with status update. Contact support!");

$settings = new SimpleXMLElement($docroot.'/settings.xml', null, true);
require_once($docroot.'/define_inc.php');

require($docroot.'/mysqlInfo.php');
$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno) {
	die('There was an error running the query [' . $mysqli->connect_error . ']');
}

require_once($docroot.'/log/log.php');
$log = new logger();

require_once($docroot.'/report/common/scripts/php_utils.php');
$php_utils = new phpUtils($docroot);

require_once($docroot.'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

require_once($docroot.'/resources/PHPMailer/PHPMailerAutoload.php');
$phpmail = new PHPMailer;

require_once($docroot.'/resources/PHPimap/PhpImap/Mailbox.php');
require_once($docroot.'/resources/PHPimap/PhpImap/IncomingMail.php');

//$log->loginfo("Checking Updates Email",0,false,basename(__FILE__),__LINE__);

try{
	$imap = new PhpImap\Mailbox('{oihealthcareportal.com:143/novalidate-cert}INBOX', 'updates@oihealthcareportal.com','06#3Z507lXR#');
}catch(Exception $e) {
	$log->logerr('PhpImap Error ['.$e->getMessage().']',0,false,basename(__FILE__),__LINE__);
	die($e->getMessage());
}

$mailboxinfo = $imap->getMailboxInfo();
d($mailboxinfo);
if($mailboxinfo->Unread == 0){
	//$log->loginfo("Updates Email inbox empty",0,false,basename(__FILE__),__LINE__);
	s('Mailbox Empty');
	s('Cleaning-up mailbox');
	if($mailboxinfo->Nmsgs >= 1){
		$mailids = $imap->sortMails();
		foreach($mailids as $mailid){
			$imap->deleteMail($mailid);
			s("Deleting mail id: ".$mailid);
		}
	}
	if($debug){echo "DONE",EOL;}
	die();
}

$log->loginfo("Updates Email inbox count: ".$mailboxinfo->Unread,0,false,basename(__FILE__),__LINE__);

$carriers = array();
$sql="SELECT address FROM misc_carrier;";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	return_message($mail->fromAddress, SQL_ERROR);
}
while($row = $result->fetch_assoc()){
	array_push($carriers, ltrim($row['address'],'@'));
}
d($carriers);

$banned = array();
$sql="SELECT ban FROM admin_banlist WHERE domain IS NOT NULL 
UNION
SELECT ban FROM admin_banlist WHERE USER IS NOT NULL;";
if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);	
	return_message($mail->fromAddress, SQL_ERROR);
}
while($row = $result->fetch_assoc()){
	array_push($banned, $row['ban']);
}
d($banned);

d($imap->searchMailbox('UNSEEN'));
$unseen = $imap->searchMailbox('UNSEEN');
foreach($unseen as $mailid){
	if($debug){echo "<br><hr><br>";}
	d($imap->getMail($mailid,false));
	$mail = $imap->getMail($mailid,$remove_msg);
	
	//check for banned
	if(check_from_type($mail->fromAddress) == false){
		$log->loginfo("Banned domain/user tried to send update via email. Address: ".$mail->fromAddress,1008,false,basename(__FILE__),__LINE__);
		echo "Banned domain/user tried to send update via email. Address: ".$mail->fromAddress,EOL;
		s("BANNED DOMAIN / USER");
		continue;	
	}
	
	if(preg_match('/automatic reply:/i',$mail->subject)){
		$log->loginfo("Automatic Reply email found from Address: ".$mail->fromAddress,0,false,basename(__FILE__),__LINE__);
		s("Removing: Automatic Reply email from ".$mail->fromAddress);
		if($remove_msg){
			$imap->deleteMail($mailid);
		}
		continue;
	}
	
	
	$sysid_matches = array();
	$match = preg_match('/(?P<id>^[^\s]+)(?P<report> [^\s]+)(?P<sysstatus> [^\s]+)?/i',$mail->textPlain,$sysid_matches);
	d($sysid_matches);
	if($match == 1){
		s("SYSID: ".$sysid_matches['id']);
		s("REPORTID: ".$sysid_matches['report']);
		s("SYSSTATUS: ".$sysid_matches['sysstatus']);
		s("FROM: ".$mail->fromAddress);
		s("DATE: ".$mail->date);
		s("TEXT: ".$mail->textPlain);

		//check if system id exists
		$sql="SELECT unique_id, nickname FROM systems_base_cont WHERE system_id = '".$mysqli->real_escape_string(strtolower($sysid_matches['id']))."' AND property = 'C' LIMIT 1;";
		s($sql);
		if(!$resultSys = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			return_message($mail->fromAddress, SQL_ERROR);
			continue;
		}
		if($resultSys->num_rows == 0){
			if(check_from_type($mail->fromAddress) != false){
				$log->loginfo("FROM: ".$mail->fromAddress." MSG: ".$mail->textPlain,1073,false,basename(__FILE__),__LINE__);
				echo "Could not find system id. FROM: ".$mail->fromAddress." MSG: ".$mail->textPlain,EOL;
				return_message($mail->fromAddress, BAD_UPDATE_SYSID, $mail->textHtml);
				continue;
			}
			continue;
		}
		$rowSys = $resultSys->fetch_assoc();
		d($rowSys);

		$uid = 'unassigned';
		$from_email_arr = explode('@',$mail->fromAddress);
		d($from_email_arr);
		if(in_array($from_email_arr[1],$carriers)){
			$cell = $php_utils->localize_us_number($from_email_arr[0]);
			$sql="SELECT uid FROM users WHERE cell = '$cell' AND id > 9;";
		}else{
			$sql="SELECT uid FROM users WHERE email = '".$mysqli->real_escape_string(strtolower($mail->fromAddress))."' AND id > 9;";
		}
		s($sql);
		if(!$resultUid = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			return_message($mail->fromAddress, SQL_ERROR);
			continue;
		}
		if($resultUid->num_rows != 0){
			$rowUid = $resultUid->fetch_assoc();
			$uid = $rowUid['uid'];
		}
		
		$status = $mail->textPlain;
		$report_id = false;
		if(is_int(intval($sysid_matches['report']))){
			$sql="SELECT unique_id FROM systems_requests_ids WHERE request_id = '".$mysqli->real_escape_string(trim($sysid_matches['report']))."';";
			s($sql);
			if(!$resultReportId = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
				return_message($mail->fromAddress, SQL_ERROR);
				continue;
			}else{
				if($resultReportId->num_rows != 0){
					$rowReportId = $resultReportId->fetch_assoc();
					$report_id = $rowReportId['unique_id'];
					d($rowReportId);
					$status = str_replace($sysid_matches['report'],'',$status);
				}else{
					return_message($mail->fromAddress, BAD_UPDATE_REQID, $mail->textHtml);
					continue;
				}
			}
		}else{
			return_message($mail->fromAddress, BAD_UPDATE_REQID, $mail->textHtml);
			continue;
		}
		

		s("sysid_matches['sysstatus']: ".$sysid_matches['sysstatus']);
		switch(trim(strtolower($sysid_matches['sysstatus']))){
			case 'up':
				$sys_status = 1;
				$sys_status_txt = 'Up';
				break;
			case 'down':
			case 'dn':
				$sys_status = 2;
				$sys_status_txt = 'Down';
				break;
			case 'limited':
			case 'limit':
				$sys_status = 3;
				$sys_status_txt = 'Limited';
				break;
			default:
				$sys_status = 'NULL';
				$sys_status_txt = 'NA';
		}
		s("sys_status insert: ".$sys_status);
		
		
		$status = trim(ltrim(ltrim($status,$sysid_matches['id'])));
		if($sys_status != 'NULL'){
			$status = trim(ltrim(ltrim($status,$sysid_matches['sysstatus'])));
		}
		s("status: ".$status);
		//$pattern = "/(oxford.*)|(thanks.*)|(regard.*)|(sent from.*)|(thank you.*)|(\.\.)/im";
		$pattern = closing_pattern();
		$status_matches = array();
		if(preg_match($pattern,$status,$status_matches, PREG_OFFSET_CAPTURE) == 1){
			d($status_matches);
			$status = trim(substr($status,0,$status_matches[0][1]));
			s("status: ".$status);
		}
		
		if($status == ""){
			$log->loginfo("FROM: ".$mail->fromAddress." MSG: ".$mail->textPlain,1073,false,basename(__FILE__),__LINE__);
			echo "Status was blank. FROM:".$mail->fromAddress." MSG: ".$mail->textPlain,EOL;
			return_message($mail->fromAddress, BAD_UPDATE, $mail->textHtml);
			continue;	
		}
		

		$sql="INSERT INTO systems_service_journal (note, uid, system_status, date, request_report_unique_id, from_email) VALUES 
		('".$mysqli->real_escape_string($status)."', '$uid', ".$sys_status.", '".date(storef,strtotime($mail->date))."', '".$rowReportId['unique_id']."', '".$mail->fromAddress."');";
		s($sql);
		if(!$result = $mysqli->query($sql)){
			$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
			$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
			return_message($mail->fromAddress, SQL_ERROR);
			continue;
		}
		if($sys_status == 'NULL'){
			$log->loginfo("FROM: ".$mail->fromAddress." SYSTEMID: ".$sysid_matches['id']." NOTE: ".$status,0,false,basename(__FILE__),__LINE__);
		}else{
			$sql="UPDATE systems_base SET `status` = ".$sys_status." WHERE unique_id = '".$rowSys['unique_id']."';";
			s($sql);
			if(!$result = $mysqli->query($sql)){
				$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
				$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
				return_message($mail->fromAddress, SQL_ERROR);
				continue;
			}
			$log->loginfo("FROM: ".$mail->fromAddress." SYSTEMID: ".$sysid_matches['id']."STATUS: ".$sys_status_txt." NOTE: ".$status,0,false,basename(__FILE__),__LINE__);
		}
		s("Deleting mail id: ".$mailid);
		$imap->deleteMail($mailid);



	}else{
		//send response to bad status update
		if(!check_from_type($mail->fromAddress)){
			$log->loginfo("FROM: ".$mail->fromAddress." MSG: ".$mail->textPlain,1073,false,basename(__FILE__),__LINE__);
			echo "Bad Status Update: FROM: ".$mail->fromAddress." MSG: ".$mail->textPlain,EOL;
			return_message($mail->fromAddress, BAD_UPDATE, $mail->textHtml);
		}
	}
}
if($debug){echo "DONE",EOL;}

function return_message($recip, $message, $orig_msg = NULL){
	global $settings, $send, $debug, $phpmail;
	
	echo "Email to:", $recip, "  Mesage: ", $message, EOL;
	
	$type = check_from_type($recip);
	$sms = false;
	if($type == 1){
		$sms = true;	
	}
	
	$phpmail->IsSMTP();
	$phpmail->Host = (string)$settings->email_host;
	$phpmail->SMTPAuth = true;
	$phpmail->Username = (string)$settings->email_updates;
	$phpmail->Password = (string)$settings->email_password;
	$phpmail->SMTPSecure = 'tls';
	$phpmail->From = (string)$settings->email_updates;
	$phpmail->FromName = 'Updates '.$settings->short_name.' Portal';
	$phpmail->AddAddress((string)trim($recip));
	//$phpmail->AddCC((string)$settings->email_support);
	$phpmail->IsHTML(!$sms);
	
	$phpmail->Body = $message;
	if(!$sms){
		$phpmail->Subject = 'System Status Update';
		if(!is_null($orig_msg)){
			$phpmail->Body = $message."<br><br><hr><br>".$orig_msg;
		}
	}
	
	if($debug){
		echo $phpmail->Body,EOL;
	}
	
	//echo EOL,EOL,EOL,"<pre>",print_r($phpmail),"</pre>",EOL,EOL,EOL;
	
	if($send){
		if(!$phpmail->Send()) {
		   echo 'Email could not be sent.',EOL;
		   echo 'Mailer Error: ' . $phpmail->ErrorInfo,EOL;
		   exit;
		}else{
			s("Email Sent");
			echo "email sent",EOL;
		}
	}else{
		echo "Debug: Not sending email",EOL;	
	}
	
}

function check_from_type($from_address){
	global $carriers, $banned, $settings;
	$from_email_arr = explode('@',$from_address);
	switch($from_email_arr[1]){
		case $settings->base_url:
			s("Portal email return false");
			return false;
			break;
		case in_array($from_email_arr[0],$banned):
			s($from_email_arr[0]." banned return false");
			return false;
			break;
		case in_array($from_email_arr[1],$banned):
			s($from_email_arr[1]." banned return false");
			return false;
			break;
		case in_array($from_email_arr[1],$carriers):
			s("SMS email return 1");
			return 1;
			break;
		default:
			s("Default return 2");
			return 2;
	}
}

function closing_pattern(){
	$pattern = "/";	
	$closings = array(
	'(oxford.*)',
	'(thanks.*)',
	'(regard.*)',
	'(send from.*)',
	'(thank you.*)',
	'(yours faithfully.*)',
	'(kind regards.*)',
	'(warm regards.*)',
	'(yours truly.*)',
	'(sincerely.*)',
	'(yours sincerely.*)',
	'(best wishes.*)',
	'(ayo oyedotun.*)',
	'(cordially.*)',
	'(cheers.*)',
	'(happy holidays.*)',
	'(enjoy your weekend.*)',
	'(keep up the good work.*)',
	'(\.\.)'
	);
	$pattern .= implode('|',$closings);
	$pattern.="/im";
	return $pattern;		
}

?>