<?php
//Update Completed 12/12/14
//Removes orphaned hours
$no_define_eol = true;
if(substr(php_sapi_name(), 0, 3) == 'cli' || empty($_SERVER['REMOTE_ADDR'])) {
    //shell
	$cron = false;
	if(isset($_SERVER['argv'][1]) and $_SERVER['argv'][1] == '--cron'){
		$cron = true;
	}
	$docroot = preg_replace("/(public_html).*/",'public_html',dirname(__FILE__),2);
	$no_define_eol = true;
	define('EOL',PHP_EOL);
} else {
    //webserver
	$cron = false;
	$docroot = $_SERVER['DOCUMENT_ROOT'];
	define('EOL','<br />');
}


$settings = new SimpleXMLElement($docroot.'/settings.xml', null, true);
require_once($docroot.'/define_inc.php');

echo "Begin clean hours table ".date(phpdispfdt,time()),EOL;


require($docroot.'/mysqlInfo.php');
$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno) {die('There was an error running the query [' . $mysqli->connect_error . ']');}

$result = $mysqli->query("DELETE FROM systems_hours WHERE NOT EXISTS( SELECT NULL FROM systems_reports AS r WHERE r.report_id = hours.report_id);");
$result = $mysqli->query("DELETE FROM systems_hours WHERE hours.`date` = '';");
echo "Hours table cleaned ".date(phpdispfdt,time()),EOL;
echo "Records deleted: ".$mysqli->affected_rows." ".date(phpdispfdt,time()),EOL;
echo "DONE ".date(phpdispfdt,time()),EOL;
exit();
?>