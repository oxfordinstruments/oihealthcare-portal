<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 1/17/2017
 * Time: 12:43 PM
 */

error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
ini_set('display_errors', 'On');
$debug = false;


if(substr(php_sapi_name(), 0, 3) == 'cli' || empty($_SERVER['REMOTE_ADDR'])) {
	//shell
	$cron = false;
	if(isset($_SERVER['argv'][1]) and $_SERVER['argv'][1] == '--cron'){
		$cron = true;
	}
	$docroot = preg_replace("/(public_html).*/",'public_html',dirname(__FILE__),2);
	$no_define_eol = true;
	define('EOL',PHP_EOL);
} else {
	//webserver
	$cron = false;
	$docroot = $_SERVER['DOCUMENT_ROOT'];
	define('EOL','<br />');
}


$settings = new SimpleXMLElement($docroot.'/settings.xml', null, true);
require_once($docroot.'/define_inc.php');
require_once($docroot.'/mysqlInfo.php');

require_once($docroot.'/log/log.php');
$log = new logger($docroot);

require_once($docroot.'/report/common/scripts/php_utils.php');
$php_utils = new phpUtils($docroot, $debug);

require_once($docroot.'/resources/moment_php/Moment.php');
require_once($docroot.'/resources/moment_php/MomentException.php');
require_once($docroot.'/resources/moment_php/MomentHelper.php');
require_once($docroot.'/resources/moment_php/MomentLocale.php');
require_once($docroot.'/resources/moment_php/MomentFromVo.php');

require_once($docroot.'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

$lock_file = fopen($settings->php_base_dir.'/tmp/email_runner.pid', 'c');
$got_lock = flock($lock_file, LOCK_EX | LOCK_NB, $wouldblock);
if ($lock_file === false || (!$got_lock && !$wouldblock)) {
	$log->loginfo("Email Runner: Unexpected error opening or locking lock file. ",0,false,basename(__FILE__),__LINE__);
	throw new Exception(
		"Unexpected error opening or locking lock file. Perhaps you " .
		"don't  have permission to write to the lock file or its " .
		"containing directory?"
	);
}
else if (!$got_lock && $wouldblock) {
	if($debug){echo "Another instance is already running; terminating.\n";}
	$log->loginfo("Email Runner: Another instance is already running; terminating. ",0,false,basename(__FILE__),__LINE__);
	exit(1);
}

// Lock acquired; let's write our PID to the lock file for the convenience
// of humans who may wish to terminate the script.
ftruncate($lock_file, 0);
fwrite($lock_file, getmypid() . "\n");

$now = new \Moment\Moment();
$now->setImmutableMode(true);
if($debug){
	echo "NOW: ".$now->format(),EOL;
}

/*body of your script goes here.*/
$log->loginfo("Email Runner: Started pid = ".getmypid(),0,false,basename(__FILE__),__LINE__);

$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno) {die('There was an error running the query [' . $mysqli->connect_error . ']');}

$sql="SELECT id, script, args, schedule, failed FROM email_actions WHERE complete = 'n' and failed <= 4;";
if(!$result = $mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']');}
if($result->num_rows == 0){
	exit(0);
}
$log->loginfo("Email Runner: Started pid = ".getmypid(),0,false,basename(__FILE__),__LINE__);

$actions = array();
while($row = $result->fetch_assoc()){
	$actions[$row['id']] = array('script'=>$row['script'], 'args'=>$row['args'], 'schedule'=>$row['schedule'], 'complete'=>false, 'completed_time'=>false, 'failed'=>$row['failed']);
}
if($debug){echo print_r($actions),EOL;}
$log->loginfo("Email Runner: Actions to perform = ".count($actions),0,false,basename(__FILE__),__LINE__);

$errors = array();
$script_base = $settings->full_url."/";
foreach($actions as $key=>$action){

	try{
		$schedule = new Moment\Moment($action['schedule'], NULL, true);
		if($debug){
			echo "Scheduled: ",$schedule->format(), EOL;
		}
		$log->loginfo("Email Runner: Scheduled for ".$schedule->format(),0,false,basename(__FILE__),__LINE__);
		if($now < $schedule){
			if($debug){
				echo "Not time to run action ",$key,EOL;
			}
			$log->loginfo("Email Runner: Not time to run action ".$key,0,false,basename(__FILE__),__LINE__);
			continue;
		}
	}
	catch(\Moment\MomentException $momentException) {
		if($debug){
			echo "Schedule is 0",EOL;
		}
	}

	$script = $script_base.$action['script'];
	//$script = str_replace('//','/',$script);
	if($debug){echo "Executing: ".$script,EOL;}
	$log->loginfo("Email Runner Executing: ".$script,0,false,basename(__FILE__),__LINE__);
	$args = json_decode($action['args'],true);
	if($debug){echo "Args: ".print_r($args),EOL;}
	d($args);

	$php_utils->http_build_query_for_curl($args,$post);

	$curl = new Curl($script, array(
		CURLOPT_POSTFIELDS => $post,
		CURLOPT_HEADER => true,
		CURLOPT_TIMEOUT => 90,
		CURLOPT_RETURNTRANSFER => 1,
		//CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_SSL_VERIFYHOST =>  0
	));
	$curl_response = false;
	$curl_code = false;
	try {
		$curl_response = $curl->getResponse();
		$curl_code = $curl->getCode();
		if($debug){echo "cURL Response Header: ",$curl_response['header'],EOL;}
		if($debug){echo "cURL Response Body: ",$curl_response['body'],EOL;}
		if($debug){echo "cURL Code: ",$curl_code,EOL;}
		if($curl_response['body'] != "OK"){
			throw new Exception("Failed to process action id: ".$key);
		}
		$sql="UPDATE email_actions SET complete = 'Y', executed_time = NOW(), completed_time = NOW() WHERE id = ?;";
		$stmt = $mysqli->prepare($sql);
		$stmt->bind_param('i',$key);
		$stmt->execute();
		if($debug){echo "Completed action: ".$key,EOL;}
		$log->loginfo("Email Runner Completed action: ".$key,0,false,basename(__FILE__),__LINE__);
	} catch (\RuntimeException $ex) {
		$log->loginfo(sprintf('Email Runner HTTP ERROR: %s CODE: %d', $ex->getMessage(), $ex->getCode()),0,false,basename(__FILE__),__LINE__);
		echo sprintf('HTTP ERROR: %s CODE: %d', $ex->getMessage(), $ex->getCode()),EOL;
		$failed = $action['failed'] + 1;
		$data = array('action'=>$key, 'script'=>$script, 'response'=>$curl_response['body'], 'http_code'=>$curl_code, 'exception'=>sprintf('HTTP ERROR: %s CODE: %d', $ex->getMessage(), $ex->getCode()));
		array_push($errors, $data);
		$sql="UPDATE email_actions SET failed = ?, failure = ?, executed_time = NOW() WHERE id = ?;";
		$stmt = $mysqli->prepare($sql);
		$stmt->bind_param('isi', $failed, json_encode($data), $key);
		$stmt->execute();
		if($debug){echo "Failed action: ".$key,EOL;}
	} catch(Exception $e) {
		$log->loginfo("Email Runner: Failed to process action id: ".$key,0,false,basename(__FILE__),__LINE__);
		echo sprintf('SCRIPT ERROR: %s CODE: %d', $e->getMessage(), $e->getCode()),EOL;
		$failed = $action['failed'] + 1;
		$data = array('action'=>$key, 'script'=>$script, 'response'=>$curl_response['body'], 'http_code'=>$curl_code, 'exception'=>sprintf('SCRIPT ERROR: %s CODE: %d', $e->getMessage(), $e->getCode()));
		array_push($errors, $data);
		$sql="UPDATE email_actions SET failed = ?, failure = ?, executed_time = NOW() WHERE id = ?;";
		$stmt = $mysqli->prepare($sql);
		$stmt->bind_param('isi', $failed, json_encode($data), $key);
		$stmt->execute();
		if($debug){echo "Failed action: ".$key,EOL;}
	}
	unset($curl);
}

if(count($errors) > 0){
	echo EOL,EOL,"ERRORS",EOL,EOL;
	foreach($errors as $error){
		echo "Action: ".$error['action'],EOL;
		echo "Script: ".$error['script'],EOL;
		echo "Response: ".$error['response'],EOL;
		echo "HTTP Code: ".$error['http_code'],EOL;
		echo "Exception: ".$error['exception'],EOL,EOL;
	}
}


// All done; we blank the PID file and explicitly release the lock
// (although this should be unnecessary) before terminating.
ftruncate($lock_file, 0);
flock($lock_file, LOCK_UN);



class Curl
{
	/** @var resource cURL handle */
	private $ch;

	/** @var mixed The response */
	private $response = false;

	private $code = false;

	/**
	 * @param string $url
	 * @param array  $options
	 */
	public function __construct($url, array $options = array())
	{
		$this->ch = curl_init($url);

		foreach ($options as $key => $val) {
			curl_setopt($this->ch, $key, $val);
		}

		curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
	}

	/**
	 * Get the response
	 * @return string
	 * @throws \RuntimeException On cURL error
	 */
	public function getResponse()
	{
		if ($this->response) {
			return $this->response;
		}

		$response = curl_exec($this->ch);
		$error    = curl_error($this->ch);
		$errno    = curl_errno($this->ch);

		$this->setCode();

		$header_size = curl_getinfo($this->ch, CURLINFO_HEADER_SIZE);
		$header = substr($response, 0, $header_size);
		$body = substr($response, $header_size);

		if (is_resource($this->ch)) {
			curl_close($this->ch);
		}

		if (0 !== $errno) {
			throw new \RuntimeException($error, $errno);
		}

		return $this->response = array('header'=>$header, 'body'=>$body);
	}

	private function setCode()
	{
		$this->code = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
	}

	public function getCode()
	{
		return $this->code;
	}

	public function clearCode()
	{
		$this->code = false;
	}
	/**
	 * Let echo out the response
	 * @return string
	 */
	public function __toString()
	{
		return $this->getResponse();
	}
}

?>