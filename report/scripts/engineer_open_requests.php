<?php

$no_define_eol = true;
if(substr(php_sapi_name(), 0, 3) == 'cli' || empty($_SERVER['REMOTE_ADDR'])) {
    //shell
	$cron = false;
	if(isset($_SERVER['argv'][1]) and $_SERVER['argv'][1] == '--cron'){
		$cron = true;
	}
	$docroot = preg_replace("/(public_html).*/",'public_html',dirname(__FILE__),2);
	$no_define_eol = true;
	define('EOL',PHP_EOL);
} else {
    //webserver
	$cron = false;
	$docroot = $_SERVER['DOCUMENT_ROOT'];
	define('EOL','<br />');
}

$debug = false;
if(isset($_GET['debug'])){
	$debug = true;
	if(isset($_GET['send'])){$send = true;}else{$send = false;}
}else{
	$send = true;
}

$settings = new SimpleXMLElement($docroot.'/settings.xml', null, true);

$no_define_eol = true;
require_once($docroot.'/define_inc.php');

echo "Begin Engineer's Open Request Check ".date(phpdispfdt,time()),EOL;

if($settings->disable_email == '1'){
	$send = false;	
}

//if (!@require_once($docroot.'/resources/kint/Kint.class.php')) {
//	require_once '/resources/kint/Kint.class.php';	
//}
//if(!$debug){
//	Kint::enabled(false);	
//}

require_once($docroot.'/resources/smarty/libs/Smarty.class.php');
$smarty = new Smarty();
$smarty->setTemplateDir($docroot.$settings->email_templates);
$smarty->setCompileDir($docroot.$settings->email_template_c);
$smarty->setCacheDir($docroot.$settings->email_cache);
$smarty->setConfigDir($docroot.$settings->email_configs);
$smarty->use_sub_dirs = true;
$smarty->force_compile = true;
//$smarty->debugging = true;
$smarty->caching = false;

require($docroot.'/mysqlInfo.php');
$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno) {die('There was an error running the query [' . $mysqli->connect_error . ']');}

echo "Getting engineers",EOL;
$engineers = array();
$sql="SELECT u.uid, u.name, u.email
FROM users AS u
LEFT JOIN users_groups AS ug ON ug.uid = u.uid
LEFT JOIN users_group_id AS gid ON gid.id = ug.gid
LEFT JOIN users_roles AS ur ON ur.uid = u.uid
LEFT JOIN users_role_id AS rid ON rid.id = ur.rid
WHERE (rid.role = 'role_engineer' OR rid.role = 'role_contractor') AND u.id > 9 AND u.active = 'y'
ORDER BY gid.`group` DESC, u.uid ASC;";
if(!$resultEngineers = $mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']');}

while($rowEngineers = $resultEngineers->fetch_assoc()){
	$engineers[$rowEngineers['uid']] = array("name"=>$rowEngineers['name'], 'email'=>$rowEngineers['email'], 'requests'=>array());
}

echo "Getting requests",EOL;
$sql = "SELECT sq.request_num, sq.system_nickname, sq.engineer, sq.initial_call_date,
IF(ss2.id IS NOT NULL, ss2.`status`, ss1.`status`) AS `status`, 
IF(ss2.id IS NOT NULL, ss2.id, ss1.id) AS status_id
FROM systems_requests AS sq
LEFT JOIN systems_status AS ss1 ON ss1.id = sq.system_status
LEFT JOIN systems_reports AS sp ON sp.report_id = sq.request_num
LEFT JOIN systems_status AS ss2 ON ss2.id = sp.system_status
WHERE sq.`status` = 'open' 
AND sq.deleted = 'n'
ORDER BY sq.request_num ASC;";
if(!$resultRequests = $mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']');}

if($resultRequests->num_rows == 0){
	echo "No open requests",EOL;	
	echo "DONE ".date(phpdispfdt,time()),EOL;
	exit();
}

while($rowRequests = $resultRequests->fetch_assoc()) {
	
	if(strtolower($rowRequests['status_id']) == '2'){
		$color = "#FF7575";
	}else{
		$color = "#FFFFFF";
	}	
	
	$initial_call_date = empty($rowRequests['initial_call_date'])?'':$rowRequests['initial_call_date'];
	$engineers[$rowRequests['engineer']]['requests'][$rowRequests['request_num']] = array('system_nickname'=>$rowRequests['system_nickname'],
																						  'initial_call_date'=>$initial_call_date,
																						  'status'=>$rowRequests['status'],
																						  'system_ver_unique_id'=>$rowRequests['system_ver_unique_id'],
																						  'color'=>$color);
}

echo "Generating Data",EOL;
foreach($engineers as $engineer=>$data){
	if(count($data['requests']) == 0){
		unset($engineers[$engineer]);
	}
	if($engineer == 'unassigned'){
		unset($engineers[$engineer]);	
	}
	if($engineer == 'contractor'){
		unset($engineers[$engineer]);	
	}
}

//d($engineers);

echo "Sending open requests emails...",EOL;
require_once($docroot.'/resources/PHPMailer/PHPMailerAutoload.php');

$email_result = array();

$mail = new PHPMailer;
$mail->IsSMTP();                                      // Set mailer to use SMTP
$mail->Host = (string)$settings->email_host;  // Specify main and backup server
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = (string)$settings->email_support;    // SMTP username
$mail->Password = (string)$settings->email_password;                           // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted
$mail->From = (string)$settings->email_support;
$mail->FromName = 'Support '.$settings->short_name.' Portal';
$mail->IsHTML(true);                                  // Set email format to HTML
$mail->Subject = 'Open Service Requests Report';

foreach($engineers as $key=>$data){
	$mail->ClearAllRecipients();
	
	$smarty->assign('user',$data['name']);
	$smarty->assign('oih_url',$settings->full_url);
	$smarty->assign('date',date(storef,time()));
	$smarty->assign('data',$engineers[$key]['requests']);
	$smarty->assign('company_name',$settings->company_name);
	$smarty->assign('copyright_date',$settings->copyright_date);
	$smarty->assign('email_pics',$settings->email_pics);
	
	
	if($debug){
		$mail->AddAddress('justin.davis@oxinst.com','Justin Davis');
		array_push($email_result,"justin.davis@oxinst.com");
	}else{
		$mail->AddAddress(trim($data['email']),$data['name']);
		array_push($email_result,$data['email']);
	}
	$mail->AddReplyTo((string)$settings->email_support, 'Support '.$settings->short_name.' Portal');
	//$mail->AddCC((string)$settings->email_support);
	$mail->Body = $smarty->fetch('engineer_open_requests.tpl');
	
	if($debug){
		echo EOL,"<pre>",print_r($data),"</pre>",EOL;
		echo $mail->Body,EOL;
	}
	
	if($send){
		if(!$mail->Send()) {
		   echo 'Email could not be sent.',EOL;
		   echo 'Mailer Error: ' . $mail->ErrorInfo,EOL;
		   exit;
		}
	}else{
		echo "Debug: Not sending email",EOL;	
	}
}

echo "Email addresses sent to:",EOL;
//echo $email_result;
foreach($email_result as $value){
	echo $value,EOL;
}

$mysqli->close();


echo "DONE ".date(phpdispfdt,time()),EOL;
exit();
?>