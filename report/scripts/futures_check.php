<?php

$no_define_eol = true;
if(substr(php_sapi_name(), 0, 3) == 'cli' || empty($_SERVER['REMOTE_ADDR'])) {
    //shell
	$cron = false;
	if(isset($_SERVER['argv'][1]) and $_SERVER['argv'][1] == '--cron'){
		$cron = true;
	}
	$docroot = preg_replace("/(public_html).*/",'public_html',dirname(__FILE__),2);
	$no_define_eol = true;
	define('EOL',PHP_EOL);
} else {
    //webserver
	$cron = false;
	$docroot = $_SERVER['DOCUMENT_ROOT'];
	define('EOL','<br>');
}

$debug = false;
if(isset($_GET['debug'])){
	$debug = true;
	if(isset($_GET['send'])){$send = true;}else{$send = false;}
}else{
	$send = true;
}

$single = false;
if(isset($_GET['single'])){
	$single = true;
	require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');
}


$settings = new SimpleXMLElement($docroot.'/settings.xml', null, true);

$no_define_eol = true;
require_once($docroot.'/define_inc.php');

echo "Begin Future Systems Check ".date(phpdispfdt,time()),EOL;

if($settings->disable_email == '1'){
	$send = false;	
}

require_once($docroot.'/resources/smarty/libs/Smarty.class.php');
$smarty = new Smarty();
$smarty->setTemplateDir($docroot.$settings->email_templates);
$smarty->setCompileDir($docroot.$settings->email_template_c);
$smarty->setCacheDir($docroot.$settings->email_cache);
$smarty->setConfigDir($docroot.$settings->email_configs);
$smarty->use_sub_dirs = true;
$smarty->force_compile = true;
//$smarty->debugging = true;
$smarty->caching = false;

require($docroot.'/mysqlInfo.php');
$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno) {die('There was an error running the query [' . $mysqli->connect_error . ']');}

echo "Getting recipients",EOL;
$emails = array();

if(!$single){
	$sql = "SELECT u.name,u.email
	FROM users AS u
	INNER JOIN users_prefs AS upr ON upr.uid = u.uid
	INNER JOIN users_pref_id AS prid ON prid.id = upr.pid AND prid.pref = 'pref_rcv_futsysmov'
	INNER JOIN users_perms AS up ON up.uid = u.uid
	INNER JOIN users_perm_id AS pmid ON pmid.id = up.pid AND pmid.perm = 'perm_rcv_futsysmov'
	WHERE u.active = 'y';";
	if(!$resultEmails = $mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']');}
	
	while($rowEmail = $resultEmails->fetch_assoc()){
		array_push($emails,array("email"=>$rowEmail['email'],"name"=>$rowEmail['name']));
	}
}else{
	array_push($emails,array("email"=>$_SESSION['userdata']['email'],"name"=>$_SESSION['userdata']['name']));
}

echo "Getting future systems ready to move",EOL;
$sql = "SELECT sbc.ver_unique_id, sbc.system_id, sbc.nickname, sbc.arrival_date, 
sbc.contract_start_date, sbc.contract_end_date, sbc.warranty_start_date, sbc.warranty_end_date,
IF(STR_TO_DATE(sbc.arrival_date,'%Y-%m-%dT%H:%i:%sZ')<= CURDATE(),1,0) AS overdue
FROM systems_base_cont AS sbc
WHERE sbc.property = 'F' 
AND STR_TO_DATE(sbc.arrival_date,'%Y-%m-%dT%H:%i:%sZ') <= DATE_ADD(CURDATE(), INTERVAL 30 DAY);";
if(!$resultFutures = $mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']');}

if($resultFutures->num_rows == 0){
	echo "No future systems needing moved",EOL;	
	echo "DONE ".date(phpdispfdt,time()),EOL;
	exit();
}

$data = array();
while($rowFutures = $resultFutures->fetch_assoc()) {
	echo "System ID: ".$rowFutures['system_id']." Arrival Date: ".$rowFutures['arrival_date']." Overdue: ".$rowFutures['overdue'],EOL;
	
	if(strtolower($rowFutures['overdue']) == '1'){
		$color = "#FF7575";
		$status = "Overdue";
	}else{
		$color = "#FFFFFF";
		$status = "";
	}
		
	$contract_start_date = empty($rowFutures['contract_start_date'])?'':date(phpdispfd,strtotime($rowFutures['contract_start_date']));
	$contract_end_date = empty($rowFutures['contract_end_date'])?'':date(phpdispfd,strtotime($rowFutures['contract_end_date']));
	$warranty_start_date = empty($rowFutures['warranty_start_date'])?'':date(phpdispfd,strtotime($rowFutures['warranty_start_date']));
	$warranty_end_date = empty($rowFutures['warranty_end_date'])?'': date(phpdispfd,strtotime($rowFutures['warranty_end_date']));
	$arrival_date = empty($rowFutures['arrival_date'])?'':date(phpdispfd,strtotime($rowFutures['arrival_date']));
	
	$data[$rowFutures['ver_unique_id']] = array('system_id'=>$rowFutures['system_id'], 'nickname'=>$rowFutures['nickname'], 'arrival_date'=>$arrival_date, 
	'contract_start_date'=>$contract_start_date, 'contract_end_date'=>$contract_end_date, 'warranty_start_date'=>$warranty_start_date, 'warranty_end_date'=>$warranty_end_date,
	'color'=>$color, 'status'=>$status);
}

$smarty->assign('date',date(storef,time()));
$smarty->assign('data',$data);
$smarty->assign('company_name',$settings->company_name);
$smarty->assign('copyright_date',$settings->copyright_date);
$smarty->assign('email_pics',$settings->email_pics);

echo "Sending future systems email...",EOL;


require_once($docroot.'/resources/PHPMailer/PHPMailerAutoload.php');

$email_result = array();

//Engineer Request Email
$mail = new PHPMailer;
$mail->IsSMTP();                                      // Set mailer to use SMTP
$mail->Host = (string)$settings->email_host;  // Specify main and backup server
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = (string)$settings->email_support;    // SMTP username
$mail->Password = (string)$settings->email_password;                           // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted
$mail->From = (string)$settings->email_support;
$mail->FromName = 'Support '.$settings->short_name.' Portal';
if($debug){
	$mail->AddAddress('justin.davis@oxinst.com','Justin Davis');
	array_push($email_result,"justin.davis@oxinst.com");
}else{
	foreach($emails as $email){
		$mail->AddAddress(trim($email['email']),$email['name']);  // Add a recipient
		array_push($email_result,$email['email']);
	}
}
$mail->AddReplyTo((string)$settings->email_support, 'Support '.$settings->short_name.' Portal');
if(!$single){
	$mail->AddCC((string)$settings->email_support);
}
$mail->IsHTML(true);                                  // Set email format to HTML
$mail->Subject = 'Future Systems Report';
$mail->Body = $smarty->fetch('future_systems_moving.tpl');

if($debug){
	echo EOL,"<pre>",print_r($data),"</pre>",EOL;
	echo $mail->Body,EOL;
}

if($send){
	if(!$mail->Send()) {
	   echo 'Email could not be sent.',EOL;
	   //$email_result = "Email could not be sent!";
	   echo 'Mailer Error: ' . $mail->ErrorInfo,EOL;
	   exit;
	}
}else{
	echo "Debug: Not sending email",EOL;	
}


echo "Email addresses sent to:",EOL;
//echo $email_result;
foreach($email_result as $value){
	echo $value,EOL;
}

$mysqli->close();


echo "DONE ".date(phpdispfdt,time()),EOL;
exit();
?>