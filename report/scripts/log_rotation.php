<?php
//Update Completed 12/12/14
//Run every 1 days m-f
$no_define_eol = true;
if(substr(php_sapi_name(), 0, 3) == 'cli' || empty($_SERVER['REMOTE_ADDR'])) {
    //shell
	$cron = false;
	if(isset($_SERVER['argv'][1]) and $_SERVER['argv'][1] == '--cron'){
		$cron = true;
	}
	$docroot = preg_replace("/(public_html).*/",'public_html',dirname(__FILE__),2);
	$no_define_eol = true;
	define('EOL',PHP_EOL);
} else {
    //webserver
	$cron = false;
	$docroot = $_SERVER['DOCUMENT_ROOT'];
	define('EOL','<br />');
}


$maxsize = 512000; //Max filesize in bytes (e.q. 2MB)
$dir = $docroot.'/log/';
$archive_dir = $docroot.'/log/archive/';
$msglog = 'messagelog.txt';
$infolog = 'infolog.txt';
$errlog = 'errorlog.txt';
$ldaplog = 'ldaplog.txt';


$settings = new SimpleXMLElement($docroot.'/settings.xml', null, true);
require_once($docroot.'/define_inc.php');

echo "Begin log rotation check ".date(phpdispfdt,time()),EOL,EOL;
$now = date(storef,time());

$filename = $msglog;
echo "Checking ".$filename.EOL;
if(file_exists($dir.$filename) && filesize($dir.$filename) > $maxsize){
	rename($dir.$filename,$archive_dir.strtok($filename,'.').'_'.$now.'.txt');
	echo 'Archived '.$filename.' as '.strtok($filename,'.').'_'.$now.'.txt',EOL;
}

$filename = $infolog;
echo "Checking ".$filename.EOL;
if(file_exists($dir.$filename) && filesize($dir.$filename) > $maxsize){
	rename($dir.$filename,$archive_dir.strtok($filename,'.').'_'.$now.'.txt');
	echo 'Archived '.$filename.' as '.strtok($filename,'.').'_'.$now.'.txt',EOL;
}

$filename = $errlog;
echo "Checking ".$filename.EOL;
if(file_exists($dir.$filename) && filesize($dir.$filename) > $maxsize){
	rename($dir.$filename,$archive_dir.strtok($filename,'.').'_'.$now.'.txt');
	echo 'Archived '.$filename.' as '.strtok($filename,'.').'_'.$now.'.txt',EOL;
}

$filename = $ldaplog;
echo "Checking ".$filename.EOL;
if(file_exists($dir.$filename) && filesize($dir.$filename) > $maxsize){
	rename($dir.$filename,$archive_dir.strtok($filename,'.').'_'.$now.'.txt');
	echo 'Archived '.$filename.' as '.strtok($filename,'.').'_'.$now.'.txt',EOL;
}

echo EOL,"DONE ".date(phpdispfdt,time()),EOL;
exit();
?>