<?php
/**
* Send nps survey emails and update nps_data table
*
* Must be setup on a cron job to run once a day at 13:00 EST
*
* PHP version 5.3
*
* @author     Justin Davis <evotodi@gmail.com>
* @version    2.0
*/

$send = true;//-----------<< Send Emails
$debug = false;//-----------<< DEBUG
$use_fake_date = false;//-----------<< Use fake date to send
$fake_date = "2016-07-15";//------------<< Fake Date yyyy-mm-dd
$fake_expire = "2016-07-31";//------------<< Fake Expire Date yyyy-mm-dd
$show_email = false;//-----------<< Show email layout
$sql_limit = false;//-----------<< limit sql queries to sql_limit_num
$sql_limit_num = "5";//-----------<< number to limit sql queries to when sql_limit is true

$no_define_eol = true;
if(substr(php_sapi_name(), 0, 3) == 'cli' || empty($_SERVER['REMOTE_ADDR'])) {
    //shell
	$cron = false;
	if(isset($_SERVER['argv'][1]) and $_SERVER['argv'][1] == '--cron'){
		$cron = true;
	}
	$docroot = preg_replace("/(public_html).*/",'public_html',dirname(__FILE__),2);
	$no_define_eol = true;
	define('EOL',PHP_EOL);
} else {
    //webserver
	$cron = false;
	$docroot = $_SERVER['DOCUMENT_ROOT'];
	define('EOL','<br />');
}

require_once($docroot.'/resources/smarty/libs/Smarty.class.php');

$settings = new SimpleXMLElement($docroot.'/settings.xml', null, true);
$nps_settings = new SimpleXMLElement($docroot.'/nps_settings.xml', null, true);
require_once($docroot.'/define_inc.php');

echo "Begin NPS Script ".date(phpdispfdt,time()),EOL;

$smarty = new Smarty();
$smarty->setTemplateDir($docroot.$settings->email_templates);
$smarty->setCompileDir($docroot.$settings->email_template_c);
$smarty->setCacheDir($docroot.$settings->email_cache);
$smarty->setConfigDir($docroot.$settings->email_configs);
$smarty->use_sub_dirs = true;
$smarty->force_compile = true;
//$smarty->debugging = true;
$smarty->caching = false;

require($docroot.'/mysqlInfo.php');
$mysqli = new mysqli("$host", "$username", "$password", "$db_name");

if ($mysqli->connect_errno) {
	$log->logerr($mysqli->connect_error,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support',1000,true,basename(__FILE__),__LINE__);	
}

echo "Checking if valid send date...",EOL,EOL;
echo "Quarter ends:",EOL;
echo $nps_settings->q1_end," - Q1",EOL;
echo $nps_settings->q2_end," - Q2",EOL;
echo $nps_settings->q3_end," - Q3",EOL;
echo $nps_settings->q4_end," - Q4",EOL,EOL;
echo "Days before quarter end to send survey = ".$nps_settings->send_days,EOL;
echo "Send Dates:",EOL;
echo date("m-d",strtotime("-".$nps_settings->send_days." day", strtotime($nps_settings->q1_end)))," - Q1",EOL;
echo date("m-d",strtotime("-".$nps_settings->send_days." day", strtotime($nps_settings->q2_end)))," - Q2",EOL;
echo date("m-d",strtotime("-".$nps_settings->send_days." day", strtotime($nps_settings->q3_end)))," - Q3",EOL;
echo date("m-d",strtotime("-".$nps_settings->send_days." day", strtotime($nps_settings->q4_end)))," - Q4",EOL,EOL;
echo "Days before quarter end to send reminder = ".$nps_settings->remind_days,EOL;
echo "Reminder Dates:",EOL;
echo date("m-d",strtotime("-".$nps_settings->remind_days." day", strtotime($nps_settings->q1_end)))," - Q1",EOL;
echo date("m-d",strtotime("-".$nps_settings->remind_days." day", strtotime($nps_settings->q2_end)))," - Q2",EOL;
echo date("m-d",strtotime("-".$nps_settings->remind_days." day", strtotime($nps_settings->q3_end)))," - Q3",EOL;
echo date("m-d",strtotime("-".$nps_settings->remind_days." day", strtotime($nps_settings->q4_end)))," - Q4",EOL,EOL;

if($use_fake_date){
	$cur_date = date(storef,strtotime($fake_date));
}else{
	$cur_date = date(storef,time());
}

echo "Current Date = ".$cur_date,EOL,EOL;
switch(date("m-d",strtotime($cur_date,time()))){
	case date("m-d",strtotime("-".$nps_settings->send_days." day", strtotime($nps_settings->q1_end))):
		echo "Today is Quarter 1 Send Date",EOL;
		if(!$use_fake_date){
			$expire = date(storef,strtotime($nps_settings->q1_end."/".date("Y",time())));
		}else{
			$expire = date(storef,strtotime($fake_expire));
		}
		$fiscal_quarter = 1;
		send_survey($mysqli);
		break;
	case date("m-d",strtotime("-".$nps_settings->send_days." day", strtotime($nps_settings->q2_end))):
		echo "Today is Quarter 2 Send Date",EOL;
		if(!$use_fake_date){
			$expire = date(storef,strtotime($nps_settings->q2_end."/".date("Y",time())));
		}else{
			$expire = date(storef,strtotime($fake_expire));
		}
		$fiscal_quarter = 2;
		send_survey($mysqli);
		break;
	case date("m-d",strtotime("-".$nps_settings->send_days." day", strtotime($nps_settings->q3_end))):
		echo "Today is Quarter 3 Send Date",EOL;
		if(!$use_fake_date){
			$expire = date(storef,strtotime($nps_settings->q3_end."/".date("Y",time())));
		}else{
			$expire = date(storef,strtotime($fake_expire));
		}
		$fiscal_quarter = 3;
		send_survey($mysqli);
		break;
	case date("m-d",strtotime("-".$nps_settings->send_days." day", strtotime($nps_settings->q4_end))):
		echo "Today is Quarter 4 Send Date",EOL;
		if(!$use_fake_date){
			$expire = date(storef,strtotime($nps_settings->q4_end."/".date("Y",time())));
		}else{
			$expire = date(storef,strtotime($fake_expire));
		}
		$fiscal_quarter = 4;
		send_survey($mysqli);
		break;
	case date("m-d",strtotime("-".$nps_settings->remind_days." day", strtotime($nps_settings->q1_end))):
		echo "Today is Quarter 1 Reminder Date",EOL;
		send_reminder($mysqli);
		update_data($mysqli);
		break;
	case date("m-d",strtotime("-".$nps_settings->remind_days." day", strtotime($nps_settings->q2_end))):
		echo "Today is Quarter 2 Reminder Date",EOL;
		send_reminder($mysqli);
		update_data($mysqli);
		break;
	case date("m-d",strtotime("-".$nps_settings->remind_days." day", strtotime($nps_settings->q3_end))):
		echo "Today is Quarter 3 Reminder Date",EOL;
		send_reminder($mysqli);
		update_data($mysqli);
		break;
	case date("m-d",strtotime("-".$nps_settings->remind_days." day", strtotime($nps_settings->q4_end))):
		echo "Today is Quarter 4 Reminder Date",EOL;
		send_reminder($mysqli);
		update_data($mysqli);
		break;	
	default:
		echo "Today is not a scheduled day to send the nps sureveys.",EOL; 
		update_data($mysqli);
		break;
}

$mysqli->close();
echo "DONE ".date(phpdispfdt,time()),EOL;
exit();

function randomCode() {
    $alphabet = "0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 6; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

function update_data(&$mysqli){
	echo EOL,"Update Data Function",EOL;
	$sql="SELECT * FROM nps_data ORDER BY id DESC LIMIT 2;";
	if(!$resultNPSData = $mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']');}
	while($rowNPSData = $resultNPSData->fetch_assoc()){
		echo "Updating nps_data id: ".$rowNPSData['unique_id'],EOL;
		echo "Fiscal Quarter: ".$rowNPSData['fiscal_quarter']."/".$rowNPSData['fiscal_year'],EOL;
		$quarter_unique_id = $rowNPSData['unique_id'];
		
		$sql = "SELECT score, count(*) AS count FROM nps_responses WHERE quarter_unique_id = '$quarter_unique_id' GROUP BY score ORDER BY COUNT ASC;";
		if(!$resultNPSRespScores = $mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']');}
		if($resultNPSRespScores->num_rows > 0){
			$scores = array(0,0,0,0,0,0,0,0,0,0,0);
			//VALUES('". implode("', '", $values) . "');";
			while($rowNPSRespScores = $resultNPSRespScores->fetch_assoc()){
				$scores[intval($rowNPSRespScores['score'])] = intval($rowNPSRespScores['count']);
			}
			//echo "Scores: ";print_r($scores);echo EOL;
			
			$nps_promoters = $scores[10] + $scores[9];
			$nps_passives = $scores[8] + $scores[7];
			$nps_demoters = array_sum(array_slice($scores,0,7));
			
			echo "Promoters: ".round(($nps_promoters/array_sum($scores))*100,2)."%  (".$nps_promoters.")",EOL;
			echo "Passives: ". round(($nps_passives/array_sum($scores))*100,2)."%  (".$nps_passives.")",EOL;
			echo "Demoters: ".round(($nps_demoters/array_sum($scores))*100,2)."%  (".$nps_demoters.")",EOL;
			
			$nps_score = round((($nps_promoters - $nps_demoters)/array_sum($scores))*100,2);
			echo "NPS Score: ". $nps_score."%",EOL;
			
			echo "Survey Send Date: ".$rowNPSData['survey_send_date'],EOL;
			
			$surveys_sent = $rowNPSData['surveys_sent'];
			echo "Surveys Sent: ". $surveys_sent,EOL;
			
			if(!$resultNPSResp = $mysqli->query("SELECT COUNT(*) AS count FROM nps_responses WHERE quarter_unique_id = '$quarter_unique_id';")){die('There was an error running the query [' . $mysqli->error . ']');}
			$rowNPSResp = $resultNPSResp->fetch_assoc();
			$survey_response =  $rowNPSResp['count'];
			echo "Survey Responses: ". $survey_response,EOL;
			
			$survey_response_percent = round(($survey_response / $surveys_sent)*100,2);
			echo "Survey Response Percent: ". $survey_response_percent."%",EOL;
			
			$sql = "UPDATE nps_data SET 
				`surveys_received`='$survey_response',
				`response_percent`='$survey_response_percent',
				`nps_score`='$nps_score',
				`promoters`='$nps_promoters',
				`passives`='$nps_passives',
				`demoters`='$nps_demoters',
				`nps_0`='".$scores[0]."',
				`nps_1`='".$scores[1]."',
				`nps_2`='".$scores[2]."',
				`nps_3`='".$scores[3]."',
				`nps_4`='".$scores[4]."',
				`nps_5`='".$scores[5]."',
				`nps_6`='".$scores[6]."',
				`nps_7`='".$scores[7]."',
				`nps_8`='".$scores[8]."',
				`nps_9`='".$scores[9]."',
				`nps_10`='".$scores[10]."'
				WHERE unique_id = '$quarter_unique_id';";
			if(!$result = $mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']');}	
			echo "nps_data table updated",EOL,EOL;
		}else{
			echo "No data to update",EOL,EOL;	
		}
	}
	return true;		
}

function send_survey(&$mysqli){
	global $cur_date, $settings, $nps_settings, $docroot;
	global $send, $debug, $use_fake_date, $fake_date, $fake_expire, $show_email, $smarty, $sql_limit, $sql_limit_num;
	global $mysqli, $fiscal_quarter, $expire;
	
	echo EOL,"Send Survey Function",EOL;
	
	$codes = array();
	$emails_sent_total = 0;
	
	if(!$smarty->templateExists('nps_survey.tpl')){
		die("Template does not exist: nps_survey.tpl");	
	}
	
	if($sql_limit){$limit = 'LIMIT '.$sql_limit_num;}
	
	if(isset($_GET['unique_id'])){
		$sql="SELECT sb.system_id, sbc.nickname, f.nps_name, f.nps_email, sb.unique_id, c.`type`, u.uid
		FROM systems_base AS sb
		LEFT JOIN systems_base_cont AS sbc ON sb.unique_id = sbc.unique_id
		LEFT JOIN misc_contracts AS c ON sbc.contract_type = c.id
		LEFT JOIN facilities AS f ON f.unique_id = sbc.facility_unique_id
		LEFT JOIN users AS u ON u.email = f.nps_email
		WHERE sb.unique_id NOT IN(SELECT n.system_unique_id FROM nps_codes AS n) 
		AND sbc.contract_type IN (".$nps_settings->contracts.") AND f.nps_name != '' AND f.nps_email != '' AND sbc.property = 'C' 
		AND sb.unique_id = '".$_GET['unique_id']."'
		GROUP BY f.nps_name
		ORDER BY sb.system_id ASC $limit;";
	}else{
		$sql="SELECT sb.system_id, sbc.nickname, f.nps_name, f.nps_email, sb.unique_id, c.`type`, u.uid
		FROM systems_base AS sb
		LEFT JOIN systems_base_cont AS sbc ON sb.unique_id = sbc.unique_id
		LEFT JOIN misc_contracts AS c ON sbc.contract_type = c.id
		LEFT JOIN facilities AS f ON f.unique_id = sbc.facility_unique_id
		LEFT JOIN users AS u ON u.email = f.nps_email
		WHERE sbc.contract_type IN (".$nps_settings->contracts.") AND sbc.property = 'C' AND f.nps_name != '' AND f.nps_email != ''
		GROUP BY f.nps_name
		ORDER BY sb.system_id ASC $limit;";	
	}
	echo "Getting Systems",EOL;
	if(!$resultSystemNPS = $mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']');}
	$systemsNPS = array();
	while($rowSystemNPS = $resultSystemNPS->fetch_assoc()) {
		array_push($systemsNPS,$rowSystemNPS);
	}
	//echo "<pre>",EOL;
	//print_r($systemsNPS);
	//echo "</pre>",EOL;
	echo "Number of systems found = ".$resultSystemNPS->num_rows,EOL;
	
	echo "Deleting un-used codes",EOL;
	$sql="DELETE FROM nps_codes WHERE used = 'N';";
	if(!$result = $mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']');}
	echo "Deleted rows: " . mysqli_affected_rows($mysqli),EOL;
	
	echo "Getting used codes so no duplicates happen",EOL;
	$sql="select code FROM nps_codes WHERE used = 'Y';";
	if(!$result = $mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']');}
	while($rowCodes = $result->fetch_assoc()){
		array_push($codes,$rowCodes['code']);
		echo $rowCodes['code'],EOL;	
	}
	echo "Adding to nps_data table: quarter uid and date",EOL;
	$quarter_unique_id = md5(uniqid());
	echo "Quarter ID: ".$quarter_unique_id,EOL;
	echo "Current Date: ".$cur_date,EOL;
	$fiscal_year = date("Y",time());
	$sql="INSERT INTO nps_data (`fiscal_quarter`, `fiscal_year`, `survey_send_date`,`unique_id`) VALUES ('$fiscal_quarter','$fiscal_year','$cur_date','$quarter_unique_id');";
	if(!$result = $mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']');}
	
	
	echo "Sending NPS Survey Emails...",EOL;
	
	require_once($docroot.'/resources/PHPMailer/PHPMailerAutoload.php');
	
	$mail = new PHPMailer;
	$mail->IsSMTP();
	$mail->Host = (string)$settings->email_host;
	$mail->SMTPAuth = true;
	$mail->Username = (string)$settings->email_survey;
	$mail->Password = (string)$settings->email_password;
	$mail->SMTPSecure = 'tls';
	$mail->From = (string)$settings->email_survey;
	$mail->FromName = 'Survey '.$settings->short_name.' Portal';
	$mail->AddReplyTo((string)$settings->email_support, 'Support '.$settings->short_name.' Portal');
	$mail->Subject = (string)$settings->company_name.' Service Experience Survey';
	$mail->IsHTML(true);
	foreach($systemsNPS as $system){
		$code = randomCode();
		if(in_array($code,$codes)){
			$try_again = true;
			while($try_again == true){
				echo "Duplicate Code!!!",EOL;
				$code = randomCode();
				if(!in_array($code,$codes)){
					$try_again = false;	
				}
			}
		}
		$unique_id = md5(uniqid());
		array_push($codes,$code);
		$mysqli->ping();
		
		$sql="INSERT INTO nps_codes (`code`,`email`,`date`,`expire`,`user_id`,`system_unique_id`,`unique_id`, `quarter_unique_id`) 
		VALUES ('$code', '".$system['nps_email']."','$cur_date', '$expire','".$system['uid']."','".$system['unique_id']."','$unique_id', '$quarter_unique_id' );";
		if(!$result = $mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']');}
		
		if($debug){
			$mail->AddAddress('justin.davis@oxinst.com','Justin Davis');
		}else{
			$mail->AddAddress(trim($system['nps_email']),$system['nps_name']);	
		}
		$mail->AddCC((string)$settings->email_support);
		
		$smarty->assign('name', $system['nps_name']);
		$smarty->assign('code', $code);
		$smarty->assign('feedback_url', $settings->feedback_url.'?e='.$code);
		$smarty->assign('company_name',$settings->company_name);
		$smarty->assign('copyright_date',$settings->copyright_date);
		$smarty->assign('email_pics',$settings->email_pics);
		
		$mail->Body = $smarty->fetch('nps_survey.tpl');
		if($show_email){
			echo $mail->Body,EOL;	
		}
		if($send){
			if(!$mail->Send()) {
			   echo 'Email could not be sent.',EOL;
			   echo 'Mailer Error: ' . $mail->ErrorInfo,EOL;
			}else{
				echo "Email sent to: ".$system['nps_name']." < ".$system['nps_email']." >",EOL;	
			}
		}else{
			echo "Send False!",EOL;
			echo "Email sent to: ".$system['nps_name']." < ".$system['nps_email']." >  ".$emails_sent . " -- Really not sent!!!",EOL;
		}
	
		$mail->ClearAddresses();
		$mail->ClearAttachments();
		$emails_sent_total++;
	}
	
	echo "Updating nps_data table with total emails sent",EOL;
	echo $emails_sent_total,EOL;
	$sql="UPDATE nps_data SET surveys_sent = '$emails_sent_total' WHERE unique_id = '$quarter_unique_id';";
	if(!$result = $mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']');}
	
	return true;	
}

function send_reminder(&$mysqli){
	global $settings, $nps_settings, $smarty, $sql_limit, $sql_limit_num, $docroot;
	global $send, $debug, $show_email;
	global $mysqli;
	
	if(!$smarty->templateExists('nps_survey_remind.tpl')){
		die("Template does not exist: nps_survey_remind.tpl");	
	}
		
	echo EOL,"Send Survey Reminder Function",EOL;
	echo "Getting un-used codes...",EOL;
	
	if($sql_limit){$limit = 'LIMIT '.$sql_limit_num;}
	
	$sql="SELECT nc.id, nc.code, sbc.nickname, f.nps_name, f.nps_email
	FROM nps_codes AS nc
	LEFT JOIN systems_base AS sb ON sb.unique_id = nc.system_unique_id
	LEFT JOIN systems_base_cont AS sbc ON sb.unique_id = sbc.unique_id
	LEFT JOIN facilities AS f ON f.unique_id = sbc.facility_unique_id
	WHERE nc.used = 'n' AND sbc.property = 'C' $limit;";
	if(!$resultUnused = $mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']');}
	
	echo "Number of un-used codes: ",$resultUnused->num_rows,EOL;

	echo "Sending NPS Survey Emails...",EOL;

	require_once($docroot.'/resources/PHPMailer/PHPMailerAutoload.php');

	$mail = new PHPMailer;
	$mail->IsSMTP();
	$mail->Host = (string)$settings->email_host;
	$mail->SMTPAuth = true;
	$mail->Username = (string)$settings->email_survey;
	$mail->Password = (string)$settings->email_password;
	$mail->SMTPSecure = 'tls';
	$mail->From = (string)$settings->email_survey;
	$mail->FromName = 'Survey '.$settings->short_name.' Portal';
	$mail->AddReplyTo((string)$settings->email_support, 'Support '.$settings->short_name.' Portal');
	$mail->Subject = 'Reminder to complete '.$settings->company_name.' survey.';
	$mail->IsHTML(true);
	while($rowUnused = $resultUnused->fetch_assoc()){
		if($debug){
			$mail->AddAddress('justin.davis@oxinst.com','Justin Davis');
		}else{
			$mail->AddAddress(trim($rowUnused['nps_email']),$rowUnused['nps_name']);	
		}
		$mail->AddCC((string)$settings->email_support);

		$smarty->assign('name', $rowUnused['nps_name']);
		$smarty->assign('code', $rowUnused['code']);
		$smarty->assign('feedback_url', $settings->feedback_url.'?e='.$rowUnused['code']);
		$smarty->assign('company_name',$settings->company_name);
		$smarty->assign('copyright_date',$settings->copyright_date);
		$smarty->assign('email_pics',$settings->email_pics);
		
		$mail->Body = $smarty->fetch('nps_survey_remind.tpl');
		if($show_email){
			echo $mail->Body,EOL;
		}
		
		if($send){
			if(!$mail->Send()) {
			   echo 'Email could not be sent.',EOL;
			   echo 'Mailer Error: ' . $mail->ErrorInfo,EOL;
			}else{
				echo "Email sent to: ".$rowUnused['nps_name']." < ".$rowUnused['nps_email']." >",EOL;	
			}
		}else{
			echo "Send False!",EOL;
			echo "Email sent to: ".$rowUnused['nps_name']." < ".$rowUnused['nps_email']." >   -- Really not sent!!!",EOL;
		}
	
		$mail->ClearAddresses();
		$mail->ClearAttachments();
		
		$mysqli->ping();
		$sql="UPDATE nps_codes SET reminder_sent = DATE_FORMAT(NOW(),'%Y-%m-%dT%H:%i:%sZ') WHERE id = ".$rowUnused['id'].";";
		if(!$result = $mysqli->query($sql)){echo 'There was an error running the query [' . $mysqli->error . ']  '.$sql,EOL;}
	}	
	return true;	
}
?>