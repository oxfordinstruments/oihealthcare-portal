<?php
die("USED");
error_reporting(E_ALL);

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

if(PHP_SAPI != 'cli'){
	die("Command Line Only Please");	
}

$host="localhost"; // Host name
$username="oiheal5_justin"; // Mysql username
$password="b00b135"; // Mysql password
$db_name="oiheal5_report"; // Database name
$db_name_tiki="oiheal5_tiki";
$title_regex = '/^[tT]itle:\s*/';
$solution_regex = '/^[sS]olution:\s*/';

echo "Begin   ".date('m/d/Y H:i',time()),EOL;

//Get clarify data
$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno) {die('There was an error running the query [' . $mysqli->connect_error . ']');}
echo "MySql Connected to db: ".$db_name."   ".date('m/d/Y H:i:s',time()),EOL;

//$sql="SELECT * FROM clarify WHERE `type` = 'MRI';";
$sql="SELECT * FROM clarify;";
echo $sql,EOL;
	if(!$resultClarify = $mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']  '.$sql);}

$clarify = array();
while($rowClarify = $resultClarify->fetch_assoc()){
	echo "ID:   ".$rowClarify['id'],EOL;
	$clarify[$rowClarify['id']] = array('type' => $rowClarify['type'], 'title' => preg_replace($title_regex, '', $rowClarify['title']), 'solution' => preg_replace($solution_regex, '', $rowClarify['solution']));	
}
$mysqli->close();


//Insert clarify data into kb
$mysqli = new mysqli("$host", "$username", "$password", "$db_name_tiki");
if ($mysqli->connect_errno) {die('There was an error running the query [' . $mysqli->connect_error . ']');}
echo "MySql Connected to db: ".$db_name_tiki."   ".date('m/d/Y H:i:s',time()),EOL;

$dups = array();
$skipped = array();
$x = 200;
foreach($clarify as $key=>$value){
	$skip = false;
	echo $x,EOL,$key,EOL,$value['title'],EOL,EOL;
	$sql = "INSERT INTO tiki_pages (page_id, pageName, hits, `data`, `version`, `user`, creator) VALUES ($x, \"".$value['title']."\", 0, \"".$value['solution']."\", 1, 'admin', 'admin');";
	if($resultPage1 = $mysqli->query($sql)){
		$sql = "INSERT INTO tiki_objects (objectId, `type`, itemId, name, href) VALUES ($x, 'wiki page', \"".$value['title']."\", \"".$value['title']."\", \"tiki-index.php?page=".urlencode($value['title'])."\");";
		if(!$result = $mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']  '.$sql);}
	}else{
		array_push($dups,$key);
		$sql = "INSERT INTO tiki_pages (page_id, pageName, hits, `data`, `version`, `user`, creator) VALUES ($x, \"".$value['title']." - 1\", 0, \"".$value['solution']."\", 1, 'admin', 'admin');";	
		if($result = $mysqli->query($sql)){
			$sql = "INSERT INTO tiki_objects (objectId, `type`, itemId, name, href) VALUES ($x, 'wiki page', \"".$value['title']." - 1\", \"".$value['title']." - 1\", \"tiki-index.php?page=".urlencode($value['title'].' - 1')."\");";
			if(!$result = $mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']  '.$sql);}	
		}else{
			$skip = true;
			array_push($skipped,$key);	
		}
	}
	
	if(!$skip){
		$sql = "INSERT INTO tiki_category_objects (catObjectId, categId) VALUES ($x, 47);";
		if(!$result = $mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']  '.$sql);}
		
		$sql = "INSERT INTO tiki_categorized_objects (catObjectId) VALUES ($x);";
		if(!$result = $mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']  '.$sql);}	
	}
	
	$x += 1;
}

$mysqli->close();

echo EOL,'Dups: ',EOL,print_r($dups),EOL,EOL;
echo EOL,'Skipped: ',EOL,print_r($skipped),EOL,EOL;

echo "DONE   ".date('m/d/Y H:i',time()),EOL;
exit();
?>