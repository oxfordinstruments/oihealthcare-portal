<?php 
die("USED");

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
if(PHP_SAPI != 'cli'){
	die("Command Line Only Please");	
}

echo "Begin "."  ----".date('m/d/Y H:i:s',time()),EOL,EOL;
$host="localhost"; // Host name
$username="oiserv5_justin"; // Mysql username
$password="b00b135"; // Mysql password
$db_name="oiserv5_dms"; // Database name

$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno) {die('There was an error running the query [' . $mysqli->connect_error . ']');}
echo "MySql Connected "."&ensp;&ensp;----".date('m/d/Y H:i:s',time()),EOL;

$sql="SELECT a.id, a.name, a.`comment`, a.folder, c.fileType, acl.target, s.statusID, l.`comment`
FROM tblDocuments AS a
INNER JOIN tblDocuments AS b ON a.name = b.name
LEFT JOIN tblDocumentContent AS c ON a.id = c.document
LEFT JOIN tblACLs AS acl ON acl.target = a.id
LEFT JOIN tblDocumentStatus AS s ON s.documentID = a.id
LEFT JOIN tblDocumentStatusLog AS l ON l.statusID = s.statusID
WHERE a.id <> b.id AND acl.target IS NULL AND c.orgFileName IS NOT NULL AND TRIM(c.fileType) != '.pdf'
ORDER BY a.name ASC;";
echo $sql,EOL;
if(!$resultDocs = $mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']');}

$sql="UPDATE tblDocuments SET `comment` = NULL WHERE `comment` LIKE CONCAT(name,'%');";
echo $sql,EOL;
if(!$result = $mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']');}

while($rowDoc = $resultDocs->fetch_assoc()){
	echo "ID: ".$rowDoc['id']." Document: ".$rowDoc['name'],EOL;
	//find docs matching pdf
	$sql="SELECT d.id,d.name,d.`comment`,c.`version`
	FROM tblDocuments AS d
	LEFT JOIN tblDocumentContent AS c ON c.document = d.id
	WHERE LOWER(TRIM(c.fileType)) = '.pdf' AND d.name = \"".$rowDoc['name']."\" AND d.folder = ".$rowDoc['folder'].";";
	echo $sql,EOL;
	if(!$resultFindMatch = $mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']');}
	$num_matches_found = $resultFindMatch->num_rows;
	
	if($num_matches_found == 0){
		echo "Unable to find a matching pdf for:".$rowDoc['name'],EOL;
		continue;	
	}
	
	if($num_matches_found > 1){
		echo "More than one matching pdf found for:".$rowDoc['name'],EOL;
		//die("DIED multi-match");	
		continue;
	}
	$rowFindMatch = $resultFindMatch->fetch_assoc();
	
	//Add permissions to doc
	$sql="UPDATE tblDocuments SET inheritAccess=0, defaultAccess=1, `comment`='This file is hidden from all but admins' WHERE id=".$rowDoc['id'].";";
	echo $sql,EOL;
	if(!$result = $mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']');}
	
	$status_log="OISP Automated Document Modify; This file is hidden from all but admins because it has a matching pdf";
	$sql="INSERT INTO tblDocumentStatusLog (statusID, `status`, `comment`, `date`, userID) VALUES (".$rowDoc['statusID'].", 2, \"".$status_log."\", NOW(), 1);";
	echo $sql,EOL;
	if(!$result = $mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']');}
	
	$sql="INSERT INTO tblACLs (target,targetType,userID,groupID,`mode`) VALUES (".$rowDoc['id'].",2,-1,3,2);";
	echo $sql,EOL;
	if(!$result = $mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']');}
	
	$sql="INSERT INTO tblDocumentLinks (document,target,userID,public) VALUES (".$rowFindMatch['id'].",".$rowDoc['id'].",1,0), (".$rowDoc['id'].",".$rowFindMatch['id'].",1,0);";
	echo $sql,EOL;
	if(!$result = $mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']');}
	
	echo EOL,EOL,EOL;		
}

pdfNeeded(); //optional

$mysqli->close();
echo "FINISHED",EOL;
exit(0);

function pdfNeeded(){
	global $mysqli;
	
	$sql="INSERT INTO pdfNeeded (id, name, fileid, orgFileName, folderList, fileType, f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, expires)
	SELECT d.id, d.name, dc.id AS fileid, dc.orgFileName, d.folderList, dc.fileType, f1.name AS f1, f2.name AS f2,
	f3.name AS f3, f4.name AS f4, f5.name AS f5, f6.name AS f6, f7.name AS f7, f8.name AS f8, f9.name AS f9, f10.name AS f10, d.expires
	FROM tblDocuments AS d
	LEFT JOIN tblDocumentContent AS dc ON d.id = dc.document
	LEFT JOIN tblFolders AS f1 ON f1.id = d.folder
	LEFT JOIN tblFolders AS f2 ON f2.id = f1.parent
	LEFT JOIN tblFolders AS f3 ON f3.id = f2.parent
	LEFT JOIN tblFolders AS f4 ON f4.id = f3.parent
	LEFT JOIN tblFolders AS f5 ON f5.id = f4.parent
	LEFT JOIN tblFolders AS f6 ON f6.id = f5.parent
	LEFT JOIN tblFolders AS f7 ON f7.id = f6.parent
	LEFT JOIN tblFolders AS f8 ON f8.id = f7.parent
	LEFT JOIN tblFolders AS f9 ON f9.id = f8.parent
	LEFT JOIN tblFolders AS f10 ON f10.id = f9.parent
	GROUP BY d.name
	HAVING COUNT(*) = 1 AND TRIM(dc.fileType) != '.pdf' AND d.expires IS NULL;";	
	echo $sql,EOL;
	if(!$result = $mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']');}
	
	$sql="UPDATE tblDocuments SET expires = 0 WHERE expires IS NULL;";
	echo $sql,EOL;
	if(!$result = $mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']');}
}

function recursive_array_search($needle,$haystack) {
    foreach($haystack as $key=>$value) {
        $current_key=$key;
        if($needle===$value OR (is_array($value) && recursive_array_search($needle,$value) !== false)) {
            return $current_key;
        }
    }
    return false;
}


function human_filesize($bytes, $decimals = 2) {
  $sz = 'BKMGTP';
  $factor = floor((strlen($bytes) - 1) / 3);
  return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
}
?>