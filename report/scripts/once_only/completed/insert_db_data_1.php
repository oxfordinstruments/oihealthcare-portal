<?php
/*###############################################################################################
Order of execution:
	1. Load Old database into oiheal5_report_tmp
	2. Browser run update_date_format.php
	3. Browser run modify_database.php
	4. CLI run insert_db_data_1.php
	6. Browser run insert_db-data_2.php
*/###############################################################################################

$debug = false;

if(!$debug){
	define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : PHP_EOL);
	$base_path = preg_replace("/(public_html).*/",'public_html',dirname(__FILE__),2);
}else{
	define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
	$base_path = $_SERVER['DOCUMENT_ROOT'];
}

//$settings = new SimpleXMLElement($base_path.'/settings.xml', null, true);

$host="localhost";
$username="oiheal5_justin";
$password="b00b135";
$db_name="oiheal5_report";

$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno){
	die('There was an error running the query [' . $mysqli->connect_error . ']');
}

$ndb = 'oiheal5_report';
$odb = 'oiheal5_report_tmp';

$sql=<<<E1
-- TRUNCATE `admin_banlist`;
-- TRUNCATE `admin_errors`;
-- TRUNCATE `customers`;
TRUNCATE `expenses_main`;
TRUNCATE `expenses_milage`;
TRUNCATE `expenses_other`;
-- TRUNCATE `facilitys`;
-- TRUNCATE `misc_carrier`;
-- TRUNCATE `misc_contracts`;
-- TRUNCATE `misc_months`;
-- TRUNCATE `misc_states`;
TRUNCATE `nps_codes`;
-- TRUNCATE `nps_data`;
TRUNCATE `nps_responses`;
TRUNCATE `registration_requests`;
TRUNCATE `remote_assignments`;
TRUNCATE `remote_computers`;
TRUNCATE `remote_online`;
-- TRUNCATE `systems`;
TRUNCATE `systems_assigned_customer`;
TRUNCATE `systems_assigned_history`;
TRUNCATE `systems_assigned_pri`;
TRUNCATE `systems_assigned_sec`;
TRUNCATE `systems_evening_updates`;
TRUNCATE `systems_face_sheet`;
TRUNCATE `systems_files`;
TRUNCATE `systems_hours`;
TRUNCATE `systems_mri_readings`;
TRUNCATE `systems_parts`;
TRUNCATE `systems_reports`;
TRUNCATE `systems_reports_files`;
TRUNCATE `systems_requests`;
TRUNCATE `systems_requests_ids`;
-- TRUNCATE `systems_status`;
-- TRUNCATE `systems_types`;
TRUNCATE `users`;
TRUNCATE `users_auto_logins`;
TRUNCATE `users_calendar`;
-- TRUNCATE `users_grp_id`;
TRUNCATE `users_invites`;
TRUNCATE `users_logins`;
-- TRUNCATE `users_perm_id`;
TRUNCATE `users_pwd_reset_reqs`;
-- TRUNCATE `users_role_id`;
-- TRUNCATE `users_secret_questions`;
-- INSERT INTO oiheal5_report.misc_carrier SELECT * FROM oiheal5_report_tmp.carrier;
-- INSERT INTO oiheal5_report.misc_contracts SELECT oiheal5_report_tmp.contracts.id,oiheal5_report_tmp.contracts.`type`,oiheal5_report_tmp.contracts.invoiced FROM oiheal5_report_tmp.contracts;
-- INSERT INTO oiheal5_report.misc_months SELECT * FROM oiheal5_report_tmp.months;
-- INSERT INTO oiheal5_report.misc_states SELECT * FROM oiheal5_report_tmp.states;
-- INSERT INTO oiheal5_report.nps_data SELECT * FROM oiheal5_report_tmp.nps_data;
-- INSERT INTO oiheal5_report.users_grp_id SELECT * FROM oiheal5_report_tmp.users_grp_id;
-- INSERT INTO oiheal5_report.users_perm_id SELECT * FROM oiheal5_report_tmp.users_perm_id;
-- INSERT INTO oiheal5_report.users_role_id SELECT * FROM oiheal5_report_tmp.users_role_id;
E1;


foreach(explode(';',$sql) as $query){
	if(strlen($query) > 2){
		echo $query,EOL;
		if(!$debug){
			if(!$result = $mysqli->query($query)){
				die($mysqli->error);	
			}
			$mysqli->commit();
			echo "Inserted Rows: ",$mysqli->affected_rows,EOL,EOL;	
		}
	}	
}

//########################################################################################################################

$new_nps_codes = explode(', ','code, email, date, expire, used, used_date, user_id, system_id, system_unique_id, unique_id, quarter_unique_id, reminder_sent');
$old_nps_codes = explode(', ','code, email, date, expire, used, used_date, user_id,   site_id,   site_unique_id, unique_id, quarter_unique_id, reminder_sent');
array_trim($old_nps_codes);
$sql = "INSERT INTO $ndb.nps_codes (`".implode('`,`',$new_nps_codes)."`) SELECT `".implode('`,`',$old_nps_codes)."` FROM $odb.nps_codes;";
echo $sql,EOL;
if(!$debug){
	if(!$result = $mysqli->query($sql)){
		die($mysqli->error);	
	}
	echo "Inserted Rows: ",$mysqli->affected_rows;
}
echo EOL,EOL;

$new_nps_responses = explode(', ','code, date, score, comment, can_contact, contact_info, unique_id, quarter_unique_id, ip, domain, browser');
$old_nps_responses = explode(', ','code, date, score, comment, can_contact, contact_info, unique_id, quarter_unique_id, ip, domain, browser');
$sql = "INSERT INTO $ndb.nps_responses (`".implode('`,`',$new_nps_responses)."`) SELECT `".implode('`,`',$old_nps_responses)."` FROM $odb.nps_responses;";
echo $sql,EOL;
if(!$debug){
	if(!$result = $mysqli->query($sql)){
		die($mysqli->error);	
	}
	echo "Inserted Rows: ",$mysqli->affected_rows;
}
echo EOL,EOL;

$new_systems_requests_ids = 'request_id, unique_id';
$old_request_ids =         'request_id, unique_id';
$sql = "INSERT INTO $ndb.systems_requests_ids ($new_request_ids) SELECT $old_request_ids FROM $odb.request_ids;";
echo $sql,EOL;
if(!$debug){
	if(!$result = $mysqli->query($sql)){
		die($mysqli->error);	
	}
	echo "Inserted Rows: ",$mysqli->affected_rows;
}
echo EOL,EOL;

$new_systems_assigned_customer = explode(', ','system_id, uid');
$old_assigned_sites_customer = explode(', ',    'site_id, uid');
$sql = "INSERT INTO $ndb.systems_assigned_customer (`".implode('`,`',$new_systems_assigned_customer)."`) SELECT `".implode('`,`',$old_assigned_sites_customer)."` FROM $odb.assigned_sites_customer;";
echo $sql,EOL;
if(!$debug){
	if(!$result = $mysqli->query($sql)){
		die($mysqli->error);	
	}
	echo "Inserted Rows: ",$mysqli->affected_rows;
}
echo EOL,EOL;

$new_systems_assigned_history = explode(', ','filename, date, email_sent');
$old_assigned_sites_history = explode(', ',  'filename, date, email_sent');
$sql = "INSERT INTO $ndb.systems_assigned_history (`".implode('`,`',$new_systems_assigned_history)."`) SELECT `".implode('`,`',$old_assigned_sites_history)."` FROM $odb.assigned_sites_history;";
echo $sql,EOL;
if(!$debug){
	if(!$result = $mysqli->query($sql)){
		die($mysqli->error);	
	}
	echo "Inserted Rows: ",$mysqli->affected_rows;
}
echo EOL,EOL;

$new_systems_assigned_pri = explode(', ','system_id, uid');
$old_assigned_sites_pri = explode(', ',    'site_id, uid');
$sql = "INSERT INTO $ndb.systems_assigned_pri (`".implode('`,`',$new_systems_assigned_pri)."`) SELECT `".implode('`,`',$old_assigned_sites_pri)."` FROM $odb.assigned_sites_pri;";
echo $sql,EOL;
if(!$debug){
	if(!$result = $mysqli->query($sql)){
		die($mysqli->error);	
	}
	echo "Inserted Rows: ",$mysqli->affected_rows;
}
echo EOL,EOL;

$new_systems_assigned_sec = explode(', ','system_id, uid');
$old_assigned_sites_sec = explode(', ',    'site_id, uid');
$sql = "INSERT INTO $ndb.systems_assigned_sec (`".implode('`,`',$new_systems_assigned_sec)."`) SELECT `".implode('`,`',$old_assigned_sites_sec)."` FROM $odb.assigned_sites_sec;";
echo $sql,EOL;
if(!$debug){
	if(!$result = $mysqli->query($sql)){
		die($mysqli->error);	
	}
	echo "Inserted Rows: ",$mysqli->affected_rows;
}
echo EOL,EOL;

$new_users_calendar = explode(', ','id, start_date, end_date, text, detail, location, uid, request_unique_id, readonly, subject, new_calendar');
$old_calendar = explode(', ',   'index, start_date, end_date, text, detail, location, uid, request_unique_id, readonly, subject, new_calendar');
$sql = "INSERT INTO $ndb.users_calendar (`".implode('`,`',$new_users_calendar)."`) SELECT `".implode('`,`',$old_calendar)."` FROM $odb.calendar;";
echo $sql,EOL;
if(!$debug){
	if(!$result = $mysqli->query($sql)){
		die($mysqli->error);	
	}
	echo "Inserted Rows: ",$mysqli->affected_rows;
}
echo EOL,EOL;

$new_systems_parts = explode(', ','report_id, number, serial, description, qty, price, unique_id, system_id, system_unique_id, date');
$old_parts = explode(', ',        'report_id, number, serial, description, qty, price, unique_id,   site_id,   site_unique_id, date');
array_trim($old_parts);
$sql = "INSERT INTO $ndb.systems_parts (`".implode('`,`',$new_systems_parts)."`) SELECT `".implode('`,`',$old_parts)."` FROM $odb.parts;";
echo $sql,EOL;
if(!$debug){
	if(!$result = $mysqli->query($sql)){
		die($mysqli->error);	
	}
	echo "Inserted Rows: ",$mysqli->affected_rows;
}
echo EOL,EOL;

$new_systems_hours = explode(', ','report_id, date, reg_labor, ot_labor, reg_travel, ot_travel, unique_id, system_id, system_unique_id');
$old_hours = explode(', ',        'report_id, date, reg_labor, ot_labor, reg_travel, ot_travel, unique_id,   site_id,   site_unique_id');
array_trim($old_hours);
$sql = "INSERT INTO $ndb.systems_hours (`".implode('`,`',$new_systems_hours)."`) SELECT `".implode('`,`',$old_hours)."` FROM $odb.hours;";
echo $sql,EOL;
if(!$debug){
	if(!$result = $mysqli->query($sql)){
		die($mysqli->error);	
	}
	echo "Inserted Rows: ",$mysqli->affected_rows;
}
echo EOL,EOL;

$new_systems_requests = explode(', ','request_num, engineer, sender, system_id, system_unique_id, system_nickname, request_date, onsite_date, initial_call_date, response_date, problem_reported, customer_actions, system_status, po_num, contract_override, pm, status, report_started, new_request, repeat_call, repeat_call_id, under_30min, response_minutes, invoice_required, edited_by, edited_date, deleted, deleted_by, deleted_date, archived, archived_prev_site, unique_id, fm_ref, fm_site_name');
$old_requests = explode(', ',        'request_num, engineer, sender, site_id,     site_unique_id,   site_nickname, request_date, onsite_date, initial_call_date, response_date, problem_reported, customer_actions, system_status, po_num, contract_override, pm, status, report_started, new_request, repeat_call, repeat_call_id, under_30min, response_minutes, invoice_required, edited_by, edited_date, deleted, deleted_by, deleted_date, archived, archived_prev_site, unique_id, fm_ref, fm_site_name');
array_trim($old_requests);
$sql = "INSERT INTO $ndb.systems_requests (`".implode('`,`',$new_systems_requests)."`) SELECT `".implode('`,`',$old_requests)."` FROM $odb.requests;";
echo $sql,EOL;
if(!$debug){
	if(!$result = $mysqli->query($sql)){
		die($mysqli->error);	
	}
	echo "Inserted Rows: ",$mysqli->affected_rows;
}
echo EOL,EOL;

$new_systems_evening_updates = explode(', ','system_id, engineer, update, email_date, unique_id');
$old_evening_updates = explode(', ',          'site_id, engineer, update, email_date, unique_id');
$sql = "INSERT INTO $ndb.systems_evening_updates (`".implode('`,`',$new_systems_evening_updates)."`) SELECT `".implode('`,`',$old_evening_updates)."` FROM $odb.evening_updates;";
echo $sql,EOL;
if(!$debug){
	if(!$result = $mysqli->query($sql)){
		die($mysqli->error);	
	}
	echo "Inserted Rows: ",$mysqli->affected_rows;
}
echo EOL,EOL;

$new_systems_mri_readings = explode(', ','system_id, he, vp, hours, contact, date, notes, uid, system_unique_id, edited, edited_by');
$old_mri_readings = explode(', ',          'site_id, he, vp, hours, contact, date, notes, uid,   site_unique_id, edited, edited_by');
array_trim($old_mri_readings);
$sql = "INSERT INTO $ndb.systems_mri_readings (`".implode('`,`',$new_systems_mri_readings)."`) SELECT `".implode('`,`',$old_mri_readings)."` FROM $odb.mri_readings;";
echo $sql,EOL;
if(!$debug){
	if(!$result = $mysqli->query($sql)){
		die($mysqli->error);	
	}
	echo "Inserted Rows: ",$mysqli->affected_rows;
}
echo EOL,EOL;

$new_systems_reports = explode(', ','report_id, user_id, system_id, system_unique_id, status, system_nickname, complaint, prob_found, service, notes, prev_report_id, engineer, assigned_engineer, date, finalize_date, po_number, contract_override, slice_mas_count, gantry_rev, helium_level, vessel_pressure, recon_ruo, cold_ruo, compressor_hours, compressor_pressure, downtime, pm, pm_date, system_status, report_edited, report_edited_date, report_edited_by, report_edited_notes, invoice_required, valuation_complete, valuation_uid, valuation_date, valuation_do_not_invoice, invoice_labor_reg_rate, invoice_labor_ot_rate, invoice_travel_reg_rate, invoice_travel_ot_rate, invoice_labor_reg, invoice_labor_ot, invoice_travel_reg, invoice_travel_ot, invoice_shipping, invoice_expense, invoiced, invoice_marked_date, invoiced_date_edit, invoiced_uid, invoiced_uid_edit, invoice_number, invoice_date, new_report, deleted, deleted_by, deleted_date, archived, phone_fix, unique_id, has_files, fm_ref, fm_site_name');
$old_reports = explode(', ',        'report_id, user_id,   site_id,   site_unique_id, status,   site_nickname, complaint, prob_found, service, notes, prev_report_id, engineer, assigned_engineer, date, finalize_date, po_number, contract_override, slice_mas_count, gantry_rev, helium_level, vessel_pressure, recon_ruo, cold_ruo, compressor_hours, compressor_pressure, downtime, pm, pm_date, system_status, report_edited, report_edited_date, report_edited_by, report_edited_notes, invoice_required, valuation_complete, valuation_uid, valuation_date, valuation_do_not_invoice, invoice_labor_reg_rate, invoice_labor_ot_rate, invoice_travel_reg_rate, invoice_travel_ot_rate, invoice_labor_reg, invoice_labor_ot, invoice_travel_reg, invoice_travel_ot, invoice_shipping, invoice_expense, invoiced, invoice_marked_date, invoiced_date_edit, invoiced_uid, invoiced_uid_edit, invoice_number, invoice_date, new_report, deleted, deleted_by, deleted_date, archived, phone_fix, unique_id, has_files, fm_ref, fm_site_name');
array_trim($old_reports);
$sql = "INSERT INTO $ndb.systems_reports (`".implode('`,`',$new_systems_reports)."`) SELECT `".implode('`,`',$old_reports)."` FROM $odb.reports;";
echo $sql,EOL;
if(!$debug){
	if(!$result = $mysqli->query($sql)){
		die($mysqli->error);	
	}
	echo "Inserted Rows: ",$mysqli->affected_rows;
}
echo EOL,EOL;

$new_systems_reports_files = explode(', ','unique_id, file_location, file_name, file_size, uid');
$old_reports_files = explode(', ',        'unique_id, file_location, file_name, file_size, uid');
$sql = "INSERT INTO $ndb.systems_reports_files (`".implode('`,`',$new_systems_reports_files)."`) SELECT `".implode('`,`',$old_reports_files)."` FROM $odb.reports_files;";
echo $sql,EOL;
if(!$debug){
	if(!$result = $mysqli->query($sql)){
		die($mysqli->error);	
	}
	echo "Inserted Rows: ",$mysqli->affected_rows;
}
echo EOL,EOL;

$new_systems_files = explode(', ','unique_id, file_location, file_name, file_size, uid');
$old_sites_files = explode(', ',  'unique_id, file_location, file_name, file_size, uid');
$sql = "INSERT INTO $ndb.systems_files (`".implode('`,`',$new_systems_files)."`) SELECT `".implode('`,`',$old_sites_files)."` FROM $odb.sites_files;";
echo $sql,EOL;
if(!$debug){
	if(!$result = $mysqli->query($sql)){
		die($mysqli->error);	
	}
	echo "Inserted Rows: ",$mysqli->affected_rows;
}
echo EOL,EOL;

$new_systems_face_sheet = explode(', ','system_id, system_unique_id, completed, quickbooks_date, quickbooks_user, quickbooks_initials, quickbooks_needed, quickbooks_complete, quickbooks_notes, logbook_date, logbook_user, logbook_initials, logbook_needed, logbook_complete, logbook_notes, afterhours_date, afterhours_user, afterhours_initials, afterhours_needed, afterhours_complete, afterhours_notes, idsticker_date, idsticker_user, idsticker_initials, idsticker_needed, idsticker_complete, idsticker_notes, basket_date, basket_user, basket_initials, basket_needed, basket_complete, basket_notes');
$old_sites_face_sheet = explode(', ',    'site_id,   site_unique_id, completed, quickbooks_date, quickbooks_user, quickbooks_initials, quickbooks_needed, quickbooks_complete, quickbooks_notes, logbook_date, logbook_user, logbook_initials, logbook_needed, logbook_complete, logbook_notes, afterhours_date, afterhours_user, afterhours_initials, afterhours_needed, afterhours_complete, afterhours_notes, idsticker_date, idsticker_user, idsticker_initials, idsticker_needed, idsticker_complete, idsticker_notes, basket_date, basket_user, basket_initials, basket_needed, basket_complete, basket_notes');
array_trim($old_sites_face_sheet);
$sql = "INSERT INTO $ndb.systems_face_sheet (`".implode('`,`',$new_systems_face_sheet)."`) SELECT `".implode('`,`',$old_sites_face_sheet)."` FROM $odb.sites_face_sheet;";
echo $sql,EOL;
if(!$debug){
	if(!$result = $mysqli->query($sql)){
		die($mysqli->error);	
	}
	echo "Inserted Rows: ",$mysqli->affected_rows;
}
echo EOL,EOL;

$new_users = explode(', ','uid, pwd, name, firstname, lastname, active, address, city, state, zip, lat, long, email, cell, carrier, grp_employee, grp_contractor, grp_customer, default_role, role_engineer, role_dispatch, role_finance, role_management, role_basic, role_admin, role_contractor, role_customer, perm_service_request, perm_remote_engineer, perm_rcv_sms, perm_rcv_evening_updates, perm_rcv_service_reports, perm_rcv_service_requests, perm_rcv_valuation_request, perm_rcv_invoice_request, perm_rcv_assignment_change, perm_face_signoff, perm_face_print, perm_change_phone, perm_edit_systems, perm_edit_reports, perm_no_assigned_systems, perm_facility_manager, perm_extend_login, perm_maintenance_menus, perm_nps_menus, perm_nps_reports, perm_mri_readings_menus, perm_dms_access, perm_kb_access, perm_oivision_access, perm_remote_diag, pref_rcv_evening_updates, pref_rcv_service_reports, pref_rcv_service_requests, pref_rcv_valuation_request, pref_rcv_invoice_request, pref_rcv_assignment_change, pref_rcv_sms, pref_show_help, serviced_systems, last_login, logged_in, ip, domain, pwd_chg_date, secret_1, secret_1_id, secret_2, secret_2_id, linux_pwd');
$old_users = explode(', ',"uid, pwd, name, firstname, lastname, active, address, city, state, zip, lat, long, email, cell, carrier, grp_employee, grp_contractor, grp_customer, default_role, role_engineer, role_dispatch, role_finance, role_management, role_basic, role_admin, role_contractor, role_customer, perm_service_request, perm_remote_engineer, perm_rcv_sms, perm_rcv_evening_updates, perm_rcv_service_reports, perm_rcv_service_requests, perm_rcv_valuation_request, perm_rcv_invoice_request, perm_rcv_assignment_change, perm_face_signoff, perm_face_print, perm_change_phone,   perm_edit_sites, perm_edit_reports,   perm_no_assigned_sites,     perm_site_manager, perm_extend_login, perm_maintenance_menus, perm_nps_menus, perm_nps_reports, perm_mri_readings_menus, perm_dms_access, perm_kb_access, perm_oivision_access, perm_remote_diag, pref_rcv_evening_updates, pref_rcv_service_reports, pref_rcv_service_requests, pref_rcv_valuation_request, pref_rcv_invoice_request, pref_rcv_assignment_change, pref_rcv_sms, pref_show_help,   serviced_sites, last_login, logged_in, ip, domain, pwdchgdate, secret_1, secret_1_id, secret_2, secret_2_id, linux_pwd");
array_trim($old_users);
$sql = "INSERT INTO $ndb.users (`".implode('`,`',$new_users)."`) SELECT `".implode('`,`',$old_users)."` FROM $odb.users;";
echo $sql,EOL;
if(!$debug){
	if(!$result = $mysqli->query($sql)){
		die($mysqli->error);	
	}
	echo "Inserted Rows: ",$mysqli->affected_rows;
}
echo EOL,EOL;


$sql="UPDATE $ndb.systems_reports_files SET file_location =
REPLACE(file_location, 'reports_files', 'systems_reports_files');";
if(!$result = $mysqli->query($sql)){
	echo EOL,EOL;
	die($mysqli->error);	
}

echo "DONE",EOL;

function array_trim(&$array){
	$array = array_map('trim', $array);	
}

die();
?>