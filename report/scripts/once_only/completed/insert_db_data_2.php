<?php
/*###############################################################################################
Order of execution:
	1. Load Old database into oiheal5_report_tmp
	2. Browser run update_date_format.php
	3. Browser run modify_database.php
	4. CLI run insert_db_data_1.php
	6. Browser run insert_db-data_2.php
*/###############################################################################################

$debug = false;
if(isset($_GET['debug'])){
	$debug = true;	
}

$limit = false;  //false or sql limit int
$insert = true;
$tz_lookup = true;


define('EOL', '<br>');
$base_path = $_SERVER['DOCUMENT_ROOT'];

require_once($base_path.'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

require($base_path.'/resources/php_geocode/GoogleMapsGeocoder.php');
$Geocoder = new GoogleMapsGeocoder();

//$settings = new SimpleXMLElement($base_path.'/settings.xml', null, true);

$host="localhost";
$username="oiheal5_justin";
$password="b00b135";
$db_name="oiheal5_report";

$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno){
	die('There was an error running the query [' . $mysqli->connect_error . ']');
}

$ndb = 'oiheal5_report';
$odb = 'oiheal5_report_tmp';

$sql=<<<E1
TRUNCATE `customers`;
TRUNCATE `facilities`;
TRUNCATE `systems`;
-- INSERT INTO `$ndb`.`facilities` (`facility_id`, `name`, `address`, `city`, `state`, `zip`, `phone`, `fax`, `contact_name`, `contact_title`, `contact_phone`, `contact_cell`, `contact_email`, `bill_name`, `bill_title`, `bill_phone`, `bill_email`, `bill_address`, `bill_city`, `bill_state`, `bill_zip`, `credit_hold_notes`, `notes`, `unique_id`) VALUES ('0', 'Unknown', 'Unknown', 'Unknown', 'FL', 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown', '0');
-- INSERT INTO `$ndb`.`customers` (`customer_id`, `name`, `address`, `city`, `state`, `zip`, `phone`, `fax`, `contact_name`, `contact_title`, `contact_phone`, `contact_cell`, `contact_email`, `credit_hold_notes`, `notes`, `unique_id`) VALUES ('0', 'Unknown', 'Unknown', 'Unknown', 'FL', 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown', '0');
E1;


echo "Running initial queries...",EOL;
foreach(explode(';',$sql) as $query){
	if(strlen($query) > 2){
		if(!$result = $mysqli->query($query)){
			echo $sql,EOL;
			die($mysqli->error);	
		}
		$mysqli->commit();
	}	
}



echo "Creating Customers...",EOL;//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

$sql="SELECT s.*
FROM `$odb`.sites AS s
WHERE s.site_id != '9999' AND s.site_name != '' AND s.site_name IS NOT NULL
GROUP BY TRIM(s.site_name)
ORDER BY s.site_name";
if(!$limit){
	$sql .= ";";
}else{
	$sql .= " LIMIT $limit;";
}
d($sql);
if(!$result = $mysqli->query($sql)){
	echo $sql,EOL;
	die($mysqli->error);	
}
$sites_array = array();
while($row = $result->fetch_assoc()){
	array_push($sites_array,$row);
}
d($sites_array);


$customer_id = 36219;
$customers_array = array();
foreach($sites_array as $site){
	$customer_unique_id = md5(uniqid());
	$customer_id += 1;
	
	$cust_name = mysql_escape_string($site['bill_name']);
	if(strlen($site['bill_name']) < 2){
		$cust_name = mysql_escape_string($site['site_name']);
	}
	echo "  Customer: ",$site['bill_name']," - ",$customer_unique_id,EOL;	
	
	if($tz_lookup){
		$customer_timezone = getTimezoneGeo($site['site_lat'],$site['site_lng']);
	}
	$address = $site['bill_address'].",".$site['bill_city'].",".$site['bill_state'].",".$site['bill_zip'];
	if($tz_lookup){
		$Geocoder->setAddress($address);
		$response = $Geocoder->geocode();
	}
	if($response['status'] == "OK"){
		$lat = $response['results'][0]['geometry']['location']['lat'];
		$lng = $response['results'][0]['geometry']['location']['lng'];
	}
	//d($response);
	
	
	
	$customer_vals = array(
		'customer_id' => $customer_id,
		'name' => $cust_name,
		'address' => mysql_escape_string($site['bill_address']),
		'city' => mysql_escape_string($site['bill_city']),
		'state' => mysql_escape_string($site['bill_state']),
		'zip' => $site['bill_zip'],
		'lat' => $lat,
		'lng' => $lng,
		'timezone' => $customer_timezone,
		'phone' => $site['bill_phone'],
		'fax' => $site['bill_fax'],
		'contact_name' => mysql_escape_string($site['bill_contact']),
		'contact_title' => '',
		'contact_phone' => '',
		'contact_cell' => '',
		'contact_email' => mysql_escape_string($site['bill_email']),
		'bill_customer' => 'N',
		'credit_hold' => $site['credit_hold'],
		'credit_hold_date' => $site['credit_hold_date'],
		'credit_hold_notes' => mysql_escape_string($site['credit_hold_notes']),
		'notes' => mysql_escape_string($site['notes']),
		'archived' => $site['archived'],
		'has_files' => 'N',
		'unique_id' => $customer_unique_id,
		'temp_site_name'=>$site['site_name'],
		'temp_site_id'=>$site['site_id']
	);
	//d($customer_vals);
	
	$sql="INSERT INTO `$ndb`.`customers` 
	(`customer_id`, `name`, `address`, `city`, `state`, `zip`, `lat`, `lng`, `timezone`, `phone`, `fax`, `contact_name`, `contact_title`, `contact_phone`, `contact_cell`, 
	`contact_email`, `bill_customer`, `credit_hold`, `credit_hold_date`, `credit_hold_notes`, `notes`, `archived`, `has_files`, `unique_id`, `temp_site_name`, `temp_site_id`) 
	VALUES ('". implode("', '", $customer_vals) . "');";
	d($sql);
	if($insert){
		if(!$result = $mysqli->query($sql)){
			echo $sql,EOL;
			die($mysqli->error);	
		}
	}
	
	$customer_vals['site_name'] = $site['site_name']; //not included in sql
	$customers_array[$customer_id] = $customer_vals;
	
}
d($customers_array);

$customers_array_short = array();
foreach($customers_array as $cust){
	$customers_array_short[$cust['customer_id']] = $cust['site_name'];	
}
d($customers_array_short);


echo "Creating Facilities...",EOL; //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

$sql="SELECT s.*
FROM `$odb`.sites AS s
WHERE s.site_id != '9999'
GROUP BY TRIM(s.site_address)
ORDER BY s.site_name";
if(!$limit){
	$sql .= ";";
}else{
	$sql .= " LIMIT $limit;";
}
d($sql);
if(!$result = $mysqli->query($sql)){
	die($mysqli->error);	
}

$sites_array = array();
while($row = $result->fetch_assoc()){
	array_push($sites_array,$row);
}
d($sites_array);


$facility_id = 23099;
$facilities_array = array();
foreach($sites_array as $site){
	$facility_unique_id = md5(uniqid());
	$facility_id += 1;
	
	$customer_id = array_search($site['site_name'],$customers_array_short);
	if(!$customer_id){
		$customer_id = 0;	
		$customer_unique_id = 0;
	}else{
		$customer_unique_id = $customers_array[$customer_id]['unique_id'];
	}
	
	echo "  Facility: ",$site['site_nickname']," - ",$facility_unique_id,"  Customer: ",$customer_unique_id,EOL;
	if($tz_lookup){
		$facility_timezone = getTimezoneGeo($site['site_lat'],$site['site_lng']);
	}
	$facility_vals = array(
		'facility_id' => $facility_id,
		'customer_unique_id' => $customer_unique_id,
		'name' => mysql_escape_string($site['site_nickname']),
		'address' => mysql_escape_string($site['site_address']),
		'city' => mysql_escape_string($site['site_city']),
		'state' => $site['site_state'],
		'zip' => $site['site_zip'],
		'lat' => $site['site_lat'],
		'lng' => $site['site_lng'],
		'timezone' => $facility_timezone,
		'phone' => $site['site_phone'],
		'fax' => $site['site_fax'],
		'contact_name' => mysql_escape_string($site['site_mgr_name']),
		'contact_title' => mysql_escape_string($site['site_mgr_title']),
		'contact_phone' => $site['site_mgr_phone'],
		'contact_cell' => $site['site_mgr_cell'],
		'contact_email' => mysql_escape_string($site['site_mgr_email']),
		'bill_facility' => 'Y',
		'bill_name' => mysql_escape_string($site['bill_name']),
		'bill_title' => mysql_escape_string($site['bill_title']),
		'bill_phone' => $site['bill_phone'],
		'bill_email' => $site['bill_email'],
		'bill_address' => mysql_escape_string($site['bill_address']),
		'bill_city' => mysql_escape_string($site['bill_city']),
		'bill_state' => $site['bill_state'],
		'bill_zip' => $site['bill_zip'],
		'credit_hold' => $site['credit_hold'],
		'credit_hold_date' => $site['credit_hold_date'],
		'credit_hold_notes' => mysql_escape_string($site['credit_hold_notes']),
		'notes' => mysql_escape_string($site['notes']),
		'email_list' => $site['email_list'],
		'archived' => $site['archived'],
		'has_files' => 'N',
		'nps_email' => $site['nps_email'],
		'nps_name' => $site['nps_name'],
		'unique_id' => $facility_unique_id,
		'temp_site_name'=>$site['temp_site_name'],
		'temp_site_id'=>$site['site_id']
	);
	d($facility_vals);
	
	$sql="INSERT INTO `$ndb`.`facilities` 
	(`facility_id`, `customer_unique_id`, `name`, `address`, `city`, `state`, `zip`, `lat`, `lng`, `timezone`, `phone`, `fax`, `contact_name`, `contact_title`, `contact_phone`, 
	`contact_cell`, `contact_email`, `bill_facility`, `bill_name`, `bill_title`, `bill_phone`, `bill_email`, `bill_address`, `bill_city`, `bill_state`, `bill_zip`, `credit_hold`, 
	`credit_hold_date`, `credit_hold_notes`, `notes`, `email_list`, `archived`, `has_files`, `nps_email`, `nps_name`, `unique_id`, `temp_site_name`, `temp_site_id`) 
	VALUES ('". implode("', '", $facility_vals) . "')";	
	d($sql);
	
	if($insert){
		if(!$result = $mysqli->query($sql)){
			echo $sql,EOL;
			die($mysqli->error);	
		}
	}
	
	$facility_vals['site_address'] = $site['site_address']; //not included in sql
	$facilities_array[$facility_id] = $facility_vals;
}
d($facilities_array);

$facilities_array_short = array();
foreach($facilities_array as $faci){
	$facilities_array_short[$faci['facility_id']] = $faci['site_address'];	
}
d($facilities_array_short);



echo "Creating Systems...",EOL; //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

$sql="SELECT s.*
FROM `$odb`.sites AS s
WHERE s.site_id != '9999'";
if(!$limit){
	$sql .= ";";
}else{
	$sql .= " LIMIT $limit;";
}
d($sql);
if(!$result = $mysqli->query($sql)){
	echo $sql,EOL;
	die($mysqli->error);	
}

$sites_array = array();
while($row = $result->fetch_assoc()){
	array_push($sites_array,$row);
}
d($sites_array);

foreach($sites_array as $site){
	$facility_id = array_search($site['site_address'],$facilities_array_short);
	if(!$facility_id){
		$facility_id = 0;	
		$facility_unique_id = 0;
		$customer_id = 0;	
		$customer_unique_id = 0;
	}else{
		$facility_unique_id = $facilities_array[$facility_id]['unique_id'];
		
		$customer_id = array_search($site['site_name'],$customers_array_short);
		if(!$customer_id){
			$customer_id = 0;	
			$customer_unique_id = 0;
		}else{
			$customer_unique_id = $customers_array[$customer_id]['unique_id'];
		}
	}
	
	echo "System: ",$site['site_nickname']," - ",$site['unique_id'],"  Facility: ",$facility_unique_id,"  Customer: ",$customer_unique_id,EOL;
	
	$system_vals = array(
	'system_id' => $site['site_id'],
	'facility_unique_id' => $facility_unique_id,
	'nickname' => mysql_escape_string($site['site_nickname']),
	'contact_name' => mysql_escape_string($site['site_contact_name']),
	'contact_title' => mysql_escape_string($site['site_contact_title']),
	'contact_phone' => $site['site_contact_phone'],
	'contact_cell' => $site['site_contact_cell'],
	'contact_email' => mysql_escape_string($site['site_contact_email']),
	'system_type' => $site['equipment_type'],
	'sw_ver' => $site['equipment_sw_ver'],
	'location' => $site['equipment_location'],
	'mobile' => $site['mobile_site'],
	'date_installed' => $site['equipment_date_installed'],
	'system_serial' => $site['equipment_serial'],
	'tube_covered' => $site['equipment_tube_covered'],
	'he_monitor' => $site['equipment_he_monitor'],
	'he_monitor_serial' => $site['equipment_he_monitor_serial'],
	'acceptance_date' => $site['equipment_acceptance_date'],
	'log_book' => $site['equipment_log_book'],
	'contract_type' => $site['contract_type'],
	'contract_terms' => mysql_escape_string($site['contract_terms']),
	'contract_start_date' => $site['contract_start_date'],
	'contract_end_date' => $site['contract_end_date'],
	'contract_hours' => $site['contract_hours'],
	'labor_reg_rate' => $site['labor_reg_rate'],
	'labor_ot_rate' => $site['labor_ot_rate'],
	'travel_reg_rate' => $site['travel_reg_rate'],
	'travel_ot_rate' => $site['travel_ot_rate'],
	'credit_hold' => $site['credit_hold'],
	'credit_hold_date' => $site['credit_hold_date'],
	'credit_hold_notes' => mysql_escape_string($site['credit_hold_notes']),
	'warranty_start_date' => $site['warranty_start_date'],
	'warranty_end_date' => $site['warranty_end_date'],
	'pm_freq' => $site['pm_freq'],
	'last_pm' => $site['last_pm'],
	'status' => $site['status'],
	'notes' => mysql_escape_string($site['notes']),
	'has_files' => $site['has_files'],
	'mr_lhe_list' => $site['mr_lhe_list'],
	'mr_lhe_list_update' => $site['mr_lhe_list_update'],
	'mr_lhe_press' => $site['mr_lhe_press'],
	'mr_lhe_contact' => mysql_escape_string($site['mr_lhe_contact']),
	'mr_lhe_phone' => $site['mr_lhe_phone'],
	'archived' => $site['archived'],
	'unique_id' => $site['unique_id'],
	'edited_by' => $site['edited_by'],
	'edited_date' => $site['edited_date'],
	'created_by' => $site['created_by'],
	'created_date' => $site['created_date']);
	
	
	$systems_fields = array('system_id', 'facility_unique_id', 'nickname', 'contact_name', 'contact_title', 'contact_phone', 'contact_cell', 'contact_email', 'system_type',
			'sw_ver', 'location', 'mobile', 'date_installed', 'system_serial', 'tube_covered', 'he_monitor', 'he_monitor_serial', 'acceptance_date', 'log_book', 
			'contract_type', 'contract_terms', 'contract_start_date', 'contract_end_date', 'contract_hours', 'labor_reg_rate', 'labor_ot_rate', 'travel_reg_rate', 'travel_ot_rate', 
			'credit_hold',  'credit_hold_date', 'credit_hold_notes', 'warranty_start_date', 'warranty_end_date', 'pm_freq', 'last_pm', 'status', 'notes', 'has_files', 
			'mr_lhe_list', 'mr_lhe_list_update', 'mr_lhe_press', 'mr_lhe_contact', 'mr_lhe_phone', 'archived', 'unique_id', 'edited_by', 'edited_date', 'created_by', 'created_date');
	

	$sql="INSERT INTO $ndb.systems(`".implode("`,`",$systems_fields)."`)VALUES('".implode("','",$system_vals)."');";
	d($sql);
	if($insert){
		if(!$result = $mysqli->query($sql)){
			echo $sql,EOL;
			die($mysqli->error);	
		}
	}
	
	if(strtolower($site['archived']) == 'n'){
		$sql="UPDATE $ndb.facilities SET property = 'C' WHERE unique_id = '$facility_unique_id';";
		if(!$result = $mysqli->query($sql)){
			echo $sql,EOL;
			die($mysqli->error);	
		}
		$sql="UPDATE $ndb.customers SET property = 'C' WHERE unique_id = '$customer_unique_id';";	
		if(!$result = $mysqli->query($sql)){
			echo $sql,EOL;
			die($mysqli->error);	
		}
	}
	
}

echo "Running final queries...",EOL;
$sql=<<<E2
UPDATE facilities SET timezone = NULL WHERE timezone IS NULL OR timezone ='';
UPDATE customers SET timezone = NULL WHERE timezone IS NULL OR timezone ='';
UPDATE systems_reports AS sr SET sr.facility_unique_id = (SELECT s.facility_unique_id FROM systems AS s WHERE s.unique_id = sr.system_unique_id);
UPDATE systems_requests AS sr SET sr.facility_unique_id = (SELECT s.facility_unique_id FROM systems AS s WHERE s.unique_id = sr.system_unique_id);
UPDATE systems_face_sheet AS s SET s.survey_notes = 'Inital insert by portal admin. These check offs have not been verified.', s.survey_initials = 'NA', s.survey_complete = 'Y', s.survey_user = 'unassigned';
E2;
foreach(explode(';',$sql) as $query){
	if(strlen($query) > 2){
		if(!$result = $mysqli->query($query)){
			echo $sql,EOL;
			die($mysqli->error);	
		}
		$mysqli->commit();
	}	
}


echo "DONE",EOL;

function array_trim(&$array){
	$array = array_map('trim', $array);	
}

function getTimezoneGeo($latForGeo, $lngForGeo) {
    $json = file_get_contents("http://api.geonames.org/timezoneJSON?lat=".$latForGeo."&lng=".$lngForGeo."&username=evotodi1");
    $data = json_decode($json);
    $tzone=$data->timezoneId;
    return $tzone;
}

die();
?>