<!DOCTYPE html>
<html>
<head>
<script type="text/javascript">
function getDocHeight() {
    var D = document;
    return Math.max(
        Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
        Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
        Math.max(D.body.clientHeight, D.documentElement.clientHeight)
    );
}

document.documentElement.scrollTop = getDocHeight();

var scroller = setInterval(function() {  
    document.documentElement.scrollTop = getDocHeight();
}, 100 /*update intervall in ms*/);

</script>
</head>
<body>
<?php
die("DONE");

/*
2015-8-14 Evotodi
This script inserts the exsisting trailers and updates the systems acordingly
*/

ob_start();//Start output buffering

$debug = false;
if(isset($_GET['debug'])){
	$debug = true;	
}

$execute = false; //Execute sql update/insert/delete
if(isset($_GET['execute'])){
	$execute = true;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/define_inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

require($_SERVER['DOCUMENT_ROOT'].'/mysqlInfo.php');
$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno) {die('There was an error running the query [' . $mysqli->connect_error . ']');}

echo "<h2>Trailers to insert</h2>";
//
//System ids array key is new id value is old id
//
$trailers = array(
'TR1000'=>array('sid'=>'CT1292','mfg'=>'Calumet','vin'=>'1TKH048221B083736','inv'=>'2519'),
'TR1001'=>array('sid'=>'CT1293','mfg'=>'Calumet','vin'=>'1TKH04827YB011795','inv'=>'AP-CT-2492'),
'TR1002'=>array('sid'=>'CT1294','mfg'=>'Ellis & Watts','vin'=>'1JJV482W9YL718108','inv'=>'AP-CT-2499'),
'TR1003'=>array('sid'=>'CT1295','mfg'=>'Calumet','vin'=>'1TKH0482X1B094368','inv'=>'AP-NO-1207'),
'TR1004'=>array('sid'=>'CT1296','mfg'=>'Calumet','vin'=>'1TKH04825YB042074','inv'=>'AP-CT-116'),
'TR1005'=>array('sid'=>'CT1297','mfg'=>'Oshkosh','vin'=>'1LH142UH111011605','inv'=>'AP-NO-114'),
'TR1006'=>array('sid'=>'CT1298','mfg'=>'Calumet','vin'=>'1TKH04824YB022012','inv'=>'2584'),
'TR1007'=>array('sid'=>'CT1299','mfg'=>'Calumet','vin'=>'1TKH048211B084361','inv'=>'AP-CT-113'),
'TR1008'=>array('sid'=>'CT1300','mfg'=>'Calumet','vin'=>'1TKH04829YB022006','inv'=>'AP-NO-1235'),
'TR1009'=>array('sid'=>'CT1301','mfg'=>'MDG','vin'=>'1PT011AJ0F9008193','inv'=>'AP-NO-1097'),
'TR1010'=>array('sid'=>'MR2275','mfg'=>'Calumet','vin'=>'1T9FA0Z38FB021612','inv'=>'2518'),
'TR1011'=>array('sid'=>'MR2260','mfg'=>'Calumet','vin'=>'1T9FA0Z24JB021283','inv'=>'3330'),
'TR1012'=>array('sid'=>'MR2271','mfg'=>'Calumet','vin'=>'1T9FA0Z30MB021536','inv'=>'3691'),
'TR1013'=>array('sid'=>'MR2274','mfg'=>'Calumet','vin'=>'1T9FA0Z33GB021860','inv'=>'3857'),
'TR1014'=>array('sid'=>'MR2273','mfg'=>'Calumet','vin'=>'1T9FA0Z37GB021779','inv'=>'AP-NO-1236'),
'TR1016'=>array('sid'=>'MR2222','mfg'=>'Calumet','vin'=>'1T9FA0Z33HB021861','inv'=>'2911'),
'TR1017'=>array('sid'=>'MR2276','mfg'=>'Schien','vin'=>'1S9FA82X81183313','inv'=>'AP-MR-2498'),
'TR1018'=>array('sid'=>'MR2286','mfg'=>'Medicoach','vin'=>'1M9A3A8216W022406','inv'=>'4160'),
'TR1019'=>array('sid'=>'MR2279','mfg'=>'Schien','vin'=>'1S9FA482891183330','inv'=>'AP-MR-2497'),
'TR1020'=>array('sid'=>'MR2263','mfg'=>'Calumet','vin'=>'1T9FA0Z20JB021569','inv'=>'2610'),
'TR1021'=>array('sid'=>'MR2282','mfg'=>'Calumet','vin'=>'1TKH04822YB011560','inv'=>'AP-MR-2407'),
'TR1022'=>array('sid'=>'MR2269','mfg'=>'Calumet','vin'=>'1TKHA4820XB116156','inv'=>'3033'),
'TR1023'=>array('sid'=>'MR2261','mfg'=>'AK Specialty','vin'=>'1S9FA482021182517','inv'=>'3503'),
'TR1024'=>array('sid'=>'MR2280','mfg'=>'Ellis & Watts','vin'=>'1KKVA48227L223655','inv'=>'AP-NO-2292'),
'TR1025'=>array('sid'=>'MR2270','mfg'=>'Calutech','vin'=>'1TKH04820VB018020','inv'=>'AP-NO-2291'),
'TR1026'=>array('sid'=>'MR2272','mfg'=>'Ellis & Watts','vin'=>'1JJV482WXYL515096','inv'=>'3201'),
'TR1027'=>array('sid'=>'MR2266','mfg'=>'Ellis & Watts','vin'=>'2M592146361109005','inv'=>'3522'),
'TR1028'=>array('sid'=>'MR2208','mfg'=>'Calumet','vin'=>'1TKH04822WB053692','inv'=>'3920'),
'TR1029'=>array('sid'=>'MR2268','mfg'=>'Ellis & Watts','vin'=>'1JJV482W5XL512394','inv'=>'AP-MR-106'),
'TR1030'=>array('sid'=>'MR2267','mfg'=>'Ellis & Watts','vin'=>'2M592146971109012','inv'=>'6364'),
'TR1031'=>array('sid'=>'MR2265','mfg'=>'Calumet','vin'=>'1TKH05021YB032062','inv'=>'AP-MR-2449'),
'TR1032'=>array('sid'=>'MR2289','mfg'=>'Calutech','vin'=>'1TKH048263B122167','inv'=>'none'),
'TR1033'=>array('sid'=>'MR2283','mfg'=>'Calumet','vin'=>'1TKH048281B083742','inv'=>'none'),
'TR1034'=>array('sid'=>'MR2296','mfg'=>'Schien','vin'=>'1S9FA482051182781','inv'=>'none'),
'TR1035'=>array('sid'=>'MR2290','mfg'=>'Schien','vin'=>'1S9FA482421182469','inv'=>'none'),
'TR1036'=>array('sid'=>'NM3000','mfg'=>'AK Specialty','vin'=>'1TKH048221B104374','inv'=>'2528'),
'TR1037'=>array('sid'=>'NM3001','mfg'=>'Calumet','vin'=>'1LH142UH011011370','inv'=>'AP-NM-2495'),
'TR1038'=>array('sid'=>'NM3002','mfg'=>'Medicoach','vin'=>'1M9A6A62XXH022146','inv'=>'AP-NM-1681'),
'TR1039'=>array('sid'=>'PCT4000','mfg'=>'AK Specialty','vin'=>'1LH142UH221012151','inv'=>'3885')
);
d($trailers);

$systems_updated = array();

if(!$execute){
	echo "<h1>Not Executing SQL</h1>";	
}else{
	echo "<h2>Executing SQL Statements</h2>";	
}

//
//Insert trailers and update systems
//
scroll();
echo "<h2>Inserting trailers and updating systems</h2>";
foreach($trailers as $tid=>$data){
	$trailer_unique_id = md5(uniqid());
	$system_unique_id = '';
	$assigned = 'N';
	
	$sql="SELECT unique_id
	FROM systems
	WHERE system_id = '".$data['sid']."';";
	if(!$reslutSystem = $mysqli->query($sql)){
		die($mysqli->error);	
	}
	if($reslutSystem->num_rows > 0){
		array_push($systems_updated,$data['sid']);
		//Update the system data
		$rowSystem = $reslutSystem->fetch_assoc();
		$system_unique_id = $rowSystem['unique_id'];
		
		$sql="UPDATE systems SET mobile = 'Y', trailer_unique_id = '$trailer_unique_id' WHERE unique_id = '$system_unique_id';";
		echo "Execute: ".$sql,EOL;
		if($execute){
			if(!$reslutSystem = $mysqli->query($sql)){
				die($mysqli->error);	
			}
		}
		$assigned = 'Y';
	}
		
	$sql="INSERT INTO trailers (trailer_id, nickname, assigned, system_unique_id, mfg, vin, notes, created_by, created_date, unique_id) 
	VALUES('$tid', '$tid', '$assigned', '$system_unique_id', '".$data['mfg']."', '".$data['vin']."', 'MIR inv num ".$data['inv']."', 'unassigned', '".date(storef,time())."', '$trailer_unique_id');";
	echo "Execute: ".$sql,EOL;
	if($execute){
		if(!$reslut = $mysqli->query($sql)){
			die($mysqli->error);	
		}
	}
}


echo "<h2>Systems updated list</h2>";
if(!empty($systems_updated)){
	foreach($systems_updated as $sys){
		echo $sys,EOL;
	}
}else{
	echo 'No Systems updated !';	
}


echo "<h2>Completed</h2>";
$mysqli->close();
scroll();

function scroll(){
	echo EOL,"<script>document.documentElement.scrollTop = getDocHeight();</script>",EOL;	
}

?>

<script type="text/javascript">
	clearInterval(scroller);
</script>
</body>
</html>

	
	
	
	
	
	
	
	
	
	
