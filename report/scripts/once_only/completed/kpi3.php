<?php

die('USED');

$debug = true;

define('EOL','<br>');

require($_SERVER['DOCUMENT_ROOT'].'/mysqlInfo.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

//Check if you ran kip3.php first
if(!isset($_GET['go'])){
	echo "Are you ready to run kpi3.php? Yes click link...",EOL,EOL;
	echo "<a href='kpi3.php?go'>Yes I am!</a>",EOL;
	die();
}

$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno){
	die('There was an error running the query [' . $mysqli->connect_error . ']');
}

//Remove bad dates from last_pm

echo "Removing 1970-01-01 from last_pm",EOL;
$sql="UPDATE systems_base AS sb SET sb.last_pm= NULL WHERE sb.last_pm = '1970-01-01T00:00:00Z';";
if(!$result = $mysqli->query($sql)){
	echo $mysqli->error;
	die("ERROR");
}

//Add pm_dates and pm_start
echo  "Checking pm_start and pm_dates columns",EOL;
$sql="SELECT * FROM systems_base LIMIT 1;";
if(!$result = $mysqli->query($sql)){
	echo $mysqli->error;
	die("ERROR");
}
$row = $result->fetch_assoc();
d($row);
if(!isset($row['pm_start'])){
	echo  "Adding pm_start and pm_dates columns",EOL;
	$sql="ALTER TABLE systems_base
    ADD COLUMN `pm_start` VARCHAR(10) NULL COLLATE 'utf8_unicode_ci' AFTER pm_freq,
    ADD COLUMN `pm_dates` VARCHAR(50) NULL COLLATE 'utf8_unicode_ci' AFTER pm_start;";
	if(!$result = $mysqli->query($sql)){
		echo $mysqli->error;
		die("ERROR");
	}
}

//Drop systems_notes
echo "Drop systems_notes table",EOL;
$sql="DROP TABLE IF EXISTS `systems_notes`;";
if(!$result = $mysqli->query($sql)){
	echo $mysqli->error;
	die("ERROR");
}

//Create table systems_notes
echo "Create systems_notes table",EOL;
$sql="CREATE TABLE `systems_notes` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`note` TEXT NULL COLLATE 'utf8_unicode_ci',
	`uid` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`date` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`system_unique_id` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`system_ver_unique_id` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`property` VARCHAR(2) NULL DEFAULT 'G' COMMENT 'G general, C credit note' COLLATE 'utf8_unicode_ci',
	`credit_hold` VARCHAR(2) NULL DEFAULT 'N' COMMENT 'is the system on credit hold y/n' COLLATE 'utf8_unicode_ci',
	PRIMARY KEY (`id`),
	INDEX `uid` (`uid`),
	CONSTRAINT `FK_systems_notes_users` FOREIGN KEY (`uid`) REFERENCES `users` (`uid`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1;";
if(!$result = $mysqli->query($sql)){
	echo $mysqli->error;
	die("ERROR");
}

//Move system credit hold notes to systems_notes table
echo "Move system credit hold notes to systems_notes table",EOL;
$sql="INSERT INTO systems_notes (note, uid, `date`, system_unique_id, system_ver_unique_id, property, credit_hold)
SELECT
CONCAT(\"System Credit Hold Note: \", sbc.credit_hold_notes) AS note,
'root' AS uid, IF(sbc.credit_hold_date != \"\", sbc.credit_hold_date, \"1970-01-01T12:00:00Z\") AS `date`,
sbc.unique_id AS system_unique_id,
sbc.ver_unique_id AS system_ver_unique_id,
\"C\" AS property,
sbc.credit_hold
FROM systems_base_cont AS sbc
WHERE sbc.credit_hold_notes != \"\";";
if(!$result = $mysqli->query($sql)){
	echo $mysqli->error;
	die("ERROR");
}

//Move systems_base notes to systems_notes
echo "Moving systems_base notes to systems_notes",EOL;
$sql="INSERT INTO systems_notes (note, uid, `date`, system_unique_id, system_ver_unique_id, property, credit_hold)
SELECT
sb.notes AS note, IF(sb.edited_by != \"\",
sb.edited_by, \"root\") AS uid,
IF(sb.edited_date != \"\", sb.edited_date, \"1970-01-01T12:00:00Z\") AS `date`,
sb.unique_id AS system_unique_id,
sbc.ver_unique_id AS system_ver_unique_id,
\"G\" AS property,
\"N\" AS credit_hold
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
WHERE sb.notes != \"\";";
if(!$result = $mysqli->query($sql)){
	echo $mysqli->error;
	die("ERROR");
}

//Move facilities cred_hold_notes to systems_notes
echo "Moving facilities cred_hold_notes to systems_notes",EOL;
$sql="INSERT INTO systems_notes (note, uid, `date`, system_unique_id, system_ver_unique_id, property, credit_hold)
SELECT
CONCAT(\"Facility Credit Hold Note: \", f.credit_hold_notes) AS note,
'root' AS uid, IF(f.credit_hold_date != \"\", f.credit_hold_date, \"1970-01-01T12:00:00Z\") AS `date`,
sbc.unique_id AS system_unique_id,
sbc.ver_unique_id AS system_ver_unique_id,
\"C\" AS property,
f.credit_hold
FROM systems_base_cont AS sbc
LEFT JOIN facilities AS f ON f.unique_id = sbc.facility_unique_id
WHERE f.credit_hold_notes != \"\";";
if(!$result = $mysqli->query($sql)){
	echo $mysqli->error;
	die("ERROR");
}

//Update systems_base_cont credit_hold from facilities
echo "Updating systems credit hold status from facilities",EOL;
$sql="SELECT
sbc.unique_id AS system_unique_id,
sbc.ver_unique_id AS system_ver_unique_id,
f.credit_hold
FROM systems_base_cont AS sbc
LEFT JOIN facilities AS f ON f.unique_id = sbc.facility_unique_id
WHERE f.credit_hold = \"Y\";";
if(!$result = $mysqli->query($sql)){
	echo $mysqli->error;
	die("ERROR");
}
$fc = array();
while($row = $result->fetch_assoc()){
	array_push($fc, $row['system_ver_unique_id']);
}
d($fc);
foreach($fc as $system){
	$sql="UPDATE systems_base_cont SET credit_hold = 'Y' WHERE ver_unique_id = '".$system."';";
	if(!$result = $mysqli->query($sql)){
		echo $mysqli->error;
		die("ERROR");
	}
}

//Move customers cred_hold_notes to systems_notes
echo "Moving customers cred_hold_notes to systems_notes",EOL;
$sql="INSERT INTO systems_notes (note, uid, `date`, system_unique_id, system_ver_unique_id, property, credit_hold)
SELECT
CONCAT(\"Customer Credit Hold Note: \", c.credit_hold_notes) AS note,
'root' AS uid, IF(c.credit_hold_date != \"\", c.credit_hold_date, \"1970-01-01T12:00:00Z\") AS `date`,
sbc.unique_id AS system_unique_id,
sbc.ver_unique_id AS system_ver_unique_id,
\"C\" AS property,
c.credit_hold
FROM systems_base_cont AS sbc
LEFT JOIN facilities AS f ON f.unique_id = sbc.facility_unique_id
LEFT JOIN customers AS c ON c.unique_id = f.customer_unique_id
WHERE c.credit_hold_notes != \"\";";
if(!$result = $mysqli->query($sql)){
	echo $mysqli->error;
	die("ERROR");
}

//Update systems_base_cont credit_hold from customers
echo "Updating systems credit hold status from customers",EOL;
$sql="SELECT
sbc.unique_id AS system_unique_id,
sbc.ver_unique_id AS system_ver_unique_id,
c.credit_hold
FROM systems_base_cont AS sbc
LEFT JOIN facilities AS f ON f.unique_id = sbc.facility_unique_id
LEFT JOIN customers AS c ON c.unique_id = f.customer_unique_id
WHERE c.credit_hold = \"Y\";";
if(!$result = $mysqli->query($sql)){
	echo $mysqli->error;
	die("ERROR");
}
$cc = array();
while($row = $result->fetch_assoc()){
	array_push($cc, $row['system_ver_unique_id']);
}
d($cc);
foreach($cc as $system){
	$sql="UPDATE systems_base_cont SET credit_hold = 'Y' WHERE ver_unique_id = '".$system."';";
	if(!$result = $mysqli->query($sql)){
		echo $mysqli->error;
		die("ERROR");
	}
}

//List systems on credit hold
echo "Systems on credit hold",EOL;
$sql="SELECT sbc.ver_unique_id, sbc.nickname FROM systems_base_cont AS sbc WHERE sbc.credit_hold = \"Y\";";
if(!$result = $mysqli->query($sql)){
	echo $mysqli->error;
	die("ERROR");
}
$credit_hold = array();
while($row = $result->fetch_assoc()){
	array_push($credit_hold, $row);
}
d($credit_hold);

//Insert financial permission
echo "Create new financial permission",EOL;
$sql="INSERT INTO users_perm_id (name, perm, notes, sequence) VALUES ('Manage Financial Data', 'perm_edit_financial', NULL, 4.6);";
if(!$result = $mysqli->query($sql)){
	echo $mysqli->error;
	die("ERROR");
}
$last_id = $mysqli->insert_id;

//Give managers and finance the new permission
echo "Give managers and finance the permission",EOL;
$sql="INSERT INTO users_role_perms (rid, pid) VALUES (4,$last_id), (3, $last_id);";
if(!$result = $mysqli->query($sql)){
	echo $mysqli->error;
	die("ERROR");
}

//Add financial columns to systems_base_cont
echo "Adding new financial columns to systems_base_cont",EOL;
$sql="ALTER TABLE `systems_base_cont`
	ADD COLUMN `contract_month_amt` DOUBLE NULL DEFAULT '0' AFTER `contract_hours`;";
if(!$result = $mysqli->query($sql)){
	echo $mysqli->error;
	die("ERROR");
}

//Create temp table aaa_temp_finance
echo "Create temp table aaa_temp_finance",EOL;
$sql="CREATE TABLE `aaa_temp_finance` (
	`system_id` VARCHAR(50) NULL,
	`amount` DOUBLE NULL
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;";
if(!$result = $mysqli->query($sql)){
	echo $mysqli->error;
	die("ERROR");
}


echo EOL,EOL;
echo "Import data into aaa_temp_finance BEFORE proceeding",EOL;
echo "Run kpi3_cleanup.php next!",EOL,EOL;
echo "<a href='kpi3_cleanup.php'>Hold your breath!</a>",EOL;
die("DONE");
?>