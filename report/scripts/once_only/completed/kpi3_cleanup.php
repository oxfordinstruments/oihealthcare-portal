<?php

$debug = true;

$no_obstart = true;
define('EOL','<br>');

require($_SERVER['DOCUMENT_ROOT'].'/mysqlInfo.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

//Check if you ran kip3.php first
if(!isset($_GET['clean'])){
	echo "Did you run kpi3.php first? Yes click link...",EOL,EOL;
	echo "<a href='kpi3_cleanup.php?clean'>Yes I ran kpi3.php first</a>",EOL;
	die();
}

$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno){
	die('There was an error running the query [' . $mysqli->connect_error . ']');
}

//Remove systems_base_cont credit_hold_notes
echo "Drop credit_hold_notes from systems_base_cont",EOL;
$sql="ALTER TABLE systems_base_cont DROP COLUMN `credit_hold_notes`;";
if(!$result = $mysqli->query($sql)){
	echo $mysqli->error;
	//die("ERROR");
}

//Remove systems_base notes
echo "Drop notes from systems_base",EOL;
$sql="ALTER TABLE systems_base DROP COLUMN `notes`;";
if(!$result = $mysqli->query($sql)){
	echo $mysqli->error;
	//die("ERROR");
}

//Remove facility credit_hold, credit_hold_notes, credit_hold_date
echo "Drop facility credit_hold, credit_hold_notes, credit_hold_date from facilities",EOL;
$sql="ALTER TABLE facilities DROP COLUMN `credit_hold`, DROP COLUMN `credit_hold_notes`, DROP COLUMN `credit_hold_date`;";
if(!$result = $mysqli->query($sql)){
	echo $mysqli->error;
	//die("ERROR");
}

//Remove customers credit_hold, credit_hold_notes, credit_hold_date
echo "Drop customers credit_hold, credit_hold_notes, credit_hold_date from facilities",EOL;
$sql="ALTER TABLE customers DROP COLUMN `credit_hold`, DROP COLUMN `credit_hold_notes`, DROP COLUMN `credit_hold_date`;";
if(!$result = $mysqli->query($sql)){
	echo $mysqli->error;
	//die("ERROR");
}

//Update systems_base_cont data from aaa_temp_finance
echo "Update systems_base_cont data from aaa_temp_finance",EOL;
$sql="UPDATE systems_base_cont AS sbc
RIGHT JOIN aaa_temp_finance AS tmp ON tmp.system_id = sbc.system_id 
SET sbc.contract_month_amt = tmp.amount;";
if(!$result = $mysqli->query($sql)){
	echo $mysqli->error;
	die("ERROR");
}

//Drop aaa_temp_finance
echo "Drop aaa_temp_finance",EOL;
$sql="DROP TABLE aaa_temp_finance;";
if(!$result = $mysqli->query($sql)){
	echo $mysqli->error;
	die("ERROR");
}

echo EOL,EOL;
echo "Run kpi3_excel.php next!",EOL,EOL;
echo "<a href='kpi3_excel.php'>This takes awhile!</a>",EOL;
die("DONE");
?>