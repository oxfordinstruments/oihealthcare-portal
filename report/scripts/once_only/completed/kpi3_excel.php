<html>
<head>
<body>
<?php
//error_reporting(E_ALL);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
ini_set('display_errors', 'On');
// Turn off output buffering
ini_set('output_buffering', 'off');
// Turn off PHP output compression
ini_set('zlib.output_compression', false);

//Flush (send) the output buffer and turn off output buffering
while (@ob_end_flush());

// Implicitly flush the buffer(s)
ini_set('implicit_flush', true);
ob_implicit_flush(true);

//header('Cache-Control: no-cache'); // recommended to prevent caching of event data.

for($i = 0; $i < 1000; $i++)
{
	echo ' ';
}

ob_flush();
flush();


$debug = false;

$settings = new SimpleXMLElement($_SERVER['DOCUMENT_ROOT'].'/settings.xml', null, true);
require_once($_SERVER['DOCUMENT_ROOT'].'/define_inc.php');
require($_SERVER['DOCUMENT_ROOT'].'/mysqlInfo.php');

$mysqli = new mysqli("$host", "$username", "$password", "$db_name");

if ($mysqli->connect_errno) {
	echo $mysqli->connect_error,EOL;
	die("ERROR!");
}

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

//echo "<html><head></head><body>";

echo "Cleaning up folder",EOL;
array_map('unlink', glob("kpi3_excel/*.xls"));
array_map('unlink', glob("kpi3_excel/*.zip"));
//ob_flush();
//flush();

$data = array();

//Get Engineers
$sql="SELECT IFNULL(u.uid,'misc') AS uid
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
LEFT JOIN systems_assigned_pri AS sap ON sap.system_ver_unique_id = sbc.ver_unique_id
LEFT JOIN users AS u ON u.uid = sap.uid
WHERE sbc.property != 'A' AND CAST(sb.pm_freq AS UNSIGNED) > 0
GROUP BY uid;";
s($sql);
if(!$result = $mysqli->query($sql)){
	echo $mysqli->error;
	die("ERROR");
}
while($row = $result->fetch_assoc()){
	//array_push($engineers,$row);
	$data[$row['uid']] = array('systems'=>array(), 'name'=>'');
}
d($data);

//Get Systems
$sql="SELECT sbc.system_id, sbc.nickname, mc.`type`, sb.pm_freq, sb.last_pm, IFNULL(u.name,'Misc') AS name, IFNULL(u.uid,'misc') AS uid, sbc.property
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
LEFT JOIN systems_assigned_pri AS sap ON sap.system_ver_unique_id = sbc.ver_unique_id
LEFT JOIN users AS u ON u.uid = sap.uid
LEFT JOIN misc_contracts AS mc ON mc.id = sbc.contract_type
WHERE sbc.property != 'A' AND CAST(sb.pm_freq AS UNSIGNED) > 0
ORDER BY name, sbc.nickname;";
s($sql);
if(!$result = $mysqli->query($sql)){
	echo $mysqli->error;
	die("ERROR");
}
while($row = $result->fetch_assoc()){
	$data[$row['uid']]['systems'][$row['system_id']] = array(
		'nickname'=>$row['nickname'],
		'type'=>$row['type'],
		'pm_freq'=>$row['pm_freq'],
		'last_pm'=>$row['last_pm'],
		'name'=>$row['name'],
		'property'=>$row['property']
	);
	$data[$row['uid']]['name'] = $row['name'];
}
d($data);


require_once ($_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/Classes/PHPExcel.php');

require_once ($_SERVER['DOCUMENT_ROOT'].'/resources/phpexcell/Classes/PHPExcel/IOFactory.php');

$template_file = 'KPI3.xls';
if (!file_exists($template_file)) {
	exit("Error the template is missing." . EOL);
}

foreach($data as $uid => $engineer){
	echo date(storef, time()),"&emsp; Making ". $engineer['name'].'.xls &emsp;';
	set_time_limit(120);
	PHPExcel_Cell::setValueBinder( new PHPExcel_Cell_AdvancedValueBinder() );
	$objPHPExcel = new PHPExcel();
	$objPHPExcel = PHPExcel_IOFactory::load($template_file);

	$objPHPExcel->setActiveSheetIndex(0);



	$objPHPExcel->getActiveSheet()->setTitle($engineer['name']);

	$objPHPExcel->getActiveSheet()->setCellValue('b2', $engineer['name']);
	$objPHPExcel->getActiveSheet()->setCellValue('g4', "=IF(COUNT(G5:G205) > 0, \"PM\", \"\")");
	$objPHPExcel->getActiveSheet()->setCellValue('h4', "=IF(COUNT(H5:H205) > 0, \"PM\", \"\")");
	$objPHPExcel->getActiveSheet()->setCellValue('i4', "=IF(COUNT(I5:I205) > 0, \"PM\", \"\")");
	$objPHPExcel->getActiveSheet()->setCellValue('j4', "=IF(COUNT(J5:J205) > 0, \"PM\", \"\")");
	$objPHPExcel->getActiveSheet()->setCellValue('k4', "=IF(COUNT(K5:K205) > 0, \"PM\", \"\")");
	$objPHPExcel->getActiveSheet()->setCellValue('l4', "=IF(COUNT(L5:L205) > 0, \"PM\", \"\")");


	$_num = 5;
	foreach($engineer['systems'] as $system_id=>$system){
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$_num, $system_id);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$_num, $system['nickname']);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$_num, $system['type']);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$_num, $system['pm_freq']);
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$_num, date(phpdispfd,strtotime($system['last_pm'])));
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$_num, " ");

		$objPHPExcel->getActiveSheet()->setCellValue('g'.$_num, "=IF(AND(COUNT(D".$_num.")=1, COUNT(F".$_num.")=1), DATE(YEAR(F".$_num."),MONTH(F".$_num."),DAY(F".$_num.")), \" \")");
		$objPHPExcel->getActiveSheet()
			->getStyle('g'.$_num)
			->getNumberFormat()
			->setFormatCode("yyyy-mm-dd");
		$objPHPExcel->getActiveSheet()->setCellValue('h'.$_num, "=IF(AND(COUNT(D".$_num.")=1, COUNT(F".$_num.")=1), IF(12/D".$_num." > 1,  DATE(YEAR(G".$_num."), MONTH(G".$_num.") + D".$_num.", DAY(G".$_num.")), \"\"),\"\")");
		$objPHPExcel->getActiveSheet()
			->getStyle('h'.$_num)
			->getNumberFormat()
			->setFormatCode("yyyy-mm-dd");
		$objPHPExcel->getActiveSheet()->setCellValue('i'.$_num, "=IF(AND(COUNT(D".$_num.")=1, COUNT(F".$_num.")=1), IF(12/D".$_num." > 2, DATE(YEAR(H".$_num."), MONTH(H".$_num.") + D".$_num.", DAY(H".$_num.")), \"\"),\"\")");
		$objPHPExcel->getActiveSheet()
			->getStyle('i'.$_num)
			->getNumberFormat()
			->setFormatCode("yyyy-mm-dd");
		$objPHPExcel->getActiveSheet()->setCellValue('j'.$_num, "=IF(AND(COUNT(D".$_num.")=1, COUNT(F".$_num.")=1), IF(12/D".$_num." >= 4, DATE(YEAR(I".$_num."), MONTH(I".$_num.") + D".$_num.", DAY(I".$_num.")), \"\"), \"\")");
		$objPHPExcel->getActiveSheet()
			->getStyle('j'.$_num)
			->getNumberFormat()
			->setFormatCode("yyyy-mm-dd");
		$objPHPExcel->getActiveSheet()->setCellValue('k'.$_num, "=IF(AND(COUNT(D".$_num.")=1, COUNT(F".$_num.")=1), IF(12/D".$_num." >= 6, DATE(YEAR(J".$_num."), MONTH(J".$_num.") + D".$_num.", DAY(J".$_num.")), \"\"), \"\")");
		$objPHPExcel->getActiveSheet()
			->getStyle('k'.$_num)
			->getNumberFormat()
			->setFormatCode("yyyy-mm-dd");
		$objPHPExcel->getActiveSheet()->setCellValue('l'.$_num, "=IF(AND(COUNT(D".$_num.")=1, COUNT(F".$_num.")=1), IF(12/D".$_num." >= 6,DATE(YEAR(K".$_num."), MONTH(K".$_num.") + D".$_num.", DAY(K".$_num.")), \"\"), \"\")");
		$objPHPExcel->getActiveSheet()
			->getStyle('l'.$_num)
			->getNumberFormat()
			->setFormatCode("yyyy-mm-dd");

		$_num++;
	}

	$objPHPExcel->getActiveSheet()->getProtection()->setPassword('Plat1num');
	$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
	//$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
	//$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
	//$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
	$objPHPExcel->getActiveSheet()->getProtection()->setFormatColumns(true);


	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	//$objWriter->setPreCalculateFormulas(true);
	$objWriter->save('kpi3_excel/'.$engineer['name'].'.xls');
	echo date(storef, time()),EOL;
	ob_flush();
	flush();
}

?>

		<script>
			window.location = "kpi3_excel_zip.php";
		</script>
	</body>
</head>
</html>


