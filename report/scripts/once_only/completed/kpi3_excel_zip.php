<?php
//error_reporting(E_ALL);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
ini_set('display_errors', 'On');

ob_start();

$debug = false;

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

$zip = new ZipArchive();
$zip->open('kpi3_excel/PMs.zip', ZipArchive::OVERWRITE);
$zip->addGlob('kpi3_excel/*.xls');
$zip->close();

$file = 'kpi3_excel/PMs.zip';
header('Content-type: application/zip'); // Please check this, i just guessed
header('Content-Disposition: attachment; filename="'.basename($file).'"');
header('Content-Length: '.filesize($file));
readfile($file);

ob_end_flush();


exit("DONE");
?>