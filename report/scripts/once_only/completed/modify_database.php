<?php 
/*###############################################################################################
Order of execution:
	1. Load Old database into oiheal5_report_tmp
	2. Browser run update_date_format.php
	3. Browser run modify_database.php
	4. CLI run insert_db_data_1.php
	6. Browser run insert_db-data_2.php
*/###############################################################################################


$debug = false;
$just_prepare = false;

if(isset($_SERVER['argv'][1]) and $_SERVER['argv'][1] == '--cron'){
	define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : PHP_EOL);
	$base_path = preg_replace("/(public_html).*/",'public_html',dirname(__FILE__),2);
}else{
	define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
	$base_path = $_SERVER['DOCUMENT_ROOT'];
}

$settings = new SimpleXMLElement($base_path.'/settings.xml', null, true);
require_once($base_path.'/define_inc.php');


require_once($base_path.'/resources/kint/Kint.class.php');

Kint::enabled(true);	


$host="localhost";
$username="oiheal5_justin";
$password="b00b135";
$db_name="oiheal5_report_tmp";
$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno){
	die('There was an error running the query [' . $mysqli->connect_error . ']');
}


echo "Making changes to the database...",EOL;
$sql_arr = explode(';',sql_data());
foreach($sql_arr as $sql){
	if(strlen($sql) > 5){
		d($sql);
		if(!$just_prepare){
			if(!$result = $mysqli->query($sql)){
				die($mysqli->error);	
			}
		}
	}
}



function sql_data(){
$sql=<<<EOT
update sites as s set s.bill_name = s.site_name where s.bill_name is null or s.bill_name = '';
update sites as s set s.bill_contact = s.site_contact_name where s.bill_contact is null or s.bill_contact = '';
update sites as s set s.bill_address = s.site_address where s.bill_address is null or s.bill_address = '';
update sites as s set s.bill_city = s.site_city where s.bill_city is null or s.bill_city = '';
update sites as s set s.bill_state = s.site_state where s.bill_state is null or s.bill_state = '';
update sites as s set s.bill_zip = s.site_zip where s.bill_zip is null or s.bill_zip = '';
update sites as s set s.bill_phone = s.site_phone where s.bill_phone is null or s.bill_phone = '';
update sites as s set s.bill_fax = s.site_fax where s.bill_fax is null or s.bill_fax = '';
update sites as s set s.bill_email = s.site_contact_email where s.bill_email is null or s.bill_email = '';

EOT;
return $sql;	
}

die("DONE");
?>