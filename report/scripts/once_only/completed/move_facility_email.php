<?php
die("DONE");

/*
 * Use this script to prepare for new email list feature
 */

//
// Error reporting
//
//error_reporting(E_ALL);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
ini_set('display_errors', 'On');

$debug = false;
if(isset($_GET['debug'])){
	$debug = true;
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

define('EOL', '<br>');
$errors = false;

echo date('m/d/Y H:i',time())," - ","Begin email move",EOL;

echo date('m/d/Y H:i',time())," - ","Getting system data and checking facility data",EOL;
$sql="SELECT sbc.property, sbc.facility_unique_id, sbc.system_id, sbc.ver_unique_id
FROM systems_base_cont AS sbc;";
if(!$resultS = $mysqli->query($sql)){
	echo $mysqli->error;
	die("SQL ERROR");
}
$pattern = '/^([a-zA-Z0-9\.\-\_])+(@)+([a-zA-Z0-9\.\-\_])+([\.])([a-zA-Z])+/';
$system_data = array();
while($rowS = $resultS->fetch_assoc()){
	$sql="SELECT f.facility_id, f.unique_id, f.email_list, f.property
	FROM facilities AS f
	WHERE f.email_list != \"\" AND f.unique_id = '".$rowS['facility_unique_id']."';";
	if(!$resultF = $mysqli->query($sql)){
		echo $mysqli->error;
		die("SQL ERROR");
	}
	if($resultF->num_rows > 0){
		$rowF = $resultF->fetch_assoc();
		$facility_emails = explode(';',$rowF['email_list']);
		foreach( $facility_emails as $email){
			if(preg_match($pattern,trim($email),$matches) == 0){
				$errors = true;
				echo "FAILED: ",$email,EOL;
				echo $rowF['facility_id'], ' - ', $rowF['unique_id'], ' - ', $rowF['email_list'], EOL;
			}
		}
		if(!$errors){
			$system_data[$rowS['ver_unique_id']] = array('property' => $rowS['property'], 'system_id' => $rowS['system_id'], 'facility_unique_id' => $rowS['facility_unique_id'], 'emails' => $facility_emails);
		}
	}
}
d($system_data);

if($errors){die("ERRORS FOUND");}
echo date('m/d/Y H:i',time())," - ","No errors found",EOL;


echo date('m/d/Y H:i',time())," - ","Dropping customer_email_list table",EOL;
$sql="DROP TABLE IF EXISTS `customers_email_list`;";
if(!$result = $mysqli->query($sql)){
	echo $mysqli->error;
	die("SQL ERROR");
}

echo date('m/d/Y H:i',time())," - ","Creaating customer_email_list table",EOL;
$sql="CREATE TABLE `customers_email_list` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`email` VARCHAR(100) NOT NULL COLLATE 'utf8_unicode_ci',
	`email_unique_id` VARCHAR(50) NOT NULL COLLATE 'utf8_unicode_ci',
	`name` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`system_ver_unique_id` VARCHAR(50) NOT NULL COLLATE 'utf8_unicode_ci',
	`system_id` VARCHAR(10) NOT NULL COLLATE 'utf8_unicode_ci',
	`active` VARCHAR(2) NOT NULL DEFAULT 'Y' COLLATE 'utf8_unicode_ci',
	`opt_out_date` VARCHAR(20) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`rcv_service_report` VARCHAR(2) NOT NULL DEFAULT 'Y' COLLATE 'utf8_unicode_ci',
	`rcv_service_request` VARCHAR(2) NOT NULL DEFAULT 'Y' COLLATE 'utf8_unicode_ci',
	`created_date` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`edited_date` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB;";
if(!$result = $mysqli->query($sql)){
	echo $mysqli->error;
	die("SQL ERROR");
}


echo date('m/d/Y H:i',time())," - ","Inserting data",EOL;

foreach($system_data as $key=>$data){
	foreach($data['emails'] as $email){
		$name = explode('@',$email);
		$name = $name[0];
		$new_name = preg_replace('/\./',' ',$name);
		if($name != $new_name){
			$name = ucwords($new_name);
		}
		if(strtolower($data['property']) == 'a'){
			$sql = "INSERT INTO customers_email_list (email, name, system_ver_unique_id, system_id, email_unique_id, active, rcv_service_report, rcv_service_request, created_date) VALUES ('" . trim($email) . "','" . trim($name) . "','" . $key . "','" . $data['system_id'] . "','','N','N','N','" . date(storef,time()) . "');";
		}else{
			$sql = "INSERT INTO customers_email_list (email, name, system_ver_unique_id, system_id, email_unique_id, created_date) VALUES ('" . trim($email) . "','" . trim($name) . "','" . $key . "','" . $data['system_id'] . "','','" . date(storef,time()) . "');";
		}
		if(!$result = $mysqli->query($sql)){
			echo $mysqli->error;
			die("SQL ERROR");
		}
	}
}

echo date('m/d/Y H:i',time())," - ","Modifying data",EOL;
$sql="SELECT email FROM customers_email_list GROUP BY email;";
if(!$result = $mysqli->query($sql)){
	echo $mysqli->error;
	die("SQL ERROR");
}
$emails = array();
while($row = $result->fetch_assoc()){
	array_push($emails, $row['email']);
}
d($emails);
foreach($emails as $email){
	$unique_id = md5(uniqid());
	$sql="UPDATE customers_email_list SET email_unique_id = '".$unique_id."' WHERE email = '".$email."';";
	if(!$result = $mysqli->query($sql)){
		echo $mysqli->error;
		die("SQL ERROR");
	}
}


echo date('m/d/Y H:i',time())," - ","Checking registration_requests table",EOL;

$sql="SHOW COLUMNS FROM registration_requests;";
if(!$result = $mysqli->query($sql)){
	echo $mysqli->error;
	die("SQL ERROR");
}
$found_email_list = false;
while($row = $result->fetch_assoc()){
	if($row['Field'] == 'email_list'){
		$found_email_list = true;
	}
}
if(!$found_email_list){
	echo date('m/d/Y H:i',time())," - ","Altering registration_requests table",EOL;
	$sql="ALTER TABLE `registration_requests`
	ADD COLUMN `email_list` VARCHAR(2) NOT NULL DEFAULT 'N' AFTER `registered`;";
	if(!$result = $mysqli->query($sql)){
		echo $mysqli->error;
		die("SQL ERROR");
	}
	$sql="ALTER TABLE `registration_requests`
	ADD COLUMN `used` VARCHAR(2) NOT NULL DEFAULT 'N' AFTER `registered`;";
	if(!$result = $mysqli->query($sql)){
		echo $mysqli->error;
		die("SQL ERROR");
	}
	$sql="UPDATE `registration_requests` SET `used` = 'Y';";
	if(!$result = $mysqli->query($sql)){
		echo $mysqli->error;
		die("SQL ERROR");
	}
	$sql="ALTER TABLE `facilities` 
	CHANGE COLUMN `email_list` `email_list` VARCHAR(255) NULL DEFAULT NULL COMMENT 'NOT USED AS OF branch 34_email_list' COLLATE 'utf8_unicode_ci' AFTER `notes`;";
	$sql="UPDATE `registration_requests` SET `used` = 'Y';";
	if(!$result = $mysqli->query($sql)){
		echo $mysqli->error;
		die("SQL ERROR");
	}
}

echo date('m/d/Y H:i',time())," - ","Done",EOL;
?>