<?php 
die("DONE");

/*
2015-08-20 Evotodi
This script modifies the users_* tables to move the permission, roles, and groups to sperate tables

It must be run once the updated pages have been uploaded.

*/

ob_start();//Start output buffering

$debug = true;
if(isset($_GET['debug'])){
	$debug = true;	
}

$execute = false; //Execute sql update/insert/delete
if(isset($_GET['execute'])){
	$execute = true;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/define_inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/php_utils.php');
$php_utils = new phpUtils();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

// Turn off output buffering
ini_set('output_buffering', 'off');
// Turn off PHP output compression
ini_set('zlib.output_compression', false);
         
//Flush (send) the output buffer and turn off output buffering
//ob_end_flush();
while (@ob_end_flush());
         
// Implicitly flush the buffer(s)
ini_set('implicit_flush', true);
ob_implicit_flush(true);


$vision_host = '67.199.146.103';
$vision_username = 'oivisi5_report';
$vision_password = 'Plat1num';
$vision_db_name = 'oivisi5_mm';

$mysqli_vision = new mysqli("$vision_host", "$vision_username", "$vision_password", "$vision_db_name");
if ($mysqli_vision->connect_errno) {die('There was an error running the query [' . $mysqli_vision->connect_error . ']');}

$sql = "SELECT * FROM sites;";
if(!$result = $mysqli_vision->query($sql)){
	die($mysqli_vision->error);	
}
$vision_sites = array();
while($row = $result->fetch_assoc()){
	array_push($vision_sites, $row);	
}
$mysqli_vision->close();
d($vision_sites);


require($_SERVER['DOCUMENT_ROOT'].'/mysqlInfo.php');
$mysqli_local = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli_local->connect_errno) {die('There was an error running the query [' . $mysqli_local->connect_error . ']');}

$sql="UPDATE systems SET he_monitor_serial = NULL;";
s($sql);
if($execute){
	if(!$result = $mysqli_local->query($sql)){
		die($mysqli_local->error);	
	}
}

$errors = array();
foreach($vision_sites as $site){
	$sql = "UPDATE systems SET he_monitor = 'Y', he_monitor_serial = '".$site['mac']."' WHERE system_id = 'MR".$site['site_id']."' LIMIT 1;";	
	s($sql);
	if($execute){
		if(!$result = $mysqli_local->query($sql)){
			die($mysqli_local->error);	
		}
		if($mysqli_local->affected_rows == 0){
			array_push($errors, $site['site_id']);	
		}
	}
}

$sql = "SELECT system_id
FROM systems
WHERE he_monitor = 'y' AND (he_monitor_serial IS NULL OR he_monitor_serial = '');";
if(!$result = $mysqli_local->query($sql)){
	die($mysqli_local->error);	
}
echo "Local Systems with monitor = y and no mac",EOL;
while($row = $result->fetch_assoc()){
	echo $row['system_id'],EOL;
}
$mysqli_local->close();

echo "ERRORS:",EOL;
d($errors);
?>