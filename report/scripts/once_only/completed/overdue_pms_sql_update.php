<?php
die("DONE");

/*
 * Use this script to prepare db for the new kpi3
 */

//
// Error reporting
//
//error_reporting(E_ALL);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
ini_set('display_errors', 'On');

$debug = true;
$insert = true;

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/php_utils.php');
$php_utils =  new phpUtils();

define('EOL', '<br>');
$errors = false;

$php_utils->message('Begin');//-----------------------------------------------------------------------------------------------

$php_utils->message('Checking for existing permissions',true);//-----------------------------------------------------------------------------------------------

$perm_overdue = false;
$perm_cont = false;
$seq_updated = false;
$overdue_users = array('davisj', 'mardikianj', 'frazierm', 'bringolfk', 'hennessyl');
$cont_users = array('davisj', 'mardikianj', 'frazierm', 'simmonss', 'bringolfk', 'bringolfm', 'fallj', 'hengemuhlew');

$sql="SELECT p.id, p.name, p.perm, p.sequence FROM users_perm_id AS p ORDER BY sequence;";
if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }

$perms = array();
while($row = $result->fetch_assoc()){
	switch($row['perm']){
		case 'perm_rcv_pm_overdue':
			$perm_overdue = true;
			break;
		case 'perm_rcv_cont_met':
			$perm_cont = true;
			break;
	}
	if($row['id'] == 1 and $row['sequence'] == 5010.4){
		$seq_updated = true;
	}
	$perms[$row['id']] = array(
		'name' => $row['name'],
		'perm' => $row['perm'],
		'sequence' => $row['sequence']
	);
}
$php_utils->message('Checking permissions sequence');//-----------------------------------------------------------------------------------------------
if(!$seq_updated){
	$php_utils->message('Updating permissions sequence');
	foreach($perms as $key=>$val){
		$split = explode('.',(string)$val['sequence']);
		$a = intval($split[0]) * 1000;
		$b = intval($split[1]) * 10;
		$seq = $a + $b;
		switch($key){
			case 9:
				$sql="UPDATE users_perm_id SET sequence = 510.4 WHERE id = $key";
				break;
			case 17:
				$sql="UPDATE users_perm_id SET sequence = 520.4 WHERE id = $key";
				break;
			default:
				$sql="UPDATE users_perm_id SET sequence = $seq.4 WHERE id = $key";
				break;
		}
		echo $sql,EOL;
		if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}
	}
}

$php_utils->message('Checking for perm_rcv_pm_overdue');//-----------------------------------------------------------------------------------------------
if(!$perm_overdue){
	$php_utils->message('Creating perm_rcv_pm_overdue');
	$sql="INSERT INTO users_perm_id ( name, perm, sequence )VALUES( 'Receive PMs Overdue/Late Email', 'perm_rcv_pm_overdue', 2100.4 );";
	echo $sql,EOL;
	if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}

	$sql="SELECT id FROM users_perm_id WHERE perm = 'perm_rcv_pm_overdue';";
	if($insert){
		if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }
		$row = $result->fetch_assoc();
	}else{
		$row['id'] = 5555;
	}

	foreach($overdue_users as $user){
		$sql="INSERT INTO users_perms ( uid, pid )VALUES( '".$user."', ".$row['id']." );";
		echo $sql,EOL;
		if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}
	}

}

$php_utils->message('Checking for perm_rcv_cont_met');//-----------------------------------------------------------------------------------------------
if(!$perm_cont){
	$php_utils->message('Creating perm_rcv_cont_met');
	$sql="INSERT INTO users_perm_id ( name, perm, notes, sequence )VALUES( 'Receive Contracts Fulfilled KPI Email', 'perm_rcv_cont_met', '(note: management role only)', 2150.4 );";
	echo $sql,EOL;
	if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}

	$sql="SELECT id FROM users_perm_id WHERE perm = 'perm_rcv_cont_met';";
	if($insert){
		if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }
		$row = $result->fetch_assoc();
	}else{
		$row['id'] = 6666;
	}

	foreach($cont_users as $user){
		$sql="INSERT INTO users_perms ( uid, pid )VALUES( '".$user."', ".$row['id']." );";
		echo $sql,EOL;
		if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}
	}
}

$php_utils->message('Checking for existing preferances',true);//-----------------------------------------------------------------------------------------------

$pref_overdue = false;
$pref_cont = false;

$sql="SELECT p.id, p.name, p.perm, p.sequence FROM users_perm_id AS p ORDER BY sequence;";
if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }

$prefs = array();
while($row = $result->fetch_assoc()){
	switch($row['pref']){
		case 'pref_rcv_pm_overdue':
			$pref_overdue = true;
			break;
		case 'pref_rcv_cont_met':
			$pref_cont = true;
			break;
	}
	$prefs[$row['id']] = array(
		'name' => $row['name'],
		'perm' => $row['perm']
	);
}

$php_utils->message('Checking for pref_rcv_pm_overdue');//-----------------------------------------------------------------------------------------------
if(!$perm_overdue){
	$php_utils->message('Creating pref_rcv_pm_overdue');
	$sql="INSERT INTO users_pref_id ( name, pref )VALUES( 'Rcv PM Overdue', 'pref_rcv_pm_overdue');";
	echo $sql,EOL;
	if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}

	$sql="SELECT id FROM users_pref_id WHERE pref = 'pref_rcv_pm_overdue';";
	if($insert){
		if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }
		$row = $result->fetch_assoc();
	}else{
		$row['id'] = 5555;
	}

	foreach($overdue_users as $user){
		$sql="INSERT INTO users_prefs ( uid, pid )VALUES( '".$user."', ".$row['id']." );";
		echo $sql,EOL;
		if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}
	}

}

$php_utils->message('Checking for pref_rcv_cont_met');//-----------------------------------------------------------------------------------------------
if(!$perm_cont){
	$php_utils->message('Creating pref_rcv_cont_met');
	$sql="INSERT INTO users_pref_id ( name, pref )VALUES( 'Rcv Contract KPI', 'pref_rcv_cont_met' );";
	echo $sql,EOL;
	if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}

	$sql="SELECT id FROM users_pref_id WHERE pref = 'pref_rcv_cont_met';";
	if($insert){
		if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }
		$row = $result->fetch_assoc();
	}else{
		$row['id'] = 6666;
	}

	foreach($cont_users as $user){
		$sql="INSERT INTO users_prefs ( uid, pid )VALUES( '".$user."', ".$row['id']." );";
		echo $sql,EOL;
		if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}
	}
}

$php_utils->message('Checking for pm_contract_calc');//-----------------------------------------------------------------------------------------------
$sql="SELECT * FROM misc_contracts";
if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }
$fields = $result->fetch_fields();
$pm_cont_calc = false;
foreach($fields as $field){
	if($field->name == 'pm_contract_calc'){
		$pm_cont_calc = true;
	}
}

if(!$pm_cont_calc){
	$php_utils->message('Creating pm_contract_calc in misc_contracts table');
	$sql="ALTER TABLE `misc_contracts` ADD COLUMN `pm_contract_calc` VARCHAR(2) NULL DEFAULT 'N' AFTER `finance_calc`;";
	echo $sql,EOL;
	if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}
	$sql="UPDATE misc_contracts SET pm_contract_calc = 'Y' WHERE id IN(1,3,7,8);";
	echo $sql,EOL;
	if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}
}


$php_utils->message('Creating kpi_pms_contracted table');//-----------------------------------------------------------------------------------------------
$sql="CREATE TABLE IF NOT EXISTS `kpi_pms_contracted` (
	`year` INT UNSIGNED NOT NULL,
	`percent_pass` INT UNSIGNED NOT NULL,
	PRIMARY KEY (`year`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB;";
echo $sql,EOL;
if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }

$sql="INSERT INTO kpi_pms_contracted (`year`, percent_pass) VALUES (2010,0), (2011,0), (2012,0), (2013,0), (2014,0), (2015,0)
ON DUPLICATE KEY UPDATE percent_pass=VALUES(percent_pass);";
echo $sql,EOL;
if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}

$php_utils->message('Checking for misc_updates table',true);//-----------------------------------------------------------------------------------------------
$sql="UPDATE misc_updates SET script = '/report/scripts/once_only/overdue_pms_sql_update.php', notes = 'run before #10_KPI_3 branch merge' WHERE id = 1;";
if(!$result = $mysqli->query($sql)){
	$php_utils->message('Query failed so creating the table');
	$sql="CREATE TABLE `misc_updates` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`script` VARCHAR(500) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`notes` TEXT NULL COLLATE 'utf8_unicode_ci',
	PRIMARY KEY (`id`)) COMMENT='Code and SQL updates needing executed' COLLATE='utf8_unicode_ci' ENGINE=InnoDB AUTO_INCREMENT=1;";
	echo $sql,EOL;
	if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}

	$php_utils->message('Inserting this file');
	$sql="INSERT INTO misc_updates (script, notes) VALUES ('/report/scripts/once_only/overdue_pms_sql_update.php', 'run before #10_KPI_3 branch merge');";
	if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}
}

$php_utils->message('Updating engineer\'s futures permissions',true);//-----------------------------------------------------------------------------------------------
$engineers_found = false;
$sql="SELECT u.uid
FROM users AS u
LEFT JOIN users_roles AS ur ON ur.uid = u.uid
WHERE u.active = 'y' AND ur.rid = 1;";
if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }
$engineers = array();
while($row = $result->fetch_assoc()){
	array_push($engineers, $row['uid']);
}
$sql_build="INSERT INTO users_perms (`uid`, `pid`) VALUES ";
foreach($engineers as $engineer){
	$sql="SELECT * FROM users_perms WHERE uid = '".$engineer."' AND pid = 37;";
	if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }
	if($result->num_rows == 0){
		$engineers_found = true;
		$sql_build .= "('".$engineer."', 37), ";
	}
}
$sql_build = rtrim($sql_build,', ');
$sql_build .= ";";
if($insert and $engineers_found){
	echo $sql_build,EOL;
	if(!$result = $mysqli->query($sql_build)){ die('There was an error running the query [' . $mysqli->error . ']'); }
}

$php_utils->message('Updating engineer\'s futures preferences',true);//-----------------------------------------------------------------------------------------------
$engineers_found = false;
$sql="SELECT u.uid
FROM users AS u
LEFT JOIN users_roles AS ur ON ur.uid = u.uid
WHERE u.active = 'y' AND ur.rid = 1;";
if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }
$engineers = array();
while($row = $result->fetch_assoc()){
	array_push($engineers, $row['uid']);
}
$sql_build="INSERT INTO users_prefs (`uid`, `pid`) VALUES ";
foreach($engineers as $engineer){
	$sql="SELECT * FROM users_prefs WHERE uid = '".$engineer."' AND pid = 11;";
	if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }
	if($result->num_rows == 0){
		$engineers_found = true;
		$sql_build .= "('".$engineer."', 11), ";
	}
}
$sql_build = rtrim($sql_build,', ');
$sql_build .= ";";
if($insert and $engineers_found){
	echo $sql_build,EOL;
	if(!$result = $mysqli->query($sql_build)){ die('There was an error running the query [' . $mysqli->error . ']'); }
}

$php_utils->message('Checking for phone hours column in systems_hours table',true);//-----------------------------------------------------------------------------------------------
$sql="SELECT * FROM systems_hours LIMIT 1;";
if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }
$fields = $result->fetch_fields();
$phone_hours = false;
foreach($fields as $field){
	if($field->name == 'phone'){
		$phone_hours = true;
	}
}
if(!$phone_hours){
	$php_utils->message('Adding phone hours column to systems_hours table',true);//-----------------------------------------------------------------------------------------------
	$sql="ALTER TABLE `systems_hours` ADD COLUMN `phone` VARCHAR(10) NULL DEFAULT '0' AFTER `ot_travel`;";
	echo $sql,EOL;
	if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}
}

$php_utils->message('Checking for phone hours tooltip',true);//-----------------------------------------------------------------------------------------------
$sql="SELECT element FROM misc_tool_tips;";
if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }
$phone_hours = false;
while($row = $result->fetch_assoc()){
	if($row['element'] == 'hours_phone_lbl'){
		$phone_hours = true;
	}
}
if(!$phone_hours){
	$php_utils->message('Adding phone hours tooltip',true);//-----------------------------------------------------------------------------------------------
	$sql="INSERT INTO misc_tool_tips (page, element, tip) VALUES ('report_new_edit', 'hours_phone_lbl', 'Amount of hours engineer was on the phone with a customer');";
	echo $sql,EOL;
	if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}
}

$php_utils->message('Checking for misc_states serviced column',true);//-----------------------------------------------------------------------------------------------
$sql="SELECT * FROM misc_states LIMIT 1;";
if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }
$fields = $result->fetch_fields();
$column = false;
foreach($fields as $field){
	if($field->name == 'serviced'){
		$column = true;
	}
}

if(!$column){
	$php_utils->message('Adding misc_states serviced column',true);//-----------------------------------------------------------------------------------------------
	$sql="ALTER TABLE `misc_states`	ADD COLUMN `serviced` VARCHAR(2) NOT NULL DEFAULT 'N' AFTER `abv`;";
	echo $sql,EOL;
	if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}
}

$php_utils->message('Updating serviced states',true);//-----------------------------------------------------------------------------------------------
$sql="SELECT f.state, f.name, f.facility_id
FROM facilities AS f
GROUP BY f.state;";
if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }
$sql_build = array();
while($row = $result->fetch_assoc()){
	if($row['state'] == ""){
		d($row);
		throw new Exception("Facility missing a state. Fix and rerun script.",0);
	}
	array_push($sql_build, "UPDATE misc_states SET serviced = 'Y' WHERE abv = '".$row['state']."';");
}
d($sql_build);
foreach($sql_build as $sql){
	echo $sql,EOL;
	if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}
}

$php_utils->message('Creating misc_2579_words table',true);//-----------------------------------------------------------------------------------------------
$sql="CREATE TABLE IF NOT EXISTS `misc_2579_words` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`word` VARCHAR(50) NOT NULL COLLATE 'utf8_unicode_ci',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1;";
if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }

$php_utils->message('Clearing misc_2579_words table',true);//-----------------------------------------------------------------------------------------------
$sql="TRUNCATE `misc_2579_words`;";
if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }

$php_utils->message('Filling misc_2579_words table',true);//-----------------------------------------------------------------------------------------------
$sql_build="INSERT INTO misc_2579_words (word) VALUES ";
$words = array(
	"tube", "tank", "generator", "hv", "inverter", "collimator", "table", "cradle", "xray", "x-ray", "jedi", "zhot", "hemrc", "obc", "orc", "orp", "ebox", "detector"
);
foreach($words as $word){
	$sql_build .= "('".$word."'),";
}
$sql_build = rtrim($sql_build, ",");
$sql_build .= ";";
echo $sql_build,EOL;
if($insert){if(!$result = $mysqli->query($sql_build)){ die('There was an error running the query [' . $mysqli->error . ']'); }}

$php_utils->message('Creating email_actions table',true);//-----------------------------------------------------------------------------------------------
$sql="CREATE TABLE IF NOT EXISTS `email_actions` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`complete` VARCHAR(2) NULL DEFAULT 'N' COLLATE 'utf8_unicode_ci',
	`script` VARCHAR(1024) NOT NULL COLLATE 'utf8_unicode_ci',
	`args` VARCHAR(1024) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`uid` VARCHAR(100) NOT NULL COLLATE 'utf8_unicode_ci',
	`schedule` TIMESTAMP NULL DEFAULT NULL,
	`created_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
	`completed_time` TIMESTAMP NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1;";
if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }

$php_utils->message('End',true);
?>