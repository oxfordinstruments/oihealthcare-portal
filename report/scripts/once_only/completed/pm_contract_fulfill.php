<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 2/2/2017
 * Time: 2:23 PM
 */

//
// Error reporting
//
//error_reporting(E_ALL);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
ini_set('display_errors', 'On');

$debug = true;
$insert = true;

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/php_utils.php');
$php_utils =  new phpUtils();

define('EOL', '<br>');
$errors = false;

$php_utils->message('Begin');//-----------------------------------------------------------------------------------------------

$delete_users = array('davisj', 'mardikianj', 'frazierm', 'mellol');

$php_utils->message('Getting existing permissions',true);//-----------------------------------------------------------------------------------------------

$sql="SELECT p.id, p.name, p.perm, p.sequence FROM users_perm_id AS p ORDER BY sequence;";
if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }

$perm_del_system = false;
$perms = array();
while($row = $result->fetch_assoc()){
	switch($row['perm']){
		case 'perm_del_system':
			$perm_del_system = true;
			break;
	}
	$perms[$row['id']] = array(
		'name' => $row['name'],
		'perm' => $row['perm'],
		'sequence' => $row['sequence']
	);
}
d($perms);

$php_utils->message('Checking for perm_del_system');//-----------------------------------------------------------------------------------------------
if(!$perm_del_system){
	$php_utils->message('Creating perm_del_system');
	$sql="INSERT INTO users_perm_id ( name, perm, sequence )VALUES( 'Delete Systems Version', 'perm_del_system', 4070.4 );";
	echo $sql,EOL;
	if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}

	$sql="SELECT id FROM users_perm_id WHERE perm = 'perm_del_system';";
	if($insert){
		if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }
		$row = $result->fetch_assoc();
	}else{
		$row['id'] = 5555;
	}

	foreach($delete_users as $user){
		$sql="INSERT INTO users_perms ( uid, pid )VALUES( '".$user."', ".$row['id']." );";
		echo $sql,EOL;
		if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}
	}

}

$php_utils->message('Checking for email_actions table',true);//-----------------------------------------------------------------------------------------------
$sql="TRUNCATE `email_actions`;";
if(!$result = $mysqli->query($sql)){
	$php_utils->message('Query failed so creating the table');
	$sql="CREATE TABLE `email_actions` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`complete` VARCHAR(2) NULL DEFAULT 'N' COLLATE 'utf8_unicode_ci',
	`script` VARCHAR(1024) NOT NULL COLLATE 'utf8_unicode_ci',
	`args` VARCHAR(1024) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`uid` VARCHAR(100) NOT NULL COLLATE 'utf8_unicode_ci',
	`schedule` TIMESTAMP NULL DEFAULT NULL,
	`created_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
	`executed_time` TIMESTAMP NULL DEFAULT NULL,
	`completed_time` TIMESTAMP NULL DEFAULT NULL,
	`failed` INT(11) NULL DEFAULT '0' COMMENT 'Failed number of times',
	`failure` TEXT NULL COLLATE 'utf8_unicode_ci',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;";
	echo $sql,EOL;
	if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}
}


$php_utils->message('Checking for perm_rcv_pm_overdue roles perms',true);//-----------------------------------------------------------------------------------------------
$sql="SELECT id FROM users_perm_id WHERE perm = 'perm_rcv_pm_overdue';";
if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }
$row = $result->fetch_assoc();
$perm = $row['id'];

$sql="SELECT urp.rid, urp.pid, upi.perm
FROM users_role_perms AS urp
LEFT JOIN users_perm_id AS upi ON upi.id = urp.pid
WHERE urp.rid IN(1,2,3,4,5,6,7,8,9) AND upi.perm = 'perm_rcv_pm_overdue';";
if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }
$roles = array();
while($row = $result->fetch_assoc()){
	$roles[$row['rid']] = $row['pid'];
}
d($roles);
if(count($roles) < 9){
	$php_utils->message('Updating perm_rcv_pm_overdue roles perms',true);//-----------------------------------------------------------------------------------------------
	for ($i = 1; $i <= 9; $i++){
		if(!array_key_exists($i, $roles)){
			$sql="INSERT INTO users_role_perms (rid, pid) VALUES ($i, $perm);";
			echo $sql,EOL;
			if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}
		}
	}
}

$php_utils->message('Checking for perm_rcv_cont_met roles perms',true);//-----------------------------------------------------------------------------------------------
$sql="SELECT id FROM users_perm_id WHERE perm = 'perm_rcv_cont_met';";
if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }
$row = $result->fetch_assoc();
$perm = $row['id'];

$sql="SELECT urp.rid, urp.pid, upi.perm
FROM users_role_perms AS urp
LEFT JOIN users_perm_id AS upi ON upi.id = urp.pid
WHERE urp.rid IN(4,6,9) AND upi.perm = 'perm_rcv_cont_met';";
if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }
$roles = array();
while($row = $result->fetch_assoc()){
	$roles[$row['rid']] = $row['pid'];
}
d($roles);
if(count($roles) < 3){
	$php_utils->message('Updating perm_rcv_cont_met roles perms',true);//-----------------------------------------------------------------------------------------------
	foreach(array(4,6,9) as $i){
		if(!array_key_exists($i, $roles)){
			$sql="INSERT INTO users_role_perms (rid, pid) VALUES ($i, $perm);";
			echo $sql,EOL;
			if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}
		}
	}
}

$php_utils->message('Checking for perm_del_system roles perms',true);//-----------------------------------------------------------------------------------------------
$sql="SELECT id FROM users_perm_id WHERE perm = 'perm_del_system';";
if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }
$row = $result->fetch_assoc();
$perm = $row['id'];

$sql="SELECT urp.rid, urp.pid, upi.perm
FROM users_role_perms AS urp
LEFT JOIN users_perm_id AS upi ON upi.id = urp.pid
WHERE urp.rid IN(2,4,6,9) AND upi.perm = 'perm_del_system';";
if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }
$roles = array();
while($row = $result->fetch_assoc()){
	$roles[$row['rid']] = $row['pid'];
}
d($roles);
if(count($roles) < 4){
	$php_utils->message('Updating perm_del_system roles perms',true);//-----------------------------------------------------------------------------------------------
	foreach(array(2,4,6,9) as $i){
		if(!array_key_exists($i, $roles)){
			$sql="INSERT INTO users_role_perms (rid, pid) VALUES ($i, $perm);";
			echo $sql,EOL;
			if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}
		}
	}
}

$php_utils->message('Checking for updated systems_base_cont table');//-----------------------------------------------------------------------------------------------
$sql="SELECT date_installed FROM systems_base_cont LIMIT 1;";
if(!$result = $mysqli->query($sql)){
	$php_utils->message('Updating systems_base_cont table');//-----------------------------------------------------------------------------------------------
	$sql="ALTER TABLE `systems_base_cont`
	ADD COLUMN `location` VARCHAR(500) NULL DEFAULT NULL AFTER `contact_email`,
	ADD COLUMN `date_installed` VARCHAR(50) NULL DEFAULT NULL AFTER `warranty_end_date`,
	ADD COLUMN `acceptance_date` VARCHAR(50) NULL DEFAULT NULL AFTER `date_installed`,
	CHANGE COLUMN `created_by` `created_by` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci' AFTER `edited_date`,
	ADD COLUMN `archived_by` VARCHAR(100) NULL DEFAULT NULL AFTER `created_date`,
	ADD COLUMN `archived_date` VARCHAR(50) NULL DEFAULT NULL AFTER `archived_by`,
	ADD COLUMN `deleted_by` VARCHAR(100) NULL DEFAULT NULL AFTER `archived_date`,
	ADD COLUMN `deleted_date` VARCHAR(50) NULL DEFAULT NULL AFTER `deleted_by`;";
	echo $sql,EOL;
	if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}

	$php_utils->message('Moving Data');//-----------------------------------------------------------------------------------------------
	$sql="UPDATE systems_base_cont AS sbc LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id SET sbc.location = sb.location;";
	echo $sql,EOL;
	if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}

	$sql="UPDATE systems_base_cont AS sbc LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id SET sbc.date_installed = sb.date_installed;";
	echo $sql,EOL;
	if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}

	$sql="UPDATE systems_base_cont AS sbc LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id SET sbc.acceptance_date = sb.acceptance_date;";
	echo $sql,EOL;
	if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}

	$php_utils->message('Dropping old systems_base columns');//-----------------------------------------------------------------------------------------------
	$sql="ALTER TABLE `systems_base`
	DROP COLUMN `location`,
	DROP COLUMN `date_installed`,
	DROP COLUMN `acceptance_date`;";
	echo $sql,EOL;
	if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}

}


?>