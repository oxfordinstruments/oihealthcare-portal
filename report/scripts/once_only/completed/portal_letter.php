<?php
die("DONE");
$debug = false;
$send = false;
if(isset($_GET['debug'])){
	$debug = true;	
}

if(isset($_GET['send'])){
	$send = true;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

//load phpmailer
require_once($_SERVER['DOCUMENT_ROOT'].'/resources/PHPMailer/PHPMailerAutoload.php');
$mail = new PHPMailer;
$mail->IsSMTP();
$mail->Host = (string)$settings->email_host;
$mail->SMTPAuth = true;
$mail->Username = (string)$settings->email_support;
$mail->Password = (string)$settings->email_password;
$mail->SMTPSecure = 'tls';
$mail->From = (string)$settings->email_support;
$mail->FromName = 'Support '.$settings->short_name.' Portal';
$mail->AddReplyTo((string)$settings->email_support, 'Support '.$settings->short_name.' Portal');
$mail->AddCC((string)$settings->email_support);
$mail->IsHTML(true);
$mail->Subject = 'Oxford Instruments Healthcare Customer Portal';

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/smarty/libs/Smarty.class.php');
$smarty = new Smarty();
$smarty->setTemplateDir($_SERVER['DOCUMENT_ROOT'].'/report/scripts/once_only');
$smarty->force_compile = true;
$smarty->caching = false;


$emails = new SimpleXMLElement($_SERVER['DOCUMENT_ROOT'].'/temp/customer_emails.xml',null, true);
d($emails);

$x = 0;

foreach($emails->ROW as $email){
//	if($x >= 2){
//		break;	
//	}
	$x += 1;
	echo $x.'  Emailing: '.$email->name.'  ->  '.$email->email,EOL;
	 
	$smarty->assign('name',$email->name);
	$smarty->assign('company_name',$settings->company_name);
	$smarty->assign('copyright_date',$settings->copyright_date);
	$smarty->assign('email_pics',$settings->email_pics);
	$smarty->assign('oih_url',$settings->full_url);
	
	$mail->AddAddress(trim($email->email),$email->name);  // Add a recipient
	
	$mail->Body = $smarty->fetch('portal_letter.tpl');
	
	if($debug){
		$mail->ClearAddresses();
		$mail->AddAddress((string)$settings->email_support, 'Support '.$settings->short_name.' Portal');
		$mail->AddAddress('evotodi@gmail.com', 'Justin Davis');
	}
	
	if($send){
		if(!$mail->Send()) {
		   echo 'Email could not be sent.';
		   $email_result = "Email could not be sent!";
		   echo 'Mailer Error: ' . $mail->ErrorInfo;
		   $log->logerr($mail->ErrorInfo,1053,false,basename(__FILE__),__LINE__);
		   exit;
		}else{
			s('    Sent: '. implode('; ',array_keys($mail->getAllRecipientAddresses())));
			if($debug){
				echo "Subject: ",$mail->Subject,EOL;
				echo "Addresses: ",implode('; ',array_keys($mail->getAllRecipientAddresses())),EOL;
				echo $mail->Body,EOL,"<hr>",EOL;	
			}
			$mail->ClearAddresses();
		}
	}else{
		echo "Debug: Not sending email.",EOL;	
	}

	d($mail);
}
die('DONE');
?>