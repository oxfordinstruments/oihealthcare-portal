<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
	<tr>
		<td>
			<table cellpadding="10" cellspacing="0" border="0" width="100%">
				<tr align="center">
					<td><img src="{$email_pics}/default_header.jpg"  alt="Logo"></td>
				</tr>
			</table>
			<table cellpadding="10" cellspacing="0" border="0" width="100%">
				<tr>
					<td><p>&nbsp;</p>
						<p><strong>Dear {$name},</strong></p>
						<p><strong>On behalf of the entire Oxford Instruments Healthcare team, I would like to thank you for your continued service and dedication to our company.</strong></p>
						<p><strong>At Oxford Instruments Healthcare we understand that your organization has its own unique requirements for improving efficiency and the overall delivery of patient care.  To continue to provide added benefits to you, our valued customer, we are pleased to offer our unique Oxford Instruments Healthcare Customer Portal.</strong></p>
						<p><strong>Our Customer Portal is a private and secure website that enables you to keep track of all your systems, view any work being done, contact your service engineer and much more.</strong></p>
						<p><strong>With 24-hour access this easy to use personalized site works for you, you can:
						</strong>
						<ul>
							<li><strong>View and print service reports for each system</strong></li>
							<li><strong>Access relevant support documentation</strong></li>
							<li><strong>Provide password protected access to key users in your organization</strong></li>
							<li><strong>View system status around the clock</strong></li>
							<li><strong>Keep track of your service index</strong></li>
						</ul>
						<strong>
						</p>
						</strong>
						<p><strong>Register for access to our Customer Portal today!<br>
						<a href="https://oihealthcareportal.com/register/" >OiHealthcarePortal.com/register</a></strong></p>
						<p><strong>If you have any questions or problems accessing our Customer Portal, feel free to contact us at 1-888-673-5151, <a href="mailto:support@oihealthcareportal.com">support@oihealthcareportal.com</a>.</strong></p>
						<p><strong>We are grateful to have you as part of the Oxford Instruments Healthcare team and we appreciate your continued service. Again, we at Oxford Instruments Healthcare thank you and look forward to continuing to work with you in the future.</strong></p>
						<p><strong>Warmest Regards,</strong></p>
						<p><strong>Jeff Fall<br>
						President<br>
						Oxford Instruments Healthcare</strong></p>
						<p>&nbsp;</p>
					</td>
				</tr>
				<tr>
					<td><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
				</tr>
			</table>
			<table cellpadding="10" cellspacing="0" border="0" width="100%">
				<tr>
					<td><pre>{$company_name}
			64 Union Way
			Vacaville, CA 95687
			Tel:  +1 (707) 469-1320 
			Fax: +1 (707) 469-1318</pre></td>
					<td><pre>{$company_name}
			1027 SW 30Th Avenue
			Deerfield Beach, FL  33442
			Tel:  +1 (954) 596-4945 
			Fax: +1 (954) 596-4946</pre></td>
				</tr>
				<tr>
				<td colspan="2">This communication contains proprietary information and may be confidential.
			If you are not the intended recipient, the reading, copying, disclosure or other use of the contents of this e-mail is strictly prohibited and you are instructed to please delete this e-mail immediately.</td>
				</tr>
				<tr>
					<td colspan="2">Copyright &copy; {$copyright_date} {$company_name}, All rights reserved.</td>
				</tr>
				<tr>
					<td colspan="2"><img src="{$email_pics}/email_iso_9001.png" width="65" height="58" alt="ISO 9001">
					<img src="{$email_pics}/email_iso_13485.png" width="65" height="58" alt="ISO 13485">
					<img src="{$email_pics}/email_dotmed_logo.gif" width="128" height="58" alt="dotmed">
					<img src="{$email_pics}/email_iamers_logo.gif" width="54" height="58" alt="iamers"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>