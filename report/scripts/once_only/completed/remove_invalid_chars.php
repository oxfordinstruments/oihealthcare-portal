<?php
die("USED");

//Run every 1 hour m-f 8am EST to 8pm EST

if(isset($_SERVER['argv'][1]) and $_SERVER['argv'][1] == '--cron'){
	define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : PHP_EOL);
}else{
	define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
}

$settings = new SimpleXMLElement('/home/oiserv5/public_html/secure/settings.xml', null, true);
date_default_timezone_set($settings->TimeZone);

echo "Begin remove invalid chars ".date('m/d/Y H:i:s',time()),EOL,EOL;

require('/home/oiserv5/public_html/secure/mysqlInfo.php');
$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno) {die('There was an error running the query [' . $mysqli->connect_error . ']');}



$sqlout = array();
$re = "/[\'@\"\$\\\#\+\?]/";
$patterns = array();
$patterns[1] = "/\'/";
$patterns[2] = '/@/';
$patterns[3] = '/\"/';
$patterns[4] = '/\$/';
$patterns[5] = '/\\\/';
$patterns[6] = '/\#/';
$patterns[7] = '/\+/';
$patterns[8] = '/\?/';

$replacements = array();
$replacements[1] = '`';
$replacements[2] = ' at ';
$replacements[3] = '';
$replacements[4] = '';
$replacements[5] = '';
$replacements[6] = '';
$replacements[7] = ' plus ';
$replacements[8] = '';



$table = "sites";
$sql="SELECT * FROM $table ORDER BY id ASC;";
$result = $mysqli->query($sql);
while($row = $result->fetch_assoc()){
	//echo "Checking: ",$row['index'],"  ",$row['site_nickname'],EOL;
	$update = false;
	$sqltmp = "UPDATE $table SET ";
	
	$site_name = "";
	if(preg_match($re,$row['site_name'])){
		$site_name = preg_replace($patterns,$replacements,$row['site_name']);	
		$sqltmp .= "`site_name` = '$site_name', ";
		$update = true;
	}	
	
	$site_nickname = "";
	if(preg_match($re,$row['site_nickname'])){
		$site_nickname = preg_replace($patterns,$replacements,$row['site_nickname']);	
		$sqltmp .= "`site_nickname` = '$site_nickname', ";
		$update = true;
	}
	
	$address = "";
	if(preg_match($re,$row['address'])){
		$address = preg_replace($patterns,$replacements,$row['address']);	
		$sqltmp .= "`address` = '$address', ";
		$update = true;
	}
	
	$city = "";
	if(preg_match($re,$row['city'])){
		$city = preg_replace($patterns,$replacements,$row['city']);	
		$sqltmp .= "`city` = '$city', ";
		$update = true;
	}
	
	$contact_name = "";
	if(preg_match($re,$row['city'])){
		$contact_name = preg_replace($patterns,$replacements,$row['contact_name']);	
		$sqltmp .= "`contact_name` = '$contact_name', ";
		$update = true;
	}
	
	$contact_title = "";
	if(preg_match($re,$row['city'])){
		$contact_title = preg_replace($patterns,$replacements,$row['contact_title']);	
		$sqltmp .= "`contact_title` = '$contact_title', ";
		$update = true;
	}
	
	$mgr_name = "";
	if(preg_match($re,$row['city'])){
		$mgr_name = preg_replace($patterns,$replacements,$row['mgr_name']);	
		$sqltmp .= "`mgr_name` = '$mgr_name', ";
		$update = true;
	}
	
	$mgr_title = "";
	if(preg_match($re,$row['city'])){
		$mgr_title = preg_replace($patterns,$replacements,$row['mgr_title']);	
		$sqltmp .= "`mgr_title` = '$mgr_title', ";
		$update = true;
	}
	
	$bill_name = "";
	if(preg_match($re,$row['bill_name'])){
		$bill_name = preg_replace($patterns,$replacements,$row['bill_name']);	
		$sqltmp .= "`bill_name` = '$bill_name', ";
		$update = true;
	}
	
	$bill_address = "";
	if(preg_match($re,$row['bill_address'])){
		$bill_address = preg_replace($patterns,$replacements,$row['bill_address']);	
		$sqltmp .= "`bill_address` = '$bill_address', ";
		$update = true;
	}
	
	$equipment_location = "";
	if(preg_match($re,$row['equipment_location'])){
		$equipment_location = preg_replace($patterns,$replacements,$row['equipment_location']);	
		$sqltmp .= "`equipment_location` = '$equipment_location', ";
		$update = true;
	}
	
	$contract_terms = "";
	if(preg_match($re,$row['contract_terms'])){
		$contract_terms = preg_replace($patterns,$replacements,$row['contract_terms']);	
		$sqltmp .= "`contract_terms` = '$contract_terms', ";
		$update = true;
	}
	
	$credit_hold_notes = "";
	if(preg_match($re,$row['credit_hold_notes'])){
		$credit_hold_notes = preg_replace($patterns,$replacements,$row['credit_hold_notes']);	
		$sqltmp .= "`credit_hold_notes` = '$credit_hold_notes', ";
		$update = true;
	}
	
	$notes = "";
	if(preg_match($re,$row['notes'])){
		$notes = preg_replace($patterns,$replacements,$row['notes']);	
		$sqltmp .= "`notes` = '$notes', ";
		$update = true;
	}
	
	if($update){
		$sqltmp = substr($sqltmp,0,-2);
		$sqltmp .= " WHERE id = ".$row['index'].";";
		$sqlout[$row['index']] = $sqltmp;
	}
	
}
echo EOL,"$table table:<pre>",print_r($sqlout),"</pre>",EOL;
foreach($sqlout as $sql){
	if(!$result = $mysqli->query($sql)){
		echo $mysqli->error,EOL;	
	}
}


?>