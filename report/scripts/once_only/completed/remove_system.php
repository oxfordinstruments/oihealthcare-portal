<?php

//

ob_start();//Start output buffering

$debug = false;
if(isset($_GET['debug'])){
	$debug = true;	
}

$from_system_id = NULL;
if(!isset($_GET['from_system_id'])){
	die('GET from_system_id not set');	
}else{
	$from_system_id = strtoupper($_GET['from_system_id']);
}

$to_system_id = NULL;
if(!isset($_GET['to_system_id'])){
	die('GET to_system_id not set');	
}else{
	$to_system_id = strtoupper($_GET['to_system_id']);
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/define_inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

require($_SERVER['DOCUMENT_ROOT'].'/mysqlInfo.php');
$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno) {die('There was an error running the query [' . $mysqli->connect_error . ']');}

echo "<h2>Checking System IDs</h2>";
$sql="SELECT system_id FROM systems WHERE system_id = '$from_system_id';";
if(!$reslut = $mysqli->query($sql)){
	die($mysqli->error);	
}
if($reslut->num_rows != 1){
	die("From System ID not valid");	
}else{
	echo "From System ID valid",EOL;	
}

$sql="SELECT system_id FROM systems WHERE system_id = '$to_system_id';";
if(!$reslut = $mysqli->query($sql)){
	die($mysqli->error);	
}
if($reslut->num_rows != 1){
	die("To System ID not valid");	
}else{
	echo "To System ID valid",EOL;	
}


echo "<h2>Getting Unique IDs</h2>";
echo "From System ID: ",$from_system_id,EOL;
$sql="SELECT unique_id, facility_unique_id FROM systems WHERE system_id = '$from_system_id';";
if(!$reslut = $mysqli->query($sql)){
	die($mysqli->error);	
}
$row = $reslut->fetch_assoc();
$from_system_unique_id = $row['unique_id'];
echo "From System Unique ID: ",$from_system_unique_id,EOL;
$from_facility_unique_id = $row['facility_unique_id'];
echo "From Facility Unique ID: ",$from_facility_unique_id,EOL;

$sql="SELECT customer_unique_id FROM facilities WHERE unique_id = '$from_facility_unique_id';";
if(!$reslut = $mysqli->query($sql)){
	die($mysqli->error);	
}
$row = $reslut->fetch_assoc();
$from_customer_unique_id = $row['customer_unique_id'];
echo "From Customer Unique ID: ",$from_customer_unique_id,EOL;

echo EOL;
echo "To System ID: ",$to_system_id,EOL;
$sql="SELECT unique_id, facility_unique_id, nickname FROM systems WHERE system_id = '$to_system_id';";
if(!$reslut = $mysqli->query($sql)){
	die($mysqli->error);	
}
$row = $reslut->fetch_assoc();
$to_system_unique_id = $row['unique_id'];
echo "To System Unique ID: ",$to_system_unique_id,EOL;
$to_system_nickname = $row['nickname'];
echo "To System Nickname: ",$to_system_nickname,EOL;
$to_facility_unique_id = $row['facility_unique_id'];
echo "To Facility Unique ID: ",$to_facility_unique_id,EOL;

$sql="SELECT customer_unique_id FROM facilities WHERE unique_id = '$to_facility_unique_id';";
if(!$reslut = $mysqli->query($sql)){
	die($mysqli->error);	
}
$row = $reslut->fetch_assoc();
$to_customer_unique_id = $row['customer_unique_id'];
echo "To Customer Unique ID: ",$to_customer_unique_id,EOL;

echo "<h2>Getting From System Table Data</h2>";
echo "Table: systems",EOL;
$sql="SELECT * FROM systems WHERE unique_id = '$from_system_unique_id';";
if(!$reslut = $mysqli->query($sql)){
	die($mysqli->error);	
}
$systems_tbl = array();
while($row = $reslut->fetch_assoc()){
	array_push($systems_tbl,$row);	
}
d($systems_tbl);

echo "Table: facilities",EOL;
$sql="SELECT * FROM facilities WHERE unique_id = '$to_facility_unique_id';";
if(!$reslut = $mysqli->query($sql)){
	die($mysqli->error);	
}
$facilities_tbl = array();
while($row = $reslut->fetch_assoc()){
	array_push($facilities_tbl,$row);	
}
d($facilities_tbl);

echo "Table: customers",EOL;
$sql="SELECT * FROM customers WHERE unique_id = '$to_customer_unique_id';";
if(!$reslut = $mysqli->query($sql)){
	die($mysqli->error);	
}
$customers_tbl = array();
while($row = $reslut->fetch_assoc()){
	array_push($customers_tbl,$row);	
}
d($customers_tbl);

echo "Table: systems_assigned_pri",EOL;
$sql="SELECT * FROM systems_assigned_pri WHERE system_unique_id = '$from_system_unique_id';";
if(!$reslut = $mysqli->query($sql)){
	die($mysqli->error);	
}
$systems_assigned_pri_tbl = array();
while($row = $reslut->fetch_assoc()){
	array_push($systems_assigned_pri_tbl,$row);	
}
d($systems_assigned_pri_tbl);

echo "Table: systems_assigned_sec",EOL;
$sql="SELECT * FROM systems_assigned_sec WHERE system_unique_id = '$from_system_unique_id';";
if(!$reslut = $mysqli->query($sql)){
	die($mysqli->error);	
}
$systems_assigned_sec_tbl = array();
while($row = $reslut->fetch_assoc()){
	array_push($systems_assigned_sec_tbl,$row);	
}
d($systems_assigned_sec_tbl);

echo "Table: systems_assigned_customer",EOL;
$sql="SELECT * FROM systems_assigned_customer WHERE system_unique_id = '$from_system_unique_id';";
if(!$reslut = $mysqli->query($sql)){
	die($mysqli->error);	
}
$systems_assigned_customer_tbl = array();
while($row = $reslut->fetch_assoc()){
	array_push($systems_assigned_customer_tbl,$row);	
}
d($systems_assigned_customer_tbl);

echo "Table: systems_face_sheet",EOL;
$sql="SELECT * FROM systems_face_sheet WHERE system_unique_id = '$from_system_unique_id';";
if(!$reslut = $mysqli->query($sql)){
	die($mysqli->error);	
}
$systems_face_sheet_tbl = array();
while($row = $reslut->fetch_assoc()){
	array_push($systems_face_sheet_tbl,$row);	
}
d($systems_face_sheet_tbl);

echo "Table: systems_reports",EOL;
$sql="SELECT * FROM systems_reports WHERE system_unique_id = '$from_system_unique_id';";
if(!$reslut = $mysqli->query($sql)){
	die($mysqli->error);	
}
$systems_reports_tbl_count = $reslut->num_rows;
s($systems_reports_tbl_count);
$systems_reports_tbl = array();
while($row = $reslut->fetch_assoc()){
	array_push($systems_reports_tbl,$row);	
}
d($systems_reports_tbl);

echo "Table: systems_requests",EOL;
$sql="SELECT * FROM systems_requests WHERE system_unique_id = '$from_system_unique_id';";
if(!$reslut = $mysqli->query($sql)){
	die($mysqli->error);	
}
$systems_requests_tbl_count = $reslut->num_rows;
s($systems_requests_tbl_count);
$systems_requests_tbl = array();
while($row = $reslut->fetch_assoc()){
	array_push($systems_requests_tbl,$row);	
}
d($systems_requests_tbl);

echo "Table: systems_evening_updates",EOL;
$sql="SELECT * FROM systems_evening_updates WHERE system_unique_id = '$from_system_unique_id';";
if(!$reslut = $mysqli->query($sql)){
	die($mysqli->error);	
}
$systems_evening_updates_tbl_count = $reslut->num_rows;
s($systems_evening_updates_tbl_count);
$systems_evening_updates_tbl = array();
while($row = $reslut->fetch_assoc()){
	array_push($systems_evening_updates_tbl,$row);	
}
d($systems_evening_updates_tbl);

echo "<h2>Checking From System for existing data</h2>";

if($systems_reports_tbl_count + $systems_requests_tbl_count + $systems_evening_updates_tbl_count > 0){
	echo "Existing data found",EOL;	
	echo "<h2>Merging data into ",strtoupper($to_system_id),"</h2>";
	
	$sql="UPDATE systems_reports SET 
	system_id = '$to_system_id',
	system_unique_id = '$to_system_unique_id',
	facility_unique_id = '$to_facility_unique_id',
	system_nickname = '$to_system_nickname'
	WHERE system_unique_id = '$from_system_unique_id';";
	if(!$reslut = $mysqli->query($sql)){
		die($mysqli->error);	
	}
	
	$sql="UPDATE systems_requests SET 
	system_id = '$to_system_id',
	system_unique_id = '$to_system_unique_id',
	facility_unique_id = '$to_facility_unique_id',
	system_nickname = '$to_system_nickname'
	WHERE system_unique_id = '$from_system_unique_id';";
	if(!$reslut = $mysqli->query($sql)){
		die($mysqli->error);	
	}
	
	$sql="UPDATE systems_evening_updates SET 
	system_unique_id = '$to_system_unique_id'
	WHERE system_unique_id = '$from_system_unique_id';";
	if(!$reslut = $mysqli->query($sql)){
		die($mysqli->error);	
	}
	
	echo "Data has been merged",EOL;
}else{
	echo "No data found",EOL;	
}

echo "<h2>Checking To/From Facility Data</h2>";
$remove_facility = false;
if($to_facility_unique_id != $from_facility_unique_id){
	$sql="SELECT system_id FROM systems WHERE facility_unique_id = '$from_facility_unique_id';";
	if(!$reslut = $mysqli->query($sql)){
		die($mysqli->error);	
	}
	$from_facilities_systems_count = $reslut->num_rows;
	if($from_facilities_systems_count == 1){
		echo "Removing Facility: ",$from_facility_unique_id,EOL;
		$remove_facility = true;
	}else{
		echo "Facility Contains other systems so will not be removed",EOL;	
	}
}else{
	echo "Facilities are the same",EOL;	
}

echo "<h2>Checking To/From Customer Data</h2>";
$remove_customer = false;
if($to_customer_unique_id != $from_customer_unique_id){
	$sql="SELECT facility_id FROM facilities WHERE customer_unique_id = '$from_customer_unique_id';";
	if(!$reslut = $mysqli->query($sql)){
		die($mysqli->error);	
	}
	$from_customers_facilities_count = $reslut->num_rows;
	if($from_customers_facilities_count == 1){
		echo "Removing Customer: ",$from_customer_unique_id,EOL;
		$remove_customer = true;
	}else{
		echo "Customer Contains other facilities so will not be removed",EOL;	
	}
}else{
	echo "Customers are the same",EOL;	
}

if($remove_facility){
	$sql="DELETE FROM facilities WHERE unique_id = '$from_facility_unique_id';";
	if(!$reslut = $mysqli->query($sql)){
		die($mysqli->error);	
	}
}

if($remove_customer){
	$sql="DELETE FROM customers WHERE unique_id = '$from_customer_unique_id';";
	if(!$reslut = $mysqli->query($sql)){
		die($mysqli->error);	
	}
}

echo "<h2>Removing From System</h2>";
$sql="DELETE FROM systems WHERE unique_id = '$from_system_unique_id'";
if(!$reslut = $mysqli->query($sql)){
	die($mysqli->error);	
}else{
	echo "From System Removed",EOL;	
}

echo "<h2>Checking From Customer Logins</h2>";
$sql="SELECT * FROM systems_assigned_customer WHERE system_unique_id = '$from_system_unique_id';";
if(!$reslut = $mysqli->query($sql)){
	die($mysqli->error);	
}
if($reslut->num_rows > 0){
	$from_users = array();
	while($row = $reslut->fetch_assoc()){
		array_push($from_users, $row['uid']);
	}
	d($from_users);
	foreach($from_users as $user){
		$sql="UPDATE users SET active = 'N' WHERE uid = '$user'";	
		if(!$reslut = $mysqli->query($sql)){
			die($mysqli->error);	
		}
		echo "De-Activated User: ",$user,EOL;
	}
}

echo "<h2>Removing Engineer/Customer Assignments</h2>";
$sql="DELETE FROM systems_assigned_pri WHERE system_unique_id = '$from_system_unique_id'";
if(!$reslut = $mysqli->query($sql)){
	die($mysqli->error);	
}else{
	echo "Engineer Primary Removed",EOL;	
}
$sql="DELETE FROM systems_assigned_sec WHERE system_unique_id = '$from_system_unique_id'";
if(!$reslut = $mysqli->query($sql)){
	die($mysqli->error);	
}else{
	echo "Engineer Secondary Removed",EOL;	
}
$sql="DELETE FROM systems_assigned_customer WHERE system_unique_id = '$from_system_unique_id'";
if(!$reslut = $mysqli->query($sql)){
	die($mysqli->error);	
}else{
	echo "Customer Primary Removed",EOL;	
}

echo "<h2>Done</h2>";
?>

	
	
	
	
	
	
	
	
	
