<?php
die("DONE");
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 3/20/2017
 * Time: 11:21 PM
 */

//
// Error reporting
//
//error_reporting(E_ALL);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
ini_set('display_errors', 'On');

$debug = true;
$insert = true;

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/php_utils.php');
$php_utils =  new phpUtils();

define('EOL', '<br>');
$errors = false;


if (version_compare(PHP_VERSION, '5.4.0', '>=')) {
	ob_start(null, 0, PHP_OUTPUT_HANDLER_STDFLAGS ^
		PHP_OUTPUT_HANDLER_REMOVABLE);
} else {
	ob_start(null, 0, false);
}



$php_utils->message('Begin');//-----------------------------------------------------------------------------------------------

$php_utils->message('Checking if table systems_service_journal exists');//-----------------------------------------------------------------------------------------------

$sql = "SELECT * FROM systems_service_journal LIMIT 1;";
$systems_service_journal_exists = true;
if(!$result = $mysqli->query($sql)){
	$systems_service_journal_exists = false;
}
d($systems_service_journal_exists);

if(!$systems_service_journal_exists){
	$php_utils->message('Creating table systems_service_journal');//-----------------------------------------------------------------------------------------------
	$sql = "CREATE TABLE `systems_service_journal` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`note` TEXT NULL COLLATE 'utf8_unicode_ci',
	`uid` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`system_status` INT(11) NULL DEFAULT NULL,
	`date` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`request_report_unique_id` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`from_email` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	PRIMARY KEY (`id`),
	INDEX `uid` (`uid`),
	CONSTRAINT `FK_systems_service_journal_users` FOREIGN KEY (`uid`) REFERENCES `users` (`uid`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB;";
	if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}
}

$php_utils->message('Checking if systems_reports_notes exists');//-----------------------------------------------------------------------------------------------
$systems_reports_notes = array();
$systems_reports_notes_exists = true;
$sql = "SELECT * FROM systems_reports_notes;";
if(!$result = $mysqli->query($sql)){
	$systems_reports_notes_exists = false;
}else{
	$php_utils->message('Getting data from systems_reports_notes');//-----------------------------------------------------------------------------------------------
	while($row = $result->fetch_assoc()){
		$systems_reports_notes[$row['id']] = $row;
	}
	d($systems_reports_notes);

	$php_utils->message('Copying data from systems_reports_notes into systems_service_journal');//-----------------------------------------------------------------------------------------------
	foreach($systems_reports_notes as $key=>$note){
		$sql="INSERT INTO systems_service_journal (`note`, `uid`, `date`, `request_report_unique_id`) VALUES (?,?,?,?);";
		if($stmt = $mysqli->prepare($sql)){
			$stmt->bind_param('ssss', $note['note'], $note['uid'], $note['date'], $note['report_unique_id']);
			$stmt->execute();
			$stmt->close();
		}else{
			die('There was an error running the query [' . $mysqli->error . ']');
		}
	}
	unset($stmt);

	$php_utils->message('Dropping systems_reports_notes');//-----------------------------------------------------------------------------------------------
	$sql="DROP TABLE systems_reports_notes;";
	if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }
}


$php_utils->message('Checking if systems_requests_notes exists');//-----------------------------------------------------------------------------------------------
$systems_requests_notes = array();
$systems_requests_notes_exists = true;
$sql = "SELECT * FROM systems_requests_notes;";
if(!$result = $mysqli->query($sql)){
	$systems_requests_notes_exists = false;
}else{
	$php_utils->message('Getting data from systems_requests_notes');//-----------------------------------------------------------------------------------------------
	while($row = $result->fetch_assoc()){
		$systems_requests_notes[$row['id']] = $row;
	}
	d($systems_requests_notes);

	$php_utils->message('Copying data from systems_requests_notes into systems_service_journal');//-----------------------------------------------------------------------------------------------
	foreach($systems_requests_notes as $key=>$note){
		$sql="INSERT INTO systems_service_journal (`note`, `uid`, `date`, `request_report_unique_id`) VALUES (?,?,?,?);";
		if($stmt = $mysqli->prepare($sql)){
			$stmt->bind_param('ssss', $note['note'], $note['uid'], $note['date'], $note['request_unique_id']);
			$stmt->execute();
			$stmt->close();
		}else{
			die('There was an error running the query [' . $mysqli->error . ']');
		}
	}

	unset($stmt);

	$php_utils->message('Dropping systems_requests_notes');//-----------------------------------------------------------------------------------------------
	$sql="DROP TABLE systems_requests_notes;";
	if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }
}


$php_utils->message('Checking if systems_requests_notes exists');//-----------------------------------------------------------------------------------------------
$systems_status_updates = array();
$systems_status_updates_exists = true;
$invalid = array();
$sql = "SELECT * FROM systems_status_updates;";
if(!$result = $mysqli->query($sql)){
	$systems_status_updates_exists = false;
}else{
	$php_utils->message('Getting data from systems_status_updates');//-----------------------------------------------------------------------------------------------
	while($row = $result->fetch_assoc()){
		$systems_status_updates[$row['id']] = $row;
	}
	d($systems_status_updates);


	$php_utils->message('Copying data from systems_status_updates into systems_service_journal');//-----------------------------------------------------------------------------------------------
	foreach($systems_status_updates as $key=>$note){
		$sql="INSERT INTO systems_service_journal (`note`, `uid`, `date`, `request_report_unique_id`, `from_email`) VALUES (?,?,?,?,?);";
		if($note['report_unique_id'] != ''){
			if($stmt = $mysqli->prepare($sql)){
				$stmt->bind_param('sssss', $note['status'], $note['uid'], $note['date'], $note['report_unique_id'], $note['from_email']);
				$stmt->execute();
				$stmt->close();
			}else{
				die('There was an error running the query [' . $mysqli->error . ']');
			}
		}else{
			array_push($invalid, $note['id']);
			$systems_status_updates[$key]['invalid'] = true;
		}
	}

	unset($stmt);

	if(count($invalid) > 0){
		echo "ERROR:  System Status IDs: ".implode(', ', $invalid)." missing report unique id",EOL;
	}
	d($systems_status_updates);

	if(count($invalid) > 0){
		$php_utils->message('Removing copied rows from systems_status_updates');//-----------------------------------------------------------------------------------------------
		$sql="DELETE FROM systems_status_updates WHERE id NOT IN (".implode(',',$invalid).");";
		if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }
		echo "MANUAL DATA EDIT OF systems_status_updates REQUIRED",EOL;
	}else{
		$php_utils->message('Dropping systems_status_updates');//-----------------------------------------------------------------------------------------------
		$sql="DROP TABLE systems_status_updates;";
		if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }
	}

}

$php_utils->message('Checking if systems_status abv column exists');//-----------------------------------------------------------------------------------------------
$sql="SELECT * FROM systems_status WHERE abv = 'up';";
if(!$result = $mysqli->query($sql)){
	$php_utils->message('Updating table systems_status schema');//-----------------------------------------------------------------------------------------------
	$sql="ALTER TABLE `systems_status`
	ADD COLUMN `abv` VARCHAR(50) NULL DEFAULT NULL AFTER `priority`;";
	if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}
	$sql_arr=array(
		"UPDATE systems_status SET abv = 'Up     ' WHERE id = 1;",
		"UPDATE systems_status SET abv = 'Down   ' WHERE id = 2;",
		"UPDATE systems_status SET abv = 'Limited' WHERE id = 3;",
		"UPDATE systems_status SET abv = 'Add Sup' WHERE id = 4;",
		"UPDATE systems_status SET abv = 'Up-Unav' WHERE id = 5;",
		"UPDATE systems_status SET abv = 'Dn-Unav' WHERE id = 16;",
		"UPDATE systems_status SET abv = 'Unknown' WHERE id = 17;"
	);
	foreach($sql_arr as $sql){
		if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}
	}
}













//UNDECIDED!!!
/*
$php_utils->message('Checking if misc_locks exists');//-----------------------------------------------------------------------------------------------
$sql="SELECT * FROM `misc_locks` LIMIT 1;";
if(!$result = $mysqli->query($sql)){
	$php_utils->message('Creating misc_locks table');//-----------------------------------------------------------------------------------------------
	$sql="CREATE TABLE `misc_locks` (
		`id` INT NOT NULL AUTO_INCREMENT,
		`table` VARCHAR(50) NULL,
		`record` VARCHAR(50) NULL,
		`session_id` VARCHAR(50) NULL,
		`uid` VARCHAR(100) NULL,
		`timestamp` VARCHAR(50) NULL,
		PRIMARY KEY (`id`)
	)
	COLLATE='utf8_unicode_ci'
	ENGINE=InnoDB;";
	if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}
}

$php_utils->message('Checking if systems_reports lock column exists');//-----------------------------------------------------------------------------------------------
$sql="SELECT * FROM systems_reports LIMIT 1;";
if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }
$systems_reports_lock = false;
while($row = $result->fetch_assoc()){
	if($row['Field'] == 'lock'){
		$sys
	}
}
*/

?>