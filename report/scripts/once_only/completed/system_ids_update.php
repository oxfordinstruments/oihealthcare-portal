<!DOCTYPE html>
<html>
<head>
<script type="text/javascript">
function getDocHeight() {
    var D = document;
    return Math.max(
        Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
        Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
        Math.max(D.body.clientHeight, D.documentElement.clientHeight)
    );
}

document.documentElement.scrollTop = getDocHeight();

var scroller = setInterval(function() {  
    document.documentElement.scrollTop = getDocHeight();
}, 100 /*update intervall in ms*/);

</script>
</head>
<body>
<?php
die("DONE");

/*
2015-8-14 Evotodi
This script removes the system id field from the database 
and changes the system id to the new oxford format (CTxxxx MRxxxx NMxxxx PCTxxxx TRxxx WSxxx).

It must be run once the updated pages have been uploaded.

This update will make changing system ids easier in the future.
*/

ob_start();//Start output buffering

$debug = false;
if(isset($_GET['debug'])){
	$debug = true;	
}

$execute = false; //Execute sql update/insert/delete
if(isset($_GET['execute'])){
	$execute = true;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/define_inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

// Turn off output buffering
ini_set('output_buffering', 'off');
// Turn off PHP output compression
ini_set('zlib.output_compression', false);
         
//Flush (send) the output buffer and turn off output buffering
//ob_end_flush();
while (@ob_end_flush());
         
// Implicitly flush the buffer(s)
ini_set('implicit_flush', true);
ob_implicit_flush(true);

require($_SERVER['DOCUMENT_ROOT'].'/mysqlInfo.php');
$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno) {die('There was an error running the query [' . $mysqli->connect_error . ']');}

$bad_ids = false;

//
//Make the required changes to the database first
//

echo "<h2>Making required changes to db</h2>";
$sql = <<<SQL
-- ALTER TABLE `iso_fbc` ADD COLUMN `system_unique_id` VARCHAR(100) NULL DEFAULT NULL AFTER `system_id`;
UPDATE iso_fbc AS z LEFT JOIN systems AS s ON s.system_id = z.system_id SET z.system_unique_id = s.unique_id;
-- ALTER TABLE `systems_assigned_customer` ADD COLUMN `system_unique_id` VARCHAR(100) NULL DEFAULT NULL AFTER `system_id`;
UPDATE systems_assigned_customer AS z LEFT JOIN systems AS s ON s.system_id = z.system_id SET z.system_unique_id = s.unique_id;
delete from systems_assigned_customer where system_unique_id is null;
-- ALTER TABLE `systems_assigned_pri` ADD COLUMN `system_unique_id` VARCHAR(100) NULL DEFAULT NULL AFTER `system_id`;
UPDATE systems_assigned_pri AS z LEFT JOIN systems AS s ON s.system_id = z.system_id SET z.system_unique_id = s.unique_id;
delete from systems_assigned_pri where system_unique_id is null;
-- ALTER TABLE `systems_assigned_sec` ADD COLUMN `system_unique_id` VARCHAR(100) NULL DEFAULT NULL AFTER `system_id`;
UPDATE systems_assigned_sec AS z LEFT JOIN systems AS s ON s.system_id = z.system_id SET z.system_unique_id = s.unique_id;
delete from systems_assigned_sec where system_unique_id is null;
-- ALTER TABLE `systems_evening_updates` ADD COLUMN `system_unique_id` VARCHAR(100) NULL DEFAULT NULL AFTER `system_id`;
UPDATE systems_evening_updates AS z LEFT JOIN systems AS s ON s.system_id = z.system_id SET z.system_unique_id = s.unique_id;
DELETE FROM systems_evening_updates WHERE system_unique_id IS NULL;
ALTER TABLE `iso_fbc` DROP COLUMN `system_id`;
ALTER TABLE `nps_codes` DROP COLUMN `system_id`;
ALTER TABLE `systems_assigned_customer` DROP COLUMN `system_id`;
ALTER TABLE `systems_assigned_pri` DROP COLUMN `system_id`;
ALTER TABLE `systems_assigned_sec` DROP COLUMN `system_id`;
ALTER TABLE `systems_evening_updates` DROP COLUMN `system_id`;
ALTER TABLE `systems_face_sheet` DROP COLUMN `system_id`;
ALTER TABLE `systems_hours` DROP COLUMN `system_id`;
ALTER TABLE `systems_mri_readings` DROP COLUMN `system_id`;
ALTER TABLE `systems_parts` DROP COLUMN `system_id`;
TRUNCATE `systems_assigned_history`;
SQL;

$multi_sql = explode(";",$sql);
d($multi_sql);
foreach($multi_sql as $msql){
	if(strpos($msql,"--") !== false){
		continue;
	}
	if(strlen($msql) < 1){
		continue;
	}
	$sql = trim($msql).";";
	echo "Execute: ".$sql,EOL;
	if($execute){
		if(!$mysqli->query($sql)){
			echo "&emsp;&emsp;ERROR: ".$mysqli->error,EOL;
		}
	}
}

//
//Get CT system ids and unique ids and make new ids
//

echo "<h2>Creating new CT system ids</h2>";
$sql="SELECT s.id, s.system_id, s.unique_id, st.`type`
FROM systems AS s
LEFT JOIN systems_types AS st ON st.id = s.system_type
WHERE st.modality = 1 AND LENGTH(s.system_id) < 7;";
d($sql);
if(!$reslut = $mysqli->query($sql)){
	die($mysqli->error);	
}

$ct_systems = array();
$ct_systems_bad = array();
while($row = $reslut->fetch_assoc()){
	$new_id = '';
	$add = false;
	if(is_numeric($row['system_id'])){
		$new_id = 'CT'.$row['system_id'];
		$add = true;
	}elseif(strpos(strtolower($row['system_id']),'a') !== false){
		$new_id = 'CT'.$row['system_id'];		
		$add = true;
	}elseif(strpos(strtolower($row['system_id']),'p') !== false){
		$new_id = 'CT'.$row['system_id'];
		$add = true;
	}
	if($add){
		$ct_systems[$row['system_id']] = array('unique_id'=>$row['unique_id'], 'new_id'=>strtoupper($new_id));
	}else{
		if(strpos(strtolower($row['system_id']),'su') !== false){
			continue;	
		}
		if(strpos(strtolower($row['system_id']),'ct') !== false){
			continue;
		}
		$ct_systems_bad[$row['system_id']] = array('unique_id'=>$row['unique_id']);
		$bad_ids = true;
	}
}
d($ct_systems);
d($ct_systems_bad);

//
//Get MR system ids and unique ids and make new ids
//

echo "<h2>Creating new MR system ids</h2>";
$sql="SELECT s.id, s.system_id, s.unique_id, st.`type`
FROM systems AS s
LEFT JOIN systems_types AS st ON st.id = s.system_type
WHERE st.modality = 2 AND LENGTH(s.system_id) < 7;";
d($sql);
if(!$reslut = $mysqli->query($sql)){
	die($mysqli->error);	
}

$mr_systems = array();
$mr_systems_bad = array();
while($row = $reslut->fetch_assoc()){
	$new_id = '';
	$add = false;
	if(is_numeric($row['system_id'])){
		$new_id = 'MR'.$row['system_id'];
		$add = true;
	}elseif(strpos(strtolower($row['system_id']),'a') !== false){
		$new_id = 'MR'.$row['system_id'];		
		$add = true;
	}elseif(strpos(strtolower($row['system_id']),'p') !== false){
		$new_id = 'MR'.$row['system_id'];
		$add = true;
	}
	if($add){
		$mr_systems[$row['system_id']] = array('unique_id'=>$row['unique_id'], 'new_id'=>strtoupper($new_id));
	}else{
		if(strpos(strtolower($row['system_id']),'su') !== false){
			continue;	
		}
		if(strpos(strtolower($row['system_id']),'mr') !== false){
			continue;
		}
		$mr_systems_bad[$row['system_id']] = array('unique_id'=>$row['unique_id']);	
		$bad_ids = true;
	}
}
d($mr_systems);
d($mr_systems_bad);

//
//Combine CT and MR arrays
//

echo "<h2>Making array of new system ids and old</h2>";
$systems = $ct_systems + $mr_systems;
d($systems);

//
//Update systems table
//

echo "<h2>Updating Systems Table</h2>";
if($bad_ids){
	echo "<h1>Cannot update due to bad ids</h1>",EOL;	
}else{
	foreach($systems as $key=>$value){
		$sql="UPDATE systems SET system_id = '".$value['new_id']."' WHERE unique_id = '".$value['unique_id']."';";
		echo "Execute: ".$sql,EOL;
		if($execute){
			if(!$reslut = $mysqli->query($sql)){
				die($mysqli->error);	
			}
		}
	}
}

//
//Update open service request without report started
//

echo "<h2>Updating Requests Table</h2>";
if($bad_ids){
	echo "<h1>Cannot update due to bad ids</h1>",EOL;	
}else{
	$requests = array();
	$sql="SELECT r.request_num, r.system_id, r.system_unique_id
	FROM systems_requests AS r;";
	if(!$reslut = $mysqli->query($sql)){
		die($mysqli->error);	
	}
	while($rowRequest = $reslut->fetch_assoc()){
		$requests[$rowRequest['request_num']] = array('system_unique_id'=>$rowRequest['system_unique_id'], 'system_id'=>$rowRequest['system_id']);
	}
	$bad_requests = array();
	foreach($requests as $key=>$value){
		if($systems[$value['system_id']]['new_id'] == ''){
			array_push($bad_requests,$key);
		}
		$sql="UPDATE systems_requests SET system_id = '".$systems[$value['system_id']]['new_id']."' WHERE request_num = $key";	
		echo "Execute: ".$sql,EOL;
		if(empty($bad_requests)){
			if($execute){
				if(!$reslut = $mysqli->query($sql)){
					die($mysqli->error);	
				}
			}
		}
	}
}

//
//Update open service reports
//

echo "<h2>Updating Reports Table</h2>";
if($bad_ids){
	echo "<h1>Cannot update due to bad ids</h1>",EOL;	
}else{
	$reports = array();
	$sql="SELECT r.report_id, r.system_id, r.system_unique_id
	FROM systems_reports AS r;";
	if(!$reslut = $mysqli->query($sql)){
		die($mysqli->error);	
	}
	while($rowReports = $reslut->fetch_assoc()){
		$reports[$rowReports['report_id']] = array('system_unique_id'=>$rowReports['system_unique_id'], 'system_id'=>$rowReports['system_id']);
	}
	$bad_reports = array();
	foreach($reports as $key=>$value){
		if($systems[$value['system_id']]['new_id'] == ''){
			array_push($bad_reports,$key);
		}
		$sql="UPDATE systems_reports SET system_id = '".$systems[$value['system_id']]['new_id']."' WHERE report_id = $key";	
		echo "Execute: ".$sql,EOL;
		if(empty($bad_reports)){
			if($execute){
				if(!$reslut = $mysqli->query($sql)){
					die($mysqli->error);	
				}
			}
		}
	}
}

if(!empty($bad_requests)){
	echo "<h1>Cannot continue found bad requests</h1>",EOL;
	d($bad_requests);
	d("select * from systems_requests where request_num in(".implode(',',$bad_requests).");");
}

if(!empty($bad_reports)){
	echo "<h1>Cannot continue found bad reports</h1>",EOL;
	d($bad_reports);
	d("select * from systems_reports where report_id in(".implode(',',$bad_reports).");");
}

echo "<h2>Completed</h2>";
$mysqli->close();


?>

<script type="text/javascript">
	clearInterval(scroller);
</script>
</body>
</html>

	
	
	
	
	
	
	
	
	
	
