<!DOCTYPE html>
<html>
<head>
<script type="text/javascript">
function getDocHeight() {
    var D = document;
    return Math.max(
        Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
        Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
        Math.max(D.body.clientHeight, D.documentElement.clientHeight)
    );
}

document.documentElement.scrollTop = getDocHeight();

var scroller = setInterval(function() {  
    document.documentElement.scrollTop = getDocHeight();
}, 100 /*update intervall in ms*/);

</script>
</head>
<body>
<?php
die("DONE");

/*
2015-8-14 Evotodi
This script updates the systems id array to the new format
and changes the system id to the new oxford format (CTxxxx MRxxxx NMxxxx PCTxxxx TRxxx WSxxx).

It must be run once the updated pages have been uploaded.

This update will make changing system ids easier in the future.
*/

ob_start();//Start output buffering

$debug = false;
if(isset($_GET['debug'])){
	$debug = true;	
}

$execute = false; //Execute sql update/insert/delete
if(isset($_GET['execute'])){
	$execute = true;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/define_inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

require($_SERVER['DOCUMENT_ROOT'].'/mysqlInfo.php');
$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno) {die('There was an error running the query [' . $mysqli->connect_error . ']');}

echo "<h2>System IDs to change</h2>";
//
//System ids array key is new id value is old id
//
$system_ids = array(
'CT1292'=>'CT413',
'CT1293'=>'CT415',
'CT1294'=>'CT418',
'CT1295'=>'CT424',
'CT1296'=>'CT425',
'CT1297'=>'CT436',
'CT1298'=>'CT442',
'CT1299'=>'CT443',
'CT1300'=>'CT444',
'CT1301'=>'CT445',
'MR2298'=>'MR212',
'MR2299'=>'MR2120',
'MR2300'=>'MR2122',
'MR2301'=>'MR2123',
'MR2302'=>'MR2124',
'MR2303'=>'MR2126',
'MR2304'=>'MR213',
'MR2305'=>'MR215',
'MR2306'=>'MR217-2 (was MR4160)',
'MR2307'=>'MR218',
'MR2308'=>'MR219',
'MR2309'=>'MR231-2',
'MR2310'=>'MR234',
'MR2311'=>'MR237-2',
'MR2312'=>'MR238 (was MR8664)',
'MR2313'=>'MR239',
'MR2314'=>'MR241',
'MR2315'=>'MR242',
'MR2316'=>'MR243',
'MR2317'=>'MR245 (was MR4138)',
'MR2318'=>'MR247',
'MR2319'=>'MR248',
'MR2320'=>'MR8675',
'MR2321'=>'MR8697',
'MR2322'=>'MR8748',
'MR2323'=>'MR8759',
'NM3000'=>'NM111',
'NM3001'=>'NM112',
'NM3002'=>'NM113',
'PCT4000'=>'PCT511-2',
'MR2221'=>'W/SC-119',
'CT1302'=>'W/SC-120',
'MR2284'=>'W/SC-122',
'MR2253'=>'W/SC-123',
'MR2292'=>'W/SC-388',
'CT1303'=>'W/SC-418',
'CT1304'=>'W/SC-859',
'MR2297'=>'W/SC-1017'
);
d($system_ids);

$systems_updated = array();

if(!$execute){
	echo "<h1>Not Executing SQL</h1>";	
}else{
	echo "<h2>Executing SQL Statements</h2>";	
}

//
//Update systems table
//
scroll();
echo "<h2>Updating Systems Table</h2>";
foreach($system_ids as $new=>$old){
	$sql="UPDATE systems SET system_id = '$new', 
	notes = concat_ws('\r\n', notes, 'MIR Product ID ".$old."')
	WHERE system_id = '$old';";
	echo "Execute: ".$sql,EOL;
	if($execute){
		if(!$reslut = $mysqli->query($sql)){
			die($mysqli->error);	
		}
		if($mysqli->affected_rows >= 1){
			array_push($systems_updated,$new);
		}
	}
}

//
//Update open service request
//
scroll();
echo "<h2>Updating Requests Table</h2>";
foreach($system_ids as $new=>$old){
	$sql="UPDATE systems_requests SET system_id = '$new' WHERE system_id = '$old';";
	echo "Execute: ".$sql,EOL;
	if($execute){
		if(!$reslut = $mysqli->query($sql)){
			die($mysqli->error);	
		}
	}
}

//
//Update open service reports
//
scroll();
echo "<h2>Updating Reports Table</h2>";
foreach($system_ids as $new=>$old){
	$sql="UPDATE systems_reports SET system_id = '$new' WHERE system_id = '$old';";
	echo "Execute: ".$sql,EOL;
	if($execute){
		if(!$reslut = $mysqli->query($sql)){
			die($mysqli->error);	
		}
	}
}

echo "<h2>Systems updated list</h2>";
if(!empty($systems_updated)){
	foreach($systems_updated as $sys){
		echo $sys,EOL;
	}
}else{
	echo 'No Systems updated !';	
}


echo "<h2>Completed</h2>";
$mysqli->close();
scroll();

function scroll(){
	echo EOL,"<script>document.documentElement.scrollTop = getDocHeight();</script>",EOL;	
}

?>

<script type="text/javascript">
	clearInterval(scroller);
</script>
</body>
</html>

	
	
	
	
	
	
	
	
	
	
