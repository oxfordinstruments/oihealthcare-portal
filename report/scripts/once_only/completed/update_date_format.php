<?php
/*###############################################################################################
Order of execution:
	1. Load Old database into oiheal5_report_tmp
	2. Browser run update_date_format.php
	3. Browser run modify_database.php
	4. CLI run insert_db_data_1.php
	6. Browser run insert_db-data_2.php
*/###############################################################################################

$just_prepare = true;
if(isset($_GET['update'])){
	$just_prepare = false;	
}

$tables = array(
	array('id','edited_date','remote_assignments'),
	array('index','date','registration_requests'),
	array('id','date','assigned_sites_history'),
	array('id','email_date','evening_updates'),
	array('index','date','hours'),
	array('index','date','mri_readings'),
	array('index','date','nps_codes'),
	array('index','expire','nps_codes'),
	array('index','used_date','nps_codes'),
	array('index','reminder_sent','nps_codes'),
	array('index','survey_send_date','nps_data'),
	array('index','date','nps_responses'),
	array('index','date','parts'),
	array('index','date','pwd_reset_reqs'),
	array('id','date','reports'),
	array('id','finalize_date','reports'),
	array('id','pm_date','reports'),
	array('id','report_edited_date','reports'),
	array('id','valuation_date','reports'),
	array('id','invoice_marked_date','reports'),
	array('id','invoice_date','reports'),
	array('id','deleted_date','reports'),
	array('index','request_date','requests'),
	array('index','onsite_date','requests'),
	array('index','initial_call_date','requests'),
	array('index','response_date','requests'),
	array('index','edited_date','requests'),
	array('index','deleted_date','requests'),
	array('index','equipment_acceptance_date','sites'),
	array('index','contract_start_date','sites'),
	array('index','contract_end_date','sites'),
	array('index','warranty_start_date','sites'),
	array('index','warranty_end_date','sites'),
	array('index','credit_hold_date','sites'),
	array('index','last_pm','sites'),
	array('index','equipment_date_installed','sites'),
	array('index','mr_lhe_list_update','sites'),
	array('index','edited_date','sites'),
	array('index','created_date','sites'),
	array('id','quickbooks_date','sites_face_sheet'),
	array('id','logbook_date','sites_face_sheet'),
	array('id','afterhours_date','sites_face_sheet'),
	array('id','idsticker_date','sites_face_sheet'),
	array('id','basket_date','sites_face_sheet'),
	array('index','last_login','users'),
	array('index','pwdchgdate','users')
);

$limit = '10000';
$debug = false;


if(isset($_SERVER['argv'][1]) and $_SERVER['argv'][1] == '--cron'){
	define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : PHP_EOL);
	$base_path = preg_replace("/(public_html).*/",'public_html',dirname(__FILE__),2);
	$html = false;
}else{
	define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
	$base_path = $_SERVER['DOCUMENT_ROOT'];
	$html = true;
	ht('top');
}

$settings = new SimpleXMLElement($base_path.'/settings.xml', null, true);
require_once($base_path.'/define_inc.php');



require_once($base_path.'/resources/kint/Kint.class.php');

Kint::enabled(false);	

$host="localhost"; // Host name
$username="oiheal5_justin"; // Mysql username
$password="b00b135"; // Mysql password
$db_name="oiheal5_report_tmp"; // Database name
//require($base_path.'/mysqlInfo.php');
$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno){
	ht('bot');
	die('There was an error running the query [' . $mysqli->connect_error . ']');
}


$sql=<<<EOT
ALTER TABLE `nps_codes`	CHANGE COLUMN `date` `date` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci' AFTER `email`, CHANGE COLUMN `expire` `expire` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci' AFTER `date`, CHANGE COLUMN `used_date` `used_date` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci' AFTER `used`;
ALTER TABLE `nps_data` CHANGE COLUMN `survey_send_date` `survey_send_date` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci' AFTER `fiscal_year`;
ALTER TABLE `parts` CHANGE COLUMN `date` `date` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci' AFTER `site_unique_id`;
ALTER TABLE `reports` CHANGE COLUMN `finalize_date` `finalize_date` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci' AFTER `date`, CHANGE COLUMN `pm_date` `pm_date` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci' AFTER `pm`;
ALTER TABLE `requests` CHANGE COLUMN `deleted_date` `deleted_date` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci' AFTER `deleted_by`;
ALTER TABLE `sites` CHANGE COLUMN `last_pm` `last_pm` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci' AFTER `pm_freq`;
ALTER TABLE `sites_face_sheet` CHANGE COLUMN `quickbooks_date` `quickbooks_date` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci' AFTER `completed`, CHANGE COLUMN `logbook_date` `logbook_date` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci' AFTER `quickbooks_notes`, CHANGE COLUMN `afterhours_date` `afterhours_date` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci' AFTER `logbook_notes`, CHANGE COLUMN `idsticker_date` `idsticker_date` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci' AFTER `afterhours_notes`, CHANGE COLUMN `basket_date` `basket_date` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci' AFTER `idsticker_notes`;
ALTER TABLE `mri_readings` CHANGE COLUMN `date` `date` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci' AFTER `contact`;
ALTER TABLE `sites` CHANGE COLUMN `credit_hold_date` `credit_hold_date` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci' AFTER `credit_hold`;
ALTER TABLE `sites` CHANGE COLUMN `mr_lhe_list_update` `mr_lhe_list_update` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci' AFTER `mr_lhe_list`;

UPDATE nps_codes SET `date` = '09/11/2014' WHERE `date` = '09/11//2014';
DELETE FROM nps_codes WHERE `index` = 1323;
UPDATE sites SET last_pm = NULL WHERE `index` IN(119,124,138,154,194,294,295,297,298,299,300,303,311,313,314,316,317,330,331,332,333,334,336,337,339);
UPDATE hours SET `date` = '1969-12-31T00:00:00Z' WHERE `index` IN(10412,10423,10484,10499,10576,10589,10591,10592,10598,10604,10616,10618,10623,10636,10637,10640,10641,10642,10646,10647,10648,10651,10653,10668,10676,10722,10732,10737,10754,10759,10768,10769,10777,10790,10800,10801,10802,10806,10809,10812,10826,10830,10838,10839,10840,10852,10871,10872,10881,10882,10884,10893,10898,10937,10974,10976,10981,10984,11025,11030,11031,11032,11061,11067,11070,11071,11072,11073,11079,11107,11195,11203,11207,11234,11237,11254,11255,11256,11257,11258,11260,11262,11266,11267,11276,11282,11287,11289,11294,11322,11323,11348,11349,11357,11363,11375,11377,11378,11384,11395,11405,11410,11416,11419,11423,11429,11430,11431,11432,11435,11436,11445,11446,11450,11455,11460,11540,11541,11542,11543,11544,11547,11557,11578,11606,11610,11611,11612,11621,11629,11633,11637,11639,11640,11649,11650,11651,11652,11657,11658,11659,11663,11667,11668,11669,11673,11674,11683,11692,11715,11717,11718,11719,11720,11721,11734);
UPDATE hours SET `date` = '12/18/2014' WHERE `index` = 1883;
UPDATE hours SET `date` = '05/21/2014' WHERE `index` = 13187;
UPDATE mri_readings SET `date` = '02/18/2014 6:24' WHERE `index` = 1107;
UPDATE requests SET onsite_date = CASE `index` WHEN 470 THEN '12/06/2013' WHEN 6635 THEN '04/09/2014' WHEN 6919 THEN '06/11/2014' WHEN 6958 THEN '06/19/2014' WHEN 6994 THEN '06/27/2014 12:37' END WHERE `index` IN (470,6635,6919,6958,6994);
UPDATE requests SET response_date = '10/04/2013 10:10' WHERE `index` = 178;
UPDATE requests SET edited_date = NULL WHERE `index` IN(3,6,7,74,80,159,178,182,189);
UPDATE requests SET onsite_date = '08/13/2013' WHERE `index` = 13;
UPDATE sites SET contract_start_date = NULL, contract_end_date = NULL WHERE `index` = 317;
UPDATE sites SET contract_end_date = '09/08/2017' WHERE `index` = 328;
UPDATE sites SET mr_lhe_list_update = NULL WHERE mr_lhe_list_update = '01/01/2000 01:00';
UPDATE sites AS s SET s.equipment_date_installed = NULL WHERE s.`index` IN (12,120,140,159,171,187,215,219,271,272,331);
UPDATE sites AS s SET s.last_pm = NULL WHERE s.`index` IN (1,146,340,341,343);
EOT;

echo "Making initial changes to the database...",EOL;
$sql_arr = explode(';',$sql);
foreach($sql_arr as $sql){
	echo $sql,EOL;
	if(strlen($sql) > 0){
		if(!$result = $mysqli->query($sql)){
			ht('bot');
			die($mysqli->error);	
		}
	}
}

foreach($tables as $key=>$table){
	$txt = "Working on ".$table[2]." - ".$table[1];
	if($html){
		echo "<h2>",$txt,"</h2>",EOL;	
	}else{
		echo "==============",$txt,"=================",EOL;
	}
	update_data($table[0],$table[1],$table[2],$table[3]);
	echo perc($key + 1),"% Percent Complete",EOL;
}
ht('bot');
die();

function perc($ct){
	global $tables;
	return number_format(($ct / count($tables)) * 100,0);
}

function update_data($id,$col,$table){
	global $mysqli, $debug, $base_path, $limit, $just_prepare, $html;
	$dst = array(
		'2012'=>array('s'=>"03/11/2012 02:00",'e'=>"11/04/2012 02:00"),
		'2013'=>array('s'=>"03/10/2013 02:00",'e'=>"11/03/2013 02:00"),
		'2014'=>array('s'=>"03/09/2014 02:00",'e'=>"11/02/2014 02:00")
	);
	
	
	$data_arr = array();
	$bad_rows = array();
	$skipped_rows = array();
	
	$sql = "SELECT `$id`, `$col` FROM $table WHERE `$col` IS NOT NULL and `$col` != '' LIMIT $limit;";
	$result = $mysqli->query($sql);
	while ($row = $result->fetch_assoc()){
		$data_arr[$row[$id]] = $row[$col];	
	}
	
	echo "Current DT: ",date(storef,time()),EOL;
	
	echo "Processing table data...",EOL;
	foreach($data_arr as $key=>$value){
		//$pattern = "/^(([0-9]{4})((-[0-9]{2}){2}(( |T)?([0-9]{2}:){2}[0-9]{2}((\+[0-9]{2}(:[0-9]{2})?)|Z)?)?|(-W[0-9]{2}(-[1-7])?)|(-[1-3]?[0-9]{2})))$/";
		$pattern = "/\d{4}(-\d{2}(-\d{2}(T\d{2}:\d{2}(:\d{2}(\.\d{2})?)?Z)))/";
		//$pattern = "/\d{4}(-\d{2}(-\d{2}(T\d{2}:\d{2}(:\d{2}(\.\d{2})?)?)))/";
		if(preg_match($pattern,$value)){
			if($debug){echo "Date for ID $key is already ISO 8601",EOL;}
			array_push($skipped_rows,$key);
			unset($data_arr[$key]);
			continue;
		}
		
		$pattern2 = "/^([0-9]|0[0-9]|1[0-2])\/([0-9]|[0-2][0-9]|3[0-1])\/[0-9]{4}(?!.)/";
		$date_only = false;
		if(preg_match($pattern2,$value)){
			$date_only = true;
			if($debug){echo "Found date only: ",$value," changed to: ",$value,"T00:00:00Z",EOL;}
			$value .= "T00:00:00Z";	
		}
		
		$dt = strtotime($value);
		if($dt === false){
			//array_push($bad_rows,$key);	
			echo "ID: $key   Fails date check",EOL;
			array_push($bad_rows,$key);	
			continue;
		}
		$dt_year = date('Y',$dt);
		if(intval($dt_year) < 1995){
			array_push($bad_rows,$key);	
			echo "ID: $key  Date less than 1998. dt year:",$dt_year,EOL;
			continue;
		}
		
		if(!$date_only){				
			$sdt = strtotime($dst[$dt_year]['s']);
			$edt = strtotime($dst[$dt_year]['e']);
			if($debug){echo "Old date: ",date(storef,$dt),"  ->  ";}
			if($dt >= $sdt && $dt <= $edt){
				$data_arr[$key] = $dt - (60*60*4);	
				if($debug){echo "DST -4   ";}
			}else{
				$data_arr[$key] = $dt - (60*60*5);
			}
			if($debug){echo "New date: ",date(storef,$data_arr[$key]),EOL;}
		}else{
			$data_arr[$key] = $dt;
			if($debug){echo "New date: ",date(storef,$data_arr[$key]),EOL;}
		}
	}
	echo "Done processing table data...",EOL;
	
	echo "Checking for skipped rows...",EOL;
	if(count($skipped_rows) > 0){
		echo "Skipped Rows:",EOL;
		if($html){ echo "<span style='color:#00F'>";}
		foreach($skipped_rows as $value){
			echo $value.',';	
		}
		if($html){echo "</span>";}
		echo EOL;
	}
	
	echo "Checking for bad rows...",EOL;
	if(count($bad_rows) > 0){
		
		echo "Bad Rows:",EOL;
		if($html){echo "<span style='color:#F00'>";}
		
		foreach($bad_rows as $value){
			echo $value.',';	
		}
		if($html){echo "</span>";}
		echo EOL;
		if($html){echo "<h3 style='color:#F00'>";}
		echo "Fix these rows then re-run this script.";
		if($html){echo "</h3>";}
		echo EOL;
		return true;
	}
	
	echo "Making sql statements...",EOL;
	$sql = "";
	foreach($data_arr as $key=>$value){
		$temp_value = date(storef,$value);
		$sql.="UPDATE $table SET `$col`='$temp_value' WHERE `$id` = $key;";
	}
	if($html){d(explode(';',$sql));}
	ht('scroll');
	if($just_prepare){
		echo "Just prepare set.",EOL;	
		return true;
	}
	
	if($sql != ""){
		echo "Updating the database...",EOL;
		
		
		if(!$mysqli->multi_query($sql)){
			if($html){d($sql);}
			die("Multi query failed: (" . $mysqli->errno . ") " . $mysqli->error);
		}
		
		
		do{
			if($res = $mysqli->store_result()){
				if($html){d($res->fetch_all(MYSQLI_ASSOC));}
				ht('scroll');
				$res->free();
			}
		}while($mysqli->more_results() && $mysqli->next_result());
	}else{
		echo "Nothing to update",EOL;	
	}
	
	echo EOL,"TABLE DONE",EOL;
}

function ht($sect){
	global $html;
	if(!$html){
		return;
	}
$html_top=<<<HTML_TOP
<!doctype html>
<html>
<head>
<meta charset='utf-8'>
<title>Untitled Document</title>
<script type='text/javascript'>
	function getDocHeight() {
		var D = document;
		return Math.max(
			Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
			Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
			Math.max(D.body.clientHeight, D.documentElement.clientHeight)
		);
	}
	document.documentElement.scrollTop = getDocHeight();

</script>
</head>
<body>
HTML_TOP;

$html_bot=<<<HTML_BOT
</body>
</html>
HTML_BOT;

$html_scroll=<<<HTML_SCROLL
<script type='text/javascript'>document.documentElement.scrollTop = getDocHeight();</script>
HTML_SCROLL;

	switch($sect){
		case 'top':
			echo $html_top;
			break;
		case 'bot':
			echo $html_bot;
			break;
		case 'scroll':
			echo $html_scroll;
			break;	
	}


}

?>