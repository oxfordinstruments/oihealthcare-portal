<?php
die("DONE");
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 3/6/2017
 * Time: 12:02 PM
 */

//
// Error reporting
//
//error_reporting(E_ALL);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
ini_set('display_errors', 'On');

$debug = true;
$insert = true;

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/php_utils.php');
$php_utils =  new phpUtils();

define('EOL', '<br>');
$errors = false;

$php_utils->message('Begin');//-----------------------------------------------------------------------------------------------

$php_utils->message('Updating permission perm_nps_reports',true);//-----------------------------------------------------------------------------------------------
$sql="UPDATE users_perm_id SET name = 'Receive NPS Report', perm = 'perm_rcv_nps_report' WHERE perm = 'perm_nps_reports';";
if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}


$php_utils->message('Getting existing permissions',true);//-----------------------------------------------------------------------------------------------

$sql="SELECT p.id, p.name, p.perm, p.sequence FROM users_perm_id AS p ORDER BY sequence;";
if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }

$perm_del_system = false;
$perms = array();
$perm_rcv_pm_overdue = 0;
$perm_rcv_futsysmov = 0;
$perm_edit_financial = 0;
while($row = $result->fetch_assoc()){
	if($row['perm'] == 'perm_rcv_pm_overdue'){
		$perm_rcv_pm_overdue = intval($row['id']);
	}

	if($row['perm'] == 'perm_rcv_futsysmov'){
		$perm_rcv_futsysmov = intval($row['id']);
	}

	if($row['perm'] == 'perm_edit_financial'){
		$perm_edit_financial = intval($row['id']);
	}

	$perms[$row['id']] = array(
		'name' => $row['name'],
		'perm' => $row['perm'],
		'sequence' => $row['sequence']
	);
}
d($perms);
d($perm_rcv_pm_overdue);
d($perm_rcv_futsysmov);
d($perm_edit_financial);

$php_utils->message('Getting existing preferences',true);//-----------------------------------------------------------------------------------------------

$sql="SELECT p.id, p.name, p.pref FROM users_pref_id AS p ORDER BY id;";
if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }

$perm_del_system = false;
$prefs = array();
$pref_rcv_pm_overdue = 0;
$pref_rcv_futsysmov = 0;
while($row = $result->fetch_assoc()){
	if($row['pref'] == 'pref_rcv_pm_overdue'){
		$pref_rcv_pm_overdue = intval($row['id']);
	}

	if($row['pref'] == 'pref_rcv_futsysmov'){
		$pref_rcv_futsysmov = intval($row['id']);
	}

	$prefs[$row['id']] = array(
		'name' => $row['name'],
		'pref' => $row['pref']
	);
}
d($prefs);
d($pref_rcv_pm_overdue);
d($pref_rcv_futsysmov);

$php_utils->message('Checking for linked_perm col in users_pref_id table',true);//-----------------------------------------------------------------------------------------------
$prefs_perm_col = false;
$sql="DESCRIBE users_pref_id;";
if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }
while($row = $result->fetch_assoc()){
	if($row['Field'] == 'linked_perm'){
		$prefs_perm_col = true;
	}
}
d($prefs_perm_col);

if(!$prefs_perm_col){
	$php_utils->message('Updating users_pref_id table schema',true);//-----------------------------------------------------------------------------------------------
	$sql="ALTER TABLE `users_pref_id` ADD COLUMN `linked_perm` VARCHAR(50) NOT NULL AFTER `pref`;";
	if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}
}

$php_utils->message('Updating users_pref_id table data',true);//-----------------------------------------------------------------------------------------------
foreach($perms as $perm){
	$pref_name = str_replace('perm_', 'pref_',$perm['perm']);
	$sql="UPDATE users_pref_id SET linked_perm = '".$perm['perm']."' WHERE pref = '".$pref_name."';";
	//echo $sql,EOL;
	if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}
}

$php_utils->message('Getting all engineers',true);//-----------------------------------------------------------------------------------------------
$engineers = array();
$sql="SELECT ur.uid, uri.role
FROM users_roles AS ur
LEFT JOIN users_role_id AS uri ON uri.id = ur.rid
LEFT JOIN users AS u ON u.uid = ur.uid
WHERE uri.role = 'role_engineer' AND u.active = 'Y'
ORDER BY u.uid;
";
if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }
while($row = $result->fetch_assoc()){
	array_push($engineers, $row['uid']);
}
d($engineers);

$php_utils->message('Giving all engineers overdue_pm permission and perm_rcv_futsysmov permission',true);//-----------------------------------------------------------------------------------------------
foreach($engineers as $engineer){

	$sql="SELECT up.uid
	FROM users_perms AS up
	LEFT JOIN users_perm_id AS upi ON upi.id = up.pid
	WHERE up.uid = '".$engineer."' AND upi.perm = 'perm_rcv_futsysmov';";
	if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }
	if($result->num_rows == 0){
		$sql="INSERT INTO users_perms (uid, pid) VALUES ('".$engineer."', $perm_rcv_futsysmov)";
		echo $sql,EOL;
		if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}
	}

	$sql="SELECT up.uid
	FROM users_perms AS up
	LEFT JOIN users_perm_id AS upi ON upi.id = up.pid
	WHERE up.uid = '".$engineer."' AND upi.perm = 'perm_rcv_pm_overdue';";
	if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }
	if($result->num_rows == 0){
		$sql="INSERT INTO users_perms (uid, pid) VALUES ('".$engineer."', $perm_rcv_pm_overdue)";
		echo $sql,EOL;
		if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}
	}

}

$php_utils->message('Giving all engineers overdue_pm preference and perm_rcv_futsysmov preference',true);//-----------------------------------------------------------------------------------------------
foreach($engineers as $engineer){

	$sql="SELECT up.uid
	FROM users_prefs AS up
	LEFT JOIN users_pref_id AS upi ON upi.id = up.pid
	WHERE up.uid = '".$engineer."' AND upi.pref = 'pref_rcv_futsysmov';";
	if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }
	if($result->num_rows == 0){
		$sql="INSERT INTO users_prefs (uid, pid) VALUES ('".$engineer."', $pref_rcv_futsysmov)";
		echo $sql,EOL;
		if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}
	}

	$sql="SELECT up.uid
	FROM users_prefs AS up
	LEFT JOIN users_pref_id AS upi ON upi.id = up.pid
	WHERE up.uid = '".$engineer."' AND upi.pref = 'pref_rcv_pm_overdue';";
	if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }
	if($result->num_rows == 0){
		$sql="INSERT INTO users_prefs (uid, pid) VALUES ('".$engineer."', $pref_rcv_pm_overdue)";
		echo $sql,EOL;
		if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}
	}

}

$php_utils->message('Getting all finance ppl',true);//-----------------------------------------------------------------------------------------------
$financers = array();
$sql="SELECT ur.uid, uri.role
FROM users_roles AS ur
LEFT JOIN users_role_id AS uri ON uri.id = ur.rid
LEFT JOIN users AS u ON u.uid = ur.uid
WHERE uri.role = 'role_finance' AND u.active = 'Y'
ORDER BY u.uid;
";
if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }
while($row = $result->fetch_assoc()){
	array_push($financers, $row['uid']);
}
d($financers);

$php_utils->message('Giving all financers perm_edit_financial permission',true);//-----------------------------------------------------------------------------------------------
foreach($financers as $financer){

	$sql="SELECT up.uid
	FROM users_perms AS up
	LEFT JOIN users_perm_id AS upi ON upi.id = up.pid
	WHERE up.uid = '".$financer."' AND upi.perm = 'perm_edit_financial';";
	if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }
	if($result->num_rows == 0){
		$sql="INSERT INTO users_perms (uid, pid) VALUES ('".$financer."', $perm_edit_financial)";
		echo $sql,EOL;
		if($insert){if(!$result = $mysqli->query($sql)){ die('There was an error running the query [' . $mysqli->error . ']'); }}
	}
}

?>