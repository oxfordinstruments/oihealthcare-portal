<!DOCTYPE html>
<html>
<head>
<script type="text/javascript">
function getDocHeight() {
    var D = document;
    return Math.max(
        Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
        Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
        Math.max(D.body.clientHeight, D.documentElement.clientHeight)
    );
}

document.documentElement.scrollTop = getDocHeight();

var scroller = setInterval(function() {  
    document.documentElement.scrollTop = getDocHeight();
}, 100 /*update intervall in ms*/);

</script>
</head>
<body>
<?php
die("DONE");

/*
2015-08-20 Evotodi
This script modifies the users_* tables to move the permission, roles, and groups to sperate tables

It must be run once the updated pages have been uploaded.

*/

ob_start();//Start output buffering

$debug = true;
if(isset($_GET['debug'])){
	$debug = true;	
}

$execute = false; //Execute sql update/insert/delete
if(isset($_GET['execute'])){
	$execute = true;	
}

$changes = false; //Execute sql db changes first
if(isset($_GET['changes'])){
	$changes = true;	
}

//if (!@require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php')) {
//	require_once '/report/common/session_control.php';	
//}

require_once($_SERVER['DOCUMENT_ROOT'].'/define_inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/php_utils.php');
$php_utils = new phpUtils();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

// Turn off output buffering
ini_set('output_buffering', 'off');
// Turn off PHP output compression
ini_set('zlib.output_compression', false);
         
//Flush (send) the output buffer and turn off output buffering
//ob_end_flush();
while (@ob_end_flush());
         
// Implicitly flush the buffer(s)
ini_set('implicit_flush', true);
ob_implicit_flush(true);

require($_SERVER['DOCUMENT_ROOT'].'/mysqlInfo.php');
$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno) {die('There was an error running the query [' . $mysqli->connect_error . ']');}

$errors = array();

//
//Make the required changes to the database first
//
echo "<h2>Making required changes to db</h2>";
require('users_update_sql.php');

$multi_sql = explode(";",$sql);
d($multi_sql);
foreach($multi_sql as $msql){
	if(strpos($msql,"--") !== false){
		continue;
	}
	if(strlen($msql) < 1){
		continue;
	}
	$sql = trim($msql).";";
	echo "Execute: ".$sql,EOL;
	scrollto();
	if($changes){
		$php_utils->ping_sql($mysqli);
		if(!$mysqli->query($sql)){
			echo "&emsp;&emsp;ERROR: ".$mysqli->error,EOL;
		}
	}
}

//
//Get current users table data
//
scrollto();
echo "<h2>Getting Users Table Data</h2>";
$sql="SELECT * FROM users;";
d($sql);
if(!$reslut = $mysqli->query($sql)){
	scrollto();
	die($mysqli->error);	
}

$users = array();
while($row = $reslut->fetch_assoc()){
	$users[$row['id']] = $row;
}
d($users);

//
//Get current group ids table data
//
scrollto();
echo "<h2>Getting Group ID Table Data</h2>";
$sql="SELECT * FROM users_group_id;";
d($sql);
if(!$reslut = $mysqli->query($sql)){
	scrollto();
	die($mysqli->error);	
}

$users_group_id = array();
while($row = $reslut->fetch_assoc()){
	$users_group_id[$row['id']] = $row;
}
d($users_group_id);

//
//Get current role ids table data
//
scrollto();
echo "<h2>Getting Role ID Table Data</h2>";
$sql="SELECT * FROM users_role_id;";
d($sql);
if(!$reslut = $mysqli->query($sql)){
	scrollto();
	die($mysqli->error);	
}

$users_role_id = array();
while($row = $reslut->fetch_assoc()){
	$users_role_id[$row['id']] = $row;
}
d($users_role_id);

//
//Get current permission ids table data
//
scrollto();
echo "<h2>Getting Permission ID Table Data</h2>";
$sql="SELECT * FROM users_perm_id;";
d($sql);
if(!$reslut = $mysqli->query($sql)){
	scrollto();
	die($mysqli->error);	
}

$users_perm_id = array();
while($row = $reslut->fetch_assoc()){
	$users_perm_id[$row['id']] = $row;
}
d($users_perm_id);

//
//Get current preference ids table data
//
scrollto();
echo "<h2>Getting Preference ID Table Data</h2>";
$sql="SELECT * FROM users_pref_id;";
d($sql);
if(!$reslut = $mysqli->query($sql)){
	scrollto();
	die($mysqli->error);	
}

$users_pref_id = array();
while($row = $reslut->fetch_assoc()){
	$users_pref_id[$row['id']] = $row;
}
d($users_pref_id);


//
//Insert group data
//
scrollto();
echo "<h2>Inserting Groups Data</h2>";
foreach($users as $key=>$user){
	$gid = '';
	foreach($users_group_id as $group){
		if(strtolower($user[$group['group']]) == 'y'){
			$gid = $group['id'];
		}
	}
	$sql="INSERT INTO users_groups (uid, gid) VALUES ('".$user['uid']."',$gid)";
	echo "Execute: ".$sql,EOL;
	scrollto();
	if($execute){
		if(!$reslut = $mysqli->query($sql)){
			scrollto();
			array_push($errors, array('Error'=>'Inserting Group Data: '.$mysqli->error, 'Num'=>$mysqli->errno, 'Data'=>$sql));
			die($mysqli->error);	
		}
	}
}

//
//Insert role data
//
scrollto();
echo "<h2>Inserting Roles Data</h2>";
foreach($users as $key=>$user){
	$rid_arr = array();
	foreach($users_role_id as $role){
		if(strtolower($user[$role['role']]) == 'y'){
			array_push($rid_arr, $role['id']);
		}
	}
	
	if(count($rid_arr) == 0){
		scrollto();
		//die('No roles for uid: '.$user['uid']);	
		array_push($errors, array('Error'=>'No roles for uid', 'Num'=>0, 'Data'=>$user['uid']));
		continue;
	}
	
	$sql="INSERT INTO users_roles (uid, rid) VALUES ";
	foreach($rid_arr as $rid_val){
		$sql.="('".$user['uid']."',$rid_val),";	
	}
	$sql = rtrim($sql, ',');
	$sql .= ";";
	echo "Execute: ".$sql,EOL;
	scrollto();
	if($execute){
		if(!$reslut = $mysqli->query($sql)){
			scrollto();
			array_push($errors, array('Error'=>'Inserting Roles Data: '.$mysqli->error, 'Num'=>$mysqli->errno, 'Data'=>$sql));
			die($mysqli->error);	
		}
	}
}

//
//Insert permission data
//
scrollto();
echo "<h2>Inserting Permissions Data</h2>";
foreach($users as $key=>$user){
	$pid_arr = array();
	foreach($users_perm_id as $perm){
		if(strtolower($user[$perm['perm']]) == 'y'){
			array_push($pid_arr, $perm['id']);
		}
	}
	
	if(count($pid_arr) == 0){
		continue;
	}
	
	$sql="INSERT INTO users_perms (uid, pid) VALUES ";
	foreach($pid_arr as $pid_val){
		$sql.="('".$user['uid']."',$pid_val),";	
	}
	$sql = rtrim($sql, ',');
	$sql .= ";";
	echo "Execute: ".$sql,EOL;
	scrollto();
	if($execute){
		if(!$reslut = $mysqli->query($sql)){
			array_push($errors, array('Error'=>'Inserting Permissions Data: '.$mysqli->error, 'Num'=>$mysqli->errno, 'Data'=>$sql));
			die($mysqli->error);	
		}
	}
}

//
//Insert preference data
//
scrollto();
echo "<h2>Inserting Preferences Data</h2>";
foreach($users as $key=>$user){
	$pid_arr = array();
	foreach($users_pref_id as $pref){
		if(strtolower($user[$pref['pref']]) == 'y'){
			array_push($pid_arr, $pref['id']);
		}
	}
	
	if(count($pid_arr) == 0){
		continue;
	}
	
	$sql="INSERT INTO users_prefs (uid, pid) VALUES ";
	foreach($pid_arr as $pid_val){
		$sql.="('".$user['uid']."',$pid_val),";	
	}
	$sql = rtrim($sql, ',');
	$sql .= ";";
	echo "Execute: ".$sql,EOL;
	scrollto();
	if($execute){
		if(!$reslut = $mysqli->query($sql)){
			array_push($errors, array('Error'=>'Inserting Preferences Data: '.$mysqli->error, 'Num'=>$mysqli->errno, 'Data'=>$sql));
			die($mysqli->error);	
		}
	}
}

//
//Make the required changes to the database first
//

echo "<h2>Cleaning up users table</h2>";
$sql = <<<SQL
ALTER TABLE `users`	
	DROP COLUMN `grp_employee`,	
	DROP COLUMN `grp_contractor`, 
	DROP COLUMN `grp_customer`;
ALTER TABLE `users`
	DROP COLUMN `role_engineer`,
	DROP COLUMN `role_dispatch`,
	DROP COLUMN `role_finance`,
	DROP COLUMN `role_management`,
	DROP COLUMN `role_basic`,
	DROP COLUMN `role_admin`,
	DROP COLUMN `role_quality`,
	DROP COLUMN `role_sales`,
	DROP COLUMN `role_sales_manager`,
	DROP COLUMN `role_contractor`,
	DROP COLUMN `role_customer`;
ALTER TABLE `users`
	DROP COLUMN `perm_service_request`,
	DROP COLUMN `perm_remote_engineer`,
	DROP COLUMN `perm_rcv_sms`,
	DROP COLUMN `perm_rcv_evening_updates`,
	DROP COLUMN `perm_rcv_service_reports`,
	DROP COLUMN `perm_rcv_service_requests`,
	DROP COLUMN `perm_rcv_valuation_request`,
	DROP COLUMN `perm_rcv_invoice_request`,
	DROP COLUMN `perm_rcv_assignment_change`,
	DROP COLUMN `perm_face_signoff`,
	DROP COLUMN `perm_face_print`,
	DROP COLUMN `perm_change_phone`,
	DROP COLUMN `perm_edit_users`,
	DROP COLUMN `perm_edit_systems`,
	DROP COLUMN `perm_edit_facilities`,
	DROP COLUMN `perm_edit_customers`,
	DROP COLUMN `perm_edit_trailers`,
	DROP COLUMN `perm_edit_reports`,
	DROP COLUMN `perm_no_assigned_systems`,
	DROP COLUMN `perm_facility_manager`,
	DROP COLUMN `perm_extend_login`,
	DROP COLUMN `perm_maintenance_menus`,
	DROP COLUMN `perm_nps_menus`,
	DROP COLUMN `perm_nps_reports`,
	DROP COLUMN `perm_mri_readings_menus`,
	DROP COLUMN `perm_dms_access`,
	DROP COLUMN `perm_kb_access`,
	DROP COLUMN `perm_oivision_access`,
	DROP COLUMN `perm_remote_diag`,
	DROP COLUMN `perm_submit_parcarfbc`,
	DROP COLUMN `perm_close_parcarfbc`,
	DROP COLUMN `perm_view_parcarfbc`,
	DROP COLUMN `perm_tool_cal_edit`,
	DROP COLUMN `perm_tool_cal_view`;
ALTER TABLE `users`
	DROP COLUMN `pref_rcv_evening_updates`,
	DROP COLUMN `pref_rcv_service_reports`,
	DROP COLUMN `pref_rcv_service_requests`,
	DROP COLUMN `pref_rcv_valuation_request`,
	DROP COLUMN `pref_rcv_invoice_request`,
	DROP COLUMN `pref_rcv_assignment_change`,
	DROP COLUMN `pref_rcv_parcarfbc_submit`,
	DROP COLUMN `pref_rcv_sms`,
	DROP COLUMN `pref_show_help`;
ALTER TABLE `systems`
	ADD COLUMN `pre_paid` VARCHAR(2) NULL DEFAULT 'N' AFTER `contract_hours`;
SQL;

$multi_sql = explode(";",$sql);
d($multi_sql);
foreach($multi_sql as $msql){
	if(strpos($msql,"--") !== false){
		continue;
	}
	if(strlen($msql) < 1){
		continue;
	}
	$sql = trim($msql).";";
	echo "Execute: ".$sql,EOL;
	scrollto();
	if($changes){
		if(!$mysqli->query($sql)){
			echo "&emsp;&emsp;ERROR: ".$mysqli->error,EOL;
		}
	}
}

if(count($errors) > 0){
	echo "<h2>Erorrs Found !</h2>";
	d($errors);
	scrollto();
}else{
	echo "<h2>Completed No Errors</h2>";
}
$mysqli->close();
scrollto();


echo "<script type='text/javascript'>";
echo "clearInterval(scroller);";
echo "</script>";


function scrollto(){
echo "<script type='text/javascript'>";
echo "document.documentElement.scrollTop = getDocHeight();";
echo "</script>";	
}

?>


</body>
</html>

	
	
	
	
	
	
	
	
	
	
