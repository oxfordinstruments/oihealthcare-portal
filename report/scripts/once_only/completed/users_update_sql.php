<?php
$sql = <<<SQL
ALTER TABLE `users` DROP INDEX `uid`, ADD UNIQUE INDEX `uid` (`uid`);
--;
RENAME TABLE `users_grp_id` TO `users_group_id`;
RENAME TABLE `users_preference_id` TO `users_pref_id`;
--;
-- DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions` (
	`id` VARCHAR(32) NOT NULL COLLATE 'utf8_unicode_ci',
	`access` INT(10) UNSIGNED NULL DEFAULT NULL,
	`data` TEXT NULL COLLATE 'utf8_unicode_ci',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB;
--;
-- DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE `users_groups` (
	`uid` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`gid` INT(11) NULL DEFAULT NULL,
	INDEX `FK_users_groups_users` (`uid`),
	INDEX `FK_users_groups_users_group_id` (`gid`),
	CONSTRAINT `FK_users_groups_users` FOREIGN KEY (`uid`) REFERENCES `users` (`uid`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `FK_users_groups_users_group_id` FOREIGN KEY (`gid`) REFERENCES `users_group_id` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB;
--;
-- DROP TABLE IF EXISTS `users_group_roles`;
CREATE TABLE `users_group_roles` (
	`gid` INT(11) NULL DEFAULT NULL,
	`rid` INT(11) NULL DEFAULT NULL,
	INDEX `FK_users_group_roles_users_group_id` (`gid`),
	INDEX `FK_users_group_roles_users_role_id` (`rid`),
	CONSTRAINT `FK_users_group_roles_users_group_id` FOREIGN KEY (`gid`) REFERENCES `users_group_id` (`id`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `FK_users_group_roles_users_role_id` FOREIGN KEY (`rid`) REFERENCES `users_role_id` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB;
--;
-- DROP TABLE IF EXISTS `users_perms`;
CREATE TABLE `users_perms` (
	`uid` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`pid` INT(11) NULL DEFAULT NULL,
	INDEX `FK_users_perms_users` (`uid`),
	INDEX `FK_users_perms_users_perm_id` (`pid`),
	CONSTRAINT `FK_users_perms_users` FOREIGN KEY (`uid`) REFERENCES `users` (`uid`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `FK_users_perms_users_perm_id` FOREIGN KEY (`pid`) REFERENCES `users_perm_id` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB;
--;
-- DROP TABLE IF EXISTS `users_prefs`;
CREATE TABLE `users_prefs` (
	`uid` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`pid` INT(11) NULL DEFAULT NULL,
	INDEX `FK_users_prefs_users` (`uid`),
	INDEX `FK_users_prefs_users_pref_id` (`pid`),
	CONSTRAINT `FK_users_prefs_users` FOREIGN KEY (`uid`) REFERENCES `users` (`uid`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `FK_users_prefs_users_pref_id` FOREIGN KEY (`pid`) REFERENCES `users_pref_id` (`id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB;
--;
-- DROP TABLE IF EXISTS `users_roles`;
CREATE TABLE `users_roles` (
	`uid` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`rid` INT(11) NULL DEFAULT NULL,
	INDEX `FK_users_roles_users` (`uid`),
	INDEX `FK_users_roles_users_role_id` (`rid`),
	CONSTRAINT `FK_users_roles_users` FOREIGN KEY (`uid`) REFERENCES `users` (`uid`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `FK_users_roles_users_role_id` FOREIGN KEY (`rid`) REFERENCES `users_role_id` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB;
--;
-- DROP TABLE IF EXISTS `users_role_perms`;
CREATE TABLE `users_role_perms` (
	`rid` INT(11) NULL DEFAULT NULL,
	`pid` INT(11) NULL DEFAULT NULL,
	INDEX `FK_users_role_perms_users_role_id` (`rid`),
	INDEX `FK_users_role_perms_users_perm_id` (`pid`),
	CONSTRAINT `FK_users_role_perms_users_perm_id` FOREIGN KEY (`pid`) REFERENCES `users_perm_id` (`id`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `FK_users_role_perms_users_role_id` FOREIGN KEY (`rid`) REFERENCES `users_role_id` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB;
--;
TRUNCATE `users_groups`;
TRUNCATE `users_roles`;
TRUNCATE `users_perms`;
TRUNCATE `users_prefs`;
TRUNCATE `users_group_roles`;
TRUNCATE `users_role_perms`;
--;
DELETE FROM users_perm_id WHERE id = 23;
DELETE FROM users_perms WHERE pid = 23;
--;
RENAME TABLE `systems_evening_updates` TO `systems_status_updates`;
ALTER TABLE `systems_status_updates` CHANGE COLUMN `tweet` `status` TEXT NULL COLLATE 'utf8_unicode_ci' AFTER `uid`;

ALTER TABLE `users_group_id` ALTER `column` DROP DEFAULT;
ALTER TABLE `users_group_id` CHANGE COLUMN `column` `group` VARCHAR(50) NOT NULL COLLATE 'utf8_unicode_ci' AFTER `name`, DROP COLUMN `roles`;
ALTER TABLE `users_perm_id`	ALTER `column` DROP DEFAULT;
ALTER TABLE `users_perm_id`	CHANGE COLUMN `column` `perm` VARCHAR(50) NOT NULL COLLATE 'utf8_unicode_ci' AFTER `name`, DROP COLUMN `roles`;
ALTER TABLE `users_role_id` ALTER `column` DROP DEFAULT;
ALTER TABLE `users_role_id`	CHANGE COLUMN `column` `role` VARCHAR(50) NOT NULL COLLATE 'utf8_unicode_ci' AFTER `name`, DROP COLUMN `group`;
--;
INSERT INTO `users_perm_id` (`id`, `name`, `perm`, `notes`, `sequence`) VALUES (36, 'Receive PARCARFBC Submit Email', 'perm_rcv_parcarfbc_submit', NULL, 2.8);
--;
INSERT INTO `users_group_roles` (`gid`, `rid`) VALUES (1, 9);
INSERT INTO `users_group_roles` (`gid`, `rid`) VALUES (1, 5);
INSERT INTO `users_group_roles` (`gid`, `rid`) VALUES (1, 2);
INSERT INTO `users_group_roles` (`gid`, `rid`) VALUES (1, 1);
INSERT INTO `users_group_roles` (`gid`, `rid`) VALUES (1, 3);
INSERT INTO `users_group_roles` (`gid`, `rid`) VALUES (1, 4);
INSERT INTO `users_group_roles` (`gid`, `rid`) VALUES (1, 6);
INSERT INTO `users_group_roles` (`gid`, `rid`) VALUES (1, 7);
INSERT INTO `users_group_roles` (`gid`, `rid`) VALUES (1, 8);
INSERT INTO `users_group_roles` (`gid`, `rid`) VALUES (3, 20);
INSERT INTO `users_group_roles` (`gid`, `rid`) VALUES (2, 10);
--;
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (10, 2);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (10, 3);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (10, 5);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (10, 6);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (10, 11);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (10, 16);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (10, 30);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (10, 32);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (20, 16);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (20, 17);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (20, 5);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (20, 6);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (1, 14);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (1, 15);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (1, 16);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (1, 26);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (1, 1);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (1, 20);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (1, 11);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (1, 18);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (1, 25);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (1, 12);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (1, 27);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (1, 28);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (1, 10);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (1, 35);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (1, 29);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (1, 21);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (1, 19);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (1, 24);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (1, 22);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (1, 4);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (1, 36);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (1, 5);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (1, 6);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (1, 3);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (1, 2);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (1, 30);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (1, 33);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (1, 34);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (1, 32);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (2, 14);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (2, 15);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (2, 16);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (2, 26);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (2, 9);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (2, 1);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (2, 18);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (2, 25);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (2, 12);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (2, 27);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (2, 28);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (2, 10);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (2, 35);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (2, 29);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (2, 21);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (2, 19);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (2, 24);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (2, 22);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (2, 4);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (2, 36);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (2, 5);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (2, 6);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (2, 30);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (2, 33);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (2, 34);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (2, 32);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (3, 14);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (3, 15);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (3, 16);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (3, 26);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (3, 9);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (3, 1);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (3, 25);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (3, 12);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (3, 27);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (3, 28);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (3, 10);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (3, 35);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (3, 29);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (3, 21);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (3, 19);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (3, 24);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (3, 22);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (3, 4);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (3, 8);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (3, 36);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (3, 5);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (3, 6);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (3, 30);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (3, 33);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (3, 34);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (3, 32);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (4, 14);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (4, 15);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (4, 16);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (4, 26);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (4, 9);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (4, 1);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (4, 20);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (4, 25);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (4, 12);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (4, 27);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (4, 28);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (4, 10);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (4, 35);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (4, 29);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (4, 21);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (4, 19);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (4, 24);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (4, 22);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (4, 4);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (4, 36);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (4, 5);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (4, 6);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (4, 7);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (4, 30);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (4, 33);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (4, 34);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (4, 32);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (5, 14);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (5, 15);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (5, 16);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (5, 26);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (5, 9);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (5, 25);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (5, 12);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (5, 27);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (5, 28);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (5, 10);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (5, 35);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (5, 21);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (5, 19);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (5, 24);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (5, 4);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (5, 36);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (5, 5);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (5, 6);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (5, 30);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (5, 33);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (5, 34);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (5, 32);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (6, 14);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (6, 15);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (6, 16);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (6, 26);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (6, 9);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (6, 31);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (6, 18);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (6, 25);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (6, 12);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (6, 27);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (6, 28);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (6, 10);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (6, 35);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (6, 29);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (6, 21);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (6, 19);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (6, 24);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (6, 4);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (6, 36);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (6, 5);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (6, 6);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (6, 30);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (6, 33);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (6, 34);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (6, 32);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (7, 14);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (7, 15);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (7, 16);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (7, 26);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (7, 9);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (7, 25);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (7, 12);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (7, 27);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (7, 28);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (7, 10);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (7, 35);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (7, 29);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (7, 21);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (7, 19);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (7, 24);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (7, 4);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (7, 36);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (7, 5);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (7, 6);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (7, 30);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (7, 33);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (7, 34);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (7, 32);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (8, 14);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (8, 15);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (8, 16);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (8, 26);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (8, 9);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (8, 25);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (8, 12);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (8, 27);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (8, 28);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (8, 10);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (8, 35);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (8, 29);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (8, 21);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (8, 19);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (8, 24);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (8, 4);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (8, 36);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (8, 5);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (8, 6);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (8, 30);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (8, 33);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (8, 34);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (8, 32);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (9, 14);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (9, 15);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (9, 16);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (9, 26);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (9, 9);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (9, 18);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (9, 25);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (9, 12);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (9, 27);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (9, 28);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (9, 10);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (9, 35);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (9, 29);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (9, 21);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (9, 19);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (9, 24);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (9, 4);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (9, 36);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (9, 5);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (9, 6);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (9, 30);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (9, 33);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (9, 34);
INSERT INTO `users_role_perms` (`rid`, `pid`) VALUES (9, 32);
--;
UPDATE `users_pref_id` SET `name`='Rcv Status Updates', `pref`='pref_rcv_status_updates' WHERE  `id`=1;
UPDATE `users_perm_id` SET `name`='Receive System Status Update Emails', `perm`='perm_rcv_status_updates' WHERE  `id`=4;
SQL;



?>