<?php 
die("USED");

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
session_name("OIREPORT");
session_start();

ob_start();

$host="localhost"; // Host name
$username="root"; // Mysql username
$password="b00b135"; // Mysql password
$db_name="report"; // Database name

$mysqli = new mysqli("$host", "$username", "$password", "$db_name");

$sql="SELECT * FROM systems_requests WHERE CAST(request_num AS UNSIGNED) < 60000;";
$resultReqFM = $mysqli->query($sql);

//while($rowReqFM = $resultReqFM->fetch_assoc()){
//
//}
//
//die("DONE");
$noreport = array();

while($rowReqFM = $resultReqFM->fetch_assoc()){
	$resultSite = $mysqli->query("SELECT * FROM systems WHERE unique_id = '".$rowReqFM['system_unique_id']."';");
	$rowSite = $resultSite->fetch_assoc();
	
	$reportsArray = array();
	$resultRepFM = $mysqli->query("SELECT * FROM systems_reports_fm WHERE unique_id = '".$rowReqFM['unique_id']."';");
	while($rowRepFM = $resultRepFM->fetch_assoc()){
		array_push($reportsArray,$rowRepFM);	
	}
	if(empty($reportsArray)){
		echo "NO REPORT: ",$rowReqFM['unique_id'],EOL;
		$noreport[$rowReqFM['request_num']] = $rowReqFM['unique_id'];	
		continue;
	}
	
	//echo EOL,$rowReqFM['unique_id'],": <pre>",print_r($reportsArray),"</pre>",EOL;
	
	
	$search = array("'","&","’","\\",'"','Open Actions:');
	$replace = array("`"," and ","`","/","`","");
	$prob_found = str_replace($search,$replace,$reportsArray[0]['SERVICE REPORTS:Prob Found']);
	$service = str_replace($search,$replace,$reportsArray[0]['SERVICE REPORTS:Det Svc1']);
	
	$unique_id = $rowReqFM['unique_id'];
	
	// Do the report
	$sql="INSERT INTO reports
	(`report_id`,
	`user_id`,
	`system_id`,
	`system_unique_id`,
	`status`,
	`site_nickname`,
	`complaint`,
	`prob_found`,
	`service`,
	`engineer`,
	`assigned_engineer`,
	`date`,
	`po_number`,
	`downtime`,
	`pm`,
	`equipment_status`,
	`invoice_required`,
	`new_report`,
	`unique_id`,
	`fm_ref`,
	`fm_site_name`,
	`contract_override`,
	`prev_report_id`,
	`slice_mas_count`,
	`gantry_rev`,
	`helium_level`,
	`vessel_pressure`,
	`recon_ruo`,
	`cold_ruo`,
	`compressor_hours`,
	`compressor_pressure`)
	VALUES
	(\"".$rowReqFM['request_num']."\",
	\"".$rowReqFM['engineer']."\",
	\"".$rowReqFM['system_id']."\",
	\"".$rowReqFM['system_unique_id']."\",
	\"closed\",
	\"".$rowReqFM['site_nickname']."\",
	\"".$rowReqFM['problem_reported']."\",
	\"$prob_found\",
	\"$service\",
	\"".$rowReqFM['engineer']."\",
	\"".$rowReqFM['engineer']."\",
	\"".date("m/d/Y",strtotime($reportsArray[0]['Date']))."\",
	\"".$rowReqFM['po_num']."\",
	\"0\",
	\"".$rowReqFM['pm']."\",
	\"1\",
	\"".$rowReqFM['invoice_required']."\",
	\"N\",
	\"$unique_id\",
	\"".$rowReqFM['fm_ref']."\",
	\"".$rowReqFM['fm_site_name']."\",
	'',
	'',
	'0',
	'0',
	'0',
	'0',
	'0',
	'0',
	'0',
	'0'
	);";
	
	//echo "Report SQL: ",$sql,EOL,EOL;
	if(!$result = $mysqli->query($sql)){
		echo EOL,"REPORT ERROR - ",$unique_id," - ",$mysqli->error,EOL;	
	}
	
	// Do the parts
	foreach($reportsArray as $key=>$value){
		if(!empty($value['SERVICE REPORTS:Part Number VVDF']) or !empty($value['SERVICE REPORTS:Description VVDF']) or !empty($value['SERVICE REPORTS:Qty VVDF'])){
			
			$desc = str_replace($search,$replace,$value['SERVICE REPORTS:Description VVDF']);
			
			$sql="INSERT INTO parts 
			(`report_id`,
			`number`,
			`description`,
			`qty`,
			`price`,
			`unique_id`,
			`system_id`,
			`system_unique_id`,
			`date`
			)VALUES(
			'".$rowReqFM['request_num']."',
			'".$value['SERVICE REPORTS:Part Number VVDF']."',
			'$desc',
			'".$value['SERVICE REPORTS:Qty VVDF']."',
			'',
			'$unique_id',
			'".$rowReqFM['system_id']."',
			'".$rowReqFM['system_unique_id']."',
			'".date("m/d/Y",strtotime($value['Date']))."'
			);";
			
			//echo "Parts SQL: ",$sql,EOL,EOL;
			if(!$result = $mysqli->query($sql)){
				echo EOL,"PARTS ERROR - ",$unique_id," - ",$mysqli->error,EOL;	
			}
		}
	}
	
	// Do the hours
	foreach($reportsArray as $key=>$value){
		if(!empty($value['SERVICE REPORTS:Labor OT']) or !empty($value['SERVICE REPORTS:Labor Reg']) or !empty($value['SERVICE REPORTS:Travel OT']) or !empty($value['SERVICE REPORTS:Travel Reg'])){
			$sql="INSERT INTO hours
			(`report_id`,
			`date`,
			`reg_labor`,
			`ot_labor`,
			`reg_travel`,
			`ot_travel`,
			`unique_id`,
			`system_id`,
			`system_unique_id`
			)VALUES(
			'".$rowReqFM['request_num']."',
			'".date("m/d/Y",strtotime($value['SERVICE REPORTS:Dates VVDF']))."',
			'".$value['SERVICE REPORTS:Labor Reg']."',
			'".$value['SERVICE REPORTS:Labor OT']."',
			'".$value['SERVICE REPORTS:Travel Reg']."',
			'".$value['SERVICE REPORTS:Travel OT']."',
			'$unique_id',
			'".$rowReqFM['system_id']."',
			'".$rowReqFM['system_unique_id']."'		
			);";
			
			//echo "Hours SQL: ",$sql,EOL,EOL;
			if(!$result = $mysqli->query($sql)){
				echo EOL,"HOURS ERROR - ",$unique_id," - ",$mysqli->error,EOL;	
			}
			
		}else{
			$sql="INSERT INTO hours
			(`report_id`,
			`date`,
			`reg_labor`,
			`ot_labor`,
			`reg_travel`,
			`ot_travel`,
			`unique_id`,
			`system_id`,
			`system_unique_id`
			)VALUES(
			'".$rowReqFM['request_num']."',
			'".date("m/d/Y",strtotime($value['Date']))."',
			'0',
			'0',
			'0',
			'0',
			'$unique_id',
			'".$rowReqFM['system_id']."',
			'".$rowReqFM['system_unique_id']."'			
			);";
			
			echo "HOURS MISSING: ",$unique_id," - ",$rowReqFM['system_id'],EOL;
			//echo "Hours SQL: ",$sql,EOL,EOL;	
			if(!$result = $mysqli->query($sql)){
				echo EOL,"HOURS ERROR - ",$unique_id," - ",$mysqli->error,EOL;	
			}
		}
	}
	
}

echo EOL,EOL,EOL,"No Reports: <pre>",print_r($noreport),"</pre>",EOL;
echo "No Report Count: ",count($noreport),EOL;

echo EOL,"DONE",EOL;
?>
