<?php
/**
 * @package OiHealthcarePortal
 * @file overdue_pms.php
 * @author Evotodi <evotodi@gmail.com>
 * @date 11/10/2016
 * @time 10:15 AM
 */
//
// Error reporting
//
//error_reporting(E_ALL);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
ini_set('display_errors', 'On');

$no_define_eol = true;
if(substr(php_sapi_name(), 0, 3) == 'cli' || empty($_SERVER['REMOTE_ADDR'])) {
	//shell
	$cron = false;
	if(isset($_SERVER['argv'][1]) and $_SERVER['argv'][1] == '--cron'){
		$cron = true;
	}
	$docroot = preg_replace("/(public_html).*/",'public_html',dirname(__FILE__),2);
	$no_define_eol = true;
	define('EOL',PHP_EOL);
} else {
	//webserver
	$cron = false;
	$docroot = $_SERVER['DOCUMENT_ROOT'];
	define('EOL','<br />');
}

$debug = false;
$send = false;
if(isset($_GET['debug'])){
	$debug = true;
	if(isset($_GET['send'])){$send = true;}else{$send = false;}
}else{
	$send = true;
}

$single = false;
if(isset($_GET['single'])){
	$single = true;
	require_once($docroot.'/report/common/session_control.php');
}

$settings = new SimpleXMLElement($docroot.'/settings.xml', null, true);

$no_define_eol = true;
require_once($docroot.'/define_inc.php');

require_once($docroot.'/log/log.php');
$log = new logger();

require_once($docroot.'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

d($_GET);

$log->loginfo("Sending Overdue PMs emails",0,false,basename(__FILE__),__LINE__);

require_once($docroot.'/resources/moment_php/Moment.php');
require_once($docroot.'/resources/moment_php/MomentException.php');
require_once($docroot.'/resources/moment_php/MomentHelper.php');
require_once($docroot.'/resources/moment_php/MomentLocale.php');
require_once($docroot.'/resources/moment_php/MomentFromVo.php');

require_once($docroot.'/report/common/scripts/php_utils.php');
$php_utils =  new phpUtils();

require_once($docroot.'/resources/smarty/libs/Smarty.class.php');
$smarty = new Smarty();
$smarty->setTemplateDir($docroot.$settings->email_templates);
$smarty->setCompileDir($docroot.$settings->email_template_c);
$smarty->setCacheDir($docroot.$settings->email_cache);
$smarty->setConfigDir($docroot.$settings->email_configs);
$smarty->use_sub_dirs = true;
$smarty->force_compile = true;
//$smarty->debugging = true;
$smarty->caching = false;

$now = new \Moment\Moment();
$now->setImmutableMode(true);
d($now->format());

if($settings->disable_email == '1' and !$debug){
	$send = false;
}

$php_utils->message('Begin');

require($docroot.'/mysqlInfo.php');
$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno) {die('There was an error running the query [' . $mysqli->connect_error . ']');}

$php_utils->message('Getting Systems PM Info');

$sql="SELECT sbc.ver_unique_id, sb.system_id, sbc.nickname, sb.pm_dates, sb.last_pm, sb.pm_freq, u.name AS engineer, sbc.credit_hold, sbc.pre_paid
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
LEFT JOIN systems_assigned_pri AS sap ON sap.system_ver_unique_id = sbc.ver_unique_id
LEFT JOIN users AS u ON sap.uid = u.uid
WHERE sbc.property = 'c'
AND CAST(sb.pm_freq AS UNSIGNED) != 0
AND sb.pm_start != '';";

if(!$result = $mysqli->query($sql)){
	die('There was an error running the query [' . $mysqli->error . ']');
}

$look_ahead = $now->addDays($settings->pm->look_ahead);
$overdue = array();
$late = array();

while($row = $result->fetch_assoc()){
	$last_pm = new \Moment\Moment($row['last_pm']);
	$last_pm_valid = true;
	if($last_pm > $now){
		$last_pm_valid = false;
	}
	$pm_dates = json_decode($row['pm_dates']);

	$pmd_array = array();
	foreach($pm_dates as $key => $val){
		$pm_date_init = new \Moment\Moment($now->getYear() . '-' . sprintf('%02d', $val[0]) . '-' . sprintf('%02d', $val[1]), null, true);
		$pm_date_init_prev = $pm_date_init->subtractYears(1);
		$pm_date_init_next = $pm_date_init->addYears(1);

		$start = $pm_date_init_prev->subtractMonths(intval($row['pm_freq']))->addDays(31)->format('Y-m-d');
		$end = $pm_date_init_prev->addDays(30)->format('Y-m-d');
		$pmd_array[$pm_date_init_prev->format('Y-m-d')] = array('start' => $start, 'end' => $end);

		$start = $pm_date_init->subtractMonths(intval($row['pm_freq']))->addDays(31)->format('Y-m-d');
		$end = $pm_date_init->addDays(30)->format('Y-m-d');
		$pmd_array[$pm_date_init->format('Y-m-d')] = array('start' => $start, 'end' => $end);

		$start = $pm_date_init_next->subtractMonths(intval($row['pm_freq']))->addDays(31)->format('Y-m-d');
		$end = $pm_date_init_next->addDays(30)->format('Y-m-d');
		$pmd_array[$pm_date_init_next->format('Y-m-d')] = array('start' => $start, 'end' => $end);
	}
	ksort($pmd_array);

	$next_pm = false;
	$keys = array_keys($pmd_array);
	foreach($pmd_array as $key => $val){
		if($last_pm->isBetween(new \Moment\Moment($val['start']), new \Moment\Moment($val['end']))){
			$next_pm = $key;
			break;
		}
	}
	$key = array_search($next_pm, $keys);
	$next_pm = $keys[$key + 1];
	$next_pm_moment = new \Moment\Moment($next_pm, 'UTC', true);
	$next_pm_overdue = new Moment\Moment($next_pm, 'UTC');
	$next_pm_overdue->addDays($settings->pm->overdue);
	$next_pm_late = new Moment\Moment($next_pm, 'UTC');
	$next_pm_late->addDays($settings->pm->late);

	$credit_hold = false;
	if(strtolower($row['credit_hold']) == 'y'){
		$credit_hold = true;
	}
	$pre_paid = false;
	if(strtolower($row['pre_paid']) == 'y'){
		$pre_paid = true;
	}

	if($next_pm_overdue < $now){
		$days = $next_pm_moment->fromNow();
		$overdue[$row['system_id']] = array(
			'system_ver_unique_id'=>$row['ver_unique_id'],
			'nickname'=>$row['nickname'],
			'engineer'=>$row['engineer'],
			'credit_hold'=>$credit_hold,
			'pre_paid'=>$pre_paid,
			'pm_freq'=>intval($row['pm_freq']),
			'last_pm' => $last_pm->format('Y-m-d'),
			'last_pm_valid' => $last_pm_valid,
			'next_pm' => $next_pm,
			'days' => sprintf('%01.2f',$days->getDays())
		);
	}elseif($next_pm_late < $now){
		$days = $next_pm_moment->fromNow();
		$late[$row['system_id']] = array(
			'system_ver_unique_id'=>$row['ver_unique_id'],
			'nickname'=>$row['nickname'],
			'engineer'=>$row['engineer'],
			'credit_hold'=>$credit_hold,
			'pre_paid'=>$pre_paid,
			'pm_freq'=>intval($row['pm_freq']),
			'last_pm' => $last_pm->format('Y-m-d'),
			'last_pm_valid' => $last_pm_valid,
			'next_pm' => $next_pm,
			'days' => sprintf('%01.2f',$days->getDays())
		);
	}

}

$php_utils->customUasort($overdue, 'next_pm');
$php_utils->customUasort($late, 'next_pm');

d($overdue);
d($late);

$php_utils->message('Getting recipients');
$emails = array();
if(!$single){
	$sql = "SELECT u.name,u.email
FROM users AS u
INNER JOIN users_prefs AS upr ON upr.uid = u.uid
INNER JOIN users_pref_id AS prid ON prid.id = upr.pid AND prid.pref = 'pref_rcv_pm_overdue'
INNER JOIN users_perms AS up ON up.uid = u.uid
INNER JOIN users_perm_id AS pmid ON pmid.id = up.pid AND pmid.perm = 'perm_rcv_pm_overdue'
WHERE u.active = 'y';";
	if(!$resultEmails = $mysqli->query($sql)){
		die('There was an error running the query [' . $mysqli->error . ']');
	}

	while($rowEmail = $resultEmails->fetch_assoc()){
		array_push($emails, array("email" => $rowEmail['email'], "name" => $rowEmail['name']));
	}
}else{
	array_push($emails,array("email"=>$_SESSION['userdata']['email'],"name"=>$_SESSION['userdata']['name']));
}
d($emails);

$php_utils->message('Filling In Template');

$smarty->assign('date',date(storef,time()));
$smarty->assign('overdue',$overdue);
$smarty->assign('late',$late);
$smarty->assign('overdue_days',$settings->pm->overdue);
$smarty->assign('late_days',$settings->pm->late);
$smarty->assign('company_name',$settings->company_name);
$smarty->assign('copyright_date',$settings->copyright_date);
$smarty->assign('email_pics',$settings->email_pics);

$php_utils->message('Creating Email');
require_once($docroot.'/resources/PHPMailer/PHPMailerAutoload.php');

$email_result = array();

$mail = new PHPMailer;
$mail->IsSMTP();                                      // Set mailer to use SMTP
$mail->Host = (string)$settings->email_host;  // Specify main and backup server
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = (string)$settings->email_support;    // SMTP username
$mail->Password = (string)$settings->email_password;                           // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted
$mail->From = (string)$settings->email_support;
$mail->FromName = 'Support '.$settings->short_name.' Portal';
if($debug){
	$mail->AddAddress('justin.davis@oxinst.com','Justin Davis');
	array_push($email_result,"justin.davis@oxinst.com");
}else{
	foreach($emails as $email){
		$mail->AddAddress(trim($email['email']),$email['name']);  // Add a recipient
		array_push($email_result,$email['email']);
	}
}
$mail->AddReplyTo((string)$settings->email_support, 'Support '.$settings->short_name.' Portal');
$mail->AddCC((string)$settings->email_support);
$mail->IsHTML(true);                                  // Set email format to HTML
$mail->Subject = 'Overdue System PMs';
$mail->Body = $smarty->fetch('overdue_pms.tpl');

if($debug){
	echo $mail->Body,EOL;
}

$php_utils->message('Sending Emails');
if($send){
	if(!$mail->Send()) {
		echo 'Email could not be sent.',EOL;
		echo 'Mailer Error: ' . $mail->ErrorInfo,EOL;
		$log->logerr($mail->ErrorInfo,1053,false,basename(__FILE__),__LINE__);
		exit;
	}
}else{
	echo "Debug: Not sending email",EOL;
}


$php_utils->message("Email addresses Sent To");
foreach($email_result as $value){
	echo $value,EOL;
	$log->loginfo("Sent to: ".$value,0,false,basename(__FILE__),__LINE__);
}

$php_utils->message('End');
?>