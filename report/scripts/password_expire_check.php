<?php
//Update Completed 12/04/14
//Run Once a day 
$no_define_eol = true;
if(substr(php_sapi_name(), 0, 3) == 'cli' || empty($_SERVER['REMOTE_ADDR'])) {
    //shell
	$cron = false;
	if(isset($_SERVER['argv'][1]) and $_SERVER['argv'][1] == '--cron'){
		$cron = true;
	}
	$docroot = preg_replace("/(public_html).*/",'public_html',dirname(__FILE__),2);
	$no_define_eol = true;
	define('EOL',PHP_EOL);
} else {
    //webserver
	$cron = false;
	$docroot = $_SERVER['DOCUMENT_ROOT'];
	define('EOL','<br>');
}

$settings = new SimpleXMLElement($docroot.'/settings.xml', null, true);
require_once($docroot.'/define_inc.php');

$debug = false;
if(isset($_GET['debug'])){
	$debug = true;
	if(isset($_GET['send'])){$send = true;}else{$send = false;}
}else{
	$send = true;
	if($settings->disable_email == '1'){
		$send = false;	
	}
}

require_once($docroot.'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

require_once($docroot.'/log/log.php');
$log = new logger($docroot);

echo "Begin Password Expire Check ".date(storef,time()),EOL;

require($docroot.'/mysqlInfo.php');
$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno) {die('There was an error running the query [' . $mysqli->connect_error . ']');}

echo "Getting Users",EOL;
$users = array();
$sql="SELECT u.uid, u.firstname, u.email, u.name, u.pwd_chg_date, u.pwd_chg_remind, DATEDIFF(u.pwd_chg_date, CURDATE()) AS diff, u.default_role
FROM users AS u
WHERE u.active = 'y' 
AND u.pwd_chg_date IS NOT NULL 
AND u.pwd_chg_date != '' 
AND DATEDIFF(u.pwd_chg_date, CURDATE()) < 14
AND DATEDIFF(u.pwd_chg_date, CURDATE()) > -5;";	
$resultUsers=$mysqli->query($sql);
while($rowUsers = $resultUsers->fetch_assoc()){
	array_push($users,$rowUsers);
}
d($users);

if($resultUsers->num_rows == 0){
	echo "No passwords expiring.",EOL;	
	echo "DONE ".date(storef,time()),EOL;
	exit();
}

$user_ids = array();
foreach($users as $user){
	array_push($user_ids, $user['uid']);
}
d($user_ids);

$sql="UPDATE users SET pwd_chg_remind = pwd_chg_remind + 1
WHERE uid IN ('".implode("', '",$user_ids)."');";
d($sql);
$result=$mysqli->query($sql);

$mysqli->close();

require_once($docroot.'/resources/smarty/libs/Smarty.class.php');
$smarty = new Smarty();
$smarty->setTemplateDir($docroot.$settings->email_templates);
$smarty->setCompileDir($docroot.$settings->email_template_c);
$smarty->setCacheDir($docroot.$settings->email_cache);
$smarty->setConfigDir($docroot.$settings->email_configs);
$smarty->use_sub_dirs = true;
$smarty->force_compile = true;
//$smarty->debugging = true;
$smarty->caching = false;


echo "Sending Emails...",EOL;

require_once($docroot.'/resources/PHPMailer/PHPMailerAutoload.php');
$email_result = array();

$mail = new PHPMailer;
$mail->IsSMTP();                                      // Set mailer to use SMTP
$mail->Host = (string)$settings->email_host;  // Specify main and backup server
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = (string)$settings->email_support;    // SMTP username
$mail->Password = (string)$settings->email_password;                           // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted
$mail->From = (string)$settings->email_users;
$mail->FromName = 'Users '.$settings->short_name.' Portal';
$mail->AddReplyTo((string)$settings->email_users, 'Users '.$settings->short_name.' Portal');
$mail->AddCC((string)$settings->email_support);
$mail->IsHTML(true);                                  // Set email format to HTML

foreach($users as $key=>$user){
	$mail->AddAddress($user['email'],$user['name']);
	
	$smarty->assign('company_name',$settings->company_name);
	$smarty->assign('copyright_date',$settings->copyright_date);
	$smarty->assign('email_pics',$settings->email_pics);
	$smarty->assign('firstname',$user['firstname']);
	
	$subject = (string)$settings->company_name.' ';
	if(strtolower($user['default_role']) == '20'){
		$subject .= 'Customer Portal password ';
		$smarty->assign('portal','Customer');
	}else{
		$subject .= 'Service Portal password ';	
		$smarty->assign('portal','Service');
	}
	
	if(intval($user['diff']) < 1){
		$subject .= 'has expired';
		$smarty->assign('recover',(string)$settings->recover_login);
		$mail->Body = $smarty->fetch('password_expired.tpl');
	}else{
		$subject .= 'expiring soon.';
		$smarty->assign('days',$user['diff']);
		$mail->Body = $smarty->fetch('password_expire_soon.tpl');
	}
	
	$mail->Subject = $subject;
	
	if($debug){
		echo EOL,$mail->Body,EOL,"<hr>",EOL;	
	}

	if($send){
		if(!$mail->Send()) {
		   echo 'Email could not be sent.',EOL;
		   $log->logerr($mail->ErrorInfo,1053,false,basename(__FILE__),__LINE__);
		   echo 'Mailer Error: ' . $mail->ErrorInfo,EOL;
		}else{
			$log->loginfo(implode('; ',array_keys($mail->getAllRecipientAddresses())),200,false,basename(__FILE__),__LINE__);
			echo "Email sent to: ",$user['email'],"    User: ",$user['uid'],"   Expire: ",$user['diff'],EOL;
		}
	}else{
		echo "Debug: Not sending email",EOL;	
	}
	
	$mail->ClearAddresses();
}

echo "DONE ".date(storef,time()),EOL;
exit();
?>