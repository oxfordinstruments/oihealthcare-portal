<?php
/**
 * @package OiHealthcarePortal
 * @file pm_contract_fulfill.php
 * @author Evotodi <evotodi@gmail.com>
 * @date 12/05/2016
 * @time 09:32 AM
 */
//
// Error reporting
//
//error_reporting(E_ALL);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
ini_set('display_errors', 'On');

$no_define_eol = true;
if(substr(php_sapi_name(), 0, 3) == 'cli' || empty($_SERVER['REMOTE_ADDR'])) {
	//shell
	$cron = false;
	if(isset($_SERVER['argv'][1]) and $_SERVER['argv'][1] == '--cron'){
		$cron = true;
	}
	$docroot = preg_replace("/(public_html).*/",'public_html',dirname(__FILE__),2);
	$no_define_eol = true;
	define('EOL',PHP_EOL);
} else {
	//webserver
	$cron = false;
	$docroot = $_SERVER['DOCUMENT_ROOT'];
	define('EOL','<br />');
}

$debug = false;
$send = false;
if(isset($_GET['debug'])){
	$debug = true;
	if(isset($_GET['send'])){$send = true;}else{$send = false;}
}else{
	$send = true;
}

$single = false;
if(isset($_GET['single'])){
	$single = true;
	require_once($docroot.'/report/common/session_control.php');
}

$get_year = false;
if(isset($_GET['year']) and $_GET['year'] != ''){
	$get_year = $_GET['year'];
}

$settings = new SimpleXMLElement($docroot.'/settings.xml', null, true);

$no_define_eol = true;
require_once($docroot.'/define_inc.php');

require_once($docroot.'/log/log.php');
$log = new logger();

require_once($docroot.'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

d($_GET);

$log->loginfo("Sending PM Contract Fulfillment emails",0,false,basename(__FILE__),__LINE__);

require_once($docroot.'/resources/moment_php/Moment.php');
require_once($docroot.'/resources/moment_php/MomentException.php');
require_once($docroot.'/resources/moment_php/MomentHelper.php');
require_once($docroot.'/resources/moment_php/MomentLocale.php');
require_once($docroot.'/resources/moment_php/MomentFromVo.php');

require_once($docroot.'/report/common/scripts/php_utils.php');
$php_utils =  new phpUtils();

require_once($docroot.'/resources/smarty/libs/Smarty.class.php');
$smarty = new Smarty();
$smarty->setTemplateDir($docroot.$settings->email_templates);
$smarty->setCompileDir($docroot.$settings->email_template_c);
$smarty->setCacheDir($docroot.$settings->email_cache);
$smarty->setConfigDir($docroot.$settings->email_configs);
$smarty->use_sub_dirs = true;
$smarty->force_compile = true;
//$smarty->debugging = true;
$smarty->caching = false;


if($settings->disable_email == '1' and !$debug){
	$send = false;
}

$php_utils->message('Begin');

require($docroot.'/mysqlInfo.php');
$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno) {die('There was an error running the query [' . $mysqli->connect_error . ']');}

$php_utils->message('Get Possible Dates');

$cur_date = new \Moment\Moment();

if(!$get_year){
	$now = new \Moment\Moment();
}else{
	$now = new \Moment\Moment();
	$now->setYear($get_year);
	$now->endOf('year');
	$now->addDays(60);
}
$now->setImmutableMode(true);

d($now->format());

$newest = $now->cloning();
$newest->setImmutableMode(false);
$newest->startOf('year');
$newest->addDays(29);
d($newest->format());
$oldest = $now->cloning();
$oldest->setImmutableMode(false);
$oldest->startOf('year');
$oldest->subtractYears(1);
$oldest->subtractDays(30);
d($oldest->format());

if($now < $newest){
	$newest->subtractYears(1);
	$newest->setImmutableMode(true);
	$oldest->subtractYears(1);
	$oldest->setImmutableMode(true);
}

$report_year = intval($newest->getYear())-1;

$incomplete = false;
if($cur_date < $newest){
	$incomplete = true;
}
d($incomplete);

$php_utils->message('Possible Dates');
echo "Year: ",$report_year,EOL;
echo "Newest: ",$newest->format(),"  ",$newest->format('U'),EOL;
echo "Oldest: ",$oldest->format(),"  ",$oldest->format('U'),EOL;

$php_utils->message('Getting KPI threshold',true);
$sql="SELECT `year`, percent_pass FROM kpi_pms_contracted WHERE `year` = $report_year;";
if(!$result = $mysqli->query($sql)){
	die('There was an error running the query [' . $mysqli->error . ']');
}
if($result->num_rows == 0){
	if(!$incomplete){
		$php_utils->message('KPI threshold not in DB so adding it',true);
		$sql="INSERT INTO kpi_pms_contracted (`year`, percent_pass) VALUES ('".$report_year."', '".$settings->pm->contract_fulfillment_base."');";
		if(!$result = $mysqli->query($sql)){
			die('There was an error running the query [' . $mysqli->error . ']');
		}
	}
	$kpi_threshold = intval($settings->pm->contract_fulfillment_base);
}else{
	$row = $result->fetch_assoc();
	$kpi_threshold = intval($row['percent_pass']);
}
echo "KPI Threshold: ",$kpi_threshold,"%",EOL;

$php_utils->message('Getting Systems',true);
$sql="SELECT sbc.system_id, sbc.nickname, sbc.ver_unique_id, sbc.property, mc.`type`, sbc.contract_end_date, sbc.date_installed, sb.pm_freq, sb.pm_start, sb.pm_dates, sbc.archived_date
FROM systems_base_cont AS sbc
LEFT JOIN systems_base AS sb ON sb.unique_id = sbc.unique_id
LEFT JOIN misc_contracts AS mc ON mc.id = sbc.contract_type
WHERE sbc.property != 'f' 
AND sbc.property != 'd' 
AND mc.pm_contract_calc = 'y' 
AND STR_TO_DATE(sbc.date_installed,'".$settings->mysql_store_datetime."') <= STR_TO_DATE('".$newest->format()."','".$settings->mysql_moment_datetime."')
AND sb.pm_freq > 0;";
s($sql);
if(!$result = $mysqli->query($sql)){
	die('There was an error running the query [' . $mysqli->error . ']');
}
$systems = array();
$systems_with_errors = array();
while($row = $result->fetch_assoc()){
	$error = array();
	$num_pms_expected = null;

	$pm_dates = json_decode($row['pm_dates']);
	if(json_last_error() != JSON_ERROR_NONE){
		$pm_dates = false;
	}
	if(count($pm_dates) != 12/intval($row['pm_freq'])){
		$pm_dates = false;
	}

	if($row['pm_start'] == ''){
		array_push($error,'PM Start Not Valid');
	}

	try{
		if($row['date_installed'] == ''){
			throw new \Moment\MomentException();
		}
		$date_installed = new \Moment\Moment($row['date_installed'], 'UTC');
	}catch(\Moment\MomentException $e){
		$date_installed = false;
		array_push($error,'Date Installed Not Valid');
		//s('ERROR: '.$row['system_id'].' = Date Installed Not Valid');
	}

	if($date_installed != false and $date_installed->isBetween($oldest, $newest)){
		if($pm_dates != false and count($pm_dates) > 0){
			//s("FOUND ONE ".$row['system_id']." = ".$row['date_installed']);
			foreach($pm_dates as $key=>$val){
				if($date_installed->isAfter($report_year.'-'.sprintf('%02d', $val[0]).'-'.sprintf('%02d', $val[1]))){
					unset($pm_dates[$key]);
				}
			}
		}else{
			array_push($error,'PM Dates Not Valid');
			//s('ERROR: '.$row['system_id'].' = PM Dates Not Valid');
		}
	}else{
		$num_pms_expected = 12/intval($row['pm_freq']);
	}

	switch(strtolower($row['property'])){
		case 'a':
			$property = "  (archived)";
			break;
		case 'f':
			$property = "  (future)";
			break;
		default:
			$property = '';
	}

	$archived_date = false;
	if($row['archived_date'] != ''){
		try{
			$archived_date = new \Moment\Moment($row['archived_date'], 'UTC');
			if($archived_date->isBefore($oldest)){
				echo $row['system_id']." archived (".$archived_date->format().") before ".$oldest->format()." so rejecting system data",EOL;
				continue;
			}
		}
		catch(\Moment\MomentException $e){
			array_push($error,'Archived Date Not Valid');
			$archived_date = false;
		}
	}

	if(count($error) > 0){
		$systems_with_errors[$row['ver_unique_id']] = array('system_id'=>$row['system_id'].$property, 'nickname'=>$row['nickname'], 'errors'=>$error);
	}



	$systems[$row['system_id'].$property] = array(
		'nickname'=>$row['nickname'],
		'property'=>$row['property'],
		'ver_unique_id'=>$row['ver_unique_id'],
		'contract_type'=>$row['type'],
		'contract_end_date'=>strtotime($row['contract_end_date']),
		'date_installed'=>strtotime($row['date_installed']),
		'pm_freq'=>$row['pm_freq'],
		'pm_start'=>$row['pm_start'],
		'pm_dates'=>$pm_dates,
		'num_pms_expected'=>$num_pms_expected,
		'num_pms_done'=>null,
		'kpi_pass'=>true,
		'error'=>$error,
	);
}
$php_utils->customUasort($systems_with_errors,'system_id');
d($systems);
d($systems_with_errors);

$php_utils->message('Getting completed PMs for each system');
$failed_kpis = array();
$passed_kpis = array();
foreach($systems as $key=>&$system){
	$sql="SELECT srp.id
	FROM systems_reports AS srp
	WHERE srp.system_ver_unique_id = '".$system['ver_unique_id']."' 
	AND srp.pm = 'y' 
	AND UNIX_TIMESTAMP(STR_TO_DATE(srp.pm_date, '".$settings->mysql_store_datetime."')) >= ".$oldest->format('U')." 
	AND UNIX_TIMESTAMP(STR_TO_DATE(srp.pm_date, '".$settings->mysql_store_datetime."')) <= ".$newest->format('U').";";
	if(!$result = $mysqli->query($sql)){
		die('There was an error running the query [' . $mysqli->error . ']');
	}
	$system['num_pms_done'] = $result->num_rows;
	if($system['num_pms_expected'] == null){
		$system['num_pms_expected'] = count($system['pm_dates']);
	}

	if(intval($system['num_pms_done']) < intval($system['num_pms_expected']) and count($system['error']) == 0){
		$system['kpi_pass'] = false;
		$failed_kpis[$system['ver_unique_id']] = array(
			'system_id'=>$key,
			'nickname'=>$system['nickname'],
			'contract'=>$system['contract_type'],
			'expected'=>$system['num_pms_expected'],
			'completed'=>$system['num_pms_done'],
			'installed'=>$system['date_installed'],
			'pm_freq'=>$system['pm_freq'],
			'under_half'=>false
		);
		$completed_percent = (intval($system['num_pms_done']) / intval($system['num_pms_expected'])) * 100;
		if($completed_percent < 50){
			$failed_kpis[$system['ver_unique_id']]['under_half'] = true;
		}
	}else{
		$passed_kpis[$system['ver_unique_id']] = array(
			'system_id'=>$key,
			'nickname'=>$system['nickname'],
			'contract'=>$system['contract_type'],
			'expected'=>$system['num_pms_expected'],
			'completed'=>$system['num_pms_done'],
			'installed'=>$system['date_installed'],
			'pm_freq'=>$system['pm_freq'],
			'too_many'=>false
		);
		$completed_percent = abs(intval($system['num_pms_expected']) - intval($system['num_pms_done']));
		if($completed_percent > 0){
			$passed_kpis[$system['ver_unique_id']]['too_many'] = true;
		}
	}
}
$php_utils->customUasort($failed_kpis,'system_id');
$php_utils->customUasort($passed_kpis,'system_id');

d($failed_kpis);
d($passed_kpis);
d($systems);

$php_utils->message('Calculate KPI');
$systems_measured = count($systems) - count($systems_with_errors);
$kpi_percent = round(((($systems_measured - count($failed_kpis))/$systems_measured)*100), 2);
echo "KPI Percent: ". $kpi_percent . "%",EOL;
$kpi_status = "Passed";
if($kpi_percent < $kpi_threshold){
	$kpi_status = "Failed";
}
echo "KPI ".$kpi_status,EOL;

$php_utils->message('Getting recipients');
$emails = array();
if(!$single){
	$sql = "SELECT u.name,u.email
FROM users AS u
INNER JOIN users_prefs AS upr ON upr.uid = u.uid
INNER JOIN users_pref_id AS prid ON prid.id = upr.pid AND prid.pref = 'pref_rcv_cont_met'
INNER JOIN users_perms AS up ON up.uid = u.uid
INNER JOIN users_perm_id AS pmid ON pmid.id = up.pid AND pmid.perm = 'perm_rcv_cont_met'
WHERE u.active = 'y';";
	if(!$resultEmails = $mysqli->query($sql)){
		die('There was an error running the query [' . $mysqli->error . ']');
	}

	while($rowEmail = $resultEmails->fetch_assoc()){
		array_push($emails, array("email" => $rowEmail['email'], "name" => $rowEmail['name']));
	}
}else{
	array_push($emails,array("email"=>$_SESSION['userdata']['email'],"name"=>$_SESSION['userdata']['name']));
}
d($emails);



$php_utils->message('Filling In Template');

$smarty->assign('date',date(storef,time()));
$smarty->assign('company_name',$settings->company_name);
$smarty->assign('copyright_date',$settings->copyright_date);
$smarty->assign('email_pics',$settings->email_pics);

if(count($systems_with_errors) > 0){
	$smarty->assign('missing',true);
}
if($incomplete){
	$smarty->assign('incomplete',true);
}

$smarty->assign('report_year', $report_year);
$smarty->assign('kpi_status', $kpi_status);
$smarty->assign('kpi_percent', $kpi_percent);
$smarty->assign('kpi_threshold', $kpi_threshold);
$smarty->assign('total_systems', $systems_measured);
$smarty->assign('kpi_passed', $systems_measured - count($failed_kpis));
$smarty->assign('kpi_failed', count($failed_kpis));
$smarty->assign('systems_with_errors', count($systems_with_errors));
$smarty->assign('failed', $failed_kpis);
$smarty->assign('errors', $systems_with_errors);
$smarty->assign('passed', $passed_kpis);



$php_utils->message('Creating Email');
require_once($docroot.'/resources/PHPMailer/PHPMailerAutoload.php');

$email_result = array();

$mail = new PHPMailer;
$mail->IsSMTP();                                      // Set mailer to use SMTP
$mail->Host = (string)$settings->email_host;  // Specify main and backup server
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = (string)$settings->email_support;    // SMTP username
$mail->Password = (string)$settings->email_password;                           // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted
$mail->From = (string)$settings->email_support;
$mail->FromName = 'Support '.$settings->short_name.' Portal';
if($debug){
	$mail->AddAddress('justin.davis@oxinst.com','Justin Davis');
	array_push($email_result,"justin.davis@oxinst.com");
}else{
	foreach($emails as $email){
		$mail->AddAddress(trim($email['email']),$email['name']);  // Add a recipient
		array_push($email_result,$email['email']);
	}
}
$mail->AddReplyTo((string)$settings->email_support, 'Support '.$settings->short_name.' Portal');
$mail->AddCC((string)$settings->email_support);
$mail->IsHTML(true);                                  // Set email format to HTML
$mail->Subject = 'Contracted PMs Fulfillment';
$mail->Body = $smarty->fetch('pm_contract_fulfill.tpl');

if($debug){
	echo $mail->Body,EOL;
}

$php_utils->message('Sending Emails');
if($send){
	if(!$mail->Send()) {
		echo 'Email could not be sent.',EOL;
		echo 'Mailer Error: ' . $mail->ErrorInfo,EOL;
		$log->logerr($mail->ErrorInfo,1053,false,basename(__FILE__),__LINE__);
		exit;
	}
}else{
	echo "Debug: Not sending email",EOL;
}


$php_utils->message("Email addresses Sent To");
foreach($email_result as $value){
	echo $value,EOL;
	$log->loginfo("Sent to: ".$value,0,false,basename(__FILE__),__LINE__);
}

$php_utils->message('End',true);
?>