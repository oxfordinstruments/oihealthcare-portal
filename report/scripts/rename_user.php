<?php

//

ob_start();//Start output buffering

$debug = false;
if(isset($_GET['debug'])){
	$debug = true;	
}

$fromuid = NULL;
if(!isset($_GET['fromuid'])){
	die('GET fromuid not set');
}else{
	$fromuid = $_GET['fromuid'];
}

$touid = NULL;
if(!isset($_GET['touid'])){
	die('GET touid not set');
}else{
	$touid = strtolower($_GET['touid']);
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/define_inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}
d($_GET);

require($_SERVER['DOCUMENT_ROOT'].'/mysqlInfo.php');
$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno) {die('There was an error running the query [' . $mysqli->connect_error . ']');}

echo "<h2>Checking UIDs</h2>";
$sql="SELECT uid FROM users WHERE uid = '$fromuid';";
if(!$result = $mysqli->query($sql)){
	die($mysqli->error);	
}
d($result->num_rows);
if($result->num_rows != 1){
	die("From UID not found");
}else{
	echo "From UID valid",EOL;
}

$sql="SELECT uid FROM users WHERE uid = '$touid';";
if(!$result = $mysqli->query($sql)){
	die($mysqli->error);	
}
if($result->num_rows != 0){
	die("To UID already taken");
}else{
	echo "To UID valid",EOL;
}

$sqls = array(
	"SET FOREIGN_KEY_CHECKS=0;",
	"UPDATE users SET uid = '$touid' WHERE uid = '$fromuid';",
	"UPDATE users SET manager = '$touid' WHERE manager = '$fromuid';",
	"UPDATE users SET created_by = '$touid' WHERE created_by = '$fromuid';",
	"UPDATE users SET edited_by = '$touid' WHERE edited_by = '$fromuid';",
	"UPDATE users_perms SET uid = '$touid' WHERE uid = '$fromuid';",
	"UPDATE users_prefs SET uid = '$touid' WHERE uid = '$fromuid';",
	"UPDATE users_groups SET uid = '$touid' WHERE uid = '$fromuid';",
	"UPDATE users_roles SET uid = '$touid' WHERE uid = '$fromuid';",
	"UPDATE users_quals SET uid = '$touid' WHERE uid = '$fromuid';",
	"UPDATE systems_assigned_customer SET uid = '$touid' WHERE uid = '$fromuid';",
	"UPDATE systems_assigned_pri SET uid = '$touid' WHERE uid = '$fromuid';",
	"UPDATE systems_assigned_sec SET uid = '$touid' WHERE uid = '$fromuid';",
	"UPDATE systems_base SET edited_by = '$touid' WHERE edited_by = '$fromuid';",
	"UPDATE systems_base SET created_by = '$touid' WHERE created_by = '$fromuid';",
	"UPDATE systems_base_cont SET edited_by = '$touid' WHERE edited_by = '$fromuid';",
	"UPDATE systems_base_cont SET created_by = '$touid' WHERE created_by = '$fromuid';",
	"UPDATE systems_base_cont SET archived_by = '$touid' WHERE archived_by = '$fromuid';",
	"UPDATE systems_base_cont SET deleted_by = '$touid' WHERE deleted_by = '$fromuid';",
	"UPDATE systems_face_sheet SET quickbooks_user = '$touid' WHERE quickbooks_user = '$fromuid';",
	"UPDATE systems_face_sheet SET logbook_user = '$touid' WHERE logbook_user = '$fromuid';",
	"UPDATE systems_face_sheet SET afterhours_user = '$touid' WHERE afterhours_user = '$fromuid';",
	"UPDATE systems_face_sheet SET idsticker_user = '$touid' WHERE idsticker_user = '$fromuid';",
	"UPDATE systems_face_sheet SET basket_user = '$touid' WHERE basket_user = '$fromuid';",
	"UPDATE systems_face_sheet SET survey_user = '$touid' WHERE survey_user = '$fromuid';",
	"UPDATE systems_files SET uid = '$touid' WHERE uid = '$fromuid';",
	"UPDATE systems_mri_readings SET uid = '$touid' WHERE uid = '$fromuid';",
	"UPDATE systems_mri_readings SET edited_by = '$touid' WHERE edited_by = '$fromuid';",
	"UPDATE systems_notes SET uid = '$touid' WHERE uid = '$fromuid';",
	"UPDATE systems_reports SET user_id = '$touid' WHERE user_id = '$fromuid';",
	"UPDATE systems_reports SET engineer = '$touid' WHERE engineer = '$fromuid';",
	"UPDATE systems_reports SET assigned_engineer = '$touid' WHERE assigned_engineer = '$fromuid';",
	"UPDATE systems_reports SET report_edited_by = '$touid' WHERE report_edited_by = '$fromuid';",
	"UPDATE systems_reports SET valuation_uid = '$touid' WHERE valuation_uid = '$fromuid';",
	"UPDATE systems_reports SET invoiced_uid = '$touid' WHERE invoiced_uid = '$fromuid';",
	"UPDATE systems_reports SET invoiced_uid_edit = '$touid' WHERE invoiced_uid_edit = '$fromuid';",
	"UPDATE systems_reports SET deleted_by = '$touid' WHERE deleted_by = '$fromuid';",
	"UPDATE systems_reports_files SET uid = '$touid' WHERE uid = '$fromuid';",
	"UPDATE systems_reports_notes SET uid = '$touid' WHERE uid = '$fromuid';",
	"UPDATE systems_requests SET engineer = '$touid' WHERE engineer = '$fromuid';",
	"UPDATE systems_requests SET sender = '$touid' WHERE sender = '$fromuid';",
	"UPDATE systems_requests SET edited_by = '$touid' WHERE edited_by = '$fromuid';",
	"UPDATE systems_requests SET deleted_by = '$touid' WHERE deleted_by = '$fromuid';",
	"UPDATE systems_requests_notes SET uid = '$touid' WHERE uid = '$fromuid';",
	"UPDATE systems_status_updates SET uid = '$touid' WHERE uid = '$fromuid';",
	"UPDATE tools SET uid = '$touid' WHERE uid = '$fromuid';",
	"UPDATE tools SET created_by = '$touid' WHERE created_by = '$fromuid';",
	"UPDATE tools SET edited_by = '$touid' WHERE edited_by = '$fromuid';",
	"UPDATE tools_calibrations SET input_by = '$touid' WHERE input_by = '$fromuid';",
	"UPDATE trailers SET edited_by = '$touid' WHERE edited_by = '$fromuid';",
	"UPDATE trailers SET created_by = '$touid' WHERE created_by = '$fromuid';",
	"UPDATE trailers_files SET uid = '$touid' WHERE uid = '$fromuid';",
	"UPDATE users_calendar SET uid = '$touid' WHERE uid = '$fromuid';",
	"SET FOREIGN_KEY_CHECKS=1;"
);
d($sqls);
foreach($sqls as $sql){
	echo $sql,EOL;
	if(!$result = $mysqli->query($sql)){
		die($mysqli->error);
	}
}

echo "<h2>Done</h2>";
?>

	
	
	
	
	
	
	
	
	
