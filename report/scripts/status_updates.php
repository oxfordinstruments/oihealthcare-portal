<?php
$no_define_eol = true;
if(substr(php_sapi_name(), 0, 3) == 'cli' || empty($_SERVER['REMOTE_ADDR'])) {
    //shell
	$cron = false;
	if(isset($_SERVER['argv'][1]) and $_SERVER['argv'][1] == '--cron'){
		$cron = true;
	}
	$docroot = preg_replace("/(public_html).*/",'public_html',dirname(__FILE__),2);
	$no_define_eol = true;
	define('EOL',PHP_EOL);
} else {
    //webserver
	$cron = false;
	$docroot = $_SERVER['DOCUMENT_ROOT'];
	define('EOL','<br />');
}

$debug = false;
if(isset($_GET['debug'])){
	$debug = true;
	if(isset($_GET['send'])){$send = true;}else{$send = false;}
}else{
	$send = true;
}

$settings = new SimpleXMLElement($docroot.'/settings.xml', null, true);
require_once($docroot.'/define_inc.php');

echo "Begin system status updates ".date(phpdispfdt,time()),EOL;

if($settings->disable_email == '1'){
	$send = false;	
}

require_once($docroot.'/resources/smarty/libs/Smarty.class.php');
$smarty = new Smarty();
$smarty->setTemplateDir($docroot.$settings->email_templates);
$smarty->setCompileDir($docroot.$settings->email_template_c);
$smarty->setCacheDir($docroot.$settings->email_cache);
$smarty->setConfigDir($docroot.$settings->email_configs);
$smarty->use_sub_dirs = true;
$smarty->force_compile = true;
//$smarty->debugging = true;
$smarty->caching = false;

require($docroot.'/mysqlInfo.php');
$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno) {die('There was an error running the query [' . $mysqli->connect_error . ']');}

echo "Getting Recipients",EOL;
$sql = "SELECT u.name,u.email
FROM users AS u
INNER JOIN users_prefs AS upr ON upr.uid = u.uid
INNER JOIN users_pref_id AS prid ON prid.id = upr.pid AND prid.pref = 'pref_rcv_status_updates'
INNER JOIN users_perms AS up ON up.uid = u.uid
INNER JOIN users_perm_id AS pmid ON pmid.id = up.pid AND pmid.perm = 'perm_rcv_status_updates'
WHERE u.active = 'y';";
if(!$resultEmails = $mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']');}

echo "Getting updates",EOL;
$sql = "SELECT sb.system_id, u.name, e.`status`, e.`date`, sbc.nickname, CASE WHEN r.report_id IS NULL THEN 'NA' ELSE r.report_id END AS report_id
FROM systems_status_updates AS e
LEFT JOIN systems_reports AS r ON e.report_unique_id = r.unique_id
LEFT JOIN systems_base AS sb ON sb.unique_id = e.system_unique_id
LEFT JOIN systems_base_cont AS sbc ON sb.unique_id = sbc.unique_id
LEFT JOIN users AS u ON u.uid = e.uid
WHERE DATE(e.`date`) = DATE(DATE_SUB(NOW(), INTERVAL 1 DAY));";
if(!$resultUpdates = $mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']');}

if($resultUpdates->num_rows == 0){
	echo "No Updates Yesterday ".date(phpdispfdt,time()),EOL;	
	echo "DONE ".date(phpdispfdt,time()),EOL;
	exit();
}

$rows_arr = array();
$update_date = "";

while($rowUpdates = $resultUpdates->fetch_assoc()){
	$update_date = explode(' ',$rowUpdates['email_date']);
	array_push($rows_arr,array( 'system_id'=>$rowUpdates['system_id'], 'nickname'=>$rowUpdates['nickname'], 'report_id'=>$rowUpdates['report_id'], 'name'=>$rowUpdates['name'], 'status'=>$rowUpdates['status'], 'status_date'=>$rowUpdates['date']));		
}
$smarty->assign('status_date',$update_date);
$smarty->assign('rows',$rows_arr);
$smarty->assign('company_name',$settings->company_name);
$smarty->assign('copyright_date',$settings->copyright_date);
$smarty->assign('email_pics',$settings->email_pics);

if($debug){
	echo "<pre>",print_r($rows_arr),"</pre>",EOL,EOL;
	$smarty->display('status_updates.tpl');
}

echo "Sending System Status Updates Emails...",EOL;

require_once($docroot.'/resources/PHPMailer/PHPMailerAutoload.php');

$email_result = array();

$mail = new PHPMailer;
$mail->IsSMTP();
$mail->Host = (string)$settings->email_host;
$mail->SMTPAuth = true;
$mail->Username = (string)$settings->email_updates;
$mail->Password = (string)$settings->email_password;
$mail->SMTPSecure = 'tls';
$mail->From = (string)$settings->email_updates;
$mail->FromName = 'System Status Updates '.$settings->short_name.' Portal';
while($rowEmails = $resultEmails->fetch_assoc()) {
	$mail->AddAddress(trim($rowEmails['email']),$rowEmails['name']);
	array_push($email_result,$rowEmails['email']);
}
$mail->AddReplyTo((string)$settings->email_support, 'Support '.$settings->short_name.' Portal');
$mail->AddCC($settings->email_suport);
$mail->IsHTML(true);
$mail->Subject = 'System Status Updates for '.$update_date[0];
$mail->Body = $smarty->fetch('status_updates.tpl');
if($send){
	if(!$mail->Send()) {
	   echo 'Email could not be sent.',EOL;
	   //$email_result = "Email could not be sent!";
	   echo 'Mailer Error: ' . $mail->ErrorInfo,EOL;
	   exit;
	}
}else{
	echo "Debug: Not sending email",EOL;	
}


echo "Email addresses sent to:",EOL;
foreach($email_result as $value){
	echo $value,EOL;
}

$mysqli->close();


echo "DONE ".date(phpdispfdt,time()),EOL;
exit();
?>