<?php

$no_define_eol = true;
if(substr(php_sapi_name(), 0, 3) == 'cli' || empty($_SERVER['REMOTE_ADDR'])) {
    //shell
	$cron = false;
	if(isset($_SERVER['argv'][1]) and $_SERVER['argv'][1] == '--cron'){
		$cron = true;
	}
	$docroot = preg_replace("/(public_html).*/",'public_html',dirname(__FILE__),2);
	$no_define_eol = true;
	define('EOL',PHP_EOL);
} else {
    //webserver
	$cron = false;
	$docroot = $_SERVER['DOCUMENT_ROOT'];
	define('EOL','<br />');
}

$debug = false;
if(isset($_GET['debug'])){
	$debug = true;
	if(isset($_GET['send'])){$send = true;}else{$send = false;}
}else{
	$send = true;
}

$single = false;
if(isset($_GET['single'])){
	$single = true;
	require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');
}

$settings = new SimpleXMLElement($docroot.'/settings.xml', null, true);

$no_define_eol = true;
require_once($docroot.'/define_inc.php');

require_once($docroot.'/log/log.php');
$log = new logger();

$log->loginfo("Sending Systems Down emails",0,false,basename(__FILE__),__LINE__);

echo "Begin Systems Down Check ".date(phpdispfdt,time()),EOL;

if($settings->disable_email == '1'){
	$send = false;	
}

require_once($docroot.'/report/common/scripts/php_utils.php');
$php_utils = new phpUtils();

require_once($docroot.'/resources/smarty/libs/Smarty.class.php');
$smarty = new Smarty();
$smarty->setTemplateDir($docroot.$settings->email_templates);
$smarty->setCompileDir($docroot.$settings->email_template_c);
$smarty->setCacheDir($docroot.$settings->email_cache);
$smarty->setConfigDir($docroot.$settings->email_configs);
$smarty->use_sub_dirs = true;
$smarty->force_compile = true;
//$smarty->debugging = true;
$smarty->caching = false;

require($docroot.'/mysqlInfo.php');
$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno) {die('There was an error running the query [' . $mysqli->connect_error . ']');}

echo "Getting recipients",EOL;
$emails = array();

if(!$single){
	$sql = "SELECT u.name,u.email
	FROM users AS u
	INNER JOIN users_prefs AS upr ON upr.uid = u.uid
	INNER JOIN users_pref_id AS prid ON prid.id = upr.pid AND prid.pref = 'pref_rcv_sysdown'
	INNER JOIN users_perms AS up ON up.uid = u.uid
	INNER JOIN users_perm_id AS pmid ON pmid.id = up.pid AND pmid.perm = 'perm_rcv_sysdown'
	WHERE u.active = 'y';";
	if(!$resultEmails = $mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']');}
	
	while($rowEmail = $resultEmails->fetch_assoc()){
		array_push($emails,array("email"=>$rowEmail['email'],"name"=>$rowEmail['name']));
	}
}else{
	array_push($emails,array("email"=>$_SESSION['userdata']['email'],"name"=>$_SESSION['userdata']['name']));
}

echo "Getting Systems Down",EOL;
$sql = "SELECT srq.request_num, srq.system_id, srq.system_nickname, ss1.`status` AS request_status, ss2.`status` AS report_status, srq.initial_call_date, 
u1.name AS engineer, u2.name AS sender, srq.problem_reported, IF(srp.report_id IS NOT NULL,1,0) AS report_started, 
TIMESTAMPDIFF(HOUR, TIMESTAMP(srq.initial_call_date), NOW()) AS diff_hours,
TIMESTAMPDIFF(DAY, TIMESTAMP(srq.initial_call_date), NOW()) AS diff_days,
TIMESTAMPDIFF(MONTH, TIMESTAMP(srq.initial_call_date), NOW()) AS diff_months
FROM systems_requests AS srq
LEFT JOIN systems_reports AS srp ON srp.report_id = srq.request_num
LEFT JOIN systems_status AS ss1 ON ss1.id = srq.system_status
LEFT JOIN systems_status AS ss2 ON ss2.id = srp.system_status
LEFT JOIN users AS u1 ON u1.uid = srq.engineer
LEFT JOIN users AS u2 ON u2.uid = srq.sender
WHERE srq.`status` = 'open' 
AND srq.deleted = 'N' 
AND IF( IF(ss2.`status` IS NOT NULL, 1, 0) = 1, ss2.priority >= 8, ss1.priority >= 8) 
AND TIMESTAMPDIFF(HOUR, TIMESTAMP(srq.initial_call_date), NOW()) >= 24
ORDER BY diff_hours DESC;";
if(!$resultSystems = $mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']');}

if($resultSystems->num_rows == 0){
	echo "No systems down",EOL;	
	echo "DONE ".date(phpdispfdt,time()),EOL;
	exit();
}

$data = array();
while($rowSystems = $resultSystems->fetch_assoc()) {
	echo "System ID: ".$rowSystems['system_id']." Hours Down: ".$rowSystems['diff_hours'],EOL;
		
	$initial_call_date = empty($rowSystems['initial_call_date'])?'':date(phpdispfdt,strtotime($rowSystems['initial_call_date']));
	$report_started = 'No';
	if($rowSystems['report_started'] == '1'){
		$report_started = 'Yes';	
	}
	
	
	$intv = $php_utils->getInterval($rowSystems['diff_hours'] * 60 * 60);
	$diff = "Years: ".$intv['y']."<br>Months: ".$intv['m']."<br>Days: ".$intv['d']."<br>Hours: ".$intv['h'];
	
	$data[$rowSystems['request_num']] = array('system_id'=>$rowSystems['system_id'],
											  'system_nickname'=>$rowSystems['system_nickname'],
											  'initial_call_date'=>$initial_call_date,
											  'engineer'=>$rowSystems['engineer'],
											  'sender'=>$rowSystems['sender'],
											  'problem_reported'=>$php_utils->limit_words($rowSystems['problem_reported'],30),
											  'report_started'=>$report_started,
											  'diff'=>$diff);
}

$smarty->assign('date',date(storef,time()));
$smarty->assign('data',$data);
$smarty->assign('company_name',$settings->company_name);
$smarty->assign('copyright_date',$settings->copyright_date);
$smarty->assign('email_pics',$settings->email_pics);

echo "Sending systems down email...",EOL;


require_once($docroot.'/resources/PHPMailer/PHPMailerAutoload.php');

$email_result = array();

$mail = new PHPMailer;
$mail->IsSMTP();                                      // Set mailer to use SMTP
$mail->Host = (string)$settings->email_host;  // Specify main and backup server
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = (string)$settings->email_support;    // SMTP username
$mail->Password = (string)$settings->email_password;                           // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted
$mail->From = (string)$settings->email_support;
$mail->FromName = 'Support '.$settings->short_name.' Portal';
if($debug){
	$mail->AddAddress('justin.davis@oxinst.com','Justin Davis');
	array_push($email_result,"justin.davis@oxinst.com");
}else{
	foreach($emails as $email){
		$mail->AddAddress(trim($email['email']),$email['name']);  // Add a recipient
		array_push($email_result,$email['email']);
	}
}
$mail->AddReplyTo((string)$settings->email_support, 'Support '.$settings->short_name.' Portal');
if(!$single){
	$mail->AddCC((string)$settings->email_support);
}
$mail->IsHTML(true);                                  // Set email format to HTML
$mail->Subject = 'Systems Down Report';
$mail->Body = $smarty->fetch('systems_down.tpl');

if($debug){
	echo EOL,"<pre>",print_r($data),"</pre>",EOL;
	echo $mail->Body,EOL;
}

if($send){
	if(!$mail->Send()) {
	   echo 'Email could not be sent.',EOL;
	   //$email_result = "Email could not be sent!";
	   echo 'Mailer Error: ' . $mail->ErrorInfo,EOL;
		$log->logerr($mail->ErrorInfo,1053,false,basename(__FILE__),__LINE__);
	   exit;
	}
}else{
	echo "Debug: Not sending email",EOL;	
}


echo "Email addresses sent to:",EOL;
//echo $email_result;
foreach($email_result as $value){
	echo $value,EOL;
	$log->loginfo("Sent to: ".$value,0,false,basename(__FILE__),__LINE__);
}

$mysqli->close();


echo "DONE ".date(phpdispfdt,time()),EOL;
exit();
?>