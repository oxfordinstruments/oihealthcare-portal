<?php
/**
 * @package OiHealthcarePortal
 * @file template_view.php
 * @author Evotodi <evotodi@gmail.com>
 * @date 11/10/2016
 * @time 10:43 AM
 */

error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
ini_set('display_errors', 'On');

$debug = false;
if(isset($_GET['debug'])){
	$debug = true;
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/php_utils.php');
$php_utils =  new phpUtils();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

if(!isset($_GET['template'])){
	die("NO TEMPLATE SPECIFIED");
}



$template = $_GET['template'];

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/smarty/libs/Smarty.class.php');
$smarty = new Smarty();
$smarty->setTemplateDir($_SERVER['DOCUMENT_ROOT'].$settings->email_templates);
$smarty->setCompileDir($_SERVER['DOCUMENT_ROOT'].$settings->email_template_c);
$smarty->setCacheDir($_SERVER['DOCUMENT_ROOT'].$settings->email_cache);
$smarty->setConfigDir($_SERVER['DOCUMENT_ROOT'].$settings->email_configs);
$smarty->use_sub_dirs = true;
$smarty->force_compile = true;
//$smarty->debugging = $debug;
$smarty->caching = false;


$smarty->assign('company_name',$settings->company_name);
$smarty->assign('copyright_date',$settings->copyright_date);
$smarty->assign('email_pics',$settings->email_pics);

$smarty->display($template);

if($debug){
	echo "<br><br><hr>";
	$php_utils->message('Available Smarty Vars');
	d(getSmartyVars(file_get_contents($_SERVER['DOCUMENT_ROOT'] . $settings->email_templates . '/' . $template)));

	echo "<br><br><hr>";
	$php_utils->message('Used Smarty Vars');
	d($smarty->tpl_vars);
}

function getSmartyVars($string){
	// regexp
	$fullPattern = '`{[^\\$]*\\$([a-zA-Z0-9]+)[^\\}]*}`';
	$separateVars = '`[^\\$]*\\$([a-zA-Z0-9]+)`';

	$smartyVars = array();
	// We start by extracting all the {} with var embedded
	if(!preg_match_all($fullPattern, $string, $results)){
		return $smartyVars;
	}
	// Then we extract all smarty variables
	foreach($results[0] AS $result){
		if(preg_match_all($separateVars, $result, $matches)){
			$smartyVars = array_merge($smartyVars, $matches[1]);
		}
	}
	return array_unique($smartyVars);
}
?>