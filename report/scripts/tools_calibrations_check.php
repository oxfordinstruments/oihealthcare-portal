<?php

$no_define_eol = true;
if(substr(php_sapi_name(), 0, 3) == 'cli' || empty($_SERVER['REMOTE_ADDR'])) {
    //shell
	$cron = false;
	if(isset($_SERVER['argv'][1]) and $_SERVER['argv'][1] == '--cron'){
		$cron = true;
	}
	$docroot = preg_replace("/(public_html).*/",'public_html',dirname(__FILE__),2);
	$no_define_eol = true;
	define('EOL',PHP_EOL);
} else {
    //webserver
	$cron = false;
	$docroot = $_SERVER['DOCUMENT_ROOT'];
	define('EOL','<br />');
}

$debug = false;
if(isset($_GET['debug'])){
	$debug = true;
	if(isset($_GET['send'])){$send = true;}else{$send = false;}
}else{
	$send = true;
}



$settings = new SimpleXMLElement($docroot.'/settings.xml', null, true);

$no_define_eol = true;
require_once($docroot.'/define_inc.php');

echo "Begin Tools Calibration Check ".date(phpdispfdt,time()),EOL;

if($settings->disable_email == '1'){
	$send = false;	
}

require_once($docroot.'/resources/smarty/libs/Smarty.class.php');
$smarty = new Smarty();
$smarty->setTemplateDir($docroot.$settings->email_templates);
$smarty->setCompileDir($docroot.$settings->email_template_c);
$smarty->setCacheDir($docroot.$settings->email_cache);
$smarty->setConfigDir($docroot.$settings->email_configs);
$smarty->use_sub_dirs = true;
$smarty->force_compile = true;
//$smarty->debugging = true;
$smarty->caching = false;

require($docroot.'/mysqlInfo.php');
$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno) {die('There was an error running the query [' . $mysqli->connect_error . ']');}

echo "Get offices",EOL;
$offices = array();
$sql="SELECT id, office FROM misc_offices;";
if(!$result = $mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']');}
while($row = $result->fetch_assoc()){
	$offices[$row['id']] = array('office'=>$row['office']);	
}

echo "Getting recipients",EOL;
$tools_emails = array();
$sql="SELECT u.name, u.email
FROM users AS u
LEFT JOIN users_perms AS perm ON perm.uid = u.uid
LEFT JOIN users_perm_id AS permid ON permid.id = perm.pid
LEFT JOIN users_prefs AS pref ON pref.uid = u.uid
LEFT JOIN users_pref_id AS prefid ON prefid.id = pref.pid
WHERE (permid.perm = 'perm_tool_cal_edit' OR permid.perm = 'perm_tool_cal_view') AND u.active = 'y';";	
if(!$resultToolsEmail=$mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']');}
while($rowToolsEmail = $resultToolsEmail->fetch_assoc()){
	array_push($tools_emails,array("email"=>$rowToolsEmail['email'],"name"=>$rowToolsEmail['name']));
}

echo "Getting tools needing calibrated",EOL;
$sql = "SELECT t.id, t.calibrating, t.tool, t.model, t.serial, t.mfg, t.location, t.office, u.name, t.notes, tc.date, tc.expire, tc.calibrated_by, tc.notes AS cal_notes
FROM tools AS t
LEFT JOIN misc_offices AS mo ON mo.id = t.office
LEFT JOIN users AS u ON u.uid = t.uid
LEFT JOIN tools_calibrations AS tc ON tc.unique_id = t.unique_id
AND tc.id = (SELECT MAX(tc2.id) FROM tools_calibrations AS tc2 WHERE tc2.unique_id = t.unique_id)
WHERE t.active = 'y'
AND STR_TO_DATE(tc.expire,'%Y-%m-%dT%H:%i:%sZ') <= LAST_DAY(NOW() + INTERVAL 1 MONTH);";
if(!$resultTools = $mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']');}

if($resultTools->num_rows == 0){
	echo "No tools needing calibrated",EOL;	
	echo "DONE ".date(phpdispfdt,time()),EOL;
	exit();
}

$data = array();
while($rowTools = $resultTools->fetch_assoc()) {
	echo "Tool ID: ".$rowTools['id']." Expire: ".$rowTools['expire'],EOL;
	
	if(strtolower($rowTools['calibrating']) == 'y'){
		$color = "#80BFFF";
		$status = "Calibrating";
	}elseif(strtotime($rowTools['expire']) <= time()){
		$color = "#FF7575";
		$status = "Expired";
	}else{
		$color = "#FFFFFF";
		$status = "";
	}
	
	$date = date(phpdispfd,strtotime($rowTools['date']));
	$expire = date(phpdispfd,strtotime($rowTools['expire']));
	
	$data[$offices[$rowTools['office']]['office']][$rowTools['id']] = array("id"=>$rowTools['id'], "tool"=>$rowTools['tool'],"model"=>$rowTools['model'],"serial"=>$rowTools['serial']
	,"mfg"=>$rowTools['mfg'],"location"=>$rowTools['location'],"name"=>$rowTools['name'],"date"=>$date,"expire"=>$expire,"calibrated_by"=>$rowTools['calibrated_by']
	,"color"=>$color, "status"=>$status);
}

$smarty->assign('month',date("F Y",time()));
$smarty->assign('data',$data);
$smarty->assign('company_name',$settings->company_name);
$smarty->assign('copyright_date',$settings->copyright_date);
$smarty->assign('email_pics',$settings->email_pics);

echo "Sending tool calibration email...",EOL;


require_once($docroot.'/resources/PHPMailer/PHPMailerAutoload.php');

$tools_email_result = array();

//Engineer Request Email
$mail = new PHPMailer;
$mail->IsSMTP();                                      // Set mailer to use SMTP
$mail->Host = (string)$settings->email_host;  // Specify main and backup server
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = (string)$settings->email_support;    // SMTP username
$mail->Password = (string)$settings->email_password;                           // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted
$mail->From = (string)$settings->email_support;
$mail->FromName = 'Support '.$settings->short_name.' Portal';
if($debug){
	$mail->AddAddress('justin.davis@oxinst.com','Justin Davis');
	array_push($tools_email_result,"justin.davis@oxinst.com");
}else{
	foreach($tools_emails as $email){
		$mail->AddAddress(trim($email['email']),$email['name']);  // Add a recipient
		array_push($tools_email_result,$email['email']);
	}
}
$mail->AddReplyTo((string)$settings->email_support, 'Support '.$settings->short_name.' Portal');
$mail->AddCC((string)$settings->email_support);
$mail->IsHTML(true);                                  // Set email format to HTML
$mail->Subject = 'Tool Calibrations Report';
$mail->Body = $smarty->fetch('tools_calibration_expiring.tpl');

if($debug){
	echo EOL,"<pre>",print_r($data),"</pre>",EOL;
	echo $mail->Body,EOL;
}

if($send){
	if(!$mail->Send()) {
	   echo 'Email could not be sent.',EOL;
	   //$email_result = "Email could not be sent!";
	   echo 'Mailer Error: ' . $mail->ErrorInfo,EOL;
	   exit;
	}
}else{
	echo "Debug: Not sending email",EOL;	
}


echo "Email addresses sent to:",EOL;
//echo $email_result;
foreach($tools_email_result as $value){
	echo $value,EOL;
}

$mysqli->close();


echo "DONE ".date(phpdispfdt,time()),EOL;
exit();
?>