<?php 
//Update Completed 12/12/14

//error_reporting(E_ALL);
error_reporting(0);

$debug = false;

$no_define_eol = true;
if(substr(php_sapi_name(), 0, 3) == 'cli' || empty($_SERVER['REMOTE_ADDR'])) {
    //shell
	$cron = false;
	if(isset($_SERVER['argv'][1]) and $_SERVER['argv'][1] == '--cron'){
		$cron = true;
	}
	$docroot = preg_replace("/(public_html).*/",'public_html',dirname(__FILE__),2);
	$no_define_eol = true;
	define('EOL',PHP_EOL);
} else {
    //webserver
	$cron = false;
	$docroot = $_SERVER['DOCUMENT_ROOT'];
	define('EOL','<br />');
}

$settings = new SimpleXMLElement($docroot.'/settings.xml', null, true);
require_once($docroot.'/define_inc.php');

require($docroot.'/mysqlInfo.php');
$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
if ($mysqli->connect_errno) {
	die('There was an error running the query [' . $mysqli->connect_error . ']');
}

require_once($docroot.'/report/common/scripts/ldap.php');
$ldap = new ldapUtil($docroot);

echo "Begin ldap update ".date(phpdispfdt,time()),EOL;

echo "Getting list of users ",EOL;
$sql="SELECT u.uid, u.pwd, u.name, u.firstname, u.lastname, u.email, u.active,
CONCAT_WS('', MAX(IF(pid.perm = 'perm_dms_access', 'Y', 'N'))) AS perm_dms_access, 
CONCAT_WS('', MAX(IF(pid.perm = 'perm_kb_access', 'Y', 'N'))) AS perm_kb_access
FROM users AS u
LEFT JOIN users_perms AS up ON up.uid = u.uid
LEFT JOIN users_perm_id AS pid ON pid.id = up.pid
LEFT JOIN users_groups AS ug ON ug.uid = u.uid
LEFT JOIN users_group_id AS gid ON gid.id = ug.gid
WHERE gid.`group` = 'grp_employee' AND u.id > 9
GROUP BY u.uid
ORDER BY u.uid ASC;";
$resultUsers = $mysqli->query($sql);



while($rowUsers = $resultUsers->fetch_assoc()){
	$kb = false;
	$dms = false;
	
	if($debug){
		echo "<br><hr><br>";
		echo "<pre>",print_r($rowUsers),"</pre><br>";
	}
	
	if($cron){
		echo "Checking User: ",$rowUsers['name'];
	}else{
		echo "Checking user: ",$rowUsers['name'],"&emsp;";
	}
	
	if($rowUsers['uid'] == 'root' or $rowUsers['uid'] == 'unassigned'){
		if($cron){
			echo "  Skiping user!",EOL;
		}else{
			echo "&emsp;Skipping user!",EOL;
		}
		continue;	
	}
	
	if(strtolower($rowUsers['perm_kb_access']) == 'y'){
		$kb = true;
	}else{
		$kb = false;
	}
	
	if(strtolower($rowUsers['perm_dms_access']) == 'y'){
		$dms = true;
	}else{
		$dms = false;
	}
	
	if(strtolower($rowUsers['active']) == 'y'){
		if($ldap->user($rowUsers['uid'],$rowUsers['pwd'],$rowUsers['name'],$rowUsers['lastname'],$rowUsers['email'],$kb,$dms)){
			if($cron){
				echo " Updating User Info",EOL;
			}else{
				echo " Updating User Info.&emsp;";
			}
		}else{
			if($cron){
				echo " Updating User Info FAILED!",EOL;
			}else{
				echo " Updating User Info FAILED!.&emsp;";
			}
		}
	}else{
		if($ldap->deluser($rowUsers['uid'])){
			if($cron){
				echo " Deleting User Info",EOL;
			}else{
				echo " Deleting User Info.&emsp;";
			}
		}else{
			if($cron){
				echo " User Info doesn't exist",EOL;
			}else{
				echo " User Info doesn't exist!.&emsp;";
			}
		}
	}
	if(!$cron){
		echo EOL;	
	}
}

echo "End ldap update ".date(phpdispfdt,time()),EOL;
?>