\$log\-\>log(err|msg|info)\(['"]there was error running the query ?\[?['"] ?\. ?\$mysqli->error ?\. ?['"]\] ?['"]\,(1000)?\);

\$log\-\>log(err|msg|info)\(['"]there was error running the query ?\[?['"] ?\. ?\$mysqli->error ?\. ?['"]\] ?['"]\,(1000)?\);((\s*( *|\t*)header\(['"]location\:/?error\.php\?.*\$mysqli.*;)|(\s*( *|\t*)die\(['"].*\$mysqli.*;))

$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);


//Update Completed 11/26/14

<?php 
$no_redirect = true;
$use_default_includes = true;

$remove_session = true;


require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

$settings = new SimpleXMLElement($_SERVER['DOCUMENT_ROOT'].'/settings.xml', null, true);
require_once($_SERVER['DOCUMENT_ROOT'].'/define_inc.php');





require_once($secure_path.'/resources/smarty/libs/Smarty.class.php');
$smarty = new Smarty();
$smarty->setTemplateDir($secure_path.$settings->email_templates);
$smarty->force_compile = true;
//$smarty->debugging = true;
$smarty->caching = false;

$smarty->assign('company_name',$settings->company_name);
$smarty->assign('copyright_date',$settings->copyright_date);
$smarty->assign('email_pics',$settings->email_pics);


require_once($secure_path.'/resources/PHPMailer/PHPMailerAutoload.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}


ob_start();
require_once($_SERVER['DOCUMENT_ROOT'].'/resources/FirePHPCore/lib/FirePHPCore/fb.php');


if(!$result = $mysqli->query($sql)){die('There was an error running the query [' . $mysqli->error . ']');}



if(isset($_SERVER['argv'][1]) and $_SERVER['argv'][1] == '--cron'){
	define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : PHP_EOL);
	$secure_path = preg_replace("/(public_html).*/",'public_html',dirname(__FILE__),2);
}else{
	define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
	$secure_path = $_SERVER['DOCUMENT_ROOT'];
}
?>



<?php require_once($head_include);?>
<?php require_once($css_include);?>
<?php require_once($js_include);?>


<?php require_once($header_include); ?>


<?php require_once($footer_include); ?>


<?php readfile($_SERVER['DOCUMENT_ROOT'].'/secure/company_name.txt'); ?>


<title><?php readfile($_SERVER['DOCUMENT_ROOT'].'/secure/company_name.txt'); ?> Service Portal</title>



<?php echo $settings->company_name; ?>

{include file='email_header.tpl'}
{include file='email_footer.tpl'}




//JS

function checkDate(dt){
	// 12/12/2014
	var date_re = RegExp("^(0[1-9]|1[012])/([123]0|[012][1-9]|31)/(19[0-9]{2}|2[0-9]{3})$");
	if(date_re.test(dt) == false){
		console.log("Date Check Failed");
		return false;
	}	
	console.log("Date Check Passed");
	return true;
}

function checkDateTime(dt){
	// 12/12/2014 23:59
	console.log(dt);
	var date_re = RegExp('^(0[1-9]|1[012])/([123]0|[012][1-9]|31)/(19[0-9]{2}|2[0-9]{3}) ([0-1][0-9]|2[0-3]):([0-5][0-9])(?!.)');
	if(date_re.test(dt) == false){
		console.log("Date Check Failed");
		return false;
	}	
	console.log("Date Check Passed");
	return true;
}


if($('#').val()==""){ids.push('#'); errors.push("");}


if($settings->disable_email == '1'){
	$send = false;	
}