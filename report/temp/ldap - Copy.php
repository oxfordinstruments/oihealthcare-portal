<?php //All of this works 12/30/2013

//error_reporting(E_ALL);

if(isset($_SERVER['argv'][1]) and $_SERVER['argv'][1] == '--cron'){
	define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : PHP_EOL);
}else{
	define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
}

$givenName="Scott Simmons";
$sn="Simmons";
$displayName="Scott A. Simmons";
$uid="simmons";
$mail="scott.simmons@oxinst.com";
$pwd="b00b135";

//ldap settings
$ldaprdn  = 'cn=admin,dc=evotodi,dc=local';     // ldap admin user rdn
$ldappass = 'b00b135';  // admin password
$baseDN = ",ou=portal,dc=evotodi,dc=local";

// connect to ldap server
$ldapconn = ldap_connect("evotodi.dyndns.org")
    or die("Could not connect to LDAP server.");

ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);

if ($ldapconn) {

    // binding to ldap server
    $ldapbind = ldap_bind($ldapconn,$ldaprdn,$ldappass);
	
	$largestUID = findLargestUidNumber($ldapconn);

	$uidNumber = intval($largestUID) + 1;
	
	$userpassword = "{SHA}" . base64_encode( pack( "H*", sha1( $pwd ) ) );
	
    // verify binding
    if ($ldapbind) {
        echo "LDAP bind successful...",EOL;
		 // prepare data
	    $info['objectClass'][0] = "top";
		$info['objectClass'][1] = "posixAccount";
		$info['objectClass'][2] = "inetOrgPerson";
		$info['gidNumber'] = "0";//required
		$info['givenName'] = $givenName;
		$info['sn'] = $sn;//required
		$info['displayName'] = $displayName;
		$info['uid'] = $uid;
		$info['homeDirectory'] = "/tmp";//required
		$info['cn'] = $givenName;//required
		$info['mail'] = $mail;
		$info['uidNumber'] = $uidNumber;//required
		$info['userPassword'] = $userpassword;
		
		// add data to directory
		if(ldap_add($ldapconn, "uid=".$uid.$baseDN,$info)){
			echo "Added",EOL;
		}else{
			echo "Failed",EOL;
		}		
		ldap_close($ldapconn);
    } else {
        echo "LDAP bind failed",EOL;
    }	
}else{
	echo "Failed to connect the the ldap server",EOL;	
}

function findLargestUidNumber($ds)
    {
      $s = ldap_search($ds, "ou=portal,dc=evotodi,dc=local", 'uidNumber=*');
      if ($s)
      {
         ldap_sort($ds, $s, "uidNumber");

         $result = ldap_get_entries($ds, $s);
         $count = $result['count'];
         $biguid = $result[$count-1]['uidnumber'][0];
         return $biguid;
      }
      return null;
    }

?>