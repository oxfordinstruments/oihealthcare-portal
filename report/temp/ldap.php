<?php //All of this works 12/30/2013

//error_reporting(E_ALL);

if(isset($_SERVER['argv'][1]) and $_SERVER['argv'][1] == '--cron'){
	define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : PHP_EOL);
}else{
	define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
}

$name="Julie Mardikian";
$sn="Mardikian";
$uid="mardikianj";
$mail="julie.mardikian@oxinst.com";
$pwd="password";
$salt = "STyf1anSTydajgnmd";
//ldap settings
$ldaprdn  = 'cn=admin,dc=oiserviceportal,dc=com';     // ldap admin user rdn
$ldappass = 'b1gb00b135';  // admin password
$baseDN = ",ou=portal,dc=oiserviceportal,dc=com";

// connect to ldap server
$ldapconn = ldap_connect("localhost")
    or die("Could not connect to LDAP server.");

ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);

if ($ldapconn) {

    // binding to ldap server
    $ldapbind = ldap_bind($ldapconn,$ldaprdn,$ldappass);
	
	$largestUID = findLargestUidNumber($ldapconn);

	$uidNumber = intval($largestUID) + 1;
	
	//$userpassword = "{SHA}" . base64_encode( pack( "H*", sha1( $pwd ) ) );
	$userpassword = "{MD5}" . base64_encode( pack( "H*", md5( $pwd . $salt ) ) );
	
	$user_exist = false;
	
    // verify binding
    if ($ldapbind) {
        echo "LDAP bind successful...",EOL;
		 // prepare data
	    $info['objectClass'][0] = "posixAccount";
		$info['objectClass'][1] = "top";
		$info['objectClass'][2] = "inetOrgPerson";
		$info['gidNumber'] = "1";//required
		$info['givenName'] = $name;
		$info['sn'] = $sn;//required
		$info['displayName'] = $name;
		$info['uid'] = $uid;
		$info['homeDirectory'] = "/tmp";//required
		$info['mail'] = $mail;
		$info['cn'] = $name;//required //displayed name on dms
		$info['uidNumber'] = $uidNumber;//required
		$info['userPassword'] = $userpassword;
		
		// add data to directory
		if(ldap_add($ldapconn, "uid=".$uid.$baseDN,$info)){
			echo "User Added",EOL;
		}else{
			echo "User exists...",EOL;
			$user_exist = true;
		}	
		
		if($user_exist){
			echo "Updating user's password...",EOL;
			unset($info);
			$info['userPassword'] = $userpassword;	
			
			if(ldap_modify($ldapconn, "uid=".$uid.$baseDN,$info)){
				echo "User password updated",EOL;
			}else{
				echo "Password update failed",EOL;
			}	
		}
			
		ldap_close($ldapconn);
    } else {
        echo "LDAP bind failed",EOL;
    }	
}else{
	echo "Failed to connect the the ldap server",EOL;	
}

function findLargestUidNumber($ds)
    {
      $s = ldap_search($ds, "ou=portal,dc=oiserviceportal,dc=com", 'uidNumber=*');
      if ($s)
      {
         ldap_sort($ds, $s, "uidNumber");

         $result = ldap_get_entries($ds, $s);
         $count = $result['count'];
         $biguid = $result[$count-1]['uidnumber'][0];
         return $biguid;
      }
      return null;
    }

?>