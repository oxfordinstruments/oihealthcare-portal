{* Update Completed 12/12/14 *}
{include file='email_header.tpl'}
<table cellpadding="10" cellspacing="0" border="0" width="100%">
	<tr>
		<td colspan="2" align="center" style="color:#F79447"><h1>Account Registration Request</h1></td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="25%" align="right">Name:</td>
		<td>{$name}</td>
	</tr>
	<tr>
		<td width="25%" align="right">User ID:</td>
		<td>{$uid}</td>
	</tr>
	<tr>
		<td width="25%" align="right">Address:</td>
		<td>{$address}&nbsp;{$city},&nbsp;{$state}&nbsp;&nbsp;{$zip}</td>
	</tr>
	<tr>
		<td width="25%" align="right">Phone:</td>
		<td>{$phone}</td>
	</tr>
	<tr>
		<td width="25%" align="right">Email:</td>
		<td>{$email}</td>
	</tr>
	<tr>
		<td width="25%" align="right">System ID:</td>
		<td>{$system_id}</td>
	</tr>
	<tr>
		<td width="25%" align="right">Role:</td>
		<td>{$role}</td>
	</tr>
	<tr>
		<td width="25%" align="right">Comment:</td>
		<td>{$comment}</td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="25%" align="right">Date Submitted:</td>
		<td>{$date}</td>
	</tr>
	<tr>
		<td width="25%" align="right">Registration ID:</td>
		<td>{$id}</td>
	</tr>
	<tr>
		<td width="25%" align="right">IP:</td>
		<td>{$ip}</td>
	</tr>
	<tr>
		<td width="25%" align="right">Domain:</td>
		<td>{$domain}</td>
	</tr>
	<tr>
		<td width="25%" align="right">Browser:</td>
		<td>{$browser}</td>
	</tr>
	<tr>
		<td width="25%" align="right">Unique ID:</td>
		<td>{$unique_id}</td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
</table>
{include file='email_footer.tpl'}