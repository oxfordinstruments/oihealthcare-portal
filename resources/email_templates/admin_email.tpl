{include file='email_header.tpl'}
<table cellpadding="8" cellspacing="0" border="0" width="100%">
	<tr>
		<td align="center" style="color:#F79447"><h1>{$subject}</h1></td>
	</tr>
	<tr>
		<td><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td><pre>{$text}</pre></td>
	</tr>
	<tr>
		<td><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
</table>
{include file='email_footer.tpl'}