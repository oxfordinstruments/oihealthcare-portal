{* Update Completed 12/12/14 *}
<table cellpadding="10" cellspacing="0" border="0" width="100%">
	<tr>
		<td><pre>{$company_name}
64 Union Way
Vacaville, CA 95687
Tel:  +1 (707) 469-1320 
Fax: +1 (707) 469-1318</pre></td>
		<td><pre>{$company_name}
1027 SW 30Th Avenue
Deerfield Beach, FL  33442
Tel:  +1 (954) 596-4945 
Fax: +1 (954) 596-4946</pre></td>
	<td><pre>{$company_name}
120 Enterprise Dr
Ann Arbor, MI  48103
Tel:  +1 (734) 821-3003 
Fax: +1 (734) 794-9871</pre></td>
	</tr>
	<tr>
	<td colspan="3">This communication contains proprietary information and may be confidential.
If you are not the intended recipient, the reading, copying, disclosure or other use of the contents of this e-mail is strictly prohibited and you are instructed to please delete this e-mail immediately.</td>
	</tr>
	<tr>
		<td colspan="3"><img src="{$email_pics}/email_iso_9001.png" width="65" height="58" alt="ISO 9001">
		<img src="{$email_pics}/email_iso_13485.png" width="65" height="58" alt="ISO 13485">
		<img src="{$email_pics}/email_dotmed_logo.gif" width="128" height="58" alt="dotmed">
		<img src="{$email_pics}/email_iamers_logo.gif" width="54" height="58" alt="iamers"></td>
	</tr>
	<tr>
		<td colspan="3">Copyright &copy; {$copyright_date} {$company_name}, All rights reserved.</td>
	</tr>
</table>
<table cellpadding="10" cellspacing="0" border="0" width="100%">
	<tr>
		<td>
			Click here to <a href="{$unsubscribe}">unsubscribe or manage</a> your email subscription with {$oih_url_pretty}
		</td>
	</tr>
</table>