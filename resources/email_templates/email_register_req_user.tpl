{include file='email_header.tpl'}
<table cellpadding="10" cellspacing="0" border="0" width="100%">
	<tr>
		<td align="center" style="color:#F79447"><h1>Email Subscription Request</h1></td>
	</tr>
	<tr>
		<td><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td>
			Hello {$name}, <br><br>
			Thank you. Your email subscription request to {$base_url_pretty} has been submitted!<br>
			You will receive an email when your email subscription has been approved.<br>
			If you have any questions please contact <a href="mailto:{$support_email}">{$support_email}</a>
		</td>
	</tr>
	<tr>
		<td><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
</table>
{include file='email_footer.tpl'}