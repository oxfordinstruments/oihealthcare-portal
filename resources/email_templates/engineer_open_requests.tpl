{include file='email_header.tpl'}
<table cellpadding="5" cellspacing="0" border="0" width="100%">
	<tr>
		<td colspan="4" align="center" style="color:#F79447"><h1>Open Service Requests for {$user}</h1></td>
	</tr>
	<tr>
		<td colspan="4" align="center" style="color:#F79447"><h2>Report Date: {$date}</h2></td>
	</tr>
	<tr>
		<td colspan="4"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>	
		<tr>
			<td colspan="4">
				<table cellpadding="5" cellspacing="0" border="1" width="100%">
					<tr align="center" bgcolor="#D5D5D5">
						<td width="5%">Request ID</td>
						<td>System Nickname</td>
						<td width="10%">Inital Call Date</td>
						<td width="20%">Status</td>
					</tr>
					{foreach from=$data key=dkey item=ditem}
					<tr bgcolor="{$ditem.color}">
						<td><a href="{$oih_url}/report/?email_request={$dkey}">{$dkey}</a></td>
						<td>{$ditem.system_nickname}</td>
						<td>{$ditem.initial_call_date}</td>
						<td>{$ditem.status}</td>
					</tr>
					{/foreach}
				</table>
			</td>
		<tr>
			<td colspan="4"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
		</tr>

</table>
{include file='email_footer.tpl'}
