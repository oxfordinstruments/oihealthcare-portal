{include file='email_header.tpl'}
<table cellpadding="5" cellspacing="0" border="0" width="100%">
	<tr>
		<td colspan="7" align="center" style="color:#F79447"><h1>Future Systems Moving</h1></td>
	</tr>
	<tr>
		<td colspan="7" align="center" style="color:#F79447"><h2>Report Date: {$date}</h2></td>
	</tr>
	<tr>
		<td colspan="7"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	{foreach from=$data key=dkey item=ditem}
		<tr>
			<td colspan="7">
				<table cellpadding="5" cellspacing="0" border="1" width="100%">
					<tr>
						<td colspan="7"><h2>{$ditem.system_id}</h2></td>
					</tr>
					<tr align="center" bgcolor="#D5D5D5">
						<td width="25%">System Nickname</td>
						<td width="5%">Status</td>
						<td>Arrival Date</td>
						<td>Contract Start</td>
						<td>Contract End</td>
						<td>Warranty Start</td>
						<td>Warranty End</td>
					</tr>
					<tr bgcolor="{$ditem.color}">
						<td>{$ditem.nickname}</td>
						<td>{$ditem.status}</td>
						<td>{$ditem.arrival_date}</td>
						<td>{$ditem.contract_start_date}</td>
						<td>{$ditem.contract_end_date}</td>
						<td>{$ditem.warranty_start_date}</td>
						<td>{$ditem.warranty_end_date}</td>
					</tr>
				</table>
			</td>
		<tr>
			<td colspan="7"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
		</tr>
	{/foreach}
</table>
{include file='email_footer.tpl'}
