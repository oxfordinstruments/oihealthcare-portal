{* Update Completed 3/11/15 *}
{include file='email_header.tpl'}
<table cellpadding="10" cellspacing="0" border="0" width="100%">
	<tr>
		<td colspan="2" align="center" style="color:#F79447"><h1>Feedback Complaint {$id} Submitted</h1></td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="175" align="right">FBC ID:</td>
		<td>{$id}</td>
	</tr>
	<tr>
		<td width="175" align="right">Customer:</td>
		<td>{$customer}</td>
	</tr>
	<tr>
		<td width="175" align="right">Customer ID:</td>
		<td>{$customer_id}</td>
	</tr>
	<tr>
		<td width="175" align="right">Created By:</td>
		<td>{$created_by}</td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="175" align="right">Description:</td>
		<td><p>{$description}</p></td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
</table>
{include file='email_footer.tpl'}