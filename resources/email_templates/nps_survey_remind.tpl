<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td align="center"><table width="570" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td><table id="textEdit" border="0" cellpadding="0" cellspacing="0" width="100%">
							<tbody>
								<tr>
									<td styleclass=" style_MainText style_Header" style="padding: 0px; font-size: 10pt; font-family: Arial,Helvetica,sans-serif; color: #454545;" align="center" valign="top"><div><a class="imgCaptionAnchor" track="on" shape="rect" href="http://www.oxford-instruments.com/ct-mr"><img src="http://www.oiserviceportal.com/email/nps_header.jpg" alt="header" name="ACCOUNT.IMAGE.231" width="602" height="161" style="display: block;" border="0" hspace="0" vspace="0"></a></div></td>
								</tr>
							</tbody>
						</table>
						<table id="textEdit" border="0" cellpadding="0" cellspacing="0" width="100%">
							<tbody>
								<tr>
									<td styleclass=" style_MainText style_Content" style="padding: 8px 0px 9px; font-size: 10pt; font-family: Arial,Helvetica,sans-serif; color: #454545;" align="left" valign="top"><div><img src="https://origin.ih.constantcontact.com/fs103/1102586889542/img/302.jpg" style="display: block;" alt="Customer Satisfaction Survey" name="ACCOUNT.IMAGE.302" align="right" border="0" height="198" hspace="20" vspace="5" width="174"></div>
										<div styleclass=" style_HeadingText" style="font-size: 12pt; font-family: Arial,Helvetica,sans-serif; color: #f79548; font-weight: bold;"><span style="color: #f79548; font-size: 20pt;">Customer Survey Reminder</span><br>
										</div>
										<div><br>
											<div style="text-align: justify; font-size: 12pt;" align="justify">
												<div style="color: #666666;">
													<div style="font-size: 11pt;">
														<p>Dear {$name},</p>
														<p>At {$company_name}, we are always looking for ways to serve you better. We would appreciate your participation in a short survey to let us know if we are meeting your expectations. <br>
															<br>
														</p>
													</div>
													<div style="font-size: 11pt;">
														<p>To complete the survey, please click on the TAKE SURVEY button below and enter the 6 digit survey code: {$code} and help us serve you better.</p>
														<p>Thank you for your  participation!&nbsp; </p>
														<p><br>
														</p>
													</div>
												</div>
											</div>
										</div></td>
								</tr>
							</tbody>
						</table>
						<table id="textEdit" border="0" cellpadding="0" cellspacing="0" width="100%">
							<tbody>
								<tr>
									<td styleclass=" style_ButtonText" style="font-size: 10pt; font-family: Arial,Helvetica,sans-serif; color: #ffffff; text-decoration: none;" align="left" valign="top"><table styleclass=" style_ButtonBGColor style_CC_NoEdit" style="width: auto ! important; margin: 0px; background-color: #f79548;" border="0" cellpadding="0" cellspacing="0">
											<tbody>
												<tr>
													<td styleclass=" style_Button style_ButtonText" style="font-size: 10pt; font-family: Arial,Helvetica,sans-serif; color: #ffffff; text-decoration: none; padding: 7px 20px;" align="left" valign="top"><div><b><strong><a styleclass=" style_ButtonText" style="font-size: 10pt; font-family: Arial,Helvetica,sans-serif; color: #ffffff; text-decoration: none;" href="{$feedback_url}" shape="rect">TAKE SURVEY</a></strong></b></div></td>
													<td style="color: #ffffff;"></td>
												</tr>
											</tbody>
										</table>
										<p>&nbsp;</p></td>
								</tr>
							</tbody>
						</table>
						<table id="textEdit" style="display: table;" border="0" cellpadding="0" cellspacing="0" width="100%">
							<tbody>
								<tr>
									<td style="padding-left: 15px; font-size: 12px; height: 29px; background-color: #f79548; color: #454545;" width="300"><div style="font-family: arial; color: white; font-size: 12px;"><strong>For more information, please contact:</strong></div></td>
									<td style="font-size: 12px; height: 29px; background-color: #f79548; padding-right: 1px; color: #454545;" align="right" width="270"><forward><br>
										</forward></td>
								</tr>
								<tr>
									<td style="padding: 15px; font-size: 11px; line-height: 12px; background-color: #e8e8e9; color: #000;" colspan="2"><p style="font-family: arial; margin-top: 0px; margin-bottom: 0px;"><strong>
											<a href="http://www.oxford-instruments.com/businesses/service/healthcare/enquiries?ref=%2fbusinesses%2fservice%2fhealthcare" shape="rect">Julie Mardikian</a>
											<br>
											Senior Compliance Audit Manager<br>
											Toll Free: +1 888-673-5151<br>
											{$company_name}</strong></p>
										<p style="font-family: arial; margin-top: 0px; margin-bottom: 0px;"><strong>
											<a href="http://www.oxford-instruments.com/businesses/service/healthcare/enquiries?ref=%2fbusinesses%2fservice%2fhealthcare" shape="rect">Click here to send an inquiry.</a>
		<br />
		<br />
To learn more about us, visit
											<a href="http://www.oxford-instruments.com/healthcare" shape="rect"><span style="color:#ff6600">www.oxford-instruments.com/healthcare</span></a>.
											</strong></p></td>
								</tr>
								<tr>
									<td style="padding-top: 0px; padding-bottom: 0px; color: #454545;" colspan="2" align="center" width="600"><img src="/email/nps_footer_new.jpg" width="602"></td>
								</tr>
							</tbody>
						</table></td>
				</tr>
			</table></td>
	</tr>
</table>