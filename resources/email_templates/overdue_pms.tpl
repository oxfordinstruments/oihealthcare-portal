{include file='email_header.tpl'}
<table cellpadding="5" cellspacing="0" border="0" width="100%">
	<tr>
		<td colspan="5" align="center" style="color:#F79447"><h1>Overdue PMs</h1></td>
	</tr>
	<tr>
		<td colspan="5" align="center" style="color:#F79447"><h2>Report Date: {$date}</h2></td>
	</tr>
	<tr>
		<td colspan="5" align="left">Color Legend: <span style="color: #ff0000">Credit Hold</span>&emsp;<span style="color: #7F00FF">Pre Paid</span> </td>
	</tr>
	<tr>
		<td colspan="5"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
		<tr>
			<td colspan="5">
				<table cellpadding="1" cellspacing="0" border="1" width="100%">
					<tr align="center" bgcolor="#D5D5D5">
						<td width="10%">System ID</td>
						<td width="30%">System Nickname</td>
						<td width="25%">Engineer</td>
						<td width="10%">Last PM</td>
						<td width="10%">Next PM</td>
						<td width="15%">Days Overdue</td>
					</tr>
					{foreach from=$overdue key=okey item=oitem}
					<tr bgcolor="#ffffff" style="color: {if $oitem.credit_hold}#ff0000{elseif $oitem.pre_paid}#7F00FF{/if}">
						<td>{$okey}</td>
						<td>{$oitem.nickname}</td>
						<td>{$oitem.engineer}</td>
						<td>{$oitem.last_pm}</td>
						<td>{$oitem.next_pm}</td>
						<td>{$oitem.days}</td>
					</tr>
					{/foreach}
				</table>
			</td>
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
	<tr>
		<td colspan="5"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td colspan="5" align="center" style="color:#F79447"><br><br><h1>Late PMs</h1></td>
	</tr>
	<tr>
		<td colspan="5"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
		<tr>
			<td colspan="5">
				<table cellpadding="1" cellspacing="0" border="1" width="100%">
					<tr align="center" bgcolor="#D5D5D5">
						<td width="10%">System ID</td>
						<td width="30%">System Nickname</td>
						<td width="25%">Engineer</td>
						<td width="10%">Last PM</td>
						<td width="10%">Next PM</td>
						<td width="15%">Days Until Overdue</td>
					</tr>
					{foreach from=$late key=lkey item=litem}
					<tr bgcolor="#ffffff" style="color: {if $litem.credit_hold}#ff0000{elseif $litem.pre_paid}#7F00FF{/if}">
						<td>{$lkey}</td>
						<td>{$litem.nickname}</td>
						<td>{$litem.engineer}</td>
						<td>{$litem.last_pm}</td>
						<td>{$litem.next_pm}</td>
						<td>{$litem.days}</td>
					</tr>
					{/foreach}
				</table>
			</td>
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
	<tr>
		<td colspan="5"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td colspan="5" align="center" style="color:#F79447"><p>This report lists the systems with a pm due date that is {$overdue_days} days overdue and {$late_days} days late.<br>
			Systems with an overdue pm fail the KPI. Systems with a late pm are close to failing the KPI.</p></td>
	</tr>
	<tr>
		<td colspan="5"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	
</table>
{include file='email_footer.tpl'}
