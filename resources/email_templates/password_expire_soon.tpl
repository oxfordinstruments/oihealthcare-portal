{* Update Completed 12/04/14 *}
{include file='email_header.tpl'}
<table cellpadding="10" cellspacing="0" border="0" width="100%">
	<tr>
		<td colspan="2" align="center" style="color:#F79447"><h1>{$company_name} {if isset($portal)}{$portal}{else}Service{/if} Portal password expiring soon</h1></td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td colspan="2" align="center" style="color:#1B2673"><h3>{$firstname} your password to access the {$company_name} {if isset($portal)}{$portal}{else}Service{/if} Portal will expire in {$days} days.</h3></td>
	</tr>
	<tr>
		<td colspan="2" align="center" style="color:#1B2673"><h3>To change your password click on support in the main menu then change password.</h3>
			<table>
				<tr>
					<td>
						Password Rules:
						<ul>
							<li>Atleast 6 chatacters</li>
							<li>CaSe SenSitIve</li>
							<li>Must contain letters AND numbers</li>
						</ul>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
</table>
{include file='email_footer.tpl'}