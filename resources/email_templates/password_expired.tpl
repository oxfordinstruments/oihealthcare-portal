{* Update Completed 12/04/14 *}
{include file='email_header.tpl'}
<table cellpadding="10" cellspacing="0" border="0" width="100%">
	<tr>
		<td colspan="2" align="center" style="color:#F79447"><h1>{$company_name} {if isset($portal)}{$portal}{else}Service{/if} Portal password expired</h1></td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td colspan="2" align="center" style="color:#1B2673"><h3>{$firstname} your password to access the {$company_name} {if isset($portal)}{$portal}{else}Service{/if} Portal has expired.</h3></td>
	</tr>
	<tr>
		<td colspan="2" align="center" style="color:#1B2673"><h3>Since your password has expired you must use the recover login information link located on the login page or follow this link <a href="{$recover}">Recover Login</a></h3></td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
</table>
{include file='email_footer.tpl'}