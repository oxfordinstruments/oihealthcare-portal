{include file='email_header.tpl'}
<table cellpadding="5" cellspacing="0" border="0" width="100%">
	<tr>
		<td colspan="5" align="center" style="color:#F79447"><h1>Contract PMs Fulfillment</h1></td>
	</tr>
	<tr>
		<td colspan="5" align="left" style="color:#F79447"><h2>Report Date: {$date}</h2></td>
	</tr>
	{if isset($incomplete)}
	<tr>
		<td colspan="5" align="left" style="color: #FF0000"><h3>Caution: This report is incomplete and does not include data for the whole year. This report will be valid 31 days past the end of the current year.</h3></td>
	</tr>
	{/if}
	{if isset($missing)}
	<tr>
		<td colspan="5" align="left" style="color: #FF0000"><h3>Caution: This report does not include systems that are missing data.</h3></td>
	</tr>
	{/if}
	<tr>
		<td colspan="5"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td colspan="5">
			<table cellpadding="1" cellspacing="0" border="1" width="100%">
				<tr align="center" bgcolor="#D5D5D5">
					<td width="10%">Report Year</td>
					<td width="10%">KPI Status</td>
					<td width="10%">KPI Percent</td>
					<td width="10%">KPI Threshold</td>
					<td width="15%">Total Systems Measured</td>
					<td width="15%">Systems With All PMs Completed</td>
					<td width="15%">Systems Without All PMs Completed</td>
					<td width="15%">Systems With Errors</td>

				</tr>
				<tr align="center" bgcolor="#ffffff">
					<td>{$report_year}</td>
					<td>{$kpi_status}</td>
					<td>{$kpi_percent}%</td>
					<td>{$kpi_threshold}%</td>
					<td>{$total_systems}</td>
					<td>{$kpi_passed}</td>
					<td>{$kpi_failed}</td>
					<td>{$systems_with_errors}</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="5"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td colspan="5" align="center" style="color:#F79447"><br><br><h1>Systems failing KPI.</h1></td>
	</tr>
	<tr>
		<td colspan="5"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td colspan="5">
			Legend: <span style="color: #E56717;">Less than 50% PMs completed</span>
		</td>
	</tr>
	<tr>
		<td colspan="5">
			<table cellpadding="5" cellspacing="0" border="1" width="100%">
				<tr align="center" bgcolor="#D5D5D5">
					<td width="20%">System ID</td>
					<td width="20%">System Nickname</td>
					<td width="20%">Contract Type</td>
					<td width="20%">PMs Expected</td>
					<td width="20%">PMs Completed</td>
				</tr>
				{foreach from=$failed key=fkey item=fitem}
				<tr bgcolor="#ffffff" style="color: {if $fitem.credit_hold}#ff0000{elseif $fitem.pre_paid}#7F00FF{elseif $fitem.under_half}#E56717{/if}">
					<td>{$fitem.system_id}</td>
					<td>{$fitem.nickname}</td>
					<td>{$fitem.contract}</td>
					<td>{$fitem.expected}</td>
					<td>{$fitem.completed}</td>
				</tr>
				{/foreach}
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="5"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td colspan="5" align="center" style="color:#F79447"><br><br><h1>Systems missing data not included in report.</h1></td>
	</tr>
	<tr>
		<td colspan="5"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td colspan="5">
			<table cellpadding="5" cellspacing="0" border="1" width="100%">
				<tr align="center" bgcolor="#D5D5D5">
					<td width="10%">System ID</td>
					<td width="30%">System Nickname</td>
					<td>Error(s)</td>
				</tr>
				{foreach from=$errors key=ekey item=eitem}
					<tr bgcolor="#ffffff" style="color: {if $litem.credit_hold}#ff0000{elseif $litem.pre_paid}#7F00FF{/if}">
						<td>{$eitem.system_id}</td>
						<td>{$eitem.nickname}</td>
						<td>
							{foreach from=$eitem.errors key=eekey item=eeitem}
								<p>{$eeitem}</p>
							{/foreach}
						</td>
					</tr>
				{/foreach}
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="5"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>

	<tr>
		<td colspan="5" align="center" style="color:#F79447"><br><br><h1>Systems passing KPI.</h1></td>
	</tr>
	<tr>
		<td colspan="5"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td colspan="5">
			Legend: <span style="color: #E56717;">Too Many PMs</span>
		</td>
	</tr>
	<tr>
		<td colspan="5">
			<table cellpadding="5" cellspacing="0" border="1" width="100%">
				<tr align="center" bgcolor="#D5D5D5">
					<td width="20%">System ID</td>
					<td width="20%">System Nickname</td>
					<td width="20%">Contract Type</td>
					<td width="20%">PMs Expected</td>
					<td width="20%">PMs Completed</td>
				</tr>
				{foreach from=$passed key=pkey item=pitem}
					<tr bgcolor="#ffffff" style="color: {if $pitem.credit_hold}#ff0000{elseif $pitem.pre_paid}#7F00FF{elseif $pitem.too_many}#E56717{/if}">
						<td>{$pitem.system_id}</td>
						<td>{$pitem.nickname}</td>
						<td>{$pitem.contract}</td>
						<td>{$pitem.expected}</td>
						<td>{$pitem.completed}</td>
					</tr>
				{/foreach}
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="5"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>

</table>
{include file='email_footer.tpl'}
