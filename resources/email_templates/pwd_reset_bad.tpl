{* Update Completed 12/12/14 *}
{include file='email_header.tpl'}
<table cellpadding="10" cellspacing="0" border="0" width="100%">
	<tr>
		<td align="center" style="color:#F79447"><h1>Account Access Attempted</h1></td>
	</tr>
	<tr>
		<td><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td>
			<p>Hello {$name} <br>
				<br>
				You (or someone else) 
				entered this email ( {$email} ) when trying to change the password of a {$company_name} Service Portal account.<br>
				<br>
				However, this email address is not in our database of registered users and therefore the attemped password change has failed.<br>
				<br>
				If you are a {$company_name} Service Portal user and were expecting this email, please try again using the email address you gave when registering with {$company_name} Service Portal.<br>
				<br>
If you are not a {$company_name} Service Portal user then please disregard this email.<br>
<br>
For more information about {$company_name} Service Portal, visit www.oxford-instruments.com/healthcare
<br>
			<br>
			</p></td>
	</tr>
	<tr>
		<td><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
</table>
{include file='email_footer.tpl'}