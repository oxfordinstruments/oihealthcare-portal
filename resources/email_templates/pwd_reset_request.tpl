{* Update Completed 1/21/2015 *}
{include file='email_header.tpl'}
<table cellpadding="10" cellspacing="0" border="0" width="100%">
	<tr>
		<td align="center" style="color:#F79447"><h1>Password Reset Request</h1></td>
	</tr>
	<tr>
		<td><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td>
			Hello {$name} <br><br>
			We've sent this message because you (or someone else) have requested that your {$company_name} Service Portal password be reset.<br>
			Should you remember your password and wish not to change it then feel free to disregard this email,
			however if you did not request a password reset from oihealthcareportal.com then please contact us at support@oihealthcareportal.com<br>
			<br>				
			Here's how to reset your password:<br>
			<ol>
				<li>Click the link below to open a new and secure browser window.</li>
				<li>Enter the requested information and follow the instructions to reset your password</li>
			</ol>
			Reset your password now:<br>
			<a href="{$pwd_reset_url}?tkn={$token}" target="new">{$pwd_reset_url}?tkn={$token}</a>
		</td>
	</tr>
	<tr>
		<td><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
</table>
{include file='email_footer.tpl'}