{include file='email_header.tpl'}
<table cellpadding="10" cellspacing="0" border="0" width="100%">
	{if not isset($edit)}
	<tr>
		<td colspan="2" align="center" style="color:#F79447"><h1>Service Report for {$system_nickname} ( {$system_id} )</h1></td>
	</tr>
	{else}
	<tr>
		<td colspan="2" align="center" style="color:#F79447"><h1>Updated Service Report for {$system_nickname} ( {$system_id} )</h1></td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td colspan="2" align="center" style="color:#F00">This report was updated by {$editor} to revise any inaccuracies on the service report. 
		Please disregard any previous service reports with the reference number stated below.</td>
	</tr>
	{/if}
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="175" align="right">Referance Number:</td>
		<td>{$report_id}</td>
	</tr>
	<tr>
		<td width="175" align="right">Assigned Engineer:</td>
		<td>{$asn_eng}</td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="175" align="right">Customer Complaint:</td>
		<td><p>{$problem_reported}</p></td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="175" align="right">Service Performed:</td>
		<td><p>{$service}</p></td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
</table>
{include file='email_footer_pub.tpl'}