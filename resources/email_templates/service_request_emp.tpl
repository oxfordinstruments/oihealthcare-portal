{* Update Completed 11/25/14 *}
{include file='email_header.tpl'}
<table cellpadding="10" cellspacing="0" border="0" width="100%">
	<tr>
		<td colspan="2" align="center" style="color:#F79447"><h1>Service Request for {$system_nickname} ({$system_id})</h1></td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="175" align="right">Request ID:</td>
		<td>{$request_id}</td>
	</tr>
	<tr>
		<td width="175" align="right">System Status:</td>
		<td>{$status}</td>
	</tr>
	<tr>
		<td width="175" align="right">PM Request:</td>
		<td>{if $pm}Yes{else}No{/if}</td>
	</tr>
	<tr>
		<td width="175" align="right">Refrigeration Service:</td>
		<td>{if $refrig}Yes{else}No{/if}</td>
	</tr>




	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="175" align="right">Facility ID:</td>
		<td>{$facility_id}</td>
	</tr>
	<tr>
		<td width="175" align="right">Facility Name:</td>
		<td>{$facility_name}</td>
	</tr>
	<tr>
		<td width="175" align="right">System Address:</td>
		<td><a href="https://maps.google.com/?q={$full_address}">{$full_address}</a></td>
	</tr>
	<tr>
		<td width="175" align="right">Facility Contact:</td>
		<td>{$contact_name} ({$contact_title})</td>
	</tr>
	<tr>
		<td width="175" align="right">Phone Number:</td>
		<td>{$contact_phone}</td>
	</tr>
	<tr>
		<td width="175" align="right">Cell Number:</td>
		<td>{$contact_cell}</td>
	</tr>
	<tr>
		<td width="175" align="right">Email:</td>
		<td>{$contact_email}</td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="175" align="right">System ID:</td>
		<td>{$system_id}</td>
	</tr>
	<tr>
		<td width="175" align="right">System Nickname:</td>
		<td>{$system_nickname}</td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="175" align="right">System Type:</td>
		<td>{$system_type_name}</td>
	</tr>
	<tr>
		<td width="175" align="right">Serial Number:</td>
		<td>{$system_serial}</td>
	</tr>
	<tr>
		<td width="175" align="right">Contract Type:</td>
		<td>{$contract_name}</td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="175" align="right">Assigned Engineer:</td>
		<td><a href="mailto:{$asn_eng_email}">{$asn_eng}</a></td>
	</tr>
	<tr>
		<td width="175" align="right">Primary Engineer:</td>
		<td><a href="mailto:{$pri_eng_email}">{$pri_eng}</a></td>
	</tr>
	<tr>
		<td width="175" align="right">Secondary Engineer:</td>
		<td><a href="mailto:{$sec_eng_email}">{$sec_eng}</a></td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="175" align="right">Initail Call Date/Time:</td>
		<td>{$initial_call_date}</td>
	</tr>
	<tr>
		<td width="175" align="right">Response Date/Time:</td>
		<td>{$response_date}</td>
	</tr>
	<tr>
		<td width="175" align="right">Engineer Onsite Date/Time:</td>
		<td>{$onsite_date}</td>
	</tr>
	<tr>
		<td width="175" align="right">Dispatcher:</td>
		<td>{$dispatcher}</td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="175" align="right">Customer Complaint:</td>
		<td><p>{$problem_reported}</p></td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="175" align="right">Customer Actions:</td>
		<td><p>{$customer_actions}</p></td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
</table>
{if is_array($journal)}
	<table cellpadding="10" cellspacing="0" border="1" width="100%">
		<tr>
			<td align="center" colspan="3">Journal Entries</td>
		</tr>
		{foreach $journal as $entry}
			<tr>
				<td width="175" align="left">{$entry.date}</td>
				<td width="175" align="left">{$entry.name}</td>
				<td align="left" style="white-space: pre-wrap;"><p>{$entry.note}</p></td>
			</tr>
		{/foreach}
	</table>
	<table cellpadding="10" cellspacing="0" border="0" width="100%">
		<tr>
			<td><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
		</tr>
	</table>
{/if}
{include file='email_footer.tpl'}