{* Update Completed 11/25/14 *}
{include file='email_header.tpl'}
<table cellpadding="10" cellspacing="0" border="0" width="100%">
	<tr>
		<td colspan="2" align="center" style="color:#F79447"><h1>Service Request for {$system_nickname} ({$system_id})</h1></td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="175" align="right">Request ID:</td>
		<td>{$request_id}</td>
	</tr>
	<tr>
		<td width="175" align="right">System Status:</td>
		<td>{$status}</td>
	</tr>
	<tr>
		<td width="175" align="right">PM Request:</td>
		<td>{if $pm}Yes{else}No{/if}</td>
	</tr>
	<tr>
		<td width="175" align="right">Refrigeration Service:</td>
		<td>{if $refrig}Yes{else}No{/if}</td>
	</tr>
	
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="175" align="right">Facility ID:</td>
		<td>{$facility_id}</td>
	</tr>
	<tr>
		<td width="175" align="right">Facility Name:</td>
		<td>{$facility_name}</td>
	</tr>
	<tr>
		<td width="175" align="right">Facility Address:</td>
		<td>{$full_address}</td>
	</tr>	
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="175" align="right">System ID:</td>
		<td>{$system_id}</td>
	</tr>
	<tr>
		<td width="175" align="right">System Name:</td>
		<td>{$system_nickname}</td>
	</tr>
	<tr>
		<td width="175" align="right">Contract Type:</td>
		<td>{$contract_name}</td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="175" align="right">System Type:</td>
		<td>{$system_type_name}</td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="175" align="right">Assigned Engineer:</td>
		<td>{$asn_eng}</td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="175" align="right">Initail Call Date/Time:</td>
		<td>{$initial_call_date}</td>
	</tr>
	<tr>
		<td width="175" align="right">Response Date/Time:</td>
		<td>{$response_date}</td>
	</tr>
	<tr>
		<td width="175" align="right">Engineer Onsite Date/Time:</td>
		<td>{$onsite_date}</td>
	</tr>
	<tr>
		<td width="175" align="right">Dispatcher:</td>
		<td>{$dispatcher}</td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>	
	<tr>
		<td width="175" align="right">Problem Reported:</td>
		<td><p>{$problem_reported}</p></td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="175" align="right">Customer Actions:</td>
		<td><p>{$customer_actions}</p></td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
</table>
{include file='email_footer_pub.tpl'}