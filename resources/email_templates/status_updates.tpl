{* Update Completed 12/12/14 *}
{include file='email_header.tpl'}
<table cellpadding="5" cellspacing="0" border="0" width="100%">
	<tr>
		<td colspan="10" align="center" style="color:#F79447"><h1>System Status Updates for {$update_date[0]}</h1></td>
	</tr>
	<tr>
		<td colspan="10"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	{foreach from=$rows key=k item=i}
	<tr>
		<td width="175" align="right">System ID:</td>
		<td>{$i.system_id}</td>
		<td width="175" align="right">System Nickname:</td>
		<td>{$i.nickname}</td>
		<td width="175" align="right">Report ID:</td>
		<td>{$i.report_id}</td>
		<td width="175" align="right">User:</td>
		<td>{$i.name}</td>
		<td width="175" align="right">Date:</td>
		<td>{$i.status_date}</td>
	</tr>
	<tr>
		<td colspan="10">{$i.status}</td>
	</tr>
	<tr>
		<td colspan="10"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	{/foreach}
</table>
{include file='email_footer.tpl'}