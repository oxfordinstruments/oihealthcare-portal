{* Update Completed 12/12/14 *}
{include file='email_header.tpl'}
<table cellpadding="5" cellspacing="0" border="0" width="100%">
	<tr>
		<td colspan="8" align="center" style="color:#F79447"><h1>Engineer Assignment Changes</h1></td>
	</tr>
	<tr>
		<td colspan="8" align="center" style="color:#F79447"><h1>{$date_from} - {$date_to}</h1></td>
	</tr>
	<tr>
		<td colspan="8"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	{foreach from=$rows key=rowkey item=rowitem}
	<tr>
		<td width="100" align="left">System ID:</td>
		<td width="250" align="left">System Nickname:</td>
		<td width="250" align="left">Status:</td>
		<td width="250" align="left">{if $rowitem.change neq ""}Assignment Change:{else}&nbsp;{/if}</td>
		
	</tr>
	<tr>
		<td>{$rowitem.system_id}</td>
		<td>{$rowitem.nickname}</td>
		<td>{$rowitem.status}</td>
		<td>{$rowitem.change}</td>
		
	</tr>
	
		<td colspan="8"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	{/foreach}
</table>
{include file='email_footer.tpl'}