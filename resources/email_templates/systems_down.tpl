{include file='email_header.tpl'}
<table cellpadding="5" cellspacing="0" border="0" width="100%">
	<tr>
		<td colspan="8" align="center" style="color:#F79447"><h1>Systems Down Over 24 Hours</h1></td>
	</tr>
	<tr>
		<td colspan="8" align="center" style="color:#F79447"><h2>Report Date: {$date}</h2></td>
	</tr>
	<tr>
		<td colspan="8"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	{foreach from=$data key=dkey item=ditem}
		<tr>
			<td colspan="8">
				<table cellpadding="1" cellspacing="0" border="1" width="100%">
					<tr>
						<td colspan="8"><h2>Request/Report: {$dkey}</h2></td>
					</tr>
					<tr align="center" bgcolor="#D5D5D5">
						<td width="5%">System ID</td>
						<td width="25%">System Nickname</td>
						<td width="8%">Initial Call Date</td>
						<td width="8%">Engineer</td>
						<td width="8%">Dispatcher</td>
						<td>Problem Reported</td>
						<td width="8%">Report Started</td>
						<td width="10%">Down Time</td>
					</tr>
					<tr bgcolor="#ffffff">
						<td>{$ditem.system_id}</td>
						<td>{$ditem.system_nickname}</td>
						<td>{$ditem.initial_call_date}</td>
						<td>{$ditem.engineer}</td>
						<td>{$ditem.sender}</td>
						<td>{$ditem.problem_reported}</td>
						<td>{$ditem.report_started}</td>
						<td>{$ditem.diff}</td>
					</tr>
				</table>
			</td>
		<tr>
			<td colspan="8"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
		</tr>
	{/foreach}
	<tr>
		<td colspan="8" align="center" style="color:#F79447"><h2>This is a report of the systems with the status of "System Down", "System Down-System Unavailable", and "Additional Support Needed" that are over 24 hours from the service request's initial call date. System status can be updated by changing the system status dropdown on the service report.</h2></td>
	</tr>
	<tr>
		<td colspan="8"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	
</table>
{include file='email_footer.tpl'}
