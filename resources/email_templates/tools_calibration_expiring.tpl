{include file='email_header.tpl'}
<table cellpadding="5" cellspacing="0" border="0" width="100%">
	<tr>
		<td colspan="10" align="center" style="color:#F79447"><h1>Tool Calibrations Expiring {$month}</h1></td>
	</tr>
	<tr>
		<td colspan="10"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	{foreach from=$data key=dkey item=ditem}
		<tr>
			<td colspan="10">
				<table cellpadding="5" cellspacing="0" border="1" width="100%">
					<tr>
						<td colspan="10"><h2>{$dkey}</h2></td>
					</tr>
					<tr bgcolor="#D5D5D5" align="center">
						<td>ID</td>
						<td>Status</td>
						<td>Tool</td>
						<td>Model Number</td>
						<td>Serial Number</td>
						<td>Manufacturer</td>
						<td>Cal Date</td>
						<td>Cal Expire</td>
						<td>Cal'd By</td>
						<td>Tool Holder</td>
						<td>Tool Location</td>
					</tr>
					{foreach from=$ditem key=tkey item=titem}
						<tr bgcolor="{$titem.color}">
							<td>{$titem.id}</td>
							<td>{$titem.status}</td>
							<td>{$titem.tool}</td>
							<td>{$titem.model}</td>
							<td>{$titem.serial}</td>
							<td>{$titem.mfg}</td>
							<td>{$titem.date}</td>
							<td>{$titem.expire}</td>
							<td>{$titem.calibrated_by}</td>
							<td>{$titem.name}</td>
							<td>{$titem.location}</td>
						</tr>
					{/foreach}
				</table>
			</td>
		<tr>
			<td colspan="10"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
		</tr>
	{/foreach}
</table>
{include file='email_footer.tpl'}
