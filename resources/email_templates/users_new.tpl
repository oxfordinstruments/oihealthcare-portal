{include file='email_header.tpl'}
<table cellpadding="10" cellspacing="0" border="0" width="100%">
	<tr>
		<td colspan="2" align="center" style="color:#F79447"><h1>{$firstname} welcome to the {$company_name} {if isset($portal)}{$portal}{else}Service{/if} Portal</h1></td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td colspan="2" align="center" style="color:#1B2673"><h3>{$firstname} below you will find your new {$company_name} {if isset($portal)}{$portal}{else}Service{/if} Portal login information.</h3></td>
	</tr>
	<tr>
		<td width="50%" align="right">Username:</td>
		<td>{$uid}</td>
	</tr>
	<tr>
		<td width="50%" align="right">Temporary Password:</td>
		<td style="color:#F00">{$pwd}</td>
	</tr>
	<tr>
		<td width="50%" align="right">First Name:</td>
		<td>{$firstname}</td>
	</tr>
	<tr>
		<td width="50%" align="right">Last Name:</td>
		<td>{$lastname}</td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	{if $errors neq ""}
	<tr>
		<td width="50%" align="right">Notification Status:</td>
		<td>{$errors}</td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	{/if}
	<tr>
		<td colspan="2" align="center" style="color:#1B2673"><h3>You will be required to change your temporary password, and set security questions when you login for the first time.<br>
		To login point your favorite browser to <a href="https://oihealthcareportal.com">https://OiHealthcarePortal.com</a></h3></td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
</table>
{include file='email_footer.tpl'}