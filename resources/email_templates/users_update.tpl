{include file='email_header.tpl'}
<table cellpadding="10" cellspacing="0" border="0" width="100%">
	<tr>
		<td colspan="2" align="center" style="color:#F79447"><h1> {$company_name} {if isset($portal)}{$portal}{else}Service{/if} Portal user profile updated</h1></td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td colspan="2" align="center" style="color:#1B2673"><h3>{$firstname} below you will find the changes made to your user profile.</h3></td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="50%" align="right">Active User:</td>
		<td>{$active}</td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="50%" align="right">Username:</td>
		<td>{$uid}</td>
	</tr>
	{if $pwd neq ""}
	<tr>
		<td width="50%" align="right">Temporary Password:</td>
		<td style="color:#F00">{$pwd}</td>
	</tr>
	{/if}
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="50%" align="right">First Name:</td>
		<td>{$firstname}</td>
	</tr>
	<tr>
		<td width="50%" align="right">Last Name:</td>
		<td>{$lastname}</td>
	</tr>
	<tr>
		<td width="50%" align="right">Address:</td>
		<td>{$addy}</td>
	</tr>
	<tr>
		<td width="50%" align="right">Cell / Phone:</td>
		<td>{$cell}</td>
	</tr>
	{if $carrier neq ""}
	<tr>
		<td width="50%" align="right">Cell Carrier:</td>
		<td>{$carrier}</td>
	</tr>
	{/if}
	<tr>
		<td width="50%" align="right">Email Address:</td>
		<td>{$email}</td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="50%" align="right">Roles Assigned:</td>
		<td>{$roles}</td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="50%" align="right">Permissions Granted:</td>
		<td>{$perms}</td>
	</tr>
	{if not $no_systems}
	{if not $customer}
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="50%" align="right">Primary Assigned Systems:</td>
		<td>{$pri_systems}</td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="50%" align="right">Secondary Assigned Systems:</td>
		<td>{$sec_systems}</td>
	</tr>
	{else}
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="50%" align="right">Systems:</td>
		<td>{$systems}</td>
	</tr>
	{/if}
	{/if}
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	{if $errors neq ""}
	<tr>
		<td width="50%" align="right">Notification Status:</td>
		<td>{$errors}</td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	{/if}
	<tr>
		<td colspan="2" align="center" style="color:#1B2673"><h3>To login point your favorite browser to <a href="https://oihealthcareportal.com">https://OiHealthcarePortal.com</a></h3></td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
</table>
{include file='email_footer.tpl'}