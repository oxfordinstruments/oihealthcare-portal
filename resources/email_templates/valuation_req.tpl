{include file='email_header.tpl'}
<table cellpadding="10" cellspacing="0" border="0" width="100%">
	<tr>
		<td colspan="2" align="center" style="color:#F79447"><h1>Valuation Request for {$system_nickname} ( {$system_id} )</h1></td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="175" align="right">Referance Number:</td>
		<td>{$report_id}</td>
	</tr>
	<tr>
		<td width="175" align="right">Assigned Engineer:</td>
		<td>{$asn_eng}</td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>	
	<tr>
		<td width="175" align="right">Customer Complaint:</td>
		<td><p>{$problem_reported}</p></td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
	<tr>
		<td width="175" align="right">Service Performed:</td>
		<td><p>{$service}</p></td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-color:#F79447; background-color:#F79447; color:#F79447"></td>
	</tr>
</table>
{include file='email_footer.tpl'}