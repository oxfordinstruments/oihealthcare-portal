// JavaScript Document


//Returns false on bad value
function check_serial(value){
	value = value.trim()

	var checks = new Array();
	checks[0] = '^(n|N)(\\\\|\\/)?(a|A)$';
	checks[1] = 'unknown';
	checks[2] = 'engineer';
	checks[3] = '^\\W';
	checks[4] = '^0*0$';
	checks[5] = 'none';
	
	if(value.length < 4){
		return false;	
	}
	
	var re = new RegExp(checks.join('|'),'gi');
	
	//console.log("Serial Check Return");
	//console.log(re.exec(value));
	
	if(re.test(value) == true){
		return false;	
	}
	
	return true;
}

//Returns false on bad value
function check_tbd(value){
	value = value.trim();
	
	var checks = new Array();
	checks[0] = '^n\/?\\\\?(a\\s?)$';
	checks[1] = '^tbd\\s?';
	checks[2] = 'unknown';
	checks[3] = 'engineer';
	checks[4] = '^\\W\\s?';

	var re1 = new RegExp(checks.join('|'),'gi');
		
	if(re1.test(value) == true){
		return false;	
	}
	
	return true;
	
}