<?php 
//require_once $_SERVER['DOCUMENT_ROOT'].'/common/session_ctrl.php';
/** Error reporting */
//error_reporting(E_ALL);

if ($_SERVER['HTTPS'] != "on" && $_SERVER['SERVER_NAME'] != 'secure.jrdhome.com') {
	$url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
	header("Location: $url");
	exit;
}else{
	if($_SERVER['SERVER_NAME'] == 'secure.jrdhome.com'){
		$srvr = '.jrdhome.com';
		$https_cookie  = false;
	}else{
		$srvr = '.oihealthcareportal.com';
		$https_cookie  = true;
	}
}

session_name("OIREPORT");
session_start();
$now = time(); // checking the time now when home page starts
ob_start();//Start output buffering
$settings = new SimpleXMLElement($_SERVER['DOCUMENT_ROOT'].'/settings.xml', null, true);

$closed_to_user = false;
if($settings->close_site == 1){
	if(isset($_SESSION['login'])){
		$allowed_users = explode(',',$settings->users_pass_close);
		if(!in_array((string)$_SESSION['login'],$allowed_users)){
			$closed_to_user = true;			
		}
	}else{
		$closed_to_user = true;
	}
}
if($closed_to_user){
	if(!strpos($_SERVER['REQUEST_URI'],'error.php?')){
		header("location:/error.php?n=1");
	}
}

$mysql_tz_offset = $settings->mysql_tz_offset;
date_default_timezone_set($settings->TimeZone);
require($_SERVER['DOCUMENT_ROOT'].'/mysqlInfo.php');
mysql_connect("$host", "$username", "$password")or die("cannot connect");
mysql_select_db("$db_name") or die("cannot select DB");

if(isset($_COOKIE['OIREPORT_AL'])){
	$cookie_vals = explode('@',$_COOKIE['OIREPORT_AL']);
	$sql="SELECT * FROM auto_logins WHERE `key` = '".$cookie_vals[1]."' AND uid = '".$cookie_vals[0]."';";	
	$resultAL=mysql_query($sql);
	$numAL = mysql_num_rows($resultAL);
	if($numAL > 0){
		$rowAL = mysql_fetch_array($resultAL);
		if($rowAL['ip'] == $_SERVER['REMOTE_ADDR']){
		  session_decode($rowAL['vars']);
		  $expire = $_SESSION['expire'];
		  $key = hash('sha256',md5(uniqid(rand(), true)),false);
		  $sql="UPDATE auto_logins SET `key` = '$key', `session_expire` = '$expire' WHERE `key` = '".$cookie_vals[1]."' AND uid = '".$cookie_vals[0]."';";
		  mysql_query($sql);
		  $expire_cookie = time()+60*60*10;
		  setcookie('OIREPORT_AL',$cookie_vals[0].'@'.$key,$_SESSION['expire'],'/',$srvr,$https_cookie,false);
		  //setcookie('OIREPORT_AL',$cookie_vals[0].'@'.$key);
		}
	}	
}

if($now > $_SESSION['expire'])
{
	session_destroy();
	header("Location:/logout.php");
}
if (!isset($_SESSION["login"])){
  header("location:/");
}

include($_SERVER['DOCUMENT_ROOT'].'/common/send_error.php');

//include $_SERVER['DOCUMENT_ROOT'].'/Mobile_Detect.php';
//$detect = new Mobile_Detect;
//
//if ($_SESSION['mobile_device']()) {
//	header("location:mobile_login.php");
?>