<?php
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_WARNING);
ini_set('display_errors', 'Off');

$debug = false;

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

if(!isset($_GET['unique_id'])){
	$log->logerr("Not set in post",1048);
	header("location:/error.php?n=1048&t=Not set in post&p=kcfinder\index");
}
$unique_id = $_GET['unique_id'];

if(!isset($_GET['table'])){
	$log->logerr("Not set in post",1049);
	header("location:/error.php?n=1049&t=Not set in post&p=kcfinder\index");
}
$table = $_GET['table'];

//$upload_url = (string)$settings->uploads_dir;
$upload_url = (string)$settings->php_base_dir . (string)$settings->uploads_dir;

switch($table){
	case 'systems_reports_files':
		$upload_url .= (string)$settings->uploads->systems_reports_files;
		break;
	case 'systems_files':
		$upload_url .= (string)$settings->uploads->systems_files;
		break;
	case 'facilities_files':
		$upload_url .= (string)$settings->uploads->facilities_files;
		break;
	case 'customers_files':
		$upload_url .= (string)$settings->uploads->customers_files;
		break;
	case 'iso_car_files':
		$upload_url .= (string)$settings->uploads->iso_car_files;
		break;	
	case 'iso_par_files':
		$upload_url .= (string)$settings->uploads->iso_par_files;
		break;	
	case 'iso_fbc_files':
		$upload_url .= (string)$settings->uploads->iso_fbc_files;
		break;
	case 'trailers_files':
		$upload_url .= (string)$settings->uploads->trailers_files;
		break;	
}
$upload_url .= '/' . $unique_id . '/';

$upload = false;
if($_SESSION['group'] == '1' or $_SESSION['group'] == 1){
	$upload = true;	
}
if(isset($_GET['ro'])){
	$upload = false;	
}

$_SESSION['KCFINDER'] = array(
    'disabled' => false,
	'uploadURL' => $upload_url, //base directory where to store the files e.g. uploads/sites or uploads/reports
    'uploadDir' => $upload_url,
	'uuid' => $unique_id,	// folder unique id aka site or report unique id
	'table' => $table,  // systems_files, systems_reports_files
	'access' => array(
        'files' => array(
            'upload' => $upload,
            'delete' => $upload,
            'copy'   => false,
            'move'   => false,
            'rename' => false
        ),

        'dirs' => array(
            'create' => $upload,
            'delete' => $upload,
            'rename' => false
        )
	)
);


dd($_SESSION);

?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>KCFinder</title>
</head>

<body>
<div>
<?php require "browse.php"; ?>
</div>
</body>
</html>
