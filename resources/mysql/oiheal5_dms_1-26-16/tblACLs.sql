-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_dms.tblACLs
CREATE TABLE IF NOT EXISTS `tblACLs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `target` int(11) NOT NULL DEFAULT '0',
  `targetType` tinyint(4) NOT NULL DEFAULT '0',
  `userID` int(11) NOT NULL DEFAULT '-1',
  `groupID` int(11) NOT NULL DEFAULT '-1',
  `mode` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8;

-- Dumping data for table oiheal5_dms.tblACLs: ~22 rows (approximately)
/*!40000 ALTER TABLE `tblACLs` DISABLE KEYS */;
INSERT INTO `tblACLs` (`id`, `target`, `targetType`, `userID`, `groupID`, `mode`) VALUES
	(5, 146, 2, -1, 3, 2),
	(6, 146, 2, -1, 3, 2),
	(26, 143, 2, -1, 3, 2),
	(35, 184, 2, -1, 3, 2),
	(36, 180, 2, -1, 3, 2),
	(37, 170, 2, -1, 3, 2),
	(38, 177, 2, -1, 3, 2),
	(39, 181, 2, -1, 3, 2),
	(42, 80, 2, -1, 3, 2),
	(44, 88, 2, -1, 3, 2),
	(45, 72, 2, -1, 3, 2),
	(46, 72, 2, -1, 3, 2),
	(47, 72, 2, -1, 3, 2),
	(48, 158, 2, -1, 3, 2),
	(51, 148, 2, -1, 3, 2),
	(53, 60, 2, -1, 3, 2),
	(56, 172, 2, -1, 3, 2),
	(64, 75, 2, -1, 3, 2),
	(65, 7, 1, -1, 3, 2),
	(73, 75, 1, -1, 5, 2),
	(76, 1, 1, 117, -1, 2),
	(77, 1, 1, -1, 4, 2);
/*!40000 ALTER TABLE `tblACLs` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
