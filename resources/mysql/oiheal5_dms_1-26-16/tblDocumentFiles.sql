-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_dms.tblDocumentFiles
CREATE TABLE IF NOT EXISTS `tblDocumentFiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `document` int(11) NOT NULL DEFAULT '0',
  `userID` int(11) NOT NULL DEFAULT '0',
  `comment` text,
  `name` varchar(150) DEFAULT NULL,
  `date` int(12) DEFAULT NULL,
  `dir` varchar(255) NOT NULL DEFAULT '',
  `orgFileName` varchar(150) NOT NULL DEFAULT '',
  `fileType` varchar(10) NOT NULL DEFAULT '',
  `mimeType` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `tblDocumentFiles_document` (`document`),
  KEY `tblDocumentFiles_user` (`userID`),
  CONSTRAINT `tblDocumentFiles_document` FOREIGN KEY (`document`) REFERENCES `tblDocuments` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tblDocumentFiles_user` FOREIGN KEY (`userID`) REFERENCES `tblUsers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- Dumping data for table oiheal5_dms.tblDocumentFiles: ~25 rows (approximately)
/*!40000 ALTER TABLE `tblDocumentFiles` DISABLE KEYS */;
INSERT INTO `tblDocumentFiles` (`id`, `document`, `userID`, `comment`, `name`, `date`, `dir`, `orgFileName`, `fileType`, `mimeType`) VALUES
	(3, 77, 2, 'New Log created. ', 'Revision Log ', 1402342686, '77/', 'REV LOG_F70-07 New Account Equipment Record.pdf', '.pdf', 'application/pdf'),
	(4, 1915, 2, '', 'Revision Log ', 1402673919, '1915/', 'REV LOG_QOP 5.5.1 Defined Responsibilities.pdf', '.pdf', 'application/pdf'),
	(6, 1934, 2, '', 'Revision Log ', 1402674089, '1934/', 'REV LOG_QOP 6.2.2 Competence and Training.pdf', '.pdf', 'application/pdf'),
	(7, 1917, 2, '', 'Revision Log ', 1402674258, '1917/', 'REV LOG_QOP 7.6 Control of Monitoring and Measuring Devices.pdf', '.pdf', 'application/pdf'),
	(8, 197, 2, '', 'Revision Log ', 1402674337, '197/', 'REV LOG_WI-0027-74 Receiving Process.doc', '.doc', 'application/msword'),
	(9, 204, 2, '', 'Revision Log ', 1402674409, '204/', 'REV LOG_WI-0032-755 Preservation of Product During Storage & Handling.doc', '.doc', 'application/msword'),
	(10, 196, 2, '', 'Revision Log ', 1402674478, '196/', 'REV LOG_WI-0035-70 MRI Coil Repair Process.doc', '.doc', 'application/msword'),
	(11, 189, 2, '', 'Revision Log ', 1403122830, '189/', 'REV LOG_WI-0031-551 Equipment & Service Orders.doc', '.doc', 'application/msword'),
	(12, 1919, 2, '', 'REV LOG', 1403894623, '1919/', 'REV LOG_QOP 5.6 Management Review.pdf', '.pdf', 'application/pdf'),
	(13, 1990, 2, '', 'Revision Log ', 1407853126, '1990/', 'REV LOG_QOP 8.3C Scrap Authorization Log.pdf', '.pdf', 'application/pdf'),
	(14, 1991, 2, '', 'New Work Instruction', 1407867607, '1991/', 'REV LOG_WI-0040-76 He Turret Leak-Check Procedure.pdf', '.pdf', 'application/pdf'),
	(15, 73, 2, '', 'Revision Log ', 1409076014, '73/', 'REV LOG_F74-02 Supplier Evaluation.doc', '.doc', 'application/msword'),
	(16, 174, 2, '', 'Document Control Form ', 1410550435, '174/', 'REV LOG_F63-07 Forklift Maintenance Log.pdf', '.pdf', 'application/pdf'),
	(17, 131, 2, '', 'Revision Log ', 1412368042, '131/', 'REV LOG_F74-07 PO Request Form.pdf', '.pdf', 'application/pdf'),
	(18, 2013, 2, '', 'Revision Log ', 1412368233, '2013/', 'REV LOG_F74-09 Siemens PO Request Form.pdf', '.pdf', 'application/pdf'),
	(19, 104, 2, '', 'Revision Log ', 1412369303, '104/', 'REV LOG_F70-14 CT Service Report.pdf', '.pdf', 'application/pdf'),
	(20, 90, 2, '', 'Revision Log ', 1412369358, '90/', 'REV LOG_F70-15 MR Service Report.pdf', '.pdf', 'application/pdf'),
	(21, 124, 2, '', 'Revision Log ', 1412708387, '124/', 'REV LOG_F755-01 ESD Wrist Strap Test Log PDF.pdf', '.pdf', 'application/pdf'),
	(22, 1932, 2, '', 'Revision Log ', 1412708417, '1932/', 'REV LOG_QOP 7.5.5 Preservation of Product During Storage and Handling.pdf', '.pdf', 'application/pdf'),
	(23, 123, 2, '', 'Revision Log ', 1414087776, '123/', 'REV LOG_F7532-01Staging Bay System ID.doc', '.doc', 'application/msword'),
	(25, 96, 2, '', 'Revision Log ', 1414174165, '96/', 'REV LOG_F70-06 100 Point MRI Refurb Checklist.pdf', '.pdf', 'application/pdf'),
	(26, 165, 2, '', 'Revision Log ', 1414179780, '165/', 'REV LOG_F851-02 FDA Advisory Notice Log.doc', '.doc', 'application/msword'),
	(27, 1935, 2, '', 'Revision Log ', 1414180043, '1935/', 'REV LOG_QOP 8.5.1 Issuance and Implementation of Advisory Notices.pdf', '.pdf', 'application/pdf'),
	(28, 110, 2, '', 'Revision Log ', 1414516265, '110/', 'REV LOG_F70-04 100 Point CT Refurb Checklist.pdf', '.pdf', 'application/pdf');
/*!40000 ALTER TABLE `tblDocumentFiles` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
