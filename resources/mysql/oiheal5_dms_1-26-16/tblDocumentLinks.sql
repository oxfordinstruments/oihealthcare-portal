-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_dms.tblDocumentLinks
CREATE TABLE IF NOT EXISTS `tblDocumentLinks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `document` int(11) NOT NULL DEFAULT '0',
  `target` int(11) NOT NULL DEFAULT '0',
  `userID` int(11) NOT NULL DEFAULT '0',
  `public` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `tblDocumentLinks_document` (`document`),
  KEY `tblDocumentLinks_target` (`target`),
  KEY `tblDocumentLinks_user` (`userID`),
  CONSTRAINT `tblDocumentLinks_document` FOREIGN KEY (`document`) REFERENCES `tblDocuments` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tblDocumentLinks_target` FOREIGN KEY (`target`) REFERENCES `tblDocuments` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tblDocumentLinks_user` FOREIGN KEY (`userID`) REFERENCES `tblUsers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=133 DEFAULT CHARSET=utf8;

-- Dumping data for table oiheal5_dms.tblDocumentLinks: ~41 rows (approximately)
/*!40000 ALTER TABLE `tblDocumentLinks` DISABLE KEYS */;
INSERT INTO `tblDocumentLinks` (`id`, `document`, `target`, `userID`, `public`) VALUES
	(10, 149, 146, 1, 0),
	(11, 146, 149, 1, 0),
	(12, 149, 146, 1, 0),
	(13, 146, 149, 1, 0),
	(52, 142, 143, 1, 0),
	(53, 143, 142, 1, 0),
	(70, 186, 184, 1, 0),
	(71, 184, 186, 1, 0),
	(72, 176, 180, 1, 0),
	(73, 180, 176, 1, 0),
	(74, 179, 170, 1, 0),
	(75, 170, 179, 1, 0),
	(76, 182, 177, 1, 0),
	(77, 177, 182, 1, 0),
	(78, 175, 181, 1, 0),
	(79, 181, 175, 1, 0),
	(84, 77, 80, 1, 0),
	(85, 80, 77, 1, 0),
	(88, 91, 88, 1, 0),
	(89, 88, 91, 1, 0),
	(90, 73, 72, 1, 0),
	(91, 72, 73, 1, 0),
	(92, 73, 72, 1, 0),
	(93, 72, 73, 1, 0),
	(94, 73, 72, 1, 0),
	(95, 72, 73, 1, 0),
	(96, 161, 158, 1, 0),
	(97, 158, 161, 1, 0),
	(102, 144, 148, 1, 0),
	(103, 148, 144, 1, 0),
	(106, 61, 60, 1, 0),
	(112, 169, 172, 1, 0),
	(113, 172, 169, 1, 0),
	(128, 76, 75, 1, 0),
	(129, 75, 76, 1, 0),
	(132, 1934, 204, 2, 1);
/*!40000 ALTER TABLE `tblDocumentLinks` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
