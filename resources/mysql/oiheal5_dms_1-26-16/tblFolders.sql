-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_dms.tblFolders
CREATE TABLE IF NOT EXISTS `tblFolders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(70) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `folderList` text NOT NULL,
  `comment` text,
  `date` int(12) DEFAULT NULL,
  `owner` int(11) DEFAULT NULL,
  `inheritAccess` tinyint(1) NOT NULL DEFAULT '1',
  `defaultAccess` tinyint(4) NOT NULL DEFAULT '0',
  `sequence` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `parent` (`parent`),
  KEY `tblFolders_owner` (`owner`),
  CONSTRAINT `tblFolders_owner` FOREIGN KEY (`owner`) REFERENCES `tblUsers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=705 DEFAULT CHARSET=utf8;

-- Dumping data for table oiheal5_dms.tblFolders: ~59 rows (approximately)
/*!40000 ALTER TABLE `tblFolders` DISABLE KEYS */;
INSERT INTO `tblFolders` (`id`, `name`, `parent`, `folderList`, `comment`, `date`, `owner`, `inheritAccess`, `defaultAccess`, `sequence`) VALUES
	(1, 'Oxford Healthcare', 0, '', 'Documents', 1389910365, 1, 0, 2, 0),
	(7, 'Portal', 1, ':1:', 'DO NOT CHANGE ANYTHING IN THIS FOLDER WITHOUT CONSULTING PORTAL SUPPORT!!!', 1389998522, 1, 0, 1, 1),
	(60, 'Quality Management System ', 1, ':1:', '', 1390877128, 2, 1, 2, 2),
	(61, 'Quality Manual', 60, ':1:60:', '', 1390877128, 2, 1, 2, -0.75),
	(62, 'External Documents', 60, ':1:60:', 'External Documents are documents of reference materials for OiService employees. ', 1390877128, 2, 1, 2, 2.1875),
	(63, 'PRO-3', 62, ':1:60:62:', '', 1390877129, 2, 1, 2, 0),
	(64, 'Forms', 60, ':1:60:', '', 1390877129, 2, 1, 2, 2),
	(65, 'CT MR ALL FORMS', 64, ':1:60:64:', 'Changed Name of Folder', 1390877129, 2, 1, 2, 4.5),
	(66, 'Supplier Evaluation Forms', 65, ':1:60:64:65:', 'changed title', 1390877129, 2, 1, 2, 0),
	(67, 'CT_MR Forms', 65, ':1:60:64:65:', 'Removed 7.0 from title', 1390877129, 2, 1, 2, -1),
	(68, 'Service Reports', 67, ':1:60:64:65:67:', 'Removed 7.0', 1390877131, 2, 1, 2, 0),
	(69, 'Service Forms ', 67, ':1:60:64:65:67:', 'Refurbishment Forms', 1390877131, 2, 1, 2, -0.75),
	(70, 'MR Forms', 69, ':1:60:64:65:67:69:', '', 1390877131, 2, 1, 2, 0),
	(71, 'CT Forms', 69, ':1:60:64:65:67:69:', '', 1390877133, 2, 1, 2, 1),
	(72, 'Preservation of Product', 65, ':1:60:64:65:', 'change title', 1390877134, 2, 1, 2, 3),
	(73, 'Risk Management', 65, ':1:60:64:65:', 'Changed title', 1390877134, 2, 1, 2, -0.5),
	(74, 'Purchasing-Shipping', 65, ':1:60:64:65:', 'Revised title', 1390877134, 2, 1, 2, -0.125),
	(75, 'CT/MR CONTRACT TEMPLATE', 1, ':1:', 'Equipment Sales Contract Template', 1390877139, 2, 0, 1, -0.25),
	(76, 'Quality Management ', 64, ':1:60:64:', 'Removed 4.0', 1390877141, 2, 1, 2, 2),
	(77, 'Control of Record ', 76, ':1:60:64:76:', 'Removed 4.2.4', 1390877141, 2, 1, 2, 0),
	(78, 'Document Control ', 76, ':1:60:64:76:', 'Removed 4.2.3 from title', 1390877141, 2, 1, 2, 1),
	(79, 'KPI\'s', 60, ':1:60:', '', 1390877141, 2, 1, 2, 3),
	(80, 'Management Responsibility', 64, ':1:60:64:', 'Removed 5.0 Title', 1390877142, 2, 1, 2, 4),
	(81, 'NJ FORMS-All Operation', 65, ':1:60:64:65:', 'NJ will be integrated within the formal QMS Filing system ', 1390877142, 2, 1, 2, 5),
	(82, 'Measurement, Analysis and Improvement', 64, ':1:60:64:', 'Removed 8.0 from title', 1390877144, 2, 1, 2, 6),
	(83, 'Audit ', 82, ':1:60:64:82:', 'changed title', 1390877144, 2, 1, 2, 0),
	(84, 'Advisory', 82, ':1:60:64:82:', 'changed  title', 1390877144, 2, 1, 2, 1),
	(85, 'Feedback', 82, ':1:60:64:82:', 'changed title', 1390877144, 2, 1, 2, 2),
	(86, 'Training and Maintenance', 64, ':1:60:64:', 'Removed 6.0 from Title', 1390877146, 2, 1, 2, 4.25),
	(87, 'Maintenance Activity', 86, ':1:60:64:86:', 'Removed 6.3 from Title', 1390877146, 2, 1, 2, 0),
	(88, 'Competence and Training', 86, ':1:60:64:86:', 'Removed 6.2 from title', 1390877146, 2, 1, 2, -1),
	(89, 'Work Instructions - Reference Manual', 60, ':1:60:', 'changed title', 1390877146, 2, 1, 2, 0.75),
	(90, 'Front Desk Reference Manual', 89, ':1:60:89:', '', 1390877147, 2, 1, 2, 0),
	(91, 'Prism Reference Manual', 89, ':1:60:89:', '', 1390877149, 2, 1, 2, 1),
	(666, 'Quality Procedures', 60, ':1:60:', '', 1390877513, 2, 1, 2, -0.5),
	(671, 'Refrigeration External Documents ', 62, ':1:60:62:', 'These Documents are located on the OIService Intranet. ', 1391618603, 2, 1, 2, 1),
	(672, 'ISO Standards', 62, ':1:60:62:', '', 1398695739, 2, 0, 1, -1),
	(673, 'CT Manuals ', 62, ':1:60:62:', '', 1398698036, 2, 1, 2, 2),
	(674, 'Corporate Policy ', 62, ':1:60:62:', '', 1398698158, 2, 1, 2, 3),
	(675, 'Corporate Forms', 62, ':1:60:62:', '', 1398698575, 2, 1, 2, 4),
	(680, 'KPI 1 On Time Service Quality ', 79, ':1:60:79:', '', 1398711917, 2, 1, 2, 1),
	(682, 'Role Profiles', 60, ':1:60:', '', 1400161103, 2, 1, 2, 4),
	(683, 'Reference Guides ', 1, ':1:', '', 1400260601, 2, 1, 2, 3),
	(685, 'MRI', 75, ':1:75:', 'Mri Contracts', 1406818379, 1, 1, 2, 1),
	(686, 'CT', 75, ':1:75:', 'CT Contracts', 1406818393, 1, 1, 2, 2),
	(687, 'Broker', 75, ':1:75:', 'Broker Contracts', 1406818417, 1, 1, 2, 3),
	(688, 'Refrigeration', 75, ':1:75:', 'Refrigeration Contracts', 1406818443, 1, 1, 2, 4),
	(689, 'Interoffice ', 1, ':1:', 'Letterheads, powerpoints, calendars, templates ', 1411053127, 2, 1, 2, 4),
	(691, 'Schedules ', 689, ':1:689:', '', 1411053198, 2, 1, 2, 1),
	(692, 'Directory', 689, ':1:689:', '', 1411397818, 2, 1, 2, 1),
	(694, 'OI Healthcare References', 75, ':1:75:', '', 1412710783, 2, 1, 2, 5),
	(695, 'Templates', 689, ':1:689:', '', 1412711056, 2, 1, 2, 0),
	(696, 'ISO Certificates', 1, ':1:', '', 1412711486, 2, 1, 2, 5),
	(697, 'Front Office Reference Guide', 689, ':1:689:', '', 1417472904, 2, 1, 2, 2),
	(698, 'Lease Agreements', 75, ':1:75:', 'Add', 1428007167, 2, 1, 2, 6),
	(699, 'Trade Show Forms ', 75, ':1:75:', 'Trade Show forms, Calendars, ', 1430244989, 2, 1, 2, 7),
	(700, 'Ann Arbor - All Forms', 60, ':1:60:', 'Created for MIR Integration', 1440172726, 2, 1, 2, 6),
	(701, 'Installation-Inspection Forms', 65, ':1:60:64:65:', 'All forms, installation GE Siemens, CT, MR, Trailer', 1443545069, 2, 1, 2, 6),
	(702, 'Master Forms Matrix List', 60, ':1:60:', 'List of QOP\'s Forms Work Instructions and Revisions.', 1445543057, 2, 1, 2, 7),
	(703, 'Policies', 689, ':1:689:', 'Policy ', 1445883298, 2, 1, 2, 3),
	(704, 'GDP Training', 1, ':1:', 'Power Point and Quiz', 1449586840, 2, 1, 2, 6);
/*!40000 ALTER TABLE `tblFolders` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
