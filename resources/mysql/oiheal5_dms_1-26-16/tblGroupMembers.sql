-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_dms.tblGroupMembers
CREATE TABLE IF NOT EXISTS `tblGroupMembers` (
  `groupID` int(11) NOT NULL DEFAULT '0',
  `userID` int(11) NOT NULL DEFAULT '0',
  `manager` smallint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `groupID` (`groupID`,`userID`),
  KEY `tblGroupMembers_user` (`userID`),
  CONSTRAINT `tblGroupMembers_group` FOREIGN KEY (`groupID`) REFERENCES `tblGroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tblGroupMembers_user` FOREIGN KEY (`userID`) REFERENCES `tblUsers` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table oiheal5_dms.tblGroupMembers: ~24 rows (approximately)
/*!40000 ALTER TABLE `tblGroupMembers` DISABLE KEYS */;
INSERT INTO `tblGroupMembers` (`groupID`, `userID`, `manager`) VALUES
	(1, 1, 1),
	(1, 2, 0),
	(1, 115, 1),
	(1, 119, 1),
	(1, 121, 1),
	(1, 134, 1),
	(2, 1, 0),
	(3, 1, 1),
	(3, 2, 1),
	(3, 113, 1),
	(3, 165, 0),
	(5, 115, 0),
	(5, 116, 0),
	(5, 117, 0),
	(5, 122, 0),
	(5, 123, 0),
	(5, 124, 0),
	(5, 125, 0),
	(5, 159, 0),
	(5, 160, 0),
	(5, 162, 0),
	(5, 174, 0),
	(5, 175, 0),
	(7, 116, 0),
	(8, 121, 0),
	(8, 129, 0),
	(8, 130, 0),
	(8, 131, 0),
	(8, 132, 0),
	(8, 133, 0);
/*!40000 ALTER TABLE `tblGroupMembers` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
