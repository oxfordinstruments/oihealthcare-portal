-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_dms.tblSessions
CREATE TABLE IF NOT EXISTS `tblSessions` (
  `id` varchar(50) NOT NULL DEFAULT '',
  `userID` int(11) NOT NULL DEFAULT '0',
  `lastAccess` int(11) NOT NULL DEFAULT '0',
  `theme` varchar(30) NOT NULL DEFAULT '',
  `language` varchar(30) NOT NULL DEFAULT '',
  `clipboard` text,
  `su` int(11) DEFAULT NULL,
  `splashmsg` text,
  PRIMARY KEY (`id`),
  KEY `tblSessions_user` (`userID`),
  CONSTRAINT `tblSessions_user` FOREIGN KEY (`userID`) REFERENCES `tblUsers` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table oiheal5_dms.tblSessions: ~43 rows (approximately)
/*!40000 ALTER TABLE `tblSessions` DISABLE KEYS */;
INSERT INTO `tblSessions` (`id`, `userID`, `lastAccess`, `theme`, `language`, `clipboard`, `su`, `splashmsg`) VALUES
	('0d1ec037e3d680003b9e27f855526031', 159, 1453304601, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('153b837388dd51f69231502638544115', 187, 1453480286, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('1641cc11e0a6698bc2e1e65f0076e71e', 159, 1453497623, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('2549027bc6906b5e98cbec0dc68dce1d', 180, 1453752839, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('2644ea708daa2bdaf90d7e77a11b610f', 165, 1453498902, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('281f3392489d6ede81becf788298723a', 164, 1453742099, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('2d0fe3fbe56bd525a8c93f2f28b0921e', 159, 1453822595, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('37b3dd3d3ca81a5c6474048eb124e45e', 165, 1453756073, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('412ed085c4fe2e9ff97e563db7915d56', 184, 1453383038, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('460c49e3bf7990a3b4b748003872942f', 116, 1453498661, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('46b03fa84baf154f371109bf7a53d73f', 126, 1453321416, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('4a5f78865a56425cb6529183931d9dcd', 120, 1453314127, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('4cb6fa6f33e5d59115ba59c22f5d0095', 165, 1453405742, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('57a2af23e953b6d86c7dceee9245ca13', 113, 1453826578, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('5957faa89ddee10b64e475588e85aa3f', 165, 1453825721, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('5ef37af5f571b73599a9770e59234fa5', 175, 1453424290, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('630e2fc85e75d4d4b2487dbb7331d8e9', 175, 1453500263, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('6c7ed5311c7d02b0ba2d927a097b7732', 120, 1453414259, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('72b90a95805396f609318f2b28e90d9a', 2, 1453824930, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('770e2bd20b7f40a895c88293acfc1fe2', 164, 1453476309, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('7995c206336f27ecac3c87ccf544699e', 122, 1453749079, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('869962b84d5b0364376793b41a33a4b2', 164, 1453324499, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('9484f7be8e05381cafbfbf19715b854e', 166, 1453484863, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('9a0f81e4bb0c97891961ebe7e486c342', 119, 1453750315, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('9a15aa0e69e1c6f62a5218d4bce7450d', 117, 1453824337, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('a8df76da6282a4af7afb77e4ea2d4c8d', 158, 1453752995, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('aa3067beb6196cfa7a62b0e98e7834a0', 116, 1453410319, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('aab2e35ccb06419f7bb4f834eaa7eb9f', 175, 1453424998, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('b5fdf222a108db53e3f0d1070fda051f', 124, 1453837220, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('b845d173bed8f1c3ec0d728cb29eda4a', 180, 1453316960, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('b8512afed6322a1881c41d8710b4aea1', 116, 1453318398, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('c1c86e81eb100b475c19870294de4395', 117, 1453394083, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('cbd4bdf6acfab7a9178a64a177a78931', 126, 1453821819, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('cfde5ca142fefb8db07f5971df2a49b5', 186, 1453489726, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('cff7a85deea59a1ebc19e496e2947345', 168, 1453837562, 'bootstrap', 'en_GB', '{"docs":["73"],"folders":[]}', 0, NULL),
	('d162ef82df197cb04693f8d0740e3b4e', 180, 1453387569, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('d4b134742812e0363888b96f94f8159a', 165, 1453323131, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('d52672a85af8b5100ad5ac5a05c518b3', 119, 1453841977, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('d5efe11b4582590988a38f7fe5583811', 119, 1453491447, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('d9a6536b4734f900e4413b42a509849c', 184, 1453303313, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('ed62ab59ce32f2ee85d457901e861e4c', 185, 1453392103, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('f0575daaa1531d83ab497e0accbb48f6', 181, 1453819136, 'bootstrap', 'en_GB', NULL, 0, NULL),
	('f6b2aec49af60e3d4137fd794be2d770', 181, 1453484863, 'bootstrap', 'en_GB', NULL, 0, NULL);
/*!40000 ALTER TABLE `tblSessions` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
