-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_dms.tblUsers
CREATE TABLE IF NOT EXISTS `tblUsers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(50) DEFAULT NULL,
  `pwd` varchar(50) DEFAULT NULL,
  `fullName` varchar(100) DEFAULT NULL,
  `email` varchar(70) DEFAULT NULL,
  `language` varchar(32) NOT NULL,
  `theme` varchar(32) NOT NULL,
  `comment` text NOT NULL,
  `role` smallint(1) NOT NULL DEFAULT '0',
  `hidden` smallint(1) NOT NULL DEFAULT '0',
  `pwdExpiration` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `loginfailures` tinyint(4) NOT NULL DEFAULT '0',
  `disabled` smallint(1) NOT NULL DEFAULT '0',
  `quota` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=188 DEFAULT CHARSET=utf8;

-- Dumping data for table oiheal5_dms.tblUsers: ~61 rows (approximately)
/*!40000 ALTER TABLE `tblUsers` DISABLE KEYS */;
INSERT INTO `tblUsers` (`id`, `login`, `pwd`, `fullName`, `email`, `language`, `theme`, `comment`, `role`, `hidden`, `pwdExpiration`, `loginfailures`, `disabled`, `quota`) VALUES
	(1, 'admin', 'd41f11fa067195e977aedd503ffc8982', 'Administrator', 'support@oiserviceportal.com', 'en_GB', 'bootstrap', '', 1, 1, '0000-00-00 00:00:00', 0, 0, 0),
	(2, 'mardikianj', '', 'Julie Mardikian', 'julie.mardikian@oxinst.com', 'en_GB', 'bootstrap', '', 1, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(112, 'martinezr', '', 'Rick Martinez', 'ricardo.martinez@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(113, 'davisj', '', 'Justin Davis', 'justin.davis@oxinst.com', 'en_GB', 'bootstrap', '', 1, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(114, 'tempreception', '', 'Temp Reception', 'reception.oiservice@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(115, 'simmonss', '', 'Scott Simmons', 'scott.simmons@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(116, 'billingsb', '', 'Bobbi Billings', 'bobbi.billings@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(117, 'clarkf', '', 'Felice Clark', 'felice.clark@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(118, 'grubbsd', '', 'Dennis Grubbs', 'dennis.grubbs@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(119, 'corea', '', 'Anthony Core', 'anthony.core@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(120, 'thrushr', '', 'RJ Thrush', 'raymond.thrush@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(121, 'gambogid', '', 'Dan  Gambogi ', 'dan.gambogi@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(122, 'hengemuhlew', '', 'Will  Hengemuhle ', 'will.hengemuhle@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(123, 'oliphantr', '', 'Rosario  Oliphant', 'rosario.oliphant@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(124, 'Tokushigeg', '', 'Glenn Tokushige', 'glenn.tokushige@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(125, 'fosterc', '', 'Chloe Foster', 'chloe.foster@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(126, 'somarribaj', '', 'Julio Somarriba', 'julio.somarriba@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(127, 'ramosr', '', 'Roberto  Ramos', 'roberto.ramos@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(128, 'neufeldj', '', 'Jason Neufeld', 'jason.neufeld@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(129, 'freundt', '', 'Tom  Freund', 'tom.freund@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(130, 'goberp', '', 'Phillip Gober', 'phillip.gober@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(131, 'alleng', '', 'Gregg Allen', 'gregg.allen@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(132, 'gloverw', '', 'Will  Glover', 'will.glover@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(133, 'SeilerD', '', 'David  Seiler', 'david.seiler@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(134, 'matthewsc', '', 'Chris  Matthews', 'chris.matthews@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(135, 'jonesk', '', 'Kiyon Jones', 'kiyon.jones@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(136, 'corec', '', 'Chris Core', 'chris.core@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(137, 'retanar', '', 'Roy  Retana', 'roy.retana@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(138, 'corbittj', '', 'Jeff Corbitt', 'jeff.corbitt@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(139, 'gustmanr ', '', 'Richard  Gustman ', 'richard.gustman@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(140, 'kuhlb', '', 'Bob Kuhl', 'bob.kuhl@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(141, 'StevensonK', '', 'Kirk Stevenson', 'kirk.stevenson@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(142, 'pettitm', '', 'Marc Pettit', 'marc.pettit@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(143, 'frazierm', '', 'Mike Frazier', 'mike.frazier@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(144, 'houzer', '', 'Robert Houze', 'robert.houze@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(145, 'banda', '', 'Adam Band', 'adam.band@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(146, 'dulaneyc', '', 'Cory Dulaney', 'cory.dulaney@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(147, 'galvackyd', '', 'Dan Galvacky', 'dan.galvacky@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(148, 'costar', '', 'Robert  Costa ', 'robert.costa@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(149, 'reiglej', '', 'Jeremy Reigle', 'jeremy.reigle@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(150, 'bringolfk', '', 'Kurt Bringolf', 'kurt.bringolf@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(151, 'abshearb', '', 'Brenda Abshear', 'brenda.abshear@oxinst.com', 'en_GB', 'bootstrap', '', 1, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(152, 'shevackj', '', 'Jon Shevack', 'jon.shevack@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(153, 'hodgem', '', 'Mike Hodge', 'mike.hodge@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(154, 'varnerd', '', 'David Varner', 'david.varner@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(155, 'moralesp', '', 'Philip Morales', 'philip.morales@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(156, 'darwisht', '', 'Tarek Darwish', 'tarek.darwish@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(157, 'mangs', '', 'Steve Mang', 'steve.mang@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(158, 'butlerj', '', 'John Butler', 'john.butler@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(159, 'penningtons', '', 'Steve Pennington', 'steve.pennington@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(160, 'shraderr', '', 'Ryan  Shrader', 'ryan.shrader@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(161, 'sylvesterh', '', 'Harry Sylvester', 'harry.sylvester@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(162, 'hahnl', '', 'Linda Hahn', 'linda.hahn@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(163, 'straszewskib', '', 'Beth Straszewski', 'beth.straszewski@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(164, 'newtonk', '', 'Kim Newton', 'kim.newton@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(165, 'hennessyl', '', 'Lacy Hennessy', 'lacy.hennessy@oxinst.com', 'en_GB', 'bootstrap', '', 1, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(166, 'thomsonc', '', 'Cam Thomson', 'cam.thomson@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(167, 'coyled', '', 'Dale Coyle', 'dale.coyle@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(168, 'jacksonb', '', 'Brandy Jackson', 'brandy.jackson@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(169, 'pelhamj', '', 'Jenifer Pelham', 'jenifer.pelham@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(170, 'davisc', '', 'Chris  Davis', 'chris.davis@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(171, 'grayg', '', 'Gordon Gray', 'gordon.gray@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(172, 'sidhui', '', 'Iqbal Sidhu', 'iqbal.sidhu@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(173, 'stancatod', '', 'Denise Stancato', 'denise.stancato@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(174, 'gilsonr', '', 'Rob Gilson', 'rob.gilson@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(175, 'caseyw', '', 'Bill Casey', 'bill.casey@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(176, 'biggsr', '', 'Richard Biggs', 'richard.biggs@qrccentral.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(177, 'rogersj', '', 'Jeff Rogers', 'jeff.rogers@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(178, 'devaultm', '', 'Mike DeVault', 'mike.devault@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(179, 'luckhardtd', '', 'Doug Luckhardt', 'doug.luckhardt@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(180, 'prattg', '', 'Glenn Pratt', 'glenn.pratt@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(181, 'mellol', '', 'Lynda Mello', 'lynda.mello@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(182, 'flechaa', '', 'Al Flecha', 'al.flecha@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(183, 'tejedap', '', 'Pedro Tejeda', 'pedro.tejeda@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(184, 'condesa', '', 'Angelito Condes', 'angelito.condes@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(185, 'taylork', '', 'Karin Taylor', 'karin.taylor@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(186, 'cheveldayoffc', '', 'Chase Cheveldayoff', 'chase.cheveldayoff@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL),
	(187, 'serranoj', '', 'Jorge Serrano', 'jorge.serrano@oxinst.com', 'en_GB', 'bootstrap', '', 0, 0, '0000-00-00 00:00:00', 0, 0, NULL);
/*!40000 ALTER TABLE `tblUsers` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
