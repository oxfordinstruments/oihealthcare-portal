-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.facilities
DROP TABLE IF EXISTS `facilities`;
CREATE TABLE IF NOT EXISTS `facilities` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `facility_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_unique_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT '0',
  `name` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'FM > Site',
  `address` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'FM > address',
  `city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'FM > city',
  `state` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'FM > state',
  `zip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'FM > zip',
  `lat` varchar(50) COLLATE utf8_unicode_ci DEFAULT '0',
  `lng` varchar(50) COLLATE utf8_unicode_ci DEFAULT '0',
  `timezone` varchar(200) COLLATE utf8_unicode_ci DEFAULT 'UTC',
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'FM > primary phone',
  `fax` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'FM > site fax',
  `contact_name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_title` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_cell` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_email` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bill_facility` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `bill_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bill_title` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bill_phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bill_email` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bill_address` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bill_city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bill_state` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bill_zip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `credit_hold` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `credit_hold_date` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `credit_hold_notes` varchar(4096) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` varchar(4096) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_list` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Temp email list for sending reports to',
  `archived` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `has_files` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `nps_email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'email addy to send nps survey to',
  `nps_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'persons name for nps survey',
  `unique_id` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `edited_by` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `edited_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `temp_site_name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `temp_site_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`unique_id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
