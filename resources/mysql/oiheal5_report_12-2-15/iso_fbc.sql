-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.iso_fbc
DROP TABLE IF EXISTS `iso_fbc`;
CREATE TABLE IF NOT EXISTS `iso_fbc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_unique_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `system_unique_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `request_unique_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `report_unique_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `description_uid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resolution` text COLLATE utf8_unicode_ci,
  `resolution_uid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resolution_target_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verification` text COLLATE utf8_unicode_ci,
  `verification_uid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verification_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `car` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `car_unique_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `car_unneeded_uid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `car_unneeded_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `car_unneeded_reason` text COLLATE utf8_unicode_ci,
  `created_uid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `edited_uid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `edited_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `deleted_uid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_reason` text COLLATE utf8_unicode_ci,
  `closed` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `closed_uid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `closed_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `has_files` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `unique_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id` (`unique_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
