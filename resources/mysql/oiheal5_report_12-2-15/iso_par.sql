-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.iso_par
DROP TABLE IF EXISTS `iso_par`;
CREATE TABLE IF NOT EXISTS `iso_par` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'location of incident',
  `department` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'department of incident',
  `title` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'title for capa',
  `finding` text COLLATE utf8_unicode_ci COMMENT 'filled out by user',
  `root_cause` text COLLATE utf8_unicode_ci COMMENT 'filled out by imr user',
  `root_cause_uid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `root_cause_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `correction` text COLLATE utf8_unicode_ci COMMENT 'filled out by imr user',
  `correction_uid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `correction_target_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `corrected_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verification` text COLLATE utf8_unicode_ci COMMENT 'filled out by imr user',
  `verification_uid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verification_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_uid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `edited_uid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `edited_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `deleted_uid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_reason` text COLLATE utf8_unicode_ci,
  `closed` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `closed_uid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `closed_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `has_files` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `unique_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id` (`unique_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
