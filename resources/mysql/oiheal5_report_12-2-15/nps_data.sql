-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.nps_data
DROP TABLE IF EXISTS `nps_data`;
CREATE TABLE IF NOT EXISTS `nps_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fiscal_quarter` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fiscal_year` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `survey_send_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `surveys_sent` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `surveys_received` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `response_percent` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nps_score` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `promoters` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passives` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `demoters` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nps_0` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nps_1` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nps_2` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nps_3` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nps_4` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nps_5` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nps_6` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nps_7` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nps_8` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nps_9` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nps_10` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unique_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
