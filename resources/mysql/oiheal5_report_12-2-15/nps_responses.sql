-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.nps_responses
DROP TABLE IF EXISTS `nps_responses`;
CREATE TABLE IF NOT EXISTS `nps_responses` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `score` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `comment` varchar(2048) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `can_contact` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `contact_info` varchar(1000) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `unique_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quarter_unique_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `domain` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `browser` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `unique_id` (`unique_id`),
  KEY `quarter_unique_id` (`quarter_unique_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Net Promoter Score';

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
