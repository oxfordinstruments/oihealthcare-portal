-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.remote_assignments
DROP TABLE IF EXISTS `remote_assignments`;
CREATE TABLE IF NOT EXISTS `remote_assignments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `system_unique_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `remote_serial` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'eth0 mac',
  `system_ip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'console ip',
  `system_port` varchar(50) COLLATE utf8_unicode_ci DEFAULT '22',
  `system_uid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'console login',
  `connection_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ssh or telnet',
  `edited_by` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `edited_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `site_unique_id` (`system_unique_id`),
  KEY `edited_by` (`edited_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
