-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.remote_computers
DROP TABLE IF EXISTS `remote_computers`;
CREATE TABLE IF NOT EXISTS `remote_computers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vpn_mac` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vpn_ip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eth0_mac` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'serial number',
  `eth0_ip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eth0_gw` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eth0_netmask` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eth1_ip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uptime` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'In Seconds',
  `idletime` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'In Seconds',
  `geo_ip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `geo_lat` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `geo_long` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `geo_state` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `geo_country` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `geo_timezone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `version` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'update version',
  `reported_online` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `eth0_mac` (`eth0_mac`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='This table is populated from the remote computers';

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
