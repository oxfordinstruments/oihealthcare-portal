-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.sales_contracts
DROP TABLE IF EXISTS `sales_contracts`;
CREATE TABLE IF NOT EXISTS `sales_contracts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `revision` int(11) DEFAULT '0',
  `modality` enum('CT','MR','PET','NM') COLLATE utf8_unicode_ci DEFAULT NULL,
  `agreement_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_address` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_city` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_state` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_zip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_address` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_city` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_state` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_zip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `install_address` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `install_city` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `install_zip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purchase_price` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `execution_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `execution_deposit` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Column 19` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
