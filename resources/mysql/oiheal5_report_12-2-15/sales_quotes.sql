-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.sales_quotes
DROP TABLE IF EXISTS `sales_quotes`;
CREATE TABLE IF NOT EXISTS `sales_quotes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `revision` int(11) DEFAULT '0',
  `quote_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'rep initials + date + rev',
  `title` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_address` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_state` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_zip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_email` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emailed_quote` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Y N',
  `system_type` int(11) DEFAULT NULL,
  `system_table` int(11) DEFAULT NULL,
  `system_console` int(11) DEFAULT NULL,
  `system_price` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `warranty` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Y N',
  `warranty_months` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `agreement_percent` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shippment_percent` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `final_percent` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sales_tax_percent` varchar(10) COLLATE utf8_unicode_ci DEFAULT '0',
  `fees` varchar(20) COLLATE utf8_unicode_ci DEFAULT '0',
  `details` text COLLATE utf8_unicode_ci,
  `created_by` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `deleted_by` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contracted` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `unique_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_sales_quotes_misc_consoles` (`system_console`),
  KEY `FK_sales_quotes_systems_types` (`system_type`),
  KEY `FK_sales_quotes_misc_tables` (`system_table`),
  KEY `FK_sales_quotes_users_2` (`created_by`),
  KEY `FK_sales_quotes_users_3` (`deleted_by`),
  CONSTRAINT `FK_sales_quotes_users_3` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`uid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_sales_quotes_misc_consoles` FOREIGN KEY (`system_console`) REFERENCES `misc_consoles` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_sales_quotes_misc_tables` FOREIGN KEY (`system_table`) REFERENCES `misc_tables` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_sales_quotes_systems_types` FOREIGN KEY (`system_type`) REFERENCES `systems_types` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_sales_quotes_users_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`uid`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
