-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.sales_quotes_sw_options
DROP TABLE IF EXISTS `sales_quotes_sw_options`;
CREATE TABLE IF NOT EXISTS `sales_quotes_sw_options` (
  `option_id` int(11) DEFAULT NULL,
  `sales_quotes_id` int(11) DEFAULT NULL,
  KEY `FK_sales_quotes_sw_options_misc_sw_options` (`option_id`),
  KEY `FK_sales_quotes_sw_options_sales_quotes` (`sales_quotes_id`),
  CONSTRAINT `FK_sales_quotes_sw_options_misc_sw_options` FOREIGN KEY (`option_id`) REFERENCES `misc_sw_options` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_sales_quotes_sw_options_sales_quotes` FOREIGN KEY (`sales_quotes_id`) REFERENCES `sales_quotes` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
