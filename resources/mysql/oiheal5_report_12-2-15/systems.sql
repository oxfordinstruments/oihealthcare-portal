-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.systems
DROP TABLE IF EXISTS `systems`;
CREATE TABLE IF NOT EXISTS `systems` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `system_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facility_unique_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT '0',
  `nickname` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'FM > Site',
  `contact_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'FM > primary',
  `contact_title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'FM > primary title',
  `contact_phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_cell` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_email` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'FM > primary email',
  `system_type` int(11) DEFAULT NULL,
  `sw_ver` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trailer_unique_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_installed` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'FM > install',
  `system_serial` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tube_covered` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `he_monitor` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `he_monitor_serial` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remote_access` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `acceptance_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `log_book` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `contract_type` int(11) DEFAULT NULL,
  `contract_terms` varchar(4096) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contract_start_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'FM > contract date',
  `contract_end_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'FM > contract end date',
  `contract_hours` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pre_paid` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `labor_reg_rate` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `labor_ot_rate` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `travel_reg_rate` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `travel_ot_rate` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `credit_hold` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `credit_hold_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `credit_hold_notes` varchar(4096) COLLATE utf8_unicode_ci DEFAULT NULL,
  `warranty_start_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `warranty_end_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'FM > wty end',
  `pm_freq` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_pm` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `notes` varchar(4096) COLLATE utf8_unicode_ci DEFAULT NULL,
  `has_files` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `mr_lhe_list` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mr_lhe_list_update` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mr_lhe_press` varchar(20) COLLATE utf8_unicode_ci DEFAULT '4',
  `mr_lhe_contact` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mr_lhe_email` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mr_lhe_phone` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `archived` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `unique_id` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `edited_by` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `edited_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `old_system_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'IDs prior to 2015-8-12',
  PRIMARY KEY (`unique_id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Customer sites table';

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
