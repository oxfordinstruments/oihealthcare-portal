-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.systems_coils
DROP TABLE IF EXISTS `systems_coils`;
CREATE TABLE IF NOT EXISTS `systems_coils` (
  `coil_id` int(11) NOT NULL,
  `systems_types_id` int(11) NOT NULL,
  KEY `coil_id` (`coil_id`),
  KEY `systems_types_id` (`systems_types_id`),
  CONSTRAINT `FK_systems_coils_misc_coils` FOREIGN KEY (`coil_id`) REFERENCES `misc_coils` (`id`),
  CONSTRAINT `FK_systems_coils_systems_types` FOREIGN KEY (`systems_types_id`) REFERENCES `systems_types` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
