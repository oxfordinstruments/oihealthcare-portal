-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.systems_face_sheet
DROP TABLE IF EXISTS `systems_face_sheet`;
CREATE TABLE IF NOT EXISTS `systems_face_sheet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `system_unique_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `completed` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `quickbooks_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quickbooks_user` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quickbooks_initials` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quickbooks_needed` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `quickbooks_complete` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `quickbooks_notes` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logbook_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logbook_user` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logbook_initials` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logbook_needed` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `logbook_complete` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `logbook_notes` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `afterhours_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `afterhours_user` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `afterhours_initials` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `afterhours_needed` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `afterhours_complete` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `afterhours_notes` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idsticker_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idsticker_user` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idsticker_initials` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idsticker_needed` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `idsticker_complete` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `idsticker_notes` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `basket_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `basket_user` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `basket_initials` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `basket_needed` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `basket_complete` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `basket_notes` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `survey_user` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `survey_initials` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `survey_needed` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `survey_complete` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `survey_notes` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `site_unique_id` (`system_unique_id`),
  KEY `quickbooks_user` (`quickbooks_user`),
  KEY `logbook_user` (`logbook_user`),
  KEY `afterhours_user` (`afterhours_user`),
  KEY `idsticker_user` (`idsticker_user`),
  KEY `basket_user` (`basket_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
