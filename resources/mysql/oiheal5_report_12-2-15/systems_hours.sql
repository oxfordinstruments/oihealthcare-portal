-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.systems_hours
DROP TABLE IF EXISTS `systems_hours`;
CREATE TABLE IF NOT EXISTS `systems_hours` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `report_id` int(11) NOT NULL,
  `date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reg_labor` varchar(10) COLLATE utf8_unicode_ci DEFAULT '0',
  `ot_labor` varchar(10) COLLATE utf8_unicode_ci DEFAULT '0',
  `reg_travel` varchar(10) COLLATE utf8_unicode_ci DEFAULT '0',
  `ot_travel` varchar(10) COLLATE utf8_unicode_ci DEFAULT '0',
  `unique_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `system_unique_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `report_id` (`report_id`),
  KEY `unique_id` (`unique_id`),
  KEY `site_unique_id` (`system_unique_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
