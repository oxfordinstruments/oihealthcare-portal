-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.systems_reports
DROP TABLE IF EXISTS `systems_reports`;
CREATE TABLE IF NOT EXISTS `systems_reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_id` int(11) DEFAULT NULL,
  `user_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `system_id` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `system_unique_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facility_unique_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `system_nickname` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `complaint` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prob_found` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `service` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` varchar(2000) COLLATE utf8_unicode_ci DEFAULT '',
  `prev_report_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `engineer` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `assigned_engineer` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `finalize_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `po_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contract_override` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slice_mas_count` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gantry_rev` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sys_scn_secs` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tube_scn_secs` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `helium_level` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vessel_pressure` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `recon_ruo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cold_ruo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `compressor_hours` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `compressor_pressure` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `downtime` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pm` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pm_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `refrig_service` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `system_status` int(11) DEFAULT NULL,
  `report_edited` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'N',
  `report_edited_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `report_edited_by` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `report_edited_notes` varchar(2000) COLLATE utf8_unicode_ci DEFAULT '',
  `invoice_required` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `valuation_complete` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `valuation_uid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `valuation_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `valuation_do_not_invoice` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'N',
  `invoice_labor_reg_rate` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoice_labor_ot_rate` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoice_travel_reg_rate` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoice_travel_ot_rate` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoice_labor_reg` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoice_labor_ot` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoice_travel_reg` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoice_travel_ot` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoice_shipping` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoice_expense` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoiced` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `invoice_marked_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoiced_date_edit` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoiced_uid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoiced_uid_edit` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoice_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoice_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `new_report` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'Y' COMMENT 'Used by FM',
  `deleted` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `deleted_by` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `archived` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `phone_fix` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `fbc` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `unique_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `has_files` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `fm_ref` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fm_site_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`unique_id`),
  KEY `report_id` (`report_id`),
  KEY `id` (`id`),
  KEY `FK_systems_reports_users` (`user_id`),
  KEY `FK_systems_reports_users_2` (`engineer`),
  KEY `FK_systems_reports_users_3` (`assigned_engineer`),
  KEY `FK_systems_reports_users_4` (`report_edited_by`),
  KEY `FK_systems_reports_users_5` (`valuation_uid`),
  KEY `FK_systems_reports_users_6` (`invoiced_uid`),
  KEY `FK_systems_reports_users_7` (`deleted_by`),
  KEY `FK_systems_reports_systems_status` (`system_status`),
  KEY `FK_systems_reports_systems` (`system_unique_id`),
  KEY `FK_systems_reports_facilities` (`facility_unique_id`),
  CONSTRAINT `FK_systems_reports_facilities` FOREIGN KEY (`facility_unique_id`) REFERENCES `facilities` (`unique_id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_systems_reports_systems` FOREIGN KEY (`system_unique_id`) REFERENCES `systems` (`unique_id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_systems_reports_systems_status` FOREIGN KEY (`system_status`) REFERENCES `systems_status` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_systems_reports_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`uid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_systems_reports_users_2` FOREIGN KEY (`engineer`) REFERENCES `users` (`uid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_systems_reports_users_3` FOREIGN KEY (`assigned_engineer`) REFERENCES `users` (`uid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_systems_reports_users_4` FOREIGN KEY (`report_edited_by`) REFERENCES `users` (`uid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_systems_reports_users_5` FOREIGN KEY (`valuation_uid`) REFERENCES `users` (`uid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_systems_reports_users_6` FOREIGN KEY (`invoiced_uid`) REFERENCES `users` (`uid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_systems_reports_users_7` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`uid`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='Service reports table';

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
