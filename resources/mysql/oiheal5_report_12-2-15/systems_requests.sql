-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.systems_requests
DROP TABLE IF EXISTS `systems_requests`;
CREATE TABLE IF NOT EXISTS `systems_requests` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `request_num` int(11) DEFAULT NULL,
  `engineer` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sender` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `system_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `system_unique_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facility_unique_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `system_nickname` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `request_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `onsite_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `initial_call_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fbc` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `response_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `problem_reported` text COLLATE utf8_unicode_ci,
  `customer_actions` text COLLATE utf8_unicode_ci,
  `system_status` int(11) DEFAULT NULL,
  `po_num` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contract_override` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pm` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `refrig_service` varchar(5) COLLATE utf8_unicode_ci DEFAULT 'N',
  `status` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `report_started` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `new_request` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `repeat_call` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `repeat_call_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `under_30min` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `response_minutes` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoice_required` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `edited_by` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `edited_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `deleted_by` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `archived` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `archived_prev_site` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unique_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fm_ref` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fm_site_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_systems_requests_users` (`engineer`),
  KEY `FK_systems_requests_users_2` (`sender`),
  KEY `FK_systems_requests_systems` (`system_unique_id`),
  KEY `FK_systems_requests_facilities` (`facility_unique_id`),
  KEY `FK_systems_requests_users_3` (`edited_by`),
  KEY `FK_systems_requests_users_4` (`deleted_by`),
  KEY `FK_systems_requests_systems_status` (`system_status`),
  CONSTRAINT `FK_systems_requests_facilities` FOREIGN KEY (`facility_unique_id`) REFERENCES `facilities` (`unique_id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_systems_requests_systems` FOREIGN KEY (`system_unique_id`) REFERENCES `systems` (`unique_id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_systems_requests_systems_status` FOREIGN KEY (`system_status`) REFERENCES `systems_status` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_systems_requests_users` FOREIGN KEY (`engineer`) REFERENCES `users` (`uid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_systems_requests_users_2` FOREIGN KEY (`sender`) REFERENCES `users` (`uid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_systems_requests_users_3` FOREIGN KEY (`edited_by`) REFERENCES `users` (`uid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_systems_requests_users_4` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`uid`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Service request table';

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
