-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.systems_status_updates
DROP TABLE IF EXISTS `systems_status_updates`;
CREATE TABLE IF NOT EXISTS `systems_status_updates` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `system_unique_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` text COLLATE utf8_unicode_ci,
  `date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `report_unique_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `from_email` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='a log of the evening updates sent from the site';

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
