-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.systems_tables
DROP TABLE IF EXISTS `systems_tables`;
CREATE TABLE IF NOT EXISTS `systems_tables` (
  `table_id` int(11) DEFAULT NULL,
  `systems_types_id` int(11) DEFAULT NULL,
  KEY `FK_systems_consoles_misc_consoles` (`table_id`),
  KEY `FK_systems_consoles_systems_types` (`systems_types_id`),
  CONSTRAINT `FK_systems_tables_misc_tables` FOREIGN KEY (`table_id`) REFERENCES `misc_tables` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_systems_tables_systems_types` FOREIGN KEY (`systems_types_id`) REFERENCES `systems_types` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
