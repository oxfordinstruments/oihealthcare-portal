-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.trailers
DROP TABLE IF EXISTS `trailers`;
CREATE TABLE IF NOT EXISTS `trailers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trailer_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nickname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `archived` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `assigned` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `system_unique_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mfg` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mfg_year` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vin` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `license_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `license_state` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `license_expire` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pop_outs` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `size` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `has_files` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `edited_by` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `edited_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unique_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`unique_id`),
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
