-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `pwd` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firstname` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `initials` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` varchar(5) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` varchar(50) COLLATE utf8_unicode_ci DEFAULT '0',
  `lng` varchar(50) COLLATE utf8_unicode_ci DEFAULT '0',
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cell` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `carrier` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'America/New_York',
  `default_role` int(11) DEFAULT '0',
  `serviced_systems` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'comma sep last serviced sites',
  `last_login` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'date',
  `logged_in` varchar(2) COLLATE utf8_unicode_ci DEFAULT '0' COMMENT '1/0',
  `timezone_reminder` int(11) DEFAULT '0',
  `ip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `domain` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pwd_chg_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'date pwd will ask to change',
  `pwd_chg_remind` int(11) DEFAULT '0',
  `customer_unique_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'For Customers Login Only',
  `secret_1` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `secret_1_id` int(11) DEFAULT NULL,
  `secret_2` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `secret_2_id` int(11) DEFAULT NULL,
  `created_by` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `edited_by` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `edited_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `linux_pwd` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `registration_unique_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='User info table';

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
