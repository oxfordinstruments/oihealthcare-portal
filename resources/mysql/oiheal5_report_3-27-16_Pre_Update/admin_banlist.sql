-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.admin_banlist
DROP TABLE IF EXISTS `admin_banlist`;
CREATE TABLE IF NOT EXISTS `admin_banlist` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ban` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `domain` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banned_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='list of banned users/ip/domains';

-- Dumping data for table oiheal5_report.admin_banlist: ~19 rows (approximately)
/*!40000 ALTER TABLE `admin_banlist` DISABLE KEYS */;
INSERT INTO `admin_banlist` (`id`, `ban`, `ip`, `user`, `domain`, `banned_by`) VALUES
	(1, 'Matt tout', NULL, 'x', NULL, 'root'),
	(2, 'ToutM', NULL, 'x', NULL, 'root'),
	(3, 'genkt-049-222.t-mobile.co.uk', NULL, NULL, 'x', 'root'),
	(4, 'guest', NULL, 'x', NULL, 'root'),
	(5, 'alahmafa', NULL, 'x', NULL, 'root'),
	(6, 'harbya0d', NULL, 'x', NULL, 'root'),
	(7, 'hammmh0c', NULL, 'x', NULL, 'root'),
	(8, 'gridlesshosting.com', NULL, NULL, 'x', 'root'),
	(9, '198.36.39.2', NULL, NULL, 'x', 'root'),
	(10, '198.36.39.184', NULL, NULL, 'x', 'root'),
	(11, '198.36.40.2', NULL, NULL, 'x', 'root'),
	(12, '198.36.40.4', NULL, NULL, 'x', 'root'),
	(13, 'ghangm0a', NULL, 'x', NULL, 'root'),
	(14, '198.36.39.182', 'x', NULL, NULL, 'root'),
	(15, '166.137.100.19', 'x', NULL, NULL, 'root'),
	(16, 'akrmspsrvz9ts105-dmz.mycingular.net', NULL, NULL, 'x', 'root'),
	(33, '212.38.169.75', 'x', NULL, NULL, 'davisj'),
	(34, '212.161.78.2', 'x', NULL, NULL, 'davisj'),
	(35, '212.38.169.76', 'x', NULL, NULL, 'davisj');
/*!40000 ALTER TABLE `admin_banlist` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
