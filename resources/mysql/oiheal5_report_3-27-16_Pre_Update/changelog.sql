-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.changelog
DROP TABLE IF EXISTS `changelog`;
CREATE TABLE IF NOT EXISTS `changelog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `version` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `commit_hash` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `change` text COLLATE utf8_unicode_ci NOT NULL,
  `tests` text COLLATE utf8_unicode_ci NOT NULL,
  `uid` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table oiheal5_report.changelog: 35 rows
/*!40000 ALTER TABLE `changelog` DISABLE KEYS */;
INSERT INTO `changelog` (`id`, `date`, `version`, `commit_hash`, `change`, `tests`, `uid`) VALUES
	(1, '2015-11-21T00:00:00Z', '2.0.202', 'ced89df5c90ca29612a0e23add4784c20e245d81', 'Features added:\r\nOiVision Data shown on systems page\r\nPre-Paid Service yes/no added to edit systems page under contract tab\r\nPre-paid only systems will alert dispatcher and show on systems page\r\nSystems map legend now allows to filter by each contract type\r\nLinks to the DMS and KB are on the login page\r\nFinance dashboard now shows graphs of the contracts and warrenties expiring in the next 30 and 120 days. Click them to show the systems.\r\nClicking PMs due on the engineer\'s dashboard will take the user to the systems page.\r\nFull revamp of the underlying groups/roles/permissions systems for users\r\nFacesheet sign-off proceedure removed\r\nService requests can no longer be assigned to unassigned or contractor none\r\nPassword Expire notice 14 days prior to expiration\r\nAdded Edit Customer button to systems page\r\nAdded email users to admin dashboard\r\nIssue tracking system implemented\r\n\r\nOxford Instruments has implemented a validation process for the OiHSP. So beginning after this update all changes to the OiHSP will be logged into this changelog and the steps taken to test the changes will be recorded.\r\n', 'All major functions of the OiHSP were tested and found acceptable.', 'Justin Davis'),
	(2, '2015-11-22T00:00:00Z', '2.0.205', 'c8dbf4cd9b692c4c1b03b6c804384c2e85c68bf3', 'Bug Fix:\r\nDuring the penetration test performed by Oxford it was found that a possible sql injection attack could occur by the error page displaying the mysql error messages to the user. Changed sql result check if statments, to log the sql, sql error, and return to the user a generic error rather than the mysql error. \r\n\r\n', 'Tested the generic error return by purposly sending bad data to a test form. The process was repeated many time until I was confident of the results always returning the generic error.', 'Justin Davis'),
	(3, '2015-11-22T00:00:00Z', '2.0.211', '7e0cbd383cd6312653e9631d3ce1f21c221c0e28', 'Removed the un-used err section from the log files.\r\nAdded the page\'s line number reporting the error to the log script.', 'No tests were needed to validate the changes', 'Justin Davis'),
	(4, '2015-11-22T00:00:00Z', '2.0.214', '9a64835d7282d07f91289e4907ef1eba652904f6', 'Added a link to view the changelog from the admin dashboard\r\nUpdated the changelog input pages to have tests run sections\r\nFixed sql statement in tools.php the word select was spelled wrong.\r\n', 'Verified the admin dashboard showed the link.\r\nInput test change log entries to test the new form field\r\nTested the tools page to validate that the users were shown correctly', 'Justin Davis'),
	(5, '2015-11-23T00:00:00Z', '2.0.216', '8e7ddcbdd8ef095b8af5bf37e27fc2e3e501aa2d', '* Bug Fix:\r\nchangelog_view.php was displaying the log in the wrong order of events.\r\n', 'Viewed the changelog to verify the order of the entries is correct', 'Justin Davis'),
	(6, '2015-11-23T00:00:00Z', '2.0.219', '8b924115a22ccb65009cf930961aac5c59e1aa5d', 'Bug Fix:\r\nFound the $path was not being pushed to the logger class\r\n', 'Ran the password_expire_check.php several time to validate that is was working properly', 'Justin Davis'),
	(7, '2015-11-23T00:00:00Z', '2.0.221', 'cb726e8e6f11205b6b06ebe30327f106bd047dbf', 'Feature: Added current date/time to top of page', 'View the changelog to verify the current date/time showed at the top of the page', 'Justin Davis'),
	(8, '2015-11-24T00:00:00Z', '2.0.222', '428726d2cae06136b60d83a6137b149b6b5b70df', 'Bug Fix: SQL for getting the primary and secondary engineer\'s email was still using the old GRPP model.\r\nModified the sql to the new GRPP model.\r\nThe sql for getting the email addresses for everyone else wanting a email was looking for the system unique id from the wrong var.\r\n', 'Created a service request and verified the request email was sent to all of the recipients. Deleted the service request and verified the email was sent to all the recipients.', 'Justin Davis'),
	(9, '2015-11-24T00:00:00Z', '2.0.223', '07ab89140c1bd297f34b3483b309105008908d0e', 'Feature: Added links to the admin dashboard to clear the logs.', 'Tested the links on all of the logs', 'Justin Davis'),
	(10, '2015-11-25T00:00:00Z', '2.0.224', 'a1b49f4040b0107ca34066254e002f20a8c67d6a', 'Bug Fix: Found that the ldap class was receiving a protocol error when doing an ldap_list. This ended up being deref being set to 9 which is no longer a valid deref. Changed to deref 0.\r\n', 'Ran the update_ldap script to verify the issue was resolved', 'Justin Davis'),
	(11, '2015-11-25T00:00:00Z', '2.0.225', '8d4ae16d9c707a1361891f40094b24a9906c612a', 'Bug Fix: Checking for blank status was looking for the post var tweet. Changed the var to status.', 'No tests were needed', 'Justin Davis'),
	(12, '2015-11-25T00:00:00Z', '2.0.228', '4c19332540ea697a2801e0daddcdabaf8377050a', 'Bug Fix: orange link to ticking system was using the incorrect url', 'Clicked the orange link to verify it sent the browser to the issue tracking system', 'Justin Davis'),
	(13, '2015-11-25T00:00:00Z', '2.0.229', 'b52b3699037b0f646bda02db8e20ab9e0b3a2a2b', 'Bug Fix: Deactivating a user was not removing them from the ldap server. Fixed by updating the ldap section.\r\nBug Fix: User without permissions or roles that was inactive caused php error because array was empty. Fixed by adding if count array >= 1 to each section\r\n', 'Deactivated test user and verified ldap entries were removed', 'Justin Davis'),
	(14, '2015-11-25T00:00:00Z', '2.0.231', 'e8f3045f8a078c4b17d5cac14b2a535df69d8697', 'Added the ability for the update_ldap script to remove users from the ldap server', 'Ran the update_ldap script', 'Justin Davis'),
	(15, '2015-11-25T00:00:00Z', '2.0.234', 'c108b5e1a7252bff0135c51add2d36e04774c4b8', 'Bug Fix: status_updates.php was looking for the old template.\r\nBug Fix: status_updates.tpl update was misspelled.\r\n', 'Ran the status_updates script in debug mode and verified the output', 'Justin Davis'),
	(16, '2015-11-25T00:00:00Z', '2.0.235', '4f69fc55fa83c64fe5f274a360366c6812b0b313', 'Bug Fix: location for templates was looking in the wrong place', 'Ran the check_system_assignments script and verified the output', 'Justin Davis'),
	(17, '2015-12-01T00:00:00Z', '2.0.238', 'b779f44bfefbd8c01ff67e20ebd5fd347a9ae185', 'Bug Fix: status_updates.php was not sending update emails. Found the smarty settings was looking for the wrong var when run as a cron job.', 'Verified the changes by running the script a cron job and making sure an email was sent', 'Justin Davis'),
	(18, '2015-12-01T00:00:00Z', '2.0.240', 'd35e5c3a4258ce41933283fb13786d9b158f2b87', 'Feature: Added ability to send status updates to updates@oihealthcareportal.com\r\nBug Fix: Changed scripts ran via cron to look for document root and not need the --cron argument\r\n', 'Tested the cron scripts by debugging then during thier runs', 'Justin Davis'),
	(19, '2015-12-02T00:00:00Z', '2.0.244', '281c6349d540fbaa852f1a9f93c73a1157348b4e', 'Bug Fix: Added smarty config directory setting to avoid warning notice.', 'Tested smarty config to verify the warning notice is gone', 'Justin Davis'),
	(20, '2015-12-02T00:00:00Z', '2.0.248', '4ef116095609d20bb5f6e7bbd4e1b19cc39dfcf8', 'Feature: Added sql to remove blank parts', 'Ran script', 'Justin Davis'),
	(21, '2015-12-04T00:00:00Z', '2.0.249', '1f56d6263fb8fc053d197a028056d319137f095c', 'Bug Fix: PDF of service report was showing current facility info instead of facility info at time of report\r\n', 'Downloaded service report pdf to verify the correct facility address', 'Justin Davis'),
	(22, '2015-12-08T00:00:00Z', '2.0.251', '319bf173c06366a893ea4209e4269405a813771b', 'Bug Fix: Deleting a car or par caused an sql error. Sql was looking for delete_reason instead of deleted_reason', 'Deleted a car and par to test', 'Justin Davis'),
	(23, '2015-12-08T00:00:00Z', '2.0.252', 'f9495ef65ecdb0e0f105db0aac34699900e11fdb', 'Feature: Added dispatcher\'s name to request emails', 'Created test service request', 'Justin Davis'),
	(24, '2015-12-08T00:00:00Z', '2.0.254', '0bf6a6a61f5c3b73b6369b26f509ec40e8d6e99b', 'Bug Fix: Found attach documents script was blocking beacuse of using old session handler.\r\nUpdated portal version.\r\n', 'Uploaded test files', 'Justin Davis'),
	(25, '2015-12-10T00:00:00Z', '2.0.254', '0f10b17d5c2fe838a61069c3ba876937c0acda8b', 'Bug Fix: Download of word doc for parcarfbc was throwing sql error. SQL was looking for old style group permissions. FBC car needed name was sending whole array instead of just name.\r\nBuild 254\r\n', 'Downloaded closed car, par, and fbc then verified the created word doc', 'Justin Davis'),
	(26, '2015-12-10T00:00:00Z', '2.0.256', 'ce0e5faa22f92350904e7ab5412c1e536863a2db', 'Feature: Added dispatchers name to the request form when viewing and editing.', 'Viewed a request to verify the name of the dispatcher was showing properly.\r\nViewed a report to verify the name of the dispatcher was showing properly.', 'Justin Davis'),
	(27, '2015-12-10T00:00:00Z', '2.0.256', '7379fef181aa4d5b6e182af444045823df1359b4', 'Bug Fix: Found that login_update_do.php was calling old adduser ldap function. Added some extra logging to make the error log more clear.', 'Performed a test password recovery', 'Justin Davis'),
	(28, '2015-12-11T00:00:00Z', '2.0.257', '6b050063a30ad3f5b4f3ffb5e578eafe5091f668', 'Feature: Added lookup request lookup to iso_fbc.php', 'Test looking up requests', 'Justin Davis'),
	(29, '2015-12-11T00:00:00Z', '2.0.258', 'fbc13c7bcb73ded65da4d107718104685d14bb60', 'Feature: Added user qualificatins to users profiles for engineers, and added quals list to map markers for engineers.', 'Updated user to verify quals changed', 'Justin Davis'),
	(30, '2016-01-18T00:00:00Z', '2.0.260', '306e5a4695f61c21c05224ec3465bb20c2e3483a', 'Bug Fix: Regex for password was limiting to 12 chars max. Changed to 100 max', 'Changed password to long text', 'Justin Davis'),
	(31, '2016-01-21T00:00:00Z', '2.0.262', '5d9fa773fc70d9edd086ae49ff4ef81a4a7db08f', 'Bug Fix: New users were not being added to the users_groups table.', 'Created a user and verified the user was added to the users_groups table', 'Justin Davis'),
	(32, '2016-01-21T00:00:00Z', '2.0.263', '663d9cf5841f25b29959a7ffe241e725b3eb92d7', 'Bug Fix: Systems Contacts report was including archived systems.', 'Ran the report a checked for archived systems', 'Justin Davis'),
	(33, '2016-01-27T00:00:00Z', '2.0.265', 'ff9eb311e903f4a8facc1b02fa28c64cb53b4797', 'DMS update to 4.3.23', 'Verified the DMS operation', 'Justin Davis'),
	(34, '2016-02-04T00:00:00Z', '2.0.268', '73f3540bc98780e67d01a8df076a7feff5836a46', 'Feature: Added ability for customers and facilities to have letters in zip codes.', 'Entered canadian zip codes in facility and customer', 'Justin Davis'),
	(35, '2016-03-07T00:00:00Z', '2.0.269', 'f494dd88d8a56246b04552a09c80da1c07ca4feb', 'Updated the regex for checking the system serial number. It was returning false when the serial had a 0 at the end.\r\n', 'Updated system serial number with a 0 on the end.', 'Justin Davis');
/*!40000 ALTER TABLE `changelog` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
