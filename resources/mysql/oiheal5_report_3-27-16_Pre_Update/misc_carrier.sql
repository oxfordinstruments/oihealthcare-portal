-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.misc_carrier
DROP TABLE IF EXISTS `misc_carrier`;
CREATE TABLE IF NOT EXISTS `misc_carrier` (
  `carrierid` int(11) NOT NULL AUTO_INCREMENT,
  `company` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`carrierid`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='list of cell carriers and email addresses\r\n';

-- Dumping data for table oiheal5_report.misc_carrier: ~8 rows (approximately)
/*!40000 ALTER TABLE `misc_carrier` DISABLE KEYS */;
INSERT INTO `misc_carrier` (`carrierid`, `company`, `address`) VALUES
	(1, 'AT&T', '@txt.att.net'),
	(2, 'Verizon', '@vtext.com'),
	(3, 'T-Mobil', '@tmomail.net'),
	(4, 'Sprint', '@messaging.sprintpcs.com'),
	(5, 'AllTel', '@message.alltel.com'),
	(6, 'Boost', '@myboostmobile.com'),
	(7, 'US Cellular', '@email.uscc.net'),
	(8, 'Virgin', '@vmobl.com');
/*!40000 ALTER TABLE `misc_carrier` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
