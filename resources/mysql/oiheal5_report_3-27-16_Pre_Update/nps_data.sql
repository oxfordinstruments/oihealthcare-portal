-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.nps_data
DROP TABLE IF EXISTS `nps_data`;
CREATE TABLE IF NOT EXISTS `nps_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fiscal_quarter` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fiscal_year` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `survey_send_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `surveys_sent` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `surveys_received` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `response_percent` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nps_score` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `promoters` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passives` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `demoters` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nps_0` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nps_1` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nps_2` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nps_3` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nps_4` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nps_5` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nps_6` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nps_7` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nps_8` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nps_9` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nps_10` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unique_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table oiheal5_report.nps_data: ~7 rows (approximately)
/*!40000 ALTER TABLE `nps_data` DISABLE KEYS */;
INSERT INTO `nps_data` (`id`, `fiscal_quarter`, `fiscal_year`, `survey_send_date`, `surveys_sent`, `surveys_received`, `response_percent`, `nps_score`, `promoters`, `passives`, `demoters`, `nps_0`, `nps_1`, `nps_2`, `nps_3`, `nps_4`, `nps_5`, `nps_6`, `nps_7`, `nps_8`, `nps_9`, `nps_10`, `unique_id`) VALUES
	(1, '3', '2013', '2013-09-30', '120', '10', '8.33', '60', '7', '2', '1', '0', '0', '0', '0', '0', '1', '0', '0', '2', '1', '6', '3874d677b6c6d38d9b59028293435862'),
	(2, '4', '2013', '2013-12-06', '119', '6', '5.04', '100', '6', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '6', 'a584d1e461f610f96df98b30d8169c71'),
	(7, '1', '2014', '2014-04-09', '117', '5', '4.27', '100', '5', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '4', '84d27778dfd0a7322b89cbfbe654aa2d'),
	(8, '2', '2014', '2014-05-31', '115', '5', '4.35', '100', '5', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '4', '7d1a81a7c1b4cd708aa3a72422045ee6'),
	(9, '3', '2014', '2014-09-11', '115', '17', '14.78', '100', '17', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '16', '608ec8a874fc0854914f44de01ab9dd1'),
	(10, '4', '2014', '2014-12-01', '146', '11', '7.53', '100', '11', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '10', '9069fc58bc773a45593d2914b6cd7561'),
	(12, '1', '2015', '2015-03-01T00:00:00Z', '140', '9', '6.43', '100', '9', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '8', '00f3cf5efff714b20a8ff6755febfd27'),
	(13, '2', '2015', '2015-05-31T16:30:01Z', '131', '9', '6.87', '88.89', '8', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '7', '655524998089d2e4e1b55039d19436e0'),
	(14, '3', '2015', '2015-08-31T16:30:01Z', '114', '9', '7.89', '88.89', '8', '1', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '1', '7', '7891083408de0b3f9fee611754f0300c'),
	(15, '4', '2015', '2015-12-01T16:30:01Z', '111', '4', '3.6', '75', '3', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '2', '1', '17f47ee4f25a21e966d4bf81c03757ed'),
	(16, '1', '2016', '2016-03-01T16:30:01Z', '109', '8', '7.34', '87.5', '7', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '7', '7104d955ec0a35c549a82e106e4a060c');
/*!40000 ALTER TABLE `nps_data` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
