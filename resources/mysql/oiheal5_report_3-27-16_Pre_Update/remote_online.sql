-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.remote_online
DROP TABLE IF EXISTS `remote_online`;
CREATE TABLE IF NOT EXISTS `remote_online` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vpn_ip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vpn_mac` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ping_min` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ping_avg` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ping_max` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `vpn_ip` (`vpn_ip`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='This table is populated from the oiremote server';

-- Dumping data for table oiheal5_report.remote_online: ~4 rows (approximately)
/*!40000 ALTER TABLE `remote_online` DISABLE KEYS */;
INSERT INTO `remote_online` (`id`, `vpn_ip`, `vpn_mac`, `ping_min`, `ping_avg`, `ping_max`, `date`) VALUES
	(1, '14.25.36.3', '00:AC:24:E2:45:55', '482', '482', '482', '2016-03-27T19:15:01Z'),
	(2, '14.25.36.4', '00:AC:F6:F9:68:91', '75.6', '75.6', '75.6', '2016-03-27T19:15:01Z'),
	(3, '14.25.36.6', '00:AC:C4:D9:1D:E2', '65.9', '65.9', '65.9', '2016-03-27T19:15:01Z'),
	(4, '14.25.36.5', '00:AC:F5:EF:9C:E2', '153', '153', '153', '2016-03-27T19:15:01Z');
/*!40000 ALTER TABLE `remote_online` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
