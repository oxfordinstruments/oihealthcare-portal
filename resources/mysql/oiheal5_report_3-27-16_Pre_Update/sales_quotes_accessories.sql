-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.sales_quotes_accessories
DROP TABLE IF EXISTS `sales_quotes_accessories`;
CREATE TABLE IF NOT EXISTS `sales_quotes_accessories` (
  `accessories_id` int(11) DEFAULT NULL,
  `sales_quotes_id` int(11) DEFAULT NULL,
  `price` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `detail` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `FK_sales_quotes_accessories_misc_accessories` (`accessories_id`),
  KEY `FK_sales_quotes_accessories_sales_quotes` (`sales_quotes_id`),
  CONSTRAINT `FK_sales_quotes_accessories_misc_accessories` FOREIGN KEY (`accessories_id`) REFERENCES `misc_accessories` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_sales_quotes_accessories_sales_quotes` FOREIGN KEY (`sales_quotes_id`) REFERENCES `sales_quotes` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table oiheal5_report.sales_quotes_accessories: ~7 rows (approximately)
/*!40000 ALTER TABLE `sales_quotes_accessories` DISABLE KEYS */;
INSERT INTO `sales_quotes_accessories` (`accessories_id`, `sales_quotes_id`, `price`, `detail`) VALUES
	(10, 1000, NULL, NULL),
	(13, 1004, '20000', 'Some Details'),
	(13, 1010, '111.00', 'c'),
	(13, 1011, '111.00', 'Memory Foam'),
	(13, 1012, '111.00', 'Memory Foam'),
	(13, 1013, '111.00', 'Memory Foam'),
	(13, 1014, '111.00', 'Memory Foam');
/*!40000 ALTER TABLE `sales_quotes_accessories` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
