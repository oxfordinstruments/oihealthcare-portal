-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.systems_assigned_customer
DROP TABLE IF EXISTS `systems_assigned_customer`;
CREATE TABLE IF NOT EXISTS `systems_assigned_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `system_unique_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table oiheal5_report.systems_assigned_customer: ~64 rows (approximately)
/*!40000 ALTER TABLE `systems_assigned_customer` DISABLE KEYS */;
INSERT INTO `systems_assigned_customer` (`id`, `system_unique_id`, `uid`) VALUES
	(11, 'd45858959c41f2b4a22d6c5645b473ec', 'stadob'),
	(12, 'e488c7b573db16d51d084943c0aa6f3c', 'lvpcc'),
	(13, 'e7243ef30ebf4f4c3a9d5ce45a2ff433', 'mdimaging'),
	(14, 'faec180ae412030209ffa5a9ce639128', 'mdimaging'),
	(15, '643cb4e0a3412f9b0fb69ae403707412', 'mdimaging'),
	(16, '4b39adaca9dd3bf10123fe27bf9a8568', 'mdimaging'),
	(17, 'ae8baa137a992719b0321d626c342392', 'flmdvm'),
	(18, 'b52746bef3f53c581efd59a904a61d04', 'pcomri'),
	(19, '235dc5b35da606ebad65453e19b483f1', 'tleonzal'),
	(20, 'cd0ceeca9c9000bcc80f1c97dd3518ff', 'tleonzal'),
	(21, '2952e6f63dbe272b46cfd9d26b1225ba', 'cdycus'),
	(22, 'd116e05fdcb37f50e8d966d792526acd', 'jmpfankuch'),
	(23, '6ba999e41f8e7aee6158c6a3c844dbc6', 'dhouston'),
	(24, '7226345f9464252c4f0840e86a00dc1c', 'hongdu'),
	(25, '4761efdea25b7aaf194ec26bce008d6e', 'cmzoss'),
	(26, '8f8b76a5b9aa2accf7d3703f75ffbd23', 'cmzoss'),
	(27, '8b06e07601c44e206d5daf61bf150781', 'cmzoss'),
	(29, '44875b5ea1ebea0e3fc0451730f3ba32', 'eosborne'),
	(31, 'f7c33ce699b0922a8e7d008bfe7a4619', 'ptierney'),
	(32, 'e0f9b3581824842aab5c8ef4c7d85e96', 'walkers1'),
	(33, 'a07bfa7116cbd3ad8a3711eb61daaed8', 'jgonzalez'),
	(34, '2989ba2a2f906bfca9613ec8c8fe96b8', 'jgonzalez'),
	(35, '6b162b0c87ea10671f1dcb0864637ff4', 'bayviewradiology'),
	(36, 'be2a302a437609e2b4b900d87ab36f91', 'bayviewradiology'),
	(37, '12a6cfaa3981887fd9681d824254b3a9', 'ahphilyaw'),
	(38, '162a2a9d949165a084c593677d584b71', 'ksingh'),
	(39, 'e0f9b3581824842aab5c8ef4c7d85e96', 'munazzam'),
	(40, '9fc6fd464af40d9a0ceb59918595cdd9', 'customer'),
	(41, '9efbfd8c67c92718d050327d31a06810', 'customer'),
	(42, 'bf6eb56537afa0716977c00268047668', 'customer'),
	(43, 'cb4d80718170819f780db86d1a9b42b9', 'customer'),
	(44, 'dba8165535b260549ca328fcc47925a0', 'customer'),
	(45, '640e7689640306093fb43a9080bc83bc', 'borellap'),
	(46, '2897e9e797c15b92b4af8fd4b5cf1009', 'vrccmri'),
	(47, 'f7f647a9db2cbcc3e8c8d4c2777b5cd1', 'jcdameron'),
	(48, '8cd3b5b5bdd0fc734b8c80fe33c88afa', 'sgordon'),
	(49, '28bee8c8269897415923b75aadc8e0d2', 'dgrnja'),
	(50, '653aa6ea6f642ef8dbd629266dd14764', 'dbryant'),
	(51, '7ca34337c7f95c6822197c920fa39b0e', 'parkd'),
	(52, '91795ab37c58ed84ffb20b751bdcbcbc', 'parkd'),
	(53, '7ca34337c7f95c6822197c920fa39b0e', 'barnesn'),
	(54, '91795ab37c58ed84ffb20b751bdcbcbc', 'barnesn'),
	(55, '8846e7ae01c7fe40d67766502b8b82e4', 'jared.newton@meritushealth.com'),
	(56, '2c1738fe541e0cf0d7266957a95787dd', 'sankam@srhca.org'),
	(57, '2c1738fe541e0cf0d7266957a95787dd', 'clasiter@srhca.org'),
	(60, 'd116e05fdcb37f50e8d966d792526acd', 'customernew'),
	(61, 'd983c289641eae8830d386ba2928b3de', 'lyndsi@jeffersonimaging.com'),
	(63, 'd983c289641eae8830d386ba2928b3de', 'suzannemuntz@comcast.net'),
	(65, '3e53df4c965806a3e9a928a5dcbc63d4', 'ramesh.nijjar@olathehealth.org'),
	(66, '687bddc38be82f3ffb0eb7192aab2f29', 'jjohansen@savneurospec.com'),
	(67, '82a367284a0f978dac3358846da2d45b', 'lsaitta@inmedllc.com'),
	(68, '2561a52ac5530fd9942771c748c06b02', 'denise@pathmedical.com'),
	(69, '84ff43145bb15088566f58a1e4072373', 'reneew@ntxortho.com'),
	(70, '3e53df4c965806a3e9a928a5dcbc63d4', 'james.neihart@olathehealth.org'),
	(71, '9a702448a6572d96b44f8e13887705a9', 'jonathan.scott.boan@gmail.com'),
	(72, 'e1f66e3fc01ac410076ed76bee1fd43e', 'jonathan.scott.boan@gmail.com'),
	(74, 'f7c33ce699b0922a8e7d008bfe7a4619', 'paul.ullrich@stjoe.org'),
	(75, '7446a95edde00352e37d7110272f3829', 'rlynch@dvullc.com'),
	(76, '5f397c986a91a4ef1418c43d93b5b3a8', 'carmen.salvat@mch.com'),
	(77, '9d1d072f867b7ebbb5a330055a270514', 'carmen.salvat@mch.com'),
	(78, '7446a95edde00352e37d7110272f3829', 'woresick@dvullc.com'),
	(79, 'b8b06e62b51697ee9eaaccafa14dec16', 'mark.allgeyer@opensidedmri.net'),
	(80, '9467852bf0727c35604db1569e83dab5', 'kmitchell@centralohiourology.com'),
	(81, '596da9c1a18214422e76d1bf0153659e', 'kmitchell@centralohiourology.com'),
	(82, '640e7689640306093fb43a9080bc83bc', 'mjones@petcureoncology.com'),
	(83, 'd1a3814009807c8cc3bf8c2ed3463a0a', 'jlandgraf@essehealth.com'),
	(84, 'f73f3cac41e749d894734242228707b8', 'cbindhamer@eliteradga.com');
/*!40000 ALTER TABLE `systems_assigned_customer` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
