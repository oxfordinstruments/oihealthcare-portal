-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.systems_assigned_history
DROP TABLE IF EXISTS `systems_assigned_history`;
CREATE TABLE IF NOT EXISTS `systems_assigned_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_sent` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table oiheal5_report.systems_assigned_history: ~10 rows (approximately)
/*!40000 ALTER TABLE `systems_assigned_history` DISABLE KEYS */;
INSERT INTO `systems_assigned_history` (`id`, `filename`, `date`, `email_sent`) VALUES
	(99, 'asgn_2016-03-04T16-50_99.xml', '2016-03-04T16:50:02Z', 'Y'),
	(100, 'asgn_2016-03-09T16-50_100.xml', '2016-03-09T16:50:02Z', 'Y'),
	(101, 'asgn_2016-03-09T21-50_101.xml', '2016-03-09T21:50:01Z', 'Y'),
	(102, 'asgn_2016-03-10T13-50_102.xml', '2016-03-10T13:50:01Z', 'Y'),
	(103, 'asgn_2016-03-11T13-50_103.xml', '2016-03-11T13:50:01Z', 'Y'),
	(104, 'asgn_2016-03-11T16-50_104.xml', '2016-03-11T16:50:01Z', 'Y'),
	(105, 'asgn_2016-03-15T16-50_105.xml', '2016-03-15T16:50:01Z', 'Y'),
	(106, 'asgn_2016-03-16T19-50_106.xml', '2016-03-16T19:50:02Z', 'Y'),
	(107, 'asgn_2016-03-18T21-50_107.xml', '2016-03-18T21:50:01Z', 'Y'),
	(108, 'asgn_2016-03-22T16-50_108.xml', '2016-03-22T16:50:02Z', 'N');
/*!40000 ALTER TABLE `systems_assigned_history` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
