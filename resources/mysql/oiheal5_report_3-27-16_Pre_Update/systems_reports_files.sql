-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.systems_reports_files
DROP TABLE IF EXISTS `systems_reports_files`;
CREATE TABLE IF NOT EXISTS `systems_reports_files` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `unique_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_location` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_size` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'user who added file',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `FK_reports_files_requests` (`unique_id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='files linked to each report';

-- Dumping data for table oiheal5_report.systems_reports_files: ~45 rows (approximately)
/*!40000 ALTER TABLE `systems_reports_files` DISABLE KEYS */;
INSERT INTO `systems_reports_files` (`id`, `unique_id`, `file_location`, `file_name`, `file_size`, `uid`) VALUES
	(1, 'dc935cec30555fa5709e66ad464db63f', '/systems_reports_files/dc935cec30555fa5709e66ad464db63f/files/', '2014-10-01 12.20.59.jpg', '2004928', 'davisj'),
	(2, '29bffdb6865f900a9890cbcc91e5a95f', '/systems_reports_files/29bffdb6865f900a9890cbcc91e5a95f/files/', '2014-10-08 15.14.20.jpg', '2792251', 'davisj'),
	(3, 'a3d6140eeff2fd8a40ef719c705fa2e7', '/systems_reports_files/a3d6140eeff2fd8a40ef719c705fa2e7/files/', 'DSC02326.JPG', '164735', 'galvackyd'),
	(4, 'a3d6140eeff2fd8a40ef719c705fa2e7', '/systems_reports_files/a3d6140eeff2fd8a40ef719c705fa2e7/files/', 'DSC02331.JPG', '168006', 'galvackyd'),
	(5, '38f9fde652b0157b70d0aff8e11f92ca', '/systems_reports_files/38f9fde652b0157b70d0aff8e11f92ca/files/', 'photo 1.JPG', '2090235', 'corec'),
	(6, '38f9fde652b0157b70d0aff8e11f92ca', '/systems_reports_files/38f9fde652b0157b70d0aff8e11f92ca/files/', 'photo 2.JPG', '2069091', 'corec'),
	(7, '38f9fde652b0157b70d0aff8e11f92ca', '/systems_reports_files/38f9fde652b0157b70d0aff8e11f92ca/files/', 'photo 3.JPG', '1822859', 'corec'),
	(8, '38f9fde652b0157b70d0aff8e11f92ca', '/systems_reports_files/38f9fde652b0157b70d0aff8e11f92ca/files/', 'photo 4.JPG', '2175155', 'corec'),
	(9, '39baed1b5d3a7c463caa6dd5beb052e0', '/systems_reports_files/39baed1b5d3a7c463caa6dd5beb052e0/files/', '20141023_161823[1].jpg', '1042059', 'coyled'),
	(10, '39baed1b5d3a7c463caa6dd5beb052e0', '/systems_reports_files/39baed1b5d3a7c463caa6dd5beb052e0/files/', '20141023_161717[1].jpg', '1467163', 'coyled'),
	(11, '39baed1b5d3a7c463caa6dd5beb052e0', '/systems_reports_files/39baed1b5d3a7c463caa6dd5beb052e0/files/', '20141023_161218[1].jpg', '1397490', 'coyled'),
	(12, '39baed1b5d3a7c463caa6dd5beb052e0', '/systems_reports_files/39baed1b5d3a7c463caa6dd5beb052e0/files/', '20141023_160510[1].jpg', '835172', 'coyled'),
	(13, '39baed1b5d3a7c463caa6dd5beb052e0', '/systems_reports_files/39baed1b5d3a7c463caa6dd5beb052e0/files/', '20141023_160012[1].jpg', '630442', 'coyled'),
	(14, '39baed1b5d3a7c463caa6dd5beb052e0', '/systems_reports_files/39baed1b5d3a7c463caa6dd5beb052e0/files/', '20141023_155903[1].jpg', '551596', 'coyled'),
	(15, '39baed1b5d3a7c463caa6dd5beb052e0', '/systems_reports_files/39baed1b5d3a7c463caa6dd5beb052e0/files/', '20141023_155830[1].jpg', '613443', 'coyled'),
	(16, '39baed1b5d3a7c463caa6dd5beb052e0', '/systems_reports_files/39baed1b5d3a7c463caa6dd5beb052e0/files/', '20141023_155746[1].jpg', '814345', 'coyled'),
	(17, '39baed1b5d3a7c463caa6dd5beb052e0', '/systems_reports_files/39baed1b5d3a7c463caa6dd5beb052e0/files/', '20141023_155705[1].jpg', '810291', 'coyled'),
	(18, '39baed1b5d3a7c463caa6dd5beb052e0', '/systems_reports_files/39baed1b5d3a7c463caa6dd5beb052e0/files/', '20141023_155453[1].jpg', '1633704', 'coyled'),
	(19, '39baed1b5d3a7c463caa6dd5beb052e0', '/systems_reports_files/39baed1b5d3a7c463caa6dd5beb052e0/files/', '20141023_154723[1].jpg', '1099680', 'coyled'),
	(20, '39baed1b5d3a7c463caa6dd5beb052e0', '/systems_reports_files/39baed1b5d3a7c463caa6dd5beb052e0/files/', '20141023_154534[1].jpg', '746015', 'coyled'),
	(21, '39baed1b5d3a7c463caa6dd5beb052e0', '/systems_reports_files/39baed1b5d3a7c463caa6dd5beb052e0/files/', '20141023_154528[1].jpg', '721562', 'coyled'),
	(22, '39baed1b5d3a7c463caa6dd5beb052e0', '/systems_reports_files/39baed1b5d3a7c463caa6dd5beb052e0/files/', '20141023_154437[1].jpg', '714430', 'coyled'),
	(23, '39baed1b5d3a7c463caa6dd5beb052e0', '/systems_reports_files/39baed1b5d3a7c463caa6dd5beb052e0/files/', '20141023_154227[1].jpg', '751839', 'coyled'),
	(24, '39baed1b5d3a7c463caa6dd5beb052e0', '/systems_reports_files/39baed1b5d3a7c463caa6dd5beb052e0/files/', '20141023_154159[1].jpg', '662865', 'coyled'),
	(25, '39baed1b5d3a7c463caa6dd5beb052e0', '/systems_reports_files/39baed1b5d3a7c463caa6dd5beb052e0/files/', '20141023_154153[1].jpg', '729813', 'coyled'),
	(26, '39baed1b5d3a7c463caa6dd5beb052e0', '/systems_reports_files/39baed1b5d3a7c463caa6dd5beb052e0/files/', '20141023_154053[1].jpg', '732992', 'coyled'),
	(27, '39baed1b5d3a7c463caa6dd5beb052e0', '/systems_reports_files/39baed1b5d3a7c463caa6dd5beb052e0/files/', '20141023_154031[1].jpg', '729436', 'coyled'),
	(28, '39baed1b5d3a7c463caa6dd5beb052e0', '/systems_reports_files/39baed1b5d3a7c463caa6dd5beb052e0/files/', '20141023_154001[1].jpg', '770323', 'coyled'),
	(29, '164f2b7f48308b99d2b9547f7451d86c', '/systems_reports_files/164f2b7f48308b99d2b9547f7451d86c/files/', 'IMG_2036.JPG', '18101', 'davisj'),
	(30, '164f2b7f48308b99d2b9547f7451d86c', '/systems_reports_files/164f2b7f48308b99d2b9547f7451d86c/files/', 'photo 1.JPG', '22325', 'davisj'),
	(31, '164f2b7f48308b99d2b9547f7451d86c', '/systems_reports_files/164f2b7f48308b99d2b9547f7451d86c/files/', 'photo 2.JPG', '22061', 'davisj'),
	(32, '164f2b7f48308b99d2b9547f7451d86c', '/systems_reports_files/164f2b7f48308b99d2b9547f7451d86c/files/', 'photo 3.JPG', '19600', 'davisj'),
	(33, '164f2b7f48308b99d2b9547f7451d86c', '/systems_reports_files/164f2b7f48308b99d2b9547f7451d86c/files/', 'photo 4.JPG', '22040', 'davisj'),
	(64, '25e041590e15c60790c5d15d3ac76cb6', '/systems_reports_files/25e041590e15c60790c5d15d3ac76cb6/files/', 'MVP000212.pdf', '802499', 'davisj'),
	(65, 'efedfe5353f192300282b6a399da95c5', '/systems_reports_files/efedfe5353f192300282b6a399da95c5/files/', 'MVP10559R6.pdf', '796262', 'davisj'),
	(66, 'c82b299ddb92a7355e8afe35c5b143b1', '/systems_reports_files/c82b299ddb92a7355e8afe35c5b143b1/files/', 'IMG953250[1].jpg', '122940', 'davisj'),
	(67, '923a5d468ce8d450f48bea92902bde37', '/systems_reports_files/923a5d468ce8d450f48bea92902bde37/files/', '20150701_114342[1].jpg', '2084564', 'davisj'),
	(68, '6df9d45ad15bcdf1131cb76ca189e6c4', '/systems_reports_files/6df9d45ad15bcdf1131cb76ca189e6c4/files/', '20151028_165945[1].jpg', '1403284', 'coyled'),
	(69, '267d76a569ea1299dd16437542244be1', '/systems_reports_files/267d76a569ea1299dd16437542244be1/files/', '1216151049.jpg', '1573545', 'davisj'),
	(70, '267d76a569ea1299dd16437542244be1', '/systems_reports_files/267d76a569ea1299dd16437542244be1/files/', '1216151049a.jpg', '1565325', 'davisj'),
	(71, 'ac38c9ac1a8e23b733a24e65020c5af1', '/systems_reports_files/ac38c9ac1a8e23b733a24e65020c5af1/files/', 'IMG_4125[1].jpg', '206262', 'coyled'),
	(72, '7bd3c3a7bd759fddcef64e2cc27cd03a', '/systems_reports_files/7bd3c3a7bd759fddcef64e2cc27cd03a/files/', 'SR-OXFORD-CT-TUBE MOBILE 2-8-2016.xls', '32768', 'bringolfk'),
	(73, '7bd3c3a7bd759fddcef64e2cc27cd03a', '/systems_reports_files/7bd3c3a7bd759fddcef64e2cc27cd03a/files/', 'SR-OXFORD-DATABASE MOBILE 2-7-2016.xls', '32256', 'bringolfk'),
	(74, 'e99d993477c78df826c163e36578ff50', '/systems_reports_files/e99d993477c78df826c163e36578ff50/files/', 'SR-400903-Jan 11,2016-Oxford Titusville,Fl-Sens 64.pdf', '39580', 'bringolfk'),
	(77, 'dacc0c27ed94d8903f05cd6705ec1dbf', '/systems_reports_files/dacc0c27ed94d8903f05cd6705ec1dbf/files/', '20160311081545688.pdf', '116873', 'bringolfk'),
	(78, '6b669cf40325849fd5e08a59dc360d33', '/systems_reports_files/6b669cf40325849fd5e08a59dc360d33/files/', '7695910_69489_2016.pdf', '50399', 'bringolfk'),
	(80, '702669e6a29f989519def9c7f2739ba2', '/systems_reports_files/702669e6a29f989519def9c7f2739ba2/files/', '7697676_69411_2016.pdf', '46403', 'bringolfk');
/*!40000 ALTER TABLE `systems_reports_files` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
