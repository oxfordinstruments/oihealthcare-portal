-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.trailers
DROP TABLE IF EXISTS `trailers`;
CREATE TABLE IF NOT EXISTS `trailers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trailer_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nickname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `archived` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `assigned` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `system_unique_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mfg` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mfg_year` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vin` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `license_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `license_state` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `license_expire` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pop_outs` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `size` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `has_files` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `edited_by` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `edited_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unique_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`unique_id`),
  KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table oiheal5_report.trailers: 41 rows
/*!40000 ALTER TABLE `trailers` DISABLE KEYS */;
INSERT INTO `trailers` (`id`, `trailer_id`, `nickname`, `archived`, `assigned`, `system_unique_id`, `mfg`, `mfg_year`, `vin`, `license_number`, `license_state`, `license_expire`, `pop_outs`, `size`, `notes`, `has_files`, `edited_by`, `edited_date`, `created_by`, `created_date`, `unique_id`) VALUES
	(1, 'TR1000', 'TR1000', 'N', 'Y', 'c8766a2b31160a9e9e7072fe9c891558', 'Calumet', NULL, '1TKH048221B083736', NULL, NULL, NULL, 'N', NULL, 'MIR inv num 2519', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:41Z', '011ed1e23574da2ba7a4baa558f2807f'),
	(2, 'TR1001', 'TR1001', 'N', 'Y', 'd391694b48ca0477772a903a7eb6b83c', 'Calumet', NULL, '1TKH04827YB011795', NULL, NULL, NULL, 'N', NULL, 'MIR inv num AP-CT-2492', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:41Z', '96444da7789cd98f5fd967b5f46d46e9'),
	(3, 'TR1002', 'TR1002', 'N', 'Y', '71138435a5059849f7a2af13f3d738ca', 'Ellis & Watts', NULL, '1JJV482W9YL718108', NULL, NULL, NULL, 'N', NULL, 'MIR inv num AP-CT-2499', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:42Z', '96298e8534e721f76a4c476e23f86c09'),
	(4, 'TR1003', 'TR1003', 'N', 'Y', '6c74c1a945e2bb8ea07d0203965dbdd6', 'Calumet', NULL, '1TKH0482X1B094368', NULL, NULL, NULL, 'N', NULL, 'MIR inv num AP-NO-1207', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:42Z', '8863d6974b07f6a308e5f15713ec3371'),
	(5, 'TR1004', 'TR1004', 'N', 'Y', 'e9a68a1650d9be21aed71bd516823eb7', 'Calumet', NULL, '1TKH04825YB042074', NULL, NULL, NULL, 'N', NULL, 'MIR inv num AP-CT-116', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:42Z', 'a57e10a04d01076f087d695c3773975e'),
	(6, 'TR1005', 'TR1005', 'N', 'Y', 'f27e1a34f7d523a20468f7a94811398d', 'Oshkosh', NULL, '1LH142UH111011605', NULL, NULL, NULL, 'N', NULL, 'MIR inv num AP-NO-114', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:42Z', 'a11575cef5ee8dd2e5aaf8d383814f7c'),
	(7, 'TR1006', 'TR1006', 'N', 'Y', 'cc37b4e3f1321fa22b8d13f02975255b', 'Calumet', NULL, '1TKH04824YB022012', NULL, NULL, NULL, 'N', NULL, 'MIR inv num 2584', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:42Z', 'f0503c05eacbbe3834c8727b94efc323'),
	(8, 'TR1007', 'TR1007', 'N', 'Y', 'ce7a525a58d97d300ac00e685d83b86c', 'Calumet', NULL, '1TKH048211B084361', NULL, NULL, NULL, 'N', NULL, 'MIR inv num AP-CT-113', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:42Z', '5a7d9c76045b7b3be1b8bbadb691c01b'),
	(9, 'TR1008', 'TR1008', 'N', 'Y', '52c85acd3cabc129858ef2a76ff75515', 'Calumet', NULL, '1TKH04829YB022006', NULL, NULL, NULL, 'N', NULL, 'MIR inv num AP-NO-1235', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:42Z', '0ed27101315db77a3367bf41612ae479'),
	(10, 'TR1009', 'TR1009', 'N', 'Y', 'c9a4dec1c436607547f7d4841530e161', 'MDG', NULL, '1PT011AJ0F9008193', NULL, NULL, NULL, 'N', NULL, 'MIR inv num AP-NO-1097', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:42Z', 'd2001c33e1c501e913aaf8fbb4457038'),
	(11, 'TR1010', 'TR1010', 'N', 'Y', '3c868d2d7c9fc016baa931c798c0795d', 'Calumet', NULL, '1T9FA0Z38FB021612', NULL, NULL, NULL, 'N', NULL, 'MIR inv num 2518', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:42Z', 'f0a6403202e612b90b4edaf39aded855'),
	(12, 'TR1011', 'TR1011', 'N', 'Y', 'e4b754b1869cbec5eb99e10c95432bc6', 'Calumet', NULL, '1T9FA0Z24JB021283', NULL, NULL, NULL, 'N', NULL, 'MIR inv num 3330', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:42Z', 'e1498abf6031fae5c5cadc5720341bd0'),
	(13, 'TR1012', 'TR1012', 'N', 'Y', '52d16ed8b2d353bf2de116263ad29568', 'Calumet', NULL, '1T9FA0Z30MB021536', NULL, NULL, NULL, 'N', NULL, 'MIR inv num 3691', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:42Z', '7060012c35c548690556663f598e9405'),
	(14, 'TR1013', 'TR1013', 'N', 'Y', '2724b009fe1e2949e17ec33f136a3ca5', 'Calumet', NULL, '1T9FA0Z33GB021860', NULL, NULL, NULL, 'N', NULL, 'MIR inv num 3857', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:42Z', 'b2b988945b7dfbfe245a4e4fd4fceb23'),
	(15, 'TR1014', 'TR1014', 'N', 'Y', '5388bfc1f929392c224d5050e084bdc1', 'Calumet', NULL, '1T9FA0Z37GB021779', NULL, NULL, NULL, 'N', NULL, 'MIR inv num AP-NO-1236', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:42Z', '05d91473b69e98267364aa7be875e039'),
	(16, 'TR1016', 'TR1016', 'N', 'Y', '74a22e217e91a223622a6bf32a1ab25d', 'Calumet', NULL, '1T9FA0Z33HB021861', NULL, NULL, NULL, 'N', NULL, 'MIR inv num 2911', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:42Z', '9fd9e53c3708dbc4c16c1f5abbc3e987'),
	(17, 'TR1017', 'TR1017', 'N', 'Y', 'fcd3b9d42d8ef4aa21248cef27c3a4db', 'Schien', NULL, '1S9FA82X81183313', NULL, NULL, NULL, 'N', NULL, 'MIR inv num AP-MR-2498', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:42Z', '640eada32c5bf359285faaf72cebafc0'),
	(18, 'TR1018', 'TR1018', 'N', 'Y', '723341afa865eab587f7510ddb6cc507', 'Medicoach', NULL, '1M9A3A8216W022406', NULL, NULL, NULL, 'N', NULL, 'MIR inv num 4160', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:42Z', 'f49ab76f79ac19e8194505c69c8576fe'),
	(19, 'TR1019', 'TR1019', 'N', 'Y', 'ef333ee1038c53365b3c39925fdde2e3', 'Schien', NULL, '1S9FA482891183330', NULL, NULL, NULL, 'N', NULL, 'MIR inv num AP-MR-2497', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:42Z', '76604688a996c96f7116032de04fe6f1'),
	(20, 'TR1020', 'TR1020', 'N', 'Y', '78e38c64f3ebad5f833cb3ca6b24b5a4', 'Calumet', NULL, '1T9FA0Z20JB021569', NULL, NULL, NULL, 'N', NULL, 'MIR inv num 2610', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:42Z', '8898249a42ca810cb6ffdbadd000770c'),
	(21, 'TR1021', 'TR1021', 'N', 'Y', '167471e197d12ae641c42616219bfb17', 'Calumet', NULL, '1TKH04822YB011560', NULL, NULL, NULL, 'N', NULL, 'MIR inv num AP-MR-2407', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:42Z', '357c5012c8d9dbc81220193fc86f9582'),
	(22, 'TR1022', 'TR1022', 'N', 'Y', '45e578d5ad438df9786a721bccb7708b', 'Calumet', NULL, '1TKHA4820XB116156', NULL, NULL, NULL, 'N', NULL, 'MIR inv num 3033', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:42Z', '5f0ef6c884dd6ee8d84e08e0aedf6462'),
	(23, 'TR1023', 'TR1023', 'N', 'Y', 'a2ed222e8fe52ea2d9698d4bc31514b7', 'AK Specialty', NULL, '1S9FA482021182517', NULL, NULL, NULL, 'N', NULL, 'MIR inv num 3503', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:42Z', '2185931d96fd847b831b17ec36560aca'),
	(24, 'TR1024', 'TR1024', 'N', 'Y', '95d7c41e83f32953773ec15afab6c3cd', 'Ellis & Watts', NULL, '1KKVA48227L223655', NULL, NULL, NULL, 'N', NULL, 'MIR inv num AP-NO-2292', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:42Z', 'abe4a9762f497e1b787be9a387ca6dca'),
	(25, 'TR1025', 'TR1025', 'N', 'Y', '45fb19f45cd385314de401c07041f563', 'Calutech', NULL, '1TKH04820VB018020', NULL, NULL, NULL, 'N', NULL, 'MIR inv num AP-NO-2291', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:42Z', '51a71b1387e53a7bf75811b217587662'),
	(26, 'TR1026', 'TR1026', 'N', 'Y', 'ee02aee1497359adf86509100f5f4282', 'Ellis & Watts', NULL, '1JJV482WXYL515096', NULL, NULL, NULL, 'N', NULL, 'MIR inv num 3201', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:42Z', '043452652cd362cfecfd66b2da7fccf2'),
	(27, 'TR1027', 'TR1027', 'N', 'Y', 'dbe7544fa4ce8e23a74e7f6f4e7320ee', 'Ellis & Watts', NULL, '2M592146361109005', NULL, NULL, NULL, 'N', NULL, 'MIR inv num 3522', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:43Z', '7a6f0155408597c1846d79869e3a921c'),
	(28, 'TR1028', 'TR1028', 'N', 'Y', 'f97568303e9831143bdbaceae2431a19', 'Calumet', NULL, '1TKH04822WB053692', NULL, NULL, NULL, 'N', NULL, 'MIR inv num 3920', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:43Z', '81691135165e38b20da6b4506a3520fc'),
	(29, 'TR1029', 'TR1029', 'N', 'Y', '28d65e328068a1e96072695e14599424', 'Ellis & Watts', NULL, '1JJV482W5XL512394', NULL, NULL, NULL, 'N', NULL, 'MIR inv num AP-MR-106', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:43Z', '844ac514cb3d2ba2e981712e9d904b07'),
	(30, 'TR1030', 'TR1030', 'N', 'Y', '88e3dc2dae9668d7475ee9dd1022c68c', 'Ellis & Watts', NULL, '2M592146971109012', NULL, NULL, NULL, 'N', NULL, 'MIR inv num 6364', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:43Z', 'b0ad0ae83f2b87e7e867990fa0961e70'),
	(31, 'TR1031', 'TR1031', 'N', 'Y', '5a88e9aaa78ad9e15e632c0d34f5047b', 'Calumet', NULL, '1TKH05021YB032062', NULL, NULL, NULL, 'N', NULL, 'MIR inv num AP-MR-2449', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:43Z', '44085f9caa36458e946cb516157e68f4'),
	(32, 'TR1032', 'TR1032', 'N', 'Y', '6a75a62389c697ef4fae6f413ca20c5f', 'Calutech', NULL, '1TKH048263B122167', NULL, NULL, NULL, 'N', NULL, 'MIR inv num none', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:43Z', 'e0b9645628f9e161c2877afeba693af0'),
	(33, 'TR1033', 'TR1033', 'N', 'Y', 'ba9a815b87c446d00ded3ad6e2c2123e', 'Calumet', NULL, '1TKH048281B083742', NULL, NULL, NULL, 'N', NULL, 'MIR inv num none', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:43Z', '0bf3ac666a6af97cd8848052f762b4e5'),
	(34, 'TR1034', 'TR1034', 'N', 'Y', '71e404759c3410022799a67a946aa352', 'Schien', NULL, '1S9FA482051182781', NULL, NULL, NULL, 'N', NULL, 'MIR inv num none', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:43Z', 'b0e466d0489cfb4f2876b92603c6f738'),
	(35, 'TR1035', 'TR1035', 'N', 'Y', 'bcb5f5f3fe2a7cd89aa50904cdcfecba', 'Schien', NULL, '1S9FA482421182469', NULL, NULL, NULL, 'N', NULL, 'MIR inv num none', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:43Z', 'ba9bd3d35722b01bcdbcf053c2d19fa5'),
	(36, 'TR1036', 'TR1036', 'N', 'Y', 'dd4720bd600f0f1c2c1e96e0abb641cb', 'AK Specialty', NULL, '1TKH048221B104374', NULL, NULL, NULL, 'N', NULL, 'MIR inv num 2528', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:43Z', 'd5b9f269cc04b6e9cba9cbaf15564c8f'),
	(37, 'TR1037', 'TR1037', 'N', 'Y', 'c553eb592dc5874f218866ef09c8728e', 'Calumet', NULL, '1LH142UH011011370', NULL, NULL, NULL, 'N', NULL, 'MIR inv num AP-NM-2495', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:43Z', 'f4669fb3d82ced9f42a259e33dfb58f5'),
	(38, 'TR1038', 'TR1038', 'N', 'Y', '0c43f0c17f18cfad267a491b92e32844', 'Medicoach', NULL, '1M9A6A62XXH022146', NULL, NULL, NULL, 'N', NULL, 'MIR inv num AP-NM-1681', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:43Z', '2442eadf0b170da4243ceaecef441260'),
	(39, 'TR1039', 'TR1039', 'N', 'Y', '9eb2dd4c4f27c93a3c306ec9f50e6825', 'AK Specialty', NULL, '1LH142UH221012151', NULL, NULL, NULL, 'N', NULL, 'MIR inv num 3885', 'N', NULL, NULL, 'unassigned', '2015-09-03T21:31:43Z', 'b9548f19580798726124a8ce9251709c'),
	(40, 'TR1040', 'TR1040', 'N', 'Y', '0b74370dc6b70eaae199c9af4a7464b1', 'Ellis & Watts', '2000', '1JJV482W2YL515108', 'HS53402', 'CO', '1970-01-01T00:00:00Z', 'N', '', '', 'N', NULL, NULL, 'hennessyl', '2016-02-02T15:43:55Z', '97d729046617f2bbb88c38a10464f087'),
	(41, '1041', 'TR1041', 'N', 'Y', '2f336c7a44de09ac2f69c9a0d13c8a17', 'Land', '2005', '1LH142UHX51013911', 'D150245', '', '1970-01-01T00:00:00Z', 'N', '', '', 'N', NULL, NULL, 'hennessyl', '2016-03-11T16:34:53Z', 'cc54dcfb24f9cf85e3791ac42122fa15');
/*!40000 ALTER TABLE `trailers` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
