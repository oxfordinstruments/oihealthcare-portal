-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for procedure oiheal5_report.users_data_get
DROP PROCEDURE IF EXISTS `users_data_get`;
DELIMITER //
CREATE DEFINER=`oiheal5_justin`@`69.27.58.13` PROCEDURE `users_data_get`(IN `userid` VARCHAR(100))
    SQL SECURITY INVOKER
BEGIN
SET @@group_concat_max_len = 10000;
SET @sql = NULL;

SET @grp = NULL;
SELECT GROUP_CONCAT(DISTINCT CONCAT_WS('','MAX(IF(`group` = ''', `group`, ''', \'Y\', \'N\')) AS ', `group`)) INTO @grp from users_group_id;

SET @role = NULL;
SELECT GROUP_CONCAT(DISTINCT CONCAT_WS('','MAX(IF(role = ''', role, ''', \'Y\', \'N\')) AS ', role)) INTO @role from users_role_id;

SET @perm = NULL;
SELECT GROUP_CONCAT(DISTINCT CONCAT_WS('','MAX(IF(perm = ''', perm, ''', \'Y\', \'N\')) AS ', perm)) INTO @perm from users_perm_id;

SET @pref = NULL;
SELECT GROUP_CONCAT(DISTINCT CONCAT_WS('','MAX(IF(pref = ''', pref, ''', \'Y\', \'N\')) AS ', pref)) INTO @pref from users_pref_id;

SET @qual = NULL;
SELECT GROUP_CONCAT(DISTINCT CONCAT_WS('','MAX(IF(qual = ''', qual, ''', \'Y\', \'N\')) AS ', qual)) INTO @qual from users_qual_id;

SET @sql = CONCAT_WS('','SELECT u.*,
(SELECT location FROM users_role_id WHERE id = u.default_role) AS location,
(SELECT role FROM users_role_id WHERE id = u.default_role) AS role,
', @grp, ',' , @role, ',', @perm, ',', @pref, ',', @qual, ', c.customer_id
FROM users AS u
LEFT JOIN users_groups AS ug ON ug.uid = u.uid
LEFT JOIN users_group_id AS gid ON ug.gid = gid.id
LEFT JOIN users_roles AS ur ON ur.uid = u.uid
LEFT JOIN users_role_id AS rid ON ur.rid = rid.id
LEFT JOIN users_perms AS up ON up.uid = u.uid
LEFT JOIN users_perm_id AS pid ON up.pid = pid.id
LEFT JOIN users_prefs AS upr ON upr.uid = u.uid
LEFT JOIN users_pref_id AS prid ON upr.pid = prid.id
LEFT JOIN users_quals AS ql ON ql.uid = u.uid
LEFT JOIN users_qual_id AS qlid ON qlid.id = ql.qid
LEFT JOIN customers AS c ON c.unique_id = u.customer_unique_id
WHERE u.uid = ''', userid, '''
GROUP BY u.uid');


PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

END//
DELIMITER ;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
