-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.users_perm_id
DROP TABLE IF EXISTS `users_perm_id`;
CREATE TABLE IF NOT EXISTS `users_perm_id` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `perm` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `notes` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sequence` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Edit with care';

-- Dumping data for table oiheal5_report.users_perm_id: ~33 rows (approximately)
/*!40000 ALTER TABLE `users_perm_id` DISABLE KEYS */;
INSERT INTO `users_perm_id` (`id`, `name`, `perm`, `notes`, `sequence`) VALUES
	(1, 'Create Service Requests', 'perm_service_request', NULL, 5.1),
	(2, 'Remote Engineer/Contractor', 'perm_remote_engineer', '(note: full address required)', 3.1),
	(3, 'Receive SMS Alerts', 'perm_rcv_sms', NULL, 2.5),
	(4, 'Receive System Status Update Emails', 'perm_rcv_status_updates', NULL, 2.1),
	(5, 'Receive Service Report Emails', 'perm_rcv_service_reports', NULL, 2.4),
	(6, 'Receive Service Request Emails', 'perm_rcv_service_requests', NULL, 2.3),
	(7, 'Receive Valuation Request Emails', 'perm_rcv_valuation_request', '(note: management role only)', 2.6),
	(8, 'Receive Invoice Request Emails', 'perm_rcv_invoice_request', '(note: finance role only)', 2.2),
	(9, 'Change Phone Number', 'perm_change_phone', '(note: only works if engineer role not selected)', 0),
	(10, 'Manage Systems', 'perm_edit_systems', NULL, 4.3),
	(11, 'Engineer/Contractor With No Assigned Systems', 'perm_no_assigned_systems', NULL, 3.2),
	(12, 'Maintenance Menu Access', 'perm_maintenance_menus', NULL, 20.1),
	(14, 'Access DMS', 'perm_dms_access', NULL, 1.1),
	(15, 'Access KB', 'perm_kb_access', NULL, 1.2),
	(16, 'Access OiVision', 'perm_oivision_access', '(note: not implimented yet)', 1.3),
	(17, 'Customer Facility Manager', 'perm_facility_manager', NULL, 0),
	(18, 'Extended Login Time', 'perm_extend_login', '(note: 10hr login time)', 5.2),
	(19, 'NPS Management', 'perm_nps_menus', NULL, 30.1),
	(20, 'Edit Service Reports', 'perm_edit_reports', NULL, 20.3),
	(21, 'MRI Readings/Input Menus', 'perm_mri_readings_menus', NULL, 10.1),
	(22, 'Receive Assignments Change Email', 'perm_rcv_assignment_change', NULL, 2.7),
	(24, 'NPS Reports', 'perm_nps_reports', NULL, 30.2),
	(25, 'Face Sheet Print', 'perm_face_print', NULL, 6.1),
	(26, 'Access Remote Diags', 'perm_remote_diag', NULL, 1.4),
	(27, 'Manage Customers', 'perm_edit_customers', NULL, 4.1),
	(28, 'Manage Facilities', 'perm_edit_facilities', NULL, 4.2),
	(29, 'Manage Users', 'perm_edit_users', NULL, 4.4),
	(30, 'Submit PAR/CAR/FBC', 'perm_submit_parcarfbc', NULL, 25.1),
	(31, 'Close PAR/CAR/FBC', 'perm_close_parcarfbc', NULL, 25.2),
	(32, 'View Closed PAR/CAR/FBC', 'perm_view_parcarfbc', NULL, 25.3),
	(33, 'Tool Calibration Edit', 'perm_tool_cal_edit', NULL, 35.1),
	(34, 'Tool Calibration Vew Only', 'perm_tool_cal_view', NULL, 35.2),
	(35, 'Manage Trailers', 'perm_edit_trailers', NULL, 4.5),
	(36, 'Receive PARCARFBC Submit Email', 'perm_rcv_parcarfbc_submit', NULL, 2.8);
/*!40000 ALTER TABLE `users_perm_id` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
