-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.iso_car_files
DROP TABLE IF EXISTS `iso_car_files`;
CREATE TABLE IF NOT EXISTS `iso_car_files` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `unique_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_location` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_size` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'user who added file',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `FK_reports_files_requests` (`unique_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='files linked to each car';

-- Dumping data for table oiheal5_report.iso_car_files: ~11 rows (approximately)
/*!40000 ALTER TABLE `iso_car_files` DISABLE KEYS */;
INSERT INTO `iso_car_files` (`id`, `unique_id`, `file_location`, `file_name`, `file_size`, `uid`) VALUES
	(1, '0cf4c68ef86ac38358b4800fe8ee3baf', '/iso_car_files/0cf4c68ef86ac38358b4800fe8ee3baf/files/', 'OxfordAuditReport2015.pdf', '437824', 'mardikianj'),
	(2, '0cf4c68ef86ac38358b4800fe8ee3baf', '/iso_car_files/0cf4c68ef86ac38358b4800fe8ee3baf/files/', 'Siemens Customer Complaint Action .pdf', '48887', 'mardikianj'),
	(3, 'bd9873b0e68e8a67774743467e1c1fc4', '/iso_car_files/bd9873b0e68e8a67774743467e1c1fc4/files/', 'Internal Audit 5-20-2015 vacaville.doc', '1001472', 'mardikianj'),
	(5, 'ea9aa763a03b28e90b8e69684bab17fb', '/iso_car_files/ea9aa763a03b28e90b8e69684bab17fb/files/', 'WorkZone_Recent_Activity_20150702_2127(1).pdf', '89179', 'clarkf'),
	(6, '0cf4c68ef86ac38358b4800fe8ee3baf', '/iso_car_files/0cf4c68ef86ac38358b4800fe8ee3baf/files/', 'OxfordSCARForm1.pdf', '91436', 'mardikianj'),
	(7, '103b592306f2113e6faa132c971a72c6', '/iso_car_files/103b592306f2113e6faa132c971a72c6/files/', 'CMS564cb826125e4.mp3', '860976', 'mardikianj'),
	(8, '103b592306f2113e6faa132c971a72c6', '/iso_car_files/103b592306f2113e6faa132c971a72c6/files/', 'FW CMS Voice Recording.msg', '931840', 'mardikianj'),
	(9, '103b592306f2113e6faa132c971a72c6', '/iso_car_files/103b592306f2113e6faa132c971a72c6/files/', 'FW_ [CMS] Voice Recording.pdf', '1543069', 'mardikianj'),
	(10, '103b592306f2113e6faa132c971a72c6', '/iso_car_files/103b592306f2113e6faa132c971a72c6/files/', 'Incident report 11-17.pdf', '809211', 'mardikianj'),
	(11, '103b592306f2113e6faa132c971a72c6', '/iso_car_files/103b592306f2113e6faa132c971a72c6/files/', 'Path Medical 11-18-15 incident.pdf', '64265', 'mardikianj'),
	(13, '622ae9d4f428b03ffcc8363f72894038', '/iso_car_files/622ae9d4f428b03ffcc8363f72894038/files/', 'RE_ Corrective action.pdf', '643015', 'mardikianj'),
	(14, '3758b4b4bfa38f5e31bc901191df307e', '/iso_car_files/3758b4b4bfa38f5e31bc901191df307e/files/', 'FW_ Deerfield Audit Findings & Questions.pdf', '793700', 'mardikianj');
/*!40000 ALTER TABLE `iso_car_files` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
