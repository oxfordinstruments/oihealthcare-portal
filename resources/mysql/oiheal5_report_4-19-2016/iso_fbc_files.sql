-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.iso_fbc_files
DROP TABLE IF EXISTS `iso_fbc_files`;
CREATE TABLE IF NOT EXISTS `iso_fbc_files` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `unique_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_location` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_size` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'user who added file',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `FK_reports_files_requests` (`unique_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='files linked to each fbc';

-- Dumping data for table oiheal5_report.iso_fbc_files: ~6 rows (approximately)
/*!40000 ALTER TABLE `iso_fbc_files` DISABLE KEYS */;
INSERT INTO `iso_fbc_files` (`id`, `unique_id`, `file_location`, `file_name`, `file_size`, `uid`) VALUES
	(2, '3746d5b67bfec9ebc23fb398efec49f6', '/iso_fbc_files/3746d5b67bfec9ebc23fb398efec49f6/files/', 'CT Problem Email - 041315.pdf', '39377', 'hengemuhlew'),
	(3, 'd67260eb45ab6f7b215f86bc793ca5ec', '/iso_fbc_files/d67260eb45ab6f7b215f86bc793ca5ec/files/', '[CMS] Message Report-MRI Woodbridge.pdf', '96462', 'mardikianj'),
	(4, '8efb563eee75d5400758af100b3425f4', '/iso_fbc_files/8efb563eee75d5400758af100b3425f4/files/', 'FBC opened for MR2118.pdf', '660706', 'mardikianj'),
	(5, '8efb563eee75d5400758af100b3425f4', '/iso_fbc_files/8efb563eee75d5400758af100b3425f4/files/', 'VISA MRI system in Anchorage, AK.pdf', '588447', 'mardikianj'),
	(6, '8efb563eee75d5400758af100b3425f4', '/iso_fbc_files/8efb563eee75d5400758af100b3425f4/files/', 'VISA MRI service update in Anchorage, AK.pdf', '588662', 'mardikianj'),
	(7, '8efb563eee75d5400758af100b3425f4', '/iso_fbc_files/8efb563eee75d5400758af100b3425f4/files/', 'Site 2118 MRI issue in Anchorage  AK.msg', '30208', 'mardikianj');
/*!40000 ALTER TABLE `iso_fbc_files` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
