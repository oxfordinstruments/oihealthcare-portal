-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.misc_accessories
DROP TABLE IF EXISTS `misc_accessories`;
CREATE TABLE IF NOT EXISTS `misc_accessories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modality` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_misc_accessories_misc_modalities` (`modality`),
  CONSTRAINT `FK_misc_accessories_misc_modalities` FOREIGN KEY (`modality`) REFERENCES `misc_modalities` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table oiheal5_report.misc_accessories: ~12 rows (approximately)
/*!40000 ALTER TABLE `misc_accessories` DISABLE KEYS */;
INSERT INTO `misc_accessories` (`id`, `name`, `notes`, `modality`) VALUES
	(1, 'Phantom Holder', NULL, 1),
	(2, '42 cm PP Phantom', NULL, 1),
	(3, '35 cm PP Phantom', NULL, 1),
	(4, 'QA Phantom (Single)', 'Single Slice', 1),
	(5, 'QA Phantom (Multi)', 'Multi Slice', 1),
	(6, 'XT Phantom', NULL, 1),
	(7, 'Cradle Extender', NULL, 1),
	(8, 'Axial Head Holder', NULL, 1),
	(9, 'Cronal Head Holder', NULL, 1),
	(10, 'Arm Board', NULL, 1),
	(11, 'IV Pole', NULL, 1),
	(12, 'Prep Tray', NULL, 1),
	(13, 'Table Pads', NULL, 2);
/*!40000 ALTER TABLE `misc_accessories` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
