-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.misc_addons
DROP TABLE IF EXISTS `misc_addons`;
CREATE TABLE IF NOT EXISTS `misc_addons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modality` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_misc_addons_misc_modalities` (`modality`),
  CONSTRAINT `FK_misc_addons_misc_modalities` FOREIGN KEY (`modality`) REFERENCES `misc_modalities` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=220 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table oiheal5_report.misc_addons: ~8 rows (approximately)
/*!40000 ALTER TABLE `misc_addons` DISABLE KEYS */;
INSERT INTO `misc_addons` (`id`, `name`, `notes`, `modality`) VALUES
	(212, 'Oncology Top', NULL, 1),
	(213, '16ch Head Neck Coil', NULL, 2),
	(214, 'Service Agreement', NULL, 1),
	(215, 'Service Agreement', NULL, 2),
	(216, 'Chiller', NULL, 2),
	(217, 'Surge Supressor', NULL, 1),
	(218, 'Other', NULL, 1),
	(219, 'Other', NULL, 2);
/*!40000 ALTER TABLE `misc_addons` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
