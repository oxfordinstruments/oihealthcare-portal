-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.misc_contracts
DROP TABLE IF EXISTS `misc_contracts`;
CREATE TABLE IF NOT EXISTS `misc_contracts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoiced` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `map_dot` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `map_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='list of contract types\r\n';

-- Dumping data for table oiheal5_report.misc_contracts: ~9 rows (approximately)
/*!40000 ALTER TABLE `misc_contracts` DISABLE KEYS */;
INSERT INTO `misc_contracts` (`id`, `type`, `invoiced`, `map_dot`, `map_name`) VALUES
	(1, 'Contract Service', 'N', 'green-dot.png', 'contract'),
	(2, 'Paid Service (T&M)', 'Y', 'yellow-dot.png', 'paid'),
	(3, 'Warranty Service', 'N', 'pink-dot.png', 'warranty'),
	(4, 'Install/Deinstall', 'Y', 'red-dot.png', 'install'),
	(5, 'Parts Only', 'Y', 'purple-dot.png', 'parts'),
	(6, 'Labor Only', 'Y', 'blue-dot.png', 'labor'),
	(7, 'Lease', 'Y', 'orange-dot.png', 'lease'),
	(8, 'PM Only', 'Y', 'ltblue-dot.png', 'pm'),
	(9, 'Unknown', 'Y', 'red-pushpin.png', 'unknown');
/*!40000 ALTER TABLE `misc_contracts` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
