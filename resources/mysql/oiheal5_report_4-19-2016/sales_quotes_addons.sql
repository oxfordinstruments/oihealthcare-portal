-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.sales_quotes_addons
DROP TABLE IF EXISTS `sales_quotes_addons`;
CREATE TABLE IF NOT EXISTS `sales_quotes_addons` (
  `addon_id` int(11) DEFAULT NULL,
  `sales_quotes_id` int(11) DEFAULT NULL,
  `price` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `detail` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `FK_sales_quotes_addons_misc_addons` (`addon_id`),
  KEY `FK_sales_quotes_addons_sales_quotes` (`sales_quotes_id`),
  CONSTRAINT `FK_sales_quotes_addons_misc_addons` FOREIGN KEY (`addon_id`) REFERENCES `misc_addons` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_sales_quotes_addons_sales_quotes` FOREIGN KEY (`sales_quotes_id`) REFERENCES `sales_quotes` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table oiheal5_report.sales_quotes_addons: ~18 rows (approximately)
/*!40000 ALTER TABLE `sales_quotes_addons` DISABLE KEYS */;
INSERT INTO `sales_quotes_addons` (`addon_id`, `sales_quotes_id`, `price`, `detail`) VALUES
	(213, 1004, '11500', 'Red'),
	(216, 1004, '2200', 'Nothing'),
	(213, 1007, '11500.00', 'Pink'),
	(215, 1007, '150000.00', '5 Year'),
	(213, 1008, '11500.00', 'Pink'),
	(215, 1008, '150000.00', '5 Year'),
	(213, 1010, '11500.00', 'a'),
	(216, 1010, '2200.00', 'b'),
	(213, 1011, '11500.00', 'Extra Pads'),
	(216, 1011, '2200.00', '10 Ton'),
	(213, 1012, '11500.00', 'Extra Pads'),
	(216, 1012, '2200.00', '10 Ton'),
	(213, 1013, '11500.00', 'Extra Pads'),
	(216, 1013, '2200.00', '10 Ton'),
	(215, 1013, '0.00', '36-Months Full Service @ $76,000/yr.'),
	(213, 1014, '11500.00', 'Extra Pads'),
	(216, 1014, '2200.00', '10 Ton'),
	(215, 1014, '0.00', '36-Months Full Service @ $76,000/yr.');
/*!40000 ALTER TABLE `sales_quotes_addons` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
