-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.systems_assigned_customer
DROP TABLE IF EXISTS `systems_assigned_customer`;
CREATE TABLE IF NOT EXISTS `systems_assigned_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `system_ver_unique_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table oiheal5_report.systems_assigned_customer: ~67 rows (approximately)
/*!40000 ALTER TABLE `systems_assigned_customer` DISABLE KEYS */;
INSERT INTO `systems_assigned_customer` (`id`, `system_ver_unique_id`, `uid`) VALUES
	(11, '87a2c75401e6a24722ce2103ffdf0157', 'stadob'),
	(12, '0ce5eb2fb856be189665cbbc85196827', 'lvpcc'),
	(13, 'f52f38aa1c5eb8226b0add9a0fb555ef', 'mdimaging'),
	(14, '6a9b14392eec70e9867b0f07b80673fe', 'mdimaging'),
	(15, '8f8fba6e28fabe9f1dae1ced8cbb4621', 'mdimaging'),
	(16, '4db805ed0059d846805f1f4729ffce9d', 'mdimaging'),
	(17, 'b860daed4348fda65206fd1f20d73f5e', 'flmdvm'),
	(18, 'f18e772202c6bc61ff9eff952384fee2', 'pcomri'),
	(19, '882843752ff20e5eba5d6a4fa69522bb', 'tleonzal'),
	(20, '0ac0e1c463636630dd5a070ad06b7fdc', 'tleonzal'),
	(21, '63a01f71d8b50247747f956bf10a52a7', 'cdycus'),
	(22, '40b0c85c64af7b8fd897653d38316ddf', 'jmpfankuch'),
	(23, '8b5fc9b9ceb5eac3a7bb0f997ae1c51c', 'dhouston'),
	(24, '8f21be428bb0234116272b7cd4035602', 'hongdu'),
	(25, '46d5e385f6a7a2c2aacadc77bb192fac', 'cmzoss'),
	(26, '319d1a21fd22a1fcce89c1a2bd7bf3ed', 'cmzoss'),
	(27, 'e5115e403c03a22ae1dcc50a719f695c', 'cmzoss'),
	(29, '0c29f9f2c77bad3e8ed8ee2b087694d6', 'eosborne'),
	(31, '62a7d174f7d1d0df2fa8a50a7f123e90', 'ptierney'),
	(32, 'a8893ec77ad248f67bf604d6aaa16a89', 'walkers1'),
	(33, '2375125893aed49dcbe8cac6b8e07dd7', 'jgonzalez'),
	(34, '7e40402bfafdeb75cf898281086c00b1', 'jgonzalez'),
	(35, '87b1fd98484f32c588696214d12abdb4', 'bayviewradiology'),
	(36, '519c1b334e01778572493b933981e30a', 'bayviewradiology'),
	(37, '415259d2c4a7dc39097fd36985317f6d', 'ahphilyaw'),
	(38, 'a01f53926368194bcd66b48db6f7c0fe', 'ksingh'),
	(39, 'a8893ec77ad248f67bf604d6aaa16a89', 'munazzam'),
	(40, '50a1f3f9293638c3348c1d68df9a7c6c', 'customer'),
	(41, 'd174475aba3d81c1fc2ed94022e09ef8', 'customer'),
	(42, '086c102bdeee65597ca42774a98cc163', 'customer'),
	(43, '11831d7d6be47709ea6ddad78098d4c7', 'customer'),
	(44, '826512f7592447f3bc822b1b92524139', 'customer'),
	(45, 'c328ac35f38a645c89b34deb60acbaa4', 'borellap'),
	(46, '94dde769877a9ca516b5fea995211668', 'vrccmri'),
	(47, 'b83e067b9999f311e2f8f2fb5c000ada', 'jcdameron'),
	(48, 'c6ffccee2e0f39f1b5addf8dbb43a691', 'sgordon'),
	(49, 'f0a035a99199e651cdd8267fa661e3a4', 'dgrnja'),
	(50, '5b192d30b98caddde15242ca04420b14', 'dbryant'),
	(51, '0b2ae01751eb36c7db319fc8b12a4779', 'parkd'),
	(52, '3fec285143404dc39e275dc80f5e0845', 'parkd'),
	(53, '0b2ae01751eb36c7db319fc8b12a4779', 'barnesn'),
	(54, '3fec285143404dc39e275dc80f5e0845', 'barnesn'),
	(55, '6859f1a27c148c71bd9aad5297a0de71', 'jared.newton@meritushealth.com'),
	(56, 'a66cfd21478f9869ff18dea02378ecf0', 'sankam@srhca.org'),
	(57, 'a66cfd21478f9869ff18dea02378ecf0', 'clasiter@srhca.org'),
	(60, '40b0c85c64af7b8fd897653d38316ddf', 'customernew'),
	(61, '2f7f637fe229fd05e863ec2a21194432', 'lyndsi@jeffersonimaging.com'),
	(63, '2f7f637fe229fd05e863ec2a21194432', 'suzannemuntz@comcast.net'),
	(65, '832ff3d1d723c435726c85028954da99', 'ramesh.nijjar@olathehealth.org'),
	(66, '8fb0cf445c6c28f7dd673b7c624b62d5', 'jjohansen@savneurospec.com'),
	(67, '5fd794f71388c0011ccfe354c5838b0a', 'lsaitta@inmedllc.com'),
	(68, 'dba274038acedc35eeaf3e4e79a4ad47', 'denise@pathmedical.com'),
	(69, 'd5373454ebabdc83a18b2e1e1547f875', 'reneew@ntxortho.com'),
	(70, '832ff3d1d723c435726c85028954da99', 'james.neihart@olathehealth.org'),
	(71, 'f5bffb0f0b1026096971f644b10832c9', 'jonathan.scott.boan@gmail.com'),
	(72, 'b57cd79d8eaab56bc74211637e78dc2c', 'jonathan.scott.boan@gmail.com'),
	(74, '62a7d174f7d1d0df2fa8a50a7f123e90', 'paul.ullrich@stjoe.org'),
	(75, 'c7832739d6ac0ed00a9a05baa43c743b', 'rlynch@dvullc.com'),
	(76, 'efe2b890c8f73f035a1836b6abb7da93', 'carmen.salvat@mch.com'),
	(77, 'b796322df51289906f748e3612d01ed9', 'carmen.salvat@mch.com'),
	(78, 'c7832739d6ac0ed00a9a05baa43c743b', 'woresick@dvullc.com'),
	(79, 'f7f3aab76c71c9ea898e6057a0dce555', 'mark.allgeyer@opensidedmri.net'),
	(80, 'd742aa8a67ec891bd89db4e094a52d30', 'kmitchell@centralohiourology.com'),
	(81, '5e769beb51de47a0b772c3b559b70c71', 'kmitchell@centralohiourology.com'),
	(82, 'c328ac35f38a645c89b34deb60acbaa4', 'mjones@petcureoncology.com'),
	(83, '2411853d3482f8b61d5aa081d3bc08da', 'jlandgraf@essehealth.com'),
	(84, '77c3d5c69fdf6f3abed85bece41ed5d9', 'cbindhamer@eliteradga.com'),
	(85, '3799c9051cc081ce494f28e5b854ab17', 'hcheek@crownimaging.com');
/*!40000 ALTER TABLE `systems_assigned_customer` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
