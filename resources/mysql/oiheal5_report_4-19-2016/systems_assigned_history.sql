-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.systems_assigned_history
DROP TABLE IF EXISTS `systems_assigned_history`;
CREATE TABLE IF NOT EXISTS `systems_assigned_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_sent` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table oiheal5_report.systems_assigned_history: ~10 rows (approximately)
/*!40000 ALTER TABLE `systems_assigned_history` DISABLE KEYS */;
INSERT INTO `systems_assigned_history` (`id`, `filename`, `date`, `email_sent`) VALUES
	(18, 'asgn_2016-04-07T13-50_18.xml', '2016-04-07T13:50:02Z', 'Y'),
	(19, 'asgn_2016-04-07T16-50_19.xml', '2016-04-07T16:50:02Z', 'Y'),
	(20, 'asgn_2016-04-07T19-50_20.xml', '2016-04-07T19:50:01Z', 'Y'),
	(21, 'asgn_2016-04-11T19-50_21.xml', '2016-04-11T19:50:01Z', 'Y'),
	(22, 'asgn_2016-04-12T16-50_22.xml', '2016-04-12T16:50:01Z', 'Y'),
	(23, 'asgn_2016-04-12T21-50_23.xml', '2016-04-12T21:50:01Z', 'Y'),
	(24, 'asgn_2016-04-13T13-50_24.xml', '2016-04-13T13:50:01Z', 'Y'),
	(25, 'asgn_2016-04-13T16-50_25.xml', '2016-04-13T16:50:01Z', 'Y'),
	(26, 'asgn_2016-04-14T19-50_26.xml', '2016-04-14T19:50:02Z', 'Y'),
	(27, 'asgn_2016-04-15T16-50_27.xml', '2016-04-15T16:50:01Z', 'N');
/*!40000 ALTER TABLE `systems_assigned_history` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
