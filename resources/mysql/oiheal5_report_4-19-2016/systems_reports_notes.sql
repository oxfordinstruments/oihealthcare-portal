-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.systems_reports_notes
DROP TABLE IF EXISTS `systems_reports_notes`;
CREATE TABLE IF NOT EXISTS `systems_reports_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `note` text COLLATE utf8_unicode_ci,
  `uid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `report_unique_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  CONSTRAINT `FK_systems_reports_notes_users` FOREIGN KEY (`uid`) REFERENCES `users` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table oiheal5_report.systems_reports_notes: ~13 rows (approximately)
/*!40000 ALTER TABLE `systems_reports_notes` DISABLE KEYS */;
INSERT INTO `systems_reports_notes` (`id`, `note`, `uid`, `date`, `report_unique_id`) VALUES
	(1, 'David from site called in for an update. I did not see any updates in SalesForce and left a message with Brandy for the update.11:54 ', 'mardikianj', '2016-03-28T15:58:33Z', '114272647357252f6bf1cdac7fa53746'),
	(2, 'Bill CT PMI service at One-Way Travel as shown on Service Report.', 'galvackyd', '2016-03-28T18:54:34Z', 'd2cc62d128a535971c417e6c491bc319'),
	(3, 'Site is no longer on Maintenance Value Plan (MVP).  Bill Customer One-Way Travel as shown in report.', 'galvackyd', '2016-03-28T19:02:15Z', 'd2cc62d128a535971c417e6c491bc319'),
	(5, 'Felice , This is Billable , please ignore the do not bill message , it was a previous journal entry that shows in the service reminder section due to recent portal upgrade', 'frazierm', '2016-03-30T16:13:04Z', '173210cb893c4dccacb9100e0bb1d84e'),
	(6, 'partial payment for 64k bill. check 25k collected', 'hodgem', '2016-03-31T13:47:02Z', '3696f1e4922329275ea1b81a0ce40e7a'),
	(7, 'Customer has expressed that this site is know for power glitches. ', 'flechaa', '2016-04-01T15:41:29Z', '673047a1dac5ba6b84268c23a380e8b5'),
	(8, 'DBRS used their own Disk Drive.  Their price was $875.', 'grayg', '2016-04-04T20:06:47Z', 'b45b289b8eaed1f431d9591d01fee56e'),
	(9, 'See attached cold head service report.', 'moralesp', '2016-04-07T18:13:54Z', '478a05a9491bde54bd40f6a11afa181b'),
	(10, 'Tube rotor slight noise during spin up', 'davisj', '2016-04-11T14:21:12Z', '76e22eddb32a96306464d44819aca8cc'),
	(11, 'David should be onsite to replace the axial encoder the first of next week', 'davisj', '2016-04-18T14:24:50Z', '97e672195da191bda3264814d6bbc5fe'),
	(12, 'The options are copied from Lawrence\'s system. Due to St. Luke\'s taking over the service the original options should be re-installed. \nThe reason was for the wide-view option which is suspect if the customer even uses it, will find out.', 'davisj', '2016-04-18T14:27:31Z', '97e672195da191bda3264814d6bbc5fe'),
	(13, 'Many pins on the Multi-port assy and Head T/R assy were crushed to the point they would not make a reliable connection.  ', 'varnerd', '2016-04-19T14:36:22Z', '9e744fa1876ff97638227c055d0d8307'),
	(14, 'Trailer extremely dusty and dirty upon arrival. Someone had tracked in a lot of dirt on the floor of the trailer. Also, many large dust bunnies on the floor and on many surfaces of the scan room and operators room.  ', 'varnerd', '2016-04-19T14:45:18Z', '9e744fa1876ff97638227c055d0d8307');
/*!40000 ALTER TABLE `systems_reports_notes` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
