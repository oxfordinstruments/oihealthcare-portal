-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.systems_requests_notes
DROP TABLE IF EXISTS `systems_requests_notes`;
CREATE TABLE IF NOT EXISTS `systems_requests_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `note` text COLLATE utf8_unicode_ci,
  `uid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `request_unique_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  CONSTRAINT `FK_systems_requests_notes_users` FOREIGN KEY (`uid`) REFERENCES `users` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table oiheal5_report.systems_requests_notes: ~23 rows (approximately)
/*!40000 ALTER TABLE `systems_requests_notes` DISABLE KEYS */;
INSERT INTO `systems_requests_notes` (`id`, `note`, `uid`, `date`, `request_unique_id`) VALUES
	(5, 'Service Request \nFor: O/C\nCaller: Nate Engels \nPhone: 1763-486-3163 \nCompany: Path Medical Center \nSite ID: MR2236 \nMRI or CT: MRI \nMessage: I have an issue with the\ncompressor. My PSI is up to 5.436 \nCaller ID:7634863163\nPatch\nRelay Order: Mike Hodge, Mike\nFrazier \n3/28 9:01A Mike Hodge - Cell - Patch\nAccepted\nDelivered to: Mike Hodge \n Message History:\nTaken: Mon 28-Mar-2016 9:01a MLN\nGiven: Mon 28-Mar-2016 9:01a MLN\n', 'mellol', '2016-03-29T13:10:26Z', '3b49d6c1b87cdaa829855ef65157a1d1'),
	(6, 'Marc reported the issue is corrected 3/29', 'hennessyl', '2016-03-29T17:07:42Z', 'c876341773da8c0bf2a852ce8afd9123'),
	(7, 'Service Request \nFor: O/C\nCaller: Ollie Alexandre \nPhone: 1318-613-6266 \nCompany: Health Imaging Services \nSite ID: CT1240 \nMRI or CT: CT \nMessage: I am getting multiple error\ncodes on our CT machine. \nCaller ID:3186136266\nPatch\nRelay Order: Kenneth Schriver, Dale\nCoyle \n3/30 8:43A Kenneth Schriver - Cell -\nPatch Accepted\nDelivered to: Kenneth Schriver \n Message History:\nTaken: Wed 30-Mar-2016 8:43a SLM\nGiven: Wed 30-Mar-2016 8:43a SLM\n', 'mellol', '2016-03-30T13:04:42Z', '53711b60a6b050b1e700f1fe0528518b'),
	(8, 'Service Request \nFor: Justin\nCaller: Kelli Hancock \nPhone: 1937-386-0000 \nCompany: Adams County Cancer Center \nSite ID: CT1085 \nMRI or CT: CT \nMessage: Our screen is not formatting\nto the entire screen. The screen is\nonly show 3/4 of the screen. The other\nportion is black. Please advise. \nCaller ID:9373860000\nPatch\nRelay Order: Justin Davis, Jeremy\nReigle \n3/31 8:27A Justin Davis - Cell -\nPatch Accepted\n...\n Message History:\nTaken: Thu 31-Mar-2016 8:27a KJ\nGiven: Thu 31-Mar-2016 8:27a KJ\n', 'mellol', '2016-03-31T13:05:39Z', '641788dab83429051180933a286892f9'),
	(9, 'I contacted Irene with GE.  Service Request #1187442318014.  Tech will be contacting the site.  ', 'mellol', '2016-03-31T20:55:08Z', '48578d6b3df4a4e98333dd44dd83d662'),
	(10, '4-1-16 I contacted GE this morning and Operator Lizette told me the Engineer name is Greg Halstad.  He will be contacting us to inform us on the situation.  ', 'mellol', '2016-04-01T15:23:47Z', '48578d6b3df4a4e98333dd44dd83d662'),
	(11, 'GE Engineer Greg Halstad (505-235-0137) called back and stated they just needed to reboot.  She panicked when it said needs to be a forced reboot. ', 'mellol', '2016-04-01T15:40:15Z', '48578d6b3df4a4e98333dd44dd83d662'),
	(12, 'Service Request \nFor: O/C\nCaller: Len Bacino \nPhone: 1973-322-8254 \nCompany: Saint Barnabas Medical\nCenter \nSite ID: MR2270 \nMRI or CT: MRI \nMessage: We are having a problem with\nour computer room getting too warm. \nCaller ID:9733225000\nPatch\nRelay Order: Unassigned, Scott\nSimmons \nDelivered to: Mike Frazier \n Message History:\nTaken: Sun 03-Apr-2016 8:07a SLM\nGiven: Sun 03-Apr-2016 8:07a SLM\n', 'mellol', '2016-04-04T12:39:43Z', '2627d2ea46711068cd2cffa7c06479eb'),
	(13, 'Service Request \nFor: O/C\nCaller: Abalberto Bravo \nPhone: 1954-385-6249 \nCompany: Nicklaus / Miami Childrens\nHospital - Dan Marino Center \nSite ID: MR2006 \nMRI or CT: MRI \nMessage: The main power switch wont\nturn on. \nCaller ID:9543856200\nPatch\nRelay Order: Mike Hodge, Mike Frazier\nDelivered to: Mike Hodge \n Message History:\nTaken: Mon 04-Apr-2016 6:45a NTC\nGiven: Mon 04-Apr-2016 6:45a NTC\n', 'mellol', '2016-04-04T13:14:15Z', '760fd9d7bd20069ee9afd632375a0330'),
	(14, 'Service Request \nFor: O/C\nCaller: Nate Engels \nPhone: 1763-486-3163 \nCompany: Path Medical Center \nSite ID: MR2236 \nMRI or CT: MRI \nMessage: I am calling to get the\nstatus of our service request. \nCaller ID:7634863163\nPatch\nRelay Order: Mike Hodge, Mike Frazier\nDelivered to: Mike Frazier \n Message History:\nTaken: Mon 04-Apr-2016 8:15a CB\nGiven: Mon 04-Apr-2016 8:15a CB\n', 'mellol', '2016-04-04T13:23:29Z', '8a1852172ba0f7de27e567adadc45c47'),
	(15, 'Service Request \nFor: O/C\nCaller: Johanna Noguera \nPhone: 1954-295-6911 \nCompany: Path Medical Center \nSite ID: MR2236 \nMRI or CT: MRI \nMessage: There is an electrical smell\ncoming from the electrical room. The\ncryo cooler is off. Please give me a\ncall. \nCaller ID:9542956911\nPatch\nRelay Order: Mike Hodge, Mike Frazier\nDelivered to: Mike Hodge \n Message History:\nTaken: Sun 03-Apr-2016 3:48p SSS\nGiven: Sun 03-Apr-2016 3:48p SSS\n', 'mellol', '2016-04-04T13:26:54Z', '8a1852172ba0f7de27e567adadc45c47'),
	(16, 'Marc will be on site today 4/4/16 at 10AM PST', 'hennessyl', '2016-04-04T16:51:11Z', 'c876341773da8c0bf2a852ce8afd9123'),
	(17, 'Service Request \nFor: Mike F\nCaller: Mary Lou Samaras \nPhone: 1912-525-1302 \nCompany: Chatham Orthopaedic\nAssociates, P.A. \nSite ID: MR2042 \nMRI or CT: MRI \nMessage: My system is down. Please\ncall asap. \nCaller ID:9123556615\nPatch\nRelay Order: Mike Frazier, Jeremy\nReigle \nDelivered to: Mike Frazier \n Message History:\nTaken: Wed 06-Apr-2016 7:32a SLC\nGiven: Wed 06-Apr-2016 7:32a SLC\n', 'mellol', '2016-04-06T13:08:04Z', '3339c6b857b2daeacf0dbd74aa7e781a'),
	(18, 'I spoke with Michelle from ADI and she took over the call.  ', 'mellol', '2016-04-06T18:14:35Z', 'd43acd28d5ee766c8a95d44cf636c50e'),
	(19, '4-6-16 16:42pm Jaimey called back from the site and said it didn\'t work.  I connected him to Chris Core.  ', 'mellol', '2016-04-06T20:46:06Z', 'b3120afe85b1a519c828f0191b03761d'),
	(20, 'I spoke with Cindy from Consensys and she dispatched Tyler to call the site.  Tyler is also going to contact Mike Frazier with an update on what is going on.  ', 'mellol', '2016-04-06T21:02:29Z', '813088ec26e40433b1a34b65277b342a'),
	(21, 'Reassign to Chris Core per Kurt Bringolf', 'mellol', '2016-04-07T18:57:36Z', '8e71317e56612434130edd1f63b6ebf5'),
	(22, 'level 4 service key 7E58658E1566F5-F9D9CB expires 4/7/17', 'jacksonb', '2016-04-08T12:37:12Z', 'c2f28af961c0a1559e368ae3f4ca4dd0'),
	(23, 'Called Sheilds ProCare and Steve took the call.  ', 'mellol', '2016-04-12T12:44:43Z', '44b81ef7b5231f3538afe9661fea0d3a'),
	(24, 'Resigned to John Stark. He will be at the site today.  ', 'mellol', '2016-04-12T13:20:08Z', '5609bd04dd537994c35a2f1362e1ca08'),
	(25, 'Cristy called in today 4/13/16 saying that during the tube warm up she rec\'d the same error message from yesterday scanner stop hardware error. Placed another service call with GE ref# 1-188089733612', 'jacksonb', '2016-04-13T13:55:38Z', 'fc4ff9e0a12644550b4ff7212f4d94c5'),
	(26, 'Mark the GE FSE called they have a tube on order (which isn\'t covered under our contract) for delivery tomorrow contacting Kurt to see how we need to proceed ', 'jacksonb', '2016-04-13T14:55:16Z', 'fc4ff9e0a12644550b4ff7212f4d94c5'),
	(27, 'Last time Marc Pettit was there, he told them to call us if the pressure is below 1.  ', 'mellol', '2016-04-14T18:50:01Z', '5d145d92e9df18a134faeeb4489bc38e'),
	(28, 'Service Request \nFor: Phil\nCaller: Shawn McCracken \nPhone: 1408-829-5344 \nCompany: Antioch Magnetic Imaging \nSite ID: MR2059 \nMRI or CT: MRI \nMessage: We are having issues with\nthe machine, Phild told us not to shut\nit down, it is missing a battery,\nwhich is on order but right now it\nstopped and says panic/ error, I think\nit\'s dumped all of my information, I\'m\nnot sure how to proceed. \nCaller ID:4088295344\nPatch\nRelay Order: Philip Morales, Mike\nFrazier \n...\n Message History:\nTaken: Sat 16-Apr-2016 1:00p VMS\nGiven: Sat 16-Apr-2016 1:00p VMS\n Message Dispatch History:\n1.	Sat 16-Apr-2016 12:59p Dialout 17073173985\\18882120260\n2.	 Sat 16-Apr-2016 1:01p Text Messaged 7073173985\n3.	 Sat 16-Apr-2016 1:02p Text Messaged 5592842379\n4.	 Sat 16-Apr-2016 1:03p Text Messaged 3252013002\n5.	 Sat 16-Apr-2016 1:04p Text Messaged 5074509271\n', 'mellol', '2016-04-18T12:42:59Z', '5d41094be3f3c29e4ed2c98f8192f21c'),
	(29, 'Service Request \nFor: Support\nCaller: Jovani Rosado \nPhone: 1305-274-2777 \nCompany: Southeast Veterinary\nNeurology \nSite ID: MR2145 \nMRI or CT: MRI \nMessage: Having trouble booting up\nthe system for the MRI unit.\nAlternate number is 786-366-0862 \nCaller ID:3054802859\nPatch\nRelay Order: Mike Hodge, Dan Galvacky\nDelivered to: Mike Hodge \n Message History:\nTaken: Sun 17-Apr-2016 9:57a RAG\nGiven: Sun 17-Apr-2016 9:57a RAG\n', 'mellol', '2016-04-18T13:07:01Z', '8473d50789bd34baee81dcb80354934a'),
	(30, 'system up as of 1400 per Mike H.', 'hennessyl', '2016-04-18T18:12:56Z', '8473d50789bd34baee81dcb80354934a');
/*!40000 ALTER TABLE `systems_requests_notes` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
