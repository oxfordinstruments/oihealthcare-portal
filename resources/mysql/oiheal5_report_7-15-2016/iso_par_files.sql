-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.30 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.iso_par_files
DROP TABLE IF EXISTS `iso_par_files`;
CREATE TABLE IF NOT EXISTS `iso_par_files` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `unique_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_location` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_size` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'user who added file',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `FK_reports_files_requests` (`unique_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='files linked to each par';

-- Dumping data for table oiheal5_report.iso_par_files: ~2 rows (approximately)
/*!40000 ALTER TABLE `iso_par_files` DISABLE KEYS */;
INSERT INTO `iso_par_files` (`id`, `unique_id`, `file_location`, `file_name`, `file_size`, `uid`) VALUES
	(1, '0bedbe58e3edee03d71e649cfc12c4c3', '/iso_par_files/0bedbe58e3edee03d71e649cfc12c4c3/files/', 'Emergency Action Plan Rev. A 3-28-2016.docx', '75314', 'hennessyl');
/*!40000 ALTER TABLE `iso_par_files` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
