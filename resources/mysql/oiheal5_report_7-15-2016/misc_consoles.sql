-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.30 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.misc_consoles
DROP TABLE IF EXISTS `misc_consoles`;
CREATE TABLE IF NOT EXISTS `misc_consoles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modality` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_misc_consoles_misc_modalities` (`modality`),
  CONSTRAINT `FK_misc_consoles_misc_modalities` FOREIGN KEY (`modality`) REFERENCES `misc_modalities` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table oiheal5_report.misc_consoles: ~18 rows (approximately)
/*!40000 ALTER TABLE `misc_consoles` DISABLE KEYS */;
INSERT INTO `misc_consoles` (`id`, `name`, `notes`, `modality`) VALUES
	(1, 'H1 w/O2', '2180551', 1),
	(2, 'H1.2', '2180551-2', 1),
	(3, 'H2', '2266832, -2', 1),
	(4, 'H2 Octane II', '2304732, -2', 1),
	(5, 'H3', '2266832-3, -4', 1),
	(6, 'H3 Octane II', '2304732-3, -4', 1),
	(7, 'GOC1', 'IRIX OS', 1),
	(8, 'GOC2', 'Linux OS', 1),
	(9, 'GOC3', 'GRE Xtream', 1),
	(10, 'GOC4', 'GRE Xtream', 1),
	(11, 'GOC5', 'VCT', 1),
	(12, 'GOC6', 'VCT', 1),
	(13, 'Indigo', NULL, 2),
	(14, 'Octane', NULL, 2),
	(15, 'GOC', NULL, 2),
	(16, 'Indigo', 'CTi', 1),
	(17, 'Octane', 'CTi', 1),
	(18, 'O2', 'Xi', 1);
/*!40000 ALTER TABLE `misc_consoles` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
