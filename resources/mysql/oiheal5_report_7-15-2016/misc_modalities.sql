-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.30 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.misc_modalities
DROP TABLE IF EXISTS `misc_modalities`;
CREATE TABLE IF NOT EXISTS `misc_modalities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modality` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table oiheal5_report.misc_modalities: ~6 rows (approximately)
/*!40000 ALTER TABLE `misc_modalities` DISABLE KEYS */;
INSERT INTO `misc_modalities` (`id`, `modality`) VALUES
	(1, 'CT'),
	(2, 'MR'),
	(3, 'NM'),
	(4, 'PET'),
	(5, 'US'),
	(6, 'XR');
/*!40000 ALTER TABLE `misc_modalities` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
