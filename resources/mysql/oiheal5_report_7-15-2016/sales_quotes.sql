-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.30 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.sales_quotes
DROP TABLE IF EXISTS `sales_quotes`;
CREATE TABLE IF NOT EXISTS `sales_quotes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `revision` int(11) DEFAULT '0',
  `quote_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'rep initials + date + rev',
  `title` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_address` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_state` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_zip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_email` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emailed_quote` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Y N',
  `system_type` int(11) DEFAULT NULL,
  `system_table` int(11) DEFAULT NULL,
  `system_console` int(11) DEFAULT NULL,
  `system_price` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `warranty` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Y N',
  `warranty_months` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `agreement_percent` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shippment_percent` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `final_percent` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sales_tax_percent` varchar(10) COLLATE utf8_unicode_ci DEFAULT '0',
  `fees` varchar(20) COLLATE utf8_unicode_ci DEFAULT '0',
  `details` text COLLATE utf8_unicode_ci,
  `created_by` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `deleted_by` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contracted` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  `unique_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_sales_quotes_misc_consoles` (`system_console`),
  KEY `FK_sales_quotes_systems_types` (`system_type`),
  KEY `FK_sales_quotes_misc_tables` (`system_table`),
  KEY `FK_sales_quotes_users_2` (`created_by`),
  KEY `FK_sales_quotes_users_3` (`deleted_by`),
  CONSTRAINT `FK_sales_quotes_misc_consoles` FOREIGN KEY (`system_console`) REFERENCES `misc_consoles` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_sales_quotes_misc_tables` FOREIGN KEY (`system_table`) REFERENCES `misc_tables` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_sales_quotes_systems_types` FOREIGN KEY (`system_type`) REFERENCES `systems_types` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_sales_quotes_users_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`uid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_sales_quotes_users_3` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`uid`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1015 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table oiheal5_report.sales_quotes: ~12 rows (approximately)
/*!40000 ALTER TABLE `sales_quotes` DISABLE KEYS */;
INSERT INTO `sales_quotes` (`id`, `revision`, `quote_id`, `title`, `customer_name`, `customer_address`, `customer_city`, `customer_state`, `customer_zip`, `customer_email`, `emailed_quote`, `system_type`, `system_table`, `system_console`, `system_price`, `warranty`, `warranty_months`, `agreement_percent`, `shippment_percent`, `final_percent`, `sales_tax_percent`, `fees`, `details`, `created_by`, `created_date`, `deleted`, `deleted_by`, `deleted_date`, `contracted`, `unique_id`) VALUES
	(1000, 1, 'JRD-20150512-1', 'Customer Quote', 'Customer', 'Address', 'City', 'IN', 'Zip', NULL, NULL, 78, 3, 14, '30000', 'Y', NULL, NULL, NULL, NULL, NULL, '0', 'These are some details.\r\nCustomer wants a pink gantry\r\nWith a yellow table', 'davisj', '2015-05-12T00:00:00Z', 'N', NULL, NULL, 'N', '456'),
	(1001, 1, 'JRD-20150513-1', 'Dr. Pol Vet 11x MR', 'Dr. Pol Vet', 'Somewhere', 'Some City', 'MI', '12345', 'justin.davis@oxinst.com', 'Y', 138, 20, 15, '300000.00', 'Y', '12', '30', '60', '10', '7', '1500', 'These are some details.\r\nCustomer wants a pink gantry\r\nWith a yellow table\r\nAnd a PDU that will not explode', 'davisj', '2015-05-13T00:00:00Z', 'N', 'davisj', '', 'N', '123'),
	(1002, 2, 'JRD-20150514-2', 'Dr. Pol Vet 11x MR', 'Dr. Pol Vet', 'Somewhere', 'Some City', 'MI', '12345', 'justin.davis@oxinst.com', 'Y', 138, 20, 15, '291000.00', 'Y', '12', '30', '60', '10', '7', '0', 'These are some details.\r\nCustomer wants a pink gantry\r\nWith a yellow table\r\nAnd a PDU that will not explode', 'davisj', '2015-05-14T00:00:00Z', 'N', 'davisj', '', 'N', '123'),
	(1003, 3, 'JRD-20150515-3', 'Dr. Pol Vet 11x MR', 'Dr. Pol Vet', 'Somewhere', 'Some City', 'MI', '12345', 'justin.davis@oxinst.com', 'Y', 138, 20, 15, '291000.00', 'Y', '12', '30', '60', '10', '7', '0', 'These are some details.\r\nCustomer wants a pink gantry\r\nWith a yellow table\r\nAnd a PDU that will not explode', 'davisj', '2015-05-15T00:00:00Z', 'N', 'davisj', '', 'N', '123'),
	(1004, 4, 'JRD-20150516-4', 'Dr. Pol Vet 11x MR', 'Dr. Pol Vet', 'Somewhere', 'Some City', 'MI', '12345', 'justin.davis@oxinst.com', 'Y', 138, 20, 15, '291000.00', 'Y', '12', '30', '60', '10', '7', '0', 'These are some details.\r\nCustomer wants a pink gantry\r\nWith a yellow table\r\nAnd a PDU that will not explode', 'davisj', '2015-05-16T00:00:00Z', 'N', 'davisj', '', 'N', '123'),
	(1006, 5, 'JRD-20150519-5', 'Dr. Pol Vet 11x MR', 'Dr. Pol Vet', 'Somewhere', 'Some City', 'MI', '12345', 'justin.davis@oxinst.com', 'N', 138, 20, 15, '291000.00', 'Y', '12', '30', '60', '10', '7', '0.00', 'These are some details.\r\nCustomer wants a pink gantry\r\nWith a yellow table\r\nAnd a PDU that will not explode', 'davisj', '2015-05-19T20:01:35Z', 'N', NULL, NULL, 'N', '123'),
	(1007, 6, 'JRD-20150519-6', 'Dr. Pol Vet 11x MR', 'Dr. Pol Vet', 'Somewhere', 'Some City', 'MI', '12345', 'justin.davis@oxinst.com', 'N', 138, 20, 15, '291000.00', 'Y', '12', '30', '60', '10', '7', '0.00', 'These are some details.\r\nCustomer wants a pink gantry\r\nWith a yellow table\r\nAnd a PDU that will not explode', 'davisj', '2015-05-19T20:03:41Z', 'N', NULL, NULL, 'N', '123'),
	(1008, 7, 'JRD-20150519-7', 'Dr. Pol Vet 11x MR', 'Dr. Pol Vet', 'Somewhere', 'Some City', 'MI', '12345', 'justin.davis@oxinst.com', 'N', 138, 20, 15, '291000.00', 'Y', '12', '30', '60', '10', '7', '0.00', 'These are some details.\r\nCustomer wants a pink gantry\r\nWith a yellow table\r\nAnd a PDU that will not explode', 'davisj', '2015-05-19T20:10:53Z', 'N', NULL, NULL, 'N', '123'),
	(1009, 8, 'JRD-20150519-8', 'Dr. Pol Vet 11x MR', 'Dr. Pol Vet', 'Somewhere', 'Some City', 'MI', '12345', 'justin.davis@oxinst.com', 'N', 138, 20, 15, '291000.00', 'Y', '12', '30', '60', '10', '7', '0.00', 'These are some details.\r\nCustomer wants a pink gantry\r\nWith a yellow table\r\nAnd a PDU that will not explode', 'davisj', '2015-05-19T21:08:00Z', 'N', NULL, NULL, 'N', '123'),
	(1010, 9, 'JRD-20150519-9', 'Dr. Pol Vet 11x MR', 'Dr. Pol Vet', 'Somewhere', 'Some City', 'MI', '12345', 'justin.davis@oxinst.com', 'N', 138, 20, 15, '291000.00', 'Y', '12', '30', '60', '10', '7', '0.00', 'These are some details.\r\nCustomer wants a pink gantry\r\nWith a yellow table\r\nAnd a PDU that will not explode', 'davisj', '2015-05-19T21:10:55Z', 'N', NULL, NULL, 'N', '123'),
	(1011, 10, 'JRD-20150519-10', 'Dr. Pol Vet 11x MR', 'Dr. Pol Vet', 'Somewhere', 'Some City', 'MI', '12345', 'justin.davis@oxinst.com', 'N', 138, 20, 15, '291000.00', 'Y', '12', '30', '60', '10', '7', '0.00', 'These are some details.\r\nCustomer wants a pink gantry\r\nWith a yellow table\r\nAnd a PDU that will not explode', 'davisj', '2015-05-19T21:24:22Z', 'N', NULL, NULL, 'N', '123'),
	(1012, 11, 'JRD-20150519-11', 'Dr. Pol Vet 11x MR', 'Dr. Pol Vet', 'Somewhere', 'Some City', 'MI', '12345', 'justin.davis@oxinst.com', 'N', 138, 20, 15, '291000.00', 'Y', '12', '30', '60', '10', '7', '0.00', 'These are some details.\r\nCustomer wants a pink gantry\r\nWith a yellow table\r\nAnd a PDU that will not explode', 'davisj', '2015-05-19T21:30:32Z', 'N', NULL, NULL, 'N', '123'),
	(1013, 12, 'WH-20150527-12', 'Dr. Pol Vet 11x MR', 'Dr. Pol Vet', 'Somewhere', 'Some City', 'MI', '12345', 'justin.davis@oxinst.com', 'N', 138, 20, 15, '291000.00', 'Y', '12', '30', '60', '10', '7', '0.00', 'These are some details.\r\nCustomer wants a pink gantry\r\nWith a yellow table\r\nAnd a PDU that will not explode', 'hengemuhlew', '2015-05-27T15:19:06Z', 'N', NULL, NULL, 'N', '123'),
	(1014, 13, 'WH-20150527-13', 'Dr. Pol Vet 11x MR', 'Dr. Pol Vet', 'Somewhere', 'Some City', 'MI', '12345', 'justin.davis@oxinst.com', 'N', 138, 20, 15, '291000.00', 'Y', '12', '30', '60', '10', '7', '0.00', 'These are some details.\r\nCustomer wants a pink gantry\r\nWith a yellow table\r\nAnd a PDU that will not explode', 'hengemuhlew', '2015-05-27T15:20:12Z', 'N', NULL, NULL, 'N', '123');
/*!40000 ALTER TABLE `sales_quotes` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
