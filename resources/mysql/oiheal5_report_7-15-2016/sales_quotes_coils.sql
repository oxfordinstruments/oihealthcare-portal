-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.30 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.sales_quotes_coils
DROP TABLE IF EXISTS `sales_quotes_coils`;
CREATE TABLE IF NOT EXISTS `sales_quotes_coils` (
  `coil_id` int(11) DEFAULT NULL,
  `sales_quotes_id` int(11) DEFAULT NULL,
  KEY `FK_sales_quotes_coils_misc_coils` (`coil_id`),
  KEY `FK_sales_quotes_coils_sales_quotes` (`sales_quotes_id`),
  CONSTRAINT `FK_sales_quotes_coils_misc_coils` FOREIGN KEY (`coil_id`) REFERENCES `misc_coils` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_sales_quotes_coils_sales_quotes` FOREIGN KEY (`sales_quotes_id`) REFERENCES `sales_quotes` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table oiheal5_report.sales_quotes_coils: ~44 rows (approximately)
/*!40000 ALTER TABLE `sales_quotes_coils` DISABLE KEYS */;
INSERT INTO `sales_quotes_coils` (`coil_id`, `sales_quotes_id`) VALUES
	(13, 1004),
	(19, 1006),
	(67, 1006),
	(32, 1006),
	(13, 1006),
	(9, 1006),
	(19, 1007),
	(67, 1007),
	(32, 1007),
	(13, 1007),
	(9, 1007),
	(19, 1008),
	(67, 1008),
	(32, 1008),
	(13, 1008),
	(9, 1008),
	(19, 1009),
	(67, 1009),
	(32, 1009),
	(13, 1009),
	(9, 1009),
	(19, 1010),
	(67, 1010),
	(32, 1010),
	(13, 1010),
	(9, 1010),
	(19, 1011),
	(67, 1011),
	(32, 1011),
	(13, 1011),
	(9, 1011),
	(19, 1012),
	(67, 1012),
	(32, 1012),
	(13, 1012),
	(9, 1012),
	(19, 1013),
	(67, 1013),
	(32, 1013),
	(13, 1013),
	(9, 1013),
	(19, 1014),
	(67, 1014),
	(32, 1014),
	(13, 1014),
	(9, 1014);
/*!40000 ALTER TABLE `sales_quotes_coils` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
