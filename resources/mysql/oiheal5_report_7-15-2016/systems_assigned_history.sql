-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.30 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.systems_assigned_history
DROP TABLE IF EXISTS `systems_assigned_history`;
CREATE TABLE IF NOT EXISTS `systems_assigned_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_sent` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table oiheal5_report.systems_assigned_history: ~10 rows (approximately)
/*!40000 ALTER TABLE `systems_assigned_history` DISABLE KEYS */;
INSERT INTO `systems_assigned_history` (`id`, `filename`, `date`, `email_sent`) VALUES
	(72, 'asgn_2016-06-29T19-50_72.xml', '2016-06-29T19:50:01Z', 'Y'),
	(73, 'asgn_2016-06-30T13-50_73.xml', '2016-06-30T13:50:01Z', 'Y'),
	(74, 'asgn_2016-06-30T16-50_74.xml', '2016-06-30T16:50:02Z', 'Y'),
	(75, 'asgn_2016-06-30T21-50_75.xml', '2016-06-30T21:50:02Z', 'Y'),
	(76, 'asgn_2016-07-01T16-50_76.xml', '2016-07-01T16:50:02Z', 'Y'),
	(77, 'asgn_2016-07-11T21-50_77.xml', '2016-07-11T21:50:01Z', 'Y'),
	(78, 'asgn_2016-07-12T16-50_78.xml', '2016-07-12T16:50:01Z', 'Y'),
	(79, 'asgn_2016-07-14T16-50_79.xml', '2016-07-14T16:50:02Z', 'Y'),
	(80, 'asgn_2016-07-14T21-50_80.xml', '2016-07-14T21:50:02Z', 'Y'),
	(81, 'asgn_2016-07-15T13-50_81.xml', '2016-07-15T13:50:02Z', 'N');
/*!40000 ALTER TABLE `systems_assigned_history` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
