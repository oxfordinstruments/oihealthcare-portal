-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.30 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.users_auto_logins
DROP TABLE IF EXISTS `users_auto_logins`;
CREATE TABLE IF NOT EXISTS `users_auto_logins` (
  `uid` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(300) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `session_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `domain` varchar(300) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `expires` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Colation set as utf8_general_ci';

-- Dumping data for table oiheal5_report.users_auto_logins: ~27 rows (approximately)
/*!40000 ALTER TABLE `users_auto_logins` DISABLE KEYS */;
INSERT INTO `users_auto_logins` (`uid`, `token`, `session_id`, `ip`, `domain`, `expires`) VALUES
	('billingsb', 'KsstdmmsIYGr25WaBW25EaPkn8', 'cd49e30f146a64beac7478e6ed83fe89', '50.254.158.169', '50-254-158-169-static.hfc.comcastbusiness.net', '1466657045'),
	('bringolfk', 'GDIFYrkam5OtaV7aWKhfrBjYkv', 'a95187a89a5984ee901fdb2f7648ac59', '50.254.158.169', '50-254-158-169-static.hfc.comcastbusiness.net', '1468541165'),
	('bringolfm', 'Pky6J58S18u88kZdkrLPvxzijE', 'b26ee2759f67224dab5bbf0340e5aa44', '50.128.152.157', 'c-50-128-152-157.hsd1.fl.comcast.net', '1468261592'),
	('bushr', 'w53kCTFer6X0UvPpLc2VzJoIvt', '0844d0a790c5c1b7440ace148fe9caa0', '107.77.213.8', 'mobile-107-77-213-8.mobile.att.net', '1467780787'),
	('clarkf', 'u2InUmOBZoK3kOP6sINB7vZChx', '6adf936945e754e66c5c10cbc5bd1b5c', '50.250.111.85', '50-250-111-85-static.hfc.comcastbusiness.net', '1467141535'),
	('coyled', '66ltQZtkGdEoHEow0mjIZhQSLl', '7c79e43de37be6c9c86d2f3e4cfd8a6f', '71.203.19.179', 'c-71-203-19-179.hsd1.fl.comcast.net', '1468442893'),
	('customer', 'M6p8xZz0wDsn4MzJX0rfSTLlPo', 'd080713c261e2756ba288e2d9d1e55c1', '69.27.58.13', 'ip-69-27-58-13.slm.blueriver.net', '1468456825'),
	('darwisht', 'sXQ5qFohi4mn5sMmMQS2SzgddW', '370f0484ef9c25e1b1aa3a6dba623377', '96.85.99.25', '96-85-99-25-static.hfc.comcastbusiness.net', '1468272396'),
	('davisj', 'SancX1x4rP5bYXI6w5lgUhsjpc', '7b8ab1ab7b13e9d71f884f5e7964f414', '69.27.58.13', 'ip-69-27-58-13.slm.blueriver.net', '1468629132'),
	('devaultm', '8S2qwdfq302sIdVQG5Q1ssEOCI', '5647e7604b96846adc1b1df4db6f4f8e', '72.185.23.13', '72-185-23-13.res.bhn.net', '1467260889'),
	('dixeyd', 'oP8wQ0Afs0h6P6MvI3wqzGvleQ', '382b7f28d98e00f99860aedce62c3ab0', '70.44.53.58', '70.44.53.58.res-cmts.flt2.ptd.net', '1468247591'),
	('flechaa', '4HNTaEaC0gJlrNyaaYJcaCkTcK', 'a030aefc897a5fe06abd0a8da18bf71a', '68.196.81.158', 'ool-44c4519e.dyn.optonline.net', '1468335791'),
	('frazierm', 'PWlNmUNZCitOJ7Se1Yt1JIg8Xh', 'c08f56c00b20ceac4d7d80a4c7a64bd7', '50.250.111.85', '50-250-111-85-static.hfc.comcastbusiness.net', '1468442275'),
	('hahnl', '4Af5wvV66JajUKQYBiTU2UGB7Y', 'e1812987ab7fd232f3586d72e1a6015b', '24.192.227.17', 'd192-24-17-227.try.wideopenwest.com', '1467042322'),
	('heeralalk', '8mcB2ZkFz0MKV5pBT0FtzigmLV', '4b00b7ff8177656914536143fb2482cd', '96.83.29.249', '96-83-29-249-static.hfc.comcastbusiness.net', '1468548377'),
	('hennessyl', 'OJ6FI2o5glo3mk5QTQPNPoJz5j', 'bebd98ac60ab1d7236af265cba0da1f8', '50.250.111.85', '50-250-111-85-static.hfc.comcastbusiness.net', '1468623975'),
	('jacksonb', '1SGPhiSmEhjerzBanisVcpfVo6', '4f1cef88a58f78795f47309e41868911', '96.85.99.25', '96-85-99-25-static.hfc.comcastbusiness.net', '1468627541'),
	('mardikianj', '4Rse4wtAoIHrmN2kb6PVzWddbY', '5bfe63155a012c30954db2d5300d75f4', '50.250.111.85', '50-250-111-85-static.hfc.comcastbusiness.net', '1468543920'),
	('penningtons', '5YHHLOdRULxC2oOHqDJGh42hUF', 'f8785353419c0fac26119e91b3a8ac2d', '96.85.99.25', '96-85-99-25-static.hfc.comcastbusiness.net', '1467322569'),
	('prattg', '16Uvtowqwrj3soCAwSOJYioOjQ', '2fc3112e8c72626cbf1d7f4b66ea0815', '96.85.99.25', '96-85-99-25-static.hfc.comcastbusiness.net', '1467950196'),
	('reiglej', 'Z8s6VkbRfRpLdPY74Wu0pxf4VW', 'b6b41de1b7227b713a14b8d929b97d12', '24.214.121.85', 'mail.weatherholtz.biz', '1467296829'),
	('serranoj', 'ouC0ASz5UL4ZNgheOlY2rmv5BI', 'd5b7bbee7b9d8c7318bca37411f716ba', '50.250.111.85', '50-250-111-85-static.hfc.comcastbusiness.net', '1467328639'),
	('simmonss', 'Zcu6cTvIazifa1yu7XViRmGQuQ', 'fcabb9e0a12bd7cdb7c8018434086ad3', '50.250.111.85', '50-250-111-85-static.hfc.comcastbusiness.net', '1467733486'),
	('somarribaj', 'R4dj3Nxrjjqoz2GlQuH8ZAgiTI', 'cf950861d7ba1fca83eb6184e75d2cac', '50.250.111.85', '50-250-111-85-static.hfc.comcastbusiness.net', '1467925619'),
	('stancatod', 'wCOUOX6SoRynE70Idy6vqoP13x', 'a683593d38c4d2a0c3f55f36943518f6', '209.155.149.82', '209.155.149.82', '1468541315'),
	('tejedap', 'wLRD0Ycbgnga2g1nAsN8vwOIGv', '72c0fdc51b7c96b3e29c4b9ea69bde7d', '108.71.120.187', 'adsl-108-71-120-187.dsl.rcsntx.sbcglobal.net', '1467922770'),
	('varnerd ', 'DzqbpSEalujV4k5H4LgHVNsZcu', '867886a61aa93267835661a707fb6333', '70.195.65.152', '152.sub-70-195-65.myvzw.com', '1468617486');
/*!40000 ALTER TABLE `users_auto_logins` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
