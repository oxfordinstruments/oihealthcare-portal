-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.30 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.users_group_roles
DROP TABLE IF EXISTS `users_group_roles`;
CREATE TABLE IF NOT EXISTS `users_group_roles` (
  `gid` int(11) DEFAULT NULL,
  `rid` int(11) DEFAULT NULL,
  KEY `FK_users_group_roles_users_group_id` (`gid`),
  KEY `FK_users_group_roles_users_role_id` (`rid`),
  CONSTRAINT `FK_users_group_roles_users_group_id` FOREIGN KEY (`gid`) REFERENCES `users_group_id` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_users_group_roles_users_role_id` FOREIGN KEY (`rid`) REFERENCES `users_role_id` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table oiheal5_report.users_group_roles: ~11 rows (approximately)
/*!40000 ALTER TABLE `users_group_roles` DISABLE KEYS */;
INSERT INTO `users_group_roles` (`gid`, `rid`) VALUES
	(1, 9),
	(1, 5),
	(1, 2),
	(1, 1),
	(1, 3),
	(1, 4),
	(1, 6),
	(1, 7),
	(1, 8),
	(3, 20),
	(2, 10);
/*!40000 ALTER TABLE `users_group_roles` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
