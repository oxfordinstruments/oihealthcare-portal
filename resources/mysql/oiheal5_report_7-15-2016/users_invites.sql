-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.30 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.users_invites
DROP TABLE IF EXISTS `users_invites`;
CREATE TABLE IF NOT EXISTS `users_invites` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `date` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'creation date',
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'email of invited',
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'name of invited',
  `code` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'reg code',
  `expire` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'code expire',
  `sent_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user who sent the invite',
  PRIMARY KEY (`id`),
  KEY `sent_by` (`sent_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='invitation email for access to the portal';

-- Dumping data for table oiheal5_report.users_invites: ~0 rows (approximately)
/*!40000 ALTER TABLE `users_invites` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_invites` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
