-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.30 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.users_pref_id
DROP TABLE IF EXISTS `users_pref_id`;
CREATE TABLE IF NOT EXISTS `users_pref_id` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `pref` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='Edit with care';

-- Dumping data for table oiheal5_report.users_pref_id: ~9 rows (approximately)
/*!40000 ALTER TABLE `users_pref_id` DISABLE KEYS */;
INSERT INTO `users_pref_id` (`id`, `name`, `pref`) VALUES
	(1, 'Rcv Status Updates', 'pref_rcv_status_updates'),
	(2, 'Rcv Service Report', 'pref_rcv_service_reports'),
	(3, 'Rcv Service Request', 'pref_rcv_service_requests'),
	(4, 'Rcv Valuation Request', 'pref_rcv_valuation_request'),
	(5, 'Rcv Invoice Request', 'pref_rcv_invoice_request'),
	(6, 'Rcv Assignment Change', 'pref_rcv_assignment_change'),
	(7, 'Rcv ParCarFbc Submission', 'pref_rcv_parcarfbc_submit'),
	(8, 'Rcv SMS', 'pref_rcv_sms'),
	(9, 'Show Help', 'pref_show_help'),
	(10, 'Rcv Systems Down', 'pref_rcv_sysdown'),
	(11, 'Rcv Future Systems Move', 'pref_rcv_futsysmov');
/*!40000 ALTER TABLE `users_pref_id` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
