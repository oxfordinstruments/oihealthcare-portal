-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.31 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.iso_car_files
DROP TABLE IF EXISTS `iso_car_files`;
CREATE TABLE IF NOT EXISTS `iso_car_files` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `unique_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_location` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_size` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'user who added file',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `FK_reports_files_requests` (`unique_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='files linked to each car';

-- Dumping data for table oiheal5_report.iso_car_files: ~26 rows (approximately)
/*!40000 ALTER TABLE `iso_car_files` DISABLE KEYS */;
INSERT INTO `iso_car_files` (`id`, `unique_id`, `file_location`, `file_name`, `file_size`, `uid`) VALUES
	(1, '0cf4c68ef86ac38358b4800fe8ee3baf', '/iso_car_files/0cf4c68ef86ac38358b4800fe8ee3baf/files/', 'OxfordAuditReport2015.pdf', '437824', 'mardikianj'),
	(2, '0cf4c68ef86ac38358b4800fe8ee3baf', '/iso_car_files/0cf4c68ef86ac38358b4800fe8ee3baf/files/', 'Siemens Customer Complaint Action .pdf', '48887', 'mardikianj'),
	(3, 'bd9873b0e68e8a67774743467e1c1fc4', '/iso_car_files/bd9873b0e68e8a67774743467e1c1fc4/files/', 'Internal Audit 5-20-2015 vacaville.doc', '1001472', 'mardikianj'),
	(5, 'ea9aa763a03b28e90b8e69684bab17fb', '/iso_car_files/ea9aa763a03b28e90b8e69684bab17fb/files/', 'WorkZone_Recent_Activity_20150702_2127(1).pdf', '89179', 'clarkf'),
	(6, '0cf4c68ef86ac38358b4800fe8ee3baf', '/iso_car_files/0cf4c68ef86ac38358b4800fe8ee3baf/files/', 'OxfordSCARForm1.pdf', '91436', 'mardikianj'),
	(7, '103b592306f2113e6faa132c971a72c6', '/iso_car_files/103b592306f2113e6faa132c971a72c6/files/', 'CMS564cb826125e4.mp3', '860976', 'mardikianj'),
	(8, '103b592306f2113e6faa132c971a72c6', '/iso_car_files/103b592306f2113e6faa132c971a72c6/files/', 'FW CMS Voice Recording.msg', '931840', 'mardikianj'),
	(9, '103b592306f2113e6faa132c971a72c6', '/iso_car_files/103b592306f2113e6faa132c971a72c6/files/', 'FW_ [CMS] Voice Recording.pdf', '1543069', 'mardikianj'),
	(10, '103b592306f2113e6faa132c971a72c6', '/iso_car_files/103b592306f2113e6faa132c971a72c6/files/', 'Incident report 11-17.pdf', '809211', 'mardikianj'),
	(11, '103b592306f2113e6faa132c971a72c6', '/iso_car_files/103b592306f2113e6faa132c971a72c6/files/', 'Path Medical 11-18-15 incident.pdf', '64265', 'mardikianj'),
	(13, '622ae9d4f428b03ffcc8363f72894038', '/iso_car_files/622ae9d4f428b03ffcc8363f72894038/files/', 'RE_ Corrective action.pdf', '643015', 'mardikianj'),
	(14, '3758b4b4bfa38f5e31bc901191df307e', '/iso_car_files/3758b4b4bfa38f5e31bc901191df307e/files/', 'FW_ Deerfield Audit Findings & Questions.pdf', '793700', 'mardikianj'),
	(15, 'f90cacf191c006ee992ad705db8c267b', '/iso_car_files/f90cacf191c006ee992ad705db8c267b/files/', '7183 Relay Instructions and Scripting 5-31-16.docx', '32123', 'hennessyl'),
	(16, 'b8ffe4c00b0ccfaa63ebc1c0dc6179f6', '/iso_car_files/b8ffe4c00b0ccfaa63ebc1c0dc6179f6/files/', 'NC Violation Response.pdf', '30257', 'mardikianj'),
	(17, 'b8ffe4c00b0ccfaa63ebc1c0dc6179f6', '/iso_car_files/b8ffe4c00b0ccfaa63ebc1c0dc6179f6/files/', 'NC Violation.pdf', '184516', 'mardikianj'),
	(18, '7d4679fedc4089a001d1f328fbecc201', '/iso_car_files/7d4679fedc4089a001d1f328fbecc201/files/', 'NC Violation Response.pdf', '30257', 'mardikianj'),
	(19, '7d4679fedc4089a001d1f328fbecc201', '/iso_car_files/7d4679fedc4089a001d1f328fbecc201/files/', 'NC Violation.pdf', '184516', 'mardikianj'),
	(20, '7d4679fedc4089a001d1f328fbecc201', '/iso_car_files/7d4679fedc4089a001d1f328fbecc201/files/', 'Carolina East.Executed.Agreement.3.17.16 (1).pdf', '538577', 'mardikianj'),
	(21, 'd688b59808e9b46e7411f3df13ffe0c0', '/iso_car_files/d688b59808e9b46e7411f3df13ffe0c0/files/', 'RE_ CAR 1059 Independant Contractor reports.pdf', '658976', 'mardikianj'),
	(22, '317299bd7efb1da0b431ad11b473e72b', '/iso_car_files/317299bd7efb1da0b431ad11b473e72b/files/', 'F71-01 Nuclear Med Risk Analysis.xlsx', '103739', 'hennessyl'),
	(23, '317299bd7efb1da0b431ad11b473e72b', '/iso_car_files/317299bd7efb1da0b431ad11b473e72b/files/', 'F71-01 PET CT Risk Analysis.xlsx', '102902', 'hennessyl'),
	(24, '317299bd7efb1da0b431ad11b473e72b', '/iso_car_files/317299bd7efb1da0b431ad11b473e72b/files/', 'F71-01 Trailer Risk Analysis.xlsx', '50994', 'hennessyl'),
	(25, 'dde1ad7359c52283ecf7db6c49d533d4', '/iso_car_files/dde1ad7359c52283ecf7db6c49d533d4/files/', 'RE  New Corrective Action Submitted.msg', '150016', 'hennessyl'),
	(26, 'cefd5bb7a4ff892dc20cb75a5e12ed69', '/iso_car_files/cefd5bb7a4ff892dc20cb75a5e12ed69/files/', 'After Hours Procedure.docx', '47250', 'hennessyl'),
	(27, 'c1587ee2668cf91b39a78e2c1c41c760', '/iso_car_files/c1587ee2668cf91b39a78e2c1c41c760/files/', 'F63-18 Calibrated Equipment Sign Out Record Rev. A.xlsx', '38454', 'hennessyl'),
	(28, 'e16310470b2ee74e44acc8ac3ff783cd', '/iso_car_files/e16310470b2ee74e44acc8ac3ff783cd/files/', 'RE_ CAR 1083 (buffer in magnet).pdf', '833960', 'mardikianj');
/*!40000 ALTER TABLE `iso_car_files` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
