-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.31 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.misc_coils
DROP TABLE IF EXISTS `misc_coils`;
CREATE TABLE IF NOT EXISTS `misc_coils` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modality` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_misc_coils_misc_modalities` (`modality`),
  CONSTRAINT `FK_misc_coils_misc_modalities` FOREIGN KEY (`modality`) REFERENCES `misc_modalities` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table oiheal5_report.misc_coils: ~67 rows (approximately)
/*!40000 ALTER TABLE `misc_coils` DISABLE KEYS */;
INSERT INTO `misc_coils` (`id`, `name`, `notes`, `modality`) VALUES
	(1, 'HD Head Neck and Spine (forward production systems)', 'Connector: 2 HD (1 16 ch and 1 8 ch)  System:     HDx  ', 2),
	(2, 'HD Head Neck and Spine (upgrade systems)', 'Connector: 2 HD and 1 Legacy (Bendix)  System:     HDx  ', 2),
	(3, 'HD 8 ch NV', 'Connector: HD  System:   HD  HDx  ', 2),
	(4, 'HD 8 ch NV Array', 'Connector: HD  System:     HDx  ', 2),
	(5, 'HD 4 ch NV Array', 'Connector: HD  System:       ', 2),
	(6, '8CH NV', 'Connector: Legacy (Bendix)  System: Excite II  HD  HDx  ', 2),
	(7, '4CH NV', 'Connector: Legacy (Bendix)  System: Excite II  HD  HDx  ', 2),
	(8, 'HD 8 ch HiRes Brain', 'Connector: HD  System:     HDx  ', 2),
	(9, '8CH Brain', 'Connector: Legacy (Bendix)  System: Excite II  HD  HDx  ', 2),
	(10, '1.5T Quad NV Cable ID', 'Connector: Legacy (Bendix)  System:       ', 2),
	(11, 'HD 8 ch CTL Array', 'Connector: HD  System:     HDx  ', 2),
	(12, 'HD 4 ch CTL', 'Connector: HD  System:       ', 2),
	(13, '8 ch CTL', 'Connector: Legacy (Bendix)  System: Excite II  HD  HDx  ', 2),
	(14, '4 ch CTL', 'Connector: Legacy (Bendix)  System: Excite II  HD  HDx  ', 2),
	(15, 'HD 8CH T/R Knee', 'Connector: HD  System:   HD  HDx  ', 2),
	(16, 'HD Quad Extremity (Knee/Foot)', 'Connector: HD  System:     HDx  ', 2),
	(17, 'Quad Extremity (Knee/Foot)', 'Connector: Legacy (Bendix)  System: Excite II  HD  HDx  ', 2),
	(18, 'HD 3 ch Shoulder', 'Connector: HD  System:     HDx  ', 2),
	(19, '3 ch Shoulder', 'Connector: Legacy (Bendix)  System: Excite II  HD  HDx  ', 2),
	(20, 'Shoulder', 'Connector: Legacy (Bendix)  System: Excite II  HD  HDx  ', 2),
	(21, 'Linear Shoulder', 'Connector: Legacy (Bendix)  System: Excite II  HD  HDx  ', 2),
	(22, 'HD 8 ch Wrist', 'Connector: HD  System:     HDx  ', 2),
	(23, '8 ch Wrist', 'Connector: Legacy (Bendix)  System: Excite II  HD  HDx  ', 2),
	(24, 'HD 4 ch Wrist Array', 'Connector: HD  System:       ', 2),
	(25, 'Hi-Res Wrist', 'Connector: Legacy (Bendix)  System: Excite II  HD  HDx  ', 2),
	(26, 'Small Extremity Wrist', 'Connector: HD  System:   HD  HDx  ', 2),
	(27, 'MAI Extremity Cable ID', 'Connector: Legacy (Bendix)  System:       ', 2),
	(28, 'MRID Extremity Cable ID', 'Connector: Legacy (Bendix)  System:       ', 2),
	(29, 'Linear Extremity Cable ID', 'Connector: Legacy (Bendix)  System:       ', 2),
	(30, 'HD 8 ch VIBRANT Breast', 'Connector: HD  System:   HD  HDx  ', 2),
	(31, 'HD 4 ch Open Breast Array', 'Connector: HD  System:       ', 2),
	(32, '7CH Breast', 'Connector: Legacy (Bendix)  System: Excite II  HD  HDx  ', 2),
	(33, '4CH Breast', 'Connector: Legacy (Bendix)  System: Excite II  HD  HDx  ', 2),
	(34, 'HD 12 ch Body (forward production systems, single connector)', 'Connector: 1 HD  System:     HDx  ', 2),
	(35, 'HD 12 ch Body (upgrade systems, dual connectors)', 'Connector: 2 HD  System:     HDx  ', 2),
	(36, 'HD 8 ch Body', 'Connector: HD  System:     HDx  ', 2),
	(37, 'HD 4 ch Torso Array', 'Connector: HD  System:       ', 2),
	(38, '8CH Body', 'Connector: Legacy (Bendix)  System: Excite II  HD  HDx  ', 2),
	(39, '4CH Torso', 'Connector: Legacy (Bendix)  System: Excite II  HD  HDx  ', 2),
	(40, 'Torso-Pelvic', 'Connector: Legacy (Bendix)  System: Excite II  HD  HDx  ', 2),
	(41, 'Auto Tuning Device', 'Connector: Legacy (Bendix)  System: Excite II  HD  HDx  ', 2),
	(42, 'Endorectal Interface Device (8ch Body)', 'Connector: HD  System:     HDx  ', 2),
	(43, 'Endorectal Interface Device (12ch Body)', 'Connector: HD  System:     HDx  ', 2),
	(44, 'Disposable Endorectal-Prostate', '', 2),
	(45, 'Disposable Endorectal-Cervix', '', 2),
	(46, 'Disposable Endorectal-Colon', '', 2),
	(47, 'Breast Immobilization and Biopsy Positioning Device', '', 2),
	(48, 'Adjustable Headrest', '', 2),
	(49, 'HD Dual Array Package', 'Connector: HD  System:     HDx  ', 2),
	(50, 'HD Dual TMJ Package', 'Connector: HD  System:     HDx  ', 2),
	(51, 'HD Dual Array Adapter', 'Connector: HD  System:     HDx  ', 2),
	(52, 'Dual Array Package', 'Connector: Legacy (Bendix)  System: Excite II  HD  HDx  ', 2),
	(53, 'Dual TMJ Package', 'Connector: Legacy (Bendix)  System: Excite II  HD  HDx  ', 2),
	(54, 'Dual Array Adapter', 'Connector: Legacy (Bendix)  System: Excite II  HD  HDx  ', 2),
	(55, '3 Round Coil', 'Connector:   System: Excite II  HD  HDx  ', 2),
	(56, '5 Round Coil', 'Connector:   System: Excite II  HD  HDx  ', 2),
	(57, 'General Purpose Flex', 'Connector:   System: Excite II  HD  HDx  ', 2),
	(58, 'Dual Surface Coil Acquisition Positioning Accessory (46-282764G1)', 'Connector: Legacy (Bendix)  System:       ', 2),
	(59, 'Coil ID Cables (ALL)', '', 2),
	(60, 'HD 16CH Lower Leg (single connector)', 'Connector: HD  System:     HDx  ', 2),
	(61, 'HD 16CH Lower Leg (dual connector)', 'Connector: HD  System:   HD  HDx  ', 2),
	(62, 'PV Array CRM', 'Connector: Legacy (Bendix)  System: Excite II  HD  HDx  ', 2),
	(63, 'PV Array BRM', 'Connector: Legacy (Bendix)  System: Excite II  HD  HDx  ', 2),
	(64, 'HD 8CH Cardiac', 'Connector: HD  System:     HDx  ', 2),
	(65, 'HD 4 ch Cardiac', 'Connector: HD  System:       ', 2),
	(66, '8CH Cardiac', 'Connector: Legacy (Bendix)  System: Excite II  HD  HDx  ', 2),
	(67, '4CH Cardiac', 'Connector: Legacy (Bendix)  System: Excite II  HD  HDx  ', 2);
/*!40000 ALTER TABLE `misc_coils` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
