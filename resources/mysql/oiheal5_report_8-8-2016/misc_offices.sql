-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.31 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.misc_offices
DROP TABLE IF EXISTS `misc_offices`;
CREATE TABLE IF NOT EXISTS `misc_offices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `office` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `zip` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `lat` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `lng` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table oiheal5_report.misc_offices: ~3 rows (approximately)
/*!40000 ALTER TABLE `misc_offices` DISABLE KEYS */;
INSERT INTO `misc_offices` (`id`, `active`, `office`, `address`, `city`, `state`, `zip`, `lat`, `lng`) VALUES
	(1, 'Y', 'Deerfield Beach', '1027 SW 30th Ave', 'Deerfield Beach', 'FL', '33442', '26.303585', '-80.144656'),
	(2, 'Y', 'Vacaville', '64 Union Way', 'Vacaville', 'CA', '95687', '38.352421', '-121.938213'),
	(3, 'Y', 'Ann Arbor', '120 Enterprise Dr. ', 'Ann Arbor', 'MI', '48103', '42.2892276', '-83.8360514');
/*!40000 ALTER TABLE `misc_offices` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
