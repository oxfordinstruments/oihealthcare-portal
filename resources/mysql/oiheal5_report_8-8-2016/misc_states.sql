-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.31 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.misc_states
DROP TABLE IF EXISTS `misc_states`;
CREATE TABLE IF NOT EXISTS `misc_states` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` char(100) COLLATE utf8_unicode_ci NOT NULL,
  `abv` char(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `abv` (`abv`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='list of state abrev and names';

-- Dumping data for table oiheal5_report.misc_states: ~98 rows (approximately)
/*!40000 ALTER TABLE `misc_states` DISABLE KEYS */;
INSERT INTO `misc_states` (`id`, `name`, `abv`) VALUES
	(1, 'Alaska', 'AK'),
	(2, 'Alabama', 'AL'),
	(3, 'Arkansas', 'AR'),
	(4, 'Arizona', 'AZ'),
	(5, 'California', 'CA'),
	(6, 'Colorado', 'CO'),
	(7, 'Connecticut', 'CT'),
	(8, 'District of Columbia', 'DC'),
	(9, 'Delaware', 'DE'),
	(10, 'Florida', 'FL'),
	(11, 'Georgia', 'GA'),
	(12, 'Hawaii', 'HI'),
	(13, 'Iowa', 'IA'),
	(14, 'Idaho', 'ID'),
	(15, 'Illinois', 'IL'),
	(16, 'Indiana', 'IN'),
	(17, 'Kansas', 'KS'),
	(18, 'Kentucky', 'KY'),
	(19, 'Louisiana', 'LA'),
	(20, 'Massachusetts', 'MA'),
	(21, 'Maryland', 'MD'),
	(22, 'Maine', 'ME'),
	(23, 'Michigan', 'MI'),
	(24, 'Minnesota', 'MN'),
	(25, 'Missouri', 'MO'),
	(26, 'Mississippi', 'MS'),
	(27, 'Montana', 'MT'),
	(28, 'North Carolina', 'NC'),
	(29, 'North Dakota', 'ND'),
	(30, 'Nebraska', 'NE'),
	(31, 'New Hampshire', 'NH'),
	(32, 'New Jersey', 'NJ'),
	(33, 'New Mexico', 'NM'),
	(34, 'Nevada', 'NV'),
	(35, 'New York', 'NY'),
	(36, 'Ohio', 'OH'),
	(37, 'Oklahoma', 'OK'),
	(38, 'Oregon', 'OR'),
	(39, 'Pennsylvania', 'PA'),
	(40, 'Rhode Island', 'RI'),
	(41, 'South Carolina', 'SC'),
	(42, 'South Dakota', 'SD'),
	(43, 'Tennessee', 'TN'),
	(44, 'Texas', 'TX'),
	(45, 'Utah', 'UT'),
	(46, 'Virginia', 'VA'),
	(47, 'Vermont', 'VT'),
	(48, 'Washington', 'WA'),
	(49, 'Wisconsin', 'WI'),
	(50, 'West Virginia', 'WV'),
	(51, 'Wyoming', 'WY'),
	(52, 'Aguascalientes - Mexico', 'AGS-MX'),
	(53, 'Baja California Norte - Mexico', 'BCN-MX'),
	(54, 'Baja California Sur - Mexico', 'BCS-MX'),
	(55, 'Campeche - Mexico', 'CAM-MX'),
	(56, 'Chiapas - Mexico', 'CHIS-MX'),
	(57, 'Chihuahua - Mexico', 'CHIH-MX'),
	(58, 'Coahuila - Mexico', 'COAH-MX'),
	(59, 'Colima - Mexico', 'COL-MX'),
	(60, 'Distrito Federal - Mexico', 'DF-MX'),
	(61, 'Durango - Mexico', 'DGO-MX'),
	(62, 'Guanajuato - Mexico', 'GTO-MX'),
	(63, 'Guerrero - Mexico', 'GRO-MX'),
	(64, 'Hidalgo - Mexico', 'HGO-MX'),
	(65, 'Jalisco - Mexico', 'JAL-MX'),
	(66, 'Mexico (Estado de) - Mexico', 'MEX-MX'),
	(67, 'Michoacan - Mexico', 'MICH-MX'),
	(68, 'Morelos - Mexico', 'MOR-MX'),
	(69, 'Nayarit - Mexico', 'NAY-MX'),
	(70, 'Nuevo Leon - Mexico', 'NL-MX'),
	(71, 'Oaxaca - Mexico', 'OAX-MX'),
	(72, 'Puebla - Mexico', 'PUE-MX'),
	(73, 'Queretaro - Mexico', 'QRO-MX'),
	(74, 'Quintana Roo - Mexico', 'QROO-MX'),
	(75, 'San Luis Potosi - Mexico', 'SLP-MX'),
	(76, 'Sinaloa - Mexico', 'SIN-MX'),
	(77, 'Sonora - Mexico', 'SON-MX'),
	(78, 'Tabasco - Mexico', 'TAB-MX'),
	(79, 'Tamaulipas - Mexico', 'TAMPS-MX'),
	(80, 'Tlaxcala - Mexico', 'TLAX-MX'),
	(81, 'Veracruz - Mexico', 'VER-MX'),
	(82, 'Yucatan - Mexico', 'YUC-MX'),
	(83, 'Zacatecas - Mexico', 'ZAC-MX'),
	(84, 'Alberta - Canada', 'AB-CAN'),
	(85, 'British Columbia - Canada', 'BC-CAN'),
	(86, 'Manitoba - Canada', 'MB-CAN'),
	(87, 'New Brunswick - Canada', 'NB-CAN'),
	(88, 'Newfoundland and Labrador - Canada', 'NL-CAN'),
	(89, 'Northwest Territories - Canada', 'NT-CAN'),
	(90, 'Nova Scotia - Canada', 'NS-CAN'),
	(91, 'Nunavut - Canada', 'NU-CAN'),
	(92, 'Ontario - Canada', 'ON-CAN'),
	(93, 'Prince Edward Island - Canada', 'PE-CAN'),
	(94, 'Quebec - Canada', 'QC-CAN'),
	(95, 'Saskatchewan - Canada', 'SK-CAN'),
	(96, 'Yukon - Canada', 'YT-CAN'),
	(97, 'Tamil Nadu - India', 'TN-IND'),
	(98, 'Oxfordshire', 'OXON-UK');
/*!40000 ALTER TABLE `misc_states` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
