-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.31 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.misc_tables
DROP TABLE IF EXISTS `misc_tables`;
CREATE TABLE IF NOT EXISTS `misc_tables` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modality` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_misc_tables_misc_modalities` (`modality`),
  CONSTRAINT `FK_misc_tables_misc_modalities` FOREIGN KEY (`modality`) REFERENCES `misc_modalities` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table oiheal5_report.misc_tables: ~11 rows (approximately)
/*!40000 ALTER TABLE `misc_tables` DISABLE KEYS */;
INSERT INTO `misc_tables` (`id`, `name`, `notes`, `modality`) VALUES
	(1, 'YMS', NULL, 1),
	(2, 'NP', NULL, 1),
	(3, 'Zeus', 'Metalles Cradle', 1),
	(4, 'GT 1700', 'VCT', 1),
	(5, 'GT 2000', 'VCT', 1),
	(6, 'H-Power', 'HP Gantry', 1),
	(7, 'H2-HCC', NULL, 1),
	(8, 'H2-LCC', NULL, 1),
	(9, 'Bravio', NULL, 1),
	(10, 'Optmia', NULL, 1),
	(20, 'MR Table', NULL, 2);
/*!40000 ALTER TABLE `misc_tables` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
