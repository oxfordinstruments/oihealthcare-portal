-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.31 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.remote_assignments
DROP TABLE IF EXISTS `remote_assignments`;
CREATE TABLE IF NOT EXISTS `remote_assignments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `system_unique_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `remote_serial` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'eth0 mac',
  `system_ip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'console ip',
  `system_port` varchar(50) COLLATE utf8_unicode_ci DEFAULT '22',
  `system_uid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'console login',
  `connection_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ssh or telnet',
  `edited_by` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `edited_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `site_unique_id` (`system_unique_id`),
  KEY `edited_by` (`edited_by`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table oiheal5_report.remote_assignments: ~11 rows (approximately)
/*!40000 ALTER TABLE `remote_assignments` DISABLE KEYS */;
INSERT INTO `remote_assignments` (`id`, `system_unique_id`, `remote_serial`, `system_ip`, `system_port`, `system_uid`, `connection_type`, `edited_by`, `edited_date`, `notes`) VALUES
	(1, 'e0f9b3581824842aab5c8ef4c7d85e96', 'b8:27:eb:15:ec:17', '192.168.14.254', '23', 'server', 'T', 'davisj', '2016-05-24T18:53:19Z', 'Test'),
	(2, '9fc6fd464af40d9a0ceb59918595cdd9', 'b8:27:eb:84:7d:6e', '192.168.12.211', '23', 'service', 'T', 'hennessyl', '2016-06-07T14:21:39Z', 'High Field Dixie GE MR 11x'),
	(8, 'bf6eb56537afa0716977c00268047668', 'b8:27:eb:84:7d:6e', '192.168.12.217', '22', 'service', 'S', 'bringolfk', '2016-06-03T19:36:15Z', 'High Field Dixie LS16'),
	(9, 'cb4d80718170819f780db86d1a9b42b9', 'b8:27:eb:84:7d:6e', '192.168.12.214', '22', 'service', 'S', 'davisj', '2016-07-08T14:08:55Z', 'High Field Shelbyville VCT64'),
	(10, '9efbfd8c67c92718d050327d31a06810', 'b8:27:eb:84:7d:6e', '192.168.12.208', '23', 'service', 'T', 'hennessyl', '2016-06-07T14:20:18Z', 'High Field Shelbyville GE MR 11x'),
	(17, '8cd3b5b5bdd0fc734b8c80fe33c88afa', 'b8:27:eb:a1:90:4f', '192.168.23.1', '23', 'service', 'T', 'hennessyl', '2016-06-10T15:11:08Z', '9x MRI'),
	(18, 'dde85f1c04e5c55fbcaa70ccb0ac1505', 'b8:27:eb:ca:3d:1b', '10.5.6.12', '22', 'service', 'S', 'hennessyl', '2016-05-05T18:41:47Z', 'OiRemote is connected to MR 2140'),
	(19, '85639f72c90c26f9ec1e62b27f95a579', 'b8:27:eb:ca:3d:1b', '10.5.6.50', '23', 'service', 'T', 'hennessyl', '2016-06-07T14:18:58Z', 'OiRemote connect to console hub'),
	(23, '5a88e9aaa78ad9e15e632c0d34f5047b', 'b8:27:eb:20:b2:80', '192.168.0.99', '22', 'service', 'S', 'hennessyl', '2015-11-02T20:15:38Z', 'GE 16x'),
	(31, '13d45de0d43a1d98871a5c8f0388c121', 'b8:27:eb:b1:28:cf', '10.10.10.55', '23', 'service', 'T', 'hennessyl', '2016-02-22T16:59:37Z', 'Changed oiremote setup ip to 12.12.12.1'),
	(43, 'e8e6a5da0fbc82f40b1061940322bd74', 'b8:27:eb:c2:4f:e5', '192.168.140.40', '23', 'service', 'T', 'davisj', '2016-04-01T16:26:00Z', 'OiRemote Setup DHCP. ');
/*!40000 ALTER TABLE `remote_assignments` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
