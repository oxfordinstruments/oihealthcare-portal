-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.31 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.remote_computers
DROP TABLE IF EXISTS `remote_computers`;
CREATE TABLE IF NOT EXISTS `remote_computers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vpn_mac` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vpn_ip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eth0_mac` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'serial number',
  `eth0_ip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eth0_gw` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eth0_netmask` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eth1_ip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uptime` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'In Seconds',
  `idletime` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'In Seconds',
  `geo_ip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `geo_lat` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `geo_long` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `geo_state` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `geo_country` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `geo_timezone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `version` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'update version',
  `reported_online` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `eth0_mac` (`eth0_mac`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='This table is populated from the remote computers';

-- Dumping data for table oiheal5_report.remote_computers: ~8 rows (approximately)
/*!40000 ALTER TABLE `remote_computers` DISABLE KEYS */;
INSERT INTO `remote_computers` (`id`, `vpn_mac`, `vpn_ip`, `eth0_mac`, `eth0_ip`, `eth0_gw`, `eth0_netmask`, `eth1_ip`, `uptime`, `idletime`, `geo_ip`, `geo_lat`, `geo_long`, `geo_state`, `geo_country`, `geo_timezone`, `version`, `reported_online`) VALUES
	(6, '00:ac:5c:d0:cb:27', '14.25.36.4', 'b8:27:eb:15:ec:17', '192.168.14.218', '192.168.14.254', '255.255.255.0', '10.10.10.1', '273359.81', '253572.41', '69.27.52.140', '38.6079', '-86.0694', 'IN', 'US', 'America/Indiana/Indianapolis', '0', '2015-06-12T23:05:02Z'),
	(7, '00:ac:c4:d9:1d:e2', '14.25.36.6', 'b8:27:eb:84:7d:6e', '192.168.12.221', '192.168.12.1', '255.255.255.0', '10.10.10.1', '19213191.57', '17603598.64', 'none', '', '', '', '', '', '0', '2016-08-09T01:05:01Z'),
	(8, 'no_setup', 'no_setup', 'b8:27:eb:41:88:e6', '192.168.14.237', '192.168.14.254', '255.255.255.0', '10.10.10.1', '88.01', '24.73', '69.27.52.140', '38.6079', '-86.0694', 'IN', 'US', 'America/Indiana/Indianapolis', '0', '2015-06-05T15:08:46Z'),
	(9, '00:ac:06:9b:12:60', '14.25.36.4', 'b8:27:eb:a1:90:4f', '192.168.23.65', '192.168.23.254', '255.255.255.0', '10.10.10.1', '3428.51', '2897.57', '50.193.241.85', '33.4804', '-82.0941', 'GA', 'US', 'America/New_York', '0', '2015-11-23T14:05:01Z'),
	(10, 'no_setup', 'no_setup', 'b8:27:eb:ca:3d:1b', '10.5.6.33', '10.5.6.2', '255.255.255.0', '10.10.10.1', '281710.59', '264558.04', 'none', '', '', '', '', '', '0', '2016-08-09T01:05:01Z'),
	(11, '00:ac:24:e2:45:55', '14.25.36.3', 'b8:27:eb:20:b2:80', '192.168.0.123', '192.168.0.1', '255.255.255.0', '10.10.10.1', '1664270.64', '1516203.10', '70.195.6.27', '38.8043', '-95.2371', 'KS', 'US', 'America/Chicago', '0', '2016-08-09T01:05:01Z'),
	(12, '00:ac:f6:f9:68:91', '14.25.36.4', 'b8:27:eb:b1:28:cf', '10.10.10.241', '10.10.10.1', '255.255.255.0', '12.12.12.1', '1690144.19', '1538789.19', '97.87.194.246', '38.8022', '-90.627', 'MO', 'US', 'America/Chicago', '0', '2016-08-09T01:05:01Z'),
	(13, '00:ac:78:61:8c:4c', '14.25.36.5', 'b8:27:eb:c2:4f:e5', '192.168.140.105', '192.168.140.254', '255.255.255.0', '10.10.10.1', '2240096.82', '2038716.04', 'none', '', '', '', '', '', '0', '2016-08-09T01:05:01Z');
/*!40000 ALTER TABLE `remote_computers` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
