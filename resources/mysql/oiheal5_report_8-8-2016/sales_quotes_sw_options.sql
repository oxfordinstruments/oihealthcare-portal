-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.31 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.sales_quotes_sw_options
DROP TABLE IF EXISTS `sales_quotes_sw_options`;
CREATE TABLE IF NOT EXISTS `sales_quotes_sw_options` (
  `option_id` int(11) DEFAULT NULL,
  `sales_quotes_id` int(11) DEFAULT NULL,
  KEY `FK_sales_quotes_sw_options_misc_sw_options` (`option_id`),
  KEY `FK_sales_quotes_sw_options_sales_quotes` (`sales_quotes_id`),
  CONSTRAINT `FK_sales_quotes_sw_options_misc_sw_options` FOREIGN KEY (`option_id`) REFERENCES `misc_sw_options` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_sales_quotes_sw_options_sales_quotes` FOREIGN KEY (`sales_quotes_id`) REFERENCES `sales_quotes` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table oiheal5_report.sales_quotes_sw_options: ~64 rows (approximately)
/*!40000 ALTER TABLE `sales_quotes_sw_options` DISABLE KEYS */;
INSERT INTO `sales_quotes_sw_options` (`option_id`, `sales_quotes_id`) VALUES
	(209, 1004),
	(30, 1000),
	(84, 1006),
	(92, 1006),
	(101, 1006),
	(111, 1006),
	(119, 1006),
	(209, 1006),
	(84, 1007),
	(92, 1007),
	(101, 1007),
	(111, 1007),
	(119, 1007),
	(209, 1007),
	(84, 1008),
	(92, 1008),
	(101, 1008),
	(111, 1008),
	(119, 1008),
	(209, 1008),
	(84, 1009),
	(92, 1009),
	(101, 1009),
	(111, 1009),
	(119, 1009),
	(209, 1009),
	(84, 1010),
	(92, 1010),
	(101, 1010),
	(111, 1010),
	(119, 1010),
	(209, 1010),
	(84, 1011),
	(92, 1011),
	(101, 1011),
	(111, 1011),
	(119, 1011),
	(209, 1011),
	(84, 1012),
	(92, 1012),
	(101, 1012),
	(111, 1012),
	(119, 1012),
	(209, 1012),
	(84, 1013),
	(87, 1013),
	(92, 1013),
	(95, 1013),
	(101, 1013),
	(111, 1013),
	(119, 1013),
	(198, 1013),
	(207, 1013),
	(209, 1013),
	(84, 1014),
	(87, 1014),
	(92, 1014),
	(95, 1014),
	(101, 1014),
	(111, 1014),
	(119, 1014),
	(198, 1014),
	(207, 1014),
	(209, 1014);
/*!40000 ALTER TABLE `sales_quotes_sw_options` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
