-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.31 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.systems_assigned_history
DROP TABLE IF EXISTS `systems_assigned_history`;
CREATE TABLE IF NOT EXISTS `systems_assigned_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_sent` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'N',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table oiheal5_report.systems_assigned_history: ~10 rows (approximately)
/*!40000 ALTER TABLE `systems_assigned_history` DISABLE KEYS */;
INSERT INTO `systems_assigned_history` (`id`, `filename`, `date`, `email_sent`) VALUES
	(79, 'asgn_2016-07-14T16-50_79.xml', '2016-07-14T16:50:02Z', 'Y'),
	(80, 'asgn_2016-07-14T21-50_80.xml', '2016-07-14T21:50:02Z', 'Y'),
	(81, 'asgn_2016-07-15T13-50_81.xml', '2016-07-15T13:50:02Z', 'Y'),
	(82, 'asgn_2016-07-19T16-50_82.xml', '2016-07-19T16:50:01Z', 'Y'),
	(83, 'asgn_2016-07-21T19-50_83.xml', '2016-07-21T19:50:01Z', 'Y'),
	(84, 'asgn_2016-07-29T16-50_84.xml', '2016-07-29T16:50:01Z', 'Y'),
	(85, 'asgn_2016-07-29T19-50_85.xml', '2016-07-29T19:50:01Z', 'Y'),
	(86, 'asgn_2016-08-02T19-50_86.xml', '2016-08-02T19:50:01Z', 'Y'),
	(87, 'asgn_2016-08-03T13-50_87.xml', '2016-08-03T13:50:01Z', 'Y'),
	(88, 'asgn_2016-08-08T19-50_88.xml', '2016-08-08T19:50:01Z', 'N');
/*!40000 ALTER TABLE `systems_assigned_history` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
