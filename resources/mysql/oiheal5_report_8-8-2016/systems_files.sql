-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.31 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.systems_files
DROP TABLE IF EXISTS `systems_files`;
CREATE TABLE IF NOT EXISTS `systems_files` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `unique_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_location` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_size` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'user who added file',
  PRIMARY KEY (`id`),
  KEY `unique_id` (`unique_id`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='files linked to each site';

-- Dumping data for table oiheal5_report.systems_files: ~6 rows (approximately)
/*!40000 ALTER TABLE `systems_files` DISABLE KEYS */;
INSERT INTO `systems_files` (`id`, `unique_id`, `file_location`, `file_name`, `file_size`, `uid`) VALUES
	(1, '9c27b2d0ae344e75caf4cd6ede074b39', '/systems_files/9c27b2d0ae344e75caf4cd6ede074b39/files/', '1258 ~ INFO.PDF', '1607882', 'corea'),
	(3, '9c27b2d0ae344e75caf4cd6ede074b39', '/systems_files/9c27b2d0ae344e75caf4cd6ede074b39/files/', '1258 ~ IMAGE.PDF', '1421434', 'corea'),
	(4, '9c27b2d0ae344e75caf4cd6ede074b39', '/systems_files/9c27b2d0ae344e75caf4cd6ede074b39/files/', '1258 ~ CHECKLIST.PDF', '1555885', 'corea'),
	(5, '71138435a5059849f7a2af13f3d738ca', '/systems_files/71138435a5059849f7a2af13f3d738ca/files/', 'DOC.pdf', '50638', 'billingsb'),
	(6, '223849351ef7513b8f90a5ac1d6e87b0', '/systems_files/223849351ef7513b8f90a5ac1d6e87b0/files/', 'Antelope Valley signed service agreement.pdf', '402094', 'bringolfk'),
	(7, 'c87a367ee7bff2bbbf9c306ff5f8d600', '/systems_files/c87a367ee7bff2bbbf9c306ff5f8d600/files/', 'Brilliance 64 CT ID-547488 Value (Silver).pdf', '905489', 'bringolfk');
/*!40000 ALTER TABLE `systems_files` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
