-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.31 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.systems_status
DROP TABLE IF EXISTS `systems_status`;
CREATE TABLE IF NOT EXISTS `systems_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Flag color',
  `priority` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='table of types of system status';

-- Dumping data for table oiheal5_report.systems_status: ~7 rows (approximately)
/*!40000 ALTER TABLE `systems_status` DISABLE KEYS */;
INSERT INTO `systems_status` (`id`, `status`, `flag`, `priority`) VALUES
	(1, 'System Up', 'green', 1),
	(2, 'System Down', 'red', 10),
	(3, 'System Up - With Limitations', 'yellow', 5),
	(4, 'Additional Support Needed', 'red', 8),
	(5, 'System Up - System Unavailable', 'yellow', 2),
	(16, 'System Down - System Unavailable', 'red', 9),
	(17, 'Unknown', 'yellow', 4);
/*!40000 ALTER TABLE `systems_status` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
