-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.31 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.trailers_files
DROP TABLE IF EXISTS `trailers_files`;
CREATE TABLE IF NOT EXISTS `trailers_files` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `unique_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_location` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_size` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'user who added file',
  PRIMARY KEY (`id`),
  KEY `unique_id` (`unique_id`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='files linked to each trailer';

-- Dumping data for table oiheal5_report.trailers_files: ~0 rows (approximately)
/*!40000 ALTER TABLE `trailers_files` DISABLE KEYS */;
/*!40000 ALTER TABLE `trailers_files` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
