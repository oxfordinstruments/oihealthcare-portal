-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.31 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.users_auto_logins
DROP TABLE IF EXISTS `users_auto_logins`;
CREATE TABLE IF NOT EXISTS `users_auto_logins` (
  `uid` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(300) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `session_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `domain` varchar(300) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `expires` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Colation set as utf8_general_ci';

-- Dumping data for table oiheal5_report.users_auto_logins: ~27 rows (approximately)
/*!40000 ALTER TABLE `users_auto_logins` DISABLE KEYS */;
INSERT INTO `users_auto_logins` (`uid`, `token`, `session_id`, `ip`, `domain`, `expires`) VALUES
	('billingsb', 'KsstdmmsIYGr25WaBW25EaPkn8', 'cd49e30f146a64beac7478e6ed83fe89', '50.254.158.169', '50-254-158-169-static.hfc.comcastbusiness.net', '1466657045'),
	('bringolfk', 'mVzDR7wG3VHaTakDhdovxoZuTY', '6d3b22113ac116fefed9b4bc09957cd7', '50.254.158.169', '50-254-158-169-static.hfc.comcastbusiness.net', '1470697096'),
	('bringolfm', 'Pky6J58S18u88kZdkrLPvxzijE', 'b26ee2759f67224dab5bbf0340e5aa44', '50.128.152.157', 'c-50-128-152-157.hsd1.fl.comcast.net', '1468261592'),
	('bushr', 'xW1CnAIhYyyM2UPoGQevlWSzBc', '25dc3fcfd1b6e4c2e56cc3ce2450c0b6', '107.77.211.113', 'mobile-107-77-211-113.mobile.att.net', '1470077274'),
	('caseyw', 'X80JzJ6Buh0ADdmNZvKdkfzyFP', '40af5357e504dbd21d1d1c0516c8283c', '69.136.242.139', 'c-69-136-242-139.hsd1.nj.comcast.net', '1470181566'),
	('clarkf', 'u2InUmOBZoK3kOP6sINB7vZChx', '6adf936945e754e66c5c10cbc5bd1b5c', '50.250.111.85', '50-250-111-85-static.hfc.comcastbusiness.net', '1467141535'),
	('corbittj', 'nU3Iv7VDOeXgCgBByqxN7SHAaA', '9f076f95752450ac65fc4fa179999c07', '50.250.111.85', '50-250-111-85-static.hfc.comcastbusiness.net', '1468869144'),
	('coyled', '66ltQZtkGdEoHEow0mjIZhQSLl', '7c79e43de37be6c9c86d2f3e4cfd8a6f', '71.203.19.179', 'c-71-203-19-179.hsd1.fl.comcast.net', '1468442893'),
	('customer', 'M6p8xZz0wDsn4MzJX0rfSTLlPo', 'd080713c261e2756ba288e2d9d1e55c1', '69.27.58.13', 'ip-69-27-58-13.slm.blueriver.net', '1468456825'),
	('darwisht', 'sXQ5qFohi4mn5sMmMQS2SzgddW', '370f0484ef9c25e1b1aa3a6dba623377', '96.85.99.25', '96-85-99-25-static.hfc.comcastbusiness.net', '1468272396'),
	('devaultm', 'uXyInsZIfGw0B3tT35e5xUkCNk', '87b790397ebc140c0debd98a4febe65f', '72.185.23.13', '72-185-23-13.res.bhn.net', '1470354530'),
	('dixeyd', '42LqV8S4jlie5kC6wEF0QPAE2F', '966521580936a6bcf4ff6235a54cad39', '70.193.136.223', '223.sub-70-193-136.myvzw.com', '1468871184'),
	('flechaa', 'qEc3w6D4sDFgGRSGqpJY8eEY2B', 'd4e064bdaaaf1313e37d06c8f79c181d', '72.205.128.52', '72.205.128.52', '1469751951'),
	('frazierm', 'bZqPXLYgSg2QX8yPbb4x8FJWO1', '46d30b91265f13be13aa0170d53cb5b4', '50.250.111.85', '50-250-111-85-static.hfc.comcastbusiness.net', '1470690449'),
	('hahnl', '4Af5wvV66JajUKQYBiTU2UGB7Y', 'e1812987ab7fd232f3586d72e1a6015b', '24.192.227.17', 'd192-24-17-227.try.wideopenwest.com', '1467042322'),
	('heeralalk', '8mcB2ZkFz0MKV5pBT0FtzigmLV', '4b00b7ff8177656914536143fb2482cd', '96.83.29.249', '96-83-29-249-static.hfc.comcastbusiness.net', '1468548377'),
	('hennessyl', '7nzn4KoKIS8EPV7EtclkjmZXS0', '5702bb7d0f4dfbe3a33a4115a2648c25', '50.250.111.85', '50-250-111-85-static.hfc.comcastbusiness.net', '1470181183'),
	('jacksonb', 'yvl6xS8j3qQTUHDmmpfQBFT2wp', '57fc8c62c0f36fb8f9f2373e7efcbc53', '96.85.99.25', '96-85-99-25-static.hfc.comcastbusiness.net', '1470697253'),
	('medfordg', 'iPyDqyq8ZMExt336GbMprfm4Hf', 'c397ca4aec6a0483a079e9c513446d77', '69.49.220.123', 'HSKL-DSL-69-49-220-123.srcaccess.net', '1469824502'),
	('penningtons', '5YHHLOdRULxC2oOHqDJGh42hUF', 'f8785353419c0fac26119e91b3a8ac2d', '96.85.99.25', '96-85-99-25-static.hfc.comcastbusiness.net', '1467322569'),
	('prattg', 'QTLPwEJFIh1GOoioiPOQL32BpA', '604f8db99a66e62001dbcebbe01861cc', '96.85.99.25', '96-85-99-25-static.hfc.comcastbusiness.net', '1469844284'),
	('reiglej', 'g6e1onIHVmTB2ySlWjHKs88Qej', 'fac258df3b50b1026fcff56d92a6954a', '70.193.139.72', '72.sub-70-193-139.myvzw.com', '1469741627'),
	('serranoj', 'ouC0ASz5UL4ZNgheOlY2rmv5BI', 'd5b7bbee7b9d8c7318bca37411f716ba', '50.250.111.85', '50-250-111-85-static.hfc.comcastbusiness.net', '1467328639'),
	('simmonss', 'Zcu6cTvIazifa1yu7XViRmGQuQ', 'fcabb9e0a12bd7cdb7c8018434086ad3', '50.250.111.85', '50-250-111-85-static.hfc.comcastbusiness.net', '1467733486'),
	('somarribaj', 'tdaB1teZJqAgu0NFe1DMJr3lpc', 'a6fac331b80d0406325b800f807545fe', '50.250.111.85', '50-250-111-85-static.hfc.comcastbusiness.net', '1469822703'),
	('stancatod', 'aXMy4o2xFulxkKT7IHlJHtaVNW', 'e0406316d1acdfa0479fd56577ba0270', '96.85.99.25', '96-85-99-25-static.hfc.comcastbusiness.net', '1470356277'),
	('thomsonc', 'vREXsgRchLCdqZyKGH5NV1HWp3', 'f72381a51e5feb7e638581a85b4c7661', '184.64.70.19', 'S0106602ad08f4c13.cg.shawcable.net', '1470686957');
/*!40000 ALTER TABLE `users_auto_logins` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
