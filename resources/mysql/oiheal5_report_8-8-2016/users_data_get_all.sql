-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.31 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for procedure oiheal5_report.users_data_get_all
DROP PROCEDURE IF EXISTS `users_data_get_all`;
DELIMITER //
CREATE DEFINER=`oiheal5_justin`@`69.27.58.13` PROCEDURE `users_data_get_all`()
    SQL SECURITY INVOKER
BEGIN
SET @@group_concat_max_len = 10000;
SET @sql = NULL;

SET @grp = NULL;
SELECT GROUP_CONCAT(DISTINCT CONCAT_WS('','MAX(IF(`group` = ''', `group`, ''', \'Y\', \'N\')) AS ', `group`)) INTO @grp from users_group_id;

SET @role = NULL;
SELECT GROUP_CONCAT(DISTINCT CONCAT_WS('','MAX(IF(role = ''', role, ''', \'Y\', \'N\')) AS ', role)) INTO @role from users_role_id;

SET @perm = NULL;
SELECT GROUP_CONCAT(DISTINCT CONCAT_WS('','MAX(IF(perm = ''', perm, ''', \'Y\', \'N\')) AS ', perm)) INTO @perm from users_perm_id;

SET @pref = NULL;
SELECT GROUP_CONCAT(DISTINCT CONCAT_WS('','MAX(IF(pref = ''', pref, ''', \'Y\', \'N\')) AS ', pref)) INTO @pref from users_pref_id;

SET @sql = CONCAT_WS('','SELECT u.*, rid.location, rid.role, ', @grp, ',' , @role, ',', @perm, ',', @pref, '
from users as u
left join users_groups as ug on ug.uid = u.uid
left join users_group_id as gid on ug.gid = gid.id
left join users_roles as ur on ur.uid = u.uid
left join users_role_id as rid on ur.rid = rid.id
left join users_perms as up on up.uid = u.uid
left join users_perm_id as pid on up.pid = pid.id
left join users_prefs as upr on upr.uid = u.uid
left join users_pref_id as prid on upr.pid = prid.id
group by u.uid');


PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

END//
DELIMITER ;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
