-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.31 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.users_qual_id
DROP TABLE IF EXISTS `users_qual_id`;
CREATE TABLE IF NOT EXISTS `users_qual_id` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qual` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `modality` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mfg` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `sequence` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table oiheal5_report.users_qual_id: ~12 rows (approximately)
/*!40000 ALTER TABLE `users_qual_id` DISABLE KEYS */;
INSERT INTO `users_qual_id` (`id`, `qual`, `modality`, `mfg`, `sequence`) VALUES
	(1, 'qual_ct_ge', 'CT', 'GE', 1.1),
	(2, 'qual_ct_siem', 'CT', 'Siemens', 1.2),
	(3, 'qual_ct_phil', 'CT', 'Philips', 1.3),
	(4, 'qual_mr_ge', 'MR', 'GE', 2.1),
	(5, 'qual_mr_siem', 'MR', 'Siemens', 2.2),
	(6, 'qual_mr_phil', 'MR', 'Philips', 2.3),
	(7, 'qual_nm_any', 'NM', 'ANY', 4.1),
	(8, 'qual_pct_gr', 'PCT', 'GE', 3.1),
	(9, 'qual_pct_any', 'PCT', 'ANY', 3.2),
	(10, 'qual_xr_any', 'XR', 'ANY', 5.1),
	(11, 'qual_us_any', 'US', 'ANY', 6.1),
	(100, 'qual_none', 'NONE', 'NONE', 0);
/*!40000 ALTER TABLE `users_qual_id` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
