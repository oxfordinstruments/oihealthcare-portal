-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.31 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.users_quals
DROP TABLE IF EXISTS `users_quals`;
CREATE TABLE IF NOT EXISTS `users_quals` (
  `uid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qid` int(11) DEFAULT NULL,
  KEY `uid` (`uid`),
  KEY `qual` (`qid`),
  CONSTRAINT `FK_users_quals_users` FOREIGN KEY (`uid`) REFERENCES `users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_users_quals_users_qual_id` FOREIGN KEY (`qid`) REFERENCES `users_qual_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table oiheal5_report.users_quals: ~52 rows (approximately)
/*!40000 ALTER TABLE `users_quals` DISABLE KEYS */;
INSERT INTO `users_quals` (`uid`, `qid`) VALUES
	('varnerd', 4),
	('hodgem', 4),
	('dirkesj', 5),
	('reiglej', 1),
	('reiglej', 4),
	('simmonss', 4),
	('stevensonk', 1),
	('galvackyd', 1),
	('shevackj', 4),
	('corec', 1),
	('corea', 1),
	('flechaa', 1),
	('tejedap', 1),
	('tejedap', 4),
	('schriverk', 1),
	('somarribaj', 100),
	('thrushr', 100),
	('grayg', 1),
	('moralesp', 4),
	('corbittj', 100),
	('sidhui', 100),
	('coyled', 1),
	('coyled', 4),
	('bushr', 1),
	('condesa', 4),
	('bringolfk', 1),
	('parrb', 5),
	('dixeyd', 4),
	('dixeyd', 5),
	('davisj', 1),
	('davisj', 4),
	('nbrooks@pi-stl.com', 100),
	('robert.hazzard@baycare.org', 100),
	('frazierm', 4),
	('heeralalk', 1),
	('heeralalk', 2),
	('heeralalk', 4),
	('heeralalk', 5),
	('starkj', 1),
	('starkj', 4),
	('ruhi.abbas@northhillsmedicalcenter.com', 100),
	('hennessyl', 100),
	('billingsc', 100),
	('pettitm', 4),
	('billingsb', 100),
	('tbanta@ecocala.com', 100),
	('ramirezj', 4),
	('ramirezj', 5),
	('ramirezj', 6),
	('caseyw', 100),
	('tokushigeg', 100),
	('parkerselfp', 100);
/*!40000 ALTER TABLE `users_quals` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
