-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.31 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.users_role_id
DROP TABLE IF EXISTS `users_role_id`;
CREATE TABLE IF NOT EXISTS `users_role_id` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `seq` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `column` (`role`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Edit with care';

-- Dumping data for table oiheal5_report.users_role_id: ~11 rows (approximately)
/*!40000 ALTER TABLE `users_role_id` DISABLE KEYS */;
INSERT INTO `users_role_id` (`id`, `name`, `role`, `location`, `image`, `description`, `seq`) VALUES
	(1, 'Engineer', 'role_engineer', '/report/engineer', '/resources/images/shine/galternatives.png', 'Engineering Dashboard', 1.1),
	(2, 'Dispatch', 'role_dispatch', '/report/dispatch', '/resources/images/shine/hydrogen.png', 'Dispatching Dashboard', 2.1),
	(3, 'Finance', 'role_finance', '/report/finance', '/resources/images/shine/emblem-money.png', 'Financial Dashboard', 4.1),
	(4, 'Manager', 'role_management', '/report/management', '/resources/images/shine/seahorse-preferences.png', 'Management Dashboard', 3.1),
	(5, 'Basic', 'role_basic', '/report/basic', '/resources/images/shine/basic.png', 'Basic Dashboard', 6.1),
	(6, 'Quality', 'role_quality', '/report/iso', '/resources/images/shine/new-edit-find-replace.png', 'Quality Management', 5.1),
	(7, 'Sales', 'role_sales', '/report/sales', '/resources/images/shine/sales-report.png', 'Sales Dashboard', 7.1),
	(8, 'Sales Manager', 'role_sales_manager', '/report/sales_manager', '/resources/images/shine/binary-tree.png', 'Sales Manager Dashboard', 8.1),
	(9, 'Admin', 'role_admin', '/report/admin', '/resources/images/shine/gnome-network-preferences.png', 'Administrator Dashboard', 50.1),
	(10, 'Contractor', 'role_contractor', '/report/contractor', '/resources/images/shine/galternatives.png', 'Contractor\'s Dashboard', 9.1),
	(20, 'Customer', 'role_customer', '/report/customer', '/resources/images/shine/new-edit-find-replace.png', 'Customer\'s Dashboard', 10.1);
/*!40000 ALTER TABLE `users_role_id` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
