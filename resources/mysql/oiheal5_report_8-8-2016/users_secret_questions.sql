-- --------------------------------------------------------
-- Host:                         oihealthcareportal.com
-- Server version:               5.6.31 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5020
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oiheal5_report.users_secret_questions
DROP TABLE IF EXISTS `users_secret_questions`;
CREATE TABLE IF NOT EXISTS `users_secret_questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Do not edit this table';

-- Dumping data for table oiheal5_report.users_secret_questions: ~14 rows (approximately)
/*!40000 ALTER TABLE `users_secret_questions` DISABLE KEYS */;
INSERT INTO `users_secret_questions` (`id`, `question`) VALUES
	(1, 'What\'s the name of your first school?'),
	(2, 'What\'s the name of your favorite pet?'),
	(3, 'What\'s the name of your favorite childhood cuddly toy?'),
	(4, 'Where did you travel on your honeymoon?'),
	(5, 'What was your childhood nickname?'),
	(6, 'In what city did you meet your spouse/significant other?'),
	(7, 'What was the name of your first stuffed animal?'),
	(8, 'In what city or town was your first job?'),
	(9, 'Where were you when you first heard about 9/11?'),
	(10, 'What is the name of the place your wedding reception was held?'),
	(11, 'What was your dream job as a child? '),
	(12, 'What is your favorite color?'),
	(14, 'What is your favorite book?'),
	(15, 'Who is your favorite actor/actress?');
/*!40000 ALTER TABLE `users_secret_questions` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
