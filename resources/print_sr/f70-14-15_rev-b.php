<?php


//============================================================+
// File name   : f70-14-15_rev-b.php
// Begin       : 09/24/2014
// Last Update : 08/21/2015
//
// Description : CT and MR Service Report PDF Rev A
//				 This prints out forms F70-14 Rev A and F70-15 Rev A
//				 Do not make any major changes without submitting to ISO at office
// 
//				 All units are in points  (72 per inch)
//               
//============================================================+

//check if script called directly or included
if(basename(__FILE__) != basename($_SERVER['SCRIPT_FILENAME'])){
	//included
	$included = true;
}else{
	//called directly
	require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');
	
	$included = false;
	$empty_report = false;
	$type_ct = true;
	
	if(isset($_GET['unique_id'])){
		$report_unique_id=$_GET['unique_id'];
	}else{
		if(isset($_GET['empty'])){
			$empty_report = true;
			if(!isset($_GET['type'])){
				//header("location:/error.php?n=1&t=Unknown Type&p=f70-14-15_rev-b.php");
				die('EMPTY type');
			}
			if(strtolower($_GET['type']) == 'ct'){
				$type_ct = true;
				$rowSystem['equipment_type'] = 'CT';
			}else{
				$type_ct = false;
				$rowSystem['equipment_type'] = 'MR';
			}
		}else{
			header("location:/error.php?n=1025&p=f70-14-15_rev-b.php");
		}
	}
}

$secondpage = false;
$showhours = false;
$showparts = false;
$showrepairact = false;
$tot_labor_reg=0.0;
$tot_labor_ot=0.0;
$tot_travel_reg=0.0;
$tot_travel_ot=0.0;
$numparts=0;
$numdays=0;

if(!$empty_report){
	$sql="SELECT r.*, u.name AS assigned_engineer_name, c.`type` AS contract_override_name, f.name AS facility_name, f.address, f.city, f.state, f.zip
	FROM systems_reports AS r
	LEFT JOIN users AS u ON r.assigned_engineer = u.uid
	LEFT JOIN misc_contracts AS c ON r.contract_override=c.id
	LEFT JOIN facilities AS f ON f.unique_id = r.facility_unique_id
	WHERE r.unique_id ='$report_unique_id';";
	$resultReport=$mysqli->query($sql);
	$rowReport=$resultReport->fetch_assoc();
}
if(!$empty_report){
	$sql="SELECT sb.*, sbc.*, e.`type` AS equipment_type, e.name as equipment_name, c.`type` AS contract_name
	FROM systems_base AS sb
	LEFT JOIN systems_base_cont AS sbc ON sb.unique_id = sbc.unique_id
	LEFT JOIN systems_types AS e ON sb.system_type = e.id
	LEFT JOIN misc_contracts AS c ON c.id = sbc.contract_type
	WHERE sb.unique_id = '".$rowReport['system_unique_id']."';";
	$resultSystem=$mysqli->query($sql);
	
	$rowSystem=$resultSystem->fetch_assoc();
	$resultHours=$mysqli->query("SELECT * FROM systems_hours WHERE unique_id='$report_unique_id';");
	$resultParts=$mysqli->query("SELECT * FROM systems_parts WHERE unique_id='$report_unique_id';");
}
$hours=array();
if(!$empty_report){
	while($rowHours = $resultHours->fetch_assoc())
	{
		$hdt=date(phpdispfd,strtotime($rowHours['date']));
		$numdays++;
		$hlr=$rowHours['reg_labor'];
		$tot_labor_reg += floatval($hlr);
		$hlo=$rowHours['ot_labor'];
		$tot_labor_ot += floatval($hlo);
		$htr=$rowHours['reg_travel'];
		$tot_travel_reg += floatval($htr);
		$hto=$rowHours['ot_travel'];
		$tot_travel_ot += floatval($hto);
		array_push($hours,array($hdt,$hlr,$hlo,$htr,$hto));
	}
}else{
	$tot_labor_reg = '';
	$tot_labor_ot = '';
	$tot_travel_reg = '';
	$tot_travel_ot = '';	
}
$parts=array();
if(!$empty_report){
	while($rowParts = $resultParts->fetch_assoc())
	{
		$numparts++;
		$pqty=$rowParts['qty'];
		$pnun=$rowParts['number'];
		$pdesc=$rowParts['description'];
		array_push($parts,array($pqty,$pnun,$pdesc));
	}
}
//load alt config JRD
require_once('tcpdf_config_alt.php');

// Include the main TCPDF library (search for installation path).
require_once($_SERVER['DOCUMENT_ROOT'].'/resources/tcpdf/tcpdf.php');


// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {
	private $equipment_type;
	private $system_id;
	private $system_name;
	private $report_date;
	private $ref_num;
	private $company_name;
	
	public function set_headfoot_data($data){
			$this->equipment_type = $data[0];
			$this->system_id = $data[1];
			$this->facility_name = $data[2];
			$this->report_date = $data[3];
			$this->ref_num=$data[4];
			$this->company_name=$data[5];
	}
	//Page header
	public function Header() {
		if($this->page==1){
			//Changed to remove the logo and just have the words
			/*// Logo 1
			$image_file1 = K_PATH_IMAGES.'oiservice_ctmr_logo.jpg';
			$this->Image($image_file1, 12, 12, 216, '', 'JPG', '', 'T', false, 300, 'L', false, false, 0, false, false, false);
	
			// Title
			$this->SetFont('times', 'B', 20);
			$this->SetXY(325, 22);	
			$this->Cell(180, 0, 'Field Service Report', 0, false, 'C', 0, '', 0, false, 'C', 'C');*/
	
			// Title
			$this->SetFont('times', 'B', 20);
			$this->SetXY(200, 22);	
			$this->Cell(180, 0, $this->company_name . ' Field Service Report', 0, false, 'C', 0, '', 0, false, 'C', 'C');
        }else{
            //Changed to remove the logo and just have the words
			/*$image_file1 = K_PATH_IMAGES.'oiservice_ctmr_logo.jpg';
			$this->Image($image_file1, 12, 12, 216, '', 'JPG', '', 'T', false, 300, 'L', false, false, 0, false, false, false);
	
			// Title
			$this->SetFont('times', 'B', 20);
			$this->SetXY(325, 22);	
			$this->Cell(180, 0, 'Field Service Report Cont.', 0, false, 'C', 0, '', 0, false, 'C', 'C');*/  
			$this->SetFont('times', 'B', 20);
			$this->SetXY(210, 22);	
			$this->Cell(180, 0, $this->company_name . ' Field Service Report Cont.', 0, false, 'C', 0, '', 0, false, 'C', 'C');
			
			//Add site info to headers on pages 2 and on
			$this->SetFont('times', 'B', 12);
			//page cell headers
			$this->SetXY(15,45);
			$this->Cell(228, 12, 'Facility Name', 'TLR', false, 'L', 0, '', 0, false, 'T', 'T');
			$this->Cell(96, 12, 'System ID', 'TLR', false, 'L', 0, '', 0, false, 'T', 'T');
			$this->Cell(120, 12, 'Ref Number', 'TLR', false, 'L', 0, '', 0, false, 'T', 'T');
			$this->Cell(132, 12, 'Date', 'TLR', true, 'L', 0, '', 0, false, 'T', 'T');
			//page cell content
			$this->SetFont('times', '', 12);
			$this->Cell(228, 24, $this->facility_name, 'LRB', false, 'C', 0, '', 0, false, 'T', 'M');//site name
			$this->Cell(96, 24, $this->system_id, 'LRB', false, 'C', 0, '', 0, false, 'T', 'M');//site id
			$this->Cell(120, 24, $this->ref_num, 'LRB', false, 'C', 0, '', 0, false, 'T', 'M');//ref num
			$this->Cell(132, 24, $this->report_date, 'LRB', true, 'C', 0, '', 0, false, 'T', 'M');//date
        }
		
	}

	// Page footer
	public function Footer() {
		$this->SetFont('times', '', 8);
		switch($this->equipment_type){
			case "MR":
				$this->SetXY(72,-27);
				$this->Cell(144, 12, 'Form F70-15  Rev B  2014-09-24', '0', false, 'C', 0, '', 0, false, 'C', 'L');
				break;
			case "CT":
				$this->SetXY(72,-27);
				$this->Cell(144, 12, 'Form F70-14  Rev B  2014-09-24', '0', false, 'C', 0, '', 0, false, 'C', 'L');
				break;
		}
		
		$this->SetXY(480,-27);
		$this->Cell(55, 12, 'Page '.trim($this->getAliasNumPage()).'/'.trim($this->getAliasNbPages()), 0, false, 'R', 0, '', 0, false, 'C', 'R');
		
		// Logo 2
		$image_file2 = K_PATH_IMAGES.'iso_logos.png';
		$this->Image($image_file2, 12, 740, 72, '', 'PNG', '', 'T', false, 300, 'L', false, false, 0, false, false, false);
		
		// Logo ISO
		$image_file2 = K_PATH_IMAGES.'oxford_logo.jpg';
		$this->Image($image_file2, 12, 740, 72, '', 'JPG', '', 'T', false, 300, 'R', false, false, 0, false, false, false);
		
		// 3 Dot Line
		$image_file2 = K_PATH_IMAGES.'3dotline.png';
		$this->Image($image_file2, 12, 725, 575, '', 'PNG', '', 'T', false, 300, 'R', false, false, 0, false, false, false);
		switch($this->equipment_type){
			case "MR":
				//Office Addy deerfield
				$this->SetXY(230,-51);
				$this->SetFont('times', 'B', 8);
				$this->Cell(120, 12, $this->company_name, '0', false, 'L', 0, '', 0, false, 'C', 'L');
				$this->SetFont('times', '', 8);			
				$this->SetXY(230,-39);
				$this->Cell(120, 12, '1027 S.W. 30th Ave.', '0', false, 'L', 0, '', 0, false, 'C', 'L');
				$this->SetXY(230,-27);
				$this->Cell(120, 12, 'Deerfield Beach, FL  33442', '0', false, 'L', 0, '', 0, false, 'C', 'L');
				$this->SetXY(360,-51);
				$this->SetFont('times', '', 8);
				$this->Cell(100, 12, 'Tel: +1 888 673 5151', '0', false, 'L', 0, '', 0, false, 'C', 'L');			
				$this->SetXY(360,-39);
				$this->Cell(100, 12, 'Fax: +1 954 596 4946', '0', false, 'L', 0, '', 0, false, 'C', 'L');
				$this->SetXY(360,-27);
				$this->Cell(100, 12, 'oiservice@oxinst.com', '0', false, 'L', 0, '', 0, false, 'C', 'L');
				break;
			case "CT":
				//Office Addy vacaville
				$this->SetXY(230,-51);
				$this->SetFont('times', 'B', 8);
				$this->Cell(120, 12, $this->company_name, '0', false, 'L', 0, '', 0, false, 'C', 'L');
				$this->SetFont('times', '', 8);			
				$this->SetXY(230,-39);
				$this->Cell(120, 12, '64 Union Way', '0', false, 'L', 0, '', 0, false, 'C', 'L');			
				$this->SetXY(230,-27);
				$this->Cell(120, 12, 'Vacaville, CA  95687', '0', false, 'L', 0, '', 0, false, 'C', 'L');			
				$this->SetXY(360,-51);
				$this->SetFont('times', '', 8);
				$this->Cell(100, 12, 'Tel: +1 888 673 5151', '0', false, 'L', 0, '', 0, false, 'C', 'L');			
				$this->SetXY(360,-39);
				$this->Cell(100, 12, 'Fax: +1 707 469 1318', '0', false, 'L', 0, '', 0, false, 'C', 'L');			
				$this->SetXY(360,-27);
				$this->Cell(100, 12, 'oiservice@oxinst.com', '0', false, 'L', 0, '', 0, false, 'C', 'L');
				break;
		}
	}
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

//set head and foot data
$pdf->set_headfoot_data(array($rowSystem['equipment_type'],
							  $rowSystem['system_id'],
							  $rowReport['facility_name'],
							  date(phpdispfd,strtotime($rowReport['date'])),
							  $rowReport['report_id'],
							  (string)$settings->company_name));

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor((string)$settings->company_name);
$pdf->SetTitle('Service Report');
$pdf->SetSubject('Service Report');
$pdf->SetKeywords('Healthcare');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}





$pdf->AddPage();
//make page 1
//Page cell headers
$pdf->SetFont('times', 'B', 12);
$pdf->SetXY(15,45);
$pdf->Cell(228, 12, 'Facility Name & Address', 'TLR', false, 'L', 0, '', 0, false, 'T', 'T');
$pdf->SetXY(243,45);
$pdf->Cell(84, 12, 'System ID', 'TLR', false, 'L', 0, '', 0, false, 'T', 'T');
$pdf->SetXY(243,81);
$pdf->Cell(84, 12, 'Ref Number', 'TLR', false, 'L', 0, '', 0, false, 'T', 'T');
$pdf->SetXY(327,45);
$pdf->Cell(108, 12, 'Billing Type', 'TLR', false, 'L', 0, '', 0, false, 'T', 'T');
$pdf->SetXY(327,81);
$pdf->Cell(108, 12, 'PO Number', 'TLR', false, 'L', 0, '', 0, false, 'T', 'T');
$pdf->SetXY(435,45);
$pdf->Cell(156, 12, 'Date', 'TLR', false, 'L', 0, '', 0, false, 'T', 'T');  //72
//$pdf->SetXY(507,45);
//$pdf->Cell(84, 12, 'PM Compete', 'TLR', false, 'L', 0, '', 0, false, 'T', 'T');
$pdf->SetXY(435,81);
$pdf->Cell(156, 12, 'Engineer', 'TLR', false, 'L', 0, '', 0, false, 'T', 'T');
$pdf->SetXY(15,117);
$pdf->Cell(228, 12, 'System Contact', 'TLR', false, 'L', 0, '', 0, false, 'T', 'T');
$pdf->SetXY(243,117);
$pdf->Cell(192, 12, 'System Contact Number', 'TLR', false, 'L', 0, '', 0, false, 'T', 'T');
if($rowSystem['equipment_type']=="CT"){
	//slice mas
	$pdf->SetXY(435,117);
	$pdf->Cell(156, 12, 'Slice / mAs', 'TLR', false, 'L', 0, '', 0, false, 'T', 'T');
	//rotation
	$pdf->SetXY(435,153);
	$pdf->Cell(156, 12, 'Rotations', 'TLR', false, 'L', 0, '', 0, false, 'T', 'T');
}else{
	//lhe
	$pdf->SetXY(435,117);
	$pdf->Cell(156, 12, 'LHe Reading', 'TLR', false, 'L', 0, '', 0, false, 'T', 'T');
	//vp
	$pdf->SetXY(435,153);
	$pdf->Cell(156, 12, 'Vessel Pressure', 'TLR', false, 'L', 0, '', 0, false, 'T', 'T');
}
$pdf->SetXY(15,153);
$pdf->Cell(228, 12, 'System Description', 'TLR', false, 'L', 0, '', 0, false, 'T', 'T');
$pdf->SetXY(243,153);
$pdf->Cell(192, 12, 'System Serial Number', 'TLR', false, 'L', 0, '', 0, false, 'T', 'T');
$pdf->SetXY(15,189);
$pdf->Cell(576, 18, 'Problem Reported', 'TLR', false, 'L', 0, '', 0, false, 'T', 'T');
//$pdf->SetXY(15,243);
//$pdf->Cell(576, 18, 'Problem Found', 'TLR', false, 'L', 0, '', 0, false, 'T', 'T');
$pdf->SetXY(15,297);
$pdf->Cell(576, 18, 'Repair Activities', 'TLR', false, 'L', 0, '', 0, false, 'T', 'T');
$pdf->SetXY(15,573);
$pdf->Cell(300, 12, 'Labor and Travel Hours', 'TLR', false, 'C', 0, '', 0, false, 'T', 'T');
$pdf->SetFont('times', '', 10);
$pdf->SetXY(15,585);
$pdf->Cell(60, 12, 'Date', 'L', false, 'C', 0, '', 0, false, 'T', 'T');
$pdf->SetXY(75,585);
$pdf->Cell(60, 12, 'Labor Reg', '', false, 'C', 0, '', 0, false, 'T', 'T');
$pdf->SetXY(135,585);
$pdf->Cell(60, 12, 'Labor OT', '', false, 'C', 0, '', 0, false, 'T', 'T');
$pdf->SetXY(195,585);
$pdf->Cell(60, 12, 'Travel Reg', '', false, 'C', 0, '', 0, false, 'T', 'T');
$pdf->SetXY(255,585);
$pdf->Cell(60, 12, 'Travel OT', 'R', false, 'C', 0, '', 0, false, 'T', 'T');
$pdf->SetFont('times', 'B', 12);
$pdf->SetXY(315,573);
$pdf->Cell(276, 12, 'Parts Used', 'TLR', false, 'C', 0, '', 0, false, 'T', 'T');
$pdf->SetFont('times', '', 10);
$pdf->SetXY(315,585);
$pdf->Cell(36, 12, 'QTY', 'L', false, 'C', 0, '', 0, false, 'T', 'T');
$pdf->SetXY(351,585);
$pdf->Cell(84, 12, 'Part Num', '', false, 'C', 0, '', 0, false, 'T', 'T');
$pdf->SetXY(435,585);
$pdf->Cell(156, 12, 'Description', 'R', false, 'C', 0, '', 0, false, 'T', 'T');


//Page Cell content
$pdf->SetFont('times', '', 12);
//site name and addy
$pdf->SetXY(15,57);
if($empty_report){
	$pdf->MultiCell(228, 60, '', 'LR', 'L', false, 0, '', '', true, 0, false, false, 60, 'M');	
}else{
	$pdf->MultiCell(228, 60, $rowReport['facility_name']."\n".$rowReport['address']."\n".$rowReport['city'].", ".$rowReport['state']."  ".$rowReport['zip'], 'LR', 'L', false, 0, '', '', true, 0, false, false, 60, 'M');
}
//site id
$pdf->SetXY(243,57);
$pdf->Cell(84, 24, $rowReport['system_id'], 'LR', false, 'C', 0, '', 0, false, 'T', 'M');
//ref num
$pdf->SetXY(243,93);
$pdf->Cell(84, 24, $rowReport['report_id'], 'LR', false, 'C', 0, '', 0, false, 'T', 'M');
//bill type
if($rowReport['contract_override']==""){$contract_type=$rowSystem['contract_name'];}else{$contract_type=$rowReport['contract_override_name'];}
$pdf->SetXY(327,57);
$pdf->Cell(108, 24, $contract_type, 'LR', false, 'C', 0, '', 0, false, 'T', 'M');
//po num
$pdf->SetXY(327,93);
$pdf->Cell(108, 24, $rowReport['po_number'], 'LR', false, 'C', 0, '', 0, false, 'T', 'M');
//date
$pdf->SetXY(435,57);
if($empty_report){
	$pdf->Cell(156, 24, '', 'LR', false, 'C', 0, '', 0, false, 'T', 'M');//72
}else{
	$pdf->Cell(156, 24, date(phpdispfd,strtotime($rowReport['date'])), 'LR', false, 'C', 0, '', 0, false, 'T', 'M');//72
}
//pm comp
//$pdf->SetXY(507,57);
//$pdf->Cell(84, 24, $rowReport['pm'], 'LR', false, 'C', 0, '', 0, false, 'T', 'M');
//engineer
$pdf->SetXY(435,93);
$pdf->Cell(156, 24, $rowReport['assigned_engineer_name'], 'LR', false, 'C', 0, '', 0, false, 'T', 'M');
//site contact
$pdf->SetXY(15,129);
$pdf->Cell(228, 24, $rowSystem['contact_name'], 'LR', false, 'C', 0, '', 0, false, 'T', 'M');
//equip desc
$pdf->SetXY(15,165);
$pdf->Cell(228, 24, $rowSystem['equipment_name'], 'LR', false, 'C', 0, '', 0, false, 'T', 'M');
//site tele
$pdf->SetXY(243,129);
$pdf->Cell(192, 24, $rowSystem['contact_phone'], 'LR', false, 'C', 0, '', 0, false, 'T', 'M');
//equip serial
$pdf->SetXY(243,165);
$pdf->Cell(192, 24, $rowSystem['system_serial'], 'LR', false, 'C', 0, '', 0, false, 'T', 'M');
if($rowSystem['equipment_type']=="CT"){$slche=$rowReport['slice_mas_count'];}else{$slche=$rowReport['helium_level']."%";}
//slice or helium
$pdf->SetXY(435,129);
$pdf->Cell(156, 24, $slche, 'LR', false, 'C', 0, '', 0, false, 'T', 'M');
if($rowSystem['equipment_type']=="CT"){$rotvp=$rowReport['gantry_rev'];}else{$rotvp=$rowReport['vessel_pressure']." psig";}
//rot or vp
$pdf->SetXY(435,165);
$pdf->Cell(156, 24, $rotvp, 'LR', false, 'C', 0, '', 0, false, 'T', 'M');
//prob reptd
$pdf->SetXY(15,207);
$pdf->MultiCell(576, 90, $rowReport['complaint'], 'LR', 'L', false, 0, '', '', true, 0, false, false, 36, 'T', true);
//////prob found
//$pdf->SetXY(15,261);
//$pdf->MultiCell(576, 36, $rowReport['prob_found'], 'LR', 'L', false, 0, '', '', true, 0, false, false, 36, 'T', true);
////repair
$pdf->SetXY(15,315);
$servicetxt = $rowReport['service'];
$cellcount = $pdf->MultiCell(576, 258, $servicetxt, 'LR', 'L', false, 0, '', '', true, 0, false, false, 250, 'T', false);
if($cellcount > 20){
	$secondpage = true;
	$showrepairact = true;
	$pdf->startTransaction();
	$fulllen = strlen($servicetxt);
	for($i=0;$i<=$cellcount;$i++){	 
		if($i==0){
			$servicetxtleftovers = $pdf->Write(12, $servicetxt, '', false, 'L', true, 0, true, false, 0, 12);
		}else{
			$linelen = strlen($servicetxtleftovers);
			$servicetxtleftovers = substr($servicetxt,($linelen-1)*-1);
			$servicetxtleftovers = $pdf->Write(12, $servicetxtleftovers, '', false, 'L', true, 0, true, false, 0, 12);
		}
	}
	$linelen = strlen($servicetxtleftovers);
	$servicetxtleftovers = substr($servicetxt,($linelen-1)*-1);
	$pdf = $pdf->rollbackTransaction();
}




//hours days 1-7
$pdf->SetFont('times', '', 10);
for ($i=0; $i<=6; $i++){
	$x=597+($i*12);
	if($i<=$numdays){
		$pdf->SetXY(15,$x);//date
		$pdf->Cell(60, 12, $hours[$i][0], '1', false, 'C', 0, '', 0, false, 'T', 'T');
		$pdf->SetXY(75,$x);//l reg
		$pdf->Cell(60, 12, $hours[$i][1], '1', false, 'C', 0, '', 0, false, 'T', 'T');
		$pdf->SetXY(135,$x);//l ot
		$pdf->Cell(60, 12, $hours[$i][2], '1', false, 'C', 0, '', 0, false, 'T', 'T');
		$pdf->SetXY(195,$x);//t reg
		$pdf->Cell(60, 12, $hours[$i][3], '1', false, 'C', 0, '', 0, false, 'T', 'T');
		$pdf->SetXY(255,$x);//t ot
		$pdf->Cell(60, 12, $hours[$i][4], '1', false, 'C', 0, '', 0, false, 'T', 'T');
	}else{
		$pdf->SetXY(15,$x);//date
		$pdf->Cell(60, 12, '', '1', false, 'C', 0, '', 0, false, 'T', 'T');
		$pdf->SetXY(75,$x);//l reg
		$pdf->Cell(60, 12, '', '1', false, 'C', 0, '', 0, false, 'T', 'T');
		$pdf->SetXY(135,$x);//l ot
		$pdf->Cell(60, 12, '', '1', false, 'C', 0, '', 0, false, 'T', 'T');
		$pdf->SetXY(195,$x);//t reg
		$pdf->Cell(60, 12, '', '1', false, 'C', 0, '', 0, false, 'T', 'T');
		$pdf->SetXY(255,$x);//t ot
		$pdf->Cell(60, 12, '', '1', false, 'C', 0, '', 0, false, 'T', 'T');
	}
}

$pdf->SetXY(15,681);//totals
$pdf->Cell(60, 12, 'Totals', 'LB', false, 'C', 0, '', 0, false, 'T', 'T');
if($numdays > 7){
	$secondpage=true;
	$showhours=true;
	$pdf->SetXY(75,681);//if more than 7 print this instead of totals
	$pdf->Cell(240, 12, 'Please see last page', 'BR', false, 'C', 0, '', 0, false, 'T', 'T');
}else{
	$pdf->SetXY(75,681);//l reg tot
	$pdf->Cell(60, 12, $tot_labor_reg, 'B', false, 'C', 0, '', 0, false, 'T', 'T');
	$pdf->SetXY(135,681);//l ot tot
	$pdf->Cell(60, 12, $tot_labor_ot, 'B', false, 'C', 0, '', 0, false, 'T', 'T');
	$pdf->SetXY(195,681);//t reg tot
	$pdf->Cell(60, 12, $tot_travel_reg, 'B', false, 'C', 0, '', 0, false, 'T', 'T');
	$pdf->SetXY(255,681);//t ot tot
	$pdf->Cell(60, 12, $tot_travel_ot, 'BR', false, 'C', 0, '', 0, false, 'T', 'T');

}


//parts items 1-8
if($numparts > 8){$secondpage=true;$showparts=true;}
$pdf->SetFont('times', '', 10);
for ($i=0; $i<=7; $i++){
	$x=597+($i*12);
	if($i <= $numparts){
		$pdf->SetXY(315,$x);//qty
		$pdf->Cell(36, 12, $parts[$i][0], '1', false, 'C', 0, '', 0, false, 'T', 'T');
		$pdf->SetXY(351,$x);//num
		$pdf->Cell(84, 12, $parts[$i][1], '1', false, 'C', 0, '', 0, false, 'T', 'T');
		$pdf->SetXY(435,$x);//desc
		$pdf->Cell(156, 12, $parts[$i][2], '1', false, 'C', 0, '', 0, false, 'T', 'T');
	}else{
		$pdf->SetXY(315,$x);//qty
		$pdf->Cell(36, 12, '', '1', false, 'C', 0, '', 0, false, 'T', 'T');
		$pdf->SetXY(351,$x);//num
		$pdf->Cell(84, 12, '', '1', false, 'C', 0, '', 0, false, 'T', 'T');
		$pdf->SetXY(435,$x);//desc
		$pdf->Cell(156, 12, '', '1', false, 'C', 0, '', 0, false, 'T', 'T');
		
	}
}

if($secondpage){
	$pdf->SetMargins(PDF_MARGIN_LEFT, 81, PDF_MARGIN_RIGHT);
	$pdf->AddPage();
	

	if($showrepairact){
		$servicetxt=$servicetxtleftovers;
		$pdf->SetFont('times', 'B', 12);
		$pdf->Cell(576, 18, 'Repair Activities Cont.', 'TLR', true, 'L', 0, '', 0, false, 'T', 'T');
		$pdf->SetFont('times', '', 12);
		$cellcount = $pdf->MultiCell(576, 0, $servicetxt, 'LRB', 'L', false, 1, '', '', true, 0, false, false, 0, 'T', false);
	}
	
	
	if($showhours){
		$pdf->Ln();
		$pdf->SetFont('times', 'B', 12);
		$pdf->Cell(300, 12, 'Labor and Travel Hours Cont.', 'TLR', true, 'C', 0, '', 0, false, 'T', 'M');
		$pdf->SetFont('times', '', 10);
		$pdf->Cell(60, 12, 'Date', 'L', false, 'C', 0, '', 0, false, 'T', 'M');
		$pdf->Cell(60, 12, 'Labor Reg', '', false, 'C', 0, '', 0, false, 'T', 'M');
		$pdf->Cell(60, 12, 'Labor OT', '', false, 'C', 0, '', 0, false, 'T', 'M');
		$pdf->Cell(60, 12, 'Travel Reg', '', false, 'C', 0, '', 0, false, 'T', 'M');
		$pdf->Cell(60, 12, 'Travel OT', 'R', true, 'C', 0, '', 0, false, 'T', 'M');
		for($i=7;$i<=$numdays-1;$i++){
			$pdf->Cell(60, 12, $hours[$i][0], '1', false, 'C', 0, '', 0, false, 'T', 'M');
			$pdf->Cell(60, 12, $hours[$i][1], '1', false, 'C', 0, '', 0, false, 'T', 'M');
			$pdf->Cell(60, 12, $hours[$i][2], '1', false, 'C', 0, '', 0, false, 'T', 'M');
			$pdf->Cell(60, 12, $hours[$i][3], '1', false, 'C', 0, '', 0, false, 'T', 'M');
			$pdf->Cell(60, 12, $hours[$i][4], '1', true, 'C', 0, '', 0, false, 'T', 'M');			
		}
		$pdf->Cell(60, 12, 'Totals', 'LB', false, 'C', 0, '', 0, false, 'T', 'M');
		$pdf->Cell(60, 12, $tot_labor_reg, 'B', false, 'C', 0, '', 0, false, 'T', 'M');
		$pdf->Cell(60, 12, $tot_labor_ot, 'B', false, 'C', 0, '', 0, false, 'T', 'M');
		$pdf->Cell(60, 12, $tot_travel_reg, 'B', false, 'C', 0, '', 0, false, 'T', 'M');
		$pdf->Cell(60, 12, $tot_travel_ot, 'BR', true, 'C', 0, '', 0, false, 'T', 'M');
	}
	if($showparts){
		$pdf->Ln();
		$pdf->SetFont('times', 'B', 12);
		$pdf->Cell(300, 12, 'Parts Used', 'TLR', true, 'C', 0, '', 0, false, 'T', 'T');
		$pdf->SetFont('times', '', 10);
		$pdf->Cell(36, 12, 'QTY', 'L', false, 'C', 0, '', 0, false, 'T', 'T');
		$pdf->Cell(84, 12, 'Part Num', '', false, 'C', 0, '', 0, false, 'T', 'T');
		$pdf->Cell(180, 12, 'Description', 'R', true, 'C', 0, '', 0, false, 'T', 'T');

		$pdf->SetFont('times', '', 10);
		for ($i=8; $i<=$numparts-1; $i++){
			$pdf->Cell(36, 12, $parts[$i][0], '1', false, 'C', 0, '', 0, false, 'T', 'T');
			$pdf->Cell(84, 12, $parts[$i][1], '1', false, 'C', 0, '', 0, false, 'T', 'T');
			$pdf->Cell(180, 12, $parts[$i][2], '1', true, 'C', 0, '', 0, false, 'T', 'T');
		}
	}
}
//this is the ending of the script
// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
switch($rowSystem['equipment_type']){
			case "MR":
				if($empty_report){
					$pdfname="/F70-15_Service_Report.pdf";
				}else{
					$pdfname="/F70-15_Service_Report_".$rowReport['report_id'].".pdf";	
				}
				break;
			case "CT":
				if($empty_report){
					$pdfname="/F70-14_Service_Report.pdf";
				}else{
					$pdfname="/F70-14_Service_Report_".$rowReport['report_id'].".pdf";	
				}
				break;
}
if($included){
	$pdf->Output($settings->php_base_dir . $settings->uploads_dir . $settings->uploads->service_reports . $pdfname, 'F');	
}else{
	$pdf->Output($settings->php_base_dir . $settings->uploads_dir . $settings->uploads->service_reports . $pdfname, 'FI');	
}

//============================================================+
// END OF FILE
//============================================================+

