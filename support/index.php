<?php

//include $_SERVER['DOCUMENT_ROOT'].'/Mobile_Detect.php';
//$detect = new Mobile_Detect;
//
//if ($_SESSION['mobile_device']()) {
//	header("location:mobile_login.php");
//}
$settings = new SimpleXMLElement($_SERVER['DOCUMENT_ROOT'].'/settings.xml', null, true);
?>
<!DOCTYPE html>
<html>
<head>
<title><?php readfile($_SERVER['DOCUMENT_ROOT'].'/company_name.txt'); ?> Portal</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<?php require($_SERVER['DOCUMENT_ROOT'].'/report/head_foot/default_inc_js.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/report/head_foot/default_inc_css.php'); ?>
</head>
<body>
<?php require($_SERVER['DOCUMENT_ROOT'].'/report/head_foot/default_header.php'); ?>
<div id="OIReportContent"> 
	<div style="padding-top:20px; text-align:justify; font-size:16px">
		<p><strong style="font-size:19px; color:#1B2673;">Portal Support Page!</strong></p>
		<img src="/resources/images/CT_MR_Side_by_site_Shadow.png" width="341" height="206" style="padding-left:20px;" align="right">
		<p>Below you will find links needed for Portal Support to help answer your questions and get you up and going.</p>
		<div style="margin-top:25px; margin-left:15px; width:50%; border:thick;">
			<div style="float:left;">
				<!-- TeamViewer Logo (generated at http://www.teamviewer.com) -->
				<div style="position:relative; width:200px; height:125px;">
				  <a href="http://download.teamviewer.com/download/TeamViewerQS_en.exe" style="text-decoration:none;">
					<img src="https://www.teamviewer.com/link/?url=246800&id=412800949" alt="TeamViewer for Remote Support" title="TeamViewer for Remote Support" border="0" width="200" height="125" />
					<span style="position:absolute; top:74.5px; left:5px; display:block; cursor:pointer; color:White; font-family:Arial; font-size:15px; line-height:1.2em; font-weight:bold; text-align:center; width:190px;">
					  Remote Support!
					</span>
				  </a>
				</div>
			</div>
				<div style="float:left; margin-left:25px;">
				<!-- TeamViewer Logo (generated at http://www.teamviewer.com) -->
				<div style="position:relative; width:200px; height:125px;">
				  <a href="http://download.teamviewer.com/download/TeamViewer_Setup_en.exe" style="text-decoration:none;">
					<img src="https://www.teamviewer.com/link/?url=183480&id=830706812" alt="Download TeamViewer Full version" title="Download TeamViewer Full version" border="0" width="200" height="125" />
					<span style="position:absolute; top:83.5px; left:5px; display:block; cursor:pointer; color:#424242; font-family:Arial; font-size:15px; line-height:1.2em; font-weight:bold; text-align:center; width:190px;">
					  Download and Install TeamViewer
					</span>
				  </a>
				</div>
			</div>
		</div>
		<div style="width:50%; margin-top:25px; margin-left:15px; clear:both">
			<a href="http://www.gotomeeting.com/online/collaboration/meeting/join-meeting"><img src="/resources/images/GoToMeetingLogo.png" width="248"></a>
		</div>
	</div>
</div>
<?php require($_SERVER['DOCUMENT_ROOT'].'/report/head_foot/default_footer.php'); ?>