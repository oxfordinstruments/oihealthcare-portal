<?php
$debug = true;
if(isset($_GET['debug'])){
	$debug = true;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/scripts/php_utils.php');
$phpUtil = new phpUtils();

$options = array(
		'host'	=> 	'localhost',
		'adminDN'	=>	'cn=admin,dc=oihealthcareportal,dc=com',
		'adminPW'	=>	'Plat1num',
		'baseDN'	=>	'dc=oihealthcareportal,dc=com',
		'dmsDN'		=>	'ou=dms,ou=portal,dc=oihealthcareportal,dc=com',
		'kbDN'		=>	'ou=kb,ou=portal,dc=oihealthcareportal,dc=com',
		'deref'		=>	0,
		'version'	=>	3,
		'debug'		=>	true
	);
$ldapconn = NULL;

if(!$ldapconn = ldap_connect($options['host'])){
	echo'Connected to LDAP server',EOL;
	die();
}

ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, $options['version']);
ldap_set_option($ldapconn, LDAP_OPT_DEREF, $options['deref']);

if(!$ldapbind = ldap_bind($ldapconn,$options['adminDN'],$options['adminPW'])){
	echo "Bind failed",EOL;	
	die();
}

$uids = array('dud','alleng','davisj','mardikianj','absheara','bringolfk');

//$dn = 'dc=oihealthcareportal,dc=com';
$dn = 'ou=kb,ou=portal,dc=oihealthcareportal,dc=com';
$attr = array('uid');

foreach($uids as $uid){
	$filter = 'uid='.$uid.'';
	if(!$sr = ldap_list($ldapconn, $dn, $filter, $attr)){
		echo "list failed",EOL;
		die();
	}
	$info = ldap_get_entries($ldapconn, $sr);
	echo "<pre>";
	print_r($info);	
	echo "</pre><hr>",EOL;
}


ldap_close($ldapconn);

die('DONE');




?>