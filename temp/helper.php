<?php

$debug = false;

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);
}

d($_POST);

require($_SERVER['DOCUMENT_ROOT'].'/mysqlInfo.php');
$mysqli = new mysqli("$host", "$username", "$password", "$db_name");

$sql="SELECT * FROM misc_tool_tips;";
d($sql);

if(!$result = $mysqli->query($sql)){
	$log->logerr($sql,1000,false,basename(__FILE__),__LINE__);
	$log->logerr('There was error running the query ['.$mysqli->error.']',1000,false,basename(__FILE__),__LINE__);
	$log->logerr('Error occurred, contact support.',1000,true,basename(__FILE__),__LINE__);
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Help Input</title>
	<script src="/resources/js/jquery/jquery.min.js"></script>
	<script>
		$(document).ready(function() {
			$("#tip").keyup(function() {
				$("#preview").html($("#tip").val());
			});

			$("#submit_btn").click(function() {
				submit_check();
			});

			$(".quick").each(function (index, elem) {
				$(this).wrapInner("<a href='javascript:void(0)'></a>");

			});

			$("#nl").click(function (e) {
				$("#tip").insertAtCaret("<br/>");
			});

			$("#bold").click(function (e) {
				$("#tip").insertAtCaret("<b></b>");
			});

			$("#italic").click(function (e) {
				$("#tip").insertAtCaret("<i></i>");
			});

			$("#u").click(function (e) {
				$("#tip").insertAtCaret("<u></u>");
			});

			$("#ul").click(function (e) {
				$("#tip").insertAtCaret("<ul><li></li></ul>");
			});

			$("#uli").click(function (e) {
				$("#tip").insertAtCaret('<li></li>');
			});

			$("#show").click(function () {
				$("#data").show();
			})


		});
	</script>
</head>
<body>

<form id="form" name="form" action="helper_do.php" method="post">
	<table cellpadding="5">
		<tr>
			<td><label for="element">Page Element ID</label></td>
			<td  colspan="2"><input id="element" name="element" value="" style="width: 400px;"/></td>
		</tr>
		<tr>
			<td><label for="page">Page Name e.g. systems.php</label></td>
			<td colspan="2"><input id="page" name="page" value="" style="width: 400px;"/></td>
		</tr>
		<tr>
			<td><label for="tip">Tool Tip</label></td>
			<td><textarea id="tip" name="tip" style="width: 400px; height: 200px;"></textarea></td>
			<td>
				<table>
					<tr><td><strong>Quick Inserts</strong></td></tr>
					<tr><td class="quick" id="nl">New Line</td></tr>
					<tr><td class="quick" id="bold">Bold</td></tr>
					<tr><td class="quick" id="italic">Italic</td></tr>
					<tr><td class="quick" id="u">Underline</td></tr>
					<tr><td class="quick" id="ul">Un-orderd List</td></tr>
					<tr><td class="quick" id="uli">Un-orderd List Item</td></tr>
				</table>

			</td>
		</tr>
		<tr>
			<td colspan="2" align="right"><input id="submit_btn" type="button" value="Save"/></td>
			<td>&nbsp;</td>
		</tr>
	</table>
</form>
<br/>
<h2>Preview</h2>
<div id="preview" style="width: 400px; height: 200px; border: solid; padding: 10px;"></div>
<br/>
<a id="show" href="javascript:void(0)">Show Current Data</a>
<div id="data" style="display: none">
	<br>
	<b style="color: red">Click on ID to delete</b>
	<br>
	<br>
	<table border="1">
		<tr>
			<th>Id</th>
			<th>Page</th>
			<th>Element</th>
			<th>Tip</th>
		</tr>
		<?php
			while ($row = $result->fetch_assoc()){
				echo "<tr>";
				echo "<td><a href='helper_do.php?del=".$row['id']."'>",$row['id'],"</a></td>";
				echo "<td>",$row['page'],"</td>";
				echo "<td>",$row['element'],"</td>";
				echo "<td>",$row['tip'],"</td>";
				echo "</tr>";
			}
		?>
	</table>
</div>
</body>
<script>
	$("#page").val(getCookie('page'));

	function submit_check() {
		if($("#element").val() == ""){
			alert("Element ID Empty");
			return;
		}

		if($("#page").val() == ""){
			alert("Page Name Empty");
			return;
		}

		if($("#tip").val() == ""){
			alert("Help Tip Empty");
			return;
		}

		setCookie('page',$("#page").val(),1);
		console.log("Submitting");
		$("#form").submit();

	}

	function setCookie(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays*24*60*60*1000));
		var expires = "expires="+ d.toUTCString();
		document.cookie = cname + "=" + cvalue + "; " + expires;
	}

	function getCookie(cname) {
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i = 0; i <ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length,c.length);
			}
		}
		return "";
	}

	jQuery.fn.extend({
		insertAtCaret: function(myValue){
			return this.each(function(i) {
				if (document.selection) {
					//For browsers like Internet Explorer
					this.focus();
					var sel = document.selection.createRange();
					sel.text = myValue;
					this.focus();
				}
				else if (this.selectionStart || this.selectionStart == '0') {
					//For browsers like Firefox and Webkit based
					var startPos = this.selectionStart;
					var endPos = this.selectionEnd;
					var scrollTop = this.scrollTop;
					this.value = this.value.substring(0, startPos)+myValue+this.value.substring(endPos,this.value.length);
					this.focus();
					this.selectionStart = startPos + myValue.length;
					this.selectionEnd = startPos + myValue.length;
					this.scrollTop = scrollTop;
				} else {
					this.value += myValue;
					this.focus();
				}
			});
		}
	});
</script>
</html>