<?php
$debug = true;
if(isset($_GET['debug'])){
	$debug = true;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/urlify/URLify.php');
$urlify = new URLify();

?>



<!doctype html>
<html>
<head>
<?php require_once($css_include);?>
<?php require_once($js_include);?>

<script src="/resources/js/portal_utils.js"></script>

<script type="text/javascript">
function check(){
	if(check_tbd(document.getElementById('inp').value) == true){
		document.getElementById('asdf').innerHTML = "true";	
	}else{
		document.getElementById('asdf').innerHTML = "false";	
	}
}
</script>
<meta charset="utf-8">
<title>Untitled Document</title>
</head>
<body>

test page
<input type="text" id="inp" />
<a onClick="check();">check</a>
data: <span id="asdf"></span>
</body>
</html>