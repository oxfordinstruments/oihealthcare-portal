<?php
$debug = true;
if(isset($_GET['debug'])){
	$debug = true;	
}

require_once($_SERVER['DOCUMENT_ROOT'].'/report/common/session_control.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/log/log.php');
$log = new logger();

require_once($_SERVER['DOCUMENT_ROOT'].'/resources/kint/Kint.class.php');
if(!$debug){
	Kint::enabled(false);	
}


?>



<!doctype html>
<html>
<head>
<?php require_once($css_include);?>
<?php require_once($js_include);?>

<script src="/resources/js/portal_utils.js"></script>

<script type="text/javascript">
function check(){
	var initDT = moment(document.getElementById("initial_call_date").value,'YYYY-M-D H:m');
	var responseDT = moment(document.getElementById("response_date").value,'YYYY-M-D H:m');
	var diffMins = responseDT.diff(initDT, 'minutes');
	console.log("diffMins = " + diffMins);
	
	if(diffMins > 29){
		document.getElementById("asdf").innerHTML = diffMins;
	}else{
		document.getElementById("asdf").innerHTML = 'NONE';	
	}
	
}
</script>
<meta charset="utf-8">
<title>Untitled Document</title>
</head>
<body>

test page
<label>initial YYYY-M-D H:m</label>
<input type="text" id="initial_call_date" />
<label>response YYYY-M-D H:m</label>
<input type="text" id="response_date" />
<a onClick="check();">check</a>
data: <span id="asdf"></span>
</body>
</html>