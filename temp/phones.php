<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
<script type="text/javascript">
function test(){
	var ph_regex = new RegExp(/^\s*(?:\+?(\d{1,3}))?([-. (]*(\d{3})[-. )]*)?((\d{3})[-. ]*(\d{2,4})(?:[-.x ]*(\d+))?)\s*$/g);
	if(document.getElementById("phone").value.match(ph_regex)){
		document.getElementById("ret").innerHTML = "Match";	
	}else{
		document.getElementById("ret").innerHTML = "Fail";	
	}
}

/*
Matches:

+42 555.123.4567
+1-(800)-123-4567
+7 555 1234567
+7(926)1234567
(926) 1234567
+79261234567
926 1234567
9261234567
1234567
123-4567
123-89-01
495 1234567
469 123 45 67
89261234567
8 (926) 1234567
926.123.4567
415-555-1234
650-555-2345
(416)555-3456
202 555 4567
4035555678
1 416 555 9292

*/
</script>
</head>
<body>
<input type="text" id="phone" onChange="test();" />
<span id="ret"></span>
</body>
</html>