<?php
define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

$template_file = 'test.docx';
$output_file = 'test.pdf';


require_once $_SERVER['DOCUMENT_ROOT'].'/resources/PhpWord/Autoloader.php';
\PhpOffice\PhpWord\Autoloader::register();

//echo date('H:i:s'), ' Creating new TemplateProcessor instance...', EOL;
$templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($template_file);

$text=<<<TEXT
Toni Holmes called Robert Costa to inform him of the continuing issues with the CT Scanner having 'scanning hardware stopped the scanner' errors.

Email from Toni Holmes explains:

At start-up we could complete the tube warm-up as recommended by the system. Then we would wait for about fifteen minutes (again recommended by the FSE since the scanner id turned off when not in use)to run the calibrations.
More often than not we would get a 'scanning hardware stopped the scanner' error.  The scanner would stop somewhere in the middle of the calibrations.

2. If after calibrations had successfully been run  we would scan  a test 'patient' (usually a stuffed dog with a bottle of contrast)and run through with one of our full protocols(usually a full abdomen series)to see whether or not the scanner would fail.
This was done prior to scanning an actual patient.

3. The scanner would simply stop scanning, sometime with a system generated error sometimes not. When this occurred it generally would require shutting down the system to the wall and restarting. Usually it performed as it should. Sometime not!

4. Recommended by FSE to keep the main disk drive relatively empty. This some times worked sometimes not!
Image space display generally indicated plenty of space for image allocation.

5. Other issues included: table travel failure. Inability to clear table travel faults.

6. Mostly the issues revolved around failure to scan, or scanner stopping mid-exam requiring a complete power down and reboot. Usually a delay of about fifteen minutes or more.
The important thing to remember the animal was under general anesthesia and these kind of delays cost the client an additional $200-300 dollars in anesthesia fees. One system failure required moving the animal to the MRI scanner, with all additional fees paid by VISA (
approximately($1000.00)

As of 3/17/15 the scanner appears to be working as expected, tube warm up/fast cals and scanning patients. 



TEXT;

$order   = array("\r\n", "\n", "\r");
$replace = '<w:br/>';
$newstr = str_replace($order, $replace, trim($text));


$templateProcessor->setValue('Text1', 'Test');
$templateProcessor->setValue('Desc', $text);
$temp_file = md5(uniqid()).'.tmp';
$templateProcessor->saveAs($temp_file);
shell_exec("unoconv -f pdf -o ".$output_file." ".$temp_file);


header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename='.$output_file);
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Pragma: public');
header('Content-Length: ' . filesize($output_file));
flush();
readfile($output_file);
//unlink($temp_file); // deletes the temporary file
exit;

?>